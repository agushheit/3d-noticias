---
category: La Ciudad
date: 2021-05-13T09:06:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/refugio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: El lunes abre el parador nocturno para personas en situación de calle
title: El lunes abre el parador nocturno para personas en situación de calle
entradilla: En la ciudad, según el último relevamiento, hay unos 126 individuos que
  viven a la intemperie. Se aconsejará a adultos mayores y población de riesgo a vacunarse
  contra el Covid-19, si aún no accedieron a la vacuna.

---
El termómetro durante la mañana de este miércoles en Santa Fe marcó 4°C. Las bajas temperaturas llegaron para quedarse y abrir así paso a la época más fría del año. 

En este contexto hay una porción de la población santafesina que debe atravesar día y noche a la intemperie, sobrevivir muchas veces con la panza vacía, con poco abrigo y en condiciones higiénicas vulnerables, sobre todo ante esta situación de pandemia cuando la higiene personal es crucial para prevenir al Covid-19. 

"Tenemos un relevamiento a enero pasado, que es una foto muy dinámica, donde relevamos 126 personas adultas viviendo en la calle", indicó María Victoria Rey, secretaría de Políticas de Cuidados y Acción Social de la ciudad, en diálogo con El Litoral, y mencionó que "la Municipalidad trabaja permanentemente con esta población que está atravesada por diversas problemáticas, como problemas de salud, de consumo, generadas por las circunstancias que genera vivir en la calle".

Para atender la vulnerabilidad en la que están sumergidas estas personas, desde el 2018 el municipio cuenta con un parador nocturno que funciona en el Centro Integrador Comunitario Roca, ubicado en Callejón Roca y Rivadavia. Este refugio abrirá el próximo lunes para alojar a 26 personas, según informó Rey. "Empezamos el operativo invernal para la población adulta, que es la que efectivamente duerme en la calle", señaló la secretaría de Políticas de Cuidados y Acción Social.

 

**Recorridas nocturnas** 

La Municipalidad, junto a la colaboración de la Cruz Roja, Cáritas y el Ejercito de Salvación, comenzará este miércoles por la noche la asistencia nocturna, que incluirá comida, infusión caliente y frazadas. "En estas recorridas nocturnas se les ofrece a las personas en situación de calle si quiere ir a pernoctar al parador, que está acondicionado para brindar un mejor servicio y preparado para ser utilizado en estas circunstancias de pandemia", indicó la funcionaria. 

En esta línea, Rey resaltó que se trabaja con un esquema de admisión, siendo evaluadas las personas por personal capacitado que trabaja en la asistencia y el acompañamiento. "Siempre tratamos de revincularlos con sus familias y también se les acompaña en el circuito para que accedan al sistema de salud o hacer trámites de pensión", precisó.

**Situación frente al Covid-19**

En el marco del avance de la segunda ola y el esquema de vacunación que comenzó en los primeros meses de este año, Rey contó las acciones que se toman para que los adultos mayores y personas de riesgo puedan acceder a la vacuna.

"Haremos un relevamiento sobre eso ahora. En las recorridas diurnas, que también hacemos, tratamos de conversar y acompañar a las personas sobre las sintomatologías que presentan", contó la funcionaria y agregó que "a las personas que no fueron vacunadas se les ofrecerá inscribirlos para que reciban la vacuna y después acompañarlos en su turno".

**CAMPAÑA DE ACTITUD SOLIDARIA**

La Fundación Actitud Solidaria lanzó una campaña para reunir frazadas, medias, guantes, bufandas, gorros y camperas. Para colaborar, los interesados pueden comunicarse al 342-4382479; o al 342-5218263.

**"Estamos colapsados"**

Gabriela Campins es una de las referentes de la Casita de Luján, un Centro de Día que ampara a las personas que viven en extrema vulnerabilidad. "El año pasado durante el Aislamiento Social Preventivo y Obligatorio acompañábamos a unas 14 personas. Si bien a muchas de las personas que veníamos acompañando se pudo reinsertarlas con sus familias o con un trabajo, la situación de calle se agravó muchísimo de un año al otro. El sábado a la noche tuvimos 23 personas durmiendo en Casita de Luján", lamentó la voluntaria, y agregó: "Sabemos que abrirá el parador va a ser exclusivamente para adultos mayores".

Según comentó Campins este viernes, el gobierno local recibirá a organizaciones sociales integradas en una mesa de diálogo de situación de calle. "Hemos hecho un pedido de camas al municipio porque tengo cuatro pibes durmiendo en el piso", indicó entre los pedidos que harán en la reunión.

En su trabajo diario, los voluntarios de la Casita de Luján reparten viandas todos los días, cuentan con "duchas solidarias" y llevan adelante programas de contención que incluye talleres de oficios, una escuela primaria que funciona como anexo a la Escuela n.º 29.

Para seguir adelante con este trabajo social comenzaron una campaña de donación, en la que recolectan abrigos (buzos, camperas, cobijas, guantes, bufandas, etc); ropa interior como medias y calzoncillos; reciben artículos de higiene (shampoo, acondicionador, desodorante); y calzados. Para colaborar, se puede acercar la donación a la sede de Casita de Luján (9 de Julio 6026); o bien comunicarse al 342-5103865.