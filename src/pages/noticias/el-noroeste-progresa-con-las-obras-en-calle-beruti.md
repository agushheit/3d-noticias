---
category: La Ciudad
date: 2021-07-29T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/OBRAS-BERUTI.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Noroeste progresa con las obras en calle Beruti
title: El Noroeste progresa con las obras en calle Beruti
entradilla: 'Con dos frentes de trabajo y a poco más de un mes de su inicio, las tareas
  en su totalidad cuentan con avances y se ejecutan en el marco del Plan Integrar.  '

---
El paisaje en calle Beruti de a poco comienza a transformarse: las históricas calles de ripio anegadas y deterioradas le dan paso a arterias pavimentadas, con sus correspondientes desagües pluviales. Una obra demandada que, con el avance de los trabajos, comienza a hacerse realidad después de años, y en muchos casos décadas, de espera.

Rosa reside en barrio Los troncos desde hace 22 años: “Estábamos pidiendo el asfaltado y la verdad que es maravilloso. Damos gracias porque esto trae muchos beneficios, por acá pasa el colectivo y es una avenida muy transitada y se estaban viendo muchas incomodidades. Cuando llovía, la gente no podía pasar y ahora con esta obra le va a cambiar la cara al barrio. Hace muchos años que estábamos esperando esto”, celebra la vecina.

El secretario de Obras y Espacio Público, Matías Pons, participó este martes por la mañana de una recorrida en la que supervisó el avance de los trabajos que incluyen tareas de bacheo, construcción de desagües y ejecución de pavimento sobre calle Beruti. La obra tiene dos frentes de trabajo simultáneos (con un 20% y un 15% de avance, respectivamente) y se conecta con los trabajos de Camino Viejo.

Para el secretario municipal esta es una obra que “realmente a nosotros nos llena de orgullo que se esté ejecutando. Era una prioridad del intendente Emilio Jatón y para los vecinos y las vecinas de este lugar, donde obras de infraestructura de semejante envergadura hacía años que no llegaban. Hoy dejó de ser un sueño, un anhelo y se está haciendo realidad”, aseguró.

Además, Pons destacó la rapidez con que se ejecutan los trabajos: “Estamos a punto de conectar ya todo lo que son desagües. Atrás viene la obra civil, la obra de hormigón, la nueva escala de lo que va a ser Beruti”.

La transformación de calle Beruti se inscribe en el Plan Integrar que se vincula con “llevar obras públicas de envergadura, de infraestructura a todos los rincones de la ciudad de Santa Fe. Sabemos que el Noroeste es uno de los sectores menos beneficiados de la historia en obra pública. Por eso, hoy estamos acá con estas obras. Esta permite vincular cinco barrios de la ciudad así las avenidas troncales que conectan el norte con el sur, beneficiando a más de 15 mil santafesinos”, destacó Pons, quien concluyó que “en el barrio hay muchas instituciones educativas, deportivas, sociales, centros de atención de salud, y esta obra mejora radicalmente y abre nuevos caminos para seguir con infraestructura en las calles paralelas y perpendiculares a la zona”.

**Muy esperada**

Rosa también destacó el compromiso del municipio: “Nos han escuchado y están respondiendo, vemos que la obra va rapidísimo”. Juan Carlos Cozzi, también vecino de la zona, valoró que con esta obra se “están haciendo bien las cosas, se ven cambios en la ciudad, se están viendo manifestaciones claras de que esto es lo que se debe hacer”.

Además, Cozzi compartió que “yo era uno de los tantos que me quejaba por los amortiguadores de mi coche, ya los cambié tres veces porque esto era un desastre”.

Para Julio Jerez, también de barrio Los Troncos, la obra “está quedando de 10”. El vecino que reside en la zona desde hace más de tres décadas consideró que “va quedando muy lindo. Antes se había prometido, pero nunca llegó. Yo hace más de 30 años que vivo acá, así que va a quedar hermoso, muy lindo. Era hora”.

**Dos etapas**

La primera etapa comenzó a mediados de junio y tiene un plazo de ejecución de cuatro meses. La misma incluye trabajos de bacheo en Arzeno y Cafferata, mientras que entre Cafferata y Camino Viejo se realiza pavimento de hormigón de 10 metros de ancho junto a los desagües pluviales.

En tanto, la etapa dos, cuenta con un plazo de ejecución de 6 meses. Las obras son entre Camino Viejo y Pasaje Geneyro y contemplan pavimento de hormigón a dos calzadas de 7 metros y un cantero central de 4 metros con los correspondientes desagües pluviales. Además, entre Pasaje Geneyro y Furlong, se demolerá la carpeta asfáltica existente y se colocará un nuevo pavimento de hormigón y desagües.

La obra se enmarca en el Plan Incluir del gobierno provincial, que cuenta con un presupuesto que alcanza los 234 millones de pesos. En tanto, el proyecto y el control de obras son de la Municipalidad.