---
category: Agenda Ciudadana
date: 2021-01-31T07:46:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/prepagas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Infobae
resumen: 'El Gobierno autorizó el aumento para las empresas de medicina prepaga: finalmente
  será del 3,5%'
title: 'El Gobierno autorizó el aumento para las empresas de medicina prepaga: finalmente
  será del 3,5%'
entradilla: Se trata de un incremento más bajo que el del 7% que había otorgado en
  diciembre pasado y que después se canceló por decisión de Alberto Fernández

---
Luego de varias idas y vueltas, el gobierno nacional finalmente autorizó un aumento de hasta un 3,5% en las cuotas de la medicina prepaga a partir de marzo del 2021. La decisión fue confirmada a través de la Resolución 531/2021, publicada este sábado en el Boletín Oficial.

Este incremento es más bajo que el del 7% que había otorgado el Poder Ejecutivo en diciembre pasado, el cual nunca se llegó a aplicar porque a las pocas horas de haber sido anunciado se canceló “por expresa decisión” del presidente Alberto Fernández, y mucho más inferior al del 10% que reclaman las empresas del sector.

De esta manera, ahora las compañías inscriptas en el Registro Nacional de Entidades de Medicina Prepaga (RNEMP) deberán notificarles a sus usuarios sobre estos cambios “con una antelación no inferior a los 30 días corridos, contados a partir de la fecha en que la nueva cuota comenzará a regir”.

En el documento publicado este sábado, y firmado por el ministro de Salud, Ginés González García, se aclaró que esta suba podrá ser de hasta el 3,5% “con relación a los valores vigentes al momento de entrada en vigencia de la presente Resolución”.

Entre los considerandos, las autoridades nacionales señalaron que “la Cámara de Instituciones Médico Asistenciales de la República Argentina (CIMARA) y la Asociación de Entidades de Medicina Privada (ADEMP) han informado el impacto que les ha causado el incremento de los costos del sector desde la fecha del aumento de cuotas autorizado a las Entidades de Medicina Prepaga en diciembre de 2019, especialmente en el delicado contexto de atención de la pandemia mundial suscitada a principios de 2020″.

En este contexto, los representantes de ambas instituciones “han requerido a la Superintendencia de Servicios de Salud que, en el ejercicio de las referidas competencias que le son propias, se sirva autorizar nuevos aumentos que permitan recomponer el financiamiento para afrontar tales costos, a aplicar eventualmente en tramos”.

Ante ese pedido, el organismo dependiente del Ministerio que conduce González García sostuvo “que resulta razonable autorizar” un incremento para el sector, al reconocer “que los costos fijos de estructura asistencial” se “incrementaron” en el último tiempo.

En el marco de un creciente conflicto entre el Gobierno y las empresas de medicina prepaga, Alberto Fernández recibió a principios de este mes en Casa Rosada a Claudio Belocopitt, dueño de Swiss Medical y titular de la Unión Argentina de Entidades de Salud privadas (UAS), para llevarle tranquilidad. En esa dirección, en el encuentro el Presidente le aseguró al empresario que no hay ningún plan para intervenir o estatizar a las prepagas, que era uno de los principales temores del sector.

Mediante una resolución publicada en el Boletín Oficial, el último día de 2020 el Gobierno oficializó un aumento del 7% en las cuotas de la medicina privada a partir de febrero de 2021. En ese momento, las empresas se sorprendieron, ya que el porcentaje fue menor que el que se había anticipado semanas antes, cuando se había anunciado una suba del 10%. Lo mismo había sucedido con el aumento estipulado para diciembre: en principio se informó que se incrementaría un 15% y el Gobierno terminó aprobando una suba menor.

Sin embargo, el asombro fue aún mayor cuando en la tarde del 31 de diciembre, con otra norma publicada en el Boletín Oficial, el Gobierno suspendió el incremento. Esto dejó a las prepagas en una situación de crisis, dado que al freno del aumento se suma la quita de ayuda estatal, que en su momento se materializó a través del programa ATP. En consecuencia, las firmas del sector remarcaron que “no pasaban” de enero, si no se encontraba una solución a la caída de los ingresos.

Asimismo, alertaron sobre posibles faltantes en las prestaciones, incluyendo los hisopados para la detección del COVID-19.

    https://app.forestry.io/sites/9yrb5nwakltpga/#/page-create?section=noticias-f4e22c74-2e45-4c43-b19b-2ca901a14fe1&pageType=noticia