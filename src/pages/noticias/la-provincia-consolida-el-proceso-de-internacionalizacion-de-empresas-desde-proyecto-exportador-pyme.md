---
category: Estado Real
date: 2021-08-20T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/PYME.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia consolida el proceso de internacionalización de empresas desde
  proyecto exportador Pyme
title: La provincia consolida el proceso de internacionalización de empresas desde
  proyecto exportador Pyme
entradilla: Alrededor de 100 empresas santafesinas participan del programa que busca
  posicionar empresas en el mercado internacional.

---
La Secretaría de Comercio Exterior del Ministerio de Producción, Ciencia y Tecnología avanza en el desarrollo de la segunda etapa del Proyecto Exportador Pyme, en el marco del Programa Desafío Exportador, iniciativa en la que ya participan 97 empresas santafesinas que buscan capacitarse, con el objetivo de lograr su internacionalización, optimizar y potenciar sus recursos particulares, propiciando la exportación de sus productos.

Esta herramienta se lleva adelante en forma conjunta con la Agencia Argentina de Inversiones y Comercio Internacional (AAICI) y en colaboración con las cámaras de comercio exterior de la provincia y otros organismos dedicados a la promoción en la materia.

En ese marco, el secretario de Comercio Exterior, Germán Burcher, mantuvo este jueves un encuentro de trabajo con representantes de la AAICI para delinear las acciones a desarrollar y sus estrategias con el fin de tener más pymes exportadoras en el territorio provincial. “Este año, ya los asistentes técnicos están apoyando a las 97 pequeñas y medianas empresas que están participando del programa. Cada una cuenta con un profesional capacitado que las ayudará a formular y ejecutar un plan de negocios internacional a su medida, formular un plan de inversión individual y acompañarla en la búsqueda del financiamiento o de los socios pertinentes”, indicó.

“Desde la gestión del gobernador Omar Perotti, junto al ministro Daniel Costamagna, estamos trabajando con el objetivo de aumentar el número de pymes exportadoras”, concluyó el funcionario provincial.

Sobre el encuentro, el coordinador técnico del programa a nivel nacional, Mariano Mastrángelo, precisó: "Dialogamos sobre las experiencias que tuvimos en la primera etapa, que comenzó el año pasado y ya estamos terminando, ya comenzamos la segunda con algunas mejoras. Hemos visto los impactos positivos que ha generado en diversos aspectos, que recaen no solamente en el logro de las ventas internacionales, sino también en mejorarles a las pymes sus procesos comerciales en el mercado local".

"A partir de la herramienta las empresas aprenden cómo se hace el proceso de exportación para que puedan realizarlo solas y formar recursos humanos locales que pasen por esta experiencia para que puedan replicarla. Eso es lo que estamos tratando de potenciar a su máxima expresión y perseguimos a partir de Desafío Exportador, que fue una idea que nace gracias al director de la Agencia Argentina de Inversiones y Comercio Internacional, Juan Usandivaras. El programa tiene muy buenos resultados, algunas de las empresas ya lograron exportar antes de finalizarlo y esperamos que cada vez se sumen más. Además, que podamos instalar esta cultura exportadora que necesitamos en Argentina”, culminó el coordinador técnico del programa.