---
category: Agenda Ciudadana
date: 2021-05-04T08:29:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/rio-parana.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: El río Paraná sigue bajando y los expertos ya piensan en cinco impactos posibles
title: El río Paraná sigue bajando y los expertos ya piensan en cinco impactos posibles
entradilla: Las previsiones del INA indican que en mayo el nivel del río en Santa
  Fe estará llegando al metro. Alertan que los efectos de la bajante en 2020 provocarían
  un escenario más grave en 2021.

---
El escenario de bajante en la costa santafesina vuelve a hacerse presente con registros preocupantes. Según el INA, los registros históricos que se sostuvieron durante todo 2020 provocarían que la falta de agua durante este año pueda ser de aún mayor gravedad. Alertan que las represas brasileras en la cuenca del Paraná no podrán realizar descargas adicionales como en 2020, por falta de almacenamiento.

Las mediciones actuales en el puerto de Santa Fe indican que el nivel del río está en 1,48 metros de piso y con tendencia a la baja. Según las proyecciones hidrométricas del Instituto Nacional del Agua (INA), el nivel bajará hasta llegar cerca del metro de profundidad durante mayo y con una perspectiva "para nada favorable".

UNO Santa Fe dialogó con el Ingeniero Juan Borús, subgerente de Sistemas de Información y Alerta Hidrológico del INA, quien al respecto indicó: "Tenemos una perspectiva para nada favorable y ya estamos pensando en los cinco de los impactos más significativos. Uno es el problema de las tomas de agua, que es el mayor problema en el que estamos poniendo toda la artillería para tratar de solucionarlo".

**Se adaptaron tomas de agua en Santa Fe**

A propósito de esto, UNO consultó sobre el estado de las tomas de agua en Santa Fe a Guillermo Lanfranco, gerente de Comunicación de Aguas Santafesinas. En este sentido, Lanfranco destacó que "en principio hay una situación de normalidad en la captación. Hicimos adaptaciones, en la toma del río Santa Fe bajamos una bomba a un nivel inferior para que trabaje mejor y ahora vamos a instalar una bomba nueva en remplazo de una que teníamos instalada desde el año pasado, con una capacidad superior".

**Las represas del Paraná están sin agua**

Según el experto del INA, el factor más importante son las lluvias, afirmando que "ha habido un marco climático desfavorable que es persistente y va a continuar". El otro factor que postuló Borús es el de las presas de embalse en Brasil, "que jamás son causas de bajante y no tiene ningún sentido pensarlo como causa. Las presas de embalse fueron hechas para la generación de electricidad y para que la central de la presa se justifique tiene que que haber generación eléctrica" dijo a UNO el especialista.

"El año pasado tuvimos 14 reuniones entre técnicos muy fructíferas, en el marco de las cancillerías de Argentina, Brasil y Paraguay. Gracias a esto se pudieron hacer descargas adicionales especiales desde Itaipú y desde la represa de Salto Santiago en el río Iguazú. El año pasado eso se pudo hacer. Este año esa posibilidad es mucho menos probable, porque las reservas de agua en los embalses son mucho menores. Si hace falta hacer ese movimiento no será tan fácil. Por lo tanto, no es de esperar que haya descargas especiales ni que haya algún cambio", afirmó el ingeniero.

**Otros impactos probables de la bajante**

Siguiendo con la descripción de los impactos probables, Borús indicó: "Otros problemas son el del impacto de la fauna íctica, que también es severo y muy importante. El problema del nivel fluvial atendiendo a que estamos en otoño y que ya vimos el año pasado lo crítico que fue".

El especialista hizo mención a la "inestabilidad de márgenes, que en Santa Fe se vio el año pasado a la altura de Puerto Gaboto, con la parte inferior de la cuenca del Paraná, que es más barrancosa". Cuando las aguas bajan tanto terminan habiendo roturas o deslaves de barrancas.

El quinto motivo que mencionó el experto del INA es el de los focos de incendio en las islas, que según Borús "es una incógnita, porque el año pasado tuvo una gran intensidad y me cuesta creer que este año tenga la misma magnitud, y no es más que el problema de los incendios en las islas en principio y luego en zonas bajas".