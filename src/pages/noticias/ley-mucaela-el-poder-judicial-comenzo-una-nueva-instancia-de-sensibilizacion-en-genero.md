---
category: Estado Real
date: 2021-10-06T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/sencibiliza.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Ley Mucaela: El poder judicial comenzó una nueva instancia de sensibilización
  en genero'
title: 'Ley Mucaela: El poder judicial comenzó una nueva instancia de sensibilización
  en genero'
entradilla: La instancia que inició este lunes, está destinada a la Procuración General,
  fiscales y defensores/as de cámara.

---
En un acto encabezado por la ministra de Igualdad, Género y Diversidad, Celia Arena; junto al presidente de la Corte Suprema de Justicia, Roberto Falistocco; la ministra, María Angélica Gastaldi; y el Procurador General Jorge Barraguirre, comenzó una instancia más de la formación en género y diversidad sexual en el marco de Ley Micaela, esta vez destinada a la Procuración general, fiscales y defensores de cámara. Esta instancia contó con la conferencia inaugural de Marisa Herrera, abogada, doctora en Derecho (UBA), investigadora del CONICET y consultora de UNICEF.

“El compromiso de llevar la Ley Micaela a cada rincón de Santa Fe se corresponde con la misión de compatibilizar la vida institucional de nuestra provincia con los instrumentos internacionales de derechos humanos, específicamente, con aquellos que tienen que ver con la prevención de la violencia por razones de género. Al hacerlo, estamos reconociendo que hay ciertas normas, hábitos y prácticas en el derecho que tienen efectos. discriminatorios en la vida cotidiana de las mujeres y diversidades sexuales.  
En este sentido, la perspectiva de género no es una elección, constituye una obligación a la que como servidores y servidoras públicos estamos sujetos y sujetas, ya que nuestro país suscribió a estos tratados y nuestro accionar puede contribuir a disminuir o a acrecentar las brechas de desigualdad para esas mujeres que recurren a los distintos niveles del estado", sostuvo Celia Arena, ministra de Igualdad, Género y Diversidad.

"Es por ello que, por decisión del Gobernador Omar Perotti, desde el Ministerio llevamos adelante la formación correspondiente a la Ley Micaela en cada rincón de la provincia, en municipios y comunas, en gremios, sindicatos, entre otras y este lunes dimos inicio a esta formación para el Poder Judicial con la presencia de Marisa Herrera", agregó.

Cabe destacar que Santa Fe es la primera provincia en establecer un curso obligatorio en perspectiva de género para el ingreso al Poder Judicial, poder en el que ya que se llevan capacitadas más de 3000 personas en la materia por parte de los replicadores del Poder Judicial santafecino formados en la Oficina de la Mujer de la Corte Suprema de la Nación, agregándose que desde la implementación de la Ley Micaela se dictaron un total de 34 talleres con 1334 personas capacitadas.

La capacitación se enmarca en la ley Nacional Nº 27499 y en la provincial Nº 13891, tiene como objetivo promover la incorporación del enfoque de género y diversidad en las prácticas judiciales, procurando impulsar la transformación de aspectos estructurales de las instituciones que contribuyan a garantizar a las mujeres y personas LGBTI+ el acceso efectivo a la justicia y al pleno ejercicio de sus derechos.

Durante el acto, acompañaron el ministro de la Corte Eduardo Spuler; las secretarias de Mujeres, Géneros y Diversidad y de Formación y Capacitación para la Igualdad, Florencia Marinaro, y Lorena Battilana, respectivamente; Cesar Merlos de la Dirección de provincial de Desarrollos Institucionales, y demás autoridades de la Procuración y del Ministerio de Igualdad, Género y Diversidad de la provincia.

**SOBRE LEY MICAELA**

  
La ley N° 27499 conocida como "Ley Micaela", es una normativa que establece la capacitación obligatoria en la temática de género para todas las personas que integran los tres poderes del Estado (Ejecutivo, Legislativo y Judicial).

El 7 de octubre de 2019 la provincia adhirió a la normativa y el 6 de marzo de 2020 se firmó la reglamentación de la Ley, conjuntamente a un convenio de colaboración con la fundación Micaela García "La Negra".

A partir de dicha reglamentación, el gobernador Omar Perotti, y su gabinete recibieron la capacitación correspondiente a la ley.

**SOBRE EL INGRESO AL PODER JUDICIAL**

  
Cabe recordar que Santa Fe es la primera provincia en establecer un curso obligatorio en perspectiva de género para el ingreso al Poder Judicial.

Según el decreto, que nombra entre sus fundamentos la adhesión de la provincia de Santa Fe a la Ley Micaela, se considera excluyente la realización de una capacitación en perspectiva de género para acceder al concurso para ser funcionario o funcionaria del Poder Judicial y juez o jueza.

La organización, metodología, duración y vigencia de la misma será dispuesta por la Escuela de Abogados del Estado en colaboración con el Ministerio de Gobierno, Justicia, Derechos Humanos y Diversidad, a través de la Secretaría de Justicia y el Ministerio de Igualdad, Género y Diversidad de Santa Fe.