---
category: Agenda Ciudadana
date: 2022-11-21T10:16:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/highway-3392100_1920.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'con informacion de Acara: www.acara.org.ar'
resumen: 'Santa fe es la segunda provincia con mas motos patentadas y cuarta en autos. '
title: 'Santa fe es la segunda provincia con mas motos patentadas y cuarta en autos. '
entradilla: En el mes de Octubre, se patentaron en nuestra provincia 3240 motos, 27%
  menos que igual mes de 2021. En automóviles, 3287 unidades patentadas, un 17% mas.

---
ACARA,  la Institución que representa a todos los Concesionarios Oficiales de Automotores en la República Argentina, dio a conocer en sus habituales informes mensuales, los datos del mercado automotor y de motovehículos. En lo que corresponde a patentamiento de  motos,  fueron casi 30 mil  (29557) en octubre,  un 20% menos que el mismo mes de 2021, pero comparando  los datos acumulados anuales, el patentamiento de motos se incremento en un 11.8%. La provincia de Santa Fe es la Segunda en cantidad de motos patentadas. 

El presidente de la División Motovehículos de ACARA, Horacio Jack comentó: “al igual que lo sucedido en septiembre, en octubre la problemática de las importaciones, tanto en unidades como en partes, vuelve a mostrar su impacto, ya que continua con un nivel importante de restricciones. De todas formas, el acumulado anual viene dando casi un 12% de crecimiento y creemos que el comportamiento del mercado seguirá hacia arriba porque los motovehículos se siguen afianzando como medio de transporte en cada vez más provincias y ciudades, no solo en el caso de las unidades más chicas, sino también en las del segmento medio, para viajes y traslados de muchos kilómetros, un segmento que si tuviera una mayor oferta de unidades podrían haber incrementado los patentamiento. Seguimos proyectando un crecimiento para este año superior al 10% y poder completar el mismo cerca de las 420.000 unidades. En este sector la gran diferencia la sigue haciendo la financiación, un aspecto clave en el que seguimos trabajando desde ACARA junto a las fábricas y el Estado, con la implementación del programa de financiación del Banco Nación, Mi Moto. Vamos a seguir dándole continuidad al mismo, insistir con medidas que mejoren una oferta más completa de productos y sumar opciones de financiación, tal como lo estamos solicitando desde todos los eslabones del sector", completó “Hori” Jack. 

Las  que ocupan el podio: en primer lugar  la Honda wave 110, segunda la Corven Energy 110 y tercera Motomel B110. El ultimo dato que extraemos del informe mensual de Acara (www.acara.com.ar) es el origen de las motos, donde revela que el 97.5% son de origen nacional.

Por ultimo,  los datos publicados que completan las composición del mercado de motovehículos,  son las TRANSFERENCIAS,  que dan cuenta de 32697 unidades que cambiaron la titularidad, un 6% mas que en el mismo mes de 2021. Anualmente esto significa un 11.31% mas que el mismo periodo del año anterior. 

En cuanto al mercado automotor, los patentamientos de octubre fueron -7,9% menores a los de septiembre (32.070 unidades), aunque mostraron una variación positiva del +14,3% respecto del mismo mes del año pasado. Esta situación es producida por la estacionalidad normal del año. 

El presidente de ACARA, Ricardo Salomé comentó "La verdad que el mercado nos sigue sorprendiendo todos los meses, y a la hora de buscar los motivos de los más de 32.000 patentamientos de octubre y este buen crecimiento interanual, no puedo dejar de destacar el extraordinario esfuerzo de toda la cadena de valor, con una mirada de largo plazo, porque pese a las dificultades, estamos trabajando con un horizonte de sustentabilidad y en equipo. Cuando miramos para atrás y repasamos que durante este año hemos tenido una crisis global de semiconductores, tensiones y problemas en la cadena logística, momentos de incertidumbre cambiaria y un aumento creciente de restricciones de piezas y vehículos importados, que alcanzó en su pico máximo en septiembre, es realmente muy positivo que estemos un 6% por encima en el acumulado anual contra 2021. En los dos meses que quedan, nos encaminamos ahora a superar las 400.000 unidades y seguir trabajando para que en 2023 consolidemos el mercado en estos niveles y podamos incrementarlo un 4/6%, una cifra que podrá claramente incrementarse si logramos una mayor liberación de importaciones. La gente va a seguir demandando vehículos y nuestro desafío será entonces poder abastecernos con más modelos de producción nacional, que ya están cercanos al 60% del mercado, e insistir que quienes se adapten a los vehículos disponibles, están ante una excelente oportunidad de inversión y disfrute", completó Salomé. 

El top 3 de la MARCAS de los mas patentados lo componen, en tercer lugar Volkswagen, en segundo lugar, Renault y encabeza la grilla Toyota. Esto es en marcas; en modelos, el auto mas patentado es el FIAT CRONOS, en segundo lugar la AMAROK de WOLKSWAGEN y en tercer lugar, el PEUGEOT 208.

La provincia de Santa Fe, ocupa el cuarto lugar en en total de unidades patentadas en el mes de octubre, atrás de Buenos Aires, CABA y Córdoba.