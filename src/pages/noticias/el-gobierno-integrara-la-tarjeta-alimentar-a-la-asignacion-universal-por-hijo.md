---
category: Agenda Ciudadana
date: 2021-10-19T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/zabaleta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno integrará la Tarjeta Alimentar a la Asignación Universal por
  Hijo
title: El Gobierno integrará la Tarjeta Alimentar a la Asignación Universal por Hijo
entradilla: El Gobierno definió que, a partir de noviembre, la tarjeta Alimentar se
  cobrará junto a la AUH para que las familias puedan usar el dinero físico u electrónico
  de modo de ampliar el sistema de compras.

---
El Gobierno nacional anunció este lunes la integración la tarjeta Alimentar a la Asignación Universal por Hijo (AUH), a partir del mes próximo, para que el beneficiario pueda utilizar todo el monto acreditado a través de dinero físico o con débito.  
  
El presidente Alberto Fernández mantuvo una reunión en su despacho de la Casa Rosada con el ministro de Desarrollo Social, Juan Zabaleta, y la directora Ejecutiva de la Administración Nacional de Seguridad Social (Anses), Fernanda Raverta, para definir al proyecto.  
  
"A partir de noviembre, el monto de la Tarjeta Alimentar pasa a depositarse en la cuenta de la AUH", dijo el Presidente en su cuenta de Twitter tras la reunión.  
  
El mandatario afirmó que de este modo "las familias van a poder organizar mejor sus compras y se amplían las posibilidades de consumo", ya que "podrán utilizar el dinero extrayéndolo del cajero o comprando con débito".  
  
Zabaleta dijo luego en conferencia de prensa, en la Casa de Gobierno, que la medida "tiene que ver con seguir garantizando las políticas alimentarias", que se inició en el caso de la tarjeta Alimentar "a comienzos de 2020" y que distribuye 19.000 millones de pesos mensuales para 3,9 millones de beneficiarios.  
  
El ministro también reseñó que la Administración Federal de Ingresos Públicos (AFIP) "devuelve un 15% del IVA, con un tope de 2.400 pesos, a los beneficiarios".  
  
Además destacó el proyecto de cambiar planes sociales por empleo y ejemplificó el caso de los trabajadores de la zafra, en donde 50.000 beneficiarios del Plan Potenciar Trabajo "cobraron salarios de convenio", y lo mismo ocurre "con la construcción, un sector muy dinámico, que pasó de 80 mil puestos de trabajo perdidos a marzo de 2020 a 90 mil nuevos empleos a agosto" pasado, y "con los gastronómicos", tras un convenio similar firmado la semana pasada.  
  
El proyecto de ley que presentó el presidente de la Cámara de Diputados, Sergio Massa, "va en la misma línea", consideró Zabaleta, y agregó que "cada sector tiene su propia modalidad y tiempo de contratación", y con todos ellos "hay diálogo".  
  
Para el ministro, la nueva norma "permitirá utilizar el dinero físico o electrónico", que servirá también "para dinamizar la economía y el consumo, descentralizándolo".  
  
En esa línea, Zabaleta apuntó que "es importante pensar que esto va a beneficiar al consumo en los barrios" y ejemplificó a "los almacenes, carnicerías, verdulerías, ferias y emprendedores".  
  
Ahora "se podrá utilizar en comercios sin posnet e ir a los mercados de barrios y economía popular, que tanto sostuvieron a familias en la pandemia" del coronavirus, completó.  
  
Rechazó que esta fuera una medida electoralista de cara a la compulsa del 14 del mes próximo, al contestar que "el Presidente entiende el problema del hambre y la pobreza y puso en marcha esta enorme política con la tarjeta Alimentar en enero de 2020, que tuvo aumentos según las escalas".  
  
En cuanto al aumento de precios, se mostró predispuesto a "ayudar en los controles" que hará la Secretaría de Comercio Interior, con "más Estado, no solo nacional sino provinciales y municipales", porque "hay que cuidar el bolsillo de los argentinos", y en ese sentido pidió "el acompañamiento de los empresarios".  
  
"Confiamos en la mesa de diálogo" que entabló el flamante secretario Roberto Feletti con los empresarios del sector, indicó el titular de la cartera de Desarrollo Social, y enfatizó que "el Presidente busca garantizar los alimentos de las familias".  
  
En la misma conferencia, Raverta consideró que la medida busca solucionar "las dificultades que encuentran algunas familias que iban a comprar con la tarjeta Alimentar, porque solo ciertos lugares tenían la posibilidad de vender, como los supermercados y no aquellos que no tenían el posnet", y refirió que eso "complicaba la diaria", al manifestar que las familias adquieren productos "en comercios de proximidad".  
  
La titular de la Anses comparó que "en el gobierno de Mauricio Macri a la AUH cayó en 20% su poder de compra" y en la actualidad "junto con la Tarjeta Alimentar se elevó a 110%", porque consideró que a la actual gestión busca "estrategias para defender el bolsillo de las familias".  
  
Y recordó que en la gestión anterior la AUH "cubría el 80%" de la canasta básica alimentaria, y en la actualidad, sumada a la Tarjeta Alimentar "es del 172%" y "se actualiza trimestralmente por la fórmula de movilidad".  
  
Raverta advirtió que la AUH "cumplió 12 años y la historia marcó que las familias son responsables de su uso y no hay comprobación empírica de que donde hay necesidades se use mal", y expresó que esa asistencia "mejora la calidad de vida diaria de los niños".  
  
La tarjeta Alimentar es un instrumento que desde enero de 2020 entrega el Estado nacional para que todas las personas puedan acceder a la canasta básica alimentaria y está dirigida a madres o padres con hijos e hijas de hasta 14 años de edad que reciben la AUH, embarazadas a partir del tercer mes que reciben AUH, personas con discapacidad que reciben AUH y madres con más de 7 hijos.  
  
La tarjeta se implementa de forma automática, a partir del cruce de datos de la Anses y de la AUH, y ese organismo es el encargado de notificar al titular que está en condiciones de retirar su tarjeta por el banco que determine cada provincia.  
  
El monto varía en función de la situación del titular y su familia: es de 6.000 pesos para las familias con un hijo o una hija de hasta 14 años de edad o con alguna discapacidad, y para quienes perciben la asignación por embarazo; de 9.000 pesos en el caso de los grupos familiares que tienen dos hijos o hijas en la misma franja etaria o con alguna discapacidad, y de 12.000 pesos para familias con tres hijos o más menores de 14 años de edad.