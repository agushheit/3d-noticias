---
category: La Ciudad
date: 2021-05-31T06:15:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/CIUDAD.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Santa Fe: ¿qué pasa con el "área calma" en la cual los autos tienen tope
  de velocidad?'
title: 'Santa Fe: ¿qué pasa con el "área calma" en la cual los autos tienen tope de
  velocidad?'
entradilla: 'Se trata de "Ciudad 30". En el Concejo, hay dudas sobre los controles
  con radares e inspectores, y los espacios de guarda para bicis. '

---
Ciudad 30 es un proyecto innovador de movilidad urbana que busca reducir los velocímetros en un sector urbano históricamente congestionado, y fomentar el uso de vehículos saludables, como las bicicletas.

La delimitación de la "zona calma" (aprobada por ordenanza en el Concejo el 6 de agosto de 2020, a instancias del Mensaje N° 9 Ejecutivo) es entre calles Urquiza al oeste, Suipacha al norte, General López al sur y Rivadavia (desde Suipacha hasta Mendoza) y 27 de Febrero (desde Mendoza hasta General López) al este.

Es esa área es donde se registra la mayor concentración de la actividad administrativa y el flujo vehicular de esta capital. Según las fuentes municipales, los inspectores hoy activos están "principalmente están abocados a los controles en territorio (por las restricciones a la circulación vehicular)", Y aseguraron que "la semana que viene habrá más personal para hacer controles" en el sector de Ciudad 30.

Además, añadieron hay acuerdos ya firmados con la UTN Santa Fe. Los convenios a que se lleguen con esa facultad regional "permitirá tener radares y así poder sancionar a los infractores". También, que "se está trabajando con la Agencia Nacional de Seguridad Vial para realizar campañas de educación vial sobre el tema en escuelas e instituciones educativas dentro del radio de Ciudad 30, entendiendo que es la educación en los más chicos lo que modificará las conductas".

**Pedido de informes**

En el Concejo se ingresó un pedido de informes donde se le requiere al municipio que brinde precisiones sobre el efectivo cumplimiento de Ciudad 30. Allí se pide saber qué resultados se han alcanzado desde la implementación de la norma; si se controla el cumplimiento de la disponibilidad de bicicleteros en edificios públicos, y si se fiscaliza el cumplimiento del área destinada a la guarda de motovehículos, ciclomotores y bicicletas en las playas de estacionamiento (ver Bicicleteros). La iniciativa, de Jorgelina Mudallel (Interbloque PJ - Frente de Todos).

Para la concejala, la idea de Ciudad 30 es "muy buena" (su bloque de hecho acompañó la aprobación de la iniciativa el año pasado). "Hay ciudades donde ya se implementa, existen argumentos de la ONU muy claros respecto a las medidas a adoptar para disminuir la siniestralidad en los microcentros de las grandes ciudades, y además estimular la movilidad sustentable, como la bici, y democratizar el espacio público para los peatones. Es un nuevo paradigma de movilidad y está justificado teóricamente", subrayó.

La radarización genera que la gente respete las normas. Pero no hay radares ni inspectores municipales. Además, nadie va a cumplir la norma por ver colgado un cartel dando la bienvenida a la zona calma: hablamos de cambio en las prácticas de la gente, de respetar al peatón, al ciclista. Y esto se cambia con controles, campañas de concientización y de educación vial", consideró la edila.

**"Que no quede sólo en un cartel"**

Joaquín Azcurrain, referente de CicloCiudad, declaró en un comunicado -a propósito de Ciudad 30, que esta iniciativa "no puede quedar sólo en un cartel". "No hemos visto que desde la gestión municipal se hayan tomado acciones concretas para hacerla realidad más allá de haber colgado algunos carteles".

"La baja de velocidades es necesaria, muchísimo más en este momento de la pandemia, donde se torna imprescindible reducir la siniestralidad vial para que las pocas camas disponibles se usen para atender casos de Covid-19", expresó. Y adelantó que desde le presentará un informe al intendente Emilio Jatón sobre las esquinas que "necesitan un rediseño urgente. Ciudad 30 es una ordenanza que presentó el mismo Ejecutivo: es tiempo de que empiece a hacerse realidad".

**Bicicleteros**

La ordenanza en cuestión establece que a partir del 1° de enero de 2021, los edificios públicos (nacionales, provinciales o municipales), con sede en el área comprendida en Ciudad 30, deberán garantizar la disponibilidad de bicicleteros públicos en espacios cubiertos o semicubiertos.

Y las playas de estacionamiento deberán admitir el ingreso de motos y bicicletas, reservando para ello como mínimo un 15% del total de la superficie destinada al estacionamiento de automóviles, considerando un 5% para moto vehículos y ciclomotores; y un 10% para bicicletas medido en superficie del espacio de estacionamiento. El pedido de informes de Mudallel también pide saber sobre estos puntos.

\-Con información de El Litoral