---
category: Agenda Ciudadana
date: 2021-04-10T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/PASO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Se confirmaron las fechas para las elecciones provinciales y se reglamentó
  la ley de paridad
title: Se confirmaron las fechas para las elecciones provinciales y se reglamentó
  la ley de paridad
entradilla: Este viernes, el gobernador firmó el decreto de convocatoria. Se ratificó
  la decisión de organizar comicios conjuntos con los nacionales, aunque las fechas
  en esa instancia podrían modificarse

---
Respetando el concepto de "simultaneidad", la provincia realizó la convocatoria para las próximas elecciones en Santa Fe. Ésa es prácticamente la única certeza por estas horas, en un escenario nacional plagado todavía de indefiniciones, e incierto por la coyuntura sanitaria. Pero los plazos vencen. Y a nivel local, la legislación estipula que antes del 10 de abril, el Poder Ejecutivo tiene que convocar formalmente a los comicios que permitirán renovar un puñado de intendencias, todas las comisiones comunales y concejos municipales. El decreto fue firmado este viernes viernes por Omar Perotti.

El ministro de Gobierno, Roberto Sukerman, había adelantado a El Litoral que se evalúan "distintas opciones de redacción". Es que, si la decisión es realizar comicios conjuntos a los nacionales, la normativa actual establece que las primarias sean el 8 de agosto y las generales el 24 de octubre. Pero eso está en discusión; tanto las fechas puesto que se evalúa un diferimiento del calendario de al menos un mes, como la realización misma de las PASO. Ambas instancias obligarían a que los cambios sean ratificados por ley del Congreso.

**Opciones**

Frente al escenario nacional descripto, Sukerman explicaba que una de las formas que se estudiaban para salvar la situación en la provincia era plantear un decreto "genérico", que garantice la simultaneidad, pero sin especificar fechas. Otra alternativa era convocar explícitamente según los días ratificados por la Cámara Electoral Nacional: 8 de agosto y 24 de octubre. "En ese caso – explicó el ministro-, si el gobierno central materializa el diferimiento, la provincia prorrogaría las fechas planteadas en su convocatoria, tal como ya sucedió en 2009. Pero más allá de las formas, lo que quedará expresado en el decreto de convocatoria es la decisión política de realizar elecciones conjuntas con las nacionales", enfatizó.

Respecto de las primarias, cabe recordar que todos los partidos en Santa Fe rechazaron la posibilidad suspenderlas. La situación es similar a nivel nacional, pero si en dicha instancia se resolviese no realizarlas, Sukerman explicó que ésa sería una discusión que debería dar la Legislatura. "Si las primarias nacionales se suspendieran, la Legislatura tendría que resolver qué hace con las PASO provinciales, porque ésa es una ley provincial. No es algo que podamos resolver nosotros (por decreto)", planteó.

Finalmente, el gobierno provincial optó por explicitar las fechas de las elecciones -8 de agosto y 24 de octubre- y adherir eventualmente a un posterior diferimiento, según lo disponga el gobierno nacional.

**Plazos y paridad**

Según pudo saber El Litoral y en función de las fechas tentativas que se plantean para las elecciones, el calendario en la provincia estipularía para el próximo 30 de mayo la fecha límite para la oficialización de alianzas frente al Tribunal Electoral Provincial. En tanto, el próximo 4 de junio vencería el plazo para la presentación de listas de candidatos ante las autoridades partidarias.

Sukerman había adelantado a El Litoral que también en el transcurso de la semana, el Poder Ejecutivo emitiría el decreto reglamentario de la Ley de Paridad, sancionada el año pasado por la Legislatura. De esta manera, las listas de candidatos a concejales en todas las ciudades deberán confeccionarse respetando el criterio de "alternancia de sexo por dueto". En cuanto a las comisiones comunales, la propia ley de Paridad establece que regirá a partir de los comicios de 2023.

**De Pedro a Diputados para negociar postergación**

Mientras, el ministro del Interior, Eduardo "Wado" de Pedro, asistió este miércoles a la Cámara de Diputados para intentar cerrar un acuerdo amplio con la oposición en torno al calendario electoral, con una eventual postergación de las PASO.

"Lo tiempos nos corren. Estamos en abril y hay que avanzar. Por eso la visita a Diputados", explicaron a NA fuentes de la cartera.

La idea del Gobierno era esperar a que Juntos por el Cambio dirima sus diferencias sobre el tema y anuncie públicamente su posición, hecho que se esperaba ocurriera este martes tras el cónclave de la plana mayor de la alianza opositora.

La intención de De Pedro era sentarse a la mesa para negociar con la oposición sobre una base concreta.

Además, durante la reunión, se abordaron otros temas como el proyecto de reforma del Ministerio Público Fiscal y la sanción de una ley marco para un acuerdo de facilidades extendidas con el FMI.

El presidente de la Cámara de Diputados, Sergio Massa, sorprendió el fin de semana al marcar diferencias con la línea predominante en el oficialismo a favor de postergar las PASO, al proponer la suspensión de las primarias obligatorias o hacerlas coincidir con la fecha de elecciones generales.

En realidad, su postura es coherente con el rol que había asumido meses atrás al oficiar de interlocutor y nexo con el grupo de gobernadores que presionó en bloque para lograr la suspensión de las PASO.

Massa los recibió y se comprometió a mediar con el Gobierno para que la propuesta sea considerada.

El presidente Alberto Fernández veía con agrado esa iniciativa, al visualizar que la segunda ola podía alcanzar su máxima altura a comienzos del segundo semestre.

Sin embargo, el plan se deshilachó rápidamente porque la idea no ganó volumen en la opinión pública ni unificó al oficialismo, al tiempo que Juntos por el Cambio rechazó una modificación de "las reglas de juego" en medio del año electoral.

En esas circunstancias, Fernández le soltó la mano a los gobernadores y Massa tomó prudencial distancia del tema.

La llegada de la "segunda ola" reinsertó el tema en la agenda pública a raíz de la preocupación por la escalada de contagios, y en ese contexto surgió la propuesta de Máximo Kirchner para correr el calendario electoral, con la expectativa de que hacía septiembre los avances en el operativo de vacunación apacigüen los efectos de la pandemia. A excepción de la presidenta del PRO, Patricia Bullrich, quien expresó reparos al plan para "cambiar las reglas en un año electoral", en general no hubo un rechazo en el arco opositor a la idea de postergar las PASO.

De hecho, Carrió se adelantó a las deliberaciones de Juntos por el Cambio y se mostró a favor del plan oficial, aduciendo que el corrimiento de la fecha de elecciones podría convenir a Juntos, ya que en plena pandemia muchos adultos mayores - proclives al voto opositor- desistirían de concurrir a las urnas por temor al contagio.

Lo que sí generó mucho ruido fue la foto del encuentro en la Casa Rosada de referentes del Gobierno y del oficialismo con el intendente de Vicente López, Jorge Macri, y con el jefe de bloque PRO en Diputados, Cristian Ritondo.

Estos dos dirigentes del ala moderada del PRO recibieron pases de factura de parte los jefes partidarios de Juntos por el Cambio, por prestarse al juego de "manipulación" y "aprovechamiento político" del Gobierno, al que acusan de haberles tendido una emboscada al instalar la idea de que ya había un "pacto" consumado sobre el corrimiento de la fecha de elecciones.