---
category: Agenda Ciudadana
date: 2021-10-07T06:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/parodocente.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los docentes de Amsafé rechazaron la propuesta del gobierno y paran el miércoles
  13 de octubre
title: Los docentes de Amsafé rechazaron la propuesta del gobierno y paran el miércoles
  13 de octubre
entradilla: Los maestros no aceptaron la oferta de un 17% en tres tramos

---
Los docentes nucleados en Amsafé no aceptaron la propuesta de actualización salarial del gobierno. Esto implica el inicio de un plan de lucha que comenzará con un paro por 24 horas el próximo miércoles 13 de octubre.

La propuesta del gobierno era de un 17% de aumento con relación a los valores de febrero 2021, a abonar en tres tramos: un incremento del 10% a partir de diciembre 2021, un 5% en diciembre y a partir de enero del 2022 una última suba del 2%.