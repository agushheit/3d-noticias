---
category: Agenda Ciudadana
date: 2021-11-08T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La combinación de vacunas triplicó anticuerpos en mayores
title: La combinación de vacunas triplicó anticuerpos en mayores
entradilla: 'Así lo determinó un estudio realizado por la provincia. Se trata de quienes
  se vacunaron con Sputnik V y recibieron como segunda dosis Moderna.

'

---
Los mayores de 60 que combinaron vacunas contra el coronavirus en Santa Fe llegaron hasta triplicar sus anticuerpos. Así lo determinó un estudio realizado por el Ministerio de Salud y en conjunto con el Centro de Tecnología en Salud Pública (CTSP) de la Facultad de Ciencias Bioquímicas y Farmacéuticas (FBIOyF) de la UNR, y el hospital Provincial del Centenario

La investigación estuvo apuntada a analizar el impacto que tuvó en la provincia de Santa Fe la combinación de esquemas de vacunas contra el Coronavirus.

Ana Laura Cavatorta, investigadora responsable del estudio, explicó que desde agosto y hasta octubre de 2021 en el vacunatorio de la Ex Rural de Rosario, 303 voluntarios que completaron su esquema de inmunización heterólogo– recibieron como primera dosis Sputnik V y como segunda Moderna, desarrolladas con tecnologías diferentes–, colaboraron participando de la pesquisa que brindó “resultados muy alentadores para planificar estrategias, basadas en datos inmunogénicos locales”.

“De la totalidad de los voluntarios, el 65 por ciento fueron mujeres y el 35 varones, entre los cuales el 67 por ciento era mayor a 60 años; y la edad fue un aspecto clave”, explicó Cavatorta, también investigadora del CONICET, docente de Virología, responsable científica del CTSP de la FBIOyF.

“Observamos que los mayores de 60 que habían recibido la primera dosis de Sputnik V (desarrollada con tecnología de adenovirus) y completaron el esquema con una realizada mediante ARN mensajero (ARNm) como Moderna, la respuesta fue mayor que en esquemas homólogos, incluso triplicando el valor de los anticuerpos”, destacó la científica, quien ademas es responsable del IBR-CONICET-UNR del hospital Provincial.

“Este cambio de tecnologías (vacunación heterólogo) a nivel del sistema inmune de las personas vuelve al esquema más inmunogénico. Dos estímulos con dos vacunas diferentes son reconocidas como muy extrañas para el organismo, y la respuesta es mucho más eficaz, algo que comprobamos a nivel local, y que se corresponde con estudios a nivel mundial”, profundizó Cavatorta.

En general, los mayores de 60 años comienzan a transitar lo que se denomina “inmunosenescencia”. “Se produce en ellos un déficit fisiológico de la respuesta inmune y se convierte de ese modo en grupo de riesgo para determinadas enfermedades, incluyendo al Covid-19”, explicó Ana Laura Cavatorta.

En otras palabras, la inmunosenescencia son cambios que se producen en el sistema inmunitario a causa del envejecimiento y que afectan la inmunidad innata y adquirida en la comunidad.

Y esa respuesta más robusta en el desarrollo de anticuerpos con esquemas heterólogos, diferenció Cavatorta, fue menor en los más jóvenes que recibieron.

“En los adultos mayores de 60 la primera dosis de Sputnik ya había generado un impacto importante, pero medio, en cuanto al título o concentración de anticuerpos. Sin embargo, luego de que se les colocó una segunda dosis de Moderna se triplicó el valor de estos anticuerpos. En los menores de 60 no hubo cambios importantes. La respuesta fue la esperada pero no como en el grupo anterior”.

Ana Laura Cavatorta destacó que “combinar este tipo de tecnologías (adenovirus con ARNm) es muy eficiente, incluso más que si se utilizan en ambas inoculaciones dosis de la misma tecnología (de una misma vacuna)”.

Otra conclusión es que cuando se aplica la segunda dosis de una misma vacuna opera como un ‘refuerzo’ de la anterior. “Lógicamente, al aplicar una vacuna distinta, y no ya un refuerzo de lo mismo, el sistema inmune vuelve a ‘verlo’ como extraño; y se potencia mucho más la respuesta, porque es como si se generara desde cero”, ilustró la responsable de la investigación.

**Efectos sobre los que tuvieron Covid**

“No hubo cambios significativos. Quienes tuvieron la enfermedad, no importa qué tipo de vacuna reciban. Ya con una primera dosis tienen niveles altos de anticuerpos, generando lo que llamamos ‘inmunidad híbrida’. A la inmunidad natural que les provocó el SARS-CoV-2 se les asoció la artificial proporcionada por la Sputnik V”, explicó Cavatorta.

“Es decir –concluyó la investigadora– que se generó en ellos una memoria inmunológica natural ya muy buena. Entonces no encontramos diferencias entre aplicar esquemas homólogos de heterólogos en personas que tuvieron Covid-19. Sí observamos la importancia de combinar vacunas, fundamentalmente en adultos, como ya dijimos”, amplió Cavatorta.

**Dos vacunas, una misma eficacia**

Finalmente, la investigadora explicó que la Sputnik V es una vacuna que se define como de tecnología a “vectores virales”, ya que utiliza como vector o transportador de información específica a otro virus, en este caso un adenovirus de tipo recombinante (rAd).

“Este porta la secuencia de ADN, es decir, la información genética para que en las células del individuo vacunado se produzca la proteína Spike (o proteína S), característica de los coronavirus y específica de SARS-CoV-2. La primera dosis de esta vacuna utiliza como vector viral al adenovirus humano 26 (Ad26)”, precisó.

“Por otro lado, la vacuna Moderna (como también la Pfizer) pertenece a otra tecnología que es la de ARN mensajero, la cual de forma directa, sin utilizar vectores de transporte, contiene una secuencia con la información genética que permite que en las células del individuo vacunado se produzca la proteína Spike (o proteína S)”, diferenció Ana Laura Cavatorta.

Y concluyó: “Los dos tipos de vacunas han demostrado una alta seguridad y eficacia. A partir de distintas tecnologías ambas enseñan a nuestras células a producir una proteína, o incluso solo una porción de una proteína del virus SARS-CoV-2 , que desencadena una respuesta inmunitaria dentro de nuestro organismo. El beneficio es que las personas que se vacunan tienen protección sin correr el riesgo de sufrir consecuencias graves en el caso de enfermarse de COVID-19”.

**Mecanismo de estudio**

A los voluntarios a completar esquemas con vacunas combinadas y a participar del estudio, se les realizó una primera extracción de sangre justo en el momento anterior a recibir la segunda dosis en la Ex Rural de Rosario, a los fines de determinar la cantidad de anticuerpos que se habían generado tras la primera inoculación con Sputnik V.

Pasado un período mínimo de 14 a 28 días fueron citados para realizarles una segunda extracción, permitiendo evaluar el impacto de la segunda inoculación con una dosis de Moderna.

“A diferencia de muchas investigaciones, ellos recibieron un informe completo por Whatsapp y correo electrónico de los resultados. Fue un trabajo muy arduo, comprometido y gratificante, en donde trabajamos muy bien con el Ministerio de Salud provincial”, destacó Cavatorta.

**El equipo de investigación**

El estudio desarrollado en el vacunatorio de la Ex Rural de Rosario que depende del Ministerio de Salud de la provincia, además de por Ana Laura Cavatorta estuvo integrado por:

Bioq. Eduardo Codino (Jefe de Servicios Asistenciales Laboratorio CTSP- FBIOyF -UNR, Hospital Provincial del Centenario).

Bioq. Miguel Taborda (Jefe de Servicio Área Virología- FBIOyF -UNR, Hospital Provincial del Centenario).

Bioq. Julián Acosta (Becario doctoral- FBIOyF-UNR).

Lic. en Biotecnología Lucía Moriena (Laboratorio CTSP-FBIOyF -UNR, Hospital Provincial del Centenario).

Luciano Civerchia (Estudiante de Bioquímica, Pasante Área Virología, FBIOyF -UNR).

Bioq. Laura Vietti (Laboratorio CTSP- FBIOyF -UNR, Hospital Provincial del Centenario).

Bioq. Laura Valenti (Laboratorio CTSP- FBIOyF -UNR, Hospital Provincial del Centenario).

Lic. en Enfermería Marisol Ferreyra (S.I.E.B.E, Sala 9, Hospital Provincial del Centenario).

Lic. en Cs. Políticas Erica Chaloupka (Laboratorio CTSP-FBIOyF-UNR, Hospital Provincial del Centenario).

Bioq. Jorgelina Crescimbeni (Laboratorio CTSP-FBIOyF-UNR, Hospital Provincial del Centenario).

Bioq. Eduardo Fernández (Laboratorio CTSP-FBIOyF-UNR, Hospital Provincial del Centenario).

Y por Román Luis Mocciaro (Responsable de Logística-Laboratorio CTSP- FBIOyF-UNR, Hospital Provincial del Centenario).