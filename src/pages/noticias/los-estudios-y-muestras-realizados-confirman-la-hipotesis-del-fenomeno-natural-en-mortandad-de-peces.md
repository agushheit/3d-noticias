---
category: Estado Real
date: 2020-12-24T11:07:31Z
thumbnail: https://assets.3dnoticias.com.ar/2412peces-muertos.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Los estudios y muestras realizados confirman la hipótesis del fenómeno natural
  en mortandad de peces
title: Los estudios y muestras realizados confirman la hipótesis del fenómeno natural
  en mortandad de peces
entradilla: Lo demuestran las inspecciones y los análisis efectuados por el Gobierno
  de Santa Fe sobre el río Salado.

---
Ante los recientes sucesos de mortandad de peces, principalmente sobre el río Salado, el Gobierno de la provincia coordinó una serie de estudios sobre peces y agua que arrojaron como resultado la confirmación de que se trató de un proceso natural, como consecuencia de la bajante histórica que atraviesa el río Paraná y sus afluentes. 

Los análisis químicos fueron realizados por la Facultad de Ingeniería Química de la Universidad Nacional del Litoral.

En ese sentido, la Dirección General de Manejo Sustentable de los Recursos Pesqueros del Ministerio de Ambiente y Cambio Climático, informó que «este proceso de estrés de los cursos de agua de la provincia se viene intensificando como consecuencia de que la temperatura media diaria paulatinamente se va incrementando, convirtiéndose así en uno de los factores de mayor relevancia para favorecer y aumentar el metabolismo oxidativo de los cuerpos de agua». Y además «se ha intensificado en los últimos cuatro meses producto de la instalación del fenómeno climatológico conocido como La Niña».

Como conclusión, de los muestreos se evidencia que «los valores de los análisis de agua se encuentran dentro de lo establecido por la Ley Nº 24.051 de Residuos Peligrosos y su decreto reglamentario Nº 831/93, así como de los Niveles Guía Nacionales de Calidad de Aguas Ambiente y Calidad de agua ambiente para protección de la biota acuática, sugeridos por la Subsecretaría de Recursos Hídricos de la Nación».

«Se continúa sosteniendo como la hipótesis más plausible para explicar la mortandad en análisis un proceso de hipoxia marcada, producto del ingreso al río Salado de agua de lluvia, en fechas próxima al 15 de noviembre, que con el aporte material alóctono incrementó el estrés en el sistema, potenciado luego por la gran cantidad de materia orgánica producto de la descomposición de los peces. Las lluvias posteriores, re-suspendieron los ejemplares ya muertos y en proceso de descomposición, incrementado su desplazamiento aguas abajo», continuó la información aportada por la Dirección mencionada.

Es importante destacar también que, según informan desde la Dirección General de Manejo Sustentable de los Recursos Pesqueros, los niveles de agroquímicos y metales pesados detectados, relacionados principalmente a actividades antrópicas, están dentro de los valores históricos del río Salado y se ajustan a los parámetros nacionales e internacionales usualmente aplicables en la materia.