---
category: La Ciudad
date: 2021-05-15T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/PODA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Primera Terminal Verde municipal para la gestión de restos de poda
title: Primera Terminal Verde municipal para la gestión de restos de poda
entradilla: Está ubicada en Colastiné Norte y trata esos residuos para ser reutilizados.
  Esto evita el traslado de ramas y hojas al relleno sanitario.

---
Hace poco menos de un mes, en calle Eucaliptus de Colastiné Norte, al límite con la defensa oeste de la capital provincial, la Municipalidad instaló la planta procesadora de residuos de poda. Se trata de un terreno en el cual diez empleados municipales y ocho integrantes de una asociación que desarrolla su actividad en Colastiné, completan sus tareas todos los días.

Hasta ese lugar arriban los camiones del municipio que trasladan los residuos de poda de Colastiné. Ese material se separa y se clasifica según su utilidad: chip de leña para espacios públicos de la ciudad y para entregar a los vecinos en los ECO canjes, para construcción de muebles, leña y compostaje.

El secretario de Ambiente municipal, Edgardo Seguro, indicó que la zona de Colastiné concentra más del 50% de la poda que se registra en todo el ejido urbano. Esto obligaba a trasladar hacia el relleno sanitario, cada temporada, más de mil camiones con restos de árboles para su enterramiento. “Esto tiene varias cuestiones ambientales negativas: gastar combustible y producir dióxido de carbono de mil camiones yendo hasta el relleno cada temporada y enterrar materia vegetal en un relleno sanitario ocupando el lugar que tenía que usar el material húmedo”, explicó.

En la flamante planta procesadora, en primera instancia se divide y clasifica la madera por su tipo. Lo que tiene capacidad de ser leña, se trabaja con motosierras para reducir volumen y darle utilidad. En cuanto a la hojarasca, la idea es compostarlo. En tanto, los árboles de calidades superiores se reservan para ser transformados en muebles y herramientas. El resto pasa por la máquina chipeadora que lo convierte en pequeñas piezas de leña para trasladar a los espacios verdes de toda la capital provincial.

De esta forma, se disminuye el volumen con el objetivo de llevar al relleno sanitario lo mínimo indispensable. Seguro informó que esta planta permitirá “ahorrar cerca de 60% de los viajes realizados hacia el relleno, reduciendo drásticamente el efecto cambio climático del transporte”. Al mismo tiempo, dijo que “se dará utilidad a todo el material leñoso que puede ser usado para energía”.

Respecto de la chipeadora, el secretario detalló que tiene la particularidad de convertir las ramas en pequeños pedazos de madera. El aparato puede procesar hasta ramas de 30 centímetros de diámetro y en él se introduce todo el material que no es utilizable como leña. La intención es, en un corto plazo, tratar la hojarasca para producir el compost en la misma planta. Posteriormente, se evaluará la posibilidad de iniciar un emprendimiento relacionado con la carpintería para que los integrantes de la asociación le den utilidad a la madera de mayor valor económico.

La planta trabaja de lunes a sábado, de 7 a 18 horas. A la hora de recibir el material y clasificarlo, el personal forma pilas sectorizadas de manera de mantener el orden en el espacio, pero también de evitar la propagación de fuego en caso de un evento ígneo. No obstante, en el lugar se ubica una estación de bombeo particular para poder actuar en caso de incendio.

De todos modos, Seguro especificó que el material se achica en el día, por lo que muy poco queda allí, para minimizar riesgos. Además, cuenta con guardia permanente las 24 horas.

El espacio de trabajo se completa con un contenedor modificado donde se ubica la oficina de guardia, los vestuarios del personal y el espacio para guardado de herramientas. La energía se obtiene de paneles solares porque no hay suministro eléctrico.

Para completar sus tareas, los trabajadores fueron capacitados, tanto el personal municipal como el que integra la asociación. El Cobem les brindó formación en prevención de incendios, de riesgos, RCP y ofidismo.

**Aparatos eléctricos y electrónicos en desuso**

Por otro lado, la Municipalidad de Santa Fe continúa con la recepción de aparatos eléctricos y electrónicos en desuso (AEES) en distintos puntos de la ciudad. Este sábado 15 de mayo, de 9 a 12 horas, la recolección será en la sede del Distrito Norte, en avenida Gorriti 3900.

Para ser recibidos, es necesario que sean aparatos completos y no piezas sueltas o dañadas. Es decir, elementos eléctricos y electrónicos en desuso que no se utilizan, pero están enteros, por lo que no se consideran residuos.

Desde la Secretaría de Ambiente recuerdan que se aceptan monitores, pantallas y aparatos con pantallas de superficie superior a los 100 cm²; pequeños electrodomésticos como planchas, secadores de cabello y pequeños ventiladores; y aparatos de informática y de telecomunicaciones pequeños como computadoras, mouse, pantalla, teclado, impresoras, copiadoras y teléfonos celulares con dimensiones exteriores que no superen los 50 cm.