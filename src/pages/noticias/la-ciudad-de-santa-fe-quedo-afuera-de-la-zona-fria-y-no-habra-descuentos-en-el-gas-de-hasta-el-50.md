---
category: La Ciudad
date: 2021-08-18T06:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/gas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La ciudad de Santa Fe quedó afuera de la "zona fría" y no habrá descuentos
  en el gas de hasta el 50%
title: La ciudad de Santa Fe quedó afuera de la "zona fría" y no habrá descuentos
  en el gas de hasta el 50%
entradilla: El centro - norte provincial no fue incorporado en la última ampliación
  del régimen especial. El régimen de zona fría es una norma especial que se aplica
  en regiones del país donde la temperatura es muy baja.

---
La modificación de la ley lo que hizo este año fue ampliar el alcance del régimen especial de zona fría, como así también la bonificación recibida, que antes se limitaba a la patagonia.

En dicha ampliación, la ciudad de Santa Fe no fue incorporada; tampoco lo fueron todos los departamentos del centro - norte de la provincia.

Quienes sí se sumaron a los beneficios fueron los departamentos del sur: Belgrano, Caseros, Constitución, General López, Iriondo, Rosario, San Lorenzo, San Martín. Según el Enargas (Ente Nacional Regulador de Gas) se verán beneficiados 537.779 usuarios; representa un 76 por ciento de los usuarios de gas por redes y estiman que impactaría en 1,72 millones de personas.

**Los principales beneficios**

\- Reduce en alrededor del 50% las tarifas a los beneficiarios de la Asignación Universal por Hijo (AUH) y la Asignación por Embarazo, de pensiones no contributivas y jubilados -hasta 4 salarios mínimos-, del monotributo social, usuarios que perciben seguro de desempleo y empleados de casas particulares.

\- Respecto del resto de los usuarios residenciales, la tarifa diferencial implicará una disminución del 30% de la factura de gas correspondiente.

\- El beneficio también podrá ser otorgado a quienes estén exentos en el pago de ABL o tributos locales de igual naturaleza o los que tengan una pensión vitalicia como veteranos de Guerra del Atlántico Sur.

\- Prorroga por 10 años el Fondo Fiduciario de Consumos Residenciales de Gas (artículo 75, Ley 25.565) y aumenta la cantidad de departamentos y localidades de todo el país de temperaturas bajas comprendidos en este beneficio.

A nivel nacional, con la nueva ley, el beneficio de Zona Fría alcanzará ahora a un total de 4 millones de usuarios, el 44% del país y unos 12,8 millones de personas, gracias a la incorporación de más de 60 localidades de once provincias.

Los descuentos serán para consumos registrados a partir del 5 de agosto. De acuerdo a lo establecido por la norma número 27.637, las rebajas serán sobre la factura final.

Para ver el listado completo de localidades beneficiadas en Santa Fe ingresá en [https://www.litoral-gas.com.ar/site/noticias/comunicaciones/ley-zona](https://www.litoral-gas.com.ar/site/noticias/comunicaciones/ley-zona "https://www.litoral-gas.com.ar/site/noticias/comunicaciones/ley-zona")

José María González, gerente de Relaciones Públicas de Litoral Gas explico que "la ley 25.585 de tarifas por zonas frías fue la que se amplió con la 27.637. En aquella norma había clientes subsidiados por un componente que tiene el precio del gas del sistema de transporte que era del 4,6 por ciento. Ese componente aumentó al 5, 6 por ciento. Con ese valor, se subsidian a clientes de la Patagonia, de Malargüe (Mendoza) y de La Puna (Jujuy). Esta última ley amplía a localidades de las provincias de Buenos Aires, Santa Fe y otras como Córdoba. Se aumenta un poco ese cargo en el precio del gas y con ese fondo se subsidia todos los descuentos que hacen a los clientes residenciales. El Estado no pone valores para subsidiar clientes".

Confirmó que también están incluidas en los descuentos “el cien por ciento de las localidades de la provincia de Buenos Aires que están dentro del área de concesión de Litoral Gas. Es decir, el 80 por ciento de los clientes residenciales de la compañía tendrán ese descuento. En algunos casos será del 30 por ciento, pero podrá ser del 50 para aquellos clientes que estén inscriptos en Ansés como beneficiarios de tarifa social”.

Explicó que "esto regirá por diez años. No será sólo por los meses de invierno. Será todos los meses por diez años. Es decir que este beneficio finaliza en diciembre de 2031”.

El ejecutivo de Litoral Gas remarcó además que el trámite de acogimiento de esos beneficios “es automático, no hay que hacer ninguna diligencia". provincia son 72 las localidades que gozan del beneficio. ¿Cuáles son?