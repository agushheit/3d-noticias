---
category: Agenda Ciudadana
date: 2021-05-05T08:09:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/carne.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Carne: el asado y milanesas entre 700 y 800 pesos el kilo'
title: 'Carne: el asado y milanesas entre 700 y 800 pesos el kilo'
entradilla: La carne aumentó entre 60 y 70% en un año en Santa Fe. Los precios en
  las carnicerías de Santa Fe aumentan cada 20 días

---
A pesar de que los precios aumentan constantemente a nuestro alrededor, el valor de la carne sorprende todos los meses. Hoy las carnicerías en Santa Fe están remarcando sus precios con un promedio de entre 700 y 800 pesos el kilo.

El Instituto de Investigación Social, Económica y Política Ciudadana publicó recientemente un relevamiento realizado en los comercios de cercanía del conurbano que indica que en un año las carnes están un 70% más caras. En esta línea, UNO Santa Fe consultó con el dueño de una carnicería local, quien aseguró que “cuando los precios se mueven en Buenos Aires se mueve acá” y que en nuestra ciudad el aumento con respecto al 2020 también estaría entre un 60 y 70 por ciento.

De todas formas, Sebastián, dueño de la Carnicería Los Hermanitos, precisó que el aumento fuerte se dio para las fiestas de diciembre, y que desde ese momento estuvo aumentando entre 10 o 15 pesos cada 20 días. “Entre diciembre y ahora nos aumentó 200 pesos de costo a nosotros”.

En su carnicería los precios actuales son: el kilo de pulpa para milanesa y para bife $820. El kilo de asado (costilla o vacío) $790. Un kilo de milanesa de carne preparada $600; la costeleta brazuelo alrededor de los $700. Un lomo o matambre de carne cuesta $900. La carne picada común $480 y la especial $750.

El pollo y el cerdo están más baratos: la milanesa de pollo $300, el kilo de pulpa de cerdo $550, la costilla y costeleta de cerdo en $500.

El referente del sector explicó que de a kilo no se vende mucho: “Se perdió mucho el dame un kilo de esto, dos kilos de aquello. La manera de comprar es por la plata que tienen en mano”.

Además precisó que en su negocio lo que más sale son los cortes más caros, por ejemplo la pulpa, "porque rinde". “Pagan 800 pesos pero en una familia comen todos y alcanza para dos comidas capaz”, dijo el carnicero.

Contó también: “Esta semana por ahí el aumento no se nota, la gente se desayuna que cambiamos los precios hoy, pero dentro de 15 días empiezan a quejarse”. “Los primeros días que se cobra o se habilita la tarjeta AlimentAR no es tanto el impacto, después del 15 o 20 del mes se empieza a ver que baja el flujo de gente que entra”, dijo Sebastián.

![](https://assets.3dnoticias.com.ar/carne.jpg)

## **El asado, inalcanzable**

“Hace un tiempo cambió la forma de comprar asado. Se terminó el «compro un asadito y comemos en casa». Hoy un kilo de molleja, que siempre fue un plato un poco más caro que los otros, sale 1.500 pesos. Es imposible. Ya arrancás el asado con 500 pesos por persona sin haber probado la carne”, dijo el experto.

El asado se volvió un lujo para las familias de clase media, y comer carne todos los días en una casa con cuatro personas, también. “Una persona que sea jefa de familia y tenga que cocinar un asadito para cuatro o cinco en su casa lo va a hacer una vez por mes, porque va a gastar entre 3.000 y 5.000 pesos. Se tiene que calcular entre 800 o 900 por persona y es imposible hacerlo todos los domingos como se hacía no hace mucho tiempo. Es imposible que una persona invite”, dijo el carnicero.

Y agregó: “Llevar un kilo de carne todos los días son más de 20.000 pesos por mes. No es algo que ganen las familias normales. Hay mucha gente que se queda afuera”.

## **¿Por qué aumenta tanto?**

El vendedor de la Carnicería Los Hermanitos aventuró las razones por las cuales la carne aumentó tanto estos últimos meses, aunque no tiene certezas. “Yo pregunto todo el tiempo, por qué si está todo más o menos quieto, la carne no tendría por qué modificarse, pero no sé cómo viene el tema en el campo. Para diciembre supuestamente no había y nos íbamos a quedar sin, pero haber hubo siempre. Hoy si no me equivoco es por el tema exportación, porque les conviene más sacarlo que dejarlo acá. No te lo venden porque les conviene más venderlo afuera”, relató.

Y opinó: “Debería haber un arreglo para que todas las carnicerías podamos trabajar con un precio más o menos acorde. 600 o 700 pesos (el kilo) no estaba mal para lo que hoy valen las cosas, pero ya 800, 850 ya es un montón de plata”.

“Yo creía que en diciembre ya habían terminado los aumentos, pero parece que va a seguir. Cuando aumenta la carne la gente se asusta, se pelea. Y al no parar nunca, es muy difícil. Está llegando a niveles muy difíciles para la mayoría”, concluyó.

## **El acuerdo de precios con frigoríficos**

A fines de enero el gobierno de Alberto Fernández anunció el Acuerdo de Precios para la carne, una medida consensuada junto al Consorcio de Exportadores de Carnes Argentina (ABC) que busca mantener los precios de ocho cortes de carne vacuna con precios rebajados respecto de los valores registrados en diciembre del año pasado.

El referente de las carnicerías santafesinos explicó que “son cortes populares a un precio que está muy bien” y, si bien no le toca trabajarlos porque es para cadenas de supermercado grandes “no está en desacuerdo”. “Pasa que está mal interpretado, no les llega a todos. No se sintió el impacto”, opinó.

Las piezas podían encontrarse durante las tres primeras semanas del mes, los miércoles y fines de semana, en 1.600 supermercados de todo el país, con un volumen de oferta de 6.000 toneladas por mes. Las carnicerías no resultaron incluidas dado que trabajan con medias reses, mientras que los frigoríficos que participan comercializan cortes, habitualmente se destinan a supermercados.

“Cuando llego a la góndola llego con un montón de problemas porque la gente se encontraba con mucha grasa, con carne negra. Creo que hoy lo acomodaron pero me parece que en cantidad no alcanza, porque aparte acá tenemos cuatro o cinco lugares que lo venden, y una persona que tenga que hacer dos cuadras de cola va a ser imposible”, dijo el carnicero.

El viernes pasado venció el acuerdo, que ya había sido prorrogado dos veces, y es inminente el anuncio de los nuevos lineamientos.

“A nosotros no nos afecta mucho porque la clientela del negocio no se va a ir hasta allá. Lo elige la gente que puede pagar eso y, además, ahí no podes pedir que te lo corten o te lo desgrasen, te llevás la bolsa como te la dan armada. Pero ayuda un montón al que no puede pagar 800 pesos el kilo. Estoy seguro de que la calidad de carne no es la misma, pero hay gente que le sirve y la economía de cada uno la maneja cada uno”, concluyó.