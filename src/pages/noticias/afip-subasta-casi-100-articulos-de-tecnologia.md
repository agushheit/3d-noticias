---
category: Agenda Ciudadana
date: 2021-07-13T07:44:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/jostick.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: AFIP subasta casi 100 artículos de tecnología
title: AFIP subasta casi 100 artículos de tecnología
entradilla: Los precios de los productos son mucho más bajos que los del mercado.
  Mirá la comparación y como participar del remate online.

---
La Administración Federal de Ingresos Públicos (AFIP) anunció este lunes una lista de casi 100 productos de tecnología que saldrán a remate el próximo 29 de julio a través del sitio del Banco Ciudad. Los precios de base suelen ser bajos en este tipo de subastas, pero por si acaso se pueden comparar con la oferta online.

* 1 Joystick (accesorio para PS4); valor base: $408,50, se ofrece a unos $ 14.000 nuevo.
* 2 Dispositivos de TV marca Chromecast, sin cargador; valor base: $987,00, que en una plataforma online aparece a $ 6.000 por unidad.
* 1 Cámara de retroceso para auto sin marca; valor base: $236,71, cuando online se consigue a $ 4.600 o más.
* 1 Cámara fotográfica Cannon, modelo Power Shot SX520, sin cargador; valor base: $1.570,80, pero se ofrece a $ 43.000 en la web.
* 1 Celular iPhone 7; sin accesorios: valor base: $11.556,60, cuando no baja de los $ 100.000 online.
* 1 Equipo de música portátil Mega Star Modelo HY K5BT, sin cargador; valor base: $517,65, cuando se ofrece a $ 15.000 en otras partes
* 1 Parlante portátil JBL 3, sin cargador; valor base: $411,25, que se puede comprar a $ 6.000 en otros sitios.
* 3 Rack de montaje Viptela - Vedge 1000 Router; valor base: $34.756,43, cuando el precio individual ronda los US$ 2.500 ($ 240.240, aprox)
* 28 Autoestéreos de varias marcas; valor base: $27.833,27, que son ítems que, dependiendo el gusto del consumidor, empiezan en $ 4.000 por unidad.
* 6 Estéreos para auto marca Napoli; valor base: $6.909,00, cuando por unidad su precio arranca en $ 4.000 online.
* 1 Autoestéreo marca Pioneer con pantalla táctil; valor base: $1.142,40, cuando uno de esos aparatos está a $ 33.000 en adelante.
* 50 Arcos para violines, sin marca; valor base: $2.990,75, el precio dice algo de la calidad del producto ya que un arco para violín puede costar entre $ 1.500 y $ 8.000 dependiendo de la marca y características.