---
category: Estado Real
date: 2021-05-08T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe ya vacunó al 50% de la población objetivo contra el Covid 19
title: Santa Fe ya vacunó al 50% de la población objetivo contra el Covid 19
entradilla: La mitad de ese porcentaje se inoculó durante el último mes. “Llevamos
  aplicadas 700 mil dosis y estamos agradecidos al personal sanitario porque realiza
  un gran esfuerzo”, sostuvo la ministra de Salud Sonia Martorano.

---
El Gobierno de la provincia, a través del Ministerio de Salud confirmó que se vacunó al 50% de la población objetivo planteado por las autoridades estratégicamente. Se estima un promedio de 20 mil dosis diarias aplicadas en los diferentes centros dispuestos en 323 localidades donde se ubican 336 vacunatorios.

La ministra de Salud, Sonia Martorano, confirmó que avanza rápidamente el operativo “histórico de vacunación alcanzando a 600 mil personas con 700 mil dosis aplicadas ya que, en muchos casos como lo es el personal de salud o docentes, ya recibieron las 2 dosis. Analizamos cómo viene impactando la pandemia en los diferentes rangos etarios y de vulnerabilidad”, explicó la funcionaria provincial.

**Población objetivo**

Cabe destacar que Santa Fe Vacuna es el plan de vacunación estratégico nacional, gratuito y voluntario que cuenta con distintas etapas definidas en base a criterios epidemiológicos específicos, como la exposición al virus o el riesgo a enfermedad grave a causa del mismo.

Para cumplir con la población objetivo se establecieron en forma paralela operativos especiales con recorridos por diferentes parajes que se encuentran más alejados de los vacunatorios, “con el objetivo de llegar a cada adulto mayor que reside en zonas rurales o de difícil acceso, sobre todo en el norte provincial”, expresó Martorano.

En el mismo sentido, se trasladaron equipos de profesionales a geriátricos, centros de rehabilitación y domicilios particulares de pacientes electrodependientes que fueron inoculados en un 100%, mientras continúa la vacunación a personas trasplantadas y en centros de día.

“Vamos a llegar a los mayores de 60 años que es la población más sensible. También estamos con las personas menores a los 59 años con comorbilidades. Se avanzó con los trasplantados, personas con fibrosis quística, y vamos a continuar con otros grupos”, detalló la ministra.

**Plazos y nuevas dosis**

En este sentido, Martorano actualizó la situación del cronograma y confirmó que ya se están colocando las segundas dosis para docentes. “La semana que viene continuaremos con Sinopharm en segunda dosis para docentes”, puntualizó.

"Esto se logró con el gran esfuerzo, la pasión y le trabajo incondicional del personal de salud, a ellos les transmitimos este agradecimiento", concluyó la ministra de Salud.

**Cantidad de vacunas por población**

El operativo de vacunación se inició el 29 de diciembre del 2021 con el personal de salud de los sectores hiper críticos, y luego fue avanzando sobre la población objetivo. Así es como se inocularon 120.988 personas con comorbilidades menores de 60 años, 120.618 personas de 60 a 69 años, 131.975 de 70 a 79 años, 57.508 de 80 a 89 años, 9.533 mayores de 100 años, 73.043 docentes y 4.976 personal de seguridad.