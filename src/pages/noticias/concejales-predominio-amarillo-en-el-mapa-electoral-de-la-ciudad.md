---
category: La Ciudad
date: 2021-09-14T06:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMARILLO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Concejales: predominio amarillo en el mapa electoral de la ciudad'
title: 'Concejales: predominio amarillo en el mapa electoral de la ciudad'
entradilla: Dos frentes políticos bien marcados se repartieron todas las seccionales
  de Santa Fe, con mucha variación del voto distrito a distrito. Juntos por el Cambio
  se impuso en 11 de las 18 seccionales.

---
Conforme al escrutinio de votos, de las elecciones santafesinas, que definieron cuáles serán los candidatos a concejales, que participaran en las generales de noviembre, el mapa electoral de la ciudad se definió con dos principales fuerzas políticas. Éstas son Juntos por el Cambio y el Frente de Todos, que entre todos sus precandidatos obtuvieron la mayoría de votos en las 18 seccionales electorales de la ciudad.

La elección para definir los candidatos a ocupar una banca en el Concejo tuvo en el Frente Amplio Progresista la candidata más votada en la ciudad, con los más de 17.000 votos que obtuvo Ana Laura Mondino, aunque como frente no ganó en ninguna de las 18 seccionales. Esto fue condicionado por tener solamente una representante del espacio socialista, aunque esto se agrava al ser la candidata del oficialismo y del intendente Emilio Jatón.

**Teñido de amarillo**

La muy buena elección realizada en conjunto entre los precandidatos a concejales de Juntos por el Cambio estuvo reflejada bajo el triunfo en más del 60% de la ciudad. La fuerza obtuvo un total de 47.834 votos, casi triplicando lo obtenido por el socialismo oficialista.

En la Seccional primera del centro de la ciudad obtuvo su mayor triunfo, con 5.779 votos e imponiéndose por casi 4 mil sufragios al Frente de Todos. Además, se hizo fuerte en distritos clave como las seccionales séptima y octava, marcando un triunfo electoral en zonas que concentran un gran número de votantes y que no siempre le han jugado a su favor.

**Frente de Todos, segunda fuerza**

En la distribución geográfica del voto en Santa Fe, se puede identificar que el bastión peronista en la ciudad fueron los barrios del noroeste de la ciudad, sumado a algunos distritos del sur y de la zona costera. En la populosa seccional décima se dio el mayor triunfo del Frente de Todos, obteniendo 8.059 votos y triunfando por casi 3 mil sufragios por sobre Juntos por el Cambio. En total, la fuerza obtuvo 39.465 votos.

En relación al Frente de Todos, la particularidad en estas PASO 2021 para concejales se da en que en ninguna de las seccionales en las que se impusieron como fuerza política obtuvo el triunfo su candidato más votado. El candidato que mas electores acaparó en todos estos distritos fue el "outsider" Juan José Piedrabuena, imponiéndose en cinco de las siete seccionales en las que el peronismo fue el espacio político mas elegido.

**Resultados: seccional por seccional**

Juntos por el cambio triunfó con sus precandidatos a concejales en las Seccionales primera, segunda, tercera, cuarta, quinta, sexta, séptima, octava, novena, undécima y en el barrio costero de Colastiné. Por su parte, el Frente de Todos se impuso en la populosa Seccional décima, en las subcomisarías primera, segunda, tercera y séptima, triunfando también en los distritos de Alto Verde y La Guardia.