---
category: Agenda Ciudadana
date: 2021-06-11T07:58:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/afip.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Los cuatro cambios claves de la marcha atrás con el pago de la deuda retroactiva
  del monotributo
title: Los cuatro cambios claves de la marcha atrás con el pago de la deuda retroactiva
  del monotributo
entradilla: Luego de las críticas de los contribuyentes, el gobierno envió un proyecto
  de ley que alivia la carga fiscal

---
El Gobierno resolvió condonar el pago retroactivo de enero a julio que debían hacer los monotributistas por las diferencias ocasionadas por el incremento del componente impositivo y previsional que tanto malestar causó entre los pequeños contribuyentes. Si bien aún faltan los detalles de cómo se instrumentará el alivio fiscal, estos son los principales cuatro puntos.

1) El punto más importante es que el valor de las cuotas de enero a junio de 2021 será el vigente a diciembre de 2020. Por eso, la actualización de la cuota regirá a partir de julio,

2) El proyecto incluye un esquema progresivo de actualización de escalas que duplica el incremento en las categorías más bajas al 77%, cuando el incremento previsto para este año es del 35%.

3) Además, genera un esquema para que los pequeños contribuyentes puedan mantenerse en el régimen simplificado a pesar de haberse excedido en al facturación - aunque con topes: no podrán aprovecharlo quienes superen 1,5 veces el monto de facturación anual de la categoría más alta ($ 5,5 millones anuales) o tengan bienes por $ 6,5 millones o más.

4) También crea un régimen de regularización de deudas en hasta 60 cuotas y tasa de interés de no más del 1,5% mensual.

El problema ocurrió porque la norma sancionada originalmente contempla los mecanismos dispuestos en la Ley 27.618, que obliga a la Afip a fijar los ajustes a partir del 1 de enero pasado más allá de que los contribuyentes hayan cumplido con el pago de sus obligaciones.

Fue así que el organismo monetario categorizó a más de 4 millones de monotributistas de acuerdo con la escala que correspondía encontrarse encuadrado a partir de febrero de este año, en función de lo establecido por ley y en igual proporción que la evolución del haber mínimo jubilatorio. Incluso se definió que la diferencia se pueda pagar hasta en 20 cuotas -siempre y cuando cada cuota supere los $500- con intereses de 2,9% mensual. La medida disparó el reclamo de contribuyentes alcanzados por la aplicación del retroactivo.