---
category: Agenda Ciudadana
date: 2021-10-04T06:15:09-03:00
thumbnail: https://d7kqr73uznmf6.cloudfront.net/2020-10-07.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Familiares de víctimas del ARA San Juan piden que no dejen salir del país
  a Gustavo Arribas
title: Familiares de víctimas del ARA San Juan piden que no dejen salir del país a
  Gustavo Arribas
entradilla: La representación de las familias de los tripulantes del submarino hundido
  en el Mar Argentino en 2017 argumentó la existencia de "peligro de fuga y contactos
  que entorpecerían el proceso..."

---
La representación de los familiares de las víctimas del submarino ARA San Juan, hundido en el Mar Argentino en 2017, pidió a la justicia que deniegue autorización para salir nuevamente del país al exjefe de la Agencia Federal de Inteligencia (AFI) macrista Gustavo Arribas, procesado en la causa por espionaje a los parientes de los tripulantes de la nave.  
La abogada Valeria Carreras argumentó la existencia de "peligro de fuga y contactos que entorpecerían no solo el proceso sino una eventual extradición" de Arribas, quien pidió permiso para viajar a Brasil por un período de 60 días.  
La presentación fue realizada por Carreras ante el juez federal subrogante de Dolores Martín Bava, quien procesó a Arribas y a la exnúmero dos de la AFI Silvia Majdalani en la misma causa.  
Además, el magistrado citó a indagatoria a Mauricio Macri para el próximo jueves como imputado por supuestamente haber ordenado y posibilitado la realización de esas tareas de inteligencia ilegal sistemáticas y le prohibió salir del país.  
Carreras solicitó que "no se conceda la autorización" a Arribas, quien pidió permiso para viajar a la ciudad brasileña de San Pablo, informó este domingo la abogada.  
"De la presentación no surge motivo válido y suficiente que amerite una nuevas salida del país, teniendo en cuenta que sería el tercer viaje (7-7-2021, 3-9-21 al 24-9-2021) y ahora solicita autorización de salida del país del 7-10-2021 al 8-12-2021, incluso haciendo mención a pasajes de otro viaje con destino a Uruguay", dijo la abogada en su escrito.  
  
"No consideramos, ni se han volcado en las presentaciones en conteste, que existan motivos impostergables para realizar un 'nuevo viaje'. No puede concederse al procesado un lapso de autorización tan extenso como el pretendido, son 60 días Sr Juez. No puede habilitársele, la autorización por dicho extenso lapso, mucho menos si el destino es Brasil", añadió.  
Y abundó: "No puede dejar de sopesarse en la decisión que el procesado Gustavo Héctor Arribas tiene esposa e hija de nacionalidad brasileña, con lo cual accedería a visas de todo tipo sin mayores exigencias, e incluso podría naturalizarse en el país donde pretende viajar por 60 días".  
"No puede en esta instancia de la causa otorgar esa extensísima autorización para que el reo especule con las resultas procesales y dependiendo del resultado, proyecte su 'fuga de la justicia desde el extranjero'", advirtió Carreras.  
También alertó acerca de que "no puede soslayarse que el procesado" Arribas "no solo tiene su anclaje familiar, sino sus inmuebles, su patrimonio, paga impuestos y especialmente previo al 2015 tenía sus vínculos profesionales, laborales, negocios, etc. Pero luego de asumir a cargo de la AFI, estrechó vínculos internacionales justamente con las agencias de inteligencia de Brasil, la Policía de Brasil quienes en cierto modo serían los encargados de 'buscarlo' si no retorna en fecha".  
"Pero algo más grave se vislumbra en caso de otorgar el permiso de salida a Brasil: podría llegar a entorpecer el proceso, a través del manejo de la información con la que ya cuenta y de información a la que podría acceder. Estamos convencidos que el procesado Arribas intentará en algún momento eludir a la justicia, y podría perpetrar una fuga, como Fabián Pepín Rodríguez Simón", añadió.  
Carreras alertó además sobre presuntas anomalías en el escrito de la presentación que Arribas hizo ante la justicia. ante lo cual podrían "plantearse nulidades, desconocimiento del encartado, desconocimiento de firma, etc.".  
Al respecto dijo que "deberá intimarse al presentante a ratificar o rectificar, tanto las firmas que se le atribuyen como así también el contenido del escrito ensamblado, ello a fin de evitar futuras nulidades".  
Además advirtió que en el texto del pedido de autorización para ir a Brasil "se incluye un párrafo que reza 'Finalmente, se destaca que las reservas correspondientes al desplazamiento a la República Oriental del Uruguay serán acompañadas antes del viaje', y desconoce esta parte si se trató de un error, si se va a entrar y salir de Brasil, hacia Uruguay, si además de los 60 días del permiso en conteste con destino Brasil hay otro pedido de permiso adicional".