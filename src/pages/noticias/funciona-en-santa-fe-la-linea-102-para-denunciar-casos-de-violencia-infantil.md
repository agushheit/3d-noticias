---
category: La Ciudad
date: 2021-09-22T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/LINEA102.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Funciona en Santa Fe la línea 102 para denunciar casos de violencia infantil
title: Funciona en Santa Fe la línea 102 para denunciar casos de violencia infantil
entradilla: 'En principio funcionará de 7 a 19 de lunes a viernes. Fuera de esa franja
  horaria continuará la atención por la guardia de la Secretaría de los Derechos de
  la Niñez, Adolescencia y Familia. '

---
La provincia de Santa Fe cuenta desde este martes con la línea 102, de asistencia telefónica para denunciar hechos de violencia infantil. El servicio, que es gratuito y confidencial de escucha, contención y orientación para niños, niñas y adolescentes, funciona en el país desde 2009.

Santa Fe era una de las siete provincias que hasta el momento no contaba con este servicio, que empezará a funcionar tras el anuncio que se hizo durante la siesta de este martes en el Auditorio del Ministerio de Desarrollo Social. De esta manera se sumará a Ciudad y provincia de Buenos Aires; Jujuy; Salta; Tucumán; La Rioja; Catamarca; Santiago del Estero; Formosa; Chaco; Misiones; Corrientes; Entre Ríos; San Juan; Neuquén; Mendoza; y Córdoba.

Patricia Chialvo, secretaria de los Derechos de la Niñez, Adolescencia y Familia de Santa Fe, dialogó con El Litoral y detalló que el servicio ya está en funcionamiento de lunes a viernes de 7 a 19 horas, "será así unos meses de prueba. La idea es avanzar y ampliarlo a los fines de semana y, más adelante, ampliar el horario". Al mismo tiempo Chialvo aseguró que fuera de esa franja horaria continuará el sistema de guardias telefónicas y presenciales, que están vinculadas a la Justicia, la Policía y Salud, a través de los números: 0342-155145520 ó 0341-153217392.

El sistema del 102 en la provincia funcionará en la sede de la Secretaría de Niñez (calle San Luis 3135) con seis operadores y tres personas que son parte de un equipo interdisciplinario por si se presentan dudas durante las llamadas.

Al consultar a la secretaria el porqué de la demora en la puesta en funcionamiento de la línea nacional 102, indicó que "el año pasado cuando empezó la cuarentena estricta nos empezó a preocupar la manera en que los chicos puedan trasmitir lo que les estaba pasando, sabemos que cuentan los que les pasa cuando están en espacios públicos, y con la cuarentena no estaba pasando".

De la presentación de este nuevo servicio participaron, además de Chialvo: Gabriel Lerner, secretario nacional de Niñez, Adolescencia y Familia; Gisella Clivaggio, autoridad de la SENAF; y Daniel Capitani, ministro de Desarrollo Social de Santa Fe.

**Atención de una problemática recrudecida**

Chialvo mencionó que Santa Fe nunca había adherido a esta línea que está en vigencia hace 12 años. "Éramos una de las pocas provincias que nunca había adherido, por eso nos estamos preparando desde el año pasado con la capacitación del personal, los recursos institucionales, entre otros aspectos. Ahora con orgullo podemos decir que somos una de las últimas provincias, pero ya tenemos la línea 102 por decisión de esta gestión", valoró la funcionaria.

La vuelta a la "normalidad" tras largos meses de cuarentena y restricciones a causa del Covid-19, dejó en evidencia una problemática que lejos de caducar está en momentos difíciles. "Entre noviembre del 2020 y abril/mayo de este año ha habido un recrudecimiento en cantidad y agravamiento de las situaciones que nos llegaban, sobre todo lo que tiene que ver con maltrato y abuso, algo que se observó en toda la provincia y no particularmente en un solo lugar", lamentó la secretaria de los Derechos de la Niñez.

También indicó que en los últimos meses se agravó la situación de los adolescentes. "Vienen con un tiempo de conflicto en sus espacios familiares, atravesados por otras cuestiones".

**Mirada integral, universal e igualitaria**

Desde el Gobierno Nacional destacan los principios de la Línea 102, que son reconocidos en la Convención de los Derechos del Niño y la Ley Nacional de Protección Integral de los derechos de las Niñas, Niños y Adolescentes (Ley 26.061), y que se basan en una mirada integral, universal e igualitaria:

* No discriminación: Prevalece la condición de persona por sobre cualquier otra condición.
* Interés superior: se debe brindar la máxima satisfacción integral y simultánea de los derechos y garantías actualmente reconocidos en las leyes. Cuando exista un conflicto entre los derechos de niños, niñas y adolescentes frente a otros derechos igualmente legítimos, prevalecerán los primeros.
* Autonomía progresiva: el derecho a ser oídos y que sus opiniones sean tenidas en cuenta de acuerdo a su madurez y desarrollo.
* Vida, supervivencia y desarrollo.
* Libertad de expresión y derecho a ser oídos.

**Aporte para tablets**

La iniciativa "Aunar" es un programa que impulsa el Ministerio de Desarrollo Social, a través de la Secretaría Nacional de Niñez, Adolescencia y Familia (SENAF) y Unicef, con el fin de fortalecer las instituciones donde viven niñas, niños y adolescentes en todo el país. "El año pasado hicieron diferentes aportes por los cuidados de Covid-19 y este año dirigieron una ayuda económica para que cada chico o chica que está alojado en un sector residencial pueda tener una tablet para sus tareas escolares", mencionó Chialvo e informó que este martes recibieron las tarjetas con el dinero correspondiente para que cada centro residencial de la provincia pueda comprar el dispositivo a cada chico o chica que está alojado a su resguardo.