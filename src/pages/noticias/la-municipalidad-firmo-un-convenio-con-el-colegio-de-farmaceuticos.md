---
category: La Ciudad
date: 2021-05-29T06:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/Jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad firmó un convenio con el Colegio de Farmacéuticos
title: La Municipalidad firmó un convenio con el Colegio de Farmacéuticos
entradilla: En el marco del Plan de Gestión Menstrual, el acuerdo contempla una serie
  de acciones tendientes a mitigar las consecuencias negativas que padecen quienes
  no tienen acceso a elementos básicos,

---
En el Día internacional de la Higiene Menstrual, el intendente Emilio Jatón y la presidenta del Colegio de Farmacéuticos de la provincia de Santa Fe 1ª Circunscripción, Mirian del Pilar Monasterolo, firmaron un convenio de colaboración. El acuerdo, que se enmarca en el Plan de Gestión Menstrual implementado por la Municipalidad, busca coordinar acciones entre ambos organismos con el objetivo de mitigar las consecuencias negativas que padecen las personas que no pueden acceder de manera concreta a los elementos necesarios para procurar su higiene y normal desenvolvimiento.

En el acuerdo, las partes se comprometen a implementar campañas de donación de elementos de gestión menstrual con el objeto de ser utilizados en el marco del plan que ejecuta el municipio. Además, se realizarán campañas de concientización sobre las distintas opciones existentes para que cada persona pueda elegir la manera de gestionar su ciclo menstrual.

El encuentro virtual contó también con la participación de la secretaria de Integración y Economía Social de la Municipalidad, Ayelén Dutruel; la concejala Laura Mondino; la subdirectora de Mujeres y Disidencias, Florencia Costa; y el secretario del Colegio de Farmacéuticos, Carlos Morales.

Luego de la rúbrica, el intendente destacó el hecho de que en la ciudad se concreten medidas tendientes a borrar las desigualdades: “Nos propusimos empezar a romper desigualdades y no solo lo hacemos a través de la obra pública sino también mediante de este tipo de acciones como el que lleva adelante la Dirección de Mujeres y Disidencias”.

En tal sentido, Jatón destacó el trabajo articulado con el Colegio de Farmacéuticos. “Es muy positivo que el Colegio se involucre en esto de pensar qué ciudad queremos, es un día importante”, consignó el mandatario y agregó que el Plan de Gestión Menstrual “es un proyecto colectivo para cambiar una ciudad con desigualdad, con desequilibrios, así que si cada uno suma desde su lugar, el efecto positivo va a ser mayor aún”.

**Acción necesaria**

Por su parte, Monasterolo destacó que la gestión menstrual “es un tema que se pone en agenda y se ha trabajado de manera seria y con mucha sensibilidad por parte de las funcionarias municipales”. La presidenta del Colegio de Farmacéuticos añadió: “Nos sumamos en un día especial, de salud y acción de las mujeres, así que nos parece importante ocuparnos de este tema que muchas veces es tabú. Y que en muchos casos, las mujeres no tienen acceso para poder gestionar su período. Se trata de una acción necesaria, porque es la única forma de avanzar y tratar estos temas olvidados”.

Con respecto a cómo continúa la implementación del programa, desde el Colegio indicaron que a partir de la próxima semana “vamos a poner en conocimiento a todas las farmacias, a colocar afiches y una caja para visibilizar la campaña”.

En tanto, Mondino repasó que desde su rol como concejala “venimos trabajando desde hace años para poner en la agenda pública la gestión menstrual. Es importante que el Estado esté presente, porque hay una situación desigual entre hombres y mujeres. Por eso presentamos la ordenanza el año pasado y venimos trabajando con la Municipalidad para que las mujeres se inscriban y puedan recibir los implementos necesarios de manera gratuita”.

**Campaña de concientización**

Cabe recordar que el Plan de Gestión Menstrual, aprobado mediante Ordenanza N° 12.713, busca disminuir las situaciones de desigualdad que sufren las personas menstruantes en situación de vulnerabilidad. De este modo, contempla la implementación de medidas como la provisión de elementos de gestión menstrual con el objeto de lograr que una circunstancia netamente física no condicione el desarrollo normal de las personas.

En el convenio rubricado este viernes, el Colegio de Farmacéuticos manifiesta su intención de apoyar esa iniciativa municipal. Por lo tanto, se compromete a realizar campañas de concientización a fin de que las personas en situación de vulnerabilidad conozcan las distintas opciones existentes para sobrellevar su ciclo menstrual de la manera más adecuada. Además, se difundirán las diferentes posibilidades de elección, entre las variedades que sean más saludables y convenientes para cada persona.