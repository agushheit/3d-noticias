---
category: Deportes
date: 2021-08-04T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/SCOLA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: TyC Sports
resumen: 'El adiós a una leyenda: se retiró Luis Scola del seleccionado argentino'
title: 'El adiós a una leyenda: se retiró Luis Scola del seleccionado argentino'
entradilla: El legendario ala-pivote argentino Luis Scola se retiró del seleccionado
  argentino a los 41 años, tras cosechar innumerable cantidad de logros con el combinado
  albiceleste.

---
Con 51.4 segundos para finalizar el partido frente a Australia, compañeros y rivales se combinaron en un aplauso generalizado que significó la importancia del argentino para la historia del básquetbol mundial.

Jugó cinco campeonatos mundiales y cinco Juegos Olímpicos con la camiseta Argentina. Toda una leyenda por donde se lo mire.

Scola, nacido en Buenos Aires el 30 de abril de 1980, formó parte del plantel que logró el oro olímpico con la Selección Argentina en los Juegos Olímpicos de 2004, como así también el subcampeonato mundial en Indianápolis 2002 y China 2019.

Se formó en el club Ciudad de Buenos Aires y en Ferrocarril Oeste, donde debutó con solo 15 años en la Liga Nacional de básquetbol. Con la Selección, además de los logros mencionados, conquistó el campeonato FIBA Américas de 2001, el oro en el FIBA Diamond Ball 2008, el bronce en los Juegos Olímpicos de Beijing 2008, el FIBA Américas de 2011 y los Panamericanos 2019.

Scola ha sido un hombre récord por donde se lo mire: recientemente, quedó en Tokio 2020 como cuarto anotador máximo de todos los tiempos en los Juegos Olímpicos con 591, detrás del español Pau Gasol (623), el australiano Andrew Gaze (789) y el brasileño Oscar Schmidt (1093).

Además, jugó 41 partidos en Mundiales, marca máxima que comparte con el brasileño Ubiratán Pereira Maciel. Como adicional a esa cantidad de encuentros disputados, triunfó en 32 oportunidades, récord que lo convierte en el más ganador de la historia. En materia de anotadores en campeonatos del mundo, cerró su carrera en segundo lugar (611) con Oscar Schmidt como número uno con 906.

Por otro lado, en 2015, destronó a Schmidt (1.287) como máximo anotador en campeonatos FIBA Américas, tras convertir 18 puntos contra México y alcanzar la marca de 1.292 unidades.

**Un crack con la camiseta de la selección y también en clubes**

Luis Scola inició su carrera en Ferro (1995-98) y emigró al básquetbol español para defender la camiseta del TAU Cerámica (1998-2007) y Gijón Baloncesto (1998-2000), donde fue cedido a préstamo.

En su primera temporada en el básquetbol español de elite (2000) fue Novato del Año y luego brilló en grande, consiguiendo tres Copas del Rey (2002, 2004 y 2006), y tres Supercopas de España (2005, 2006 y 2007). Fue MVP de la Liga ACB en 2005 y 2007.

Dejó la ACB para emigrar la NBA, liga en la que vistió los uniformes de Houston Rockets (2007-2012), Phoenix Suns (2012-2013), Indiana Pacers (2013-2015), Toronto Raptors (2015-2016) y Brooklyn Nets (2016-2017). En China, jugó para Shanxi Zhongyu (2017-2018) y Shanghai Sharks (2018-2019), y en Italia para Olimpia Milano (2019-2020) y Pallacanestro Varese (2020-presente).