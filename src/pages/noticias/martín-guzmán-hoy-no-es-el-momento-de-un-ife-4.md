---
layout: Noticia con imagen
author: "Fuente: NA"
resumen: '"No es momento de IFE"'
category: Agenda Ciudadana
title: 'Martín Guzmán: "Hoy no es el momento de un IFE 4"'
entradilla: El ministro de Economía confirmó, este miércoles por la noche, que
  no habrá otro Ingreso Familiar de Emergencia (IFE).
date: 2020-11-12T13:29:17.683Z
thumbnail: https://assets.3dnoticias.com.ar/guzman.jpg
---
El ministro de Economía, Martín Guzmán, anunció esta noche la decisión de que no haya un cuarto Ingreso Familiar de Emergencia (IFE) de 10 mil pesos, el subsidio que se venía pagando a unos 9 millones de personas para paliar el impacto de la pandemia por coronavirus.

"El IFE sirvió muchísimo. Protegió a gente que estaba en una situación muy dura. Generó un efecto real en la economía. Implicó un esfuerzo importantísimo. Las medidas de protección social van evolucionando. Hoy no es el momento de un IFE 4 porque hay que mantener ciertos equilibrios para también proteger a la actividad, proteger a la gente, a todo el país", dijo Guzmán.

Explicó que se van "manejando los tiempos, no es es el momento de eso", al tiempo que agregó que "hay una situación muy dura, pero hay equilibrios muy delicados que mantener para que haya estabilidad en Argentina".

"Por ahora no habrá IFE 4 -agregó- pero nos guardamos flexibilidad, igualmente, por si la pandemia pega la vuelta como pasó en Europa", dijo Guzmán en declaraciones a formuladas a la señal de cable C5N.

El funcionario explicó que irán reemplazando estos subsidios con otras medidas. "Iremos expandiendo programas existentes como el Potenciar Trabajo y las asistencias directas como la AUH. Buscamos focalizar ayuda en los sectores que más lo necesitan", señaló.

No obstante, el ministro dijo que en lo que queda del año "la ayuda está aumentando". "En lugar de hacerlo en forma de IFE, lo hacemos vía otros programas más focalizados en los que más lo necesitan", indicó.

Dijo comprender que todos lo que recibían el IFE están "en una situación dura, pero al mismo tiempo hay un conjunto de políticas que buscan reactivar la producción y el empleo".

"Estamos parados en un lugar que nos permite tener una perspectiva positiva hacia adelante. Se han logrado avances muy importantes para poder avanzar en el camino de la recuperación", detalló el funcionario.

Por último, precisó: "Recordemos de dónde venimos, en el año 2018, dos años después de que Argentina hubiese regresado a los mercados de crédito internacional. El país empezó a endeudarse, en el 2018 el mundo dijo basta y ahí se recurrió inmediatamente al FMI".