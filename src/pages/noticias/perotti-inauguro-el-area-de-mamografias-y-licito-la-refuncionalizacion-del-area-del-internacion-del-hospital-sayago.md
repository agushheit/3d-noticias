---
category: Estado Real
date: 2021-10-14T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/SAYAGO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti inauguró el area de mamografias y licitó la refuncionalización del
  área del internación del Hospital Sayago
title: Perotti inauguró el area de mamografias y licitó la refuncionalización del
  área del internación del Hospital Sayago
entradilla: El nuevo sector habilitado se ejecutó en el marco del Plan Incluir, y
  contó con una inversión de $14.574.664. Por otra parte, las futuras obras a licitar
  tienen un presupuesto superior a los $240.000.000.

---
El gobernador Omar Perotti encabezó la apertura de sobres con las ofertas económicas para la refuncionalización del pabellón Nº 21 de internación del Hospital “Gumersindo Sayago”, de la ciudad de Santa Fe, obras que tienen un presupuesto oficial de $246.784.271.

Previamente, el gobernador inauguró el área de mamografías de ese nosocomio, obra que se ejecutó en el marco del Plan Incluir y contó con una inversión de $14.574.664.

Durante el acto, que se desarrolló en el mismo Hospital Sayago, Omar Perotti agradeció “al personal médico, asistentes, enfermeros y a todos los que en este hospital estuvieron desde el primer día al pie del cañón, viendo cómo complementábamos la atención. Ustedes, también, son merecedores de estas inversiones”, manifestó.

A continuación, el gobernador destacó el “nivel de equipamiento y de refuerzo en la infraestructura”, para “ampliar la capacidad de atención”; y “esto seguirá para brindar la mejor atención a nuestra gente”. Asimismo, resaltó que “el sistema de salud de la provincia, y cada uno de sus integrantes, ha respondido y ha estado a la altura”, destacó.

Por último, el mandatario indicó que Santa Fe “demostró su capacidad para ser referente en salud, en el cuidado de su gente, con más y mejor infraestructura y equipamiento”.

A su turno, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, sostuvo que “este hospital atiende a una gran población del norte de la ciudad. Por eso era imprescindible contar con un conjunto de obras como las que hoy se están licitando. También inauguramos la sala de mamografía, que es de gran importancia que exista ese equipamiento en este lugar”, explicó.

“Estar hoy interviniendo en el Hospital Sayago tiene que ver con un gobierno que piensa en el día a día, en la necesidad concreta, para hacer aquello que nos propuso Omar Perotti, que es poner a Santa Fe de pie”, finalizó Frana.

En tanto, el secretario de Salud, Jorge Prieto, destacó que “la salud en la provincia crece, porque esta gestión a priorizado, a pesar de la pandemia, asistir a la salud no solo lo que hace a recursos humanos, sino en obras e infraestructura”; y la importancia al “acceso cuidado, con la individualidad que va a tener esta sala pabellón histórica que viene desarrollando su actividad hace 68 años”.

Prieto señaló que gracias a “la gestión de gobierno, y una muy buena administración de los recursos, se alcanzaron estos logros que sin duda van a quedar y marcar una trayectoria”.

Por último, la directora del hospital, Claudia Maidone, afirmó: “Para nosotros es un orgullo tenerlos en nuestra casa. Le comentaba a Omar Perotti que en los 68 años que tiene este hospital, jamás un gobernador vino a visitarnos. Para nosotros es un placer enorme tenerlo hoy, que conozca nuestro hospital, que sabemos que es pequeño, que tiene un gran futuro y que cumple un rol fundamental en la atención ambulatoria de zona norte”, subrayó.

**Ofertas**

Para la ejecución de las futuras obras se presentaron cinco empresas oferentes. La primera correspondió a la firma Dinale SA, que cotizó por $287.757.566; la segunda fue de Mundo Construcciones SA, por $303.314.130; la tercera de Cocyar SA, por $280.779.974; la cuarta de VFM SA, por $ 255.000.000; y la quinta correspondió a la UT conformada por MT SRL y Orion Ingenieria SRL, por $280.075.051.

**Pabellón n°21**

La intervención en el pabellón de internación Nº 21 comprenderá la ejecución de 30 habitaciones de internación (incluyendo cuatro de ellas para aislamiento y cuatro para pacientes psiquiátricos leves); áreas de enfermería con box limpio y sucio, vestuarios, office y sanitarios para el personal; habitación de guardia médica con sanitario propio y lockers de guardado; sala de médicos y departamento enfermería, locales para terapia ocupacional, psicología, informes médicos y admisión, depósitos, locales de servicio de diversa índole y un área de locales específicos para instalaciones.

Además, se prevé un nuevo ascensor, la instalación de paneles solares para ahorro energético, servicio contra incendios y escalera metálica externa para evacuación del edificio en casos de emergencia. Por otra parte, en el patio norte colindante con los pabellones Nº 21 y 22, se realizará el tratamiento arquitectónico del mismo, incluyendo galerías de estar y trabajos de parquización.

La obra tiene un presupuesto oficial de $246.784.271 y el plazo de ejecución es de 300 días desde su adjudicación. El proyecto fue elaborado por el Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Arquitectura y Obras Públicas, ya que el efector fue concebido originalmente como una remodelación de parte del patrimonio edilicio del antiguo Liceo Militar General Belgrano, razón por la cuál el esquema circulatorio del hospital no es el más apto.

Esto es porque para acceder a la circulación pública, ubicada al norte del mismo, se debe transitar gran parte del edificio por pasillos de reducidas dimensiones, provocando un conflicto de circulación, con mezcla de usos privados con otros más públicos.

**Presentes**

Participaron también de la actividad la vicedirectora del Hospital Sayago, Elba Aguilar; la concejala de la ciudad de Santa Fe, Jorgelina Mudallel; el subsecretario de Planificación, Omar Romero; las autoridades de los hospitales Mira y López, Ignacio Francia; del Iturraspe, Osvaldo Marelli; y de Vera Candioti, Rubén Candioti; entre otras autoridades.