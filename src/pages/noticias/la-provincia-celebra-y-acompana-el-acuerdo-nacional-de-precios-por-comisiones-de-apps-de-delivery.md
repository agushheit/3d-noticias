---
category: Agenda Ciudadana
date: 2020-12-21T14:40:03Z
thumbnail: https://assets.3dnoticias.com.ar/2112apps1.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Acuerdo nacional de precios por comisiones de apps de delivery
title: La provincia celebra y acompaña el acuerdo nacional de precios por comisiones
  de apps de delivery
entradilla: Rappi y Pedidos Ya acordaron con la Federación Empresaria Hotelera Gastronómica
  Argentina implementar topes en las comisiones. «Santa Fe tomó el liderazgo e innovó
  en esta materia», destacó Aviano.

---
En mayo de este año la Secretaría de Comercio Interior y Servicios de la provincia de Santa Fe, dependiente del Ministerio de Producción, Ciencia y Tecnología, como autoridad de aplicación a nivel provincial de la Ley de Abastecimiento, comenzó por primera vez a nivel país un proceso inédito de conciliación colectiva voluntaria con la empresa Pedidos Ya, a fin de lograr la baja de comisiones que dicha firma cobraba al sector gastronómico. La misma se fundamentó en el pedido realizado a la provincia por los representantes de los sectores gastronómicos de las ciudades de Rafaela, Santa Fe y Rosario.

**El pedido estaba basado en la dura crisis que comenzaba a atravesar el sector gastronómico**, como consecuencia de las medidas de restricción de la actividad comercial, y de las comisiones abonadas a las empresas de reparto a domicilio (apps – aplicaciones informáticas web) superando la rentabilidad del producto, ubicándose en algunos casos rondando el 35%.

Detallando las gestiones realizadas, la directora Provincial de Promoción de la Competencia y Defensa del Consumidor, María Betania Albrecht, indicó que «la provincia llevó adelante acciones conciliatorias a efectos de arribar a un acuerdo de partes, entendiendo el rol relevante del Estado de articular para el beneficio de la sociedad en general y, en particular, en este caso del sector gastronómico, lo cual también redunda en beneficio del sector más débil de la cadena: las y los consumidores y usuarios finales de los productos».

![](https://assets.3dnoticias.com.ar/2112apps.webp)

«Ante la falta de acuerdo, y la no presentación de documentación por parte de Pedidos Ya, la autoridad de aplicación solicitó a API información referente a las ventas declaradas por la empresa, de la cual surgieron los aumentos en su facturación y la comprobación de que, frente a sus competidores en el mercado local, dicha empresa concentra el 90% de la facturación, teniendo por tanto una posición dominante», agregó la funcionaria.

En dicho marco se dispuso multar con 3 millones de pesos a la empresa; y consecuentemente se fijó, provisoriamente, el porcentaje máximo a cobrar por comisiones en 18%.

Finalmente, en reuniones posteriores entre partes, lideradas y coordinadas por la Secretaría de Comercio Interior y Servicios, se logró arribar a un acuerdo entre la empresa de _delivery_ y los asociados de las tres cámaras gastronómicas denunciantes. 

Además, la mencionada secretaría dictó el 10 de septiembre la Disposición N° S-17 donde estableció para Pedidos Ya S.A. en 25% más IVA el máximo que puede percibir en comisiones por _marketplace_ con logística vinculada a la comercialización de alimentos y bebidas, extensivo a todos los titulares de comercios santafesinos con contratos vigentes y por celebrarse, desde el 1 de agosto hasta el 31 de diciembre de 2020, no afectando a los contratos vigentes que registren una comisión menor al 25% más IVA.

Sobre el acuerdo alcanzado a nivel nacional, el secretario de Comercio y Servicios, Juan Marcos Aviano, destacó que «en estos días de diciembre, las firmas Rappi y Pedidos Ya acordaron con la Federación Empresaria Hotelera Gastronómica de la República Argentina implementar topes en las comisiones que cobran las apps a los comercios, quedando por tanto abarcados los sectores gastronómicos santafesinos, dando así un empuje final a la iniciativa de **nuestra provincia, quien tomó el liderazgo e innovó en esta materia a nivel país** en el momento justo en que el sector comercial más necesitaba del apoyo del Estado».