---
category: Agenda Ciudadana
date: 2020-11-29T12:57:29Z
thumbnail: https://assets.3dnoticias.com.ar/BARES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Coronavirus: nuevas habilitaciones'
title: 'Coronavirus: se  dispusieron nuevas habilitaciones y modificaciones de horarios '
entradilla: 'Los comercios podrán abrir hasta las 21 horas, y los bares y restaurantes
  hasta la 1:30 los viernes y sábados. '

---
Las habilitaciones comenzarán con el Fútbol 5, y continuará con jardines maternales, natatorios y colonias de vacaciones.

El gobierno de la provincia de Santa Fe dispuso modificaciones en los horarios de aperturas de comercios, bares y restaurantes; habilita la práctica de deportes como el “Fútbol 5” en la modalidad entrenamiento; las competencias automovilísticas y motociclísticas; la realización de eventos culturales y recreativos relacionados con la actividad teatral y música en vivo; como así también, la apertura de jardines maternales y colonias de vacaciones.

En primer lugar, a través del decreto Nº 1527, se habilita a los comercios mayoristas y minoristas de venta de mercaderías de rubros no esenciales, que brinden atención al público en los locales, a permanecer abiertos hasta las 21 horas.

En tanto, que extiende el horario de apertura de bares, restaurantes, heladerías, y otros, con y sin concurrencia de comensales, incluyendo las actividades de envíos a domicilio, hasta la 1:30 los días viernes y sábados. Asimismo, el horario de restricción de circulación en la vía pública será de 0:30 a 6 de domingo a jueves; y de 1:30 a 6 viernes y sábados.

En el mismo decreto se remarca que los salones de eventos y de fiestas, que fueron habilitados por municipios y comunas para funcionar como bares y restaurantes, deberán cumplir los protocolos específicos para estas actividades, que cuentan con un factor de ocupación permitido del treinta por ciento (30%), asegurando el distanciamiento entre personas y sin actividades de baile o circulación entre las mesas.

### Deportes con contacto físico, el Fútbol 5, autos y motos

Además, a partir del 28 de noviembre, la provincia autorizó, a través del decreto Nº 1528, la práctica de deportes con interacción o contacto físico, en la modalidad entrenamiento, incluidos el "Fútbol 5", de 7 a 22 horas, deberá tener turnos previos y realizarse en espacios suficientemente ventilados. En este caso se prohíbe la realización de competencias o torneos, la asistencia de espectadores y la habilitación de espacios de uso común.

En tanto que, a partir del 1 de diciembre, el decreto provincial Nº 1529 autoriza la práctica de competencias automovilísticas y motociclísticas, sin concurrencia de espectadores; cumpliendo el protocolo nacional para el desarrollo de la actividad respetando las medidas de distanciamiento e higiene necesarias para disminuir el riesgo de contagio de COVID-19.

### Eventos culturales y recreativos

Asimismo, y también desde el 1 de diciembre, el decreto Nº 1530 autoriza la realización de eventos culturales y recreativos relacionados con la actividad teatral y música en vivo, con concurrencia de personas, tanto al aire libre como en teatros, centros culturales y otros lugares cerrados; de acuerdo al protocolo nacional para el desarrollo de la actividad. La ocupación en estos espacios compartidos, debe asegurar 2,25 metros cuadrados de circulación entre personas, incluso para los artistas; y se debe disponer una distancia entre butacas ocupadas de 1,5 metros. En cuanto a ambas situaciones, el límite de ocupación será el 50% del espacio habilitado al aire libre y del 30% del establecimiento habilitado en lugares cerrados.

### Jardines maternales y colonias de vacaciones

En tanto, el decreto Nº 1531, habilita desde el 1 de diciembre el funcionamiento de jardines maternales de gestión privada, los que podrán funcionar de 7 a 19 horas; en espacios abiertos o cerrados ventilados; con un factor de ocupación que no supere el 50%; y organizando grupos reducidos estables que funcionen como burbujas sociales, evitando la interacción con los otros grupos o burbujas. Por último, el decreto Nº 1532 habilita a partir del 5 de diciembre el funcionamiento en clubes e instituciones sociales de las "colonias de vacaciones" y "talleres de verano", con o sin uso de natatorios, con cumplimiento de los protocolos. El horario permitido es de 7 a 19 horas, y se deberán organizar grupos reducidos que funcionen como burbujas sociales, evitando la interacción con otros grupos o burbujas; y con un factor de ocupación en el medio acuático de una persona cada cinco (5) metros cuadrados.