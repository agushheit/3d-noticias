---
category: Deportes
date: 2020-12-13T14:12:03Z
thumbnail: https://assets.3dnoticias.com.ar/basket.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'FUENTE: DIEZ EN DEPORTES - PRENSA ASB'
resumen: Unión A y Banco B definen el título en categoría U15 del Torneo Mixto
title: Unión A y Banco B definen el título en categoría U15 del Torneo Mixto
entradilla: Los rojiblancos superaron a Macabi por 82 a 53, en tanto el elenco de
  Guadalupe dio cuenta de Colón (SJ) B por 87 a 61. El lunes continuarán las llaves
  semifinales en las otras categorías

---
Banco Provincial B y Unión A se convirtieron este viernes en finalistas de la categoría U15, en el Torneo Presencial Mixto organizado por la Asociación Santafesina de Básquet.

Los de Guadalupe superaron en las tres pruebas (tiros libres, habilidades y tiros de tres puntos) a Colón (SJ) B por 87 a 61. Asimismo, el Tate dio cuenta de Círculo Israelita Macabi por 82 a 53.

<br/>

**A continuación, las síntesis de ambos choques y la programación en el resto de las series semifinales:**

<br/>

***

**BANCO PROVINCIAL B 87**: Buttera 3, Pereyra 8, Sagardía 6, Suárez 6, Ezcurra 4, Massara 4, J. Borsatti 7, Romero 9, Bortolotto 11, Manattini 12, Bugnon 11, Schatzler 6. DT: F. Borsatti.

**COLÓN (SJ) 61**: Becerra 5,Tibaldo 4, Gilardi 6, Alessio 2, Micheloni 4, Bournissent 6, Bruno 6, Campana 6, Estrubia 3, Grosso 8, Scarafía 8, Esparza 4. DT: D. Giménez.

**Parciales**: 23-17 y 47-39.

**Jueces:** V. Rodríguez y J. Peralta.

**Canchas**: Julián Ortiz de Zárate y Félix Colombo.

<br/>

***

**UNIÓN A 82:** U. González 2, Cantero 7, Rodríguez 8, Sosa 3, Garzon 7, Machuca 9, Figura 6, Márquez 5, Antoniazzi 19, Ferrari 16. DT: A. Ferrari.

**MACABI 53**: Ritvo 6, Carreras 3, Quaino 6, Rivadeneira 5, Kolb 6, García Mas 4, E. Wulfsohn 4, G. Planells 6, Martínez 5, Rial 3, L. Wulfsohn 5. DT: O. Acosta.

**Parciales**: 20-20 y 36-34.

**Jueces**: L. Menéndez y G. Damasco.

**Canchas:** Ángel Malvicino e Iehúda Hamacabí.

<br/>

***

**RESTO DE LAS SEMIFINALES**

***

🗓 **Lunes 14 de diciembre**

<br/>

* **CATEGORÍA U13**

🕘**16.30** Rivadavia vs. Santa Rosa

🕘**18.00** Almagro vs. Banco Provincial

<br/>

* **CATEGORÍA U19**

🕘**18.30** Unión vs. Banco Provincial

🕘**20.00** Almagro vs. CUST

<br/>

***

**🗓 Martes 15 de diciembre**

<br/>

* **CATEGORÍA U17**

🕘**16.30** Unión vs. Macabi

🕘**17.30** Almagro vs. CUST

<br/>

***

🗓 **Miércoles 16 de diciembre**

<br/>

* **CATEGORÍA MAYORES**

🕘**19.00** Banco Provincial vs. Santa Rosa

🕘**20.15** Rivadavia vs. Almagro

<br/>