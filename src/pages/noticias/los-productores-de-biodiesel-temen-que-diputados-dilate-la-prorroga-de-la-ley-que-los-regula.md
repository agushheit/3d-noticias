---
category: El Campo
date: 2021-01-23T10:09:10Z
thumbnail: https://assets.3dnoticias.com.ar/biodisel1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: La Política Online
resumen: Los productores de biodiesel temen que Diputados dilate la prórroga de la
  ley que los regula
title: Los productores de biodiesel temen que Diputados dilate la prórroga de la ley
  que los regula
entradilla: El actual régimen vence en mayo y fija el piso de corte para los biocombustibles.
  Piden que se trate en la próxima sesión extraordinaria.

---
Después de largos meses de crisis, que puso al sector al borde de la quiebra, las Cámaras que nuclean a los productores de biocombustibles recibieron una buena noticia, la Secretaría de Energía decretó un aumento del precio para la producción destinada al mercado interno que estaba fijada por debajo de los costos de fabricación.

Si bien, esta decisión que venían reclamando hacía tiempo llevó alivio a los productores, necesitan que la ley que regula, entre otros temas, los pisos de corte que es del 10% en biodiesel y 12% para bioetanol y vence en mayo próximo.

Luego de varias reuniones de representantes del sector con el bloque de diputados oficialistas, se había acordado tratar una prórroga de la ley en el período de sesiones extraordinarias, que se iba a realizar este miércoles 20 de enero pero terminó pasando para la próxima semana.

![](https://assets.3dnoticias.com.ar/biodisel.jpg)

Hasta ahora, el tema no se había incorporado en la orden del día lo que motivó que el bloque Consenso Federal que conduce Graciela Camaño enviara una nota al presidente de la Cámara, Sergio Massa, en la que recuerdan que "el régimen es vertebral para la producción federal, el agregado de valor y el sosteniendo del trabajo en gran parte del territorio nacional".

En consecuencia, solicitan el cumplimiento del acuerdo para tratar de inmediato la prórroga del Régimen de Biocombustibles tal como se había comprometido la Cámara en la última sesión del 29 de diciembre y aclararon a través de una nota que "no está en nuestro ánimo, ni forma parte de nuestras acciones, imponer asuntos a ser tratados ni limitar el tratamiento de los que están previstos (en extraordinarias). Sólo pedimos que se cumplan los acuerdos".

Se sospecha que la dilatación en la sanción de la prórroga de la ley que se aprobó en 2006 se explica en el lobby que ejercen las provincias petroleras en sintonía con las grandes empresas que se resisten a fijar pisos de mezcla con biocombustibles que por otra parte, es una de las cláusulas del Acuerdo Climático de París, suscripto por Argentina.

>  **Las cámaras de biocombustibles acusan al gobierno de favorecer a las empresas petroleras**

En cuanto a la situación en el sector, desde la Cámara Santafesina de Energías Renovables, explicaron a LPO que "con la recomposición de tarifa, de a poco se va a acomodando. Se sabía que enero iba a ser un mes de transición, luego de un "parate" tan largo y con el precio del aceite que recién está empezando a bajar. Entendemos que a partir de febrero las plantas van a operar de manera más regular", sostuvo su presidente Juan Facciano.