---
category: La Ciudad
date: 2021-12-31T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/GUARDIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Ante la suba de casos Covid, el Cullen suspende visitas y restringe la circulación
  de personas
title: Ante la suba de casos Covid, el Cullen suspende visitas y restringe la circulación
  de personas
entradilla: Así lo resolvió el Consejo de Administración, la Dirección y el Comité
  de Coronavirus. Se dispuso el cierre de la puerta principal del efector durante
  la mañana

---
El Consejo de Administración, la Dirección y el Comité de Coronavirus del Hospital Cullen definieron restringir, desde el día 31 de diciembre de 2021, la libre circulación de personas dentro del efector ante el notorio crecimiento de la curva de contagios. La medida comenzará a regir este viernes.

El objetivo de la medida es disminuir sensiblemente la cantidad de personas que diariamente se mueven por los pasillos, salas y patios del nosocomio. Siempre con la idea de "proteger a la comunidad y, por supuesto también, a nuestros agentes de la salud", destacaron a través de un comunicado.

Por esta razón, se dispuso el cierre de la puerta principal durante las mañanas del hospital Cullen. Con relación a los turnos programados de aquellos servicios que permanecen en el hospital y que aún no migraron al CEMAFE, indicaron que "por el momento, están respetándose. Igualmente, los turnos de rayos y tomografías van a continuar cumpliéndose, salvo que el Ministerio de Salud disponga en contrario".

En ese sentido, advierten que "mientras permanezca esta situación de contención, queda suspendida la visita y solo podrá ingresar un solo familiar por paciente a recibir informes, en el horario que cada sala le indique al momento de la internación".

Aclararon que dicha medida aplica "tanto para las salas generales, como las de Terapia". En tanto, "el ingreso a la guardia será de una sola persona (el paciente o consultante), sin acompañante, de no ser imprescindible el mismo".

"Comprendemos que cuando un familiar o allegado se encuentra internado, o en estado crítico, o por un nacimiento, quisiéramos estar presentes, pero hoy, la prioridad sanitaria es la prevención y la vigilancia, y ello implica restringir la circulación de personas", destaca el comunicado.

El director del Cullen, Juan Pablo Poletti, dijo en declaraciones de LT 10: "Debemos proteger a nuestros internados, a nuestro personal. Con la circulación y la curva de contagios tan brusca y con una cepa tan contagiosa, realmente ponemos en riesgo a la salud de los internados permitiendo una visita masiva externa hacia el interior del hospital".

"Realmente los casos están en ascenso y sabemos que el 31 es una fecha donde muchos familiares vienen a saludar", comentó. Reconoció que la medida podrá resulta "antipática", y continuó: "el tener tanta circulación en los pasillos del hospital pone en riesgo no solo la salud del personal nuestros sino de los propios familiares. Esto es un poco lo que queremos proteger".

****