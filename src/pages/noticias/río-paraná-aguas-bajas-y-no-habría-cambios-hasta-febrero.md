---
layout: Noticia con imagen
author: "Fuente: Radio 2"
resumen: "Río Paraná: aguas bajas"
category: Agenda Ciudadana
title: "Río Paraná: aguas bajas y no habría cambios hasta febrero"
entradilla: "El Instituto Correntino del Agua y del Ambiente (ICAA) publicó el
  informe semanal del Instituto Nacional del Agua (INA): el Río Paraná en tramo
  argentino – paraguayo el caudal se mantuvo inferior a lo normal."
date: 2020-11-22T13:53:25.504Z
thumbnail: https://assets.3dnoticias.com.ar/rio-parana.jpeg
---
El Instituto Correntino del Agua y del Ambiente (ICAA) publicó el informe semanal del Instituto Nacional del Agua (INA), con respecto al Río Paraná en tramo argentino – paraguayo el caudal se mantuvo inferior a lo normal, mientras que el Río Uruguay se presenta estable o con oscilaciones poco significativas en aguas bajas.

La tendencia climática actualizada con horizonte al 31 de enero 2021 asume un panorama desfavorable, no se esperan eventos que puedan aliviar la situación de escasez y bajante que predomina en la región.

En el Río Paraná, no se registraron precipitaciones significativas sobre la cuenca de aporte al tramo producto de la presencia de un sistema de altas presiones en el continente. No se pronostican lluvias significativas en el tramo para la semana entrante.

El caudal entrante a Yacyretá, esta semana se observó un sostenido aumento hasta el día 14 de noviembre graduando en 9.400 metros cúbicos por segundo, luego sufrió una pronta disminución y se mantiene oscilando en torno a 8.800 metros cúbicos por segundo. El promedio semanal se encuentra próximo a 8.650 metros cúbicos por segundo, es 400 metros cúbicos superior al promedio de la semana anterior.

Se mantiene muy inferior al promedio de los últimos 25 años para el mes de noviembre. Es probable que continúe con el mismo comportamiento los próximos días, no se esperan eventos meteorológicos que permitan un cambio en la situación.

La descarga sostuvo un comportamiento oscilante en torno a 8.000 metros cúbicos por segundo durante toda la semana. El 18 de noviembre próximo a 8.100 metros cúbicos por segundo. En los próximos días se mantendría oscilando en el rango 8.000 metros cúbicos por segundo - 9.000 metros cúbicos por segundo. No se espera un cambio en las condiciones actuales.

En Corrientes – Barranqueras, esta semana los niveles se mantuvieron oscilando sin tendencia definida. Los máximos se dieron el día 15 de noviembre con una cota próxima de 0,87 metros y 0,82 metros respectivamente. El día 18 de noviembre llegó a 0,85 metros y 0,90 metros.

El promedio semanal se encuentra próximo a 0,88 metros, se mantiene similar al promedio de la semana anterior y se ubica 3,13 m por debajo del promedio mensual de noviembre de los últimos 25 años.

Persiste la condición de aguas bajas y no se prevé aún un cambio en la situación por lo menos hasta el 31 de enero. Es probable que comiencen a incrementarse los niveles, encontrándose en torno a 1,30 metros la próxima semana.

La futura evolución dependerá fuertemente de la evolución de las lluvias en la región, especialmente en la parte de la cuenca de aporte de respuesta más rápida (cuenca del río Iguazú, cuenca próxima al embalse de Itaipú y cuenca de aporte al tramo misionero-paraguayo).

En Goya esta semana el nivel se mantuvo en descenso hasta el día 15 de noviembre que marcó el mínimo en torno a 1,17 metros luego se incrementó fugazmente y se mantiene oscilante desde entonces. El 18 de noviembre se mantuvo próximo a 1,18 metros. Es probable que retorne al ascenso y la semana entrante se presente regulando en torno a 1,40 metros. El promedio semanal observado 1,20 metros es similar al promedio de la semana anterior y se encuentra 2,86 metros por debajo del promedio mensual de noviembre desde 1995.

El Río Uruguay se presentó estable o con oscilaciones poco significativas en aguas bajas, durante los últimos 7 días se produjeron lluvias con valores medios areales acumulados moderados sobre las nacientes y leves sobre el resto de la Alta Cuenca, siendo deficitarios o nulos los valores registrados para la cuenca media y baja.

Finalmente, las previsiones numéricas de precipitación indican acumulados leves sobre las nacientes en el corto plazo con posibilidad de intensificarse hacia valores moderados al mediano plazo.