---
category: La Ciudad
date: 2020-12-04T10:51:12Z
thumbnail: https://assets.3dnoticias.com.ar/CHANGOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Solicitan changos y canastos adaptables a sillas de ruedas en supermercados
title: Solicitan changos y canastos adaptables a sillas de ruedas en supermercados
entradilla: 'Se trata de un proyecto de Resolución presentado por el concejal de UCR-Juntos
  por el Cambio Carlos Suárez. '

---
En el marco del Día Internacional de las Personas con Discapacidad, el concejal del bloque UCR-Juntos por el Cambio Carlos Suárez recordó la solicitud que realizó para que los supermercados e hipermercados de la ciudad cuenten con changos y canastos adaptables a sillas de ruedas. El pedido fue formalizado a través de un proyecto de Resolución, que presentó ante el Concejo Municipal en el mes de septiembre.

“El objetivo de nuestra propuesta es avanzar hacia una sociedad más inclusiva, igualitaria y justa. Falta mucho camino por recorrer para construir una ciudad que incluya a todos sus integrantes”, indicó el legislador. En el mismo sentido señaló que hay pequeños aportes que pueden significar grandes avances en la vida de las personas con discapacidad.

“Pensar en cómo mejorar la cotidianidad de las personas con movilidad reducida, en una actividad tan esencial como realizar las compras en supermercados e hipermercados, es necesario”, sostuvo Carlos Suárez.

El concejal de UCR-Juntos por el Cambio destacó que otras ciudades ya implementaron medidas similares. “Por eso estamos solicitando que se implementen carros o canastos de compras con características tales que permitan su adaptación a las sillas de ruedas de los consumidores con movilidad reducida”, destacó.

## **Ciudad de todos**

El 3 de diciembre se conmemora el Día Internacional de las Personas con Discapacidad. “Esta fecha nos invita a reflexionar en todas aquellas acciones que podemos desarrollar para que realmente nuestra ciudad incluya a todos sus habitantes”, afirmó Carlos Suárez.

Este día fue fue declarado en 1992 por la Asamblea General de las Naciones Unidas, con el objetivo de promover los derechos y el bienestar de las personas con discapacidades en todos los ámbitos de la sociedad y el desarrollo, así como concientizar sobre su situación en todos los aspectos de la vida política, social, económica y cultural.

Este año el lema del Día Internacional de las Personas con Discapacidad de Naciones Unidas es "Participación y el liderazgo de las personas con discapacidad: Agenda de Desarrollo 2030". Se centra en el empoderamiento de las personas con discapacidad para un desarrollo inclusivo, equitativo y sostenible, como se pedía en la Agenda 2030 para el Desarrollo Sostenible, que se compromete a "no dejar a nadie atrás" y considera la discapacidad como una cuestión transversal en la implementación de sus 17 Objetivos de Desarrollo Sostenible (ODS).