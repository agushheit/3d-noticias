---
category: Estado Real
date: 2021-07-28T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/LIF.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti y Salvarezza recorrieron el Laboratorio Industrial Farmacéutico de
  la ciudad capital
title: Perotti y Salvarezza recorrieron el Laboratorio Industrial Farmacéutico de
  la ciudad capital
entradilla: 'El gobernador recibió este martes al ministro de Ciencia, Tecnología
  e Innovación de la Nación. '

---
El gobernador de la provincia, Omar Perotti, recibió este martes al ministro de Ciencia, Tecnología e Innovación de la Nación (Mincyt), Roberto Salvarezza, en el Laboratorio Industrial Farmacéutico (LIF), de la capital santafesina.

Durante el encuentro, el gobernador Perotti afirmó que “es un orgullo para nosotros recibir en nuestro laboratorio al ministro y a la presidenta del Conicet. Son dos personas con quienes venimos trabajando desde el primer día. El trabajo en pandemia le permitió al sector científico mostrarse y ganarse el reconocimiento de aquellos que no visualizaban el potencial de nuestro sistema científico-tecnológico”, dijo.

“El LIF demostró su potencial y su colaboración. Además, tuvo una tarea adicional, que fue facilitarnos la logística para la llegada de insumos. En estos momentos está trabajando en la ampliación de medicamentos, queremos tener la primera instancia de la experiencia con el cannabis medicinal. Eso es parte de lo mucho que se está haciendo”, agregó Omar Perotti.

“Tenemos un gran potencial que, trabajando junto con la Nación y, con el apoyo que el sector científico tecnológico tiene del presidente Alberto Fernández, es una posibilidad de crecer muchísimo más para bien de nuestra provincia y para el conjunto del sistema de salud de nuestra Argentina”, finalizó el gobernador de la provincia.

**Capacitación y dedicación**

Por su parte, el ministro Salvarezza agradeció el recibimiento y destacó “la preocupación que ha tenido siempre Omar Perotti por la importancia que tiene el conocimiento, la ciencia y la tecnología en impulsar el desarrollo de Argentina, y en particular de Santa Fe, donde hay una enorme capacidad”, dijo.

Y agregó: “Santa Fe tiene una cantidad de investigadores, técnicos y becarios de enorme dedicación que han hecho contribuciones muy importantes, además de tener al LIF donde se producen medicamentos para toda la provincia. Cuando hablamos de distribución pública de medicamentos hablamos del LIF, es un ejemplo de cómo funciona”, expresó.

Luego, Salvarezza reafirmó “el compromiso del gobierno nacional en apoyar a la ciencia y la tecnología en todo el país. Argentina ha hecho durante la pandemia muchas contribuciones en cuanto a diagnóstico y elementos de protección. Nos queda un tema central, que es que Argentina pueda producir sus propias vacunas y Santa Fe va a ser un lugar importante para ello”, finalizó el funcionario nacional.

También hubo autoridades provinciales y nacionales que recorrieron el LIF con el gobernador Perotti y el ministro Salvarezza. Entre ellos estuvieron presentes, la secretaria de Ciencia, Tecnología e Innovación de la provincia, Marina Baima; los directores del LIF, Élida Formente, Analía San Román, Esteban Robaina y Lisandro Zilli; la presidenta del Conicet, Ana Franchi; el subsecretario de Coordinación Institucional del Mincyt, Pablo Nuñez y la subsecretaria de Proyectos Científico-Tecnológicos, Eva Rueda.