---
category: La Ciudad
date: 2021-10-06T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/alassia.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El Hospital Alassia es el lugar elegido para la vacunación de niños con comorbilidades
title: El Hospital Alassia es el lugar elegido para la vacunación de niños con comorbilidades
entradilla: 'Restan detalles para definir al Hospital de Niños como centro vacunatorio
  para menores de entre 3 y 11 años con factores de riesgo, tal como lo fue para adolescentes
  en este grupo objetivo.

'

---
Con el anuncio de las autoridades sanitarias respecto al inicio de la vacunación a menores de entre 3 y 11 años, el Hospital de Niños Orlando Alassia se prepara para inocular a los niños santafesinos con factores de riesgo. Según adelantaron fuentes sanitarias a UNO solo restan detalles para que el ministerio de Salud defina al nosocomio infantil como el centro vacunatorio para menores con comorbilidades en Santa Fe.

Desde el Alassia aguardan definiciones que se podrían dar durante el miércoles de esta semana, a la espera de que se confirme al efector de Salud ubicado en barrio Roma para ser el lugar de vacunación en este grupo objetivo. En el lugar quedó dispuesta la carpa sanitaria que se había montado afuera del nosocomio al momento de largar con la vacunación de adolescentes de 12 a 17 años con comorbilidades.

Desde la cartera sanitaria habían solicitado en la reunión del Consejo Federal de Salud (Cofesa) que la inmunización de niños con factores de riesgo se realice en centros de Salud, para prevenir y actuar ante cualquier eventualidad.

En cuanto a la inmunización de niños de entre 3 y 11 años sin comorbilidades, aún no está definido si se llevará a cabo en alguno de los vacunatorios de la ciudad o si se montará un operativo de inoculación en las escuelas primarias, como es el caso de otras provincias como Buenos Aires o Córdoba.

En el sistema de turnos del registro provincial ya se anotaron 100.000 personas de entre 3 y 11 años, de una población estimada para ese rango etario de 450.000. El fin de semana estarían llegando las dosis de Sinopharm para la vacunación de niños con comorbilidades a la provincia, para lo que luego se hará la distribución a cada localidad.

En cuanto a los adolescentes de entre 12 y 17 años, alrededor de 300 mil en el distrito, Salud informó que las 27 mil personas con comorbilidades ya completaron sus esquemas con la vacuna Moderna, ya recibieron la Pfizer todos los inscriptos de 17 años y ahora se avanza con los de 16, en ambos casos sin factores de riesgo.

El Alassia, a favor de la vacunación

El director del hospital Alassia, Osvaldo González Carrillo, se posicionó a favor de la vacunación a menores. Consultado por su postura en cuanto a la recomendación de aplicarse la vacuna a menores de 11 años, el director del Alassia no dudó: "Seguro que estamos de acuerdo. Si está avalado por la Anmat por supuesto que lo recomendamos".

Luego de tener algunos reparos, el presidente de la Sociedad Argentina de Pediatría (SAP), Omar Tabacco, avaló la vacunación contra el coronavirus para niños y niñas de entre 3 y 11 años con la vacuna Sinopharm, a la que "recomendó" luego de haber accedido a la evidencia científica que disponía el Ministerio de Salud.

Factores de riesgo:

\- Diabetes tipo 1 o 2 (insulinodependiente y no insulinodependiente).

\- Obesidad grado 2 (índice de masa corporal -IMC- mayor a 35) y grado 3 (IMC mayor a 40).

\- Enfermedad cardiovascular

\- Enfermedad renal crónica

\- Enfermedad respiratoria crónica

\- Personas que viven con VIH

\- Pacientes en lista de espera para trasplante de órganos sólidos y trasplantados de órganos sólidos.

\- Personas con discapacidad residentes de hogares, residencias y pequeños hogares.

\- Pacientes oncológicos y oncohematológicos con diagnóstico reciente o enfermedad “ACTIVA” (menos de 1 año desde el diagnóstico; tratamiento actual o haber recibido tratamiento inmunosupresor en los últimos 12 meses; enfermedad en recaída o no controlada).