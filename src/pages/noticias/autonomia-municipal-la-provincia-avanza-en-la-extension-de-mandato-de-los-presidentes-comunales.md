---
category: Agenda Ciudadana
date: 2021-02-03T06:41:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/autonomía.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno Santa Fe
resumen: 'Autonomía municipal: La provincia avanza en la extensión de mandato de los
  presidentes comunales'
title: 'Autonomía municipal: La provincia avanza en la extensión de mandato de los
  presidentes comunales'
entradilla: Los ministros Corach y Sukerman encabezaron un encuentro de trabajo con
  los presidentes comunales de toda la provincia.

---
Los ministros de Gestión Pública, Marcos Corach; y de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman, encabezaron este martes una reunión mediante videoconferencia con más de 130 presidentes comunales de toda la provincia, con el objetivo de avanzar en el proyecto de extensión de mandato, en el marco de la autonomía municipal.

Al respecto, Corach detalló que “hoy empezamos a profundizar ese diálogo que inauguramos la semana pasada con el gobernador, intendentes y presidentes comunales. Creemos que tenemos que hablar con quienes son los protagonistas directos de esta modificación que impulsa el gobernador Perotti. En principio, hoy hablamos de extensión de mandato de los presidentes comunales, pero será una discusión cada vez más profunda”.

“Por eso, queremos dar certezas y pasos claros para ir profundizando el proceso de discusión con los protagonistas del proceso de autonomía que estamos impulsando. Hoy empezamos con los presidentes comunales, seguiremos con los intendentes, añadió el ministro.

Por su parte, Sukerman afirmó que “la propuesta fue muy bienvenida cuando lo anunció el gobernador. Necesitamos igualar los derechos y las formas que tienen los municipios a las comunas, distinción que figura en nuestra Constitución pero que hoy es criticada por las comunas por entender que son discriminadas”.

“Nosotros estamos revirtiendo eso. El objetivo es llevar igualdad de oportunidades a cada población de la provincia para que cada uno pueda desarrollarse en el lugar donde nació”, finalizó el ministro de Gobierno.

De la actividad participaron, además, el secretario de Integración y Fortalecimiento Institucional, José Luis Freyre; y el subsecretario de Comunas, Carlos Kaufmann.

**Voces protagonistas**

En tanto, la presidenta comunal de Carlos Pellegrini, Marina Bordigoni, manifestó su acompañamiento “tanto a que se extienda el mandato de 2 a 4 años, como al proyecto de ley de autonomía. Creo que no solamente debemos tratar de darles mayores responsabilidades, sino también de que venga acompañado de mayores recursos, que no solo tienen que salir de los tributos de cada localidad sino también de la coparticipación”.

“Es una gran iniciativa y el consenso es unánime, todos los presidentes de comunas deseamos que esto se pueda llevar a la práctica, porque dos años son muy pocos para ejecutar proyectos a largo plazo”, agregó Bordigoni.

Por su parte, el presidente comunal de General Lagos, Esteban Ferri, destacó que “tenemos la oportunidad de debatir el proyecto de ley para extender los mandatos”, lo que va a permitir “pensar en proyectos de mediano plazo para el desarrollo de nuestros territorios”; y la autonomía “es la oportunidad de tener el protagonismo que nos merecemos los territorios más chicos. Son definiciones políticas concretas que avalamos y apoyamos fuertemente”.

Por último, el presidente comunal de Centeno, Juan Guffi, celebró “la puesta en debate de la autonomía municipal; y creo que todos coincidimos que sea ya, que en 2021 tengamos mandato por cutro años, pero hay un montón de aristas que tienen que ver con el espíritu de este cambio, con la autonomía, que tenemos que analizar muy detenidamente, para ver los pro y los contra de estos nuevos mandatos. Felicitaciones por estar llevando adelante esto, hace mucho bien tener estos espacios de debate para que todos podamos opinar”, concluyó.