---
category: La Ciudad
date: 2020-12-21T14:22:17Z
thumbnail: https://assets.3dnoticias.com.ar/2112regalos.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Regalos navideños: consejos para una compra segura'
title: 'Regalos navideños: derechos de los consumidores y consejos para una compra
  segura'
entradilla: La Municipalidad de Santa Fe brinda información detallada a tener en cuenta
  a la hora de adquirir juguetes, realizar operaciones con tarjetas y compras en línea
  o a distancia, uso de garantías y posibilidad de cambios.

---
La Municipalidad de Santa Fe brinda información y consejos a tener en cuenta al momento de llevar a cabo las compras con motivo de las fiestas de Navidad y Año Nuevo.

¿Qué se debe tener en cuenta a la hora de adquirir juguetes o realizar operaciones con tarjetas? ¿Qué es importante al momento de hacer compras en línea o a distancia? ¿Cómo es el uso de las garantías y cómo se realizan los cambios en los productos? Para dar respuesta a estos interrogantes, la Dirección de Derechos y Vinculación Ciudadana da a conocer los **derechos que poseen los usuarios y consumidores al momento de realizar una compra**, para que la experiencia sea exitosa, segura y responsable.

Además, se recuerda que en caso de que los derechos de usuarios y consumidores hayan sido vulnerados, en primer lugar se deberá realizar el reclamo al comercio y/o empresa donde se haya adquirido el producto o servicio. 

En caso de que persista el inconveniente o ante cualquier inquietud, los vecinos podrán dirigirse personalmente a la Dirección de Derechos y Vinculación Ciudadana, situada en calle Salta 2840, de lunes a viernes de 7.15 a 13 horas, con turno previo. 

Además, pueden contactarse vía WhatsApp al 342 5315450, al teléfono fijo 4574119 o al correo electrónico derechosyvinculacionciudadana@santafeciudad.gov.ar

# **Compra y comprobantes de pago**

Al momento de comprar juguetes, en la caja del producto debe encontrarse la **edad indicada para su uso**, y contener el manual redactado en idioma nacional (si no lo tiene se puede pedir a la fábrica, o importador). Asimismo, es importante que la caja tenga el **sello de seguridad**, ya que el mismo garantiza que ese producto fue elaborado respetando los estándares de calidad y que no presenta peligro para la salud o la seguridad de los niños y niñas. **La etiqueta debe tener todos los datos del fabricante o importador**.

**El ticket o la factura es el comprobante de una compra y es la única constancia legal para acreditar el vínculo con el comercio**. Es también el documento necesario para hacer valer la garantía.

# **Operaciones con tarjeta**

El gobierno nacional continúa con el programa de compra con tarjetas de crédito **Ahora 12** y **Ahora 18**. El mismo cuenta con tres meses de gracia para el pago de las cuotas y el beneficio es automático: se podrá visualizar en los consumos de la tarjeta y ocupa saldo disponible, pero se comienza a pagar tres meses después de realizada la compra. **El programa tiene vigencia hasta el 31 de diciembre de 2020**.

Además, al momento de realizar compras con tarjetas de crédito se debe tener en cuenta que la financiación se encuentra integrada por el Costo Financiero Total (CFT), es decir, el costo real de un crédito que incluye, además de la tasa de interés, todos los otros cargos asociados.

El número que publicitan los vendedores en grande indica solo la tasa de interés. Sin embargo, lo que el consumidor terminará pagando por su préstamo o su compra en cuotas es lo que se indique en el CFT, aunque la tasa sea de 0%. Es que el CFT incluye la tasa de interés, pero también otros conceptos.

En el caso de Argentina, este costo está compuesto por: la tasa de interés básica que determina la cuota pura (fija o variable), gastos de evaluación del cliente que solicita el préstamo, gastos de evaluación de los solicitantes de las financiaciones, gastos de contratación de seguros (de vida, de incendio, etc.), gastos de apertura y mantenimiento de cuentas de depósitos, y los vinculados a tarjetas de crédito y/o compras asociadas a las financiaciones, más erogaciones por envío de avisos de débito y otras notificaciones.

# **Compras a distancia**

Para las compras virtuales, la Secretaría de Comercio Interior que depende del Ministerio de Desarrollo Productivo de la Nación, a través de la resolución 424/2020, dispuso la creación del **botón de arrepentimiento**, el cual debe alojarse en la página de inicio del sitio de Internet institucional de los proveedores de productos y servicios, o en la sección principal de las aplicaciones, con un link de acceso fácil y directo que deberá ocupar un lugar destacado. 

Tampoco debe dejar dudas respecto del trámite seleccionado, sin condicionamientos ni gastos a su cargo, y si ya fue recibido en el domicilio, el proveedor debe retirarlo del mismo modo.

Además, a la hora de realizar una compra a distancia, ya sea por teléfono, por correo electrónico o internet, se goza de una tutela especial que da la ley, que es la posibilidad de **deshacer la compra** y **devolver el producto** (sin indicar la causa) en 10 días corridos, desde recibido el mismo, **sin cobrar ninguna multa o cargo por ejercer este derecho**. Para ello, es necesario guardar el producto con todo el embalaje original.

Otra sugerencia a tener en cuenta al comprar por Internet es que la empresa sea Argentina o tenga sede en Argentina (o dominio **.ar** de la url). Esto es conveniente, ya que si se tiene algún problema con el producto o con la empresa, si esta es extranjera será más complejo reclamar.

# **Precios, garantías y cambios**

Todos los productos nuevos tienen garantía legal mínima de seis meses (los usados, tres meses). El vendedor puede ampliar ese plazo, nunca disminuirlo. Si se ofrece al comprador una **garantía extendida**, debe tenerse en cuenta que es un seguro que tiene costo y generalmente empieza a tener vigencia luego de que se vence la garantía legal del fabricante. Por tal razón, es necesario leer bien las condiciones y alcances del mismo.

También se debe tener en cuenta que el precio publicado debe ser respetado. Al ver un folleto o una publicidad por Internet, es recomendable guardar la constancia de esa publicación y, en caso de negarse el comercio a respetar el precio que ofertó, realizar el reclamo pertinente. Además, **si hay diferencias entre el precio ofertado en la góndola y el que se cobra en la caja, siempre deben cobrar el menor**. Asimismo, al pagar con tarjetas de crédito o débito no se pueden generar recargos (débito o crédito hasta 1 pago debe ser el mismo cobro que en efectivo).

Por otra parte, si pasadas las fiestas el comprador quiere realizar algún cambio del producto, por talle por ejemplo, el comercio no puede limitar el horario o los días para poder concretar la modificación. **Si el lugar está abierto, debe efectuar el cambio, respetando el precio que se pagó por el producto.**

  
 