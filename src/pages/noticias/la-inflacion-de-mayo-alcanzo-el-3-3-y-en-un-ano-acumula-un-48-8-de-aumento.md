---
category: Agenda Ciudadana
date: 2021-06-17T09:09:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/supermercado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: INDEC
resumen: La inflación de mayo alcanzó el 3,3% y en un año acumula un 48,8% de aumento
title: La inflación de mayo alcanzó el 3,3% y en un año acumula un 48,8% de aumento
entradilla: En lo que va del año, la inflación muestra un incremento del 21,5%. En
  la región pampeana donde está abarcada Santa Fe el aumento interanual supera el
  50%.

---
El índice de precios al consumidor (IPC-Costo de Vida) aumentó durante mayo 3,3%, por debajo del 4,1% registrado en abril pasado, informó hoy el Instituto Nacional de Estadística y Censos (Indec). Con esta suba, la inflación minorista en los primeros cinco meses del año alcanzó al 21,5% y en los últimos 12 meses acumuló un aumento del 48,8% informó el organismo.

![](https://media.unosantafe.com.ar/p/6694a1368946c0c677f2572a59001dba/adjuntos/204/imagenes/030/141/0030141430/indec-mayo-2021-inflacionpng.png?0000-00-00-00-00-00)

En la región pampeana, donde está abarcada Santa Fe, la inflación de mayo fue del 3,2%, con una variación interanual que supera el 50% de aumento (50,8% en la región). Lo que mas aumentó en 2021 con respecto al mismo período de 2020 son los alimentos, con un aumento del 49,9% en el país y un incremento del 52,5% en la región pampeana.

![](https://media.unosantafe.com.ar/p/b3b39ac5558c1fe8a11ee9bf36253231/adjuntos/204/imagenes/030/141/0030141472/indec-mayopng.png?0000-00-00-00-00-00)

Los rubros en los que más se sintió el golpe inflacionario en el país fueron la Salud y el Transporte.

Los insumos sanitarios registraron un aumento del 4,8% a nivel nacional y del 4,3% en la región pampeana, mientras que el transporte registró una suba del 6% a nivel nacional. Por su parte, en el rubro alimenticio la suba de precios fue del 3,1% en mayo en todo el país.

La suba en la división Alimentos y bebidas no alcohólicas (3,1%) fue la de mayor incidencia en gran parte de las regiones. Según el Indec, el incremento se explicó principalmente por aumentos en Aceites, grasas y manteca; Café, té, yerba y cacao; Carnes y derivados; Leche, productos lácteos y huevos; Verduras, tubérculos y legumbres; y Pan y cereales, los cuales estuvieron parcialmente compensados por la baja en Frutas.

![](https://media.unosantafe.com.ar/p/044e57f5c659668a0f51123ad381dc12/adjuntos/204/imagenes/030/141/0030141456/indec-mayo-2021png.png?0000-00-00-00-00-00)