---
category: Agenda Ciudadana
date: 2021-11-27T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/BANCOCENTRAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Decisión del BCRA castigó las acciones del principal operador turístico en
  internet
title: Decisión del BCRA castigó las acciones del principal operador turístico en
  internet
entradilla: 'Por la nueva regulación, las acciones de Despegar muestran su mayor caída
  diaria desde marzo de 2020, cuando comenzaron las restricciones y cierre de fronteras
  por el inicio de la pandemia del coronavirus.

'

---
Parte de las ventas de pasajes y alojamientos en el exterior en cuotas se paralizaron hoy tras la decisión del BCRA que afecta al turismo, mientras las acciones del gigante de internet Despegar cayeron más del 9% en Wall Street.

Por la nueva regulación, las acciones de Despegar muestran su mayor caída diaria desde marzo de 2020, cuando comenzaron las restricciones y cierre de fronteras por el inicio de la pandemia del Coronavirus.

El gigante de los viajes por internet creado en la Argentina perdió USD 130 millones de valor de mercado, pero unos USD 70 millones son por cuestiones locales.

La emisora está sufriendo la tercera baja diaria más importante de su historia, sólo superado por las fuertes ventas de la pandemia de 2020 y en una corrección en 2019.

La medida provocó un importante desconcierto entre quienes querían comprar billetes de avión con descuentos importantes  
durante la realización del Black Friday.

En el primer día de vigencia de la prohibición para vender pasajes y alojamientos en el exterior en cuotas también se frenaron las ventas de las agencias de viajes.

La medida también afectó a la gente que esperó para comprar pasajes de avión con importantes descuentos y se encontraron con la prohibición y la novedad de que el pago en cuotas fue reemplazado por un desembolso al contado rabioso.

En la edición del Black Friday 2021, las ofertas se concentran en pasajes aéreos a destinos locales e internacionales, los servicios de alojamiento y otros paquetes, que junto a promociones en artículos del hogar, indumentaria y electrodomésticos, figuran entre las más requeridas por el público consumidor.

Las agencias de turismo explicaron que el sistema de pagos solo puede discriminar por el rubro turismo, con lo que en algunos casos el freno a las cuotas podrían alcanzar las ventas a destinos  
nacionales, que se realizan por fuera del programa "Ahora 12".

Por su parte, las emisoras de tarjetas de créditos avisaron a las agencias de viaje que no pueden procesar pagos en cuotas con plásticos de ningún tipo de productos de turismo.

Según los operadores, las emisoras cortaron esos servicios porque no se quieren exponer a ser sancionados por el Banco Central.

Tras la decisión del BCRA, quienes quieren viajar al exterior deben abonar los pasajes en un sólo pago con tarjeta y cuando llega el resumen pagarlo todo, o abonar el mínimo y financiar el resto con tasas de 39% hasta 200.000 pesos.

Algunas personas deben chequear antes del pago contado el limite máximo de su tarjeta que el banco le permite a cada cliente, porque en algunos casos puede no alcanzar para cubrir el costo de un pasaje.

En esos casos podría cubrirse el pago al contado con dos tarjetas, para los que el límite bancario no permite abonarlo en un sólo plástico.

Otra alternativa es sacar un préstamo personal para cubrir el costo en dólares, pero por ese tipo de crédito la tasa supera el 70% y aumenta en forma considerable el gasto esperado para poder  
veranear en el exterior.

Las agencias también deberán analizar qué medidas toman para los clientes que ya habían comprado el pasaje en cuotas y les faltaba adquirir el hotel u otros servicios en el exterior.

Es que se puede dar el caso de gente que compró en cuotas servicios que no se pueden devolver, y ahora no pueden adquirir el pasaje aéreo, explicaron operadores.