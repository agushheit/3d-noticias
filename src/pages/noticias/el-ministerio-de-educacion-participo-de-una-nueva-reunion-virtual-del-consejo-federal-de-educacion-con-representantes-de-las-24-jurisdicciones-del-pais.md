---
category: Estado Real
date: 2021-05-23T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/EDUCACION.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Ministerio de Educación participó de una nueva reunión virtual del Consejo
  Federal de Educación con representantes de las 24 jurisdicciones del país
title: El Ministerio de Educación participó de una nueva reunión virtual del Consejo
  Federal de Educación con representantes de las 24 jurisdicciones del país
entradilla: El secretario de Educación, Víctor Debloc, asistió este sábado de manera
  virtual a la reunión convocada por el ministro de Educación de la Nación, Nicolás
  Trotta.

---
El Ministerio de Educación de Santa Fe, participó de una nueva reunión virtual del Consejo Federal de Educación con representantes de las 24 jurisdicciones del país. El objetivo del encuentro fue garantizar el fortalecimiento de todas las instancias de no presencialidad en zonas de alto riesgo y alarma epidemiológica en todos los puntos del territorio nacional.

El encuentro se dio en el marco del cumplimiento del Decreto de Necesidad y Urgencia N° 334/2021 dictado por el presidente de la Nación que prevé la suspensión de las clases presenciales en zonas de alto riesgo y alarma epidemiológica.

Con el objetivo de garantizar la continuidad pedagógica de las y los estudiantes de todos los niveles y modalidades educativas, las funcionarias y los funcionarios de educación detallaron la situación epidemiológica en sus territorios y las acciones que se están llevando adelante.

Por su parte, el secretario de Educación de nuestra provincia, Víctor Debloc, explicó la situación sanitaria y remarcó los departamentos y localidades más afectados por la pandemia. También informó sobre el programa Cuidar Escuelas que consiste en un sistema de uso docente para reportar los casos positivos que se presentan en las instituciones educativas, sobre el funcionamiento de la Línea de Acompañamiento a las Escuelas (LAE) que es un 0800 destinado a atender las consultas referidas a la situación sanitaria y por último, sobre cuestiones netamente pedagógicas como el uso de la plataforma virtual Juana Manso y la producción de la tercera edición de los cuadernos para todos los niveles de la educación obligatoria.

Respecto a la suspensión de las actividades presenciales, Debloc remarcó que “se puso en marcha el modo de la educación a distancia que en nuestra provincia tiene cuatro formas: las clases virtuales, los grupos entre docentes y alumnos a través de la plataforma WhatsApp, la entrega de material pedagógico y las consultas y tutorías escolares. Estos dos últimos estarán suspendidos en los tres días hábiles de la próxima semana porque las escuelas van a estar cerradas, pero se mantiene el trabajo a la distancia con los dos primeros formatos para las alumnas, alumnos y docentes que puedan acceder a la conectividad”.

Para poder sostener el acompañamiento del aprendizaje, el funcionario manifestó: “Vamos a trabajar en la agenda del programa Nacional Acompañar que tiene que ver con un sostenimiento en las actividades que se hacen en la escuela, en la casa y en los centros de apoyo que se puedan establecer para tener continuidad en el vínculo pedagógico con las y los estudiantes.”

Respecto a la actividad docente durante los días hábiles de la semana próxima, Debloc dijo: “Es un momento para trabajar en el calendario escolar y el documento de evaluación formativa que es un material de trabajo permanente.”

Por último, el secretario de Educación informó que desde el Consejo Federal de Educación se va a convocar a una mesa de trabajo conjunta entre las carteras de Educación y de Salud para poder acompañar las trayectorias escolares sin descuidar la salud.