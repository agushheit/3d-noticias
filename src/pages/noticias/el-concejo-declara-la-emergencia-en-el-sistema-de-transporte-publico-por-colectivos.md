---
category: La Ciudad
date: 2021-06-25T08:01:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Concejo Municipal Santa Fe
resumen: El Concejo declara la emergencia en el sistema de Transporte Público por
  Colectivos
title: El Concejo declara la emergencia en el sistema de Transporte Público por Colectivos
entradilla: El Concejo Municipal declaró por 180 días la emergencia en el sistema
  de Transporte Público por Colectivos que podría ser prorrogado. Además, se congeló
  la tarifa del boleto hasta el 30 de noviembre de 2021.

---
En la sesión de este jueves, concejales y concejalas votaron a favor de declarar la emergencia en el Transporte Público durante 180 días, tiempo en el cual el Ejecutivo Municipal tendrá que convocar a una mesa de seguimiento de la situación de emergencia.

Según la Ordenanza establecida, la mesa de seguimiento estará conformada por: 2 representantes del DEM; 2 representantes del Concejo Municipal; 1 representante del Órgano de Transporte; 2 representantes de las empresas prestatarias del servicio; 1 representante del sindicato representativo de los trabajadores del sector; representación del Ministerio de Transporte de la Nación; representación de la Secretaría de Transporte del Superior Gobierno de la Provincia de Santa Fe.

Cabe destacar, que la mesa podrá convocar a representantes de las diversas áreas del propio municipio que tengan intervención sobre la temática, así como a entidades intermedias y/o instituciones con interés en la materia como universidades y centros de investigación.

En tanto, la mesa abordará los aspectos de Sustentabilidad económica del sistema; planificación de un Sistema de transporte acorde a la situación actual y de crecimiento de la ciudad; abordaje del sistema con una mirada multimodal; y la gestión de recursos económicos nacionales y/o provinciales para el sistema.

**Declaraciones**

Al respecto, el presidente del Concejo, Leandro González, hizo uso de la palabra y destacó que “hoy tomamos decisiones importantes vinculadas al transporte público. Por un lado, damos la certeza a trabajadores y trabajadoras de congelar el boleto de colectivo hasta el 30 de noviembre. Por otro lado, declaramos la emergencia del sistema, de manera tal de tomar un lapso donde el Ejecutivo Municipal podrá convocar a una mesa de trabajo con distintos actores para discutir el sistema que viene. Hay una crisis alarmante de lo que es el transporte de colectivos, bajó realmente muchísimo el corte de boleto y eso va a resignificar la discusión con respecto a los colectivos”. “Una de las discusiones a futuro para la ciudad, más allá de esta coyuntura compleja, tiene que ver con la inter o multimodalidad para pensar la ciudad en base al colectivo, al peatón, el remis, el ciclista. Hay mucho para trabajar con el intendente Jatón, y de esta crisis tenemos una oportunidad los santafesinos”, concluyó.

En tanto, el concejal Carlos Suárez expresó que “en términos generales creo que todos entendemos que el sistema está en emergencia. Creo que el sistema viene en emergencia hace muchos años producto principalmente en la inequidad de la entrega de los subsidios nacionales pero también es cierto que desde la ciudad no hemos hecho mucho. Y en este contexto, la pandemia vino a forzarnos a buscar una solución porque sino el sistema está al borde de la muerte. Nos parecía necesario, allá por enero de este año, declarar la emergencia y discutir qué sistema de transporte queremos para la ciudad de Santa Fe”.

Seguidamente, el concejal Guillermo Jerez celebró “el buen tino del concejal Suarez de haber insistido en el proyecto formulado. Estamos a las puertas de un proceso electoral y es muy importante que esta mesa de trabajo, en estos 180 días, pueda debatir de cara a la ciudadanía, me parece que va a haber distintas propuestas de quienes quieran ocupar estas bancas, por eso me parece importante que se puedan evaluar las distintas propuestas”.

Por su parte, la concejala Laura Mondino afirmó que “después de la reunión con el subsecretario de transporte de la municipalidad, hemos visto un mapa generalizado de la situación tan crítica que hoy atraviesa el sistema de transporte y donde quedó claro que no es un tema exclusivo de la ciudad sino que es una problemática que atraviesan todas las ciudades de las provincias del país. Es un tema histórico en la ciudad tener que repensar el sistema de transporte en la ciudad porque es un sistema que hace años está en crisis y la pandemia la ha profundizado. Lo positivo de avanzar en este tipo de ordenanza es pensar en el usuario porque, en definitiva, quienes sufren cotidianamente las complicaciones del sistema de transporte son los vecinos”.

A su turno, el concejal Carlos Pereira insistió en que “hasta el 2019 había un sistema de transporte que funcionaba y hoy no existe más, lo que va a venir va a ser mucho peor, salvo que de algún lugar aparezcan una cantidad de fondos millonarios para compensar la enorme caída en la cantidad de pasajeros que a futuro nos va a dejar la pandemia. Además, a esta altura del partido perdimos flota, y nadie va a invertir en eso el año que viene. Esto no es un problema histórico sino que es un problema que se iba a dar en la medida en que empezamos a decirle a la gente que no se suba a los colectivos por miedo al contagio”.

Además, el concejal Juan José Saleme sostuvo que “siempre es una triste noticia declarar en emergencia. Pero la apoyamos para que se conforme esta mesa de trabajo que tendrá que buscar una solución de fondo y esperamos tener una propuesta concreta como un llamado a licitación, teniendo en cuenta la cuestión multimodal, y que esta licitación traiga transparencia y un mejor servicio para todos los santafesinos”.

Por último, el concejal Federico Fulini sumó su opinión al decir que “luego de que pase la pandemia tenemos que sentarnos a discutir cuestiones de fondo, tenemos que cortar con los parches, esta ciudad tiene que definir qué perfil de sistema de transporte queremos. La pandemia es una oportunidad para que todas las fuerzas políticas podamos pensar en mediano o largo plazo, no podemos seguir pensando en el corto plazo”.