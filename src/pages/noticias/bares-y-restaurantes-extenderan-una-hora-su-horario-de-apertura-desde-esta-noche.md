---
category: La Ciudad
date: 2021-02-25T06:30:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/bares.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Bares y restaurantes extenderán una hora su horario de apertura desde esta
  noche
title: Bares y restaurantes extenderán una hora su horario de apertura desde esta
  noche
entradilla: A causa de la reducción en la cantidad de contagios de Covid-19 en la
  ciudad y el resto de la provincia, las medidas restrictivas se flexibilizan cada
  vez más

---
Así lo confirmó el Ministro de Trabajo santafesino, Juan Manuel Pusineri. "Se va a extender el horario de atención nocturna en bares y restaurantes en todo el territorio de la provincia. Será una extensión de una hora respecto a la última norma vigente. De manera tal que habrá un desdoblamiento: durante la semana será hasta la 1.30 y los fines de semana y vísperas de feriados hasta las 2.30".

La normativa comienza a regir desde este mismo miércoles. Al momento de responder acerca de cuál era el motivo por el cual se tomó esta decisión, el titular de la cartera laboral dijo: "Es un pedido de los empresarios y de los trabajadores a través de sus organizaciones sindicales. Y es una decisión del Gobierno en función de los indicadores sanitarios. Si bien todavía hay margen para que existan menos casos, lo cierto es que el nivel que tenemos actualmente de contagios, permite avanzar de manera prudente, como se hizo hasta ahora con distintos rubros y actividades, en la extensión horaria para la gastronomía".