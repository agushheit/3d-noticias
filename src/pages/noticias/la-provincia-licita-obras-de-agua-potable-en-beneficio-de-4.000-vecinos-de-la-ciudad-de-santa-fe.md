---
category: La Ciudad
date: 2021-09-06T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUAPOTABLE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La provincia licita obras de agua potable en beneficio de 4.000 vecinos de
  la ciudad de Santa Fe
title: La provincia licita obras de agua potable en beneficio de 4.000 vecinos de
  la ciudad de Santa Fe
entradilla: Serán ejecutadas en los barrios Santo Domingo y Las Lomas. El plazo de
  ejecución de las obras es de 10 meses.

---
El gobierno de la provincia de Santa Fe, a través de Aguas Santafesinas, lanzó dos licitaciones públicas para incorporar y mejorar el acceso al servicio de agua potable de más de 4.000 vecinos y vecinas de los barrios Santo Domingo y Las Lomas de la ciudad de Santa Fe.

Las obras se financian a través del programa Incluir que lleva adelante el Gobierno de la Provincia y responden a históricas demandas de mejoras por parte de los habitantes de dichos barrios.

El presidente de Aguas Santafesinas, Hugo Morzán, destacó que “estas son las obras que tienen impacto directo en la sociedad. Por un lado, constituyen una respuesta al largo reclamo de los vecinos que incluso se hizo ver en los últimos tiempos en la calle. Y por otro lado atiende un derecho humano básico como es el acceso al agua potable, por ello estamos muy satisfechos por el respaldo de la gestión del gobernador Omar Perotti a estos proyectos”.

El diseño programado para ambos barrios incluye el tendido de más de 5 kilómetros de nuevas cañerías de la red de distribución de agua potable, la ejecución de cerca de 1.000 nuevas conexiones domiciliarias, la instalación de hidrantes para el purgado y los empalmes necesarios a las redes existentes.

Tiene como plazos de ejecución cinco meses en el caso de las obras correspondientes al barrio Santo Domingo y cuatro meses para las previstas en el barrio Las Lomas.

La apertura de las ofertas presentadas para ejecutar las obras tendrá lugar el jueves 16 de septiembre a las 12:00 hs. en la planta potabilizadora Santa Fe de Aguas Santafesinas, calle Gobernador Candioti 1573.

**Ampliación de filtros**

Para hacer posible la incorporación de nuevos usuarios del servicio en los barrios mencionados es necesario ampliar la capacidad de filtración de la planta potabilizadora Santa Fe.

Es por eso que además se está licitando la construcción de dos nuevas unidades filtrantes que sumarán 800 m3/hora a la producción, asegurando la calidad del servicio y la prestación a sectores de la zona Noroeste de la ciudad.

Las obras incluyen adaptaciones necesarias para vincular los nuevos filtros a la infraestructura existente, la instalación eléctrica y de todas las cañerías y válvulas necesarias para su operación, al igual que los sistemas de telecomandos y automatización de las instalaciones.

También se prevé la demolición de la sala de cloración existente y su reemplazo por una nueva de 15 metros cuadrados y el montaje de un tinglado de 27 metros cuadrados para la protección de dos tanques de almacenamiento de este insumo de 6000 litros cada uno.

En este caso las obras tienen un plazo de ejecución de 10 meses.