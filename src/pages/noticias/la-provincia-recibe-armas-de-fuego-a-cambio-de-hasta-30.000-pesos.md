---
category: Agenda Ciudadana
date: 2021-07-20T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/ARMA2jpeg.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La provincia recibe armas de fuego a cambio de hasta 30.000 pesos
title: La provincia recibe armas de fuego a cambio de hasta 30.000 pesos
entradilla: El programa "Santa Fe Libre de Armas" busca reducir la circulación de
  armas a través de la entrega anónima a cambio de entre 9.000 y 30.000 pesos.

---
El Ministerio de Seguridad de la Provincia de Santa Fe continúa con la iniciativa “Santa Fe libre de armas”, un programa para reducir la circulación de armas en el territorio provincial que consiste en la entrega anónima de armas y municiones a cambio de una compensación económica.

La implementación de este trabajo es de competencia específica de la Agencia Nacional de Materiales Controlados (Anmac) y coordinado en Santa Fe por la Agencia de Prevención de Violencia con Armas de Fuego (APVAF).

El subsecretario de la APVAF, Lautaro Sappietro, aseguró que ya recibieron 600 armas y alrededor de 12.000 municiones en su paso por Venado Tuerto, Rosario y Rafaela. “Es un número muy importante porque para nosotros grafica lo que es la colaboración de la gente en este problema de reducir la circulación de armas”, apuntó.

“El plan consiste en brindarle la posibilidad al ciudadano de que se acerquen de manera voluntaria y hagan entrega de las armas de fuego que tengan en su poder, que muchas veces nosotros sabemos que son por herencia o porque han quedado”, explicó Sappietro. “A cambio se le entrega una remuneración económica y el arma es inutilizada y posteriormente destruida”.

El intercambio

El vocero de la Agencia de Prevención de Violencia con Armas de Fuego aseguró que la entrega es anónima ya que no se registra la identidad del particular que trae el arma, ya que el objetivo es “que se acerque la mayor cantidad de gente posible y se entregue la mayor cantidad de armas posible”.

“Cuando se hace entrega del arma lo que hace el personal de Anmac, que es el organismo que hace la recepción, es chequear en sus registros si el arma no tiene algún pedido de la Justicia o no está en el marco de una causa judicial. En ese sentido lo que se hace con esa arma es dejarla aparte para ponerla a disposición de la Justicia para que siga las vías correspondientes. Muchas veces lo que hacen es denunciar el arma porque la perdieron o se la robaron y están en el marco de procesos judiciales, pero es muy difícil que estén en situaciones ilícitas más complejas. Quien comete ilícitos con el arma muy pocas veces decide descartar el arma porque es una herramienta que utiliza de manera reiterada”, explicó.

Durante esta semana el programa va a desarrollarse en la ciudad de Santa Fe de 8 a 13, en J. J. Paso 3478. Al finalizar se realiza una breve encuesta para recabar datos sociodemográficos para elaborar un informe desde Anmac sobre la proveniencia del arma. “Eso de ninguna manera releva el anonimato con el cual se promociona el programa porque a lo que apunta es a sacar de circulación armas y de otra manera nunca hubiese llegado a manos del Estado”, dijo el funcionario.

En este sentido, Sappietro informó que el público es variado y se acercan porque han heredado de abuelos o de padres que usaban hace mucho tiempo y quedan en la casa. “Son un riesgo para cualquier accidente en el hogar y también para eventuales robos que puedan recibir y que esas armas terminen en manos de la delincuencia”, agregó.

“Sobre todo en el marco de la pandemia que ha socavado económicamente a muchas familias, y la verdad que ante la posibilidad de que un fierro que tenías tirado ahí en tu casa que te genera un peligro más que otra cosa, llevarte 9.000, 20.000 o 30.000 pesos, es estimulante”, resaltó.

¿De dónde vienen las armas?

El representante de la Agencia de Prevención de Violencia con Armas de Fuego confirmó que el objetivo es reducir la circulación de armas por medio de la prevención con la ciudadanía y a través de la investigación de requisas y secuestro en la actividad policial.

Sappietro relató que este año, con el Ministerio Público de la Acusación, se logró desbaratar a una organización que trabajaba en inyectar armas del mercado legal al ilegal. “Recordemos que todas las armas en definitiva nacen legales, pero en lo que nosotros trabajamos es en ese pasaje a la ilegalidad”.

“Para dar cuenta de esto es importante el informe dando cuenta de la cantidad de armas que secuestró la policía el año pasado: 3.705 armas en un año. Habla del flujo y de la cantidad de armas que hay en circulando en nuestra sociedad y en lo que hay que trabajar”, precisó.

“Desde Control Institucional del Armamento estamos desarrollando varios protocolos y dispositivos para trabajar la pérdida y extravío de las armas de la policía, que pudimos reducir de manera abrupta el año pasado. Veníamos con un promedio, antes de iniciar la gestión, de 100 armas por año perdidas o robadas, y el año pasado lo bajamos a 40. Al mismo tiempo generamos dispositivos de control institucional, no solo del armamento sino de las municiones, porque hay que tener en cuenta la trazabilidad de las municiones”, contó.

Aclaró que el calibre 9 mm es el reglamentario de las fuerzas de seguridad pero que no es de uso exclusivo de la policía y que los particulares pueden tenerlo. “Puede haber armas que estén en los circuitos ilegales que hayan sido del armamento policial”, reconoció y aseguró que durante la gestión anterior las municiones de 9 mm que tenía la institución policial eran las mismas que cualquier particular podía comprar en la armería y que actualmente se están fabricando lotes de municiones con la grabación "PDSF" para diferenciarlas.

“En el marco de la pandemia y lo que conocemos como violencia familiar, que no existan armas en el hogar es una garantía”, expresó Sappietro y subrayó que “reducir la cantidad de armas en circulación apunta a la prevención y a la posibilidad de evitar estas situaciones de violencia”.

Afirmó que este programa constituye una acción concreta para bajar los índices de violencia porque "la posibilidad de que esas armas no caigan en el uso de la delincuencia reduce la letalidad" en robos o balaceras que en la ciudad de Rosario se producen de manera muy asidua.

“Desde el Ministerio junto a la Agencia de Investigación Criminal y con el Área de Seguridad Pública se trabaja con diferentes líneas de investigación. Trabajamos junto a Nación en Anmac con diferentes talleres que fuimos brindando a la sociedad a lo largo de estos meses”, dijo el subsecretario, ejemplificando con las charlas “Masculinidades Armadas” y “Creencias que Matan”.

“Un dato que hay que tener en cuenta es que el 98% de los legítimos usuarios en el país son hombres. Esto marca el vínculo entre la necesidad de tener un arma y varios valores que se presentan en la sociedad como reales”, dijo Sappietro. “Tener un arma para defensa personal no termina siendo efectivo y la mayoría de las veces estas armas que se compran para esto terminan en manos de la delincuencia o en accidentes domésticos. Está comprobado con estadísticas”, finalizó.