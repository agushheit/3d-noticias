---
category: La Ciudad
date: 2021-10-12T06:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/COSO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad refaccionará la terminal de colectivos y estudiará un posible
  tren urbano con Santo Tomé
title: La Municipalidad refaccionará la terminal de colectivos y estudiará un posible
  tren urbano con Santo Tomé
entradilla: 'El intendente Emilio Jatón rubricó los acuerdos con el ministro de Transporte
  de la Nación y el gobernador. '

---
Este jueves, en dependencias del Aeropuerto de Sauce Viejo, el intendente Emilio Jatón firmó dos convenios con el gobierno nacional y la provincia. El primero de los acuerdos refiere a la elaboración de estudios sobre la viabilidad técnica, económica y operativa del servicio de transporte ferroviario de pasajeros en el tramo Santa Fe-Santo Tomé. Fue rubricado por Jatón junto al ministro de Transporte de la Nación, Alexis Guerrera; el gobernador Omar Perotti; la intendenta de Santo Tomé, Daniela Qüesta; y el rector de la Universidad Nacional del Litoral, Enrique Mammarella.

Posteriormente, se firmó un convenio asistencia técnica, económica y financiera con el Ministerio de Transporte de la Nación y el Gobierno provincial. El mismo establece la financiación, por un monto de $ 104.696.874, del proyecto de refacción de la Terminal de Ómnibus de Santa Fe.

Luego del acto protocolar, el intendente recordó que ambos convenios son el resultado de las gestiones iniciadas por el municipio ante el Ministerio de Transporte de la Nación, hace tiempo. En particular, respecto de la remodelación de la Terminal “Manuel Belgrano”, rememoró que fue la Municipalidad la que presentó el proyecto para refaccionarla en su interior, su exterior y todas las zonas aledañas. Por lo tanto, se mostró esperanzado en que la licitación se realice próximamente.

Según dijo Jatón, “hace mucho tiempo que el edificio de la terminal estaba paralizado, casi como en sus inicios, y no se le realizaba ningún tipo de obra; por eso es que nosotros fuimos a buscar financiamiento a Nación”.

Consultado sobre los trabajos que se concretarán en el inmueble, adelantó que las cuestiones técnicas tienen que ver con arreglar el interior, el exterior y buena parte del entorno, no sólo para el mantenimiento de los espacios, sino también para refuncionalizarlo y modernizarlo. “Próximamente lo vamos a presentar a los vecinos y las vecinas de la ciudad de Santa Fe, pero lo importante, que era firmar este convenio para tener el financiamiento, lo concretamos hoy”, aseguró.

Por otra parte, el intendente aseguró que el primero de los convenios, rubricado junto a la UNL y la ciudad de Santo Tomé, servirá para elaborar el plan de factibilidad que determine si es posible traer otra vez el tren de cercanía. Al respecto, indicó que ese proyecto “en el que veníamos trabajando hace más de un año y medio, lo habíamos presentado a Nación y lo que viene ahora es un estudio por parte de la Universidad Nacional del Litoral que va a durar 90 días. Después de ese plazo, se plantearán cuáles son las problemáticas y cuáles son las factibilidades para que retorne ese servicio”.

“Creemos que estamos muy cerca porque hay un espíritu de la Nación y de la provincia de hacerlo. Nosotros esperaremos el estudio de la UNL para, a partir de allí, ver cuánto de realidad puede tener el tren entre Santa Fe y Santo Tomé”, concluyó.