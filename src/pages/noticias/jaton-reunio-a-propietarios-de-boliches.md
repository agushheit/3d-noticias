---
category: La Ciudad
date: 2021-09-18T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/REUNION.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón reunió a propietarios de boliches
title: Jatón reunió a propietarios de boliches
entradilla: " Los empresarios elaborarán una propuesta con el objetivo de analizar
  la posibilidad de implementación por parte de la autoridad sanitaria, con el acompañamiento
  de la Municipalidad."

---
El intendente Emilio Jatón recibió este jueves a propietarios de boliches bailables. El objetivo del encuentro fue dialogar sobre una posible vuelta a la actividad para el rubro, uno de los que aún no pudo retomar el trabajo, en el marco de la pandemia.

En la oportunidad, los empresarios plantearon la necesidad de volver a trabajar, teniendo en cuenta que llevan más de un año y medio con sus puertas cerradas. Es una de las situaciones más complejas que se presentan, debido a que el de boliches y discotecas es el único rubro que permanece inactivo.

Luego de describir la situación sanitaria actual de la capital santafesina, el intendente manifestó su preocupación por la realidad del sector. En ese sentido, los instó a presentar ante las autoridades municipales, una propuesta que permita la vuelta al trabajo.

La intención es que ambas partes puedan evaluar esa proposición y ajustarla a los requisitos de la autoridad sanitaria, que es quien determina las nuevas aperturas y su modalidad. En la misma, los propietarios de los locales explicarán en detalle cómo diagramarán la actividad para cumplir con todos los protocolos, reducir la capacidad de los locales y aplicar burbujas para evitar la transmisión.

Concluido el encuentro, el secretario general municipal, Mariano Granato, aseguró que se trató de una reunión muy productiva en la cual los empresarios pudieron expresar sus preocupaciones por el tiempo que llevan sin poder trabajar. Del mismo modo, estableció que el intendente Emilio Jatón puso todo su equipo a disposición para, a través del trabajo coordinado, encontrar una salida.

Granato recordó que desde el comienzo de las restricciones en el marco de la pandemia de Covid-19, el municipio dispuso la realización de reuniones con todos los sectores, para evaluar las particularidades de cada rubro. El secretario evaluó que esos espacios permitieron, en muchos casos, la vuelta a la actividad gracias a la elaboración de los protocolos correspondientes, ajustados a cada actividad.