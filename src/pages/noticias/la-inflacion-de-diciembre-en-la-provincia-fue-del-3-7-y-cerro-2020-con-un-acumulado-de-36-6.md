---
category: Agenda Ciudadana
date: 2021-01-19T09:00:52Z
thumbnail: https://assets.3dnoticias.com.ar/inflacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La inflación de diciembre en la provincia fue del 3,7% y cerró 2020 con un
  acumulado de 36,6%
title: La inflación de diciembre en la provincia fue del 3,7% y cerró 2020 con un
  acumulado de 36,6%
entradilla: El Índice de Precios al Consumidor (IPC) de Santa Fe, que elabora el gobierno
  de la provincia, se incrementó 3,7% en diciembre respecto del mes anterior, y acumuló
  un aumento del 36,6% desde diciembre de 2019.

---
Índice de Precios al Consumidor (IPC) de Santa Fe, que elabora el gobierno de la provincia, se incrementó 3,7% en diciembre respecto del mes anterior, y acumuló un aumento del 36,6% desde diciembre de 2019.

[>>> Mirá el informe completo](https://es.scribd.com/document/491159052/Diciembre-2020 "Índice de Precios al Consumidor")

 

![](https://assets.3dnoticias.com.ar/cuadro1.jpg)

 

Así lo informó el Instituto Provincial de Estadística y Censos (Ipec), que elabora el IPC sobre la base de nueve capítulos de una canasta que en diciembre de 2020 registró los principales aumentos en los ítems de Alimentos y Bebidas (5,5%), Atención Médica y Gastos para la Salud (5,2%).

 

![](https://assets.3dnoticias.com.ar/cuadro2.jpg)

Mientras, en el acumulado de 12 meses los principales aumentos se dieron en Esparcimiento (52,4%), Indumentaria (52,2 %) y Almentos y Bebidas (42%)

[>>> Mirá el informe completo](https://es.scribd.com/document/491159052/Diciembre-2020 "Índice de Precios al Consumidor")