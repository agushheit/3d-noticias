---
category: Agenda Ciudadana
date: 2021-12-17T06:15:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/OIL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Oil Combustibles: condena para Echegaray y absolución para Cristóbal López
  y Fabián de Sousa'
title: 'Oil Combustibles: condena para Echegaray y absolución para Cristóbal López
  y Fabián de Sousa'
entradilla: 'El TOF 3 resolvió condenar al ex titular de la AFIP a cuatro años y ocho
  meses de prisión por administración fraudulenta.

'

---
El Tribunal Oral Federal número 3 resolvió absolver a los empresarios Fabián De Sousa y Cristóbal López en el marco del juicio por evasión impositiva a través de la empresa Oil Combustibles, al tiempo que condenó por "administración fraudulenta" al ex titular de la AFIP Ricardo Echegaray.

El TOF 3 dispuso en su fallo "condenar a Echegaray a cuatro años y ocho meses de prisión e inhabilitación perpetua por administración fraudulenta en perjuicio de la administración pública".

Al leer el fallo, los magistrados revelaron que los dos empresarios quedaron absueltos, y detallaron que el 17 de marzo de 2022 se conocerán los argumentos de la decisión.

Por el caso, el fiscal Juan García Elorrio había pedido que el ex jefe de la AFIP durante el gobierno de Cristina Kirchner sea condenado a cuatro años y ocho meses de prisión por favorecer a las empresas del Grupo Indalo en la evasión del impuesto a los combustibles.

En el mismo contexto, había reclamado para los empresarios López y De Sousa la pena de cuatro años y cuatro meses de prisión, de cumplimiento efectivo.

Los dueños del grupo Indalo estuvieron en la cárcel con prisión preventiva por ese caso durante dos años.

El fiscal los acusó de "defraudación en perjuicio de la administración pública" y en ese contexto pidió que Echegaray sea condenado, además, a la inhibición perpetua para ejercer cargos públicos y al decomiso de bienes de López y De Sousa.

García Elorrio cuantificó la deuda del grupo empresario en unos 5.900 millones de pesos, unos dos mil millones menos que los ocho mil por los que fueron originalmente imputados.

Los empresarios sostienen que esa deuda estaba documentada y los planes de pago se estaban cumpliendo rigurosamente hasta que, con el cambio de autoridades de la AFIP, fueron dejados de lado para impulsar una causa penal en su contra.

Ahora, el Tribunal Oral tras dos años de juicio resolvió absolverlos y solo coincidió con el fiscal en la condena del ex titular de la AFIP.

"Ha quedado claro lo que ha pasado en estos seis años así que solo me resta agradecer a mis abogados. Entendemos que el final va a ser un final de justicia que es lo que requerimos desde el primer día. Y pedirle perdón a mis socios, a mis amigos, a los empleados, colaboradores, y obreros de Grupo Indalo que la pasaron muy mal con la incertidumbre de ver cuándo se quedaban sin trabajo, gente que por años no le pudimos actualizar sueldos por la crisis financiera del grupo", aseguró López en sus últimas palabras en el juicio antes del veredicto.

De Sousa, por su parte, dijo que es víctima de una "persecución judicial y mediática", y expresó: "Oil Combustibles era una gran empresa. Ninguna de las firmas del Grupo Indalo ocultó un solo peso".

Echegaray también negó los cargos y sostuvo en sus últimas palabras que "no hubo ningún perjuicio fiscal" para el Estado.