---
category: Agenda Ciudadana
date: 2021-03-13T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/cannabis.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Acceso al Programa de Cannabis para el cultivo controlado
title: Acceso al Programa de Cannabis para el cultivo controlado
entradilla: La nueva resolución amplía derechos y abre nuevas formas de cultivo.  Es
  requisito excluyente contar con indicación médica. Los usuarios de cannabis por
  necesidad terapéutica se podrán inscribir en el nuevo sistema.

---
El Gobierno reglamentó este viernes, a través de una resolución del Ministerio de Salud, el funcionamiento del Sistema de Registro del Programa de Cannabis (Reprocann), donde "usuarios y usuarias de cannabis medicinal" podrán inscribirse "por sí o a través de un representante y obtener autorización para cultivar, para acceder al cultivo a través una tercera persona (cultivador) o a través de una organización civil autorizada a esos efectos".

La resolución, que lleva el número 800/2021, establece además la derogación de la resolución ministerial 1537 del 21 de septiembre de 2017, que establecía una regulación más restrictiva de la ley de Uso Medicinal del Cannabis (27.350) y que fue superada a partir de la nueva reglamentación que se sancionó en noviembre de 2020.

En adelante, Reprocann "registrará a los usuarios y usuarias que acceden a la planta de Cannabis y sus derivados, como tratamiento medicinal, terapéutico y/o paliativo del dolor, a través del cultivo controlado" lo que les permitirá cultivar, o bien acceder al cultivo a través de un cultivador o de organizaciones autorizadas para este fin.

**Cómo será el sistema**

Desde Salud se especificó que el sistema registrará a las y los usuarios que cuenten con un diagnóstico e indicación médica para que puedan acceder al cultivo controlado de la planta de cannabis como tratamiento medicinal, terapéutico y/o paliativo del dolor.

La normativa establece que "los usuarios y usuarias que acceden a la planta de cannabis y sus derivados, los terceros cultivadores y los médicos tratantes deberán contar con usuario vigente en la plataforma Argentina.gob.ar", en tanto que la autorización que se obtiene a través de la inscripción tendrá vigencia de un año.

Para solicitar esta inscripción, que puede realizarse en [https://reprocann.salud.gob.ar,](https://reprocann.salud.gob.ar, "https://reprocann.salud.gob.ar,") es requisito "excluyente" contar con indicación médica de uso de cannabis y sus derivados por parte de un profesional médico.

El coordinador del Programa Nacional de Investigación sobre los Usos Medicinales del Cannabis, Marcelo Morante, explicó que “la resolución surge como respuesta al artículo 8 insuficiente de la Ley 27.350, con el fin de ampliar o proteger derechos individuales para que quien requiera un cultivo terapéutico y el acompañamiento médico lo puede hacer bajo la autorización del Ministerio de Salud de la Nación”.

En el anexo II de la resolución se autoriza el cultivo de entre una y nueve plantas florecidas, en hasta seis metros cuadrados en interior y se pueden transportar entre 1 y 6 frascos de 30 mililitros o hasta 40 gramos de flores secas.

Por otra parte, advirtieron “no confundir (interior) con el término indoor, que refiere a un cultivo bajo cobertura, el cultivo en interior no se refiere necesariamente a la cobertura sino a las condiciones controladas".

El Reprocann, tal como se crea a partir de esta resolución, está en línea con la nueva reglamentación de la Ley 27.350 aprobada por el Presidente de la Nación en noviembre del año pasado.

El Decreto 883/2020 amplía su utilización más allá de la epilepsia refractaria, creó las condiciones necesarias para permitir el autocultivo controlado y sacar de la clandestinidad a muchas familias de nuestro país; asimismo garantiza provisión para pacientes, fomenta la investigación y la producción pública y privada de aceite y otros derivados.

Por otra parte, el Ministerio de Salud junto al Instituto Nacional de Semillas (Inase) y la Administración Nacional de Medicamentos, Alimentos y Tecnología Médica (Anmat), se encuentran trabajando articuladamente para garantizar un acceso cada vez más más seguro, de calidad y equitativo para los y las argentinas que lo necesiten.

**Apoyo de las organizaciones**

La ONG Mamá Cultiva, que preside Valeria Salech, también manifestó su satisfacción por la resolución y consideró que “es un paso más en el largo camino de lucha por la libertad de la planta y nuestro derecho a la salud”.

“Esta resolución presenta desafíos y oportunidades de mejora, en especial en lo que refiere a las condiciones de acceso al cultivo y la necesidad de formación de médicas y médicos para indicar cannabis”, y destacaron “que tenemos mucho que aportar, ya que somos las organizaciones y personas que cultivamos de manera solidaria quienes venimos dando respuesta a la demanda de la sociedad por el acceso justo e inclusivo a la planta, mediante el autocultivo, el cultivo solidario y el cultivo en red”.

Salech agregó, por último, que “continuamos trabajando colectivamente por mejores legislaciones y la integración de las terapias con cannabis al sistema de salud”.

“Saludamos que finalmente se ponga en marcha el registro”, dijo a Télam Pablo Fazio, presidente de la Cámara Argentina de Cannabis (Argencann).

“Un paso más hacia adelante realizando una puesta en marcha de manera progresiva”, agregó, y destacó que “el logro es de las organizaciones y activistas que tanto militaron por esta causa”.

Fazio explicó que esta medida “va a dinamizar una serie de actividades que realizan muchos de los miembros de la cámara, como la venta de insumos, nutrientes para el cultivo, sistemas de iluminación, carpas para indoor, etc”.

Y añadió que “desde el punto de vista comercial las principales beneficiarias de la legalización de la actividad serán los cientos de growshops, que son el principal canal de venta de dichos productos”.

A estos comentarios proactivos por la creación del Reprocann, Sebastián Basalo, director de la revista THC, dedicada a noticias y cultura canábica celebró que "finalmente el gobierno haya aclarado que en la nueva reglamentación del autocultivo con fines medicinales se podrá plantar al aire libre, ya que restringir el autocultivo medicinal a la modalidad "indoor" hubiera sido poco realista, injusto y muy preocupante, ya que la mayor parte de los usuarios medicinales cultiva en exterior y hubiera seguido siendo criminalizada u obligada a recurrir al mercado negro”.

“Es una excelente noticia que el gobierno nacional haya puesto en marcha el registro para que las personas que cultivan cannabis medicinal para sí mismas o para otros usuarios medicinales estén protegidas legalmente, no solo en lo que hace a su cultivo sino también a la tenencia de cannabis en la vía pública", explicó.

Por último, agregó que será "clave que el gobierno resguarde la protección de los datos privados de las personas que se inscriban en ese registro”.