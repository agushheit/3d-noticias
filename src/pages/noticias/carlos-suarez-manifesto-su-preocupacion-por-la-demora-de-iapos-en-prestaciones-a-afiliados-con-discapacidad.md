---
category: La Ciudad
date: 2021-01-20T09:16:30Z
thumbnail: https://assets.3dnoticias.com.ar/iapos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Carlos Suarez
resumen: Carlos Suárez manifestó su preocupación por la demora de IAPOS en prestaciones
  a afiliados con discapacidad
title: Carlos Suárez manifestó su preocupación por la demora de IAPOS en prestaciones
  a afiliados con discapacidad
entradilla: El concejal de UCR-Juntos por el Cambio se sumó al reclamo que realizan
  las asociaciones civiles, madres y familiares de niños y adultos con discapacidad.

---
“Desde el comienzo de la pandemia venimos advirtiendo distintas problemáticas en las coberturas de medicamentos y autorizaciones que no se cumplen en tiempo y forma por parte  de la obra social”, señaló.

El concejal de UCR-Juntos por el Cambio Carlos Suárez, se sumó al reclamo de padres, madres y familiares de personas con discapacidad, quienes denuncian demoras en las prestaciones de la obra social IAPOS para la cobertura de tratamientos, medicamentos o materiales. 

“Estamos hablando de personas con discapacidad, encefalopatías crónicas y enfermedades degenerativas que necesitan asistencia de manera continua. Por eso acompañamos el reclamos de las asociaciones civiles, madres y familiares, porque entendemos que no pueden admitirse demoras en los servicios médicos que necesitan para tener una mejor calidad de vida”, afirmó el edil.

El reclamo fue realizado este martes en la sede de calle San Martín de la obra social IAPOS y contó con la presencia de integrantes de la asociación civil Mamás Cannabis Medicinal (MACAME), asociación de Equinoterapia Gobernador Crespo, asociación Madres Cabronas, Asociación Santafesina del Enfermo de Fibromialgia (ASEF), Fundación por las cardiopatías congénitas- Santa Fe,  asociación "Yo te incluyo" por los derechos de las personas con Discapacidad -Laguna Paiva, Grupo Lázaro Esclerosis Múltiple, Familias Unidas Especiales (FAE), Foro Metropolitano y Protección de los Derechos de las Personas con Discapacidad.

“Esta situación se viene dando desde el inicio de la pandemia y eso motivó que presentemos distintas iniciativas en el Concejo Municipal, para solicitar la urgente normalización de las prestaciones”, recordó el concejal y agregó: “Nos parece importante que esto se resuelva de manera urgente y que no se pongan trabas en las autorizaciones, porque de las demoras dependen vidas”, afirmó Carlos Suárez.