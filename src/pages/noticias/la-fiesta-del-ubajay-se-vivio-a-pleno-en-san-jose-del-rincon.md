---
category: La Ciudad
date: 2021-10-18T06:15:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/UBAJAY.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La Fiesta del Ubajay se vivió a pleno en San José del Rincón
title: La Fiesta del Ubajay se vivió a pleno en San José del Rincón
entradilla: La tercera edición del evento que busca visibilizar y disfrutar del paisaje
  costero se realizó este sábado con música popular, gastronomía y emprendimientos
  regionales y actividades recreativas.

---
La Fiesta del Ubajay tuvo su tercera edición y se disfrutó este sábado en San José del Rincón. Sabores locales, música popular, actividades para disfrutar la naturaleza, emprendimientos y el acompañamiento de clubes e instituciones de la ciudad se unieron para compartir el paisaje costero, con el cierre musical de Mario Pereyra.

 La agenda de actividades fue variada e incluyó la realización de una maratón tanto para corredores como para hacer caminando, paseos en kayak, arte para las infancias, deportes de playa, plantación de especies arbóreas autóctonas y una recorrida para redescubrir la flora y fauna local.

 La música regional también tuvo su espacio con la participación del grupo de jóvenes Buzzines,la Escuela de Danzas Argentinas "Mi Terruño" del Liceo Municipal, de Toni Flores, de Reveldia Rapera y el cierre de Mario Pereyra.

 "Es una alegría, el día nos acompañó para disfrutar de la belleza de nuestra ciudad con la Fiesta del Ubajay", celebró Silvio González y afirmó que "uno de los objetivos siempre fue dejar de darle la espalda a la ciudad, a una belleza tan linda como es el arroyo Ubajay". 

 El intendente aseguró que "es una fiesta popular que buscamos que se transforme en una oferta turística de nuestra ciudad, que se constituya como una atracción más, junto con la gastronomía y con la historia de los artistas y de San José del Rincón". 

 Durante la jornada, González destacó la amplia convocatoria que tuvo el evento, en el que se mantuvo un protocolo en el marco de la pandemia por Covid-19. "Sigue siendo una fiesta cuidada, se promueve el consumo responsable de alcohol y evitamos la nocturnidad, además brindamos recomendaciones de no compartir elementos para evitar contagios y para cuidarnos del sol para disfrutar de la tarde con responsabilidad. El haber hecho la fiesta nos marca que en términos epidemiológicos estamos mejor y eso es una buena noticia".