---
category: La Ciudad
date: 2021-01-03T11:23:15Z
thumbnail: https://assets.3dnoticias.com.ar/festival-guadalupe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Por el aumento de contagios de coronavirus en la ciudad, se postergó el Festival
  de Guadalupe
title: Por el aumento de contagios de coronavirus en la ciudad, se postergó el Festival
  de Guadalupe
entradilla: En su lugar, se realizará una colecta solidaria.

---
La Comisión Organizadora del Festival Folklórico de Guadalupe anunció, a través de un comunicado de prensa, la decisión de **postergar el Festival** programado para los días 15, 16 y 17 de enero, en el marco de la pandemia por el coronavirus.

«La solidaridad, tan pregonada y vivida por nuestro Festival, comienza por el cuidado de la salud. En estos momentos difíciles, nuestra decisión quiere cuidarlos y cuidarnos. Si todos así lo hacemos y las condiciones sanitarias mejoran, podremos concretar la realización de nuestro querido Festival y disfrutar de un espacio de participación, un ámbito cultural, que pueda renovarnos en nuestro entusiasmo y nuestra esperanza», expresa el escrito que publicaron en su página web.

Más allá de la suspensión del Festival, se realizará, los días 6, 8, 9, 13, 15 y 16 de enero, por la tarde, una colecta de alimentos y pañales en cuatro sucursales de los supermercados Kilbel, donde encontrarán un lugar identificado con un afiche indicador.