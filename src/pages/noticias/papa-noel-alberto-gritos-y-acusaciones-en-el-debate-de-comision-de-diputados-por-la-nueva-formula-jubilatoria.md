---
category: Agenda Ciudadana
date: 2020-12-26T10:35:41Z
thumbnail: https://assets.3dnoticias.com.ar/2412debate-jubilaciones.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: "«Papa Noel Alberto»: gritos y acusaciones en Diputados por la fórmula jubilatoria"
title: "«Papá Noel Alberto»: gritos y acusaciones en el debate de comisión de Diputados
  por la nueva fórmula jubilatoria"
entradilla: La oposición llamó también «tío Alberto», en referencia al Presidente.
  El oficialista Moreau tomó la palabra, los acusó de haber «destruido» el sistema
  y se cruzó con los legisladores Lospennato, Iglesias y Medina.

---
La discusión por conseguir un dictamen para llevar al recinto el debate sobre la nueva fórmula de ajuste jubilatorio fue _in crescendo_ en los gritos a medida que promediaba la comisión.

El puntapié inicial estuvo en el discurso del opositor Luciano Laspina, quien hizo referencia que la norma que se estaba discutiendo era más «populismo» e hizo referencia a dos legisladores que en 2017 se habían comportado de manera «violenta» y hoy son «carmelitas descalzas». Básicamente hablaba de Leopoldo Moreau y Nicolás del Caño.

Al término tomó la palabra Moreau, quien tuvo un discurso encendido en donde acusó al bloque del Pro de haber «devastado» al sector de los jubilados y de que «hasta dos millones de jubilados perdieron el beneficio de los medicamentos gratuitos durante el gobierno de Cambiemos, que nosotros tuvimos que recomponer. Vaciaron el sistema previsional de la Argentina».

«Si ustedes hubieran escuchado en 2017, no habría habido 1000 gendarmes en la plaza, detenidos, ocho chicos que perdieron un ojo por balas de goma», lo que generó una fuerte respuesta de gritos del lado de la bancada del Pro. En especial de la diputada Silvia Lospennato y Fernando Iglesias. Este último tomó la palabra y, cuando estaba a punto de abrir el micrófono, el legislador Moreau se levantó y le gritó «Chau Iglesias». Luego se retiró del recinto mientras el legislador del Pro le gritaba «ojalá que no te cruces con el gordo mortero», en referencia al militante del PTS que fue buscado y apresado, luego de estar prófugo, por orden de la Justicia por disparar pirotecnia contra las fuerzas de seguridad.

Los discursos continuaron, y la oposición hizo referencia al Presidente de la Nación como el «tío Alberto» y «Papá Noel Alberto», mientras el oficialismo respondía a los gritos.

Los legisladores del Bloque del Pro, en cada oportunidad que tomaron la palabra, se refirieron de ese modo al presidente, en su rol del ajuste de las jubilaciones y pensiones vía decreto, y así lo hizo Laspina, al señalar que «a los jubilados este año les robaron entre 5% y 15% de su jubilación, y el año próximo  probablemente pierdan con la inflación, con la única excepción de septiembre, justo antes de las elecciones; va a venir el tío Alberto y te van a devolver una parte de lo que te robaron este año, otra de la que te robarán el año que viene... Es un recurso típico del populismo, y es algo que ya hicieron cuando fueron gobierno, y por eso no es una sorpresa», agregó el legislador.

Otro que hizo referencia al Presidente en esos términos fue el legislador bonaerense por el Pro Nicolás Medina quien acusó al oficialismo de «no importarles» los jubilados ni los pobres.

A la hora de referirse a la ley que se discutía, Medina, quien había invitado a sus pares del Frente de Todos a «ir al conurbano a ver cómo cobran los jubilados, vengan, los vamos a cuidar, no les va a pasar nada», señaló que iba a llamar a la ley de movilidad como la «ley Papá Noel, porque la quieren aprobar en esta época. Como dijo Laspina, la economía va a estar difícil en 2021 y antes de las elecciones va a aparecer Papá Noel Alberto con un regalito para los jubilados, pero los jubilados no son tontos, no van a salir a marchar, a tirar piedras, a agredir al congreso». En este momento se suscitó un nuevo cruce y el legislador del Pro atinó a pedir silencio haciendo un gesto de «cerrar la boca».