---
category: Agenda Ciudadana
date: 2021-10-08T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/TURISMOLARGO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Fin de semana "extralargo": 100% de ocupación en las cabañas santafesinas
  y muchas consultas por "Previaje"'
title: 'Fin de semana "extralargo": 100% de ocupación en las cabañas santafesinas
  y muchas consultas por "Previaje"'
entradilla: 'Al feriado trasladable por el 12 de octubre -que se adelantó al lunes
  11- se le agrega el viernes 8 como “puente” con fines turísticos. '

---
Llega un fin de semana extra largo para los argentinos con motivo del feriado del Día del Respeto a la Diversidad Cultural, que se conmemora el 12 de Octubre pero es “movible” y se concretará el lunes 11, y el viernes 8, que se declaró feriado con fines “turísticos”. Aunque muchos trabajan el sábado o el domingo, incluso uno o ambos feriados, según sus actividades, algunos optan por realizar unas mini – vacaciones en familia y uno de los destinos más elegidos son las cabañas santafesinas. 

 Así lo confirmó Guillermo Kees Scotta, secretario de Cabasetur - Cámara de Cabañeros y Prestadores de Servicios Turísticos de la provincia de Santa Fe -, quien aseguró que hay un cien por ciento de ocupación prevista para el finde semana “XXL” en los corredores turísticos de la Ruta Provincial 1 y de la Ruta Nacional 11, “fundamentalmente familias, en mayor porcentaje santafesinas pero también de Córdoba, Buenos Aires, Entre Ríos y Chaco”. 

 En el mismo sentido, Kees indicó que ya se están tomando reservas para Navidad y Año Nuevo y que hay “muchas consulas por el programa Previaje”. Al respecto, aclaró que los interesados deben consultar en cada complejo si califica al beneficio, ya que “no todos los cabañeros están inscriptos”. “Quienes estamos inscriptos y tributamos calificamos en Previaje y los turistas podrán gozar del 50% de devolución en billetera virtual  hasta $ 100.000, que pueden gastarlo en el año 2022”, agregó el representante del sector.

 