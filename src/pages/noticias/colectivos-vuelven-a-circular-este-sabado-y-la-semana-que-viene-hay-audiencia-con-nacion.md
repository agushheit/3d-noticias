---
category: La Ciudad
date: 2021-05-29T18:15:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Colectivos: vuelven a circular este sábado y la semana que viene hay audiencia
  con Nación'
title: 'Colectivos: vuelven a circular este sábado y la semana que viene hay audiencia
  con Nación'
entradilla: Este viernes se cumplió el segundo día de paro de choferes de corta y
  media distancia en la ciudad de Santa Fe y otras localidades del interior del país.

---
Desde las 0 del jueves dejaron de circular los colectivos urbanos e interurbanos en el área metropolitana de la ciudad de Santa Fe. Este paro decretado por la UTA a nivel nacional se replicó en otras localidades del interior del país.

El transporte público en la capital provincial actualmente atraviesa una crisis que se acentuó con la llegada del coronavirus y la aplicación de cuarentenas, tanto en 2020 como en este 2021.

Desde la Seccional Santa Fe de UTA informaron que los colectivos volverán a circular este sábado. Mientras tanto, “el martes hay audiencia en el Ministerio de Trabajo de la Nación, luego se resuelve cómo se sigue”, le dijo a este diario Osvaldo Agrafobo (secretario general del gremio).

El jueves se llevó a cabo un encuentro de modo virtual entre funcionarios del gobierno municipal, del sector empresarial y sindical. El eje de la reunión fue la asfixiante situación económica financiera. Entre los pedidos que hicieron los empresarios se encuentra la posibilidad de brindar un servicio de emergencia con reducción de coches, líneas y recorridos; la eliminación de la circulación nocturna y de domingos y feriados por 60 días; y el cese de todos los boletos gratuitos.

Quienes participaron fueron el Secretario General de la Municipalidad, Mariano Granato, y el subsecretario de Transporte y Movilidad, Lucas Crivelli; los empresarios Alejandro Rossi (Autobuses Santa Fe y Recreo SRL) y Daniela Brunatti (Ersa Urbano); y el secretario general de UTA, Osvaldo Agrafogo.

\-Con información de El Litoral