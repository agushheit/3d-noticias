---
layout: Noticia con imagen
author: .
resumen: Peligran prestaciones de Iapos
category: La Ciudad
title: Peligran las prestaciones bioquímicas para los afiliados de Iapos
entradilla: A través de un comunicado en redes sociales, la Sociedad de
  Bioquímicos de Santa Fe alerta sobre atrasos en el pago de aranceles en las
  prácticas de la obra social.
date: 2020-11-04T21:33:40.657Z
thumbnail: https://assets.3dnoticias.com.ar/ciudad-.jpg
---
Según la Federación Bioquímica de la provincia de Santa Fe, desde 2018 los prestadores bioquímicos vienen padeciendo un atraso muy significativo en los aranceles que abona Iapos, por lo que hoy se hace insostenible brindar algunos controles clínicos, y más aún cuando se tratan de patologías que requieren análisis de mediana y alta complejidad.

Para la realización de las determinaciones se requieren reactivos e insumos que en los últimos años han tenido aumentos muy altos debido a la inflación y al incremento del valor del dólar.

En estos tres años, más de 11 asociaciones y sociedades de bioquímicos de distintos departamentos de la provincia vienen reclamando a través del diálogo, una solución a esta situación cada vez más difícil de sobrellevar, y que inexorablemente redundará en una baja en la cobertura de atención a los beneficiarios del Instituto.

En gran medida por la relación con nuestros pacientes, porque los conocemos y atendemos desde hace años, hemos venido absorbiendo esta diferencia, a la que este año se han incorporado las nuevas exigencias de prevención que demanda la pandemia que sufrimos. 

Estas decisiones unilaterales del Instituto también se han manifestado en la contratación directa de las determinaciones de prácticas relacionadas con el Covid-19, circunstancia que alerta sobre la pérdida de la libre elección profesional.

Reiteramos a la población en general nuestra disconformidad con la situación descripta, que vulnera e impide nuestra normal y acostumbrada capacidad de respuesta a los beneficiarios de Iapos, hoy más necesaria que nunca, e insistimos en que sean considerados prontamente los reclamos efectuados, a fin de evitar el inminente deterioro en la cobertura bioquímica.

[Noticia original](https://www.facebook.com/permalink.php?story_fbid=142656354238947&id=111177500720166)