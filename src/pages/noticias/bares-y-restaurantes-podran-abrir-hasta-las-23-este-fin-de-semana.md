---
category: La Ciudad
date: 2021-07-02T08:59:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/bares.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Bares y restaurantes podrán abrir hasta las 23 este fin de semana
title: Bares y restaurantes podrán abrir hasta las 23 este fin de semana
entradilla: El gobierno provincial emitirá una resolución para extender el horario
  y luego volver a las 19 entre el domingo y el jueves. El viernes un nuevo decreto
  lo llevará hasta las 23hs.

---
El gobierno de Santa Fe habilitará la apertura de bares y restaurantes este fin de semana hasta las 23. La medida se enmarca en las flexibilizaciones “cuidadas” a las restricciones que aún están vigentes en territorio santafesino.

El nuevo permiso de extensión horaria llegará luego de que se anuncie el regreso de las clases presenciales en el nivel secundario en los departamentos La Capital y Rosario desde este jueves.

Según La Capital, la otra actividad que podría habilitarse son los deportes náuticos, en la medida en que se puedan seguir mejorando las variables epidemiológicas.

El secretario de Comercio Interior de la provincia, Juan Marcos Aviano, adelantó a La Capital que “vamos camino en este sentido”, mientras se espera que el ministro de Gestión Pública de la provincia Marcos Corach termine de firmar la resolución en lo que queda de la jornada.

El martes a la tarde se produjo un encuentro virtual entre Aviano, el secretario de Turismo santafesino, Alejandro Grandinetti, y el ministro de la Producción, Daniel Costamagna, con los sectores productivos en donde se evaluó este escenario para el próximo fin de semana.

“Mantuvimos un encuentro con los gastronómicos de Rosario, donde hablamos en buenos términos y se puso sobre la mesa las medidas que se están tomando y las demandas que plantean desde el rubro. Una de ellas era el horario, por eso para este viernes y sábado será hasta las 23 y a partir del 9 de julio ver si podemos ir todos los días hasta éste horario. Pero la lógica es transitar paso a paso y punto por punto”, destacó Aviano.

Fuentes de la municipalidad de Santa Fe señalaron a UNO que están a la espera del nuevo decreto, documento que establecerá las nuevas disposiciones provinciales para que los gobiernos locales puedan implementarlas en su territorio.

En Rosario, el secretario de la Producción municipal, Sebastián Chale, también confirmó que espera la resolución del ministro Corach. “En principio sería para el fin de semana a las 23 para la gastronomía, luego el horario de cierre regresaría a las 19 para el resto de la semana que viene a partir del lunes y si todo sigue bien el próximo fin de semana figuraría en el decreto un horario más extendido”.

La medida de extensión horaria traerá aparejado la incorporación del turno noche para recibir comensales en los aforos de hasta el 30 por ciento en las superficies cubiertas y su espacios al aire libre. Según habían indicado desde el sector, en las restringidas ecuaciones económicas de la actualidad, esto significaría un ingreso adicional del 60 por ciento a las cajas recaudadoras de bares y restaurantes, que funcionan con poca llegada de clientela y en base al delivery o take away.

“Nos llamaron desde el gobierno, y nos confirmaron que este viernes y sábado la gastronomía cierra a las 23 y con el vencimiento del decreto la semana que viene, a partir del domingo hasta el jueves deberíamos respetar el horario de las 19 y con el nuevo decreto la extensión horaria hasta las 23, esto es lo que tenemos. Está confirmado en un 99 por ciento”, dijo con beneplácito el titular de la Asociación Hotelera Gastronómica de la ciudad, Carlos Mellano.