---
category: Agenda Ciudadana
date: 2021-02-18T08:13:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/amra.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa AMRA
resumen: Los médicos del sector público harán paro este jueves y viernes
title: Los médicos del sector público harán paro este jueves y viernes
entradilla: Aseguran que hay incumplimientos de los acuerdos paritarios del 2020.
  Entre ellos, atrasos en pago a contratados y problemas con las licencias. Garantizarán
  las guardias mínimas y testeos.

---
La Asociación de Médicos de la República Argentina (AMRA) Seccional Santa Fe expresó su profundo malestar ante el incumplimiento de la paritaria Ley 13.042 correspondiente al año 2020 (Acta firmada en fecha 14/01/2021), por cuanto aseguran que no se están otorgando las licencias denominadas "Descanso Excepcional COVID-1911" dispuestas en el punto 15 del acta antes referida. En efecto, a quienes se pre­sentan como reemplazantes para cubrir dicha licencia se les está exigiendo una cantidad de requisitos de imposible cumplimiento en el acotado tiempo que se tiene para poder presentarse y cubrir dicho lugar de trabajo.

Este cúmulo de exigencias hace que resulte imposible para los profesiona­les ley 9282 cumplimentarlos en debida forma imposibilitando a los efectores encontrar los reemplazos correspondientes. Exigimos la urgente aplicación del Decreto Provincial 213/20 que adhirió al DNU 260/20 (y posteriores ampliaciones) por cuanto las autorida­des sanitarias, vigente la actual emergencia, se encuentran facultadas para la contrata­ción de recurso humano de manera directa exceptuándolo, de ser necesario, tempora­riamente, del régimen de incompatibilidad.

Asimismo, de actas paritarias anteriores, se encuentran incumplidos los siguientes puntos:

\- Falta de pago a contratados, que llevan meses sin cobrar.

\- Falta de implementación del seguro de responsabilidad civil por accidentes per­sonales y vida que, según decreto provincial Nº 362/20, fuera ordenada su contra­tación con SANCOR Cooperativa de Seguros Limitada para los trabajadores mo­notributistas de salud.

"Ante esta injusta situación que están padeciendo muchos de nuestros compañeros y compañeras, y en repudio a la falta de respuesta por parte del Poder Ejecutivo, se ha resuelto un Paro general de los profesionales de la salud que trabajan en los hospitales, SAMCOS y Centros Asistenciales dependientes de la provincia a realizarse desde las 00:00hs del día jueves 18/02/2021 por el término de 48hs, sin asistencia a los lugares de trabajo y garantizándose las guardias mínimas, COVID y no COVID y el personal abo­cado al testeo del mismo".