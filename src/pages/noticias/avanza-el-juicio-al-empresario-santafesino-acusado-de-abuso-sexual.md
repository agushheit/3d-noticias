---
layout: Noticia con imagen
author: "Por: Marisa Lemos"
resumen: Avanza el juicio a Dolinsky
category: La Ciudad
title: Avanza el juicio al empresario santafesino acusado de abuso sexual
entradilla: Esta mañana se desarrollaron los alegatos de cierre en el juicio al
  empresario santafesino acusado de abuso sexual infantil.
date: 2020-11-13T17:44:50.609Z
thumbnail: https://assets.3dnoticias.com.ar/tribunales.jpg
---
Esta mañana se desarrollaron los alegatos de cierre en el juicio al empresario santafesino acusado de abuso sexual infantil. El acusado identificado como Alberto Dolinsky sostuvo que todo es una mentira del denunciante para "sacarle plata".

El juicio a Dolinsky, el empresario oriundo de nuestra cuidad acusado de abuso sexual infantil, tuvo esta mañana su última audiencia con los alegatos de cierre de las partes. La decisión del tribunal, si lo condena o lo absuelve, se conocerá el próximo martes.

La audiencia se desarrolló en la sala n°6 de los tribunales santafesinos, ante la presencia de los jueces Sergio Carraro, Rodolfo Mingarini y Rosana Carrara. La acusación estuvo representada por los fiscales Matías Broggi y Alejandra Del Río Ayala, y el abogado José Mohamad.

Los primeros en aducir fueron los fiscales. Broggi sostuvo que el ministerio público de la acusación “cumplió con su palabra la de acreditar con total certeza los hechos atribuidos” al acusado

El fiscal Matias Broggi, recordó que la menor fue involucrada por el acusado y en ese entonces la pareja de su padre, en prácticas sexuales aquella noche de verano. Cuando su papá logró separarse de esa mujer, entabló una nueva relación con otra mujer y fue en ese momento donde la niña pudo confesarle todo a ella y juego a su padre.

[](<>)Broggi también sostuvo que acreditadas las circunstancias del hecho: la menor, su padre y la pareja había viajado desde Brasil, donde viven, a fines de diciembre de 2013, y por el pacto laboral con Dolinsky se habían alojado en un hotel céntrico, ya que el acusado reside en un edificio vecino, parte del mismo complejo. Si retrocedemos un tiempo, según el testimonio de la menor, la entonces pareja de su papá la llevó a tomar un helado y se comunicó con el acusado. Cuando volvieron al hotel en la zona de las piletas le dieron de tomar whisky y la mujer se propaso un poco con la menor. Más tarde fueron al departamento de Dolinsky y cometió ese aberrante suceso.

En dicha declaración, el padre recuerda esa noche ya que cuando se despertó en plena madrugada no vio a la niña ni a su cónyuge. En ese momento salió a buscarlas por todos lados.

El fiscal alertó que esto confirma la declaración de la menor.

Cabe destacar que debido al hecho la niña de 11 años dio un giro de 360° con su cambio y comportamiento, antes del suceso era una niña alegre y extrovertida y pasó a estar continuamente deprimida y a tener problemas en su entorno escolar y con sus notas e incluso le tocó ser mamá a muy temprana edad. Por último, el fiscal sostuvo que “no existe versión de la defensa”, que sólo cuestionaron el trabajo de la Fiscalía pero que nunca lograron restituir las pruebas en contra del acusado. Sólo dijeron que es inocente de lo que se lo acusa. El fiscal solicitó que el acusado, Dolinsky sea condenado como autor del delito de abuso sexual con acceso carnal agravado por la participación de dos personas, y promoción a la corrupción de menores agravado por la edad de la víctima a 15 años de prisión, y que se le quite el beneficio de prisión domiciliaria con salidas laborales.

El abogado demandante Mohamad sostuvo que en el juicio se logró acreditar que los hechos ocurrieron, y que Dolinsky participó de ellos. Dicho esto, remarcaron que la entrevista en Cámara Gesell se realizó con todas las garantías, y con la presencia de todas las partes del proceso.

El defensor cuestionó que la Cámara Gesell se haya realizado a tres años de la ocurrencia del “hipotético hecho”, y sostuvo que la acusación no acreditó las circunstancias en las que se habría producido el abuso: “no probaron que tomaron un helado, no probaron que fueron a la pileta, ni que hayan estado en el departamento de Dolinsky; nada está probado”, insistió.

Tras los alegatos de la acusación, declaró el acusado, quien en primera instancia aclaró que no iba a aceptar ninguna pregunta de ninguna de las partes. El acusado sostuvo todo el tiempo que es inocente, que nunca estuvo en la pileta con esa mujer ni que estuvieron en su domicilio.

Para concluir, Berizzo declaró que el empresario es inocente y lo remarcó como que fue solo un objeto de una maniobra difamatoria” y que debía ser absuelto de todo los cargos de lo cual se lo acusaba.

El próximo martes será la lectura de la sentencia ya que había sido programada por motivos personales.