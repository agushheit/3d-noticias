---
category: Agenda Ciudadana
date: 2022-01-10T06:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/RITONDO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Juntos por el Cambio se ordena en Diputados: Ritondo será el presidente
  del interbloque'
title: 'Juntos por el Cambio se ordena en Diputados: Ritondo será el presidente del
  interbloque'
entradilla: 'Se prevé que la decisión oficial se tome en el encuentro que los referentes
  nacionales y los legisladores de la alianza opositora celebrarán en Santa Fe.

'

---
Luego de varios idas y vueltas, se impuso la lógica numérica y ya hay acuerdo para que Cristian Ritondo sea oficializado el mes que viene como nuevo presidente del interbloque Juntos por el Cambio en la Cámara de Diputados, en reemplazo del radical Mario Negri.

La coronación tiene fecha y lugar: ocurrirá durante el encuentro de la mesa nacional de Juntos por el Cambio con los legisladores nacionales prevista para el 18 y 19 de febrero en Santa Fe.

Luego del recambio legislativo el PRO quedó como la fuerza con mayor cantidad de miembros dentro del interbloque opositor, con 50 diputados, por encima de la UCR que contaba, antes de su división, con 45 legisladores.

En ese sentido, el partido fundado por Mauricio Macri estaba en condiciones de reclamar ese cargo, aunque no había explicitado, al menos públicamente, el anhelo de adjudicarse ese lugar. La incertidumbre dio lugar a que se especulara con la continuidad de Negri como titular del interbloque.

La fractura del bloque radical, con la escisión de 12 diputados que conformaron una bancada aparte (Evolución) selló la suerte del cordobés, quien ya había perdido terreno a partir de la dura derrota en las primarias de su provincia a manos de Luis Juez.

En un momento dado, desde el radicalismo surgió la idea de que el titular de Evolución, Rodrigo de Loredo, fuera investido como nuevo presidente del interbloque opositor.

Ese movimiento, revelaron a NA fuentes parlamentarias del radicalismo, hubiera permitido reordenar la interna de la UCR y reunificar los bloques en Diputados, ratificando a Negri como jefe de la bancada del partido centenario.

Esta suerte de solución salomónica no prosperó porque el PRO y la Coalición Cívica se negaron a que la presidencia del interbloque sea utilizada como prenda para resolver la interna de uno de los partidos políticos del frente.

De esta manera, la designación de Ritondo cayó de maduro, siguiendo un criterio estrictamente numérico, ponderando la mayoría que el PRO tiene al interior del interbloque de 116 legisladores.

El bonaerense tendrá el desafío de coordinar y administrar las diferencias en un interbloque balcanizado, integrado por 10 bancadas diferentes. Juntos por el Cambio solía estar compuesto únicamente por tres bancadas (PRO, UCR y Coalición Cívica) pero se fue reconfigurando (especialmente con el último recambio legislativo) y el nuevo mapa opositor arroja un paisaje mucho más fragmentado y complejo.

Es por esa razón que deberá lidiar con perfiles tan disímiles como Ricardo López Murphy, Margarita Stolbizer, Facundo Manes y Fernando Iglesias, por citar algunos ejemplos.

La interna radical es uno de los problemas que asoman en el horizonte, y si bien la solución no depende de Ritondo, sí tendrá que sobrellevar los efectos de esa decisión, dado que la fragmentación duplicó los interlocutores y eso puede complicar la toma de decisiones.

El gobernador jujeño y presidente del Comité Nacional de la UCR, Gerardo Morales, y el senador nacional Martín Lousteau (líder de Evolución) trabajan para llegar a marzo con la reunificación del bloque radical, pero la negociación es compleja ya que hay una disputa por cargos y lugares de representación en el bloque y en las comisiones.

La decisión de Lousteau de forzar el quiebre de la bancada radical coloca a Evolución en mejores condiciones para negociar los términos de la reunificación.

Otro de los desafíos de Ritondo, quien pertenece al sector de "las palomas" del PRO, es contener a los sectores más duros del partido amarillo, que suelen desmarcarse con frecuencia para instalar agendas no consensuadas con el resto de las fuerzas.

Días atrás, el sector del PRO que responde a Patricia Bullrich y Mauricio Macri se unió al liberal Javier Milei para presentar un proyecto de rebaja del IVA y derogación de retenciones que no contó con el respaldo del resto del interbloque.

La Coalición Cívica, a través de su presidente Maximiliano Ferraro, salió rápidamente a cruzar al sector de "los halcones" del PRO, a los que acusó de "demagogia" y de hacer "populismo a la inversa".

En la dinámica de relaciones parlamentarias también se juega la política de alianzas de Juntos por el Cambio hacia 2023: mientras que Patricia Bullrich apuesta a integrar a los libertarios de Milei y José Luis Espert, la UCR y la Coalición Cívica no quieren saber nada con ensanchar la coalición hacia la derecha y apuestan a consolidar un perfil de centro, alejado de los extremos ideológicos.