---
category: Estado Real
date: 2021-05-29T06:00:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/CAJA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia firmó un convenio con Anses por el financiamiento del déficit
  de la caja de jubilaciones
title: La Provincia firmó un convenio con Anses por el financiamiento del déficit
  de la caja de jubilaciones
entradilla: Luego del anticipo que recibió el gobierno de Santa Fe, correspondiente
  al ejercicio 2019, Nación transferirá el saldo en siete cuotas mensuales.

---
El gobernador Omar Perotti y la directora Ejecutiva de Anses, Fernanda Raverta, firmaron este jueves un convenio que establece el financiamiento del déficit del sistema previsional de la provincia correspondiente a al ejercicio 2019, por un monto de 11.396.488.165,92 pesos. Cabe recordar que, como anticipo, el gobierno de Santa Fe percibió 4.642.482.569,03 pesos. A partir de la rúbrica, el organismo nacional se comprometió a transferir el saldo de 6.754.005.596,89 pesos en siete cuotas mensuales.

En la oportunidad, el titular de la Casa Gris remarcó la importancia del convenio para Santa Fe y destacó “la predisposición y todo lo hecho a través de Anses en la provincia durante todo este tiempo y todas las herramientas que se han podido trabajar, agilizar y poner a disposición de cientos de santafesinos y santafesinas en plena pandemia”.

En referencia al acuerdo firmado, Perotti reconoció el trabajo conjunto de los equipos técnicos de Nación y Provincia. “Ese trabajo nos va permitiendo limpiar cosas sobre las que no tendríamos que volver a hablar el año siguiente, tenerlas claras, saber cuáles son los casos de estudio, analizarlos juntos y ya dejarlos totalmente separados”.

El mandatario provincial, sobre la importancia del convenio firmado concluyó: “Fundamentalmente sirve para consolidar la relación y la confianza en la documentación que tiene cada uno, que envía cada uno, en las bases de datos y puesta en común. Allí tenemos el mayor agradecimiento de haber podido coordinar el trabajo de esta manera”.

En el mismo sentido, Raverta destacó: “Nuestros equipos han trabajado durante mucho tiempo de manera de construir una inteligencia común para poder llegar a este día”. Y explicó: “Una tarea de inteligencia común para llegar a este número que hemos ido modificando para estar a la altura de los requerimientos de la provincia, pero llegamos a buen puerto”.

“Sabemos del enorme esfuerzo que está haciendo el gobierno provincial y el gobierno nacional de estar a la altura de este tiempo histórico, del tiempo de la pandemia, no solo en cuestiones que hacen a la vida económica de las familias que viven en la provincia, sino también lo que tiene que ver con el enorme esfuerzo en materia de cuidado de la salud”, destacó la funcionaria nacional.

Finalmente, el ministro del Interior de la Nación, Eduardo de Pedro, sintetizó: “Cuando estuvimos en Santa Fe, hace más de un mes, junto con el presidente Alberto Fernández, firmamos unos cuántos proyectos que hacen al plan estratégico del gobernador. Y desde el gobierno nacional estamos comprometidos con el desarrollo de la provincia, con su perfil productivo. Estamos comprometidos en hacer las obras que hacen falta para que Santa Fe pueda tener una red de puertos, pueda tener conectividad. Trabajamos en un plan de desarrollo para potenciar la matriz productiva de la provincia”.

De la firma participaron también el ministro de Economía de la provincia, Walter Agosto, y el director del Caja de Jubilaciones y Pensiones de Santa Fe, Humberto Giobergia.

**El convenio**

A los fines de determinar el monto a financiar, se llevó adelante un proceso de intercambio y validación de información entre la Caja de Jubilaciones y Pensiones de la provincia y la Administración Nacional de Seguridad Social (ANSES), resultando para el ejercicio 2019 una suma de 11.396.488.165,92 pesos.

Cabe destacar que, hasta el momento, la provincia ha percibido en concepto de anticipos la suma de 4.642.482.569,03 pesos, determinándose un saldo a cancelar de 6.754.005.596,89 pesos, que ANSES se compromete a transferir en siete cuotas mensuales.

El convenio se desarrolla en el marco de lo dispuesto por la Ley N°27.260 y normas reglamentarias que rigen el financiamiento por parte del organismo nacional a las cajas provinciales no transferidas a la Nación. Asimismo, el Poder Ejecutivo remitirá el acuerdo a consideración de la Legislatura provincial para su ratificación legislativa.