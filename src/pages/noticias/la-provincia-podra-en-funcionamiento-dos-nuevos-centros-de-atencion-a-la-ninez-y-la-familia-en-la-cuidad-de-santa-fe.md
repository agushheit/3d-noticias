---
category: Agenda Ciudadana
date: 2021-12-11T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/centroniñez.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia podrá en funcionamiento dos nuevos centros de atencion a la
  niñez y la familia en la cuidad de Santa Fe
title: La provincia podrá en funcionamiento dos nuevos centros de atención a la niñez
  y la familia en la cuidad de Santa Fe
entradilla: En el marco del Programa “Las Niñas y los Niños Primero, los espacios
  se gestionarán conjuntamente con la municipalidad y funcionará en los barrios Colastiné
  Norte y Abasto.

---
En el marco del Programa “Las Niñas y los Niños Primero”, la provincia de Santa Fe, a través del Ministerio de Trabajo, Empleo y Seguridad Social, pondrá en funcionamiento dos nuevos Centros de Atención a la Niñez y la Familia (CANyF) en la ciudad de Santa Fe, sumándose a los dos Centros que ya se encuentran en actividad.

Este viernes el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, y el intendente de Santa Fe, Emilio Jatón, firmaron el convenio por medio del cual se acuerda gestionar conjuntamente dos espacios físicos de cuidados y abordaje integral de niñas y niños que se encuentren en situación o riesgo de trabajo infantil.

Uno de los CANyF estará localizado barrio Abasto, y el otro en Colastiné Norte, donde los equipos del Ministerio de Trabajo han detectado situaciones de vulnerabilidad de niños y niñas que quedan en riesgo de trabajo infantil, consecuencia de la situación laboral informal o precaria en que se encuentran sus familias. Esto limitaría sus posibilidades de acceso a espacios de recreación y estimulación temprana, y provocaría graves consecuencias en su desarrollo educativo, emocional y psicofísico.

Al respecto, Pusineri explicó que la puesta en funcionamiento de los CANyF ”tiene varias finalidades: en primer lugar contar con un espacio seguro, un espacio cuidado, donde se propenda al desarrollo, sobre todo para las familias más vulnerables. En segundo lugar, con una política de género ya que la mamá de ese chico sale a trabajar, a estudiar, a buscar trabajo y necesita que el cuidado de sus chicos se lleve adelante de manera segura, cuidada, responsable”.

Asimismo, agradeció la posibilidad de trabajar de manera articulada con el municipio y remarcó el apoyo y colaboración brindados por la concejala Jorgelina Mudallel para la firma del convenio. “Se trabaja de manera conjunta: la provincia hace un aporte económico y el municipio aporta los docentes y todo lo que tiene que ver con la infraestructura. En una etapa futura, a partir del año próximo nosotros también vamos a ir colaborando con el equipamiento de estos Centros”, destacó el funcionario.

Asimismo, recordó que “en la ciudad de Santa Fe estamos trabajando con distintas instituciones y en distintos barrios. En este momento tenemos dos Centros de cuidado infantil abiertos, localizados en la zona norte y oeste”.

En tanto, la directora Provincial de Promoción del Empleo Digno, Fernanda Medina, señaló que “desde el Ministerio de Trabajo, Empleo y Seguridad Social tenemos un fuerte compromiso con la prevención y erradicación del Trabajo Infantil, por eso ya contamos con más de 30 Centros de Atención a la Niñez y la Familia, y alcanzando a más de mil niños y niñas en toda la provincia, seguiremos fortaleciendo esta política pública”.

A su turno, la concejala Mudallel adelantó que “estamos analizando la posibilidad de replicar esta iniciativa en otros barrios de la ciudad. Son aportes necesarios para una ciudad en la cual trabaja en conjunto municipalidad y provincia, que es lo que los vecinos y vecinas necesitan para resolver los problemas que tenemos”.

En el acto de firma del convenio también estuvieron presentes la secretaria de Políticas de Cuidado y Acción Social de la municipalidad, María Victoria Rey, y la concejala Laura Mondino.