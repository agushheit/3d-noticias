---
category: Agenda Ciudadana
date: 2021-12-21T06:15:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/2001.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Marchas y protestas en el centro porteño, a 20 años del estallido social
  de 2001
title: Marchas y protestas en el centro porteño, a 20 años del estallido social de
  2001
entradilla: "La circulación se volvió difícil ya que las convocatorias de los distintos
  sectores políticos se desplazaron por las principales arterias de la Capital. \n"

---
A veinte años del estallido social de diciembre de 2001, distintas organizaciones sociales de izquierda y cercanas al oficialismo realizaron este lunes marchas y homenajes a los muertos durante la represión de ese momento.

El centro porteño volvió a convertirse en una zona de difícil circulación, ya que las convocatorias de los distintos sectores políticos se desplazaron por las principales arterias de la Capital.

El punto central de encuentro fue la Plaza de Mayo, donde cayeron fallecidas cinco personas durante la represión de diciembre de 2001, sobre un total de 39 que murieron en todo el país en medio de las protestas sociales de ese momento.

Las agrupaciones de izquierda fueron las primeras en llegar a la Plaza de Mayo, tras realizar una convocatoria a las 10 para marchar desde 9 de Julio por Avenida de Mayo hasta la Casa de Gobierno.

Uno de los convocantes y dirigente del Nuevo MAS Federico Winokur, en diálogo con NA, destacó que las movilizaciones del 2001 fueron "una justa respuesta a la desocupación, el hambre, la miseria económica y salarial, el vaciamiento de las fábricas, y el corralito”.

“A 20 años de esa gesta histórica que le puso un límite a los poderosos y al FMI, las banderas de la transformación social están más vigentes que nunca”, afirmó.

Tras leer un comunicado en Plaza de Mayo, las columnas de la izquierda comenzaron a desconcentrar pasado el mediodía.

En zona del centro se complicó a nivel tránsito y en 9 de Julio y Sarmiento se produjo un incendio de gomas en el que debió intervenir personal de bomberos para apagarlo.

Luego, pasadas las 14, comenzaron a concentrarse las agrupaciones cercanas al Gobierno, como el Movimiento Evita y la UTEP, que también convocó a conmemorar la fecha en Plaza de Mayo.

"A 20 años de las históricas jornadas de lucha y resistencia social del 19 y 20 de diciembre de 2001, los movimientos populares marchamos", señaló el Evita en sus redes sociales.

Los homenajes a los fallecidos y los actos de conmemoración se repitieron en todo el país, ya que los 39 muertos por la represión policial se dieron en distintas provincias, en el marco de una profunda crisis social y política de la Argentina.