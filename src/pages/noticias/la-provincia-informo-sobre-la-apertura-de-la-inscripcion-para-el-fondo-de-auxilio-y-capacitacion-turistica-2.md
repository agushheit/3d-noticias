---
category: Estado Real
date: 2021-07-24T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/AUXILIO.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia informó sobre la apertura de la inscripción para el fondo de
  auxilio y capacitación turística 2
title: La provincia informó sobre la apertura de la inscripción para el fondo de auxilio
  y capacitación turística 2
entradilla: Las empresas del sector interesadas pueden anotarse, para recibir el beneficio,
  hasta el 31 de julio.

---
La Secretaría de Turismo del Ministerio de Producción, Ciencia y Tecnología realizó este viernes una reunión informativa con el fin de dar a conocer los detalles del Fondo de Auxilio y Capacitación Turística 2 (FACT 2), instrumento de apoyo que busca preservar a las empresas para que lleguen fortalecidas a la instancia de normalización del turismo.

El encuentro fue convocado por el secretario de Turismo, Alejandro Grandinetti, y contó con la participación del representante del Ministerio de Turismo y Deportes de la Nación, Leandro Balasini, y más de 60 referentes de instituciones y de emprendimientos hoteleros, gastronómicos y de servicios, vinculadas al turismo, quienes pudieron interiorizarse sobre las características de este beneficio y hacer las consultas pertinentes.

**FACT 2**

Consiste en un aporte de 22 mil pesos por cada trabajador, destinado a empresas y personas que tengan personal a cargo, cuya labor principal se encuentre vinculada con la actividad turística: agencias de viajes, establecimientos gastronómicos, alojamientos, servicios de transporte, parques nacionales y temáticos, entre otros.

Como contraprestación, contempla la capacitación de los trabajadores para mejorar sus competencias laborales en cursos del Programa de Formación Virtual del Ministerio de Turismo y Deportes.

La inscripción está abierta hasta el 31 de julio del corriente año.

Para más información ingresar en [https://www.argentina.gob.ar/turismoydeportes/fact2](https://www.argentina.gob.ar/turismoydeportes/fact2 "https://www.argentina.gob.ar/turismoydeportes/fact2")

En el caso de consultas, pueden realizarse al correo electrónico: consultasfact2@turdep.gob.ar