---
category: Agenda Ciudadana
date: 2021-05-16T06:15:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/EMERGENCIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno espera un trámite legislativo favorable por la ley "Emergencia
  Covid"
title: El Gobierno espera un trámite legislativo favorable por la ley "Emergencia
  Covid"
entradilla: Prevén una prórroga del Decreto de Necesidad y Urgencia que estableció
  nuevas restricciones hace tres semanas y vence este viernes 21 de mayo.

---
El Gobierno nacional aguarda un trámite legislativo "favorable" para el proyecto de ley que establece criterios básicos epidemiológicos y las acciones a tomar ante cada escenario para combatir la segunda ola de coronavirus, mientras prevé una prórroga del Decreto de Necesidad y Urgencia que estableció nuevas restricciones hace tres semanas y vence el viernes 21 de mayo.

"Esperamos aprobarla", dijo esperanzado una figura de máxima relevancia del Gobierno nacional, minutos después de que el avión presidencial abandonara este viernes tierra europea con rumbo a Buenos Aires.

Se refería así al proyecto de ley enviado esta semana por el Poder Ejecutivo al Congreso de la Nación que establece criterios básicos epidemiológicos que mitiguen la pandemia y que será debatido la semana próxima en el recinto, en una sesión especial luego de haber obtenido el miércoles último dictamen en un plenario de las comisiones de Asuntos Constitucionales y de Salud del Senado.

La intención del oficialismo es que, una vez que el proyecto sea aprobado en el Senado, se gire rápidamente a Diputados para lograr una veloz sanción, ya que debería comenzar a regir antes del 22 de mayo, cuando pierde vigencia el último decreto presidencial que estableció nuevas restricciones para todo el país en las últimas tres semanas.

En tanto, en la Casa Rosada, un funcionario de la órbita presidencial señalaba, en diálogo con Télam, que "seguramente" será prorrogado el DNU presidencial que vence el próximo viernes 21.

Sin embargo, las fuentes aclararon que el presidente Alberto Fernández tiene, ante la demora parlamentaria, el poder para "sólo prorrogar" o dictar una nueva medida de emergencia que contemple la cambiante situación epidemiológica nacional.

Por ejemplo, las acciones adoptadas desde mediados de abril y principios de mayo hicieron bajar la incidencia de casos en el Área Metropolitana de Buenos Aires (AMBA), pero los registros actuales la mantendrían aun dentro de la situación de "alarma sanitaria".

Mientras tanto, en los últimos días, algunas provincias -entre ellas La Pampa, Catamarca y Neuquén, que ayer anunció nuevas restricciones- empeoraron su situación epidemiológica por el crecimiento de casos.

"Es esperable que se revisen las distintas situaciones sanitarias de acuerdo a los mecanismos de estratificación de riesgo que ya proponía el anterior DNU y que el propio Gobierno incluyó en la Ley", afirmó otro vocero gubernamental.

**Emergencia Covid**

Respecto del paso legislativo de la iniciativa de "Emergencia Covid", en Balcarce 50 dicen esperar un tratamiento "respetuoso" como el que se vio en las comisiones del Senado y aclaran que están "abiertos" a discutir los parámetros sanitarios que estratifican el riesgo, aunque descartaron que exista la posibilidad de establecer algún tipo de cláusula de "adhesión voluntaria" para las provincias.

"La Ley que prohíbe la venta de cigarrillos a menores no dice 'las provincias pueden adherir o no'. Esto es igual. Se va a discutir en el parlamento una ley que se va a aplicar de Tierra del Fuego a Jujuy", ejemplificaron los funcionarios nacionales.

El lunes último, mientras el Presidente se encontraba de gira oficial por Europa, la Casa Rosada envió un proyecto de ley al Congreso Nacional para estratificar los riesgos epidemiológicos y las acciones que se pueden tomar de acuerdo a la cantidad de casos positivos cada 100 mil habitantes o la ocupación del sistema de salud.

La iniciativa parlamentaria es, desde el punto de vista del oficialismo, la puesta en marcha de un sistema basado en "criterios objetivos" que permitirán a las autoridades jurisdiccionales y a la Presidencia saber qué medidas deben tomarse ante cada escenario epidemiológico.

La Casa Rosada tiene en el espejo retrovisor el fallo de la corte a favor de la Ciudad de Buenos Aires sobre la presencialidad en las escuelas y el actual estado de "rebeldía" del gobierno de Horacio Rodríguez Larreta en ese punto, una discusión que -para el oficialismo- quedaría saldada con la sanción de la ley.

Respecto de los planteos de inconstitucionalidad que los representantes de Juntos por el Cambio y algunos diputados de otras bancadas ya han manifestado, el Gobierno sostiene que el proyecto de ley en ningún caso propone "superpoderes" para el Presidente.

"En los lugares de 'alto riesgo y de alarma epidemiológica y sanitaria' se faculta al Ejecutivo a disponer restricciones de forma razonable y proporcionada, previa consulta con los gobernadores", defendieron la norma.

"Dicen que sancionar esta ley le da al Presidentes superpoderes. Es todo lo contrario. Ahora, con los DNU -que tienen fuerza de ley- tiene más poderes que con una ley que le dice cuándo puede actuar y de qué modo puede hacerlo", añadieron.

Además, los voceros destacaron que los criterios propuestos ya son bastante más flexibles que los adoptados, por ejemplo, en países como Alemania.

Facultada por el parlamento teutón, la canciller Ángela Merkel puede disponer -por encima de las autoridades regionales- que los colegios pasen a la enseñanza a distancia y el cierre de comercios no esenciales cuando se pasa de 165 casos cada 100 mil habitantes, un límite bastante inferior a los 500 cada 100 mil de la propuesta del Ejecutivo argentino.

Mientras el debate comienza a dar sus primeros pasos en el Congreso, el ministerio de Salud sostiene una mirada de atenta preocupación a la situación actual del Área Metropolitana de Buenos Aires y de otras provincias que muestran índices de ocupación de camas y de contagios graves.

La propia Carla Vizzotti estimó esta semana que "no se llegó a dimensionar" el riesgo de colapso sanitario que se vivió a mediados de abril en el AMBA, una situación que se logró contener pero que, de ninguna manera, puede declarar como terminada.