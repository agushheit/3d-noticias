---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Vacuna Estadounidense Pfizer
category: Agenda Ciudadana
title: Según Pfizer, su vacuna contra el Covid-19 es "eficaz en un 90%" en un
  primer análisis de la fase 3
entradilla: Luego de los primeros resultados de la fase 3, la farmacéutica
  estadounidense y la firma BioNTech anunciaron que la protección frente al
  coronavirus se logra una semana después de la segunda dosis.
date: 2020-11-10T14:45:22.760Z
thumbnail: https://assets.3dnoticias.com.ar/vacunas01.jpg
---
La farmacéutica Pfizer afirmó hoy que su vacuna contra el coronavirus es "eficaz en un 90%", según el primer análisis intermedio de su ensayo de fase 3, la última etapa antes de pedir formalmente su homologación.

Esta eficacia de protección frente al virus SARS-CoV-2 se logró siete días después de la segunda dosis de vacuna y 28 días después de la primera, indicó la farmacéutica estadounidense en un comunicado conjunto con la firma BioNTech, citado por la agencia AFP.

"Hoy es un gran día para la ciencia y la humanidad. Los primeros resultados de la fase 3 de nuestro ensayo de vacuna contra el Covid-19 proveen las pruebas iniciales de la capacidad de nuestra vacuna para prevenir" esta enfermedad, dijo el presidente de Pfizer, Albert Bourla.

"Estamos alcanzando este hito crítico en nuestro programa de desarrollo de vacunas en un momento en el que el mundo más lo necesita, con tasas de infección que establecen nuevos récords, hospitales que se acercan al límite de su capacidad y economías que luchan por reabrir. Hemos dado un paso importante y estamos más cerca de proveer a los ciudadanos del mundo" esta vacuna "necesaria para contribuir a acabar con esta crisis sanitaria mundial", añadió Bourla en un comunicado publicado en el sitio oficial de la compañía.

El análisis de los casos entre los individuos vacunados y los que recibieron el placebo indicó una tasa de eficacia de la vacuna superior al 90%, 7 días después de la segunda dosis.

"Esto significa que la protección se logra 28 días después del inicio de la vacunación, que consiste en un esquema de 2 dosis", indicó la farmacéutica.

El ensayo clínico de fase 3 de la vacuna BNT162b2 comenzó el 27 de julio y se inscribieron 43.538 participantes, 38.955 de los cuales recibieron una segunda dosis de la vacuna candidata al 8 de noviembre de 2020.

El 42% de los participantes globales -entre los que se cuentan los voluntarios en la Argentina- y el 30% de los participantes de los Estados Unidos tienen antecedentes raciales y étnicos diversos.

Según las primeras conclusiones, no se observaron problemas de seguridad graves, mientras que se evaluaron "94 casos confirmados de COVID-19 en participantes del ensayo".

La presentación de la autorización de uso de emergencia a la Administración de Drogas y Alimentos de los Estados Unidos (FDA) está prevista para "poco después de que se alcance el hito de seguridad requerido, que actualmente se espera que ocurra en la tercera semana de noviembre".

Los participantes voluntarios continuarán siendo monitoreados durante dos años luego de su segunda dosis.

El estudio también evaluará el potencial de la vacuna candidata para brindar protección contra Covid-19 en aquellos que tuvieron una exposición previa al SARS-CoV-2, así como para la prevención de la vacuna contra la enfermedad grave de coronavirus.

El ensayo clínico continuará hasta el análisis final en 164 casos confirmados para recopilar más datos.

"Además de los criterios de valoración primarios de eficacia que evalúan los casos confirmados de COVID-19 acumulados 7 días después de la segunda dosis, el análisis final incluirá, con la aprobación de la FDA, nuevos criterios de valoración secundarios que evalúan la eficacia en función de los casos acumulados 14 días después de la segunda dosis", completó el comunicado.

Basándose en proyecciones, ambas empresas afirmaron que prevén suministrar 50 millones de dosis en el mundo en 2020 y hasta 1.300 millones en 2021.