---
category: La Ciudad
date: 2021-03-05T07:05:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/peatonal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D con información Prensa Centro Comercial
resumen: Casi el 70% de los comerciantes santafesinos vendió menos que en febrero
  2020
title: Casi el 70% de los comerciantes santafesinos vendió menos que en febrero 2020
entradilla: El dato lo relevó un informe del Centro Comercial de Santa Fe, donde además
  se plasmó que solo el 14% de los comerciantes santafesinos incrementó sus ventas
  interanuales.

---
El Centro Comercial de Santa Fe elaboró un informe que releva la actividad en la ciudad de Santa Fe en forma comparativa con el mes de febrero de 2020. El dato sobresaliente fue que el 70% de los comerciantes santafesinos tuvo menos ventas que el año pasado.

Solamente un 14% de los comercios marcaron un incremento con respecto a febrero de 2020. En diálogo con LT10, el presidente del Centro Comercial de Santa Fe, Martín Salemi, indicó: "En febrero 2020 habíamos comenzado a ver un tipo de recuperación por inyección de medidas de consumo del gobierno que recién arrancaba, aunque ahora hay una gran contracción".

"Consultamos en el informe acerca de cómo ven los comerciantes la evolución de la situación. Allí tuvieron más esperanzas, ya que el 46% sostiene que se incrementarán las ventas, un 32% espera mantener su nivel de ventas y solamente un 21% habla de que sus ventas disminuirán", continuó Salemi.

En cuanto a la rentabilidad de los negocios en relación a la baja de las ventas, el presidente del Centro Comercial de Santa Fe sostuvo que "el 71,4% advierte que tiene rentabilidad positiva, mientras que solo el 3% sostiene que la rentabilidad es negativa y un 25% indica que es nula".

> "La realidad que se analiza es que es muy probable que los comerciantes hayan tenido que reorganizar sus estructuras, en una situación de resiliencia y de adaptación al cambio. La recuperación es cada vez más lenta, no sabemos si podría haber algún rebrote de contagios, estamos con expectativas positivas de que avance el sistema de vacunación y poder volver a la normalidad", destacó Salemi.

Frente a la consulta por si la venta de útiles y equipamiento escolar ayuda a un repunte de la situación, Salemi respondió: "Ayuda, lo venimos monitoreando con la respectiva vuelta de los distintos rubros. El rubro que está cerrado no genera ingresos, por lo tanto la torta es chica. Mientras más actividades estén activas repercutirá positivamente en cada uno de los comercios".

"Cuando uno lo ve aislado, uno piensa que es solamente el rubro el que no se mueve, pero atrás de eso repercute en la economía de personas que hacen mover la rueda. Por ende la salida es más rápida cuando mayor cantidad de empresas están abiertas", continuó el referente del Centro Comercial.

"Esperamos novedades, necesitamos que esto se solucione buscando la forma para que estas actividades aporten a la economía y lo venimos reclamando. Tuvimos una conferencia de prensa con salones de eventos la semana pasada, apoyándolos en el reclamo y planteando ideas o propuestas. Necesitamos arriesgar, hay que cambiar el miedo por responsabilidad", concluyó Salemi.