---
category: Estado Real
date: 2021-05-30T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/MARTORANO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Martorano: “hoy contamos con 1.200 camas críticas”'
title: 'Martorano: “hoy contamos con 1.200 camas críticas”'
entradilla: La ministra de Salud repasó la ampliación del sistema de salud santafesino
  y destacó que en la última semana se agregaron 23 plazas de terapia intensiva.

---
Ante el incremento de casos de Coronavirus, la provincia de Santa Fe, a través del Ministerio de Salud, continúa reforzando el sistema sanitario, para la atención de pacientes; a la vez que agiliza el operativo de vacunación e incrementa los puestos de testeo. De esta manera, la ministra de Salud, Sonia Martorano, informó que en el período comprendido entre marzo de 2020 y mayo de 2021 se sumaron 297 nuevas camas en las unidades de terapia intensiva (UTI), o camas críticas, sólo en el sistema público, lo que permitió, junto con el sector privado, alcanzar 1.200 camas en toda la provincia.

Al respecto, Martorano destacó que “además de las herramientas de detección y abordaje temprano y de la prevención mediante una campaña de vacunación sin precedentes, la provincia hizo una inversión histórica dotando a los efectores de la tecnología y los elementos fundamentales para dar respuesta en un momento en el que el Covid puso en crisis a todos los sistemas de salud mundiales”.

Posteriormente, precisó que en los últimos siete días se sumaron otras 23 camas críticas de las cuales 2 se destinaron a la localidad de Las Parejas; 8 a Villa Gobernador Gálvez (hospital Gamen); 5 a Villa Constitución; 6 a Reconquista; y 2 a Rafaela. “La última incorporación tiene que ver con las 8 camas en Villa Gobernador Gálvez, que son las que hoy están dando oxígeno a la región sur. Lo mismo en la zona de Venado Tuerto, donde pasamos a 30 camas. Damos ejemplos paradigmáticos, por ejemplo, en Rafaela comenzamos con 5 camas y hoy contamos con 40 camas críticas. Es decir, se ha reforzado toda la provincia y se han triplicado las camas, llegamos a 462. Y sumando las camas privadas, que también se han reforzado, hoy contamos con 1.200 camas críticas”, destacó.

Del mismo modo, ante el incremento de casos en el sur de la provincia, Martorano reafirmó: “En la región Rosario sumamos 118 camas críticas en el sistema provincial de Salud. Lo que es importante es conocer que hemos hecho un fuerte fortalecimiento de los sistemas de salud, tanto en el recurso humano como en la capacidad de camas críticas y camas generales con oxígeno. Esto es de alta importancia porque en las camas generales e intermedias se utiliza el alto flujo con cánulas y se utilizan los cascos Helmet”.

“Es histórico lo que estamos haciendo, Las Parejas y Villa Gobernador Gálvez suman salas de terapia intensiva, fundamentales para este momento, es infraestructura que potencia la atención sanitaria”, explicó la ministra.

Y destacó: “No solo se trata de invertir en más camas sino también de fortalecer la atención en cada localidad”.

**Detalle por regiones**

Para ilustrar, Sonia Martorano detalló la evolución de las unidades de terapia intensiva en algunas regiones y efectores, en donde la inversión fue sustancial y la ampliación se hizo de un modo estratégico para atender la demanda de atención y otras variables epidemiológicas.

“En Rafaela en 2019 había 5 camas críticas y hoy hay 40; en el hospital regional de Ceres no había ninguna, y hoy hay 3; mientras que en el hospital Cullen de la ciudad de Santa Fe había 20 y hoy hemos logrado ampliarlas a 55. Los hospitales Iturraspe viejo y nuevo pasaron de tener 20 a 66 camas críticas; en Venado Tuerto se pasó de 8 a 30; en Rosario el hospital Centenario pasó de 14 a 40, el Provincial de 13 a 32, por mencionar algunos casos”, remarcó la titular de la cartera sanitaria.

Otras ciudades también tuvieron un crecimiento sustancial de camas de terapia posibilitando que más santafesinos y santafesinos tengan acceso. En Armstrong, en 2019, no había ninguna cama crítica y hoy hay 8; en Villa Constitución se amplió de 5 a 17; en Villa Gobernador Gálvez, a la fecha, se sumaron 8 plazas de internación críticas, mientras que en 2019 no había ninguna, por citar algunos ejemplos.

Para cerrar, Martorano hizo hincapié en el trabajo de los equipos sanitarios y concluyó: “Estamos trabajando constantemente en esta expansión, agradezco a todo el equipo de salud porque está haciendo un esfuerzo enorme para que todas y todos los santafesinos puedan ser atendidos en tiempo y en forma”.