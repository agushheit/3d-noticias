---
category: La Ciudad
date: 2021-11-07T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/DISEÑA21.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se anuncian los seleccionados para La Diseña
title: Se anuncian los seleccionados para La Diseña
entradilla: 'Se trata de 200 diseñadoras y diseñadores de la región, elegidos según
  el procedimiento previsto en la Ordenanza N° 12.656/19, que expondrán en las ferias
  que se desarrollen en la ciudad hasta noviembre de 2022. '

---
La cantidad de expositores titulares -160- fue definida al momento de la convocatoria, en base a los protocolos vigentes para el cuidado de la salud de los asistentes, y en cuanto a las características espaciales relevadas. Dado que la situación sanitaria lo permite, se sumarán los 40 participantes que hoy conforman el listado de suplentes de la convocatoria 2021/2022.

Luego de la convocatoria realizada por la Municipalidad de Santa Fe a diseñadoras y diseñadores de la ciudad así como de Paraná y del Área Metropolitana, esta semana el jurado de La Diseña seleccionó a quienes expondrán sus producciones en las próximas muestras. Se presentaron 398 marcas de las cuales 240 participaron de las instancias obligatorias de clínicas formativas para la mejora de las producciones, siendo 40 las marcas que no han cumplido con los requisitos de admisibilidad. Así se conforman los 200 seleccionados para participar de las ferias. Cabe destacar que cerca del 40% de los seleccionados se presentó es la primera vez que va a participar de la esta feria.

Este espacio de encuentro y comercialización del diseño local, se desarrollará tanto en la feria navideña de diciembre de 2021, prevista entre el 15 y el 22 de ese mes en la Estación Belgrano, como en los eventos que se realicen en otras dependencias municipales.

La convocatoria se dió en el marco de la Ordenanza N° 12.656/19, que también prevé instancias de clínicas de formación de la mano de profesionales y especialistas del sector. Estos contenidos teórico-conceptuales se estructuraron en torno a ejes que profundizaron sobre innovación y diseño; el empleo de materiales locales; la sustentabilidad; la proyección en el territorio; la evaluación sobre el público al que está destinado y la relación con nuestra historia, cultura y sus vínculos en el presente. Este proceso de formación se desarrolló entre septiembre y octubre de este año, en modalidad virtual. Las clínicas son de acceso abierto al público y se encuentran disponibles en la [plataforma de contenidos culturales](https://www.santafeciudad.gov.ar/capitalcultural/usinadepensamiento/pensar-el-diseno/) de la Municipalidad de Santa Fe, Capital Cultural.

**Mesa de Diseño Santafesino**

Está integrada por un representante de la Secretaría de Educación y Cultura del Municipio, uno del Diseño Gráfico, uno del Diseño Industrial y uno del Diseño de Indumentaria, elegidos por dicha Secretaría. Además, un representante de Expresiva, incubadora de emprendimientos de base cultural; uno por cada una de las universidades participantes y tres representantes del Honorable Concejo Municipal. Además, participa como invitado un representante de la Secretaría de Producción y Desarrollo Económico.

La Mesa fue la encargada de elegir el jurado para esta edición, para lo cual se buscó a profesionales que a la vez puedan dar clínicas durante el proceso de evaluación. La idea de la capacitación es que sea un aporte concreto para los emprendimientos, tal como lo pide la ordenanza, debiendo este método de selección ser “instancias de clínicas de formación para la revisión y mejoramiento de las producciones”. En este marco, los criterios generales evaluados, coincidentes con los lineamientos para La Diseña 2021/22.

**Integrantes del jurado**

Jéssica Roude es diseñadora industrial y profesora en Diseño Industrial. Investigadora en la Comisión de Investigaciones Científicas (CIC), de la provincia de Buenos Aires. Profesora de Tecnología de Diseño Industrial (UNLa). Integra el equipo de docentes del Centro Interactivo de Ciencia y Tecnología “Abremate”, en el área del Laboratorio Pedagógico de Nuevas Tecnologías, TECLAB (UNLa).

Judith Savino es escritora, ceramista, diseñadora de dispositivos lúdicos, investigadora y facilitadora en pedagogía lúdica. Creadora de “Conbarro”, objetos lúdicos elaborados en arcilla del litoral santafesino. En 2016 resultó ganadora de la convocatoria de Fomento del Ministerio de Cultura con “Autito de barro”. Fue coordinadora pedagógica del Proyecto Socioeducativo El Patio del Ministerio de Educación de la provincia de Santa Fe.

Alejandro Sarmiento es diseñador industrial. Realizó trabajos para diversas empresas como Essen de Argentina, I-D Fashion Magazine de Londres, Centro Studi Alessi-Proyecto Biológico Milán, Italia. Creó el “Proyecto Pet” de reciclaje de residuos sólidos urbanos. Co-fundador del proyecto “Vacavaliente”, director creativo para el diseño de productos en cuero estructural; creador de “Satorilab” laboratorio de diseño experimental interdisciplinar, co-creador de Contenido Neto. Phaidon Press –Londres-, a través de su libro FORK, lo ha considerado como uno de los 100 diseñadores más influyentes del mundo.