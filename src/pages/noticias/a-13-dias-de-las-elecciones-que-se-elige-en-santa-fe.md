---
category: La Ciudad
date: 2021-08-31T06:15:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/BOX.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'A 13 días de las elecciones: qué se elige en Santa Fe'
title: 'A 13 días de las elecciones: qué se elige en Santa Fe'
entradilla: Ese día se realizarán las elecciones nacionales y locales de forma simultánea,
  con dos sistemas de votación diferente.

---
El 12 de septiembre se realizarán las primarias, abiertas, simultáneas y obligatorias (Paso). En esa fecha se harán de forma simultánea los comicios nacionales y locales con dos sistemas de votación diferente. Para las elecciones nacionales habrá boleta sábana y cuarto oscuro. Mientras que en las de Santa Fe se votará con boleta única y en los box de votación.

En medio de la pandemia por Covid la Justicia Electoral nacional y provincial tomaron los recaudos para minimizar las posibilidades de contagios durante las elecciones. Por ese motivo se designó un facilitador sanitario para cada local de votación y se distribuirán kits sanitarios para las autoridades de mesa. Además, entre las 10.30 y las 12.30 se dispuso un horario de prioridad para que voten las personas que integran los diferentes grupos más vulnerables.

En esta oportunidad el total de electores habilitados para votar es de 2.701.619, de los cuales 1.391.253 son mujeres y 1.310.366 son hombres. Además, hay 28.340 extranjeros empadronados que solo votarán en las elecciones locales. Mientras que en las elecciones nacionales hay 68.183 menores de entre 16 y 17 años habilitados –aunque no tienen la obligación– para votar.

A nivel nacional se renovarán 127 bancas de las 257 que tiene la Cámara de Diputados. De las principales fuerzas el Frente de Todos tiene 51 diputados a los que se les vence el mandato. Mientras que Juntos por el Cambio renueva 60 bancas.

En Santa Fe se renuevan los tres senadores (se eligen cada seis años) y nueve de los 19 diputados nacionales.

**Los senadores nacionales a los que se les vence el mandato**

**Frente de Todos**

Roberto Mirabella (reemplazó a Omar Perotti)

María de los Ángeles Sacnun

**Juntos por el Cambio**

Alejandra Vucasovich (reemplazó a Carlos Reutemann)

**Los diputados nacionales a los que se les vence el mandato**

**Frente de Todos**

Esteban Bogdanich

Josefina González

Patricia Mounier

**Juntos por el Cambio**

Luciano Laspina

Lucila Lehmann

Albor Cantard

Gonzalo del CerroGisela Scaglia

**Frente Progresista**

Luis Contigiani

Para elegir los candidatos que participarán de los comicios generales los santafesinos tendrán en el cuarto oscuro 22 boletas de senadores y 23 de diputados nacionales. En las generales, si todos los partidos y frentes consiguen pasar, como máximo habrá 14 opciones. Haciendo clic aquí se pueden ver todas las boletas.

**Qué se elige en la provincia**

De los 59 municipios, 14 eligen intendente y dos lo hacen por primera vez. Es el caso de Sauce Viejo y de San Vicente. Mientras que las 306 localidades restantes eligen a sus representantes comunales. En 148 se trata de comunas de cinco miembros (una es nueva: María Susana, del departamento Castellanos). Las comunas de tres miembros son las de 158 localidades.

En total se elegirán 14 intendentes; 210 concejales titulares y 184 suplentes; 1.214 miembros comunales titulares y la misma cantidad de suplentes; y 980 contralores de cuentas, tanto titulares como suplentes.

**Los concejales a los que se les vence el mandato en Santa Fe**

**Frente Progresista**

Leandro González

Laura Mondino

María Laura Spina

**Juntos por el Cambio**

Luciana Ceresola

Carlos Pereira

Carlos Suárez

**Partido Justicialista**

Jorgelina Mudallel

**Frente Renovador**

Sebastián Pignata

Para renovar esas ocho bancas en estas primarias competirán 42 listas de concejales de 17 fuerzas políticas. Es la mayor cantidad de nóminas en una elección desde que Santa Fe cuenta con la boleta única. Las mismas se repartirán en dos columnas de 21 filas cada una.