---
category: La Ciudad
date: 2022-01-06T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/MANZITTI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad licitó obras para la Escuela Especial “Dr. Edgardo Manzitti”
title: La Municipalidad licitó obras para la Escuela Especial “Dr. Edgardo Manzitti”
entradilla: " La acción se enmarca dentro del programa “Escuelas de mi Ciudad” que
  lleva a cabo la Municipalidad con financiamiento del FAE.\n\n"

---
En la Estación Belgrano, el intendente Emilio Jatón encabezó la apertura de sobres de la licitación pública para realizar mejoras en la fachada y arreglos de las veredas en el predio de la Escuela Especial N° 2075 “Dr. Edgardo Manzitti”. Las obras se enmarcan en el Plan “Escuelas de mi ciudad”, un programa que lleva adelante la Municipalidad, con financiamiento del Fondo de Asistencia Educativa (FAE).

Vale recordar que esta propuesta tiene como objetivo apoyar a las instituciones educativas públicas de la ciudad con aportes que van desde la compra de mobiliario y otros elementos de uso cotidiano, hasta la licitación de grandes obras de mantenimiento y reparación. En este caso, el presupuesto oficial es de $7.244.849,35 y se presentaron dos empresas interesadas en llevar a cabo estos trabajos. La primera es Prosaco Construcciones, quien propuso hacer las obras por $7.591.220; y la segunda es Alanco SA y cotizó $6.789.200.

Luego de la apertura, el intendente Emilio Jatón manifestó: “Siempre estuve apoyando a la Escuela Manzitti porque lo sentía como un deber que tenía como santafesino. Cuando uno va a esa escuela ve mucho amor, cariño y respeto, mucho trabajo por los chicos, pero el acceso al edificio no estaba preparado para ellos. Las veredas están rotas, hay vallas, ladrillos, muros, no hay barandas, no hay veredas podotáctiles ni la iluminación necesaria”.

Siguiendo esta línea, agregó: “Hoy licitamos las obras que necesitaba la escuela, muy pronto se vendrá el cambio que era esperado por los padres y los alumnos, en una institución que es muy necesaria en la ciudad de Santa Fe”.

En este sentido, destacó el trabajo conjunto con el FAE y el destino que se le da a los fondos. “Nuestra prioridad es invertir en el sistema educativo acompañando a las escuelas. Hoy estamos trabajando en conjunto con las escuelas porque son fondos de las instituciones que administramos desde la Municipalidad. Vamos a seguir detrás de cada una de ellas con FAE. Este año tenemos un buen programa de acciones para seguir mejorando las escuelas”, finalizó Jatón.

**Promesa cumplida**

Por su parte, la directora de la Escuela Especial N° 2075 “Dr. Edgardo Manzitti”, Vilma Gieco, entre lágrimas, destacó la importancia de estas obras para la institución: “Estamos muy felices porque estos trabajos son algo que venimos pidiendo hace mucho tiempo, y a través de distintas vías. Es algo que la escuela se merece”.

Luego agregó: “Como dijo el intendente, nuestros alumnos tienen muchas dificultades para llegar hasta la escuela, no contamos con veredas delimitadas, teníamos el problema de la basura y los pastizales, que hay que tratar de solucionar para hacer cada vez más accesible a la institución. Creemos que estas obras son una medida de fondo que va a dar lugar a que los papás, los transportes escolares y los alumnos que llegan de manera independiente con su bastón, puedan acceder más fácilmente al edificio escolar”.

**Seguir trabajando**

Por último, el secretario de Educación y Cultura, Paulo Ricci, contó en detalles de qué se trata esta propuesta y mencionó las obras están en marcha en el marco de este programa. “Esta es la primera apertura de sobres del 2022 del Programa “Escuelas de mi ciudad” que comenzó en la segunda mitad del año pasado, pero que ya va por su cuarta licitación para la puesta en valor de veredas y fachadas de escuelas públicas”, dijo en primer lugar.

Luego agregó: “Con esto, el Fondo de Asistencia Educativa de la Municipalidad está recuperando una presencia muy grande en la vinculación con la educación pública de la ciudad de Santa Fe, porque es su principal función y el objetivo que tiene que cumplir por ley, pero también por la indicación que nos dio Emilio de estar muy presentes acompañando la educación pública”.

También manifestó su deseo de seguir mejorando instituciones en la ciudad. “Esperamos que en los próximos meses podamos seguir licitando obras para fachadas y la puesta en valor de los entornos de las escuelas de la ciudad, porque la llegada a la escuela y su vinculación con el barrio es realmente una parte muy importante de la presencia de las instituciones. Poner en valor la vereda, la fachada, la ochava y todo el entorno también es una mejora significativa para el barrio, que lo celebra porque siente cómo mejoran las condiciones de habitabilidad de su propio entorno”.

Para cerrar, hizo una mención especial a la Escuela Manzitti: “Es una escuela histórica y muy importante de la ciudad de Santa Fe que necesitaba esta inversión, y que se suma a las tres que ya están en obra y que en pocas semanas vamos a estar inaugurando. Son obras ejecutivas, con plazos de menos de 90 días, así que ahora se empiezan a encadenar las inauguraciones de las que afortunadamente ya pudimos licitar y adjudicar, con el inicio de los trabajos en las escuelas que vamos a recuperar este año”.

**Puesta en marcha**

“Escuelas de mi ciudad” comenzó el año pasado. En este marco se iniciaron obras en la Escuela de Educación Secundaria Orientada N° 331 “Almirante G. Brown”, en el Instituto Superior de Profesorado N° 8 y en el Eempa N° 1326. También se iniciaron los trabajos en la fachada, las veredas y el entorno de la Escuela Nº 42 “General Gregorio Las Heras”, de Barrio La Esmeralda; en las fachadas y entornos del Jardín Nº 173, del Centro de Educación Física Nº 52, del CEPA Nº 49, del Cecla Nº 20, de la Escuela Nº 809 y la ESSO Nº 512, del barrio Santa Rosa de Lima.

Por otra parte, se entregó mobiliario en la Escuela Normal Superior y de Comercio N° 46 “Domingo Guzman Silva”; y en la Escuela N° 941 “General Juan Apóstol Martínez”, de La Guardia, donde también se limpiaron los tanques de agua y se hicieron las tareas de mantenimiento del césped.

En el primer año de esta iniciativa, las tareas se distribuyeron en las escuelas, jardines municipales, en el Liceo Municipal “Antonio Fuentes del Arco” y en las Estaciones. Otra línea de acción fue el mantenimiento del césped en los establecimientos, que se encaró a partir de un mapeo que realizó el municipio, que permitió organizar las tareas en 331 instituciones agrupadas en ocho zonas. También se limpiaron los tanques de agua de las escuelas públicas santafesinas que lo solicitaron a través de un formulario que permite ordenar los trabajos de acuerdo a las necesidades planteadas por los directivos.

El trabajo impulsado desde la Comisión del FAE abarca no sólo la concreción de estas obras sino también la compra de libros ilustrados para jardines, escuelas y Estaciones, a través de FAE Lectura; y la adquisición de instrumentos para incrementar los recursos del Programa de Orquestas para las Infancias “Somos Música”, que incorporó 21 violines nuevos, entregados a las niñas y niños en el último concierto de 2021.