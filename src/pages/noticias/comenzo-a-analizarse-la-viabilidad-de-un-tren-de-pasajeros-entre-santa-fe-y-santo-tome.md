---
category: La Ciudad
date: 2021-10-28T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/SANTOTREN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Comenzó a analizarse la viabilidad de un tren de pasajeros entre Santa Fe
  y Santo Tomé
title: Comenzó a analizarse la viabilidad de un tren de pasajeros entre Santa Fe y
  Santo Tomé
entradilla: El plazo de análisis técnico, social y de infraestructura es de 12 meses.
  De concretarse la iniciativa, la renovada Estación Mitre sería una de las cabeceras

---
_“Por primera vez, se está pensando un sistema articulado e integral de movilidad en el cual el ferrocarril aporta este último elemento tan importante para darnos una escala metropolitana”, indicó Javier Mendiondo, secretario de Desarrollo Urbano del municipio._

El intendente Emilio Jatón participó esta mañana, en la sede del Ente Administrador del Puerto de Santa Fe, de la primera reunión de la comisión coordinadora para dar factibilidad al Tren de pasajeros Santa Fe-Santo Tomé. El objetivo del encuentro fue establecer los canales de cooperación recíproca, brindar asistencia técnica y coordinar esfuerzos de manera conjunta entre Nación, Provincia y los municipios de Santa Fe y Santo Tomé.

Al término de la reunión, Javier Mendiondo, secretario de Desarrollo Urbano del municipio, destacó los puntos más relevantes del encuentro: “Conforme al convenio firmado hace algunos días, acaba de finalizar la primera reunión entre el Ministerio de Transporte de la Nación, la provincia y los municipios de Santa Fe y Santo Tomé, junto con la Universidad Nacional del Litoral; y de esa manera quedó conformada la Comisión Técnica de Seguimiento para la elaboración de los estudios de factibilidad de este proyecto tan anhelado por todos los santafesinos, que tiene como objetivo recuperar el tren entre la Estación Mitre y la Estación de Santo Tomé”.

Cabe señalar que la elaboración del proyecto tiene un plazo de 12 meses, en los cuales se evaluarán cuestiones de alcance técnico, ferroviario y “la cuestión social, ya que muchos terrenos donde están las vías están habitados y eso requiere un profundo análisis de la cuestión habitacional y social de esas familias”, indicó Mendiondo.

Por otra parte, el funcionario destacó que “la reunión se concretó un día antes de un hecho histórico para nuestra ciudad como es la reapertura de la Estación Mitre, con lo cual podemos decir que la Municipalidad de Santa Fe aporta desde el día cero de esta comisión de trabajo una Estación que había estado abandonada por el Estado y que ahora queda con las vías disponibles para la vuelta del ferrocarril”.

**Tren multimodal**

Cabe mencionar que el proyecto está diseñado desde un enfoque multimodal. “Sabemos que el ferrocarril es el único medio de transporte que admite cargar y llevar bicicletas”, señaló Mendiondo, y añadió que en ese contexto “desde el lado de Santa Fe analizamos que bulevar Zavalla y General López sean un nodo estratégico. En ese sentido, estamos avanzando en la Mesa de Movilidad, junto a las tres universidades locales en un convenio paralelo a este, donde estamos evaluando todo el sistema de transporte de pasajeros y de movilidad de la ciudad”.

“Podemos decir que, por primera vez, se está pensando un sistema articulado e integral de movilidad en el cual el ferrocarril aporta este último elemento tan importante para darnos una escala metropolitana”, concluyó Mendiondo.

**Agenda de trabajo**

Por su parte, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, destacó la presencia de funcionarios de trenes de la Nación, para ya poder dar los primeros pasos, donde cada una de las jurisdicciones van a fijar un representante y se realizará un protocolo de trabajo para analizar la factibilidad de la traza, que en algunos sectores están inutilizadas desde hace muchos años”.

“La idea es revitalizar el sector como una de las tantas formas de conectar la ciudad de Santa Fe con la de Santo Tomé y el área metropolitana en general”, indicó la ministra.

En tanto, el subgerente de Trenes del Interior, del Ministerio de Transporte de la Nación, Andrés Florentín, indicó que “venimos a volcar nuestra experiencia para analizar la viabilidad de este proyecto, en ese sentido, analizaremos el estado de las vías: si necesitan renovación total o no, y el material rodante a usar. En el caso de que esos puntos den cuenta de que se puede brindar un servicio óptimo para los pasajeros, ver la contratación de personal y otros temas por el estilo”.

Asimismo, asistieron a la reunión, el asesor del vicepresidente de BCyL (TAC), Leandro De Manuele; el subgerente de la Unidad de Producción de Trenes Argentinos de Cargas, Mariano Junquera; la intendenta de Santo Tomé, Daniela Qüesta; el secretario de Transporte de la provincia, Osvaldo Miatello; el director provincial de la Unidad Especial de Gestión Ferroviaria, Fernando Rosúa; el rector de la UNL, Enrique Mammarella, entre otros.