---
category: La Ciudad
date: 2021-02-20T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/REMODELACIONMUNI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Avanza la remodelación de los edificios de las Estaciones municipales
title: Avanza la remodelación de los edificios de las Estaciones municipales
entradilla: Se encuentran en distintas instancias los procesos de licitación para
  remodelar los espacios territoriales de la capital Santafesina.

---
Se encuentran en distintas instancias los procesos de licitación para remodelar los espacios territoriales de San Pantaleón, Las Lomas, San Agustín, Villa Teresa, Liceo Norte y Loyola Sur. En tanto, en barrio Los Troncos se concretará un nuevo espacio.

La Municipalidad avanza en las mejoras de los edificios donde funcionan las Estaciones, que son espacios barriales destinados al cuidado de niñas, niños y adultos mayores. Mientras que 18 de ellas continúan desarrollando actividades veraniegas para miles de niñas y niños, avanzan los procesos licitatorios para la construcción y re funcionalización de dos espacios más.

En ese marco, esta mañana se conocieron las ofertas económicas para las tareas de recuperación y reforma del edificio ubicado en el barrio San Pantaleón. Luego de la apertura de sobres, María Victoria Rey, secretaria de Políticas de Cuidado y Acción Social, destacó los ejes del programa, que se enmarca en el Plan Integrar, a través del cual la Municipalidad promueve la integración social y urbana de las distintas realidades barriales con miras a fortalecer la cohesión social.

“Las Estaciones son el nuevo modelo de las políticas sociales en el territorio implementadas por la gestión del intendente Emilio Jatón, en las cuales se desarrolla un programa de trabajo vinculado a generar políticas sociales para toda la ciudadanía”, destacó Rey, y añadió que “algunas estaciones tienen una agenda más vinculada a las infancias, otras a los adultos mayores y en otros casos a las juventudes”.

**Actividades para todas y todos**

Con respecto a las actividades que se realizan actualmente en las Estaciones, la funcionaria repasó que durante el verano se utilizan como espacios de colonia, para niñas y niños entre los 6 y 12 años. “En tanto, cuando finalicen las acciones de verano, se implementará una serie de propuestas y servicios orientadas, por un lado, a brindar servicios de cuidado a niños y niñas, generar propuestas pedagógicas, recreativas y de acompañamiento a adultos mayores. Y por otro lado, también habrá propuestas de capacitación orientadas a las juventudes”.

Vale destacar, que en las Estaciones se aplicarán los programas que allí se implementan conforme a los objetivos de las Secretarías de Integración y Economía Social, de Políticas de Cuidados y Acción Social, y de Educación y Cultura. Ofrecerán a la ciudadanía un conjunto de servicios y actividades articuladas, cuya definición se hará conforme a la lectura de las demandas y necesidades de los vecinos, actores comunitarios y redes de los barrios.

**Obras en marcha**

Este viernes hubo dos ofertas económicas para concretar la remodelación del edificio emplazado detrás del Cementerio Municipal, en el barrio San Pantaleón. Griselda Bertoni, secretaria de Obras y Espacio Público aclaró que se trata de “la apertura de sobres del segundo llamado para las obras de San Pantaleón, que será un espacio paradigmático para la ciudad, que en este momento se encuentra sin uso, en un barrio que necesita de las acciones del Estado”.

Con respecto a las licitaciones que se concretarán, la funcionaria remarcó que la segunda semana de marzo se licitarán los trabajos para poner en valor los espacios de “San Agustín y Villa Teresa y, posteriormente, las de Liceo Norte y Loyola Sur. Por otro lado, se realizarán reparaciones de mantenimiento a las sedes que están funcionando”.

Dos empresas presentaron ofertas para concretar la puesta a punto en San Pantaleón, “las cuales serán analizadas por la comisión evaluadora”, explicó Bertoni. El presupuesto oficial de la obra es de 8.073.810. Las empresas que se presentaron a la licitación fueron: la firma de José Luis Aimar, que ofertó 16.878.638 pesos; y Auge SRL, que propuso la suma de 15.888.839 pesos.

**20 Estaciones**

“Además de las Estaciones que están en marcha, en barrio Los Troncos se construirá desde cero un espacio diseñado por la Municipalidad en articulación con el gobierno nacional y provincial. En el caso de San Pantaleón, se pondrá en valor íntegramente ya que es un edificio que está en mal estado, imposible para ser usado por las familias en condiciones dignas”, repasó Rey.

El programa Estaciones se enmarca en el Plan Integrar. Funcionan en los ex Solares, Escuelas de Trabajo y Centros Integrales Comunitarios (CIC). Además de San Pantaleón, se licitarán las obras de refacción para cinco de esos espacios, ubicados en Las Lomas, San Agustín, Villa Teresa, Liceo Norte y Loyola Sur.

Las otras 13 Estaciones están Facundo Zuviría, Coronel Dorrego, Villa Hipódromo, Barranquitas, San Lorenzo, Abasto, Los Troncos, Varadero Sarsotti, Santa Rosa de Lima (Mediateca), Polideportivo de Alto Verde, La Boca, La Guardia, y Colastiné Norte. En tanto, la número 20 se hará de cero y estará ubicada también en barrio Los Troncos y formará parte de una intervención sociourbana mucho mayor, con una serie de adecuaciones infraestructurales y fondos nacionales del Promeba.