---
category: Agenda Ciudadana
date: 2021-01-15T04:12:03Z
thumbnail: https://assets.3dnoticias.com.ar/Hisopado.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: 'Habrá cambios en el protocolo sanitario: las personas con síntomas Covid-19
  irán a PCR'
title: 'Habrá cambios en el protocolo sanitario: las personas con síntomas Covid-19
  irán a PCR'
entradilla: 'Se volverá al esquema inicial: cualquier santafesino con síntomas asociados
  deberá hisoparse. '

---
El 6 de septiembre de 2020, la Ministra de Salud de la provincia, Sonia Martorano informó que en la bota santafesina se empezaba a aplicar lo que se conoce como criterio de confirmación de caso por nexo clínico-epidemiológico: esto es, que **toda persona considerada por la autoridad sanitaria como contacto estrecho de otra con coronavirus pasaba a clasificarse como positiva sin hisopado**. 

Ahora, en la provincia **se volverá al esquema inicial: cualquier santafesino con síntomas asociados a Covid-19 deberá hisoparse.**

Así se lo confirmó a El Litoral el secretario de Salud, Dr. Jorge Prieto. Si bien se va a implementar más adelante, ya se está reajustando el protocolo sanitario provincial (el cambio se está definiendo al nivel nacional). La decisión se tomó, entre otros motivos, «por el crecimiento que se logró en los laboratorios centrales, pues recordemos que al principio (de la pandemia) teníamos un solo laboratorio de procesamiento de muestras: hoy tenemos seis», en toda la bota, dijo el médico.

De estos laboratorios centrales hay cuatro en Rosario, uno en la ciudad capital (funciona en el edificio del ex Hospital Italiano) y un sexto en el Hospital «Eva Perón» de Granadero Baigorria.

«Todos los pacientes con síntomas asociados serán diagnosticados por muestra de laboratorio. Tuvimos gente que fue confirmada por nexo epidemiológico y a los 23 días desarrollaron la enfermedad. Esa persona no se aisló, porque tuvo contacto con otras personas», contó Prieto.

Este es otro punto clave: el aislamiento del paciente positivo, para no abrir la cadena de contagios hacia otras personas. En este sentido, el tiempo de aislamiento para las personas positivas y sospechosas de Covid-19 pasaría a ser de 10 días (no ya de 14; los cuatro restantes serían monitoreadas). Es otro elemento que se está evaluando.

¿Por qué? «Porque queremos hacer efectivo el aislamiento, que se cumpla, y que se cumpla correctamente. Si se indica 14 días, la gente hoy, con el verano, se muestra reticente. Entonces decimos: **10 días de aislamiento, y los restantes cuatro esas personas serán vigiladas clínicamente** y controladas con todos los recaudos, hasta completar con el seguimiento», advirtió el médico.

En el retorno a la confirmación de casos por evaluación de muestra clínica, también aparecen los tests rápidos (de antígenos), que son capaces de diagnosticar el virus en una persona desde el inicio del contagio con la misma fiabilidad que el PCR y en apenas 15-20 minutos. «Estos están indicados para las personas con síntomas, y tienen un 90% de positividad o efectividad», indicó el secretario de Salud.

<br/>

## **Antecedente**

Se considera caso confirmado por criterio epidemiológico «a todos los contactos estrechos de una persona con diagnóstico positivo de Covid en un área que tenga circulación comunitaria, y que aquel contacto estrecho cumpla con la definición de caso sospechoso», había declarado aquel 6 de septiembre la titular de la cartera sanitaria, Sonia Martorano.

**Un contacto estrecho es aquella persona que convive con un otra positiva** **o que tuvo contacto con un caso positivo** «mínimamente de 15 minutos sin protección, sin el barbijo y a menos de 2 metros de distancia. Confirmar por nexo epidemiológico nos va a permitir una agilización y poder más rápidamente aislar a estas personas», había agregado.