---
category: Estado Real
date: 2021-07-01T08:48:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/viaje.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno Santa Fe
resumen: Santa Fe refuerza los controles para que los ciudadanos que vuelven del exterior
  cumplan el aislamiento
title: Santa Fe refuerza los controles para que los ciudadanos que vuelven del exterior
  cumplan el aislamiento
entradilla: "“Seguimos apelando a la responsabilidad individual”, señaló González
  del Pino, y confirmó que se reforzarán los operativos y las sanciones: “Vamos domicilio
  por domicilio a verificar que la gente cumpla”."

---
La secretaria de Gestión Federal de Santa Fe, Candelaria González del Pino, junto a la directora nacional de Migraciones, Florencia Carignano, brindaron este miércoles detalles sobre los operativos de control conjuntos a personas que regresan del exterior y que deben cumplir con el aislamiento obligatorio.

“Seguimos apelando a la responsabilidad individual, y vamos a reforzar los operativos y las sanciones. Vamos domicilio por domicilio a verificar que la gente cumpla el aislamiento”, aseguró González del Pino.

E insistió: “Vamos a reforzar aún más los controles que venimos realizando de manera semanal, en forma conjunta con Migraciones, Seguridad y Salud, a cada uno de los domicilios de los santafesinos que llegan del exterior”.

Por otro lado, explicó que, en caso de que las personas no se encuentren cumpliendo con el aislamiento que corresponde, “se labra un acta, y se da paso a un proceso penal, porque lo que se está haciendo es incumplir un DNU, y cometiendo un delito tipificado en el Código Penal”.

“La idea es hacer hincapié en las estrategias de cuidado, reforzar la responsabilidad individual y entender que se pone en riesgo a toda la provincia, y nos puede llegar a generar imposibilidades de seguir manteniendo actividades productivas”, indicó la funcionaria, y sentenció: “Necesitamos que se tome conciencia y la responsabilidad individual ante todo”.

**OPERATIVOS CONJUNTOS**

A su turno, Carignano amplió: “Se van a realizar operativos para corroborar el aislamiento de personas que han llegado del exterior, quienes deben hacer obligatoriamente 7 días de aislamiento en sus domicilios, y el último día hacerse un PCR para poder salir a la calle”.

En paralelo, detalló que hoy existe un cupo de ingreso al país de 600 personas por día, y planteó que quienes se encuentran en el exterior no son “varados”, porque al momento de viajar cada una de las personas firmó una declaración jurada donde aceptaba cambios en el reingreso.

“No estamos en la misma situacion de marzo del año pasado, donde se cerraron fronteras de un día para el otros; hoy Argentina tiene conectividad y está operando con varias líneas aéreas. Por ende, quienes estuvieron saliendo han firmado una declaración jurada donde aceptaban las condiciones que el Estado nacional reimpondría a su regreso, como también aceptar los costes económicos, jurídicos y sanitarios que podría generarle”, remarcó.

“El problema que estamos detectando es que, más allá de las denuncias penales que podamos hacer, el daño sanitario está hecho, por eso necesitamos ganar tiempo para vacunar todos los días a la mayor cantidad de gente, para que cuando entre nos encuentre a los argentinos mejor parados y evitar la mayor cantidad de muertes”, concluyó.

Del operativo, llevado a cabo en Rosario, participaron también el subsecretario de Seguridad Preventiva de Santa Fe, Diego Lluma, y Florencia Galati, miembro del equipo de Epidemiología provincial.

**CENTRO PARA MIGRANTES Y REFUGIADAS**

Previamente, González del Pino y Carignano, junto al ministro de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman, y el senador nacional, Roberto Mirabella, inauguraron en Rosario el Centro de Integración para personas Migrantes y Refugiadas, ubicado en calle Mitre al 1400, donde también estuvo la secretaria de Derechos Humanos, Lucila Puyol.

“Históricamente, Rosario ha recibido a muchos migrantes en distintos momentos”, señaló Sukerman, y agregó: “Hay inmigraciones más recientes que necesitan protección. En ese sentido, no solamente abrimos los brazos, sino también lugares como estos para poder asistir. Nosotros no nos sentimos representados por políticas de persecución o de expulsión de personas que vienen de otros lugares. Tenemos absolutamente claro lo que tiene que ver con una Latinoamérica unida y con un mundo unido”.

En tanto, Mirabella destacó la importancia de “asistir a personas que son perseguidas, que sufren o que tienen que emigrar de su país”, y recordó que “en distintos momentos de la historia Argentina, nuestro país recibió mucha migración, pero también muchos argentinos tuvieron que migrar en épocas de persecución política y muchos países del mundo nos acogieron”.