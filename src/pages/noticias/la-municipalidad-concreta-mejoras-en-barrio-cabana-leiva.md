---
category: La Ciudad
date: 2020-12-26T12:19:38Z
thumbnail: https://assets.3dnoticias.com.ar/cabana-leiva.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Mejoras en barrio Cabaña Leiva
title: La Municipalidad concreta mejoras en barrio Cabaña Leiva
entradilla: El intendente Emilio Jatón recorrió, junto a los vecinos, las tareas de
  ripiado que se hacen en 15 cuadras. Las intervenciones forman parte del Plan Integrar
  que se concreta en el noroeste de la ciudad.

---
El Plan Integrar sigue adelante en diferentes sectores de la ciudad. En este sentido, se ejecutan tareas de ripiado en 15 cuadras de Cabaña Leiva, y ya se concretaron cambios luminarias, desmalezado y limpieza de zanjas, entre otras. El intendente Emilio Jatón se reunió con los vecinos y juntos caminaron por calles, que ya cuentan con el mejorado. Aprovecharon para observar el cambio que tiene una plaza recuperada por ellos.

El principal objetivo es recuperar la conectividad y mejorar la transitabilidad sobre todo en las calles por donde circulan las líneas del transporte público. **En total se concretará el ripiado de 300 cuadras**. Actualmente, se avanza en Scarafía, Los Troncos, Loyola Norte y Sur, y San Agustín, entre otros barrios.

En este sentido, la secretaria de Obras y Espacio Público municipal, Griselda Bertoni, contó cómo surgió este plan de acción para barrio Cabaña Leiva: «a raíz del relevamiento que hace la Municipalidad en los diferentes distritos, lo que hicimos acá es un importante trabajo de iluminación, con la recuperación de 50 equipos de luces. Si bien sigue habiendo demanda, los vecinos nos comentaban que están muy contentos porque ya se observan los cambios».

Siguiendo esta línea, la funcionaria destacó que «en este momento estamos haciendo un trabajo de ripio importante», y agregó: «Son unas 15 cuadras en las arterias de penetración del barrio, que las conectan con Blas Parera. Estos trabajos van a permitir que los vecinos entren y salgan cuando llueve y así acceder a las avenidas tanto en forma vehicular como peatonalmente».

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;">**Satisfacción de los vecinos**</span>

Mariela Sandoval es vecina del barrio, una de las beneficiadas con la obra de mejorado en las calles y además integra la cooperativa de trabajo: «para nosotros está buenísimo que hagan el ripiado. Hace más de 15 años que no se hacen estos trabajos. La calle estaba repleta de pozos. Los vecinos estamos contentos y agradecidos. El estado de las calles impedía que pasen los patrulleros, las ambulancias, cuando llueve se forman lagunas directamente. Ahora esto es un gran cambio. Gracias al intendente porque hacía mucha falta; se necesitaba el apoyo del Estado y gracias a Dios, se dio».

Por último, José Luis Demercier, vicepresidente de la vecinal Cabaña Leiva manifestó: «veníamos esperando estos trabajos hace más de 12 años. Cabaña Leiva estaba totalmente abandonada y hoy en día, a través del diálogo, empezaron las obras. Esto recién comienza. Hay que avisarles a los vecinos que los trabajos se retomarán en enero, y que tengan paciencia. Los trabajos empezaron con el cambio de luminarias, se repusieron más de 40 focos. Ahora estamos con el ripio y la próxima va a ser los desagües. Vamos por el buen camino».

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;">**Red Barrial**</span>

Bertoni aseveró que «los vecinos están contentos» y que «hablan de una ausencia municipal de más de 12 años». En esta línea aprovechó para destacar la plaza ubicada en Grandoli y Diez de Andino, espacio que fue recuperado por los propios vecinos y hoy cuenta con juegos, veredas y bancos, pero «le falta iluminación», agregó la funcionaria y se llevó el pedido de los vecinos con el compromiso de colocarla.

La secretaria de Obras y Espacios Públicos municipal aprovechó para contar que este barrio será incorporado a una red barrial que comenzará a funcionar en febrero. «Las redes funcionan como un catalizador de todas las estructuras gubernamentales y no gubernamentales, de distintas procedencias: educativas, salud, vecinales, que se acercan a consensuar qué actividades son prioritarias. Los vecinos están agradecidos de que eso suceda», detalló Bertoni.

Por otra parte, la funcionaria se refirió a la cooperativa de trabajo La Vieja Cabaña Leiva que realiza tareas de limpieza y mantenimiento en el barrio: «Son un grupo de mujeres que trabaja con mucho compromiso, eso es muy importante, y no solo en la limpieza y el mantenimiento, sino que sienten como muy propio el barrio».