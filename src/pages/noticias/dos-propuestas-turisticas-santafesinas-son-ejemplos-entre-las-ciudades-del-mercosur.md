---
category: La Ciudad
date: 2021-01-13T08:00:16Z
thumbnail: https://assets.3dnoticias.com.ar/13121-turismo-santafe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: Dos propuestas turísticas santafesinas son ejemplos entre las ciudades del
  Mercosur
title: Dos propuestas turísticas santafesinas son ejemplos entre las ciudades del
  Mercosur
entradilla: Se trata de dos iniciativas en las que la Municipalidad de Santa Fe adaptó
  sus estrategias al contexto por la pandemia.

---
En el marco del 25° aniversario de Mercociudades, se publicó un documento bajo el título de «Los desafíos de la gestión turística en la búsqueda de esquemas asociativos público/privado en un nuevo contexto global». El mismo cuenta con el apoyo de profesionales del turismo y representantes de los gobiernos locales Santa Fe, Quilmes y Tandil (Argentina), de Lima (Perú) y La Paz (Bolivia). Tiene como objetivo principal compartir experiencias de las medidas de adaptación de los distintos municipios ante la pandemia por Covid-19.

La ciudad de Santa Fe compartió dos experiencias: **Mi ciudad como Turista** y **Santa Fe Capital para los Santafesinos**. El _paper_ tiene como objetivo contar dos experiencias de diseño, concertación e instrumentación de políticas turísticas para el desarrollo equilibrado y sustentable del turismo en la ciudad capital, priorizando la democratización del disfrute turístico de los ciudadanos residentes.

Vale recordar que en Santa Fe existe el Ente Autárquico Municipal de Turismo (Safetur), integrado por representantes de los sectores público y privado. Siguiendo esta lógica, el intendente Emilio Jatón puso en la agenda de este organismo la necesidad de promover políticas de Estado vinculadas al turismo social y al desarrollo de la competitividad turística de la ciudad, con la premisa de generar una mejora estructural en el sistema turístico.

<br/>

# **Ser anfitriones**

La experiencia **Mi ciudad como turista** busca generar un doble impacto: por un lado, la eliminación de las barreras económicas, sociales, culturales, demográficas y psicológicas que impiden a muchos santafesinos disfrutar de su propia ciudad; y en segundo lugar, no menos importante, generar conciencia anfitriona y mejorar el sentido de pertenencia. Se pretende promover una conciencia embajadora de los ciudadanos, que sientan orgullo de ser santafesinos y santafesinas.

Se definió que la mejor manera de asegurar el doble impacto era poner a disposición de santafesinas y santafesinos paseos guiados y gratuitos, que permitan disfrutar la ciudad de una manera distinta. La propuesta se puso en funcionamiento en la pre-pandemia, en enero 2020, y a partir de la fecha en la que se decretó el Aislamiento Social Preventivo y Obligatorio en todo el territorio nacional, se avanzó con un esquema de paseos virtuales.

Los paseos de **Mi ciudad como turista** fueron diseñados con una escala accesible, entre 400 y 1.500 metros, lo que facilita el disfrute turístico. En la pre-pandemia, los paseos fueron organizados informando día, horario y punto de encuentro, sin necesidad de reserva. En tanto que en el contexto de la pandemia se establecieron protocolos para que los santafesinos y santafesinas puedan seguir disfrutando y al mismo tiempo sentirse cuidados.

<br/>

# **Propuesta innovadora**

Con el fin de eliminar las barreras físicas y temporales para el disfrute de la ciudad, se diseñaron audioguías para todos los paseos de **Mi ciudad como turista**. Este desarrollo permite que los santafesinos y santafesinas puedan aprovechar al máximo su salida recreativa, y posibilita que la Municipalidad pueda generar alternativas innovadoras en el contexto de la pandemia.

«La publicación de la ciudad de Santa Fe fue considerada como un ejemplo, con un desarrollo exitoso para gestionar la actividad turística en el contexto de pandemia porque permite, por un lado, generar oportunidades de disfrute turístico para los propios residentes de la ciudad; y al mismo tiempo ampliar la oferta para los visitantes», destacó el director de Turismo y presidente del Safetur, Franco Arone.

En referencia a las audioguías, Arone dijo: «se trata de una innovación a nivel nacional. Son nueve paseos en este formato que permite que los santafesinos y los turistas puedan disfrutar de la ciudad y de los recorridos en cualquier momento del día con la posibilidad de acceder a cada audioguía a través de la página web de Santa Fe capital o con la aplicación de Spotify. Es un antecedente muy importante porque nos permite estar dentro de los mejores casos de gestión de turismo de todas las ciudades del Mercosur».

<br/>

# **Turismo de cercanía**

Por otro lado, la experiencia **Santa Fe Capital para los Santafesinos** que también fue tomada como ejemplo en este documento, estableció la necesidad de trabajar un plan de acción estratégico y prospectivo para la reactivación turística en la pos pandemia. De esta manera se diseñó e instrumentó un plan de promoción y comercialización del turismo en la ciudad de Santa Fe.

Atendiendo a las recomendaciones de los organismos especializados a nivel global, el plan se armó sobre la lógica del turismo de cercanía, desplazamientos en cortas distancias y con la dinámica de «burbujas» en destino. El plan tiene como objetivo captar 110.000 turistas y 205.000 excursionistas durante 2021.

La exposición de Santa Fe y publicada en el _paper_, cierra: «entendemos que las experiencias **Mi ciudad como Turista** y **Santa Fe Capital para los Santafesinos** son un claro ejemplo de articulación y concertación público-privada para el desarrollo estratégico, prospectivo y sustentable de la actividad turística en Santa Fe Capital».