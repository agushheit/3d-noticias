---
category: Agenda Ciudadana
date: 2021-08-03T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/AHORA12.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Los detalles de la ampliación del Ahora 12 que suma planes con 24 y 30  Cuotas
title: Los detalles de la ampliación del Ahora 12 que suma planes con 24 y 30  Cuotas
entradilla: El programa funcionará todos los días de la semana sin límite de monto,
  y agrega la posibilidad de hacerlo en 24 y 30 cuotas fijas mensuales, que se suman
  a las modalidades ya existentes de 3, 6, 12 o 18 pagos.

---
La Secretaría de Comercio Interior prorrogó el programa Ahora 12 hasta el 31 de enero de 2022 para abonar compras financiadas con tarjeta de crédito, todos los días de la semana sin límite de monto, y agregó la posibilidad de hacerlo en 24 y 30 cuotas fijas mensuales, que se suman a las modalidades ya existentes de 3, 6, 12 o 18 pagos.

Lo hizo a través de la resolución 753/2021 publicada este lunes en el Boletín Oficial, que precisó que los electrodomésticos de línea blanca podrán pagarse hasta en 30 cuotas.

**Las 24 cuotas**

En tanto, habrá hasta 24 cuotas para línea blanca, materiales y herramientas para la construcción, muebles, bicicletas, colchones, neumáticos, kit de conversión de vehículos a gas GNC y repuestos para automotores y motos, computadoras, notebooks y tablets, televisores y pequeños electrodomésticos.

La resolución también aumentó a $ 15.000 el precio máximo de anteojos recetados y lentes de contacto, adquiridos en ópticas, que pueden financiarse con el programa, e incluyó a los monitores entre los productos financiables.

**Rubros alcanzados**

El Ahora 12 permite adquirir productos de línea blanca; indumentaria; calzado y marroquinería; materiales y herramientas de la construcción; muebles; bicicletas; colchones; libros; artículos de librería; anteojos y lentes de contacto; juguetes y juegos de mesa; neumáticos, accesorios y kit de conversión de vehículos a gas GNC y repuestos.

También instrumentos musicales; computadoras, notebooks y tabletas; artefactos de iluminación; televisores; perfumería; pequeños electrodomésticos; servicios de preparación para el deporte; equipamiento médico; máquinas y herramientas.

La resolución estableció que los proveedores y comercios en las operaciones realizadas a través de tarjetas de crédito cobrarán en un plazo de 10 días hábiles, para las ventas realizadas con las modalidades de 3 y 6 cuotas, con la aplicación de tasas máximas de descuento del 3,10% y 6,01%, respectivamente.

Asimismo, los proveedores y comercios en las operaciones realizadas a través de tarjetas de crédito podrán elegir cobrar en un plazo de 60 días corridos con una tasa máxima de descuento del 8,80% directa o en un plazo de hasta 10 días hábiles con una tasa máxima de descuento del 11,48% directa, para las ventas realizadas con la modalidad 12 cuotas.

En el caso de 18 cuotas, las tasas máximas de descuentos serán de 13,95% si cobran a los 60 días y de 16,52% si lo hacen a los diez; para 24 cuotas, 20,60% y 23,27% respectivamente; y para 30 cuotas, 26,08% y 28,67%.

La secretaria de Comercio Interior, Paula Español, afirmó que “el consumo, la producción y el empleo” son las variables que “hacen girar la rueda de la economía”, y destacó que el programa Ahora 12, cuya ampliación será anunciada oficialmente este mediodía, “es una herramienta importante” para impulsarlas.

“Desde el Gobierno apostamos a poner en marcha la reactivación de la industria y la economía nacional a través del impulso del consumo y de la recuperación del poder adquisitivo de los salarios”.

“Desde el Gobierno apostamos a poner en marcha la reactivación de la industria y la economía nacional a través del impulso del consumo y de la recuperación del poder adquisitivo de los salarios”.

En este marco, destacó que “la decisión de ampliar la financiación a 12 cuotas fijas para calzado e indumentaria" fue tomada "pensando en el beneficio de las y los consumidores”.

“Este es uno de los rubros que mayor demanda tiene y, además, es generador de puestos de trabajo. Decidimos incluirlo pensando en la reactivación del país, con la convicción de que son el consumo, la producción y el empleo los que hacen girar la rueda de la economía”, remarcó Español.

En este contexto, agregó: “Desde el Gobierno apostamos a poner en marcha la reactivación de la industria y la economía nacional a través del impulso del consumo y de la recuperación del poder adquisitivo de los salarios”.

La funcionaria sostuvo que “Ahora 12 es una herramienta importante para impulsar las compras de productos y servicios producidos por argentinas y argentinos”, y remarcó que “fue un programa fundamental para sostener el consumo durante este año y medio de pandemia”.

En ese sentido, precisó que “entre enero de 2020 y junio de 2021, se realizaron 86,8 millones de operaciones a través del Ahora 12”, y remarcó que “el rubro de mayores ventas fue el de electrodomésticos y línea blanca”.

“A partir de esta semana, ampliamos la posibilidad de financiación a 24 y 30 cuotas porque seguimos apostando a un país que crezca a través de la producción y el trabajo”, subrayó Español.

Explicó que, “con la ampliación de las posibilidades de financiación, buscamos que las y los argentinos tengan mayores opciones para adquirir los bienes y servicios que necesitan, y también que puedan planificar sus compras sabiendo que el valor de la cuota no se va a modificar”.

“En la renovación del programa no solo dimos la posibilidad de financiar en 24 o 30 cuotas fijas los productos de línea blanca que son los que más compraron las y los consumidores, sino que también ampliamos a 24 cuotas fijas las compras de televisores y electrodomésticos pequeños; computadoras, notebooks y tabletas; colchones; muebles; materiales y herramientas para la construcción; bicicletas, y neumáticos, accesorios y repuestos”, puntualizó la funcionaria