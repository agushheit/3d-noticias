---
category: Estado Real
date: 2020-12-28T11:35:05Z
thumbnail: https://assets.3dnoticias.com.ar/2812-operativo-vacuna.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Covid 19: Llegan las vacunas a Santa Fe'
title: 'Covid 19: Llegan las vacunas a Santa Fe'
entradilla: Será este lunes, a partir de las 9:15. Rosario, Santa Fe, Rafaela, Venado
  Tuerto y Reconquista son las ciudades donde llegarán las primeras dosis para iniciar
  la campaña de vacunación, este martes 29 de diciembre.

---
En el marco de la campaña nacional, este lunes llegarán a la provincia de Santa Fe las vacunas contra el Covid-19. Las primeras dosis arribarán, a partir de las 9:15 horas, a las ciudades de **Rosario**, **Santa Fe**, **Rafaela**, **Venado Tuerto** y **Reconquista**.

**La vacunación comenzará este martes 29 de diciembre.**

Al respecto, el gobernador Omar Perotti precisó que se definieron «21 centros de almacenamiento para recibir las vacunas en esta instancia, donde el mismo transporte que se ha hecho cargo del esquema nacional, llegará a cada uno de estos centros con la seguridad de no alterar el frío que es el que hay que resguardar en todo momento».

Asimismo, Perotti señaló que este **martes 29** es «el día de inicio en todo el país de la vacunación. **A las 9 horas se estará comenzando el proceso de vacunación**, tomando como prioridad a todo el personal de terapias, guardias, ambulancias y responsables de laboratorios. Es decir, el considerado personal crítico y de resguardo, que han tenido que llevar hasta aquí el trabajo con los casos positivos que requirieron atención».

<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: bold">LAS VACUNAS </span>

Las dosis para el aglomerado <span style="color: #EB0202;">**Rosario**</span> **arribarán al hospital Eva Perón de Granadero Baigorria**, avenida San Martín 1645, y serán recibidas por las ministras de Salud, Sonia Martorano, y de Ambiente y Cambio Climático, Erika Gonnet, y el director del efector Jorge Kilstein.

Además, **llegarán al efector Gutiérrez de** <span style="color: #EB0202;">**Venado Tuerto**</span>, avenida Santa Fe 1311, y las recepcionarán el coordinador de la Región de Salud de Venado Tuerto, Pedro Bustos, y el director del nosocomio, Daniel Alzari.

En tanto, las vacunas **llegarán al hospital José María Cullen de** <span style="color: #EB0202;">**Santa Fe**</span>, avenida Freyre 2138, y las recibirán el secretario de Salud, Jorge Prieto, el Coordinador de la Región de Salud Santa Fe, Rodolfo Rosselli, y el director Juan Pablo Poletti.

**En** <span style="color: #EB0202;">**Rafaela**</span>**, arribarán al hospital Jaime Ferre**, Lisandro de la Torre 737, y las recepcionarán la coordinadora de la Región de Salud Rafaela, Eter Senn; y el director del efector, Diego Lanzotti.

Por último, **las vacunas llegarán al hospital Olga Stucki de** <span style="color: #EB0202;">**Reconquista**</span>, bulevar Hipólito Yrigoyen 1951, y serán recibidas por la coordinadora de la Regional de Salud de Reconquista, Leira Mansur, y el director del efector, Rudi Nuzarello.

Una vez finalizada la descarga de las dosis, las autoridades presentes en los cinco efectores brindarán una conferencia de prensa.