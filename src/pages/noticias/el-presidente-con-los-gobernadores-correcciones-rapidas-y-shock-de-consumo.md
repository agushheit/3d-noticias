---
category: Agenda Ciudadana
date: 2021-09-19T06:15:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/reunionrioja.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'El presidente con los gobernadores: correcciones rápidas y shock de consumo'
title: 'El presidente con los gobernadores: correcciones rápidas y shock de consumo'
entradilla: "“Mis candidatos son los candidatos de los gobernadores”, dijo Alberto
  Fernández ante los mandatarios provinciales."

---
El presidente Alberto Fernández aseguró hoy que sus candidatos "son los candidatos de los gobernadores", al encabezar en La Rioja una reunión de trabajo con mandatarios del Frente de Todos (FdT), quienes le expresaron su respaldo para el relanzamiento de la gestión y se comprometieron a "corregir rápido lo que haya que corregir".

"Recuerden que mi palabra empeñada es la de ser un gobernador más. No es un enunciado, milito el federalismo como política central de mi gobierno", proclamó el Presidente ante los mandatarios provinciales que se dieron cita en la residencia de Ricardo Quintela, el anfitrión del encuentro.

Ante los gobernadores y acompañado de parte de su Gabinete, el Presidente se puso al frente de la campaña de cara a las elecciones legislativas del 14 de noviembre y remarcó: "Mis candidatos son los candidatos de los gobernadores".

Fernández y los mandatarios analizaron los resultados electorales de las primarias abiertas, simultáneas y obligatorias (PASO) del último domingo en cada provincia y a nivel nacional y se comprometieron a "corregir rápido todo lo que haya que corregir" en el rumbo de la gestión.

Coincidieron que en noviembre "se gana con más peronismo y con un shock de consumo para darle respuestas a los que menos tienen", según se informó oficialmente.

Estuvieron presentes el gobernador y designado jefe de Gabinete, Juan Manzur (Tucumán), y sus pares Sergio Uñac (San Juan), Jorge Capitanich (Chaco), Raúl Jalil (Catamarca), Alberto Rodríguez Saá (San Luis), Sergio Ziliotto (La Pampa), Gildo Insfrán (Formosa), Gerardo Zamora (Santiago del Estero) y Mariano Arcioni (Chubut), además del anfitrión, Ricardo Quintela.

De forma virtual, participaron los gobernadores Axel Kicillof (Buenos Aires), Alicia Kirchner (Santa Cruz), Gustavo Bordet (Entre Ríos), Omar Perotti (Santa Fe) y Gustavo Melella (Tierra del Fuego).

La reunión fue convocada tras el resultado de las recientes PASO del 12 de septiembre y a pocas horas de que el jefe de Estado decidiera cambios en su Gabinete en medio de las tensiones entre los socios de la coalición oficialista.

Los mandatarios provinciales celebraron la designación del gobernador tucumano como jefe de Gabinete y acordaron acelerar la gestión del nuevo esquema de anuncios sociales y económicos para revertir el resultado electoral.

"Se va a revertir la situación", dijo con firmeza Manzur al realizar un balance de la reunión y sintetizar la estrategia a seguir de cara a las elecciones definitivas de noviembre.

Agregó que durante el encuentro se habló de "coordinar acciones para mejorar y revertir el resultado electoral" y fue optimista en cuanto a que "están dadas las condiciones" para hacerlo, lo que implica "un desafío muy importante" para los próximos dos meses.

"Los veo a la totalidad de los gobernadores con esta vocación y esta entrega de servicio, por lo que no tengo dudas, se va a revertir esta situación", confió Manzur.

Quintela hizo la misma lectura del encuentro y afirmó que el FdT "va a trabajar fuertemente para revertir la situación" que dejó las PASO y dejó claro que hacerlo es "en beneficio de todos los argentinos y argentinas" porque "no hay nada más importante" que la ciudadanía. "Es lo que está por sobre los intereses de los sectores, de los partidos políticos y de cada uno de nosotros", puntualizó.

El ministro de Obras Públicas de la Nación, Gabriel Katopodis, que fue uno de los funcionarios del Gabinete que acompañó a Fernández a La Rioja y, tras el encuentro, también expresó su convicción respecto a la necesidad de "trabajar sin descanso" y "acelerar todas las respuestas para llegar a cada argentino y cada argentina".

"Los argentinos nos enviaron un mensaje. El Presidente de la Nación, el Gobierno escuchó ese mensaje y respondemos a ese mensaje no con palabras sino con hechos concretos, con hechos de más trabajo, con más obra pública en toda la Argentina y con un Gobierno que todos los días va a trabajar para este país se ponga de pie", destacó Katopodis.

El ministro recordó también que "desde el primer día" el Presidente les indicó a sus funcionarios "estar cerca de la ciudadanía, de los gobernadores, de los intendentes" y "trabajar sin descanso para que lleguen soluciones de trabajo, de persianas de empresas que se están levantando, de reactivación de la economía".

Finalmente, dijo que el actual Gobierno no declama el federalismo, sino que "lo practica" y que expresión de ese compromiso es que haya sido un gobernador el designado como nuevo jefe de Gabinete.

El Presidente llegó a La Rioja poco antes de las 14 para participar de lo que fue una reunión de trabajo con gobernadores de todo el país, el primer paso de un relanzamiento de la gestión -tras los cambios en el Gabinete- mientras se estudian medidas económicas y sociales de cara a la ciudadanía, sobre todo los sectores más vulnerables.

En suelo riojano fue recibido por el gobernador anfitrión, Ricardo Quintela, y el designado jefe de Gabinete, Juan Manzur.

El jefe de Estado llegó acompañado por los ministros del Interior, Eduardo De Pedro; de Obras Públicas, Gabriel Katopodis; de Desarrollo Social, Juan Zabaleta; y el designado titular de Educación, Jaime Perzyck.

También lo acompañaron el presidente de la Cámara de Diputados, Sergio Massa; el secretario General de la Presidencia, Julio Vitobello; el jefe de asesores presidenciales, Juan Manuel Olmos; y la primera dama, Fabiola Yañez.

El mandatario y su comitiva se dirigieron junto a Quintela y Manzur a una de las residencias de la gobernación de La Rioja, ubicada a unos 20 minutos de la capital provincial. Allí lo esperaban ocho gobernadores que participaron del encuentro en forma presencial y a quienes luego se sumaron cinco de manera virtual.

El viaje del Presidente a La Rioja se produjo luego de que anoche anunciara la nueva conformación del Gabinete nacional.

El mandatario designó ayer como su nuevo jefe de Gabinete a Manzur, en reemplazo de Santiago Cafiero, que fue nombrado a cargo del Ministerio de Relaciones Exteriores y Culto.

Además, designó a Aníbal Fernández al frente del Ministerio de Seguridad; a Julián Domínguez, en Agricultura, Ganadería y Pesca; a Jaime Perzyck, en Educación; a Daniel Filmus, en Ciencia y Tecnología; y a Juan Ross como secretario de Comunicación y Prensa tras la renuncia de Juan Pablo Biondi.

El Presidente permanecerá junto a la primera dama en la provincia de La Rioja durante el resto del fin de semana y retornará a la ciudad de Buenos Aires para cuando está prevista, a partir de las 16 en la Casa Gobierno, la jura de los nuevos integrantes del Gabinete.