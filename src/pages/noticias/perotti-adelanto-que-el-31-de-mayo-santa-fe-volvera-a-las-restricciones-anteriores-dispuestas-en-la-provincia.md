---
category: Agenda Ciudadana
date: 2021-05-26T08:27:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/perotti.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El ciudadano
resumen: Perotti adelantó que el 31 de mayo Santa Fe volverá a las restricciones anteriores
  dispuestas en la provincia
title: Perotti adelantó que el 31 de mayo Santa Fe volverá a las restricciones anteriores
  dispuestas en la provincia
entradilla: '"Ojalá esa semana podamos desarrollarla con el mayor nivel de actividad
  y la menor movilidad posible", afirmó Perotti. Sobre las clases dijo que "no hubo
  doble vuelta en la educación" y que la decisión es epidemiológica.'

---
El gobernador Omar Perotti manifestó que a partir del 31 de mayo Santa Fe saldrá del sistema actual de aislamiento y retomará las restricciones que él había dispuesto el jueves anterior al decreto presidencial. Consideró necesario el cumplimiento por parte de la población y que la gente “tiene convencimiento de que hay que cuidarse”. Aseguró además que la provincia tiene uno de los mayores porcentajes de vacunación del país, y que ya se encuentran en la etapa de inocular a personas de 55 años con comorbilidades.

En una entrevista brindada a Canal Tres de Rosario, Perotti dijo que el lunes 31 de mayo la provincia retomará la vigencia plena del decreto que comenzó a regir el jueves. “Volveremos a la misma situación que transitó la provincia el jueves y el viernes”, indicó y agregó: “Ojalá esa semana podamos desarrollarla con el mayor nivel de actividad, con la menor movilidad posible y después volveremos al fin de semana donde bajaremos los niveles de circulación y de actividad”.

De todas formas explicó que la continuidad de las restricciones dependerá del informe epidemiológico que entregue Nación una vez cerrada esta semana. “Los días viernes cierra la semana epidemiológica y allí es donde Nación hace los informes correspondientes y allí cada uno, en base a las mediciones toma las decisiones”, explicó.

Sobre el controvertido tema de las clases presenciales, aseguró: “No hubo doble vuelta en la educación. La provincia tomó una decisión que es tener presencialidad. Vacunó a la totalidad de docentes que manifestaron su voluntad, se invirtió en escuelas y se equipó con implementos necesarios. La decisión de la posibilidad de dar clases o no es epidemiológica”.

A su vez detalló: “La no presencialidad surge de los departamentos que están en alarma epidemiológica, no surge del decreto provincial. Sí lo que tenemos es la facultad de que en una localidad con alta cantidad de casos, no haya clases, tenemos muchas ciudades con esas características”

El gobernador comentó que planifican “sumar más conectividad” para aquellos estudiantes que no tienen acceso a cursar de forma virtual y aseveró que tendrá “acompañamiento de sectores políticos para destrabar esta situación”.

Perotti explicó que hizo los anuncios de cierre sin esperar el DNU nacional porque “el día a día cuenta”, y agregó: “Teníamos claro el panorama tras reunirme con directores de hospitales. Dos días cuentan. No mido si el costo lo pagué yo solo. El costo que se paga acá es la vida que no se salva”.

Sobre la vacunación sostuvo que Santa Fe es una de las provincias con mayor porcentaje de vacunación. “En la próxima semana, a más tardar el miércoles, estarán vacunados todos los mayores de 60 años. Estamos en niveles de comorbilidad de entre 18 y 59 años, vacunando a personas de 55 años en distintos lugares. El ritmo se acelera con la llegada de más vacunas. Ya tenemos colocadas 860 mil dosis en Santa Fe. Es un número altísimo”, añadió.

También expresó que se hicieron gestiones para tratar de conseguir vacunas por parte de la provincia, pero enfatizó que el inconveniente mundial es que “no hay”.