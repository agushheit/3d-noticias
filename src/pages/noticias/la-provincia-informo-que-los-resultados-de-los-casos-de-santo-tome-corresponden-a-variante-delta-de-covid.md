---
category: Estado Real
date: 2021-09-18T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/DELTASANTOTO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia informó que los resultados de los casos de Santo Tomé corresponden
  a variante delta de covid
title: La provincia informó que los resultados de los casos de Santo Tomé corresponden
  a variante delta de covid
entradilla: Desde el Ministerio de Salud solicitaron a la población extremar los cuidados
  y respetar los protocolos a quienes regresen a la provincia desde el exterior.

---
El Ministerio de Salud provincial informó este viernes que los resultados de los análisis por PCR realizados a los 3 pacientes de la ciudad de Santo Tomé corresponden a la variante Delta de Covid 19.

Ante esto, el secretario de Salud, Jorge Prieto remarcó: “No obstante se realizan lo que se llama secuenciación genómica, lo que sería una reconfirmación de acuerdo a la carga viral. En este caso únicamente se lo pudo secuenciar a dos de ellos que reafirma la variante Delta y esto tiene que ver con la carga viral al momento del examen”.

“Queremos señalar que los tres pacientes se encuentran asintomáticos y el equipo de salud seguirá de cerca su evolución”, destacó.

“Recordemos que en la provincia hasta el momento tuvimos 10 casos confirmados con esta variante. Y en ese sentido se está trabajando en cuidados estrechos de fronteras con un equipo interinstitucional de Migraciones, Salud, Protección Civil y Seguridad, en lo que hace a la vigilancia epidemiológica. Este número reducido de casos permite confirmar que un buen aislamiento, seguimiento y control sanitario son fundamentales para retrasar el ingreso de esta cepa”, continuó.

Asimismo, desde la cartera sanitaria se solicitó extremar los cuidados y cumplir con los protocolos sanitarios a todas aquellas personas que regresen del exterior ante la circulación predominante de variante delta a nivel mundial.

**Testeos en el barrio privado**

A raíz de los 3 casos positivos de la probable variante Delta detectados el pasado fin de semana, desde la cartera sanitaria se llevó adelante la instalación de una carpa sanitaria para realizar testeos voluntarios a las personas que fueron contacto estrecho y vecinos y vecinas del lugar.

Los resultados de los análisis realizados los días 14 y 15 del corriente mes fueron los siguientes:

Personas testeadas: 68

Sintomáticos: 13

Contacto estrecho: 3

Tests rápidos: 44

Negativos: 44

Positivos: 0

Muestras para PCR: 25

25 muestras negativas

Es decir que se han testeado en total 68 personas y todas las muestras han sido negativas.

**Recomendaciones**

En este sentido, el secretario de Salud indicó que aquellas personas que ingresan al país “no deben descuidar el aislamiento por más que tengan test rápido con un resultado negativo, porque de esta forma vamos a continuar retrasando y postergando la posible circulación de una variante que tiene una alta contagiosidad”.

“Los controles de rutina nos permiten visualizar la importancia de extremar los cuidados y seguir con las recomendaciones que se imparten al ingreso del territorio provincial desde el call center 0800, quien remite las normativas vigentes en el territorio y determina el día que deben ser sometidos a los test diagnósticos”, concluyó.