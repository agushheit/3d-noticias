---
category: Agenda Ciudadana
date: 2021-08-01T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/DELTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe, una provincia con la variante Delta de coronavirus
title: Santa Fe, una provincia con la variante Delta de coronavirus
entradilla: Tres personas de la ciudad de Rosario fueron diagnosticadas con la variante
  Delta de coronavirus entre las 15 que tenían antecedentes de viajes al exterior.

---
Distintos estudios aseguran que la variante Delta de coronavirus es una de las más contagiosas de todas y es por eso que se realizan rigurosos controles en ingresos de extranjeros como así de pacientes infectados para impedir la circulación viral tanto en el país como la provincia.

Según se informó en las últimas horas, en la provincia de Santa Fe, las autoridades sanitarias confirmaron la presencia de tres casos de la variante Delta en la ciudad de Rosario, de un total de 15 personas que dieron resultado positivo de coronavirus. Así lo confirmaron desde el municipio que, en un trabajo conjunto con la Dirección Nacional de Migraciones y el Ministerio de Salud provincial, realizaron el seguimiento epidemiológico hasta este viernes 30 de julio a 996 personas que tienen antecedentes de viaje al exterior. De ese total, tres personas fueron diagnosticados por el Instituto Malbrán con la variante Delta.

El primero de los casos –informado en su momento y ya dado de alta–, fue detectado en el mes de mayo. Fue una mujer que regresó de Estados Unidos. Las otras dos personas son viajeros provenientes de España durante el mes de julio, en ambos casos dieron positivo de Covid-19 durante el hisopado en Ezeiza, por lo que ambos cumplieron el aislamiento epidemiológico en Buenos Aires. Se trata de una mujer que ya fue dada de alta y un hombre que todavía cursa el aislamiento con el control médico adecuado hasta el otorgamiento del alta epidemiológica. En todos los casos con una buena evolución de la enfermedad.

**Córdoba**

Córdoba registraba este sábado 20 casos positivos de la variante Delta de coronavirus y más de 1.000 personas aisladas preventivamente por ser contactos de contactos estrechos de contagiados, mientras continúan los relevamientos y testeos focalizados en la comunidad educativa y en el entorno intrafamiliar de los portadores del virus, informaron desde el Ministerio de Salud local.

**Ciudad de Buenos Aires**

El subsecretario de atención primaria del gobierno porteño, Gabriel Batistella, informó que la Ciudad "está investigando" si existe "transmisión comunitaria" de la variante Delta de coronavirus, luego de que se detectaran dos casos en la población y 41 personas tuvieran que ser aisladas por ser contactos estrechos.

"Estamos investigando si alguno de los contactos de los casos confirmados viajó al exterior; si es así no hay circulación comunitaria", dijo el funcionario a la prensa. Explicó que la circulación comunitaria "es cuando empiezan a detectarse casos sin nexo epidemiológico".

Precisó que entre los contactos estrechos que se investigan "hay familiares y hay personas que estuvieron en el entorno de circulación previo al diagnóstico" y aclaró que ninguno de ellos está con síntomas".

**Provincia de Buenos Aires**

El ministro de Salud bonaerense, Nicolás Kreplak, confirmó en las últimas horas que la provincia de Buenos Aires registra 22 casos de la variante Delta de coronavirus vinculada a personas viajeras y no registra circulación comunitaria.

En un informe del Programa de seguimiento publicado por el titular de la cartera de Salud, se indicó que se registran 22 casos confirmados, de los cuales "el 77% cumplió con el período de aislamiento, en tanto un 15% se encuentra en proceso de control, mientras que un 8% de los viajeros incumplió el aislamiento y se les labró el acta" publicó a través de su cuenta de Twitter.

Sobre la situación de la variante Delta, desde la cartera sanitaria se precisó que del total de casos registrados, 20 son viajeros internacionales, 13 de ellos detectados en punto de entrada, siete detectados en seguimiento en hoteles, mientras que otros dos casos son por contacto estrecho.

**Salta**

Las autoridades sanitarias de Salta aguardaban los resultados de las muestras enviadas al Instituto Malbrán de ocho pacientes con coronavirus, que fueron contactos estrechos de un deportista que en la primera quincena de julio llegó de Europa con la variante Delta, y por el que están aisladas 36 personas.

Así lo informó hoy el jefe del Programa de Vigilancia Epidemiológica de Salta, Francisco García Campos, quien detalló que esta mañana se comunicó con el Instituto Malbrán para poder contar con los resultados y confirmar si esos ocho pacientes padecen Covid-19 de la variante Delta.

Hasta el momento, son 36 las personas aisladas por este hombre, que llegó desde Europa en la primera quincena de julio y que se realizó un primer hisopado en el Aeropuerto Internacional de Ezeiza, que dio negativo.