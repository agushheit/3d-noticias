---
category: Agenda Ciudadana
date: 2021-05-06T08:45:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/hotel-cerradojpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Salud pidió a hoteleros de Santa Fe ponerse a disposición para recibir pacientes
title: Salud pidió a hoteleros de Santa Fe ponerse a disposición para recibir pacientes
entradilla: La opción que barajan los empresarios es la de reacondicionar hoteles
  cerrados por la pandemia para albergar pacientes, descomprimiendo la ocupación de
  camas en los efectores de la ciudad.

---
A raíz de la gran ocupación de camas en los efectores públicos y privados de la ciudad, el Ministerio de Salud inició conversaciones con hoteleros de Santa Fe para barajar la opción de que se puedan readaptar hoteles en caso de que se necesiten derivar pacientes. En primera instancia, la opción que se propuso fue reacondicionar aquellos hoteles que aún permanecen cerrados por la pandemia.

La opción de utilizar camas de hoteles para la atención de pacientes en el marco de la pandemia de Covid fue algo que la cartera sanitaria provincial ya había planteado como posibilidad. En relación a esto, en Rosario el Ministerio de Salud ya había evaluado la chance de readaptar un hotel para tener una opción de internación de pacientes en caso de saturación.

Al respecto, UNO Santa Fe dialogó con el presidente de la Cámara de Hoteles de Santa Fe, Ignacio Copoletta, quien manifestó: "Hace dos semanas desde Salud se dio la opción a los hoteles para adaptarlos buscando albergar pacientes. No fue una obligación, sino que se comunicó que el quiera ofrecer el hotel para recibir pacientes podía hacerlo".

Hasta ahora ningún hotel fue readaptado para recibir pacientes, "pero sí hubo hoteles que se ofrecieron", según lo afirmó Coppoletta. Y agregó: "En principio la idea era reacondicionar los hoteles que estaban cerrados por la pandemia, es muy raro que en hoteles que tengan pasajeros se hospeden enfermos. Sería una complicación muy grande tanto para los empleados como para los pasajeros".

Con respecto a esto, desde la regional Santa Fe de Salud indicaron a UNO que desde Turismo se definiría cuales serían los hoteles a reacondicionarse para recibir pacientes, pero por el momento no se avanzó mas allá y siguen las conversaciones en este sentido.

> Dos hoteles cerraron definitivamente y seis permanecen cerrados

Consultado por la preocupante situación del sector, el referente hotelero indicó que ya cerraron sus puertas dos hoteles definitivamente en Santa Fe, mientras que por la pandemia hay entre cinco o seis hoteles que siguen cerrados pero con la posibilidad de reabrir sus puertas.

En relación al porcentaje de ocupación de habitaciones, Coppoletta dijo a UNO que "sigue todo igual. Nuestra temporada fuerte comienza en el mes de marzo, pero por el momento no hubo cambios en cuanto al volumen de pasajeros".

Frente a la asistencia económica que recibe el sector, el presidente de la Cámara de Hoteles manifestó: "Ahora estamos esperando una ayuda provincial adicional, para complementar las ayudas del gobierno nacional. En principio se comenzó con el ATP nacional, que cubría el 50% del sueldo y el restante 25% se hacía cargo el propietario. En diciembre se dejó de dar el ATP y empezó a darse el Repro".

El pasado lunes, el ministro de Trabajo provincial, Juan Manuel Pusineri, había adelantado que “se está avanzando con la posibilidad de una asistencia directa o un subsidio en función de la cantidad de trabajadores” para el sector de la hotelería. En este sentido, el referente hotelero de Santa Fe se mostró optimista, destacando que "varias veces se han hecho pedidos al gobernador y se han hecho cartas, seguramente va a salir".