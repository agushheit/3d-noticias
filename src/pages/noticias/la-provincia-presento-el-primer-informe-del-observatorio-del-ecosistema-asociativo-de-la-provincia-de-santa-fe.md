---
category: Estado Real
date: 2021-07-05T06:00:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/OBSERVATORIO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia presentó el primer informe del observatorio del ecosistema asociativo
  de la provincia de Santa Fe
title: La Provincia presentó el primer informe del observatorio del ecosistema asociativo
  de la provincia de Santa Fe
entradilla: Se trata de una iniciativa que brinda información sobre el Cooperativismo
  y Mutualismo en nuestro territorio.

---
En el marco del día Internacional de las Cooperativas, el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, encabezó ayer sábado, desde Sunchales y a través de una videoconferencia, la presentación del primer informe del Observatorio del Ecosistema Asociativo de la Provincia de Santa Fe (OEASF), iniciativa que surge del trabajo en conjunto de la Dirección Provincial de Economía Social, Agricultura Familiar y Emprendedorismo de la provincia de Santa Fe y del Instituto Cooperativo de Enseñanza Superior (ICES) dependiente de la Fundación Grupo Sancor Seguros

Del acto participaron también en forma presencial el secretario de Desarrollo, Territorial y Arraigo, Fabricio Medina; el senador provincial, Alcides Calvo; el director provincial de Economía Social, Agricultura Familiar y Emprendedorismo, Guillermo Tavernier; el presidente de la Fundación Sancor Seguros, Eduardo Reixach y el director del ICES, Román Frutero.

El informe presentado se actualizará anualmente y en él se relevarán datos del trabajo de cada cooperativa y mutual santafesina, los cuales serán procesados y expuestos por el Observatorio, ya que éste es un instrumento de monitoreo que posibilita la investigación y análisis en profundidad de este sector. El objetivo es generar espacios de diálogo, intervenciones conjuntas y consensos.

Durante la presentación, que contó con más de 200 representantes del sector a través de una videoconferencia, se rubricó un convenio de cooperación entre el Ministerio de Producción, Ciencia y Tecnología y la fundación mencionada. El objetivo del acuerdo es trabajar en conjunto para la sistematización de información que colabore al fortalecimiento del ecosistema asociativo.

Al tomar la palabra, y luego de felicitar al movimiento cooperativo en su día, Costamagna destacó que “este día tiene que ver con algo muy profundo de nuestra provincia, el movimiento cooperativo es territorio, es organización y sociedad, es arraigo y compromiso”.

“Estar en Sunchales, en una provincia como la de Santa Fe, en un día como este es toda una señal. La que presentamos aquí nos permitirá avanzar en construir escenarios que nos den la posibilidad de crear una agenda común de desarrollo, de arraigo local y trabajo colectivo, como siempre nos pide el gobernador Omar Perotti. Es por esto que queremos potenciar aquellas entidades que mejoran la calidad de vida de las y los santafesinos. Considero sustancial seguir consolidando el entramado de vínculos y redes que dan fuerza a nuestras organizaciones”.

Por su parte, el presidente de la Fundación Sancor Seguros, Eduardo Reixach indicó que “es un gran desafío el observatorio y es muy interesante el trabajo articulado con la Dirección Provincial de Economía Social, Agricultura Familiar y Emprendedurismo. Queremos reconocer el trabajo que se generó y agradecemos la tarea de abordar esta idea para contar con información del ecosistema asociativo de la provincia y el país. Será fundamental para tomar decisiones, una sociedad para crecer debe estar organizada y para ello es necesario la información, este informe apunta a eso”.

A su turno, el senador Calvo manifestó que “el asociativismo es una forma de ver la economía, la salud, la educación, la cultura, que se centra en las personas y se basa en la solidaridad y valores de ayuda mutua. Hoy no es un día más, la pandemia nos ha marcado y debemos conceptualizar más que nunca la idea de estar juntos y que nadie se salva solo. Más que nunca el concepto de cooperativismo debe estar vigentespara superar los escollos entre todos”.

Al tomar la palabra, Fabricio Mediana resaltó que “estamos poniendo a disposición de todo el asociativo santafesino una herramienta transformadora, que viene a brindar información para la toma de decisiones. Es un claro ejemplo de sinergia entre sector público y privado. Como dice nuestro gobernador Omar Perotti: integrar territorialmente la provincia y equilibrarla poblacionalmente. Seguramente esos dos pilares se asentarán sobre la base del cooperativismo”.

Finalmente, el director Tavernier subrayó que “el observatorio es el primer paso para convertir a Santa Fe en el ecosistema asociativo más importante de Latinoamérica, no existe en el continente un polo tan potente como tenemos en la provincia. Así lo dicen los datos de este informe y la historia, porque si pensamos los inicios de esta provincia pensamos en cooperativismo y mutualismo y en la cultura asociativa. Estos movimientos están presentes en los más de 10 millones de personas asociados a cooperativas y mutuales”.