---
category: La Ciudad
date: 2021-02-21T07:01:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/ACTIVIDADESFINDE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Música, cine y murga: las propuestas del fin de semana'
title: 'Música, cine y murga: las propuestas del fin de semana'
entradilla: La Municipalidad difunde una variada agenda de actividades hasta el domingo,
  todas cumplen con los requisitos sanitarios para prevenir contagios de coronavirus.

---
Entre el sábado 20 de febrero y el martes 2 de marzo continúan las actividades culturales, deportivas y recreativas en espacios abiertos de la ciudad. Cada una de ellas se realiza con demarcaciones que permiten un distanciamiento adecuado para disfrutar con los protocolos de prevención necesarios.

**Carnaval murguero en el Anfiteatro**

Con la recuperación del Anfiteatro “Juan de Garay” del Parque del Sur (Av. Illia y San Jerónimo), la Municipalidad avanza en la tarea de construir una programación pública y gratuita, que estará abierta también a las producciones independientes.

En este sentido, se dará un nuevo paso con un encuentro de murgas de estilo uruguayo. Este viernes 19 de febrero, desde las 21 horas, subirán al escenario Los Príncipes de Momo y Rezonga La Ronca; y el sábado 20 a la misma hora, se presentarán Las Damajuanas y La Desbocada. La entrada es gratuita, con cupos limitados, por lo que deberán retirarse previamente en la boletería del Teatro Municipal “1° de Mayo” (San Martín 2020).

**Verano Capital en los botánicos**

El viernes 19, desde las 19, en el Jardín Botánico “República del Líbano” (Av. Aristóbulo del Valle 4700), habrá lecturas de poesía a cargo de Violeta Vignatti, Adriana Ceñal y Gabriela Schumacher de la Biblioteca Popular Mateo Booz, que funciona en la Vecinal Fomento 9 de Julio. La música en vivo llegará de la mano de Matt Hungo & la Hot Band, y Maca Revolt & Nicoté Sound System. La entrada es libre y gratuita.

En tanto, el domingo 21 de febrero, a las 17 horas, en el Jardín Botánico “Ing. Lorenzo Parodi” (Avenida Gorriti 3902-4000) habrá una propuesta de ferias y folclore para los vecinos del norte de la ciudad, con emprendimientos, música en vivo y actividades deportivas, entre otras.

**Danza en la Playa: “Somos folclore”**

El ciclo de danza en el Centro de Deportes (Av. Almte. Brown 5200) continúa el viernes 19 de febrero, a las 20, con “Somos folclore”. El espectáculo cuenta con las interpretaciones de Mateo Jullier, Natalia Boglione, Fernando Martínez, Natalia Busi, Lara Acosta, Dalma Acosta y Jonatan Giménez, también director de la puesta. La entrada es libre y gratuita.

**Pantalla Pública: “Y tu mamá también”**

Maribel Verdú, Gael García Bernal y Diego Luna protagonizan “Y tu mamá también”, el film de Alfonso Cuarón que se podrá disfrutar en la próxima fecha del ciclo “Pantalla Pública”. La proyección está prevista para el viernes 19 de febrero, a las 19.30, en el Auditorio Federal del Museo de la Constitución (1º de Mayo y Avda Circunvalación). La entrada es gratuita, con capacidad limitada de la sala, por lo que deberá reservarse previamente en la sección “Pantalla Pública” de la plataforma Capital Cultural.

**Fin de semana en La Marechal**

El ciclo en la emblemática Sala Marechal del Teatro Municipal “1º de Mayo” (San Martín 2020) continúa este fin de semana con tres funciones. El viernes 19, a las 21, horas se presentará “Urda y el Brutopez. Una monstruosa historia de porquería”, con dramaturgia de Federico Toobe, intérprete de la obra; y María Flavia Del Rosso, directora de la puesta.

La propuesta del sábado 20 también comienza a las 21, de la mano de la compañía Ham, que creó y dirige Cecilia Kucharuk desde el año 2009.

En tanto el domingo 21, a las 20, la programación propone la comedia “Drácula… transilvando bajito”, de Federico Kessler. Se trata de una obra de teatro apta para todo público, inspirada en la clásica novela de Bram Stocker.

Las entradas ya se encuentran a la venta en la boletería, a 200 pesos. Pueden adquirirse de martes a viernes, de 17 a 21 horas. Atendiendo al protocolo vigente para el funcionamiento de la sala, el cupo será limitado a 55 entradas, por lo que se recomienda adquirirlas con anticipación.

**Música en los paradores de playa**

El viernes 19 de febrero, a las 19, Cada Cual se presenta en el Parador Santa Kite (Alte. Brown 6880), con el proyecto que une diversos intereses musicales y adopta elementos de géneros como el folklore, el rock, el R&B y el jazz, creando así una fusión musical interesante.

También el viernes, pero a las 21 y en el Parador Santa Fe, ubicado en la Costanera Este, actuará el músico santafesino Ale de Larrechea, presentando canciones de Uno, su primer disco, compuesto por 10 canciones de su autoría.

El sábado 20 a las 19, será el turno del grupo Máscaras, en Santa Kite. Andrés Ferrero en voz y guitarra, Julio Rossini en guitarra, Iván “Tato” Mounier en batería y Sebastián Giménez en el bajo, integran esta formación típica del género.

**Verano Capital en el Parque del Sur**

El ciclo de espectáculos circenses al aire libre, que alterna semanalmente en los Parques Garay y del Sur, continúa el domingo 21 de febrero, desde las 17.30, en el Parque del Sur, con la participación de artistas del pequeño circo ambulante “Circo del Litoral”.

**Carnavales en los barrios**

La propuesta continúa el sábado 20 de febrero en avenida Gorriti y Acceso Norte, en Loyola Sur. Allí se prevé una exhibición de zumba de la Asociación Civil “Cebollitas” y la participación de Herederos del Sol y Sueño Dorado.

El domingo 21, en la manzana 6 de Alto Verde, habrá una nueva fecha del Movimiento de Organizaciones Murgueras del Oeste, integrando a cuatro agrupaciones que surgieron en el distrito costero: Renegados, Payasos de la 3, Amanecer y Peques de Alto Verde. Los encuentros están previstos, de 19 a 21 horas, con entrada libre y gratuita.

**Centro Experimental del Color**

En continuidad con las actividades enmarcadas en el proyecto artístico-pedagógico “Kiwi, el barro y las palabras”, esta semana habrá dos propuestas ligadas al dibujo, la literatura y la cerámica. Las actividades tendrán lugar en el Centro Experimental del Color (Estación Belgrano, Bv. Gálvez 1150). La entrada es gratuita, con cupos limitados. Las inscripciones se pueden realizar mediante el siguiente correo electrónico: kiwi.barroypalabras@santafeciudad.gov.ar

El jueves 18 de febrero, a las 17, para niñas y niños a partir de los 12 años, Karina Mendoza y Virginia Martí coordinarán el taller de dibujo y cerámica “La orilla azulada”.

El viernes 19, a la misma hora, el equipo pedagógico del área de Museos, guiará el taller de literatura y cerámica “Bogando estrellas”. Esta propuesta es para niñas y niños, entre 6 y 12 años de edad.

**Santa Fe Jazz Sessions: Boogagroove**

En el octavo capítulo de Santa Fe Jazz Sessions, que refleja la escena del jazz santafesino, se presenta Boogagroove, la formación que integran Pepi Dallo en trompeta, Facundo Céspedes en órgano, Ricardo Rosa en batería; con la saxofonista Fernanda Lagger como invitada especial.

El concierto se podrá ver el sábado 20 de febrero, a las 21, en el canal de YouTube de Santa Fe Jazz Sessions, promocionado también desde los sitios web Capital Cultural, Agenda Cultural UNL y redes sociales de las instituciones organizadoras.

**60 Años del Taller de Cerámica de La Guardia**

El sábado 20 de febrero, a las 19, se estrena un nuevo capítulo de la serie que indaga en los 60 años de historia del Taller de cerámica de La Guardia, que se proyecta como un espacio donde se investigan y preservan técnicas antiguas de la cerámica del litoral.

“Tradiciones” es el título de la tercera entrega, en la que los docentes del Taller cuentan acerca del trabajo de recuperación de tradiciones ancestrales del territorio. La recreación de una pieza requiere de una investigación previa. Gracias a esta observación y estudio, se da continuidad a las técnicas tradicionales de distintas culturas y grupos. Más información en www.santafeciudad.gov.ar/capitalcultural

**Deportes al sol**

Las actividades deportivas gratuitas continúan durante este fin de semana en la capital santafesina en seis espacios a cielo abierto. Se trata de las sedes ubicadas en las playas de la Costanera Este, la del Centro de Deportes de la Costanera Oeste, las del Espigón I y II, y los Parques del Sur y Garay.

Habrá Beach Voley este sábado, de 18 a 20 horas, en el Centro de Deportes de la Costanera Oeste y, de 17 a 18 horas, en Playa Este, mientras que el domingo, de 17 a 18 horas, será el turno de los Espigones de la Costanera Oeste.

Por otra parte, habrá Beach Tenis el sábado, de 9 a 12 horas, y el domingo, de 9 a 12.30, en el Centro de Deportes de la Costanera Oeste.

Funcional se realizará el sábado, de 10.15 a 11 y de 19 a 20 horas, y el domingo, de 19 a 20, en el Centro de Deportes de la Costanera Oeste.

La propuesta de Ritmos se llevará a cabo el sábado, de 18 a 19 horas, en el Centro de Deportes de la Costanera Oeste, y el sábado y domingo, de 18 a 19, en Playa Este. Y, el Beach Handball se disputará el domingo desde las 17 horas en el Centro de Deportes de la Costanera Oeste.

Además, el sábado y el domingo, de 9 a 13 y de 16 a 20, en los playones principales de la Dirección de Deportes, se llevará a cabo un certamen amistoso 3 x 3 con reglas Fiba y bajo estrictos protocolos. El evento cuenta con una importante anuencia de equipos, dos tableros principales y medidas oficiales.

La propuesta de Beach Tenis se realizará el sábado y el domingo, desde las 9, y tendrá como epicentro el complejo de Costanera Este y será con inscripción previa.

Y, por último, el triatlón vuelve a las calles de la ciudad. Será este domingo desde las 7.30 en calle Boneo y costanera. La actividad está dirigida a todas las edades, requiere de inscripción previa y es organizada y fiscalizada por la Federación Santafesina de Triatlón con el apoyo de la Municipalidad de Santa Fe.

Cabe señalar que además de los 90 guardavidas que supervisarán las playas, la Municipalidad dispuso la presencia de más de 100 profesores de Educación Física que estarán a cargo de las actividades deportivas y recreativas.

**Ferias**

Como cada fin de semana, los artesanos y las artesanas que participan de la Feria del Sol y la Luna se reúnen en la Plaza Pueyrredón (bulevar Gálvez 1600), en un encuentro que es Patrimonio Cultural y Artístico de la capital santafesina. Este fin de semana se podrá recorrer el sábado y el domingo, desde las 17 horas. Es obligatorio concurrir con tapabocas y mantener el distanciamiento físico.

**Música en vivo**

Durante este fin de semana habrá cena show y música en vivo en distintos clubes y bares de la ciudad y de la costa. En este sentido, en dos clubes de la ciudad se llevarán a cabo propuestas de cena show. Desde las 20, este sábado la cita será en el Club Atlético Villa Dora (Ruperto Godoy 1231). Por su lado, en República del Oeste (avenida Freyre 2765) habrá actuaciones el viernes y el sábado, a partir de las 20 horas.

En Piedras Blancas, se podrá disfrutar de un show acústico el viernes y el sábado a partir de las 20.30 horas.