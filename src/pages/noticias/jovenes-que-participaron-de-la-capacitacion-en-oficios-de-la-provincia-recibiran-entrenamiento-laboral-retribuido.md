---
category: Estado Real
date: 2021-07-15T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/JOVENES.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jóvenes que participaron de la capacitación en oficios de la provincia recibirán
  entrenamiento laboral retribuido
title: Jóvenes que participaron de la capacitación en oficios de la provincia recibirán
  entrenamiento laboral retribuido
entradilla: Las mujeres beneficiadas recibieron la certificación de la Escuela de
  Oficios Artísticos del Ministerio de Cultura de la Provincia y se desempeñarán en
  el área de Sonido, que fue la que eligieron cursar.

---
En la Casa de la Cultura de San Justo, se firmó este miércoles un convenio entre la Oficina de Empleo local del Ministerio de Trabajo de la Nación, la Secretaría de Cultura de dicha localidad y la empresa privada Megadance, mediante el cual tres jóvenes, que realizaron una capacitación en oficios dictada por la cartera cultural provincial, accederán a un entrenamiento laboral pago en el área de Sonido, que es la que cursaron.

Se trata de un acontecimiento de suma importancia, ya que actualiza el convenio suscripto en octubre del 2020, entre el Ministerio de Cultura de la provincia de Santa Fe y la cartera laboral nacional.

La resolución N°2020-549 plantea el objetivo de “generar propuestas que refuercen las competencias, habilidades y calificaciones de trabajadoras y trabajadores de la provincia de Santa Fe, las personas que aprueben los cursos de la Escuela de Oficios Artísticos y tengan Monotributo Social o estén desocupadas, pueden aplicar en los programas de empleo del Ministerio de Trabajo, Empleo y Seguridad Social de la Nación”.

La firma del convenio fue seguida en forma virtual por el ministro de Cultura, Jorge Llonch, quien expresó que “San Justo es pionero en estos convenios de articulación con empresas para vincular a los estudiantes en el campo laboral”, y destacó “la importancia de la articulación entre el Ministerio y el municipio”.

Asimismo, el titular de Cultura recordó que esta semana está empezando la tercera etapa de la EOA, que cada curso tiene 19 aulas –una por departamento– con 50 alumnos en cada una, y felicitó a las tres jóvenes que terminaron el curso de sonido, al tiempo que anunció: “Vamos a reeditar cada uno de los cursos que ya se dieron para dar una oportunidad a aquellos que no pudieron hacerlo”.

Además de Llonch, la firma del convenio contó con la presencia, virtual, del intendente de San Justo, Nicolás Cuesta; la directora de Innovación Sociocultural, Florencia Lattuada; el coordinador general de la EOA, Hugo Sanguinetti; la coordinadora de Cultura de San Justo, Georgina Martinez; representantes de la empresa y Cecila Theumer, de la Oficina de Empleo del Ministerio de Trabajo, Empleo y Seguridad Social.