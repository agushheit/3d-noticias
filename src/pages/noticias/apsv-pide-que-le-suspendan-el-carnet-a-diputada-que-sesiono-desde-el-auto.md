---
category: Agenda Ciudadana
date: 2021-07-06T08:49:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/diputada-belavi.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: APSV pide que le suspendan el carnet a diputada que sesionó desde el auto
title: APSV pide que le suspendan el carnet a diputada que sesionó desde el auto
entradilla: El titular de la Agencia Provincial de Seguridad Vial solicitará que la
  legisladora Natalia Armas Belavi sea privada de su licencia hasta tanto el Tribunal
  de Faltas se expida sobre su caso

---
La semana pasada, la diputada provincial Natalia Armas Belavi, del bloque Somos Vida y Familia, quedó envuelta en una polémica por participar de la última sesión de la Legislatura desde su auto. Pese a que aseguró que estaba detenida, el repudio se hizo sentir desde ONGs que bregan por la conciencia ciudadana en materia vial. También manifestaron su indignación los titulares de las agencias nacional y provincial de seguridad vial, Pablo Carignano y Osvaldo Aymo, respectivamente.

Por ello, desde la APSV decidieron actuar “de oficio” y presentar una resolución para pedir a la dependencia que le entregó la licencia de conducir a la legisladora que le suspenda la misma hasta tanto la Justicia de Faltas se expida sobre lo ocurrido. 

Así lo confirmó por LT10 Aymo, quien además anticipó que este martes estará en Santa Fe para firmar el pedido en nombre de la Agencia Provincial de Seguridad Vial. 

El argumento de la APSV es que Armas Belavi, al generar una distracción mientras estaba en su automóvil, incurrió en una “conducción temeraria”. 

Aymo explicó que el hecho de que sea diputada no implica un agravante porque “todos somos iguales ante la ley”, pero sí se refirió a la cuestión ética subyacente en este tipo de conductas. 

En ese sentido, y haciendo especial hincapié en los adultos y el ejemplo que éstos dan a los menores, el titular de la APSV reclamó: “debe haber un correlato entre lo que decimos y lo que hacemos”.