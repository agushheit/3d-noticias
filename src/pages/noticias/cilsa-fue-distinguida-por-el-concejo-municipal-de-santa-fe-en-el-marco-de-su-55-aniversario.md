---
category: La Ciudad
date: 2021-05-16T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/CILSA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: CILSA fue distinguida por el Concejo Municipal de Santa Fe en el marco de
  su 55° aniversario
title: CILSA fue distinguida por el Concejo Municipal de Santa Fe en el marco de su
  55° aniversario
entradilla: Este viernes 14 de mayo, en el recinto del Concejo Municipal de la ciudad
  de Santa Fe, se realizó la entrega de declaración de Interés Municipal a la ONG
  CILSA por su 55° aniversario.

---
En esta oportunidad la ceremonia se efectuó de manera presencial y a través de la plataforma digital meet, cumpliendo con los protocolos de distanciamiento y prevención correspondientes a la pandemia.

Al respecto, la concejala Ines Larriera afirmó “Para mí es un honor hacer esta entrega, con CILSA tengo un vínculo muy especial ya que tenemos un hijo en común. El Centro CONIN Nº 2 de Varadero Sarsotti lo hicimos junto. CILSA nació en Santa Fe y lleva 55 años al servicio de las personas con discapacidad. Nos enorgullece hacer esta distinción”, finalizó.

Por su parte, el vicepresidente y fundador de CILSA, Juan Luis Costantini, se mostró muy emocionado al momento de tomar la palabra: “CILSA ha sido fundada, administrada y dirigida por personas con discapacidad. Y eso nos da una fortaleza especial ya que nosotros somos los que sabemos qué nos hace falta, hasta dónde podemos llegar y qué se necesita en el ámbito de la discapacidad”. “Este tipo de reconocimiento nos hacen ver que vamos por el buen camino, por el camino correcto. Muchas gracias”

Por su parte María Itatí Castaldi, secretaria de la Comisión Directiva de CILSA, agregó: “Para nosotros esto es una caricia al alma. La institución ha crecido mucho en estos 55 años. Y Juan Luis es un ícono de CILSA, siempre luchó por las personas con discapacidad, para que estén mejor y que tengan oportunidades. Y CILSA trata de hacer eso, de brindar herramientas y oportunidades a aquellas personas que están en situaciones de vulnerabilidad”.

Del acto participaron los concejales Santafesinos Carlos Pereira, Carlos Suárez, Laura Spina y Jorgelina Mudallel. De manera virtual la entrega fue seguida por autoridades de la Comisión Directiva de CILSA, Directivos de la organización y representantes de las oficinas de CILSA de todo el país.