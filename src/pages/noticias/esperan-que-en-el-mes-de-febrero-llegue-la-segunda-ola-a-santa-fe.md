---
category: Agenda Ciudadana
date: 2021-01-03T11:20:02Z
thumbnail: https://assets.3dnoticias.com.ar/03012021-covid.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Esperan que en el mes de febrero llegue la segunda ola a Santa Fe
title: Esperan que en el mes de febrero llegue la segunda ola a Santa Fe
entradilla: Así lo confirmó la ministra de Salud provincial Sonia Martorano quien
  además dijo que para esa fecha se estima que comenzará la vacunación a la población
  general con la vacuna Sputnik V.

---
La ministra de Salud, Sonia Martorano, manifestó preocupación por el aumento de casos y anticipó que esperan una segunda ola de contagios para febrero, mes en que también prevén el inicio de la campaña de vacunación para la población general.

«Nos preocupa que suba, sabemos que los contagios están puertas adentro, cuando la gente se relaja. Los contagios son intramuros y hay que insistir en cuidarse», manifestó la ministra.

«Un rebrote vamos a tener, una segunda ola en febrero, pero podemos morigerarla cuidándonos unos meses más. Podemos intentar que esta segunda ola no sea tan severa, que no haya colapso», dijo a continuación.

Martorano admitió que están preocupados por «las imágenes de tanta gente sin barbijos» que se reprodujeron en estas fiestas. 

Aunque reconoció que en Rosario y en Santa Fe los números de fiestas clandestinas y encuentros masivos fueron menores al resto del país, teniendo en cuenta el aumento de casos, observó: «tenemos restricción de la circulación que no siempre se respeta, pero es importante reducir la accidentología y, con ello, las camas que se ocupan; no estaría mal generar más restricciones nocturnas».

La funcionaria comentó que también para febrero se espera la vacunación general para toda la población. Aunque aún es un cálculo: «ahora vinieron pocas vacunas, fue para probar la logística. La semana que viene pueden llegar 12 mil dosis más y, para terminar con la población de Salud, docentes y Seguridad, esperamos una remesa de 4 millones de dosis para enero. Abriendo toda la logística podríamos empezar en febrero con la población general, más allá de la vacunación a pacientes de riesgo que vamos a incluir apenas podamos –se espera a la autorización del Anmat–», sostuvo.

Martorano ahondó sobre los próximos pasos: «La vacuna Sputnik es la que conseguimos primero y es muy buena, se compraron 300 mil dosis y 24 vienen para nosotros. En enero –continuó–llega otra remesa de 4 millones y unas 320 mil serán destinas a Santa Fe para terminar con el personal esencial, en febrero llegan otros 4 millones y en marzo los 22 millones de Astra Zeneca».