---
category: Agenda Ciudadana
date: 2022-12-17T08:29:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/warning-sign-30915.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Las pymes en alerta ante el bono dispuesto por el Gobierno nacional
title: Las pymes en alerta ante el bono dispuesto por el Gobierno nacional
entradilla: '“Es claro que nuestros colaboradores perdieron poder adquisitivo por
  la inflación. Pero el marco correcto para recuperarlo son las paritarias" dijo el
  presidente de CAME, Alfredo González '

---
“Es claro que nuestros colaboradores perdieron poder adquisitivo por la inflación. Pero el marco correcto para recuperarlo son las paritarias, muchas de ellas aún abiertas, como es el caso de la mercantil, la más grande del país”, dijo el presidente de CAME, Alfredo González.

La Confederación Argentina de la Mediana Empresa (CAME) manifiesta su preocupación ante la decisión por parte del Gobierno nacional de otorgar un bono obligatorio de $ 24.000 para trabajadores del sector privado con ingresos netos de hasta tres salarios mínimos. “Es claro que nuestros colaboradores perdieron poder adquisitivo por la inflación. Pero el marco correcto para recuperarlo son las paritarias, muchas de ellas aún abiertas, como es el caso de la mercantil, la más grande del país”, dijo el presidente de CAME, Alfredo González.

En un contexto de cinco meses consecutivos de baja de ventas minoristas y con negociaciones abiertas con el gremio para definir el segundo tramo de actualizaciones salariales por inflación, desde CAME aseguran que las pymes no están en condiciones de afrontar el pago del bono. “Mayor poder adquisitivo podría dinamizar el consumo interno. Pero la situación para las pymes no es sencilla porque este mes también se debe abonar el segundo medio aguinaldo”, enfatizó González.

“Muchas de las pymes que representamos son monotributistas y no podrán soportar el impacto financiero de esta medida, ya que el mecanismo de deducción del 50% del bono en el Impuesto a las Ganancias no las contempla”, continuó González y agregó: “Así, los empresarios debemos hacernos responsables de la erosión salarial por la inflación. Sin embargo, nosotros sufrimos sus consecuencias al igual que los trabajadores”.

Cabe destacar que CAME es una de las entidades empresarias que participa de la paritaria que beneficia a 1.200.000 trabajadores mercantiles; la más grande del país. “Exhortamos al Gobierno nacional a revisar esta medida e incluir cualquier iniciativa similar en el marco de la negociación sindical-empresaria que ha demostrado su eficacia a lo largo de los muchos años de vigencia”, finalizó González.