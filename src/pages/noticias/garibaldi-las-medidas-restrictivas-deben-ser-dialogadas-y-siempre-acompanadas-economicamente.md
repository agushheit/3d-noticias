---
category: La Ciudad
date: 2021-05-16T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/GARIBALDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Garibaldi: "Las medidas restrictivas deben ser dialogadas y siempre acompañadas
  económicamente"'
title: 'Garibaldi: "Las medidas restrictivas deben ser dialogadas y siempre acompañadas
  económicamente"'
entradilla: Así se expresó el edil santafesino, solicitando al Gobierno provincial
  que se abra a la escucha, en la búsqueda de que las restricciones  sean consensuadas
  en mesas de diálogo con los sectores afectados.

---
Ante el avance de la segunda ola, a lo largo y a lo ancho del país se ha buscado reducir la circulación de las personas para minimizar los contagios, esto implicó también el cierre de algunas actividades económicas específicas, como ser las restricciones en el sector gastronómico, los establecimientos con ofertas culturales y ahora gimnasios, canchas de fútbol 5 y clubes socio-deportivos, que en el gran Santa Fe nuclea a unos 12 mil trabajadores, aproximadamente, lo que se desprende de los datos suministrados por la Encuesta Permanente de Hogares (EPH) correspondientes al 3er. trimestre de 2020. Estas medidas provocan que los sectores afectados por estas restricciones se encuentren con muchísimas dificultades para sostener su actividad, con el consecuente impacto en el empleo que eso representa.

En ese sentido Garibaldi, expresó: “Es fundamental que el Gobernador asuma un rol de liderazgo ante esta situación de la pandemia. Esto implica que las tomas de decisiones se realicen en base a un plan, sostenido por datos objetivos y respaldado por el consenso con los sectores afectados y cuidando sobre todo cada uno de los puestos de trabajo”.

Con 1 de cada 3 empleados sin registrar, las restricciones a las actividades económicas vigentes dejan a unos 7000 trabajadores al borde del desempleo. A 15 meses del primer decreto que marcó el inicio de la pandemia, en lo que respecta a empleo privado registrado, indicadores laborales del Ministerio de Trabajo de la Nación mostró una cierta evolución favorable a partir del mes de mayo, por lo tanto, es clave que desde el Gobierno Provincial se hagan todos los esfuerzos posibles para acompañar esa recuperación.

El consenso previo debe ser una herramienta imprescindible para poder cumplir con los objetivos sanitarios de cuidado, impidiendo la continuidad del deterioro de la economía. Ante esto, el concejal expresó: “La premisa es preservar la salud sin que se sigan perdiendo puestos de trabajo. Las medidas restrictivas deben ser dialogadas y siempre acompañadas económicamente por la provincia, por eso cuando el estado toma decisiones criteriosas y consensuadas, hemos visto que los santafesinos han acompañado estas decisiones. Cuando las mismas se toman de manera errática e improvisada, sucede lo que estamos viendo”, y continuó: “La provincia posee los medios para tener una presencia más firme junto a los sectores más afectados, por eso no alcanza con decretar decisiones. Estas tienen también que ser claras y planificadas, y además hay que velar por su cumplimiento”, finalizó.