---
category: Agenda Ciudadana
date: 2021-03-10T06:53:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/gremios.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Con expectativas de propuesta salarial, se reanuda la paritaria estatal
title: Con expectativas de propuesta salarial, se reanuda la paritaria estatal
entradilla: Funcionarios provinciales y representantes de UPCN y ATE volverán a encontrarse
  este miércoles. El ofrecimiento del gobierno sería similar al que se hizo a los
  docentes.

---
El gobierno santafesino convocó a los gremios de la Administración Central a una nueva reunión paritaria. Será este miércoles 10 de marzo a las 10:00 en Casa Gris.

El encuentro se producirá tres semanas después del último, que tuvo lugar el 17 de febrero pasado. En esa oportunidad, los secretarios generales de UPCN y ATE anunciaron la concreción del proceso de pase a planta de los contratados y el inicio de la discusión salarial.

Sin embargo, ese día no hubo propuesta de aumento por parte de la Provincia, por lo que para la reunión de mañana las expectativas están puestas casi en su totalidad en ese aspecto.

Esta semana, los sindicatos docentes recibieron una oferta de incremento del 35% en tres tramos. Tras el encuentro, el ministro de Trabajo Juan Manuel Pusineri no descartó que el ofrecimiento para los empleados estatales sea similar.