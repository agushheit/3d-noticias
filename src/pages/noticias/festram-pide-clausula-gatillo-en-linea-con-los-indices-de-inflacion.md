---
category: Agenda Ciudadana
date: 2021-02-19T06:01:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/festram.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Festram pide cláusula gatillo en línea con los índices de inflación
title: Festram pide cláusula gatillo en línea con los índices de inflación
entradilla: El gremio afirmó que la pérdida del poder adquisitivo de los trabajadores
  del sector fue del 16% en 2020. Sigue a la espera de los porcentajes de la paritaria
  nacional. Pasaron a cuarto intermedio.

---

En un encuentro virtual se llevó adelante este jueves la paritaria municipal. De la mesa de negociación participaron intendentes y presidentes comunales y representantes de los trabajadores municipales. Por el gobierno provincial formó parte de la reunión José Luis Freyre de la secretaría de Municipios y Comunas.

Festram, tal como había adelantado, puso en juego la reactivación de la cláusula gatillo como mecanismo para que los salarios suban al ritmo de la inflación, aunque la propuesta gremial fue que se otorgue de manera "transitoria".

Los dirigentes señalaron que ya hay una pérdida del 16 por ciento del poder adquisitivo, en relación con la inflación de 2020, y que este porcentaje debería estar en la base de la negociación, teniendo en cuenta, además, que en enero 2021 la inflación fue de 4,8 por ciento y que esto ya debería estar liquidándose con los haberes de febrero.

La cláusula gatillo se dejó de aplicar la semana pasada y por el momento los representantes locales no manifestaron ninguna respuesta.

Además, resolvieron esperar la evolución de las paritarias nacional y provincial, tanto de los docentes como de los trabajadores estatales.

Sobre el encuentro, José Luis Freyre señaló al móvil de LT10 que se trató de un primer acercamiento, donde se expuso la situación económica de la provincia y la importancia de mejorar las condiciones laborales de los trabajadores.

"Los representantes de los trabajadores pidieron que mientras se defina una propuesta se analice la manera de compensar la pérdida del poder adquisitivo", agregó el funcionario.

La negociación continuará el miércoles que viene al mediodía, luego del cuarto intermedio definido en el encuentro de hoy.