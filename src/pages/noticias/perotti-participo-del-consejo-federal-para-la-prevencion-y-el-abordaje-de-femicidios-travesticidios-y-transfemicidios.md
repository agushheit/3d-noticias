---
category: Agenda Ciudadana
date: 2021-07-20T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/FEMICIDIOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti participó del Consejo Federal para la prevención y el abordaje de
  femicidios, travesticidios y transfemicidios
title: Perotti participó del Consejo Federal para la prevención y el abordaje de femicidios,
  travesticidios y transfemicidios
entradilla: 'El gobernador afirmó este lunes que “todos estos días estamos viendo
  cómo las acciones de gobierno y los sistemas de salud luchan para salvar vidas y
  cuidar la vida de nuestra gente. '

---
El gobernador de la provincia de Santa Fe, Omar Perotti, junto con las ministras nacionales de Mujeres, Género y Diversidades, Elizabeth Gómez Alcorta; y de Seguridad, Sabina Frederic; y el ministro de Justicia y derechos Humanos, Martín Soria; encabezó este lunes la Reunión Regional de las Provincias de la Región Centro, del Consejo Federal para la Prevención y el abordaje de Femicidios, Travesticidios y Transfemicidios.

En la oportunidad, Perotti destacó que “todos estos días estamos viendo cómo las acciones de gobierno y los sistemas de salud luchan para salvar vidas y cuidar la vida de nuestra gente. Sin dudas, este encuentro también tiene que ver con salvar vidas”.

“Por eso la importancia que le damos y el deseo de potenciar pasos que hemos dado en la provincia con la jerarquización del área y con trabajar con el presupuesto participativo con perspectiva de género”, agregó el gobernador.

“Además hay que destacar el trabajo en el territorio, que para nosotros es lo más directo, tanto en la capacitación en el cumplimiento de la Ley Micaela, como en los Puntos Violetas. Son 40 iniciativas que estamos trabajando en esta primera parte, para estar allí en la prevención de cada una de estas acciones que queremos desterrar de la provincia”, finalizó Perotti.

**Violencia por motivos de género**

En tanto, la ministra de las Mujeres, Género y Diversidades de la Nación, Elizabeth Gómez Alcorta, indicó que “es una prioridad del gobierno nacional atender tanto las desigualdades por motivos de género como su contracara, que son las violencias por motivos de género”; y destacó la “decisión política de avanzar con una deuda que tenemos, tanto social como en términos estatales, por lo que nos abocamos a pensar una estrategia estatal y gubernamental que piense los déficit que tenemos como Estado”.

Por este motivo, “pensamos esta mesa, entendiendo que además de la complejidad del fenómeno, hay complejidad en las intervenciones estatales, porque están vinculadas a la asistencia del Ejecutivo, en seguridad y del Poder Judicial, que nos llevó a pensar que teníamos que estar en una misma mesa para pensar cómo hacemos”.

Por último, Alcorta detalló que “construimos los indicadores básicos de evaluación de riesgo para casos de violencia por motivos de género, para hacer intervenciones más eficientes y rápidas; y avanzamos en un sistema integral de intervención de casos, al que ya adhirieron 17 provincias”.

Por su parte, la ministra de Igualdad, Género y Diversidad, Celia Arena, destacó que “es un honor que nuestra provincia sea anfitriona de esta reunión”; y manifestó que este Consejo Federal “es un espacio muy amigable porque tenemos un trabajo codo a codo con cada una de las provincias”.

“Para nuestro gobernador la cooperación regional y apostar a estas acciones en conjunto, para ir viendo las cuestiones que compartimos y cómo podemos usar las experiencias, son muy buenas y una indicación permanente de trabajo”.

Sobre el Consejo, resaltó que “une en un mismo espacio a los actores que intervienen en la lucha contra la violencia por razones de género, estamos avanzando muy fuerte en este esquema entendiendo que en la base hay muchas desigualdades que tenemos que trabajar. Necesitamos un Estado unificado ante las personas que atraviesan violencia por razones de género, y donde podamos ir mejorando nuestra accionar para dar este tipo de respuestas“, concluyó Arena.

**Consejo Federal para la prevención y el abordaje de femicidios, travesticidios y transfemicidios**

El Consejo Federal se creó con el objetivo de garantizar un abordaje integral y articulado con los distintos organismos del Estado Nacional, las provincias y la Ciudad Autónoma de Buenos Aires en materia de prevención, investigación, sanción, asistencia y reparación de los femicidios, travesticidios y transfemicidios y de otras violencias extremas por motivos de género.

La propuesta del Programa Interministerial de Abordaje Integral de las Violencias Extremas por Motivos de Género es promover la sistematización de las mediciones de riesgo en casos de violencia por motivos de género para avanzar hacia su predicción y así lograr prevenir las situaciones de violencias extremas.

Este sistema de indicadores será puesto a disposición de todos los organismos nacionales, provinciales y municipales, tanto gubernamentales como judiciales, que lleven adelante tareas de asistencia y recepción de consultas o denuncias en materia de violencias por motivos de género en todo el país.

**Presentes**

En la oportunidad, también estuvieron presentes, el presidente de la Corte Suprema de Justicia de Santa Fe, Roberto Falistocco; la jefa del Gabinete del Ministerio de las Mujeres, Género y Diversidad de la Nación, María Lara González Carabajal; la secretaria de Políticas contra la Violencia por Motivos de Género, Josefina Kelly; y la subsecretaria de Programas Especiales contra las Violencias de Género, Carolina Varsky.

Además, participaron ministras y ministros provinciales de la Región Centro. Por Santa Fe: el ministro de Gestión Pública de Santa Fe, Marcos Corach; el ministro de Gobierno, Justicia y Derechos Humanos, Roberto Sukerman; y el ministro de Seguridad, Jorge Lagna. Por Córdoba: el ministro de Seguridad de la Provincia de Córdoba, Alfonso Fernando Mosquera; y la ministra de Mujeres de la Provincia de Córdoba, Claudia Martínez. Por Entre Ríos: la ministra de Gobierno y Justicia, Rosario Romero; y la ministra de Desarrollo Social, Marisa Paira.

Participaron también la diputada provincial por Santa Fe Erika Hynes; y la legisladora provincial por Córdoba Alejandra Piasco; entre otras autoridades.