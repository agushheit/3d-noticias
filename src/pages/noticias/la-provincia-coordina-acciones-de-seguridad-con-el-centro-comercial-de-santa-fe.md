---
category: Estado Real
date: 2022-01-14T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/CENTROCMERCIAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La provincia coordina acciones de seguridad con  el Centro Comercial de Santa
  Fe
title: La provincia coordina acciones de seguridad con  el Centro Comercial de Santa
  Fe
entradilla: El Ministerio de Seguridad y la Policía de la provincia se reunieron con
  los comerciantes de la ciudad capital, frente a diferentes acontecimientos sucedidos
  en los últimos días.

---
Este jueves al mediodía, funcionarios del Ministerio de Seguridad de la provincia y autoridades de la Policía de Santa Fe, se reunieron con representantes del Centro Comercial, para diagramar acciones de prevención frente a hechos de robos acontecidos en comercios en los últimos días.

En ese marco, el subsecretario de Prevención de la provincia, Gustavo Pucheta, destacó: “Estamos trabajando en aumentar el patrullaje de caminantes, nos gustaría tener más, pero todos sabemos que también estamos afrontando las consecuencias de la pandemia, y tenemos menos policías en la ciudad de Santa Fe, y eso hace que merme notoriamente nuestra capacidad operativa”.

Por su parte, el coordinador provincial de Seguridad Social, Facundo Bustos, indicó que se reunieron con el “Centro Comercial de Santa Fe, que nuclea diferentes avenidas dentro del ejido urbano de la ciudad”, y detalló que se trata de un encuentro que “se enmarca en estas reuniones coordinativas que tenemos de forma periódica con el centro comercial y otros actores. En este ida y vuelta, la idea es coordinar diferentes acciones porque en el último tiempo, hemos tenido algunos acontecimientos, y entendemos la preocupación de los comerciantes”.

Finalmente, el Subjefe de la Unidad Regional I, José Carruega, remarcó que “como encargados de la seguridad preventiva de la ciudad, hemos coordinado patrullajes en algunos sectores de la ciudad, teniendo en cuenta los hechos de robos calificados a comercios, que no se estaban dando, asi que estamos atendiendo la situación como se merece y con los recursos disponibles de la Unidad Regional, que están en la calle”, finalizó.