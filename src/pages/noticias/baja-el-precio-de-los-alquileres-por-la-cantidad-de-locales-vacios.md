---
category: La Ciudad
date: 2021-01-21T08:49:36Z
thumbnail: https://assets.3dnoticias.com.ar/baja_alquileres_comerciales.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Lt10
resumen: Baja el precio de los alquileres por la cantidad de locales vacíos
title: Baja el precio de los alquileres por la cantidad de locales vacíos
entradilla: Si bien, muchos negocios cerraron debido a la pandemia, para quienes resisten
  hay algo de alivio. Desde la Cámara Inmobiliaria de Santa Fe señalaron que al haber
  más oferta disminuyen los montos.

---
El impacto de la pandemia en el comercio provocó el cierre de gran cantidad de negocios. Ante la imposibilidad de abrir y generar ventas, muchos comerciantes debieron cerrar al no poder pagar el alquiler del local, entre otros gastos.

Aunque, para quienes resisten y buscan reponerse de golpe económico, hay algo de alivio.

Alberto Bottai, presidente de la Cámara Inmobiliaria de Santa Fe indicó por LT10 que ante el cierre de negocios, “baja el monto de los alquileres porque hay más locales desocupados”.

Más allá de algunos rubros esenciales que pudieron mantener sus ventas, “el comercio se atrasó y todos dejaron de percibir ganancias”. Es por eso que muchos inquilinos no pudieron cubrir el monto total del alquiler, y al haber una mayor oferta de locales desocupados se produce una disminución en los precios de los arrendamientos.

En este sentido, sostuvo la importancia de no agravar los efectos de la pandemia y fomentar un diálogo entre propietarios e inquilinos para ver cada situación, “siempre entre la oferta y la demanda la gente entiende y creo que ha sido la mejor manera de conducirnos”, concluyó.