---
category: Agenda Ciudadana
date: 2021-09-19T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/RIOJA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Presidente retoma el lunes su agenda con las juras de los ministros
title: El Presidente retoma el lunes su agenda con las juras de los ministros
entradilla: En la capital de esa provincia Alberto Fernández encabezó este sábado
  una reunión con gobernadores.

---
El presidente Alberto Fernández permanecerá el resto del fin de semana junto a la primera dama Fabiola Yañez en la provincia de La Rioja, en cuya capital hoy encabezó un encuentro de trabajo con gobernadores de todo el país.

El Jefe de Estado regresará el lunes a la Ciudad de Buenos Aires, donde retomará sus actividades de agenda, entre las que se encuentra la toma de juramento a los nuevos ministros del Gabinete nacional, prevista para las 1.

Alberto Fernández llegó hoy a La Rioja junto a la primera dama, acompañado por los ministros del Interior, Eduardo De Pedro; de Obras Públicas, Gabriel Katopodis; de Desarrollo Social, Juan Zabaleta; y el designdo titular de Educación, Jaime Perczyck.

También viajaron el presidente de la Cámara de Diputados, Sergio Massa; el secretario General de la Presidencia, Julio Vitobello; el jefe de asesores presidenciales, Juan Manuel Olmos; y la primera dama, Fabiola Yañez.

El viaje del Presidente a La Rioja se produce luego de que anoche anunciara la nueva conformación del Gabinete nacional.

En el marco de estos cambios, el gobernador de Tucumán Juan Manzur fue designado como jefe de Gabinete en reemplazo de Santiago Cafiero, que estará a cargo de la Cancillería.

Los cambios se completan con Aníbal Fernández al frente del Ministerio de Seguridad; Julián Domínguez, en Agricultura, Ganadería y Pesca; Jaime Perczyck, en Educación; y Daniel Filmus, en Ciencia y Tecnología, en tanto Juan Ross será el secretario de Comunicación y Prensa; se informó oficialmente pasadas las 22 de ayer.

A todos estos designados funcionarios, el Presidente les tomará juramento el lunes a las 16 en Casa de Gobierno.