---
category: Deportes
date: 2021-02-15T03:38:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/union.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: La salvación llegó sobre la hora
title: La salvación llegó sobre la hora
entradilla: El Tatengue y el Decano igualaron 2-2 en Santa Fe por la primera fecha
  de la Copa de la Liga

---
Gracias a un gol de tiro libre de Marcos Borgnino,Unión de Santa Fe logró empatar sobre la hora en el

partido de la primera fecha de la Copa de la Liga Profesional de Fútbol, que fue cambiante y entretenido por

la cantidad de goles y las situaciones creadas por ambos conjuntos.

Ramiro Carrera abrió el marcador para la visita a los 17'. Luego, a los 30' llegó el empate de Ezequiel Cañete.

Pero 10 minutos después Santiago Vergini volvió a adelantar a los tucumanos.

Cuando parecía que los 3 puntos tenían un rumbo,llegó la estupenda ejecución de Borgnino, que de tiro

libre anotó la paridad final en el minuto 48 de la segunda etapa.

En la próxima fecha, Unión visitará a Huracán el lunes 22 de febrero desde las 19.15.

**FORMACIÓN:**

**UNIÓN - ATLÉTICO TUCUMÁN**

_Unión:_ Sebastián Moyano; Federico Vera, Brian Blasi, Jonathan Galván, Juan Portillo; Ezequiel Cañete, Juan Nardoni o Nelson Acevedo, Marco Borgnino; Mauro Luna Diale, Fernando Márquez y Gastón González.

_Atlético Tucumán_: Cristian Lucchetti; Álvaro Lagos, Santiago Vergini, Guillermo Ortíz, Gabriel Risso Patrón; Guillermo Acosta, Cristian Erbes, Franco Mussis, Ramiro Carrera; Kevin Isa Luna y Javier Toledo.

Hora: 21.30

TV: TNT Sports

Árbitro: Darío Herrera