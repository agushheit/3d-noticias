---
category: Agenda Ciudadana
date: 2021-10-12T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/equipo.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El ministerio de educación realizó un balance de la implementación del boleto
  educativo rural
title: El ministerio de educación realizó un balance de la implementación del boleto
  educativo rural
entradilla: Equipos técnicos de la cartera educativa provincial mantuvieron un encuentro
  de trabajo para analizar los alcances del programa. Desde su inicio se registraron
  18.000 solicitudes para acceder a este derecho.

---
Con la premisa de seguir garantizando el transporte rural de estudiantes, docentes y asistentes escolares, diferentes equipos técnicos del Ministerio de Educación mantuvieron un encuentro de trabajo para analizar los alcances de la implementación del Boleto Educativo Rural (BER).

La secretaria de Gestión Territorial Educativa, Rosario Cristiani, junto al director provincial de Educación Rural, Ubaldo López y la directora provincial de Equidad y Derechos, Vanina Flesia, realizaron un balance de las diferentes instancias llevadas adelante para la concreción de este beneficio que permite que estudiantes, docentes o asistentes escolares de todos los niveles y modalidades puedan trasladarse al establecimiento educativo rural al que asisten a clases o trabajan diariamente, siempre que esté situado en el ámbito rural, en un medio de transporte acordado con el Municipio o Comuna correspondiente.

En este sentido, las y los funcionarios provinciales realizaron un balance desde la etapa de inscripción que se dio al inicio del año, la instancia de sensibilización que involucró reuniones de presentación del programa para la posterior firma del convenio, y ahora con los fondos depositados la etapa de implementación en cada una de los municipios y comunas que firmaron el convenio recibieron las partidas asignadas.

Al respecto, Cristiani señaló que “nosotros pretendemos que todas las niñas, niños y adolescentes en edad de educación obligatoria estén en las aulas. Por ello el Boleto Educativo Rural es un sistema que se establece para remover el obstáculo que muchas veces significa el traslado hacia las escuelas y de este modo garantizar el derecho a la educación”.

Por su parte, Flesia explicó la importancia de la reunión indicando que “estamos ajustando los mecanismos y dando respuestas a los a los inconvenientes que van surgiendo con un balance positivo porque la mayoría de las comunas y municipios el BER se está implementando sin inconvenientes. Ahora estamos la etapa de evaluación y monitoreo en dónde vamos a tener instancias evaluatorias con los municipios y comunas y con los referentes que estuvieron trabajando del Ministerio de Educación en el territorio, de cara a la implementación del programa en el año 2022”.

**El BER en números**

Desde el inicio del programa Boleto Educativo Rural, se registraron 18.000 solicitudes para acceder a este derecho. Los equipos técnicos del Ministerio de Educación llevaron adelante 15 reuniones de presentación del programa y 28 encuentros departamentales para la firma de convenios. En ese marco, se rubricaron un total de 323 convenios con comunas y municipios de la Provincia de Santa Fe.

A lo largo del 2021, 295 localidades ya recibieron el pago de los dos primeros trimestres, sumando un total de $ 424.597.312,22 entregados. Con todas ellas se fueron realizando reuniones entre los referentes BER en el territorio, las y los directivos de escuelas y los representantes de los gobiernos locales para organizar la ejecución de los fondos y el acceso a este derecho de manera ajustada a la trama de transporte que se da en cada circuito rural.