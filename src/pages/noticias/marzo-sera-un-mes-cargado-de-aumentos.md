---
category: Agenda Ciudadana
date: 2021-03-02T06:38:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/nafta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: Marzo será un mes cargado de aumentos
title: Marzo será un mes cargado de aumentos
entradilla: Marzo comenzará con aumentos en prepagas, naftas, útiles escolares y VTV,
  que pondrán más presión a la inflación y comprometerán la meta del 29% pautada en
  el Presupuesto, en medio de las negociaciones salariales.

---
Con relación a febrero, las consultoras privadas estimaron una inflación en un rango de entre 3,1% y 3,5%, inferior a los resultados de diciembre y enero pero igual de preocupante para las proyecciones oficiales.

El Ministerio de Economía apunta a terminar el año con un Índice de Precios al Consumidor inferior al 31%, pero si en febrero el costo de vida arroja lo esperado por los analistas, en los diez meses restantes del año el promedio mensual no debería superar el 2% para alcanzar el objetivo.

En ese escenario, el Gobierno busca un acuerdo de precios y salarios para intentar contener la suba de precios, especialmente en alimentos, que impacta de manera directa en la medición de la pobreza e indigencia. 

Según un informe de la consultora LCG, los alimentos registraron en febrero un incremento del 4,1% y con ese nivel el arrastre estadístico para marzo se ubica por encima del 1%. 

Ese 29% parece aún más difícil de poder alcanzar, si se tienen en cuenta los aumentos registrados en febrero y aquellos que están previstos para marzo.

También subirá la tarifa de la Verificación Técnica Vehicular (VTV), en un 45% y en el sistema de estacionamiento por parquímetros el incremento será del 100%.

En marzo llegarán también los ajustes en las matrículas y las cuotas de colegios privados y, según informó la Asociación de Entidades Educativas Privadas Argentinas (ADEEPRA), ya se autorizaron incrementos del 15% en la provincia de Buenos Aires y del 25% en la Ciudad Autónoma de Buenos Aires. 

Se registrará también el aumento que el Gobierno autorizó en las cuotas de la medicina prepaga que será del 3,5%.

Además, volverán a actualizarse los impuestos específicos que gravan la nafta y el gasoil, al igual que el valor de los biocombustibles, si la administración de Alberto Fernández no lo posterga.

Estos ajustes podrían impactar, según especialistas del mercado, en un 2,6% en el litro de nafta y un 2,1% en el del gasoil sobre el precio de referencia de CABA.

En marzo, por el cambio de estación, se esperan también aumentos en la indumentaria, calzado y marroquinería. 

En cuanto a los útiles, según informó la consultora Focus Market, la canasta escolar de 23 artículos, arrancó en febrero con un ajuste promedio que supera al 45%, mientras se espera que sigan en alza para marzo.

Para contener el alza en los precios, la Secretaría de Comercio Interior presentó el programa "Vuelta al Cole con Precios Cuidados", que establece valores de referencia para la canasta escolar antes del inicio del ciclo lectivo, que estará vigente hasta el 31 de marzo, con un aumento promedio del 21%.