---
category: La Ciudad
date: 2021-02-03T05:36:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/fae.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'FAE: El municipio ratifica la deuda de $ 145 millones y definió un plan
  de pagos'
title: 'FAE: El municipio ratifica la deuda de $ 145 millones y definió un plan de
  pagos'
entradilla: |2-

  La cifra corresponde al período 2013-2019 y se propone saldar en 73 cuotas iguales y consecutivas. La oferta fue presentada a la Comisión del FAE en noviembre de 2020.

---
Con el objetivo de ponerse al día con el Fondo de Asistencia Educativa (FAE), la Municipalidad de Santa Fe presentó a la Comisión un convenio de pago para saldar la deuda de $145.708.788,24. La cifra corresponde al período 2013-2019 y se propone saldar en 73 cuotas iguales y consecutivas.

El FAE está conformado por una comisión administradora tripartita, integrada por la Municipalidad de Santa Fe a través de un delegado que ejerce la presidencia de FAE, un delegado del Ministerio de Educación de la provincia de Santa Fe como secretario y un delegado de la Federación de Cooperadoras Escolares como tesorero.

“La propuesta de pago se realizó en noviembre de 2020 y luego de esperar una respuesta del resto de las instituciones que integran la Comisión, el 29 de diciembre la Federación de Cooperadoras Escolares nos presentó una nota aduciendo la deuda que hicieron pública a través de los medios, de 187 millones”, contextualizó Paulo Ricci, secretario de Educación y Cultura del municipio, sobre la situación que atraviesa el FAE, que se destina para obras y mantenimiento de escuelas públicas.

**Investigación municipal**

Tras la solicitud de la Federación, la Municipalidad inició una investigación desde la Secretaría de Hacienda que arrojó la deuda actual, que es la reconocida oficialmente y “para la cual ya habíamos presentados una oferta de pago de 73 cuotas. Es algo que no se hacía desde hacía casi 10 años en la Municipalidad de Santa Fe, esto de reconocer la deuda y de ofrecer una alternativa para saldarla”, expresó Ricci. Sobre la información entregada a la Federación, detalló que “está todo visible en un expediente que se le presentó a la Comisión FAE para que sea explicado y entendido”.

Desde el municipio indicaron que el origen de las cifras que la Federación de Cooperadoras Escolares toma como base para establecer el monto adeudado corresponde al devengado no pagado, es decir, a la deuda existente al cierre de cada ejercicio anual, el cual en su totalidad no refleja el pago de deudas de un período determinado en años siguientes. De esta manera, el total de lo devengado no pagado entre 2013 y 2019 asciende a $ 276.777.280,03, mientras que el monto abonado entre 2014 y 2020 de dicho periodo es de $ 131.068.941,79, siendo lo que resta por saldar un monto de $ 145.708.788,24.

Ricci afirmó que “la Comisión FAE es el espacio natural de trabajo para sanear todas las dudas y para discutir el destino de los fondos, para hablar de la deuda y para ordenar también qué queremos hacer con los fondos municipales que están destinados para el mantenimiento y mejoras de las escuelas públicas de Santa Fe, y también contribuir al enriquecimiento de la política educativa que tiene la ciudad”. Por esa razón destacó que "una de las prioridades de ésta gestión fue darle una verdadera dinámica de trabajo y participación a la Comisión FAE que junto a la Municipalidad integran la Provincia y la Federación de Cooperadoras Escolares”.

 

**Prioridad**

El secretario de Educación y Cultura recordó que el primer encuentro con la Comisión del FAE se realizó a pocos días de que asumiera Emilio Jatón al frente del Ejecutivo Municipal, en diciembre de 2019: “Nosotros convocamos a una primera reunión de la Comisión FAE para darle actividad y darle funcionamiento. No se pudo conformar esa Comisión hasta los meses de julio y agosto de 2020 porque el Ministerio de Educación de la Provincia no designó hasta ese entonces delegado de la Regional IV y la silla de provincia la tiene que ocupar alguien en representación de la ministra de Educación”, explicó Ricci. Por esta razón, al estar la comisión incompleta “no se pueden ejecutar gastos ni mover fondos porque se requiere la firma de los tres representantes”.

Ricci afirmó que “la Comisión FAE es el espacio natural para sanear todas las dudas, para discutir el destino de los fondos, para hablar de la deuda y para ordenar también qué queremos hacer con los fondos municipales que están destinados para el mantenimiento y mejoras de las escuelas públicas de la ciudad de Santa Fe, y también contribuir al enriquecimiento de la política educativa que tiene la ciudad de Santa Fe”. En ese sentido, destacó que “lo importante es seguir sosteniendo ese espacio de diálogo, de trabajo y de articulación con otras organizaciones y otros gobiernos como es el provincial y la Federación de Cooperadoras Escolares como el ámbito natural de trabajo, de discusión y de resolución de los problemas”.  

El funcionario municipal también se refirió a la deuda de 2020, que se conocerá entre abril y mayo, cuando cierre el ejercicio presupuestario: “Tenemos absoluta voluntad de sentarnos a trabajar en una propuesta de pago de esa deuda” y adelantó que “en relación con el ejercicio 2021, nosotros vamos a empezar a transferir cada mes todos los fondos dinerarios que corresponden a la Comisión FAE como querríamos haber hecho si no hubiésemos encontrado la Municipalidad tan delicada en términos presupuestarios y si no nos hubiese tocado lidiar, a tres meses de haber asumido, con una pandemia que frenó absolutamente todos los ejercicios presupuestarios que estaban previstos para el año 2020”, concluyó.