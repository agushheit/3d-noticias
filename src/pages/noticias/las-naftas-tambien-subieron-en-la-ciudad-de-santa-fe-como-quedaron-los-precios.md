---
category: Agenda Ciudadana
date: 2020-12-16T12:34:22Z
thumbnail: https://assets.3dnoticias.com.ar/nafta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'Las naftas también subieron en la ciudad de Santa Fe: cómo quedaron los
  precios'
title: 'Las naftas también subieron en la ciudad de Santa Fe: cómo quedaron los precios'
entradilla: Tal como se anunció desde la medianoche, la petrolera de bandera nacional
  aumentó el precio de los combustibles. Se trata del quinto aumento que se registra
  desde agosto. Los precios en Santa Fe.

---
Tal como se había anunciado, desde la medianoche **YPF aumentó el precio de las variedades de naftas y gasoil en todas sus estaciones de servicio del país**, lo que incluye la actualización del componente impositivo, según informaron fuentes de la compañía. El promedio estimado fue del 4,5%.

Se trata del **quinto aumento que se registra desde agosto**, luego de un período de congelamiento de precios desde el 1 de diciembre del año pasado, con lo cual el incremento ronda el 20% de acuerdo a zona del país y marca.

Fuentes de la petrolera informaron que el aumento de los productos de la marca reflejarán el traslado de actualización de impuestos sobre los combustibles líquidos y al CO2 que representan un incremento del 1,3% en el caso de las naftas súper y premium y del 1% en las variedades de gasoil, y un aumento del precio de los combustibles.

**YPF es la que marca prácticamente la política de precios en el segmento de venta al público** con el 57% de participación del mercado, por lo que se descarta que las otras petroleras darán a conocer sus aumentos en las próximas horas.

El último aumento de precios se produjo el 24 de noviembre mientras que, las subas anteriores fueron el 16 de octubre; el 19 de septiembre, y el 19 de agosto.

En Santa Fe ya se pueden ver las subas de precios en YPF y se esperará que en las próximas horas la sigan las otras petroleras.

## **La Pizarra de YPF en Santa Fe**

• **Súper:** <span style="color: #3681D1;">$72,10</span>

• **Infinia:** <span style="color: #3681D1;">$81,70</span>

• **Infinia Diesel:** <span style="color: #3681D1;">$75,10</span>

• **Diesel 500:** <span style="color: #3681D1;">$64</span>

![](https://assets.3dnoticias.com.ar/tabla-nafta.jpg)