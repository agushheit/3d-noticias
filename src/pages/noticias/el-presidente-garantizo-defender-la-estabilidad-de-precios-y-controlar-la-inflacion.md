---
category: Agenda Ciudadana
date: 2021-10-21T06:15:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/CASAROSADA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Presidente garantizó defender la estabilidad de precios y controlar la
  inflación
title: El Presidente garantizó defender la estabilidad de precios y controlar la inflación
entradilla: El jefe de Estado se reunió en la Casa Rosada con representantes de la
  CTA Autónoma y les aseguró que defenderá con políticas activas la estabilidad de
  los precios.

---
El presidente Alberto Fernández se reunió este miércoles en la Casa Rosada con representantes de la CTA Autónoma y les aseguró que defenderá con políticas activas la estabilidad de los precios para controlar la inflación y cuidar a las familias argentinas.  
  
Fernández relató que solo una minoría de empresas bloqueó el acuerdo de precios y defendió la resolución dictada porque incluye una amplia canasta de bienes de consumo masivo necesarios para que tengan un precio justo, estable y seguro, informaron fuentes oficiales.  
  
"Nos encaminamos a poner de pie a la Argentina sin perder de vista todo lo que hicimos para contener la situación social durante la pandemia, un trabajo coordinado entre el Estado y las organizaciones sociales", remarcó el Presidente, y añadió: "El futuro tenemos que encararlo con una distribución que debe hacerse de otro modo".  
  
El mandatario también dialogó con los sindicalistas acerca del programa de Compre Social, el régimen del monotributo para las economías populares y los créditos no bancarios.  
  
Por parte de la CTAA asistieron el secretario general, Ricardo Peidro; el adjunto, Hugo Godoy, y la adjunta Claudia Baigorria, entre otros.  
  
Tras la reunión, Godoy dijo a la prensa que se habló "de tres medidas en concreto para los sectores mas postergados" por parte del Gobierno, y enumeró "el monotributo social, el tema de los préstamos para cooperativas para incentivar la producción que hoy no tienen acceso al mercado de financiamiento y la paritaria de salud".  
  
Sostuvo que el Presidente "se comprometió a tomar ese tema porque los trabajadores de la salud son los más sufridos y los que más requieren una respuesta y mejora a nivel salarial y de equiparación de las condiciones laborales en todo el país y dijo que va a promover una reunión con la ministra de Salud, Carla Vizzotti, por este tema".  
  
"También alentamos a que haya medidas que fortalezcan el mercado interno y la distribución y él coincidió con esa mirada", consignó. Y apuntó que Fernández "reafirmó la idea de que no va a haber negociación de la deuda afectando los intereses del pueblo argentino".  
  
Los gremialistas plantearon además que "las paritarias se tiene que mantener abiertas para garantizar el objetivo de que se esté por encima de la inflación".  
  
Sobre el proyecto del salario básico universal que la central presentó al jefe de Gabinete, Juan Manzur, y al Presidente, Godoy contestó que para el mandatario "la mayor dificultad es como reunificarlo con el conjunto de los planes sociales que existen", pero "no se niega a pensarlo".  
  
En cuanto a los precios fustigó que "hay sectores que son muy egoístas, que no hacen ningún aporte solidario". Añadió que "planteamos que se tiene que extender todos los años el impuesto a las grandes fortunas, que solamente fue por una vez".  
  
Godoy contó que el Presidente resaltó "la decisión de seguir priorizando a los sectores de menores ingresos y de alentar la participación de todos los actores sociales las distintas centrales, los movimientos sociales, las pequeñas y medianas empresas", y aseguró que "el Consejo de la lucha contra el hambre sigue siendo un eje principal de la política del Gobierno".  
  
Por su parte, Baigorria destacó que "estamos las organizaciones sindicales y sociales para poder tomar intervención y participación porque los sectores más mezquinos terminan perjudicando a los sectores populares".  
  
Y Peidro señaló que Fernández "nos dio aliento, dijo que eran importantes las propuestas y reclamos que llevamos porque ayuda a equilibrar la balanza de los poderosos".