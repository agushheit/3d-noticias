---
layout: Noticia con imagen
author: "Fuente: Prensa UNL"
resumen: Debate sobre Reforma Judicial
category: La Ciudad
title: Ricardo Gil Lavedra disertará sobre la Corte Suprema y el traslado de jueces
entradilla: Este sexto encuentro será el 9 de noviembre, a las 18:30hs, a través
  de la plataforma Zoom y se transmitirá en simultáneo por el canal de YouTube
  de la FCJS. La actividad es gratuita y abierta a todo público.
date: 2020-11-07T15:08:27.148Z
thumbnail: https://assets.3dnoticias.com.ar/glavedra.jpg
---
El lunes 9 de noviembre de 2020, se llevará a cabo el **sexto encuentro del Seminario Permanente de Debate sobre Reforma Judicial**, en esta oportunidad con la **disertación de Ricardo Gil Lavedra**.

En el encuentro, que será a las **18:30hs**, se abordará el tema "Seis sugerencias sobre la Corte Suprema de Justicia de la Nación Argentina".

El seminario se realizará a través de la plataforma Zoom, por lo tanto quienes deseen participar deberán ingresar con la siguiente información:

**ID de la reunión:** 915 6882 7387

**Contraseña:** 075852

En caso de que se vea colmada la capacidad de la reunión, se podrá ver el encuentro en simultáneo en el [canal de YouTube](https://www.youtube.com/channel/UC-gzsRJCQ3pT4NK6ULoAnLg) de la Faultad de Ciencias Jurídicas y Sociales (FCJS) de la UNL, ingresando [AQUÍ](https://www.youtube.com/channel/UC-gzsRJCQ3pT4NK6ULoAnLg).

El seminario es organizado por las facultades de: Ciencias Jurídicas y Sociales de la Universidad Nacional del Litoral (UNL); de Derecho de la Unviersidad Nacional del Nordeste (UNNE); de Derecho de la Universidad Nacional del Sur (UNS); de Derecho y Ciencias Sociales de la Universidad Nacional de Tucumán (UNT); de Derecho de la Universidad Nacional de Catamarca (UNCA); y por la carrera de Especialización en Derechos Humanos y Acceso a la Justicia de la Universidad Nacional de San Luis (UNSL).

![](https://assets.3dnoticias.com.ar/glavedra1.jpg "Seminario Debate sobre Reforma Judicial")

**Informes:** Secretaría de Posgrado | FCJS

**E-mail:** posgrado@fcjs.unl.edu.ar