---
layout: Noticia con imagen
author: "Fuente: El Litoral"
resumen: Vuelos Santa Fe - Buenos Aires
category: La Ciudad
title: El 31 de octubre vuelven los vuelos Santa Fe - Buenos Aires
entradilla: El gobierno provincial autorizó el regreso de los vuelos de
  cabotaje, en sintonía con la decisión nacional, y la empresa Aerolíneas
  Argentinas será la primera en retomarlos.
date: 2020-10-22T15:05:30.309Z
thumbnail: https://assets.3dnoticias.com.ar/vuelos.jpg
---
Por el momento, solo habrá una frecuencia semanal, los días sábados. El primer viaje está previsto para el próximo 31 de octubre, desde y hacia Ezeiza porque Aeroparque se encuentra cerrado por refacciones.

Silvana Serniotti, interventora del Aeropuerto Metropolitano Sauce Viejo, dialogó con El Litoral y explicó cómo será el estricto protocolo que se aplicará en medio de la pandemia de Covid-19.

En esta etapa, solo podrán viajar las personas esenciales o quienes deban realizar un tratamiento médico, con un certificado de hisopado o un test PCR negativos hecho 72 horas antes del vuelo, el certificado único de habilitación para circular y una declaración jurada del estado de salud, que se baja desde la aplicación móvil de Provincia, argumentando el motivo del viaje y el estado de salud.

Serniotti aclaró que se podrá permanecer por 72 horas en el territorio provincial, sin sociabilizar y se exime de hacer la cuarentena de 14 días. Quien deba permanecer más días, sí deberá cumplir con ese periodo de aislamiento. Además, podrán viajar las personas residentes en otras provincias del país y deseen volver. En estos casos sí deberá cumplir con la cuarentena obligatoria de 14 días.

Dentro del aeropuerto de Sauce Viejo, los protocolos serán estrictos en cuanto a las medidas sanitarias: solo podrá ingresar la persona que viaje, salvo menores o quien tenga una discapacidad; se sanitizará el calzado, se controlará la temperatura corporal y el distanciamiento, y será obligatorio el uso del barbijo, incluso arriba del avión. Habrá una posta sanitaria dentro del aeropuerto, donde se derivará cualquier caso sospechoso.

La interventora remarcó que el Poder Ejecutivo provincial se mantiene muy firme en la postura de que estas medidas serán evaluadas de forma permanente según la evolución de la situación epidemiológica porque la prioridad es la salud de la población.