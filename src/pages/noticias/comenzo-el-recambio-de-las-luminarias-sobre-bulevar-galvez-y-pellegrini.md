---
category: Agenda Ciudadana
date: 2021-04-24T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/Iluminacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Comenzó el recambio de las luminarias sobre Bulevar Gálvez y Pellegrini
title: Comenzó el recambio de las luminarias sobre Bulevar Gálvez y Pellegrini
entradilla: " Para este programa, que abarca una gran cantidad de barrios, está prevista
  una inversión cercana a los 187 millones de pesos."

---
La Municipalidad continúa con el Plan de Iluminación previsto para el primer semestre de 2021. Ya comenzó el reemplazo de las luminarias sobre bulevar Gálvez y Pellegrini, desde Vélez Sarsfield hasta avenida Freyre. En total se cambiarán unos 200 artefactos led que ya estaban obsoletos y el sistema comenzó a tener inconvenientes técnicos por lo que será también sustituido.

Este programa tiene como premisas la recuperación de alumbrado existente, incluyendo el mantenimiento de la red, y la nueva iluminación para barrios que fueron históricamente privados de un servicio eficiente. Es por eso que se prevén intervenciones de dos tipos: la actualización tecnológica, mediante el recambio de luces de sodio por led, y el nuevo sistema de alumbrado mediante columnas y equipos led. El desembolso del municipio rondará los 187 millones de pesos.

En cuanto al primer punto, se comenzó por todo el corredor de bulevar y luego se continuará por calle Rivadavia donde se repondrán unas 40 luminarias; y la misma tarea se hará por avenida Alem, donde la situación es similar. Más tarde se hará lo mismo en otras avenidas como Gorriti, Galicia, entre otras.

“Se arrancó con el cambio del sistema de iluminación tanto de bulevar Gálvez como de Pellegrini donde se cambiarán todas las luminarias que quedaron obsoletas, los sistemas comenzaron a tener inconvenientes técnicos que hicieron al desgaste de la iluminación existente y por eso el reemplazo”, detalló Matías Pons Estel, gerente ejecutivo de Gestión Urbana del municipio.

En esta línea, el funcionario agregó: “El plan de iluminación comprende varios lugares de la ciudad. La próxima semana comenzamos con los trabajos que se licitaron en barrios como Scarafía, Liceo Norte, entre otros. Además de los cambios de las luminarias en sí, es un recambio en los sistemas tecnológicos”.

Antes de finalizar, Pons Estel contó que “la inversión total del plan de iluminación es de alrededor de 187 millones de pesos y, de este total, un porcentaje va para el cambio de tecnología e iluminación y una parte es para el mantenimiento diario”.

**Plan de iluminación**

Sobre las intervenciones de nueva iluminación, la primera etapa contempla varios barrios de la ciudad para los cuales ya se licitaron las tareas, mientras que, para otros, se hará la apertura de sobres en pocos días. La semana que viene comienza la obra en Scarafía, donde se instalarán 100 columnas a lo largo de 32 cuadras y la inversión ronda los 12 millones de pesos.

También se comenzará a trabajar en Liceo Norte, donde se instalarán 61 columnas, en unas 20 cuadras y con un presupuesto de unos $ 7,5 millones. En pocos días se licitará la obra para Santa Marta con 84 columnas y la jurisdicción de la vecinal Ceferino Namuncurá para la avenida 12 de Octubre. A esto se suman las inversiones en columnas con artefactos led en barrio Roma, Fomento 9 de Julio, y en el entorno de los hospitales de la ciudad.

Sin lugar a dudas, las inversiones más grandes se darán en Guadalupe Noreste, en Siete Jefes y en Yapeyú Oeste, cuyas licitaciones ya se concretaron y luego del análisis de las propuestas, se adjudicarán para comenzar las tareas. En el primer barrio, se instalarán 405 columnas con equipos led, cableado y tableros; en el segundo serán 310 columnas; y el tercero contempla unas 103 columnas.