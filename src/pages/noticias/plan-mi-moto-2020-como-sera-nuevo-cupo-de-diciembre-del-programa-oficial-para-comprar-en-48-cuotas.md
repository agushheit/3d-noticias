---
category: Agenda Ciudadana
date: 2020-12-01T11:15:38Z
thumbnail: https://assets.3dnoticias.com.ar/MI-MOTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: 'Plan “Mi Moto 2020”: como será nuevo cupo de diciembre del programa oficial
  para comprar en 48 cuotas'
title: 'Plan “Mi Moto 2020”: como será nuevo cupo de diciembre del programa oficial
  para comprar en 48 cuotas'
entradilla: Se lanzará esta semana. Extienden los valores hasta unidades de $ 250.000
  y reducen el plazo de 15 a 7 días para confirmar la compra.

---
En los próximos días el Gobierno nacional dará a conocer la segunda edición del plan de financiamiento para la compra de motos en cuotas y a tasa subsidiada “Mi Moto”.

La nueva edición llegará con algunos cambios a pedido de los sectores productivos y luego de la experiencia anterior en donde en 72 horas se acabó el cupo de 5.500 solicitudes de crédito.

El apuro tiene que ver con varias razones. La primera es que la llegada del buen clima implica el inicio de la temporada alta para la venta de motovehículos. La segunda es que la suba del costo de transporte incentiva la demanda en las ciudades del interior, donde concentra la mayor cantidad de ventas de unidades. Y la tercera es que la nueva normalidad en donde la población busca evitar el transporte público, las vuelve más atractivas.

El Gobierno está terminando de definir cuestiones operativas por lo que estiman que esta semana, a más tardar los primeros días de la semana que viene se podrá anunciar la nueva apertura del cupo, porque la nueva edición del programa llega con algunos cambios.

El primero tiene que ver con el tope de los modelos a financiar. En la edición anterior se había establecido que eran unidades de fabricación nacional con un precio límite de hasta 200.000 pesos pero, inflación mediante, el nuevo monto a financiar es de motovehículos de hasta 250.000 pesos.

El otro punto que se modificará es el de los plazos entre la pre adjudicación del crédito del Banco Nación y el cierre de la operación.

En la primera edición el plazo era de 15 días pero lo que sucedió es que de las 5.500 motos que se habían puesto a la venta se terminaron realizando operaciones por poco menos de 2.000 ya que no se terminaron de cerrar las operaciones y no podían ingresar nuevos compradores porque el cupo está ocupado.

![](https://assets.3dnoticias.com.ar/FINANCIACION-MI-MOTO.jpg)En esta edición el plazo entre la aceptación del Banco Nación y la compra de la unidad es de 7 días. Si en ese tiempo no se cerró la compra, la moto vuelve al cupo para que pueda acceder un nuevo comprador.

Lo que se mantiene igual es el requisito respecto de la nacionalidad del vehículo, ya que tiene que ser de fabricación nacional.

Lo mismo en lo referente a la tasa y las cuotas, en donde se mantiene un plazo de 48 meses calculadas por el sistema francés y para los clientes que perciben sus haberes en el banco, la tasa es del 41,17% de costo financiero total efectivo anual. Para aquellos que no acreditan haberes en el banco, asciende a 57,09% de costo financiero total efectivo anual.

El crédito está destinado a cualquier “persona humana apta para obligarse. Previo al desembolso del préstamo se deberá contar con al menos una Caja de Ahorro (o bien, deberás abrir una a través de la aplicación del Banco Nación). Además, establece que sólo se podrá adquirir una moto por cliente.

  
 

En el caso de los clientes con cuenta sueldo en el Banco Nación los compradores puede destinar hasta el 35% de los ingresos netos para el crédito, pero, para los clientes del sistema previsional como para el público en general que tenga cuenta pero no cobre su sueldo, hasta el 30% de los ingresos netos del solicitante.

El Banco sólo financiará el monto de la Moto, no financiará los gastos de flete, patentamiento y cualquier otro cargo que genere la operación.

El crédito sólo se puede destinar a la adquisición de motos 0 km (cero kilómetro) de Fabricación Nacional comercializadas por concesionarias adheridas a la presente operatoria y que serán informadas al Banco por la Secretaría de Industria de la Nación.