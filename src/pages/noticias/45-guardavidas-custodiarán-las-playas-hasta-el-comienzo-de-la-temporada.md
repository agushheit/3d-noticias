---
layout: Noticia con imagen
author: "Fuente: El Litoral"
resumen: "Playas con guardavidas "
category: La Ciudad
title: 45 guardavidas custodiarán las playas hasta el comienzo de la temporada
entradilla: A partir de este domingo, habrá un operativo especial en los
  balnearios de la ciudad. Lo mismo ocurrirá en Santo Tomé y Rincón. La
  inauguración de la temporada fue postergada para diciembre
date: 2020-11-14T18:04:40.765Z
thumbnail: https://assets.3dnoticias.com.ar/guardavidas.jpg
---
Con la postergación del inicio de la temporada de playas para el mes de diciembre, existe un riesgo latente en las playas santafesinas debido a que con el calor y el buen tiempo la gente comenzó a concurrir a tomar sol y algunos hasta se tiran al agua. Por este motivo, este domingo se inicia un operativo preventivo en las playas, con presencia de guardavidas, para evitar ahogamientos ante la posible imprudencia de los bañistas.

"Comienzan el domingo 45 guardavidas a hacer prevención en la playa, esto al menos será así hasta que Provincia nos habilite el espacio público. Mientras tanto haremos capacitaciones con los chicos, viendo cómo manejar las situaciones de acuerdo a nuestros protocolos que fueron aprobados a nivel provincial y nacional", le comentó a El Litoral Axel Menor, director de Deportes de la Municipalidad, tras la reunión que tuvo en la tarde del viernes con los socorristas.

**Prevención**

En la Costanera Este, por ejemplo, la bajante del río hizo que los pozos y barrancas profundas sobre la costa se transformen en un latente peligro. Y pese a la prohibición de ingreso al agua, mucha gente lo hace igual y así ocurren las tragedias. Por este motivo, lo que se busca ahora con la presencia de guardavidas es prevenir estas acciones.

En el mismo sentido, Santo Tomé y San José del Rincón también dispondrán de presencia de guardavidas en las playas desde este domingo. La idea es similar a la de Santa Fe. En el caso de Rincón será la inauguración oficial de la temporada, aunque sin la posibilidad de darse un "chapuzón" en el río Ubajay.

**A diciembre...**

La postergación del inicio de la temporada se debe a la presencia de la pandemia. Fue una decisión tomada por los municipios y el gobierno provincial. Lo que se busca mientras tanto es ajustar los protocolos sanitarios a implementar en dichos espacios públicos para evitar contagios del virus Sars-Cov-2, que genera la enfermedad covid-19.

De esta forma, Santa Fe no tendrá esta temporada su tradicional habilitación del 15 de noviembre, como es habitual que ocurra cada año, en el marco del aniversario de la fundación de Santa Fe. Este atípico 2020 se celebrará el 447º aniversario de la ciudad fundada en 1573 por Juan de Garay.