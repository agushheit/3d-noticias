---
category: Agenda Ciudadana
date: 2021-01-27T10:10:01Z
thumbnail: https://assets.3dnoticias.com.ar/mercado_libre.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de La Nación
resumen: 'Mercado Libre quebró su récord en Wall Street: triplicó su valor en un año
  y superó los US$100.000 millones'
title: 'Mercado Libre quebró su récord en Wall Street: triplicó su valor en un año
  y superó los US$100.000 millones'
entradilla: Marcos Galperin es el fundador de Mercado Libre; la empresa nació en 1999
  y ahora es la más valiosa de América Latina.

---
Es la empresa más valiosa de la Argentina y su cotización hoy parece no tener techo. Mercado Libre, la firma fundada por Marcos Galperin en 1999, superó los US$100.000 millones de valuación en Wall Street y se consolidó como la más importante de la región.

Fue el viernes pasado cuando la acción de Mercado Libre operó en alza y empujó hacia arriba a la valuación de la empresa. Sobre el cierre de la rueda, ese valor retrocedió, pero luego rebotó a comienzos de esta semana. Con las cifras actuales, la firma que nació dedicada al comercio electrónico y luego se expandió a la logística y el negocio financiero, triplica su valor de hace un año.

Según marcan los registros históricos de la bolsa de Nueva York, Mercado Libre valía US$653 millones en enero de 2019. El boom en Wall Street y el impulso a su actividad que generó la pandemia, con cierre de comercios y una masiva adopción de las plataformas digitales para comprar bienes, contratar servicios y concretar transacciones financieras, infló el valor de la empresa.

![](https://assets.3dnoticias.com.ar/5258.jpg)

Así llegó a superar los US$2000 por acción -en enero de 2019 estaban US$342-, un récord histórico para la mayor empresa del país y de la región. Con su valor actual, se ubica en el primer lugar entre las firmas latinoamericanas, por encima de Vale, la minera de origen brasileña cuya valuación supera los US$86.000 millones.

De todas maneras, el escenario negativo de los mercados internacionales que afecta a los índices bursátiles en Wall Street y golpea a las empresas argentinas también afectó a Mercado Libre. Pasado el mediodía, la acción perdía hoy un 3,5% y así la valuación total de la empresa retrocedía a US$95.000 millones.

**Mercado Libre: gran ganador de la pandemia**

La estructura del negocio de Mercado Libre también explica parte de su expansión. Porque más allá de haber sido fundada en Buenos Aires y tener sus oficinas centrales en esta ciudad, hoy la empresa de Galperin obtiene más del 50% de su facturación por su operación en Brasil, la mayor economía de América Latina. Allí compite con Amazon en el segmento del comercio electrónico y recientemente comenzó a operar una flota de aviones para acelerar la logística y el reparto de paquetes en ese país. Además de la Argentina, el otro gran mercado para la firma es México.

En los primeros nueve meses de 2020, la empresa acumuló una facturación total de US$2646,1 millones, con un incremento del 63% en dólares con respecto a igual período de 2019.

Con estos números, revirtió su escenario de negocios. Entre enero y septiembre de 2019, acumulaba pérdidas por US$84,3 millones, pero en igual período de 2020 anotó una ganancia de US$152,8 millones. A fines de febrero, la firma difundirá su balance financiero correspondiente al cuatro trimestre de 2020 y compartirá sus números anuales.

En este escenario postivo, Mercado Libre también enfrenta un escenario financiero holgado que le permite escapar a la coyuntura argentina y el riesgo país que se mantiene por encima de los 1400 puntos. Días atrás, la empresa colocó deuda por US$1100 millones en dos instrumentos con tasas del 2,375% (un bono ambiental que vence en 2026) y 3,125% (con vencimiento en 2031).