---
category: Agenda Ciudadana
date: 2021-10-09T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/universidad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Autorizaron las actividades presenciales plenas en las universidades: qué
  pasa en Santa Fe'
title: 'Autorizaron las actividades presenciales plenas en las universidades: qué
  pasa en Santa Fe'
entradilla: La resolución publicada este jueves en el Boletín Oficial, faculta a las
  autoridades provinciales a reanudar las actividades según la situación epidemiológica

---
El Ministerio de Educación de la Nación aprobó la reanudación de las actividades académicas presenciales en universidades e institutos universitarios de todo el país tras eliminar el distanciamiento de 1,5 metros que era obligatorio dentro de las aulas en el marco de la pandemia de coronavirus.

Así lo estableció mediante la resolución 3043/2021, publicada con la firma del ministro de Educación, Jaime Perczyk.

“Establecer que la efectiva reanudación de las actividades académicas presenciales en universidades e institutos universitarios será decidida por las autoridades provinciales y de la Ciudad Autónoma de Buenos Aires, según corresponda, quienes podrán suspender las actividades y reanudarlas conforme a la evolución de la situación epidemiológica”, dice la norma en su artículo 2.

Asimismo, deja sin efecto a la resolución 1084 del 8 de agosto de 2020 que establecía el protocolo marco y lineamientos generales para el retorno a las actividades académicas presenciales.

En su artículo 3 indica que “las instituciones universitarias pondrán en conocimiento de las autoridades de la Secretaría de Políticas Universitarias las medidas que se adopten en el marco de lo referido en el artículo precedente”.

En este sentido, el texto señala que “resulta factible promover la presencialidad plena en las instituciones universitarias, considerando siempre las medidas sanitarias emanadas por las autoridades jurisdiccionales y nacionales en la materia”.

**Qué pasa en la Universidad Nacional del Litoral**

Enrique Mammarella, rector de la UNL, sostuvo por LT10: “Desde hoy vamos a poder planificar la presencialidad y su grado de intensidad, en el marco del cuidado de la salud. Si bien ya veníamos trabajando con presencialidad ya no tendremos que coordinar con Nación. La bimodalidad llegó para quedarse porque es algo bueno para mejorar las enseñanzas. Esto va a depender de las asignaturas y cursos”.

Además, agregó: “Estamos trabajando para tener aulas híbridas que permitan escuchar las clases en el aula o participar a distancia. Pero esta decisión de Nación, nos permite incrementar la presencialidad en nuestros laboratorios, talleres y gabinetes”.

Consultado sobre cuántos alumnos podrían volver a las aulas, el rector manifestó que según una encuesta propia, el 75% de los alumnos estaban en Santa Fe o pretendían regresar a la presencialidad.