---
category: Agenda Ciudadana
date: 2020-11-30T15:54:27Z
thumbnail: https://assets.3dnoticias.com.ar/JUBILADOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Jubilaciones: el aumento de 5% de diciembre será un solo pago a cuenta'
title: 'Jubilaciones: el aumento de 5% de diciembre será un solo pago a cuenta'
entradilla: Se descontará en los haberes que reciban a partir de marzo. Así lo establece
  el proyecto de ley enviado por el Gobierno al Congreso.

---
El Poder Ejecutivo envió al Congreso el proyecto definitivo de movilidad previsional. Y el texto incluye una sorpresa. El Gobierno tiene previsto descontarles a los jubilados, a la hora de dar un aumento a los haberes en marzo de 2021, el 5% que se otorgará el mes próximo según lo que se anunció y se dispuso por el decreto 899, publicado el miércoles último en el Boletín Oficial.

  
 

De esta manera, el incremento del último mes de este año no será en realidad más que un anticipo de las recomposiciones a otorgar en 2021 y no una suba genuina previa a la implementación de una nueva modalidad de cálculo de las actualizaciones.

La fórmula que el Poder Ejecutivo busca que apruebe el Congreso está basada en la variación de la recaudación de recursos tributarios que se destinan a la Anses dividida por la cantidad de beneficios y en la evolución de los salarios formales según el índice Ripte.

Para determinar el porcentaje del mes de marzo se considerará lo ocurrido con las variables en el segundo semestre de este año. Pero en el caso de 2021 y de aprobarse la cláusula mencionada, al resultado que arroje el cálculo se le descontará el aumento de diciembre próximo. Y luego, para determinar la recomposición de septiembre (habrá dos en todo el año) se aplicará un tope, por el cual la suma de los aumentos de marzo y de septiembre no podrá superar a la variación, incrementada en un 3%, que haya tenido en un período de doce meses la recaudación de recursos totales de la Anses, según indica La Nación.

El descuento previsto del porcentaje que se otorgará en diciembre responde a que la nueva fórmula será semestral y, por tanto, "en marzo se actualizarán los haberes por el período de octubre de 2020 a marzo de 2021". Así, según la explicación, se está considerando que "lo correspondiente al cuarto trimestre de este año ya habrá sido pagado". Por lo tanto, puntualizaron las fuentes, "ese aumento de diciembre va a cuenta de lo que será el de marzo".