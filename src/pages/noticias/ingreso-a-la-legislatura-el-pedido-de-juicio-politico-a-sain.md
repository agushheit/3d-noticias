---
category: Agenda Ciudadana
date: 2021-03-12T07:01:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/sain1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Ingresó a la Legislatura el pedido de juicio político a Sain
title: Ingresó a la Legislatura el pedido de juicio político a Sain
entradilla: Es por injurias y mal desempeño de funcionario público. La Coalición Cívica
  hizo efectiva la presentación contra el ministro de Seguridad.

---
Luego de conocerse los audios del ministro de Seguridad Marcelo Sain, en los que denigra a los policías santafesinos en un nuevo exabrupto del funcionario, la diputada nacional Lucila Lehmann adelantó que presentaría un pedido de juicio político en la Legislatura provincial.

El mismo ingresó el martes a la Cámara de Diputados y según detalló a LT10 su par provincial Sebastián Julierac los argumentos son el mal desempeño de funcionario público e injurias.

En cuanto al mal desempeño de Sain, Julierac manifestó que "en lo que va de la gestión desmejorado la seguridad en toda la provincia de Santa Fe, con una cantidad de asesinatos que superó a 2019 que ya tuvo índices terroríficos".

Sobre la injuria sostuvo que trató de "cualquier cosa" no solo a la fuerza policial santafesina sino a todos los ciudadanos. "Hay una percepción errada de algunas personas que piensan que llegan a la función pública y pueden ejercer el poder sin límites y sin rendir cuentas", afirmó Julierac, quien agregó respecto a Sain que "es un funcionario que no tiene límites ni rinde cuentas y cree que la función pública está al servicio de su capricho".

Con esta presentación la Cámara baja puede darle lugar y conformar la acusación junto al Senado, que se convierte en Cámara acusadora. "Ahora vamos a ver si todos esos legisladores que declaran que este funcioanrio no tiene que estar más le dan lugar a este pedido o se callan como pasa muchas veces", apuntó Julierac.

Recordemos que el integrante de la Coalición Cívica mantiene aún su reclamo para asumir la banca que fue ocupada por la diputada Cesira Arcando (FE) en el marco de un planteo por paridad luego de las elecciones de 2019 y manifestó que sigue trabajando ad honorem mientras resuelve su proscripción.