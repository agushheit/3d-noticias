---
category: Agenda Ciudadana
date: 2021-10-24T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/aumentopolicia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La Provincia anunció un nuevo aumento de salario para el personal policial
  y de la tarjeta alimentaria
title: La Provincia anunció un nuevo aumento de salario para el personal policial
  y de la tarjeta alimentaria
entradilla: El personal policial recibirá un nuevo incremento salarial con el sueldo
  de octubre, que suma un acumulado del 45% anual.

---
El gobierno provincial, a través del Ministerio de Seguridad, anunció el aumento en el monto mensual de la Tarjeta Alimentaria para el Personal Operativo, que desde octubre es de $5.724 pesos por mes para los más de 14 mil agentes que realizan tareas operacionales, lo que suma una inversión de más de 77 millones de pesos por mes. Esta tarjeta, creada mediante el Decreto N° 1.168 de 2020, asiste económicamente, de manera directa y complementaria al personal policial que cumple de manera efectiva tareas operacionales de prevención, investigación y de control policial. Los fondos están destinados a que el personal policial pueda realizar compras de productos alimenticios, de forma renovable mes a mes, por medio de una tarjeta magnética en los comercios adheridos y mediante la modalidad débito automático.

Junto a esto, la cartera de Seguridad provincial también informó que con los sueldos de octubre, a pagarse los primeros días de noviembre, el personal de la Policía de la provincia de Santa Fe percibirá un incremento salarial. Es decir que, desde noviembre, el salario para un ingresante a la policía, es decir un suboficial con tareas operacionales, asciende a un monto bruto de $87.200 pesos.

**Nuevos beneficios para la Policía**

Sumado a lo anterior, el Ministerio de Seguridad también informó la firma de diferentes decretos durante este año, para contribuir al bienestar del personal policial de la provincia.

Por Decreto N° 205 del corriente año, se otorgó la Asignación Extraordinaria por Escolaridad, con un monto de $ 5.000 por cada hijo en edad escolar, que beneficia a 10.113 agentes. Esta asignación representa una inversión de más de $50 millones por parte del gobierno provincial. Además, este año también se otorgó una suma de $16.500 para cada agente destinado a la reposición de uniformes, que significa una inversión de $ 352 millones de la provincia.

Finalmente, este año también se concretaron los Concursos de Ascenso Policial pendientes del año 2018 de la jerarquía de Director General. También están en proceso los ascensos del año 2019 (segundo tramo); y los del año 2020 para todos los grados con más de 4.000 vacantes.