---
category: Deportes
date: 2021-09-10T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/ARGENTINABOL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: TyC Sports
resumen: 'La Selección Argentina, de fiesta al ritmo de Messi: goleó a Bolivia ante
  sus hinchas'
title: 'La Selección Argentina, de fiesta al ritmo de Messi: goleó a Bolivia ante
  sus hinchas'
entradilla: La Albiceleste se impuso por 3-0 con tantos de la Pulga, por Eliminatorias,
  con 21 mil fanáticos en el Monumental.

---
La Selección Argentina vivió una noche mágica para festejar la coronación de la Copa América: se impuso por 3-0 a Bolivia, con tres goles de Lionel Messi, en condición de local por la décima fecha de las Eliminatorias rumbo al Mundial de Qatar 2022.

Fue el regreso de los hinchas a las tribunas del fútbol argentino. Hubo 21 mil personas en el Monumental: enloquecieron con la Pulga, ovacionaron a Lionel Scaloni y Ángel Di María e incluso recordaron a Diego Armando Maradona, inmortal (y justo a los 10 del primer tiempo).

En la primera mitad, la Albiceleste dominó desde el inicio. Y golpeó de la mano de su capitán. Lionel Messi frotó la lámpara y abrió el marcador a los 13 minutos: caño a Luis Haquín y una excelente definición contra el palo. Golazo. La Pulga festejó besándose la camiseta.

Luego, la Albiceleste -que jugó al ritmo del público- tuvo otras chances claras. A Lautaro Martínez le anularon un gol por off side y luego se perdió una chance increíble. Más adelante, el astro rosarino casi marca el segundo: un remate que pasó al lado del palo.

Por su parte, Bolivia tuvo pocas aproximaciones. Sin embargo, estuvieron cerca del empate, tras unos pases errados de Leandro Paredes y Rodrigo De Paul: la pelota le quedó a Henry Vaca, quien la tiró por encima del travesaño.

En la segunda mitad, la Albiceleste siguió dominando el partido. Primero, avisó con remates de De Paul y la Pulga. Y, a los 18, logró estirar la ventaja de la mano de su capitán. Lionel Messi -tras una gran jugada individual y una conexión con Lautaro- definió y marcó el 2-0, para el delirio de los hinchas en el Monumental.

Si algo faltaba para que la noche fuera absolutamente mágica, llegó el tercero del Diez: Lampe dio un rebote tras un bombazo de Paredes y Messi anotó el tercero, a los 42 minutos del segundo tiempo.

La Selección Argentina festejó la coronación de la Copa América, ante sus hinchas, que vivieron una noche inolvidable de la mano de Lionel Messi. Con este triunfo, los campeones de América llegan a 18 puntos en las Eliminatorias. Ahora, afrontarán una nueva triple fecha de Eliminatorias, en octubre: se medirán con Paraguay, Uruguay y Perú.