---
category: Estado Real
date: 2021-06-14T06:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/ACTUALIZACION.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia convoca a productores a actualizar sus datos en el registro
  único de producciones primarias
title: La Provincia convoca a productores a actualizar sus datos en el registro único
  de producciones primarias
entradilla: 'Se realiza vía web desde el 15 al 30 de junio, es necesario contar con
  dicha inscripción para llevar adelante gestiones administrativas en los organismos
  públicos provinciales. '

---
El Ministerio de Producción, Ciencia y Tecnología informa a todos los productores primarios santafesinos que desde el próximo martes 15 y hasta el miércoles 30 de junio, inclusive, se encuentra disponible el sistema para actualizar sus datos en el Registro Único de Producciones Primarias (RUPP).

El trámite, que debe realizarse vía web, emitirá una constancia actualizada de inscripción, necesaria para realizar gestiones administrativas en los organismos públicos provinciales.

Accediendo al siguiente enlace, www.santafe.gov.ar/rupp, el productor debe ingresar con su CUIT/CUIL y contraseña generada en la declaración jurada (DDJJ) anterior. Si no recuerda la contraseña, el sistema le realizará algunas preguntas y en caso de contestarlas correctamente, mostrará la clave recuperada.

**DATOS A CARGAR**

Los datos personales que fueron declarados no deben cargarse nuevamente, ya que los mismos quedaron registrados. Sí se deberá actualizar y/o rectificar lo declarado anteriormente en cuanto a los establecimientos productivos y las respectivas producciones primarias que en ellos se realicen. Esto no implica confeccionar una nueva declaración jurada, sino rectificar la anterior.

Además, el productor podrá declarar si fue víctima en el último cuatrimestre de algún delito rural y redireccionarse desde el mismo RUPP a un link al Ministerio de Seguridad para ampliar su declaración.

**ACTUALIZACIÓN ANUAL**

Es obligación del productor que desarrolla su actividad primaria en territorio santafesino, cumplir con las tres actualizaciones anuales previstas para la segunda quincena de los meses de febrero, junio y octubre.

Contar con información productiva es clave para los gobiernos provincial y locales, de modo de identificar y planificar las políticas públicas necesarias para el sector.

Se trata, además, de una herramienta indispensable para que los productores puedan denunciar si fueron víctimas de algún tipo de delito en su establecimiento productivo.

Para consultas dirigirse a: rupp@santafe.gov.ar ó santafe.rupp@gmail.com.

Teléfono: 0342 4505300, interno 4181/4103.

**CAPACITACIÓN A MUNICIPIOS Y COMUNAS E INSTITUCIONES**

Desde la cartera productiva se informa también que se realizará una capacitación destinada a municipios y comunas e instituciones de la provincia que tiene como objetivo brindar herramientas para que desde cada gobierno local o entidad puedan facilitar la inscripción de productores en el Registro.

Aquellas localidades interesadas pueden inscribirse completando el siguiente formulario: [https://docs.google.com/forms/d/e/1FAIpQLSd111rNpCHg4bLAFMBROthuW9lsOlvpon7WLZAeFe8ml5zcMQ/viewform?usp=sf_link](https://docs.google.com/forms/d/e/1FAIpQLSd111rNpCHg4bLAFMBROthuW9lsOlvpon7WLZAeFe8ml5zcMQ/viewform?usp=sf_link "https://docs.google.com/forms/d/e/1FAIpQLSd111rNpCHg4bLAFMBROthuW9lsOlvpon7WLZAeFe8ml5zcMQ/viewform?usp=sf_link")