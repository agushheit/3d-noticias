---
category: Agenda Ciudadana
date: 2022-12-12T08:13:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivojpeg.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: El Transporte urbano por colectivos perdió casi la tercera parte de kilómetros
  recorridos con respecto a 2019.
title: El Transporte urbano por colectivos perdió casi la tercera parte de kilómetros
  recorridos con respecto a 2019.
entradilla: 'Los datos proporcionados por  SUBE, comparan primer semestre de 2019
  con primer semestre del corriente año, donde se verifica una disminución de 2.717.810
  kilómetros, que en porcentajes implica un 28.13% de pérdida. '

---
A comienzos del año 2016 convivían, en la ciudad de Santa Fe, varios sistemas para la cancelación del boleto:  monedas, una tarjeta local (Operadora Santa Fe) y la SUBE. Recién en septiembre de ese año, se unificaron todos los medios de pago en la tarjeta SUBE, lo que permitió contar con datos sólidos y constantes para evaluar la performance del sistema de colectivos.

Vale la aclaración: antes de la implementación del sistema SUBE para las rendiciones mensuales a los gobiernos locales, provinciales y nacionales, las empresas utilizaban DECLARACIONES JURADAS con los datos mensuales.  

Los kilómetros transitados por la flota total, están compuestos por los recorridos, los desvíos por distintos motivos, la cantidad de unidades afectadas a esos recorridos, y las estacionalidades, como, por ejemplo, los meses de enero, febrero y julio, época de vacaciones. También influyen las decisiones gremiales:  medidas de fuerza, las asambleas etc., que determinan menores servicios que paralizan momentáneamente el servicio, lo que implica menor cantidad de kilómetros recorridos. 

Como veremos en la gráfica, MAYO DE 2020 fue el mes de menor cantidad de kilómetros recorridos por la flota total, a causa de una medida de fuerza de UTA que se extendió por 19 días.

![](https://assets.3dnoticias.com.ar/grafico para nota transporte ed.png)

(*) No se considera mayo del 2020 por el conflicto de UTA mencionado en párrafo anterior.

Tomando el tramo seleccionado, la primera vez que la flota recorre menos del millón de kilómetros, fue en enero del 2020, con 972.115 km, y por 28 meses consecutivos los valores estuvieron por debajo de ese valor. Recién en junio de este año, se superó, recorriendo 1.022.679,91.

Como se observa en el gráfico, existen dos periodos divididos por el mes de ENERO del 2020: uno que va desde septiembre de 2016 a diciembre de 2019 cuyos kilometrajes recorridos rondan entre 1.4 millones de máxima y 1 millón de mínima. Y el otro, desde enero 2020 a la actualidad, donde los kilometrajes de máxima son un poco más del millón, y los de mínima, 600 mil kilómetros. Como conclusión, podemos ver que no se logró aun recuperar los valores mínimos en forma sostenida, previo a 2020.

**_La menor cantidad de unidades en circulación; la eliminación de los recorridos de las líneas 20 y 21, el ramal Cabal de la línea 1, y la modificación en los recorridos de las líneas 2, 4 y 5, fueron decisiones que contribuyeron a la disminución de los kilómetros recorridos que aludimos en los párrafos anteriores._**