---
category: Agenda Ciudadana
date: 2021-08-02T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/RAWSON.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Variante Delta: el "caso cero" de Córdoba está internado por neumonía bilateral'
title: 'Variante Delta: el "caso cero" de Córdoba está internado por neumonía bilateral'
entradilla: El hombre, mayor de edad, se encuentra en el Hospital Rawson de la capital
  provincial. Contrajo el virus luego de un viaje por Perú.

---
El hombre identificado como el "caso cero" por ingresar la variante Delta de coronavirus a Córdoba, tras regresar de viaje desde Perú, permanece internado en un hospital de la ciudad con un cuadro de neumonía bilateral, informaron este domingo fuentes médicas.

El hombre de 62 años, oriundo de Perú, reside en Córdoba y tras regresar de viaje al país en un vuelo que llegó desde Lima, no respetó el aislamiento obligatorio y generó que el virus se propagara y contagiara al menos a 20 personas.

Además de él, una mujer, que resultó afectada por ser contacto estrecho, está internada en el Hospital Rawson de la capital cordobesa. El director del nosocomio, Miguel Díaz, comentó a los medios locales: "El día sábado fue ingresado el caso índice con neumonía bilateral grave y permanece bajo atención médica en la terapia intensiva del hospital. Durante la jornada del domingo, no se evidenció mejoría en el cuadro clínico". En tanto, la mujer internada atraviesa un cuadro de neumonía moderada.

Luego de conocerse el jueves pasado el positivo del hombre con la variante Delta, más de 800 personas están aisladas preventivamente por ser contactos de contactos estrechos de contagiados, mientras continúan los relevamientos y testeos focalizados en la comunidad educativa y en el entorno intrafamiliar de los portadores del virus, informaron desde el Ministerio de Salud local.

Asimismo, son cinco los detenidos e imputados como los presuntos responsables de originar el brote. Se trata del viajero que ingresó desde Perú a mediados de julio al país y otro ciudadano peruano que mantuvo contacto estrecho con el positivo, a los que se suman tres argentinos que no cumplieron con el aislamiento ni las normas sanitarias para evitar la propagación del virus, informó el Ministerio Público Fiscal (MPF).

El hombre de Perú fue el portador del virus a la provincia mediterránea, quien el 19 de julio arribó al Aeroparque Jorge Newbery proveniente de Lima con test negativo. Dos días después se realizó el control, seguimiento telefónico y se programó el testeo correspondiente al séptimo día, y el 26 de julio, en Córdoba, se realizó el test para el alta y el resultado fue positivo, informó el Gobierno provincial.

El peruano que viajó desde ese país "violó las normas de restricción" por no cumplir el aislamiento y el otro ciudadano por ser “contacto directo de éste y porque tampoco respetó las restricciones”, sostiene el sumario de la causa judicial tras la denuncia del Gobierno local.

En tanto, sobre los tres argentinos "se constató que no guardaron aislamiento a pesar de saber que tenían coronavirus con la variante Delta y abrieron sus comercios atendiendo a clientes", detalla el informe del MPF.

El brote de coronavirus hasta el momento se dio en un núcleo intrafamiliar y los aislados son personas mayores y menores con contactos de contactos estrechos con el viajero, internado tras complicarse su estado de salud.