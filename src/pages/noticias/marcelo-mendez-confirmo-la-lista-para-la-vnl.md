---
category: Deportes
date: 2021-05-18T21:24:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/voley.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa FEVA
resumen: MARCELO MÉNDEZ CONFIRMÓ LA LISTA PARA LA VNL
title: MARCELO MÉNDEZ CONFIRMÓ LA LISTA PARA LA VNL
entradilla: En la sexta y última semana de trabajo para la Selección Masculina Mayor
  en Buenos Aires, Marcelo Méndez confirmó la lista de 18 jugadores que afrontarán
  la Liga de Naciones en Rimini, Italia, desde el próximo 28 de mayo

---
En la sexta y última semana de trabajo para la Selección Masculina Mayor en Buenos Aires, Marcelo Méndez confirmó la lista de 18 jugadores que afrontarán la Liga de Naciones en Rimini, Italia, desde el próximo 28 de mayo. El combinado nacional partirá el próximo viernes rumbo a Europa y permanecerá dentro de la burbuja diseñada por FIVB hasta finalizar su participación en el certamen.

Los elegidos del entrenador son los armadores Luciano De Cecco, Nicolás Uriarte y Matías Sánchez; los opuestos Bruno Lima, Federico Pereyra y Luciano Palonsky; los centrales Sebastián Solé, Pablo Crer, Martín Ramos y Agustín Loser; los puntas Facundo Conte, Cristian Poglajen, Jan Martínez, Ezequiel Palacios, Nicolás Méndez y Nicolás Lazo; y los líberos Santiago Danani y Franco Massimino.

Méndez completó su grupo de trabajo la última semana con la llegada de Massimino, De Cecco y Solé, y continuó con un intenso ritmo de trabajo de cara al primer compromiso oficial de la Selección tras las suspensiones en el marco del COVID.

La VNL, que se disputará completa en Italia hasta el 26 de junio, constará de la ronda preliminar en formato de todos contra todos -15 partidos para cada equipo-, y luego tendrá a los mejores cuatro disputando las finales el 25 y 26 de junio. Argentina tendrá su debut el próximo 28 ante Brasil a las 16hs de nuestro país con transmisión de TyC Sports.

**CONVOCADOS VNL 2021**

* Luciano De Cecco
* Nicolás Uriarte
* Matías Sánchez
* Bruno Lima
* Federico Pereyra
* Luciano Palonsky
* Sebastián Solé
* Pablo Crer
* Martín Ramos
* Agustín Loser
* Facundo Conte
* Cristian Poglajen
* Jan Martínez
* Ezequiel Palacios
* Nicolás Méndez
* Nicolás Lazo
* Santiago Danani
* Franco Massimino