---
category: Agenda Ciudadana
date: 2022-11-18T15:22:47-03:00
thumbnail: https://d7kqr73uznmf6.cloudfront.net/2020-10-07.jpg
layout: Noticia con video
link_del_video: https://youtu.be/bmSRs5b_ia4
author: 3d noticias
resumen: Circunvalar Santa Fe y la oportunidad de repensar la ciudad.
title: Circunvalar Santa Fe y la oportunidad de repensar la ciudad.
entradilla: 'El Foro de Debate Metropolitano realizó una charla debate sobre el destino
  que tendrán los terrenos y el patrimonio de la traza ferroviaria que quedará liberada
  con la culminación del proyecto circunvalar ferroviario. '

---
El Foro de Debate Metropolitano realizó una charla debate sobre el destino que tendrán los terrenos y el patrimonio de la traza ferroviaria que quedará liberada con la culminación del proyecto circunvalar ferroviario.

“Pensamos que el destino que tendrán las vías, los terrenos y en definitiva que se hará con este patrimonio de la sociedad, amerita un debate democrático, entendemos que no hay motivos para impedir que soñemos una ciudad y área metropolitana diferente, con mayor conectividad, con más espacios públicos, con planes de vivienda inclusivos y no exclusivos, con monumentos y edificios preservados y potenciados para su aprovechamiento. Estamos convencidos que la posibilidad histórica que brinda este momento es fundacional”, expresan desde el Foro.

“En este sentido es que damos un paso hacia la apertura del debate: convocamos a los ciudadanos que quieran participar de esta búsqueda de consenso a acercarse y comenzar a dialogar sobre la sociedad que queremos y deseamos para nuestro futuro y el de nuestros hijos”.

Participaron de la charla, el especialista Sergio Ludueña integrante del Foro quien ha trabajado en Transporte de la Municipalidad; las arquitectas Mirta Blazcow, Paula Freyre y María Laura Bertuzzi integrantes de la Comisión de Desarrollo Urbano territorial del Colegio de Arquitectos Distrito 1 y la doctora arquitecta Alejandra Saus autora de la tesis doctoral: “Desafíos de la gestión local ante políticas nacionales de transporte; el caso de Santa Fe en la recuperación ferroviaria Argentina”

3Dnoticias habló con Sergio Ludueña luego de realizada la conferencia.