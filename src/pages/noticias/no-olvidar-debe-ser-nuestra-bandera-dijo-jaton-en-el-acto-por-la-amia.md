---
category: La Ciudad
date: 2021-07-17T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: "“No olvidar debe ser nuestra bandera”, dijo Jatón en el acto por la AMIA"
title: "“No olvidar debe ser nuestra bandera”, dijo Jatón en el acto por la AMIA"
entradilla: Se realizó una emotiva ceremonia este viernes en conmemoración del 27°
  aniversario del atentado.

---
Bajo la consigna “Porque tenemos memoria exigimos justicia”, la Municipalidad y la DAIA filial Santa Fe conmemoraron un nuevo aniversario del atentado terrorista contra la sede de la AMIA en Capital Federal. El acto se realizó este viernes, en la plazoleta “Danilo Villaverde” de la capital provincial, que lleva el nombre de la única víctima santafesina que se registró en el ataque a la mutual.

Durante la emotiva ceremonia, se hizo un llamado a mantener viva la memoria, alzar la voz para reclamar justicia y trabajar por la paz. En la oportunidad, se escuchó la sirena, como aquel 18 de julio de 1994, se dio lectura a la nómina de las 85 víctimas fatales y se ofrendaron flores.

En especial, se recordó la figura de Danilo Villaverde. El intendente, Emilio Jatón, les agradeció la presencia a los familiares de aquel santafesino que perdiera su vida en el atentado. En ese sentido, pidió “dejar algo muy claro: este no es un acto protocolar; este es un acto necesario, este es un acto imprescindible. Es un acto que tiene que ver con el valor humano de cada uno y por eso es tan importante que estemos aquí”, dijo.

Jatón recordó que “el 18 de julio de 1994, los argentinos sufrimos una enorme tragedia como sociedad. Aún hoy, 27 años después, ese ataque cruel nos sigue doliendo como aquel día y nos impulsa a seguir en la búsqueda de respuestas, de verdad y justicia. Nos convoca a honrar la memoria y recordar a quienes perdieron injustamente la vida”, agregó.

El intendente aseguró que “como comunidad nos vimos quebrados, interpelados, desencajados ante el asombro y el dolor; y nos hicimos preguntas: qué pasó, para qué, cómo, quiénes permitieron que esto sucediera. Quiénes son los responsables hoy, todavía no lo sabemos”, reflexionó.

Por ello, señaló que “hoy estamos aquí, como todos los años y como siempre, en este lugar, de pie, para enaltecer la memoria de los argentinos que murieron en la AMIA y de todos los que sufrieron heridas a causa de este hecho”. No obstante, insistió en que “como sociedad y como argentinos nos merecemos respuestas. Debemos seguir juntos y fortalecer la lucha y el reclamo de justicia. No olvidar debe ser nuestra bandera”.

El intendente pidió “ejercitar nuestra memoria y honrar a los inocentes trabajando con más fuerza que nunca para construir una sociedad más justa, más solidaria y con más respeto por el otro”. Y para concluir, instó a los presentes a “que me acompañen a seguir adelante en la construcción de una ciudad sin mezquindades y en la que no tengan lugar las expresiones de odio y violencia de ningún tipo”.

“**Asignatura pendiente”**

El presidente de la DAIA filial Santa Fe, Horacio Roitman, recordó que “durante 27 años hemos dicho, como hoy, ‘presente’ al nombrar a Danilo Villaverde, la única víctima santafesina y al escuchar los nombres de las restantes víctimas”. Y agregó que “27 años después, el espanto vuelve a estar a flor de piel en cada una de las víctimas, en cada uno de los sobrevivientes, en cada uno de sus familiares”, ya que “el éxito de un crimen se agiganta cuando se mantiene la impunidad”.

En ese sentido, aseguró que “es la impunidad vigente la que no permite cicatrizar la herida. Como hemos dicho reiteradamente, es una asignatura pendiente de nuestra democracia para con la sociedad argentina en su conjunto. Se prolonga en el tiempo sin reclamos y sin justicia”.

Del mismo modo, Roitman mencionó que aún hoy “prima la idea de que sólo fue un atentado a la comunidad judía y no se entiende que fue un atentado a todo el pueblo argentino. Es entonces cuando la memoria aparece como el último recurso frente a la desidia de la justicia, porque un pueblo sin memoria es un pueblo condenado a repetir su pasado”.

Por su parte, el ministro de Gobierno, Justicia y Derechos Humanos de la provincia, Roberto Sukerman, afirmó que todavía “seguimos reclamando memoria, verdad y justicia para que se pueda esclarecer y condenar a los culpables” del atentado.

**Participantes**

También estuvieron el secretario de Educación y Cultura de la municipalidad, Paulo Ricci; el director de Derechos y Vinculación Ciudadana, Franco Ponce de León; el presidente del Concejo Municipal, Leandro González; la presidenta de la comunidad israelita AMIA local, Susana Pitashny; la representante de la Sociedad Hebrea Sefaradí, Noemí Mena; y el presidente del Círculo Israelita Macabi, Andres Curtzer; junto con familiares de Danilo Villaverde, referentes de la Mesa de Diálogo Interreligiosa, I.L. Peretz y el INADI.