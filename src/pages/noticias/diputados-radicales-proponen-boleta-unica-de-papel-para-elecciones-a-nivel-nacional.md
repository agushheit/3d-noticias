---
category: Agenda Ciudadana
date: 2020-12-18T11:34:34Z
thumbnail: https://assets.3dnoticias.com.ar/1812diputados.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Parlamentario'
resumen: Diputados radicales proponen boleta única de papel para elecciones a nivel
  nacional
title: Diputados radicales proponen boleta única de papel para elecciones a nivel
  nacional
entradilla: La impulsora del proyecto, la porteña Dolores Martínez, sostuvo que se
  busca «dar mayor autonomía al votante» y «eficientizar el financiamiento de los
  partidos políticos».

---
La diputada nacional Dolores Martínez (UCR) presentó un proyecto de ley para **implementar la boleta única de papel**, que toma varias características de la boleta única que ya se usa a nivel provincial y local en Córdoba, de modo de hacerlo a nivel nacional, a través de una modificación del Código Electoral. Además, se introducen cambios en la Ley de Financiamiento de Partidos Políticos, la Ley de las PASO y la Ley Orgánica de Partidos Políticos.

«La boleta única busca reducir y optimizar el gasto en impresión de boletas, dar mayor autonomía al votante para decidir con una oferta electoral completa, y eficientizar el financiamiento de los partidos políticos», sostuvo la legisladora.

La diputada porteña resaltó que «el oficialismo parece dispuesto a discutir el sistema electoral y es por esto que nosotros proponemos expandir la discusión y tratar la herramienta con la que se vota, dando un salto de calidad con la implementación de la boleta única de papel», que cuenta con las experiencias de la citada provincia de Córdoba, y también Salta, Santa Fe y San Luis.

«Tomamos como referencia las experiencias locales pero también las internacionales. Muchos países han tenido elecciones este año: Nueva Zelanda, Estados Unidos, Bolivia, y todos utilizan boleta única de papel. Representa **menos gasto, menos discrecionalidades y mayor transparencia**, sin dejar de lado la importancia del rol de los partidos políticos y su fortalecimiento», destacó la radical.

El proyecto propone la creación de una boleta única permitiendo en caso de simultaneidad que los distritos subnacionales se sumen a ella, tanto para elecciones generales como para las primarias, abiertas, simultáneas y obligatorias; la impresión de las boletas por parte del Estado Nacional, igualando el acceso al cuarto oscuro por parte de los partidos políticos, y permite fácilmente distribución de la elección en las distintas categoría y órdenes.

«La boleta partidaria que utilizamos hoy ha sido una herramienta que ayudó a consolidar a los partidos políticos, de la mano de una rica dinámica interna. Cuando su relación con la ciudadanía se fue erosionando por la forma en las que muchos dirimían sus conflictos a espaldas de la sociedad, se produjo un distanciamiento, que se vio reflejado en una merma en la participación», señaló Martínez.

Y completó: «Con el crecimiento económico y las PASO hemos vuelto a ver altos números de participación, pero también una crítica al instrumento. La boleta única da igual acceso al cuarto oscuro a todos los partidos, brinda transparencia y es una manera más eficaz de invertir en nuestra democracia».

La iniciativa fue acompañada por los diputados radicales Carla Carrizo, Emiliano Yacobitti, Diego Mestre, Luis Pastori, Claudia Najul, José Luis Riccardo, Jimena Latorre, Federico Zamarbide, Ximena García, Juan Martín, Gonzalo del Cerro y Gabriela Lena.