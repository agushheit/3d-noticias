---
category: Agenda Ciudadana
date: 2020-12-03T10:55:32.000+00:00
thumbnail: https://assets.3dnoticias.com.ar/conectividad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Se viene un aumento en las tarifas de internet, cable y telefonía
title: Se viene un aumento en las tarifas de internet, cable y telefonía
entradilla: Las tarifas están congeladas desde el mes de agosto pasado y hasta el
  31 de diciembre próximo por disposición del Poder Ejecutivo nacional.

---
Las empresas que brindan servicios de Internet, cable y telefonía (fija y celular) definieron **aumentar 20 por ciento las tarifas a partir de enero** próximo, según le comenzaron a avisar a sus clientes.

Los costos de estos servicios se habían congelado hasta fin de año, en medio de la pandemia de coronavirus.

Las firmas proveedoras de Internet, televisión por cable y telefonía fija y móvil definieron aplicar esos ajustes, y aseguran contar ya con un primer visto bueno del Gobierno. Al respecto, **argumentan la necesidad de aplicar esos incrementos para “mantener la calidad del servicio”**.

“Con el fin de poder mantener la calidad de los servicios y en virtud de que la última actualización de precios fue realizada en el marzo de este año, informamos que el 1 de enero de 2021 modificaremos los mismos hasta un 20%”, informó en la factura de diciembre una reconocida empresa de cable e internet a sus clientes.

Otra empresa también informó en las facturas con vencimiento a noviembre que los precios se actualizarán hasta 20% a partir de enero.

En mayo, el Ente Nacional de Comunicaciones (Enacom) informó que habían llegado a un acuerdo con las empresas para que no hubiera aumentos hasta el 31 de agosto.

Pero cuando las compañías habían anunciado ya ajustes del 11% para septiembre, el presidente Alberto Fernández anunció que congelaba hasta el 31 de diciembre las tarifas de telefonía celular y fija, los servicios de acceso a Internet y a TV paga, además de declararlos servicios públicos esenciales.

“De esta manera garantizamos el acceso a los mismos para todos y todas”, señaló en ese momento el Presidente. Advirtió, además, que el Gobierno estaba “recuperando herramientas regulatorias que el gobierno anterior quitó al Estado”.

“El derecho de los usuarios y consumidores es un derecho constitucionalmente reconocido. En lo sucesivo, no podrá haber ningún aumento sin la previa aprobación del Estado”, recordó el mandatario.

Por medio del decreto 690/2020, el Gobierno declaró los servicios de telefonía, Internet y cable como “públicos, esenciales y estratégicos en competencia”, por lo que ahora podría fijar un tope a los aumentos.