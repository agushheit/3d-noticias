---
category: Agenda Ciudadana
date: 2021-11-03T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/ypf.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Estaciones de servicio alertan por el atraso en los precios y de un posible
  desabastecimiento
title: Estaciones de servicio alertan por el atraso en los precios y de un posible
  desabastecimiento
entradilla: Así lo señala la Federación Argentina de Expendedores de Nafta del Interior.
  Advierten la crítica situación que atraviesan las estaciones de bandera blanca.

---
A través de un comunicado, la Federación Argentina de Expendedores de Nafta del Interior (Faeni) señala que "pese a las advertencias lanzadas en estos últimos días, comunicados y reuniones, la situación que afecta a las estaciones, especialmente las blancas en relación al desabastecimiento de combustibles se complica".

Mediante una nota, la federación advierte que "los faltantes lejos de solucionarse parecerían haberse intensificado. En el interior del país, se detectan mangueras cruzadas sobre los surtidores como parte del paisaje urbano".

"Faeni alerta sobre el atraso de los precios a causa del congelamiento. En un escenario donde las petroleras no actualizan los valores, lo que sigue es dejar de enviar productos al mercado", advierten desde el sector.

"Toda esta situación está provocando que las petroleras empiecen por aumentar el precio mayorista, segundo a restringir el volumen y en una tercera etapa poner cuotas a todas las estaciones de servicio de bandera para traccionar hacia un aumento de los precios", destacaron.

"En consecuencia -continúa el comunicado- se puede observar un aumento de en los costos en el canal mayorista, en donde se abastece la industria, el agro y las estaciones de servicio independientes".

En ese sentido, manifestaron que "no pueden trasladar dicho aumento al precio de cartel, ya que las transformaría cada vez más en menos competitivas. Por otro lado, también se advierte que el siguiente paso es empezar a disminuir el volumen de venta del producto. Lo que afecta en primera instancia a las estaciones de línea blanca, que es lo que se está visualizando actualmente".

Desde la Federación apuntaron que "el escenario es grave, hoy estas estaciones se van quedando sin producto. Sólo tienen combustible dos días a la semana y luego no".

Por otro lado, la entidad hizo referencia al atraso en los precios: "Es visible, hoy es de un 10 a 12 por ciento en los precios pero el impacto en las estaciones es muy fuerte. Del contacto con los estacioneros FAENI advierte que con este congelamiento y sin llegar a los niveles de venta prepandemia, el contexto no es alentador. Por eso, manifestamos la necesidad urgente de solucionar este tema. Es difícil no aumentar en un contexto de inflación, día tras día cae fuerte la rentabilidad".

"Por estos días se ha expresado la situación a la Secretaría de Comercio Interior, por lo que desde este ámbito se adelanta que se analizará la situación del sector durante esta semana. Anhelamos una respuesta urgente del Estado, que debe garantizar el abastecimiento y más cuando se congelan precios en un contexto de inflación constante", finaliza el documento.