---
category: Estado Real
date: 2021-08-14T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILLERETA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia recuerda las vías habilitadas para reclamos por Billetera Santa
  Fe
title: La provincia recuerda las vías habilitadas para reclamos por Billetera Santa
  Fe
entradilla: La primera medida es comunicarse con Plus Pagos, luego de realizar el
  reclamo por dicha vía y con la Dirección de Promoción de la Competencia y Defensa
  del Consumidor.

---
El Ministerio de Producción, Ciencia y Tecnología, a través de la Dirección Provincial de Promoción de la Competencia y Defensa del Consumidor, en el marco del Programa Billetera Santa Fe, recuerda que ante inconvenientes en el uso de la herramienta la primera medida que deben adoptar las y los consumidores es reclamar ante la operadora del sistema, Plus Pagos.

Las vías habilitadas para realizar reclamos ante la mencionada operadora son el chat boot a través del número 3416470678 o por medio de un correo electrónico a info@pluspagos.

Luego de realizar reclamos por la mencionada vía, la Dirección Provincial de Promoción de la Competencia y Defensa del Consumidor es quien resulta competente para recibir para recibir, tramitar y resolver válidamente cualquier reclamo relativo a este Programa.

Ante dicha área las vías de reclamo son amplias y diversas para atender a las necesidades de las y los usuarios:

De manera presencial, solicitando turno en: [https://turnos.santafe.gov.ar/turnos/web/frontend.php](https://turnos.santafe.gov.ar/turnos/web/frontend.php "https://turnos.santafe.gov.ar/turnos/web/frontend.php")

>>En Santa Fe: Ministerio de la Producción, Pellegrini 3100 - 0800 555 6768 (opción 3) de 8 a 12 horas en días hábiles.

>>En Rosario: Mitre 930 – 3º Piso - (0341) 4721519-4721511 de 8 a 12 horas en días hábiles.

O a través de un trámite online en Ventanilla Única Federal: [https://www.argentina.gob.ar/produccion/defensadelconsumidor/formulario](https://www.argentina.gob.ar/produccion/defensadelconsumidor/formulario "https://www.argentina.gob.ar/produccion/defensadelconsumidor/formulario")

Finalmente, también se pueden presentar reclamos por Billetera Santa Fe ante las Oficinas Municipales o Comunales de Información al Consumidor (OMICs/OCICs), creadas por convenio con el Gobierno Provincial y que trabajan con facultades descentralizadas en materia de protección de consumidores. El listado de OMICs/OCICs se puede consultar ingresando al siguiente enlace:

[https://www.santafe.gob.ar/index.php/web/content/download/259414/1365404/](https://www.santafe.gob.ar/index.php/web/content/download/259414/1365404/ "https://www.santafe.gob.ar/index.php/web/content/download/259414/1365404/")

Al respecto, la directora de Promoción de la Competencia y Defensa del Consumidor, Maria Betania Albrecht, informó: “Los reclamos más frecuentes consisten en la necesidad de reversión de una operación por un error en el tipeado del importe, contando los comercios con un plazo de 4 horas para revertir dicha operación; también nos encontramos con cuentas bloqueadas que deben ser blanqueadas. Respecto de esto último es importante recordar que siempre debemos cargar el número de trámite del último DNI que hayamos tramitado, de lo contrario la cuenta se bloqueará. La aplicación tiene un sistema sensible de seguridad, lo cual es bueno e importante para cuidar la privacidad y los intereses económicos de las y los consumidores, derechos esenciales de los mismos. Por ello, ante estos simples errores se bloquea la cuenta. Pero comunicándose con Plus Pagos o ante esta Dirección Provincial, rápidamente intervenimos para dar una solución efectiva al inconveniente”.

Asimismo Albrecht indicó: “En caso de inconvenientes por recargos en el precio, limitación del uso de Billetera Santa Fe en determinados días, exigencia de una compra mínima para abonar con la aplicación, o cualquier otra práctica abusiva, es importante que las y los consumidores estén atentos y nos hagan llegar a nosotros sus reclamos, en virtud de ser la única autoridad de aplicación en la normativa de comercialización y protección de derechos de consumidores”,

“Estamos hace un tiempo recorriendo todo el territorio provincial, informando a comerciantes y consumidores, promocionando derechos, auxiliando en el uso de la aplicación y también fiscalizando el debido y correcto uso de la herramienta”, concluyó la funcionaria provincial.

**RECLAMOS ANTE LA DEFENSORÍA DEL PUEBLO**

El Secretario de Comercio interior y Servicios, Juan Marcos Aviano, recibió este viernes, en la Delegación del Ministerio de Producción, Ciencia y Tecnología de Rosario, a autoridades de la Defensoría del Pueblo delegación Sur de la provincia. Durante el encuentro se dialogó sobre los canales ya indicados por la Provincia para presentar reclamos, de manera directa y ágil, por inconvenientes con Billetera Santa Fe.

Aquellos reclamos que la Defensoría del Pueblo reciba y quiera tramitar serán válidamente canalizados a través de la plataforma de Ventanilla Única Federal de Defensa del Consumidor, desde donde se gestionarán a nombre de la Defensoría, al igual que se viene haciendo desde el inicio del programa Billetera Santa Fe en el mes de enero 2021.