---
layout: Noticia con imagen
author: .
resumen: Safe Travels Stamp
category: La Ciudad
title: Santa Fe Capital obtuvo la certificación internacional para turismo
  seguro en pos pandemia
entradilla: La ciudad fue distinguida con el sello “Safe Travels Stamp” del
  Consejo Mundial de Viajes y Turismo para municipios y entes que cumplen con
  los protocolos de higiene y sanitización.
date: 2020-11-13T17:31:13.893Z
thumbnail: https://assets.3dnoticias.com.ar/atardecer.png
---
La ciudad de Santa Fe recibió este jueves la distinción “Safe Travels Stamp”. La misma acredita que la capital provincial respeta y cumple los protocolos necesarios para la reactivación del turismo seguro. Se trata de nueve protocolos que abordan distintas aristas de la actividad turística como gastronomía, paseos náuticos, visitas guiadas, turismo de reuniones, playas y balnearios, y atención en centros de información, entre otros.

Al respecto, el intendente Emilio Jatón afirmó que “la obtención de este sello es un gran reconocimiento para el sector tanto público como privado de nuestra ciudad. Es también la muestra clara de nuestra decisión de trabajar de manera sumamente profesional en todo lo referido a la reactivación del turismo, de cara a la nueva normalidad”, aseguró.

Jatón concluyó que esta distinción “garantiza los protocolos de seguridad e higiene para que se desarrollen actividades turísticas, aún en el marco de la pandemia”.

Cabe destacar que la capital provincial obtuvo el sello gracias a un trabajo mancomunado con las áreas de turismo de la provincia y la Nación, ya que nuestro país es el único miembro en Latinoamérica del Consejo Mundial de Viajes y Turismo. La distinción es una gran responsabilidad para el destino que lo recibe ya que demanda la supervisión y promoción por parte de las autoridades, para el estricto cumplimento de los protocolos globales de higiene y sanitización.

La información fue confirmada este jueves, durante una reunión encabezada por el ministro de Turismo y Deportes de la Nación, Matías Lammens, de la que participaron referentes del área de todo el país, entre ellos, el director Ejecutivo de Turismo de la ciudad de Santa Fe, Franco Arone.

Luego del encuentro, Arone recordó que la Municipalidad conjuntamente con el Safetur, trabajó en la elaboración de nueve protocolos que pueden ser consultados en el [sitio oficial de la ciudad de Santa Fe](https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/protocolos-covid-19/): [santafeciudad.gov.ar](https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/protocolos-covid-19/%20)

![](https://assets.3dnoticias.com.ar/sello.jpg)

“Safe Travels Stamp” busca dar respaldo a todos aquellos destinos y empresas que han trabajado en la elaboración de protocolos para los diversos rubros de la actividad de forma alineada con los establecidos a nivel mundial y nacional.

Para acceder a esa distinción, los destinos y las empresas presentan todos los protocolos elaborados y se hace un seguimiento de validación de los mismos, por parte del Instituto Nacional de Promoción Turística (Inprotur), para finalmente otorgar la distinción.

Vale mencionar que Argentina es el único país de Sudamérica que integra el Consejo Mundial de Viajes y Turismo, a través del Inprotur.