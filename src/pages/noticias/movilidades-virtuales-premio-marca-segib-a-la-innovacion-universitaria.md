---
category: La Ciudad
date: 2021-01-28T10:01:09Z
thumbnail: https://assets.3dnoticias.com.ar/marca1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Argentina.gob.ar
resumen: 'Movilidades virtuales: Premio MARCA SEGIB a la innovación universitaria'
title: 'Movilidades virtuales: Premio MARCA SEGIB a la innovación universitaria'
entradilla: La Universidad Nacional del Litoral fue la ganadora de la convocatoria
  2020 del Programa Marca con un proyecto que llevará adelante junto a otras 7 universidades
  del MERCOSUR

---
Debido al contexto de pandemia, el Programa MARCA no pudo realizar su habitual convocatoria de intercambios presenciales de estudiantes y docentes. Sin embargo, para fomentar la innovación universitaria, lanzó un llamado para la presentación de proyectos de movilidad virtual entre instituciones universitarias del MERCOSUR.

La iniciativa surgió a partir de la colaboración entre el Espacio Iberoamericano del Conocimiento dependiente de la Secretaría General Iberoamericana (SEGIB) y el Sector Educativo del MERCOSUR (del cual es miembro el Ministerio de Educación nacional), con el objetivo de fortalecer la cooperación e internacionalización universitaria en la región, a partir de la crisis ocasionada por el Covid 19.

A la convocatoria se presentaron 8 proyectos del cual resultó ganador **“InnovaVet MARCA”**, una propuesta para la carrera de Veterinaria, coordinada por la Universidad Nacional del Litoral y llevada adelante junto a la Universidad Nacional de La Pampa y tres universidades de Brasil, una de Bolivia, una de Colombia y una de Uruguay. Bajo el título “Movilidad académica de grado como aporte a la mejora de la formación de los estudiantes y docentes de veterinaria”, el curso formará en Desarrollo Rural Sustentable y estará conformado por un cuerpo docente de las universidades del MERCOSUR.

“Todas las propuestas presentadas fueron de una gran calidad académica y estamos acompañando a las instituciones para que puedan llevar adelante estrategias de internacionalización integral innovadoras en un contexto tan difícil para la Internacionalización de la Educación Superior”, comentó Anahí Astur, sub - coordinadora del programa PIESCI, de la Secretaría de Políticas Universitarias, que lleva adelante la iniciativa.

Por su parte, Indiana Diez Rodriguez, coordinadora del proyecto ganador, expresó que “la asociación generada a través del Programa MARCA ha creado sólidos vínculos entre los coordinadores académicos, lo que permitió elaborar el proyecto de manera colaborativa. De esta manera, pudimos realizar dos propuestas innovadoras de formación: un curso virtual para estudiantes de las 8 carreras de veterinaria impartido por docentes de las 8 universidades participantes y un curso-taller de formación docente sobre utilización de nuevas tecnologías en la educación”.

**MARCA** es un Programa de movilidad académica regional para carreras de grado acreditadas por el Sistema de Acreditación Regional del MERCOSUR (ARCUSUR) que busca fortalecer las carreras acreditadas, fomentar la integración e internacionalización de la educación superior en la región y cumplir con el objetivo central de integración regional.

![](https://assets.3dnoticias.com.ar/marca.jpg)