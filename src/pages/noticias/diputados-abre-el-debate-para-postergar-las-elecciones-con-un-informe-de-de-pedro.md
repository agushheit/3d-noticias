---
category: Agenda Ciudadana
date: 2021-04-25T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/paso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Diputados abre el debate para postergar las elecciones con un informe de
  De Pedro
title: Diputados abre el debate para postergar las elecciones con un informe de De
  Pedro
entradilla: La propuesta del oficialismo transmitida a los bloques opositores en las
  últimas dos semanas terminó de reunir este viernes un consenso mayoritario.

---
La Cámara de Diputados abrirá la semana próxima el debate en comisión sobre el proyecto para postergar hasta septiembre las primarias abiertas simultáneas y obligatorias (PASO) y hasta noviembre las elecciones generales, en un plenario donde está previsto que expongan el ministro del Interior, Eduardo "Wado" de Pedro, y gobernadores provinciales.

El gobierno nacional propuso a los bloques opositores posponer las primarias previstas para el 8 de agosto al 12 de septiembre y las elecciones fijadas para el 24 de octubre al 14 de noviembre, en las que se elegirán 127 diputados nacionales y 24 senadores, debido al aumento de contagios de coronavirus en el marco de la segunda ola de la pandemia que atraviesa el país.

La intención del oficialismo es hacer una reunión informativa el próximo viernes 30, si hay acuerdo en emitir dictamen, y sino hacerlo entre el lunes 3 o martes 4 de mayo, previo a la sesión donde se buscará aprobar con el consenso de los bloques parlamentarios la iniciativa que propone modificar el cronograma electoral para este año.

**Futura reunión con la Cámara Nacional Electoral**

La propuesta transmitida por Wado de Pedro a los bloques opositores en las últimas dos semanas terminó de reunir el viernes un consenso mayoritario cuando la principal bancada opositora de Juntos por el Cambio informó que votará a favor del corrimiento de las PASO y las elecciones generales.

Además, De Pedro se reunirá el jueves con la Cámara Nacional Electoral (CNE) para trabajar de "manera articulada" sobre los protocolos sanitarios y otras acciones operativas de cara al proceso electoral de este año.

Un día después, el viernes, un plenario de las comisiones de Asuntos Constitucionales y Legislación General, que conducen los oficialistas Hernán Pérez Araujo y Cecilia Moreau, abrirá el debate en la Cámara de Diputados del proyecto para postergar las PASO y las elecciones generales, informaron fuentes parlamentarias.

**Tratamiento en Diputados**

El tratamiento se abrirá con un informe del ministro De Pedro, quien esgrimirá los motivos para posponer un mes las elecciones primarias, que están basados en argumentos sanitarios, ya que la Argentina transita la segunda ola pandemia con un alto nivel de contagios.

Además, la campaña electoral de los partidos políticos se debería hacer en pleno invierno cuando, además de la Covid, aparecen otros virus propios de esa época.

Fuentes parlamentarias informaron que en el plenario de las comisiones expondrá también un grupo de mandatarios provinciales, aunque aún no se informó quienes serán los gobernadores que hablarán en la reunión.

**Búsqueda de consensos**

El gobierno nacional ya tenía asegurada la aprobación del proyecto con los respaldos aportados por los distintos partidos, dado que ya alcanzaban unos 137 votos; es decir ocho más de los 129 que se exigen de mayoría absoluta para poder aprobarlo en la Cámara de Diputados.

De todos modos, el oficialismo prefería un consenso contundente por tratarse de un proyecto electoral.

Por eso aguardo la decisión de Juntos por el Cambio para realizar la convocatoria a un plenario y discutir la propuesta del Gobierno, en base al borrador que el ministro De Pedro envió días atrás a los bloques parlamentarios.

**La posición de Juntos por el Cambio**

Al sentar su postura, Juntos por el Cambio señaló en un comunicado que acompañarán el "corrimiento del cronograma", y señaló su "interés en contribuir a la mejor gestión de la pandemia y, a la vez, respetar las normas electorales vigentes con la garantía de las elecciones primarias que hoy consagra la ley".

Otro punto que se deberá resolver es si se acepta el pedido de JxC de incluir "una cláusula que reasegure el compromiso del Gobierno nacional con el respeto de la legislación electoral vigente".

Al respecto, la oposición solicitó que se incluya un artículo que exprese que "la presente ley no podrá ser modificada ni derogada durante el año calendario en curso, en tanto, regula un derecho subjetivo de los partidos políticos, instituciones fundamentales del sistema democrático, a elegir sus candidatos a los cargos electivos previstos en la Constitución Nacional".

**Corrimiento “por única vez”**

El proyecto que se someterá a discusión pide cambiar por "única vez" la fecha de las PASO, prevista por el artículo 20 de la Ley Nº 26.571 estableciendo para la realización de los comicios el segundo domingo de septiembre, que cae 12.

También fija que se modifica por "única vez" la fecha de las elecciones nacionales generales, "prevista por el artículo 53 de la Ley 19.945 y sus modificatorias, estableciendo para la realización de los comicios el segundo domingo de noviembre del año 2021", que cae 14.

En la misma línea pide cambiar los plazos previstos para la presentación de listas para las PASO y para las elecciones generales de renovación parlamentaria.