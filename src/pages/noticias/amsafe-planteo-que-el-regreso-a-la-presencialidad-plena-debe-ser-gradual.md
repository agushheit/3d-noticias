---
category: Agenda Ciudadana
date: 2021-08-29T06:00:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMSAFE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Amsafé planteó que el regreso a la presencialidad plena "debe ser gradual" '
title: 'Amsafé planteó que el regreso a la presencialidad plena "debe ser gradual" '
entradilla: Lo hizo a través de un comunicado en donde responden a los dichos de la
  ministra de Educación, Adriana Cantero, quien había instado a los docentes a ser
  creativos y flexibles.

---
La Asociación del Magisterio de Santa Fe (Amsafé), señaló que “el retorno” a la presencialidad escolar “debe ser gradual” porque “la forma intempestiva de retorno pleno genera malestar”.

A través de un comunicado, la entidad sindical respondió los dichos de la ministra de Educación santafesina, Adriana Cantero, quien había instado a los docentes a ser creativos y flexibles ante el regreso de la presencialidad sin alternancia, que comenzará el próximo lunes en toda la provincia.

“No se trata de tener creatividad. Se trata del cuidado de la salud, de los tiempos y la organización escolar”, sostuvo Amsafé.

El gremio indicó que “la forma intempestiva de retorno pleno genera malestar, dificultades en la organización escolar, trabajo durante días no laborables, desconociendo el esfuerzo y compromiso que la docencia santafesina tuvo y tiene en esta y todas las circunstancias”.

El viernes, el Ministerio de Educación distribuyó en las escuelas una circular con la decisión de comenzar el proceso de retorno a la presencialidad plena a partir del lunes.

Entre otras cosas, el documento indica que los directivos y equipos docentes deberán implementarla haciendo uso de espacios como gimnasio, bibliotecas, galerías, comedores y patios” de los establecimientos, con el fin de garantizar el distanciamiento entre alumnos.

Desde el gremio docente señalaron que “los espacios comunes tienen que ser respetados como tales” atento a que “los pasillos y patios escolares no pueden ser ocupados y usados como aulas”.

Para Amsafé, “no están preparados para tal fin, provocan que las y los trabajadores de la educación tengan que esforzar aún más la voz, a la vez que impide la circulación del resto de las personas que habitan la escuela”.

Además, el comunicado puntualizó que “los pasillos son lugares que por normas de seguridad deben quedar totalmente libres de obstáculos, en tanto que los patios son los lugares donde se desarrollan los recreos”.

Por eso, siguió, “el retorno a la presencialidad plena no tiene que poner en riesgo la salud y la vida”.

Por último, Amsafé señaló que “si los establecimientos educativos no cuentan con las condiciones para sostener el distanciamiento se deberá continuar con la bimodalidad o presencialidad alternada”.

La ministra Cantero había advertido que eso puede suceder sólo en “un número menor” de escuelas de las grandes ciudades de la provincia que “por la gran matrícula y la dimensión de sus espacios, requieran intervención del Ministerio para analizar alternativas de excepcionalidad”.