---
category: Estado Real
date: 2021-04-04T08:01:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/CPA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Ministerio de Salud de la Provincia informa un nuevo caso de la cepa Manaos
  y otro de la cepa Nueva York de coronavirus
title: El Ministerio de Salud de la Provincia informa un nuevo caso de la cepa Manaos
  y otro de la cepa Nueva York de coronavirus
entradilla: Los pacientes son residentes de Funes y Rosario, respectivamente. Se encuentran
  aislados y cursando la infección de manera ambulatoria.

---
El Ministerio de Salud de la provincia informó la aparición de nuevos casos con cepas mutantes de Coronavirus. En este sentido, desde la cartera sanitaria santafesina señalaron que se registró un caso de contagio con la cepa de Manaos en Funes y otro caso de contagio con la cepa de Nueva York en Rosario; ambos con antecedente de viaje.

De esta forma, aclararon que ambos pacientes se encuentran aislados y cursando la infección de manera ambulatoria.

**Otros casos registrados**

Vale remarcar que, el sabado, la ministra Sonia Martorano informó la presencia de un caso de cepa Manaos en Rafaela y otro de la de Reino Unido en Santa Fe. En todos los casos, se llevaron adelante los protocolos de aislamiento en los contactos estrechos de cada paciente.

En ese sentido, se remarcó que la presencia de estas cepas hace que el Coronavirus tenga mayor capacidad de adaptabilidad, lo que lo vuelve más contagioso, y en algunos casos, con cuadros que complican más la salud de los pacientes.

Asimismo, volvieron a remarcar que aquellas personas que ingresen al país deben acreditar una PCR negativa 72 horas antes del viaje; una vez en Argentina realizar una estricta cuarentena de 10 días, y un test PCR a 7 días de arribado, aún sin tener síntomas.

De esta forma se busca evitar la introducción de nuevas cepas y su dispersión en la población.