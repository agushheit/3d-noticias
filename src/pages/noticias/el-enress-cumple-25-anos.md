---
category: Agenda Ciudadana
date: 2020-12-06T11:33:58Z
thumbnail: https://assets.3dnoticias.com.ar/enress.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El ENRESS cumple 25 años
title: El ENRESS cumple 25 años
entradilla: La entidad se dedica a la regulación, el control y la mejora del Servicio
  Sanitario de toda la provincia de Santa Fe.

---
El Ente Regulador de Servicios Sanitarios (ENRESS), que forma parte de la estructura del gobierno de la provincia de Santa Fe, se conforma el 5 de diciembre de 1995. 

Desde entonces ha cumplido con la intensa tarea de **ejercer el poder de policía sobre la prestación del Servicio Público** en todo el ámbito provincial, incluso después de la creación formal de la empresa pública estatal, en el año 2006, Aguas Santafesinas S.A. (ASSA), que reemplazó la concesión a cargo de Aguas Provinciales de Santa Fe, con el objetivo de proveer agua potable y desagües cloacales en quince ciudades de la Provincia de Santa Fe.

La creación y continuidad del organismo de control ha sido sin dudas, **una experiencia novedosa y positiva en el sector del saneamiento santafesino**, pues se ha demostrado que la independencia de estos entes autárquicos constituye un aliciente a la eficiencia de los servicios públicos.

## **EL PRESENTE**

Actualmente, el Ente Regulador ejerce el control sobre la prestación de estos servicios públicos esenciales, además de las 15 localidades donde tiene presencia ASSA, sobre cientos de municipios, comunas y cooperativas, que funcionan como prestadores locales y se encuentran distribuidos a lo largo y ancho de todo el territorio provincial. 

En este sentido, dicta las reglamentaciones necesarias para cumplir su cometido y controla la aplicación del marco regulatorio en la provisión del servicio.

El Ente cuenta con un laboratorio con tecnología de punta, que nos destaca a nivel nacional, ya que las calificaciones del Enress a la hora de controlar la calidad del agua muestran un nivel alto, con 100% de precisión para la llamada “caracterización de aguas para consumo humano”.

## **NUEVOS DESAFÍOS**

A futuro, la entidad afronta nuevos desafíos: el control del Sistema de Grandes Acueductos, las obras que lleva adelante la empresa Aguas Santafesinas, en tanto proveedora mayorista de agua potable para cooperativas y municipios, y la extensión del servicio y conformación de nuevos prestadores que van a surgir con las nuevas obras de infraestructura que lleva adelante el gobierno de la provincia.