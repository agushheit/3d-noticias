---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: Usurparon comedor comunitario
category: La Ciudad
title: Un usurpador se apropió de un local donde funcionaba un comedor comunitario
entradilla: "La edificación está hecha sobre un terreno de Vialidad Nacional.
  Este violento personaje les pide que le paguen 30 mil pesos o que le consigan
  un plan social porque se considera dueño del terreno. "
date: 2020-11-11T21:54:45.441Z
thumbnail: https://assets.3dnoticias.com.ar/usuarpador.jpg
---
Un violento sujeto de Barrio Chalet se quedó con un local donde funcionaba un comedor comunitario que había sido edificado por los voluntarios de una organización social, en un terreno de Vialidad Nacional.

Este terreno había sido tomado de forma ilegal por este hombre hace años y, en un gesto de supuesta solidaridad, les cedió un espacio para que pudieran construir el comedor. Hace unos días, el hombre apareció y les pidió que le pagaran un alquiler de 30 mil pesos o le consiguieran un plan social. Como la gente del comedor no accedió a sus pedidos los sacó del lugar y les robó alimentos del comedor.

Sandra Casal, titular del comedor, sostuvo por LT10: “estamos indignados y muy angustiados porque este hombre nos pretende cobrar un alquiler de 30 mil pesos para utilizar el local que nosotros construimos (o que le consigamos un plan social). Nosotros confiamos en él porque en el comedor trabajaban sus dos hijas. Mucha gente me había advertido que no edifiquemos en ese lugar”.

Además, Sandra agregó: “en este lugar le damos de comer a 80 chicos y niños del barrio. Este tipo es un violento que nos está jodiendo a nosotros y a los chicos del barrio. Todos en el barrio saben que este hombre se dedica a joder gente y tomar casas”.