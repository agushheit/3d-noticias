---
category: Agenda Ciudadana
date: 2021-09-04T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/REBAÑO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Sonia Martorano anticipa que Santa Fe tendrá "inmunidad de rebaño" en septiembre
title: Sonia Martorano anticipa que Santa Fe tendrá "inmunidad de rebaño" en septiembre
entradilla: La ministra de Salud de Santa Fe dijo que el objetivo es llegar este mes
  al 70% de la población vacunada. Sonia Martorano agregó que hoy ya hay más santafesinos
  con dos dosis que con una.

---
La provincia de Santa Fe llegará en el mes de septiembre a la inmunidad de rebaño, porque se llegaría al 70% de la población con dos dosis de vacunas contra el coronavirus. Así lo anticipó la ministra de Salud Sonia Martorano, en una conferencia de prensa brindada esta mañana en Rosario, para describir la situación actual del plan de vacunación en la provincia.

“Hoy hemos recibido más vacunas, entre ellas Sputnik V de segundo componente, AstraZéneca, así que vamos a poder seguir con el cronograma de vacunación e ir completando estos esquemas. Nuestro objetivo en este mes de septiembre es llegar como mínimo a los 2 millones de dosis, así que eso ya nos va a permitir hablar de prácticamente de una inmunidad de rebaño, una inmunidad colectiva”, adelantó la funcionaria.

Sonia Martorano agregó que este viernes “ya alcanzamos al 40% de toda la población con dos dosis. Es muy importante, y son 350.000 con dos dosis. Hoy son más las personas que tienen las dos dosis colocadas, es decir, inmunización completa, que las que les falta completar”.

En cuanto a la positividad en los testeos, La ministra dijo: “Solamente el 8% da positivo actualmente. Eso es bien bajo, pero va acompañado de otros parámetros. Estamos por debajo de 400 casos promedio y si miramos la ocupación de camas, esta baja, está en 56% y solo un 15% como máximo es Covid. Así que la foto de hoy es buena. Hay que continuar vacunando y veremos cómo se desarrolla la Delta y las demás variantes.