---
category: Agenda Ciudadana
date: 2021-07-09T08:29:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/cementerio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Servicios municipales previstos para el feriado del 9 de Julio
title: Servicios municipales previstos para el feriado del 9 de Julio
entradilla: La Municipalidad informa sobre los servicios que se garantizan a la población
  durante este fin de semana largo. Mañana se conmemora el Día de la Independencia.

---
Con motivo del feriado nacional por el Día de la Independencia, que se conmemora este viernes 9 de julio, la Municipalidad de Santa Fe informa los horarios de los servicios de recolección de residuos, cementerio y estacionamiento medido.

**Recolección de residuos**

Este 9 de julio el servicio de recolección no tiene cambios y será el habitual de un día viernes; lo mismo para sábado y domingo.

**Cementerio**

Este viernes, las puertas permanecerán abiertas de 8 a 12 horas, con motivo del feriado, para las visitas del público.

En tanto, sábado y domingo el horario será el habitual: de 9 a 16 horas para el sábado, y de 8 a 12 horas para el domingo.

**SEOM**

El Sistema de Estacionamiento Medido (SEOM) no estará operativo.

**Mercado Norte**

El Mercado Norte, ubicado en la intersección de Santiago del Estero y Urquiza, abrirá este viernes, en el horario de 9 a 14 horas. Mientras que serán los habituales para los días sábado, de 9 a 19 horas; y domingo, de 9 a 13 horas.