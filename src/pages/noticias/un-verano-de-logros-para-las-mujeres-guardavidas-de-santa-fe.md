---
category: La Ciudad
date: 2020-12-15T13:10:43Z
thumbnail: https://assets.3dnoticias.com.ar/guardavidas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Un verano de logros para las mujeres guardavidas de Santa Fe
title: Un verano de logros para las mujeres guardavidas de Santa Fe
entradilla: 'Esta será la primera temporada en que cuatro “jefas de sector” tengan
  a su cargo la coordinación del personal y los operativos en playas y parques. '

---
**Este verano será histórico para las mujeres guardavidas que se desempeñan en playas y parques de la capital provincial**. Es que por primera vez se definió el armado de una estructura diferente a la de años anteriores para organizar el trabajo de 90 guardavidas -de los cuales 15 son mujeres-, que cuidan a quienes se acercan a disfrutar de la temporada en la costa y los balnearios.

Para este **Verano Capital** se designó a cuatro “jefas de sector”, las cuales dependen de la coordinación ejecutiva. **El rol de las jefas es formar sus equipos y establecer los días y horarios de cada integrante del mismo**, al tiempo que determinan los criterios de trabajo generales en cada uno de los espacios. Del mismo modo, resuelven las cuestiones puntuales que surgen en sus zonas.

Se trata de un **logro para el colectivo de mujeres guardavidas** que cada temporada de verano brinda servicios, teniendo en cuenta que la jefatura de playa era un puesto reservado para los varones. Pero no es el único: **por primera vez también hay una jefa de lanchas** y, además, se asignó a dos guardavidas mujeres para el turno a bordo de las lanchas que patrullan la costa e imparten indicaciones a otras embarcaciones.

Soledad Artigas, directora municipal de Mujeres y Disidencias, explicó que las conversaciones se iniciaron el verano anterior cuando, a poco de asumir al frente de la gestión, “conocimos a las compañeras guardavidas que nos contaron sus demandas. Este año, en el marco de la paritaria, pudimos llegar a algunos acuerdos” con el Sindicato Único de Guardavidas y Afines (Sugara).

En ese sentido, detalló que después de algunas conversaciones “conseguimos que las compañeras mujeres sean jefas de playa y puedan armar sus equipos para garantizar la seguridad y el disfrute de los vecinos y las vecinas en las playas”. De igual modo, mencionó que se discutió la posibilidad de que parejas compuestas por dos mujeres cumplan turnos a bordo de las lanchas. Y, también, que una mujer esté en el puesto de jefa de lancha, históricamente ocupado por un varón.

Artigas explicó que estas decisiones, “que desde el inicio de la gestión fueron impulsadas por el intendente Emilio Jatón, tienen que ver con procesos de aprendizaje que están haciendo los varones guardavidas, los varones empleados municipales de otras áreas y los varones en general”. En ese marco, dijo que el rol del municipio es “proponerles a los varones que repiensen sus funciones y estas ideas arraigadas de que hay tareas exclusivas de varones y tareas exclusivas de mujeres”.

La directora mencionó, además, que las mujeres rinden exactamente la misma prueba física que los varones, por lo que queda claro que están preparadas del mismo modo. “Las chicas están tan formadas y tan habilitadas para hacer las tareas como un varón y, en ese sentido, entendemos que no hay diferencias”, indicó.

![](https://assets.3dnoticias.com.ar/guardavidas2.jpg)

# **Las protagonistas**

**Marianela Mendoza es la encargada de las lanchas**. Su trabajo consiste en preparar las embarcaciones cada mañana para salir a patrullar. “Me propusieron la jefatura de las lanchas y sinceramente no me lo esperaba. Yo tenía intención de subir a la lancha en algún momento, pero no me esperaba que me designaran jefa. Me sorprendí y consideré que estaba buena la idea, que era necesario empezar a visibilizarnos como mujeres”, relata.

Del mismo modo, señala que “varones y mujeres tenemos la misma capacitación, que es la formación profesional de la escuela de guardavidas”. Incluso, menciona que para acceder a una embarcación, es necesaria la habilitación de conductor náutico o timonel, emitida por la Prefectura, que es igual para cualquier persona.

“Es muy emocionante compartir con mis compañeras este lugar que históricamente fue ocupado por hombres. Siempre nos decían que no podíamos y estamos demostrando que podemos y que todos por igual somos capaces, siempre que estemos capacitados”, cierra.

**Victoria Cantero atraviesa su décimo primera temporada como guardavidas en espacios públicos** y asegura que el acceso de las mujeres a cargos jerárquicos se relaciona con “esta lucha que se está haciendo con las chicas y muchos compañeros que nos apuntalan”.

Si bien reconoce que “algunos varones han manifestado alguna reticencia, es entendible porque esta profesión se asocia al género masculino por una cuestión de fuerza, de capacidad y de velocidad en el agua. Y la verdad es que al día de hoy no hay ninguna distinción en el curso de guardavidas, respecto de los exámenes o las exigencias de tiempo: es exactamente igual para varones y mujeres”.

Según rememora, hasta hace pocos años, las mujeres no podían acceder a jefaturas de playa, incluso en alguna ocasión se designó a dos mujeres para ocupar ese cargo, pero juntas “y eso ni siquiera se cuestionaba. Recién en los últimos años lo empezamos a conversar. Hoy nos ganamos el respeto de nuestros compañeros, ocupando lugares que se suponía que eran muy difíciles para nosotras y demostramos estar a la par y al nivel que ese lugar merece”, indica.

No obstante, acepta que “es un lugar de enorme responsabilidad, con mucha tensión y fuerzas que se contraponen: una tiene que poner la cara y el cuerpo para sostener situaciones de vulnerabilidad, de discriminación y de violencia”.

![](https://assets.3dnoticias.com.ar/guardavidas3.jpg)

# **Parques y lanchas**

**Sandra Arroyo Rivera es jefa de sector de Parques**, que comprende a del Sur y al Garay. Allí desarrolla su trabajo de prevención, pero también de organización de sus compañeras y compañeros. “Es mucha responsabilidad porque es un trabajo sin descanso, pero a su vez, hermoso y gratificante”, asegura.

Desde el 2007 se desempeña como guardavidas para la Municipalidad y también realizó trabajos en diferentes clubes de la ciudad. Pero en esta temporada, como jefa de Parques, asevera que “las chicas avanzamos mucho en este campo, porque siempre fueron cargos de varones”. Incluso, recuerda que “cuando ingresé, la mujer quedaba relegada. Pero en estos últimos años, nos incorporamos con fuerza y hemos logrado muchas cosas, con apoyo del municipio y del gremio”.

**María Florencia Canal es una de las guardavidas que por primera vez**, patrulla desde las embarcaciones con otra mujer. “Es el primer año que nos subimos dos mujeres arriba de las embarcaciones, que siempre fue un lugar de privilegio reservado para los varones. Este es un reconocimiento a tantos años de lucha colectiva. Nos costó llegar a este puesto y es un espacio ganado”, asevera.

# **Políticas de género**

**Artigas informó que en los próximos días las y los guardavidas iniciarán las capacitaciones en el marco de la Ley Micaela**, tal como se acordó en las reuniones paritarias celebradas con Sugara. “Hay mucha predisposición por aprender sobre estos temas. Creemos que es fundamental que todo agente municipal y toda persona que está al servicio de otra, tenga una perspectiva de género”, insistió.

La directora mencionó que “esta es una de las agendas que nos hemos dispuesto a trabajar desde la Dirección y en la que insiste el intendente Emilio Jatón, quien siempre nos propone impregnar con la perspectiva de género todas las tareas, de modo que los agentes municipales estén preparados y preparadas para asistir y para prevenir la violencia”.

Por otra parte se refirió a la aplicación, nuevamente en esta temporada, de los Puntos Violetas. Según recordó, “el año pasado tuvimos 13 Puntos Violetas en todas las playas, incluido el Parque Garay y el Parque del Sur, y este año la vamos a repetir”.

Para ello, las y los guardavidas fueron capacitados con la idea de que colaboren en la detección y la prevención de las violencias que se dan en el entorno del verano. Están identificados con una pluma violeta y contienen material e información para que quienes están la playa o los parques cuenten con herramientas para saber cómo actuar ante una situación violenta.