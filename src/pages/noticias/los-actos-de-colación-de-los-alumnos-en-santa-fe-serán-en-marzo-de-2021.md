---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: Actos de colación
category: Agenda Ciudadana
title: Los actos de colación de los alumnos en Santa Fe serán en marzo de 2021
entradilla: Así lo informaron desde la cartera provincial de Educación. Llegada
  esa fecha los alumnos de todos los años tendrán su acto de colación respetando
  la distancia social y el uso de barbijos.
date: 2020-11-20T11:48:16.089Z
thumbnail: https://assets.3dnoticias.com.ar/diplomas.jpg
---
El Ministerio de Educación confirmó a la comunidad educativa, tanto alumnos como docentes y directivos, que el cierre del ciclo lectivo actual será en marzo de 2021. Llegada esa fecha los alumnos de todos los años tendrán su acto de colación respetando la distancia social y el uso de barbijos.

También se informa a los estudiantes que finalizan un ciclo, como los de 7mo grado y 5to año, que tendrán su ceremonia de promoción en su debido momento, cuando el contexto epidemiológico lo amerite.

Por otro lado, la ministra de Educación, Adriana Cantero, también brindó precisiones acerca del inicio del ciclo lectivo 2021 indicando: "Nuestra propuesta ya ha sido elevada a la

En relación a las fechas precisas respecto del presente ciclo y el correspondiente a 2021, Cantero manifestó: “A partir del 15 de diciembre los chicos entrarían en sus vacaciones y el 17 de febrero, que es cuando los docentes se reintegran de sus licencias anuales ordinarias, volvemos de las vacaciones y vamos a comenzar el cursado del periodo de intensificación pedagógica con los grupos prioritarios”.

“Estos grupos son 7º grado para la primaria, 5º año para la finalización de la secundaria y 6º año para la escuela técnica. Estos, tendrán un cursado intensivo entre el 17 de febrero y finales de marzo, por eso a los padres les decimos que los actos de colación serán en marzo y no en diciembre porque el ciclo lectivo aún no habrá terminado”, añadió la ministra.

![](https://assets.3dnoticias.com.ar/cantero.jpg)

Por otra parte, la titular de la cartera educativa agregó: "Además, durante el mes de febrero las escuelas van a poder citar a grupos de niños y adolescentes para poder tener tutorías, espacios de seguimiento y cierre de las evaluaciones del año 2020. En tanto, a partir del 15 de marzo y hasta el 20 de diciembre se extenderá el ciclo lectivo 2021, que conlleva en ese plazo la garantía de los 180 días de clases pautados por ley, son dos ciclos lectivos que se articulan y que tendrán por esa razón indicaciones precisas a la organización escolar que aseguren trayectorias continuas”.

## **Expectativas por la presencialidad**

En otro tramo, para referirse a la importancia de la presencialidad, la ministra expresó: "Nosotros estamos muy expectantes de que el 17 de febrero podamos estar en presencialidad en todas las localidades donde no hemos podido tener todavía esa experiencia”.

En ese contexto, la titular de la cartera educativa aclaró: “En la medida que las autoridades de Salud van indicando que las localidades ingresan en la clasificación de bajo riesgo de contagio, allí abrimos las escuelas para garantizar esta posibilidad del nuevo encuentro entre los estudiantes y los educadores en el espacio escolar con los protocolos respectivos. Esto esperamos poder hacer con todos los chicos en febrero y marzo, pero si la pandemia no diera esta posibilidad, también está planificado el modo para trabajar en la distancia o de manera combinada".