---
category: Agenda Ciudadana
date: 2021-05-05T08:11:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/puerto.gif
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Por la bajante, el Puerto de Santa Fe evalúa operar de noche y con barcazas
title: Por la bajante, el Puerto de Santa Fe evalúa operar de noche y con barcazas
entradilla: '"Contra una bajante tan pronunciada como ésta no hay mucho que podamos
  hacer para mantener una operatividad al 100%", advirtieron desde el ente portuario
  de Santa Fe.'

---
En diálogo con UNO, el gerente de Infraestructura del Puerto de Santa Fe, Esteban Franco, indicó que "contra una bajante tan pronunciada como ésta no hay mucho que se pueda hacer para mantener una operatividad al 100%". Las tareas de dragado no podrán asegurar el tráfico de buques en el canal de acceso y se piensa operar con barcazas. Además, se retomarían las operaciones nocturnas para que no se resienta el ritmo de carga.

\- ¿El dragado alcanzará para mantener la operatividad si la bajante en Santa Fe persiste?

\- Nosotros venimos advirtiendo la situación de bajante, que es de fuerza mayor. Las estadísticas y los parámetros con los cuales nos manejamos permiten planificar el dragado, que es la acción correctiva que siempre tomamos en estos casos. Antes de 2020, en el 80% del año el nivel del río rondaba los tres metros. Contra una bajante tan pronunciada como se está presentando ahora no hay mucho que podamos hacer como para tener una operatividad al 100%. Nosotros tenemos planes de dragado anuales, con una draga propia de corte y succión. Esa draga ya está trabajando todos los días para mantener la profundidad de los muelles, en lo que se denomina la zona del antepuerto.

\- El dragado de 70.000 metros cúbicos que se hizo el año pasado ¿influyó en la operatividad?

\- Para los dragados que se venían realizando en el Puerto de Santa Fe significó una acción significativa con una fuerte inversión para prevenir este panorama. Esto nos permite garantizar una mínima operatoria dentro de este escenario hidrológico negativo. No podemos hablar de buques oceánicos, pero sí de operar con barcazas o buques de menor porte.

\- ¿Qué profundidad es la recomendada para el calado de los buques?

\- Como parámetro nosotros tenemos la medida de 25 pies, que serían aproximadamente 7,80 metros, que es lo que se draga en la Hidrovía del río Paraná. Nosotros dragamos esa profundidad y la tratamos de mantener, pero esos 25 pies se hacen respecto a un plano de referencia que se da cuando el nivel de agua en el puerto está en tres metros. Con una bajante extraordinaria no es posible garantizar esos 25 pies y habría que profundizar a niveles que serían totalmente antieconómicos. Con este escenario para los próximos meses no vamos a poder mantener esos 25 pies, sino que podremos llegar a tener entre 15 y 17 pies de profundidad, que no permitiría circular buques oceánicos pero sí el tráfico de barcazas.

\- ¿En qué cambiaría la logística en la operatividad del Puerto trabajando con barcazas?

\- Dentro del puerto tenemos la terminal de combustibles Raízen, exShell, que es una de las más importantes de la región Litoral y abastece a todo el centro-norte del país. Con la bajante dejó de operar con buques y comenzó a operar con barcazas. Estas barcazas cargan mucha menor cantidad de combustible y eso implica que para mantener la frecuencia y el ritmo de carga haya que incrementar la frecuencia de arribo de esas barcazas, pasando de un buque por semana a tres barcazas por semana.

\- ¿Qué otra medida analizan adoptar en este contexto?

\- Otra medida complementaria al dragado es poder operar de noche. Normalmente por cuestiones de seguridad en la navegación Prefectura nos exigía la operatoria diurna, pero al no poder usar buques por su calado, al tener poca profundidad se pueden usar barcazas. Mediante sistemas de señalización nocturna, sistemas lumínicos y determinadas normas logramos que se habilite la actividad de noche.