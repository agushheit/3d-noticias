---
category: La Ciudad
date: 2021-09-09T06:00:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/RCP.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Talleres en primeros auxilios para personal de los Jardines Municipales
title: Talleres en primeros auxilios para personal de los Jardines Municipales
entradilla: Los organiza la Municipalidad y se extienden hasta el 16 de septiembre.
  En total serán 226 los trabajadores y trabajadoras de los 17 Jardines Municipales
  que participarán de los cursos.

---
Este martes, 41 trabajadores y trabajadoras de los jardines municipales ubicados en Urquiza al 199, en barrio Chalet y en Varadero Sarsotti, recibieron los conocimientos fundamentales para reaccionar frente a una emergencia. La iniciativa se enmarca en el programa impulsado por el municipio denominado “Espacios Seguros”. En ese sentido, Cintia Gauna, directora de Gestión de Riesgos, repasó que la capacitación se realiza en diferentes espacios municipales desde el año pasado.

“Se trata de un curso de RCP, primeros auxilios, obstrucción de vías aéreas, manejo de extintores y riesgos de incendios que finaliza con el plan de evacuación del lugar”, indicó la funcionaria y añadió: “Arrancamos con el jardín de barrio Chalet, después vamos a Las Flores, luego Barranquitas y así hasta abarcar todos los jardines municipales. Estamos trabajando con todas las secretarías y ya realizamos los cursos en espacios culturales, en el Museo de la Constitución y el Teatro Municipal, es decir, lugares que tienen mayor concurrencia de público”.

El primer módulo que se llevó adelante estuvo a cargo de trabajadores del Cobem (primeros auxilios y RCP). Posteriormente, integrantes del departamento de Higiene y Seguridad, “dictó un curso sobre riesgo de incendio y extinción”, detalló Gauna y repasó que las acciones “las venimos coordinando entre las distintas secretarías. El objetivo es abarcar a todo el personal municipal para, de esa manera, brindar espacios seguros a la comunidad y que, en caso de una emergencia, sepan cómo actuar, qué hacer, a quién llamar”.

**Mejor preparados**

Carlos Ávalos, encargado de la sección de capacitaciones del Cobem, brindó al personal de los Jardines Municipales, las nociones básicas para actuar frente a una emergencia. Según explicó “tratamos de enseñar RCP y la importancia que tiene conocer estas prácticas como así también, la desobstrucción de las vías aéreas. A esto agregamos convulsiones y cómo actuar en estas situaciones.

Ávalos hizo hincapié en la importancia de que cada vez más santafesinos estén preparados para actuar frente a una emergencia: “Nos centramos en reanimación cardiopulmonar porque es fundamental que más personas sepan cómo actuar; son casos que se pueden dar en cualquier momento de la vida, accidentes que le pueden pasar a cualquiera”.

En ese sentido, el trabajador del Cobem se refirió a la importancia de que se brinden estas capacitaciones en los jardines municipales porque “se trabaja con chicos y está orientado hacia ese lugar: qué hacer cuando a un chico le pasa algo”.

En referencia a la capacitación del martes, mencionó que “es el primer encuentro y la intención es abarcar todos los jardines. Estos talleres forman parte de las acciones por el aniversario del Cobem, que se iniciaron como una manera de mostrar todas las cosas que hacemos”.

**Actuar frente a un imprevisto**

Una de las primeras asistentes escolares infantiles que culminó la capacitación fue Rocío Córdoba, trabajadora del Jardín de Chalet. “Se agradecen estas prácticas, estoy muy contenta porque esto sirve para que mejoremos año a año. Al trabajar con niñas y niños es importante saber actuar frente a un imprevisto. Tomamos como ejemplos algunos casos que se pueden llegar a dar y nos brindaron herramientas para implementar en nuestro trabajo pero también en la vida cotidiana, en la familia, o en la calle”, indicó Rocío.

“Es una tranquilidad para todos, estar capacitadas. Tener un conocimiento previo para saber cómo actuar”, añadió Rocío.

Los talleres continuarán hasta el 16 de septiembre. En ese período se pretende llegar a 226 integrantes de los 17 Jardines Municipales.