---
category: La Ciudad
date: 2021-01-29T10:29:49Z
thumbnail: https://assets.3dnoticias.com.ar/ediles.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Lt10
resumen: Aseguran que el aumento del transporte urbano es desmesurado
title: Aseguran que el aumento del transporte urbano es desmesurado
entradilla: "Los ediles Carlos Pereira e Inés Larriera sostuvieron que esta decisión
  profundiza la crisis del sistema y perjudica a los usuarios. “La gente no da más.
  Acá se debieron haber gestionado más subsidios”, remarcaron.\n\n"

---
Desde la Municipalidad de Santa Fe anunciaron el incremento del costo del boleto de colectivo en la ciudad. De esta forma, a partir del 8 de febrero, la tarifa frecuente del transporte urbano de colectivos pasará a costar $ 42,35, luego de 16 meses donde no hubo reajustes.

Carlos Pereira, concejal de Juntos por el Cambio, sostuvo que “estamos sorprendidos por el aumento desmesurado de la tarifa del transporte público. Este importe le va a generar más crisis al sistema porque la gente se va a bajar de los colectivos. Este 46% de aumento duplica las paritarias de los empleados municipales”.

Además, el concejal de Juntos por el Cambio manifestó que “llama la atención que el órgano de control autorizó un incremento del 34% y se decide aumentar un 12% más. Esto merece una explicación. A esto se le suma la frecuencia deficiente que tiene el sistema y el recorte de recorrido que se viene”.

Por su parte, la edil Inés Larriera opinó que “la gente no da más. Acá se debieron haber gestionado más subsidios”.