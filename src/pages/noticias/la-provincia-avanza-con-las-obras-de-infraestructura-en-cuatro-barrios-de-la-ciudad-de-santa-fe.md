---
layout: Noticia con imagen
author: 3DNoticias
resumen: Obras en cuatro barrios
category: Estado Real
title: La provincia avanza con las obras de infraestructura en cuatro barrios de
  la ciudad de Santa Fe
entradilla: Los trabajos de mejoramiento barrial se desarrollan en Centenario,
  Villa Hipódromo, Pompeya y Las Flores.
date: 2020-11-25T12:42:12.450Z
thumbnail: https://assets.3dnoticias.com.ar/las-flores.jpg
---
El gobierno provincial, a través de la Dirección Provincial de Vivienda y Urbanismo del Ministerio de Infraestructura, Servicios Públicos y Hábitat, avanza a ritmo sostenido con las **obras de infraestructura en los barrios Centenario, Villa Hipódromo, Pompeya y Las Flores** de la ciudad de Santa Fe.

Al respecto, el director Provincial de Vivienda y Urbanismo, José Kerz, remarcó que "el objetivo es mejorar la accesibilidad a los servicios públicos, la circulación, y la protección ante situaciones climáticas desfavorables provocando una significativa mejora en la calidad de vida de los vecinos. Estas obras no solo tendrán un efecto en los barrios mencionados, sino que también estimamos que con estas intervenciones impactamos en más de 10 barrios de la ciudad, beneficiando a más de 30 mil vecinos", expresó Kerz.

Finalmente, mencionó la importancia de este tipo de acciones como política de Estado: "Como nos delineó el gobernador Omar Perotti, trabajamos con el objetivo de una Santa Fe sin ranchos, para generar barrios más accesibles y seguros mejorando las condiciones de hábitat de los sectores más postergados", concluyó Kerz.

## **LOS TRABAJOS**

En barrio **Las Flores** se está trabajando en la reparación integral del Complejo Habitacional Las Flores II. La intervención consiste en la reconstrucción de las áreas comunes, la reparación de las estructuras de hormigón en los núcleos de escaleras y tanques de aguas, impermeabilización de cubiertas, reconstrucción de sendas, y pintura general de las fachadas, siendo su alcance la totalidad de los trabajos, que implican acciones sobre lo espacios exteriores de las unidades habitacionales.

En tanto, en **Centenario** se están ejecutando obras de desagües pluviales con descargas al canal Tacca.; y, paralelamente, se comenzó con la construcción de cordón cuneta sobre la calle Raúl Tacca y sobre las calles transversales. Además, se están realizando los trabajos de cordón cuneta frente al Fonavi San Jerónimo.

En **Villa Hipódromo** se están colocando caños para desagüe pluvial y construyendo bocas de registros y de tormenta. Con respecto a los trabajos viales, ya se ejecutaron 1533 metros de cordón cuneta sobre calles Gaboto, Pedro de Vega, Ricardo Aldao y pasajes varios; y próximamente se construirá el cordón cuneta sobre calle La Madrid y se completará los cordones en varias calles ya intervenidas.

[](<>)Por último, en barrio **Pompeya** se están llevando adelante los trabajos de entubado y las acometidas a desagües pluviales existentes. Además, ya se ejecutó la pavimentación en calle Gobernador Freyre en los tramos comprendidos entre Gorriti y Berutti, y Berutti y French. En todos los sectores intervenidos se fueron realizando trabajos de saneamiento de cunetas, como así también bajadas de cañerías de agua y extracciones de árboles sobre la traza.