---
category: Agenda Ciudadana
date: 2020-12-13T13:36:50Z
thumbnail: https://assets.3dnoticias.com.ar/afip.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Agencias'
resumen: "¿Cuál será el salario más bajo alcanzado por Ganancias?"
title: "¿Cuál será el salario más bajo alcanzado por Ganancias?"
entradilla: 'Sube el mínimo no imponible: un empleado soltero que gane más de $74.810
  netos al mes comenzará a tributar este impuesto, mientras que un empleado casado
  con dos hijos lo hará a partir de los $98.963,20.'

---
El mínimo no imponible (MNI) para el pago del Impuesto a las Ganancias subirá 35,38% desde enero de 2021 para todo el año, a partir de la publicación del índice de Remuneración Imponible Promedio de los Trabajadores Estables (Ripte) de octubre que se utiliza para decidir los incrementos.

De esta manera, **un empleado soltero que gane más de $74.810 netos al mes comenzará a tributar este impuesto**, mientras que **un empleado casado con dos hijos lo hará a partir de los $ 98.963,20**.

Estos umbrales podrán modificarse, de contar los contribuyentes con algún tipo de deducciones familiares que se puedan realizar.

Esta actualización se encuentra casi 10 puntos porcentuales por debajo de la establecida para 2020, la cual fue de 44,28%.

Los empleados solteros tributaban a partir de un neto de $55.261 al mes, mientras que para los casados el umbral de pago era  de $ 64.415 con un hijo y  $73.014 pesos si tenían dos.

  
 