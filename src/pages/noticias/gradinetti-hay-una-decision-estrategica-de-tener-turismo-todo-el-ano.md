---
category: Agenda Ciudadana
date: 2020-12-09T11:17:15Z
thumbnail: https://assets.3dnoticias.com.ar/GRANDINETTI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Grandinetti: “Hay una decisión estratégica de tener turismo todo el año”'
title: 'Grandinetti: “Hay una decisión estratégica de tener turismo todo el año”'
entradilla: El secretario de Turismo brindó precisiones sobre el cronograma de apertura
  progresiva del turismo en la provincia y destacó que la actividad es “uno de los
  motores de divisas, crecimiento y empleo”.

---
El secretario de Turismo, Alejandro Grandinetti, brindó precisiones sobre la habilitación de la actividad turística en la provincia de Santa Fe, y destacó: “estamos muy conformes con volver a poner en marcha uno de los motores de divisas, crecimiento y empleo”, una de las pocas actividades económicas que “nos faltaba habilitar”.

Al respecto, precisó: “hemos convenido con el gobernador Omar Perotti, a partir de las disposiciones de circulación que tomó Alberto Fernández, generar un **cronograma progresivo de apertura**, que empezó el 5 de diciembre, con la posibilidad de hacer turismo en la provincia para todos los santafesinos; y a partir del 21 de diciembre va a estar habilitado para todos los argentinos la posibilidad de hacer turismo en toda la provincia de Santa Fe”.

Grandinetti resaltó que desde “hace un mes estamos promocionando el destino en los principales medios de la República Argentina”; e indicó que “Santa Fe tiene que competir, como lo hace en otros rubros. Somos una provincia grande, importante, y lo demostramos en la generación de divisas para la Argentina; nos faltaba poner este motor en marcha”.

[![Mira el video](https://assets.3dnoticias.com.ar/descarga.jpg)](https://assets.3dnoticias.com.ar/GRANDINETTI.mp4 "Reproducir")

Asimismo, el secretario señaló que “la decisión estratégica del gobernador fue acompañada por todo el arco político, incluso de la oposición, tanto en la Legislatura, como por los intendentes de Santa Fe y Rosario, que consideran que tenemos que ponernos en sintonía para desarrollar el turismo en la provincia”.

“Queremos conseguir que vengan más a visitarnos y que muchos santafesinos puedan recorrer” la provincia de Santa Fe; manifestó Grandinetti y precisó que “las expectativas son muy buenas. Sabemos de dónde venimos, sabemos que estamos corriendo atrás del resto de las provincias que llevan muchos años de políticas de Estado; pero cuando los santafesinos nos ponemos de acuerdo somos muy competitivos y creo que tenemos muchas cosas para ofrecer al resto de la Argentina”.

Por último, Grandinetti destacó que “fuimos la segunda provincia de Argentina que obtuvo una distinción por nuestros protocolos de seguridad para desarrollar un turismo seguro para todos aquellos que nos vengan a visitar”, y agradeció “a todos los trabajadores y empresarios que tuvieron la templanza para llevar estos meses de Covid y volver a apostar por un proyecto de turismo; incluso con los hoteles cerrados se estuvieron realizando inversiones, porque ven que hay una decisión estratégica de tener turismo todo el año”.

## **CRONOGRAMA**

El gobierno de la provincia de Santa Fe dispuso, a través del decreto Nº 1640, un cronograma progresivo de la vuelta a la actividad turística en el territorio santafesino. 

La norma establece que a partir del sábado 5 de diciembre de 2020 se habilita para:

* El turismo interno de residentes de la provincia de Santa Fe.
* Los propietarios de inmuebles o complejos turísticos, aunque no sean residentes en la provincia, para concurrir y alojarse en los que son de su propiedad.
* Para el turismo receptivo de personas residentes en otras provincias, cuyo destino turístico se encuentre en los Departamentos Caseros, Constitución, General López, Rosario y San Lorenzo.

Además, el decreto N° 1640 establece que **a partir del lunes 21 de diciembre de 2020 se habilitará el turismo receptivo de personas residentes en otras provincias en el resto del territorio provincial.**