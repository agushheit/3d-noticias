---
category: La Ciudad
date: 2021-05-18T09:44:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Jatón se vacunó y dijo que la situación en la ciudad es "verdaderamente crítica"
title: Jatón se vacunó y dijo que la situación en la ciudad es "verdaderamente crítica"
entradilla: El mandatario local recibió la primera dosis de la vacuna Sputnik-V. Minutos
  antes a una reunión que mantendrá con otros intendentes y el gobernador Perotti
  anticipó que podrá haber más restricciones.

---
El intendente de la ciudad, Emilio Jatón, recibió la primera dosis de vacuna contra el Covid-19. El mandatario, de 57 años, concurrió hasta el centro de vacunación montado en la Esquina Encendida para ser inmunizado con la Sputnik-V.

En el marco del "colapso sanitario" advertido este lunes por funcionarios de la cartera sanitaria, Jatón afirmó que la situación en la ciudad capital es "verdaderamente crítica".

Las declaraciones del intendente fueron minutos antes de una reunión virtual que mantendrá con funcionarios de primera línea del gobierno y el gobernador Omar Perotti. En el encuentro se podrían definir nuevas restricciones.

"Es un momento en el cual debemos repensar muchas de las cosas que hacemos en términos particulares y en términos de sociedad también. Es el momento de pensar en el otro", manifestó.

Comentó que cuando se realizan los análisis de situación "de ocupación de camas, sobre la cantidad de contagiados y la cantidad de personas que fallecen, son números que nos alertan".

Dijo que "toda situación crítica es un llamado de atención. Esta situación no nace hoy, fue en aumento; tenemos una meseta muy alta".

Destacó que "no sucede lo que ocurría en la primera ola (de contagios) donde los jóvenes atravesaban la enfermedad como una simple gripe. Hoy no, hay muchas personas que fallecen y son jóvenes. El virus ha mutado, es mucho más fuerte".

Recordó que en el mes de octubre hubo picos de 371 casos (diarios) "y hoy, con 220 casos (diarios) estamos colapsando los hospitales. Ahí está la diferencia".

Con relación a la primera dosis de vacuna recibida, señaló: "Lo tomé como un hecho importante en mi vida. Me había anotado hace un tiempo, me llegó el turno en el teléfono y acudí a colocarme la vacuna".