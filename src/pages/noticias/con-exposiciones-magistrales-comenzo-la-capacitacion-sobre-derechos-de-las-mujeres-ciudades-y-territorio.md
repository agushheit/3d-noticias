---
category: La Ciudad
date: 2021-04-07T06:46:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/mercociudades.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Con exposiciones magistrales, comenzó la capacitación sobre “Derechos de
  las mujeres, ciudades y territorio”
title: Con exposiciones magistrales, comenzó la capacitación sobre “Derechos de las
  mujeres, ciudades y territorio”
entradilla: "\nEl espacio formativo se desarrollará a lo largo de un semestre para
  el diseño de una guía de buenas prácticas que promueva un urbanismo con perspectiva
  de género. "

---
¿Por qué las mujeres en el diseño de la ciudad y los espacios públicos? Con esa pregunta como título de la capacitación, la profesora emérita Ana Falú del Centro de Intercambio y Servicios Cono Sur Argentina (CISCSA) abrió su ponencia en el Primer Foro del Proyecto “Mujeres en la ciudad: herramientas para reflexionar y diseñar el espacio público desde una perspectiva de género interseccional”.

La iniciativa tiene como objetivo doble capacitar a profesionales vinculados a la construcción -tanto de los sectores público como privado- y elaborar una guía de buenas prácticas para el diseño de proyectos urbanísticos inclusivos, con perspectiva de género. La cooperativa de profesionales de la arquitectura Punt 6, con sede en Barcelona, es la entidad que tiene a su cargo brindar herramientas y pautas para su elaboración. Con esa meta, tras la intervención de Falú, la integrante del colectivo Adriana Ciocoletto ofreció una introducción sobre cómo aplicar la perspectiva de género en la práctica de la arquitectura y el urbanismo.

La propuesta formativa es impulsada por la Municipalidad de Santa Fe a través de la secretaría de Obras y Espacio Público en conjunto con la Dirección de Mujeres y Disidencias, y tiene como ciudad socia a Asunción del Paraguay en el marco del Programa de Cooperación Sur Sur de Mercociudades, y cuenta con el aval de ONU-Hábitat – Representante Regional para América Latina y el Caribe.

**Nuevas miradas para hacer la ciudad**

La secretaria de Obras y Espacio Público, Griselda Bertoni, y la directora de Mujeres y Disidencias, Soledad Artigas, encabezaron la jornada de apertura, mientras que el encuentro fue coordinado por Mercedes Tejedor, quien está a cargo de la Agencia de Cooperación, Inversiones y Comercio Exterior del gobierno local.

Bertoni expresó que desde Santa Fe “tenemos un trabajo muy importante que hacer que es que las ciudades intenten dar cuenta de la diferencia que tienen los distintos usuarios”. En ese sentido planteó la necesidad de contemplar edades y ciclos de vida, las dimensiones familiar y física al vincularse con la ciudad “para que los espacios públicos de nuestra ciudad atiendan a todas las necesidades”. El proyecto implica la puesta a prueba del manual a elaborar con el diseño de un proyecto para la intervención de un espacio público a partir de la participación colectiva, a fin de “aportar a la integración y a la igualdad de todos los actores”.

El ciclo de capacitaciones cuenta con más de 150 inscriptos oriundos de Argentina y distintos países Latinoamericanos, lo que para Bertoni “muestra el deseo que hay de ver desde la perspectiva de género a la ciudad. Lo mismo sucede cuando participan las entidades que educan a los arquitectos, como son los representantes de las facultades de arquitectura y de otras disciplinas que están construyendo otro tipos de saberes sobre la ciudad, como geólogos, politólogos y sociólogos”.

Por su parte, Artigas valoró espacios como estos “que nos permiten pensar más estructuralmente, mejor y más eficazmente las intervenciones que como Estado tenemos que hacer”. En su exposición planteó a la inundación del río Salado de 2003 como un quiebre que profundizó las desigualdades. En ese sentido, la funcionaria afirmó que “como municipio, el intendente Emilio Jatón nos planteó el desafío de achicar esa brecha que existe en Santa Fe, una ciudad con particularidades, un territorio diferente, con sus dos márgenes mirando al agua”.

Finalmente, aseguró que “las mujeres estamos haciendo un cambio, una épica feminista -expresó parafraseando a Falú-, necesitamos nuevas metáforas y leyendas que nos cuenten, y la ciudad es parte de eso”.

![](https://assets.3dnoticias.com.ar/mercociudades1.png)

**Repensar los espacios que habitamos**

Las capacitaciones a cargo de CISCSA comprenden la realización de tres foros y el abordaje de diversos temas a través de siete módulos en los que se abordarán problemáticas como las dimensiones analísticas de la política pública, territorial y urbana; el derecho a la tierra, el ordenamiento territorial e instrumentos para la planificación; la noción de vivienda y ciudad; la dimensión de movilidad y ciudad; y las violencias y espacios públicos. También se compartirán experiencias locales desde una perspectiva feminista de Latinoamérica y habrá una instancia práctica.

Ana Falú, en su ponencia, destacó la importancia de trabajar colectivamente y en red en pos de un “municipalismo transformador” desde una perspectiva feminista. La integrante de CISCSA afirmó que “estamos en un cambio de épica; es un momento de voluntad política con la que tenemos que trabajar, y Mercociudades es una oportunidad”. Al respecto, señaló que “se está trabajando para reforzar la importancia de la perspectiva de género en las prácticas del Estado, en el sentido de su obligatoriedad y de las responsabilidades institucionales”.

En ese sentido, sobre el proyecto impulsado por la ciudad de Santa Fe expresó que “muestra lo que para mí es central y es de un peso significativo: que la secretaría de Obras Públicas tenga iniciativa para transversalizar estas miradas y estas dimensiones en la institucionalidad y en la práctica del gobierno local”.

A su turno, Adriana Ciocoletto, de Punt 6, dio herramientas sobre cómo aplicar la perspectiva de género en la práctica de la arquitectura y el urbanismo. En su ponencia consideró que la transformación de los espacios supone un abordaje interdisciplinario de experiencias y de distintas escalas, desde el cuerpo hasta la ciudad, y atender las esferas de lo cotidiano. En ese camino, plantea tres objetivos desde el urbanismo feminista: “El primero es cómo poner los cuidados en el centro, desjerarquizando, despatriarcalizando los espacios y el urbanismo actual, y romper el binarismo entre lo público y lo privado, incluyendo una perspectiva ecofeminista. El otro eje es la seguridad y la autonomía, cómo hacer para que los espacios sean libres de violencias. Por último, incluir la participación y la experiencia de las mujeres para el aporte de sus conocimientos diferenciados, como sujetas de la transformación de estos espacios”.

**Proyecto valioso**

Participaron del lanzamiento del ciclo de capacitaciones funcionarias de la Municipalidad de Asunción, como la directora de Relaciones Internacionales y Cooperación Externa, Sandra Díaz Vera, quien agradeció al municipio santafesino por la iniciativa de trabajar con la ciudad de Asunción y manifestó su disposición a seguir trabajando, apoyar, acompañar en todo lo posible.

Por su parte, Marcela Petrantonio, secretaria Ejecutiva de Mercociudades, planteó que el tema de género es “central en la agenda de Mercociudades” y destacó que en este proyecto “están poniendo en valor real el tema de la inclusión desde un aspecto muy novedoso, que tiene que ver con cómo abordar los temas de género e inclusión desde una perspectiva urbana. No nos queda más que felicitarlas”.

El Primer Foro del Proyecto “Mujeres en la ciudad: herramientas para reflexionar y diseñar el espacio público desde una perspectiva de género interseccional” se encuentra disponible para su visualización en el Canal de YouTube de la Municipalidad local.

![](https://assets.3dnoticias.com.ar/mercociudades2.jpg)