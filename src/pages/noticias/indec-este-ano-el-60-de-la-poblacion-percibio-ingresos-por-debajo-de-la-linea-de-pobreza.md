---
category: Agenda Ciudadana
date: 2021-06-30T08:45:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/inflación.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Indec
resumen: 'Indec: este año el 60% de la población percibió ingresos por debajo de la
  línea de pobreza'
title: 'Indec: este año el 60% de la población percibió ingresos por debajo de la
  línea de pobreza'
entradilla: Según el informe del organismo, el 60% de los argentinos percibió $42.394
  por mes en promedio al término del primer trimestre de este año, con el umbral de
  pobreza en $50.854.

---
Alrededor del 60% de la población percibió en promedio ingresos de $ 42.394 al término del primer trimestre de este año, informó este lunes el Instituto Nacional de Estadística y Censos (Indec), mediante su informe sobre la Distribución del Ingreso.

En base a los resultados del informe sobre la "Distribución del Ingreso", el organismo precisó que el 10% más pobre de la población total explicaba el 1,1 % del total de ingresos, mientras que el 10% más rico concentró el 31,8% al cierre del primer trimestre del año.

En lo que respecta al nivel de equidad de los ingresos, medidos a través del Coeficiente de Gini (una relación matemática que tiene al "0" como el nivel de mayor igualdad y al "1" con el de mayor desequilibrio), al cierre del primer trimestre fue de 0,445 puntos, frente al 0,444 de igual período de 2020.

Respecto a la relación de dependencia de los hogares, la cantidad de personas no ocupadas fue de 139 por cada 100 ocupados, mientras que la cantidad de no perceptores de ingreso fue de 69 por cada 100 perceptores.

En lo que respecta al ingreso familiar por hogares, el informe del Indec reseñó que el 60% de estas unidades percibieron salarios de hasta $ 70.000 al cierre de primer trimestre del año. En este sentido, se asentó que en el país hay 8,6 millones de asalariados mientras que los no asalariados son 3,2 millones de no asalariados.