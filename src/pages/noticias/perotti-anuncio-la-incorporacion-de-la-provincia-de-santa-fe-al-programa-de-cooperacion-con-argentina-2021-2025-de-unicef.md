---
category: Estado Real
date: 2021-11-04T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/UNICEF.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti anunció la incorporación de la provincia de Santa Fe al programa
  de Cooperación con Argentina 2021-2025 de UNICEF
title: Perotti anunció la incorporación de la provincia de Santa Fe al programa de
  Cooperación con Argentina 2021-2025 de UNICEF
entradilla: 'El gobernador se reunió con Luisa Brumana, representante en Argentina
  del Fondo de las Naciones Unidas para la Infancia. '

---
El gobernador Omar Perotti recibió este miércoles, en la capital santafesina, a la representante de UNICEF en Argentina, Luisa Brumana. En la oportunidad, el mandatario anunció que Santa Fe es una de las cinco provincias priorizadas (junto con Chaco, Salta, Jujuy y Buenos Aires) para incorporarse al Programa de Cooperación con Argentina 2021-2025, para centrar sus acciones en la implementación de las diferentes líneas.

Esto implica la formulación de un plan de trabajo conjunto para el período 2022-2023. Acceso a asistencia técnica, capacitaciones, intercambio y recursos para la implementación de determinados programas volcados en cinco ejes de trabajo: la reducción de la pobreza y las desigualdades; el desarrollo integral de niñas y niños de 0 a 6 años; las oportunidades equitativas para adolescentes; los entornos libres de violencia, protección y justicia; y la sociedad comprometida con los derechos de los niños, niñas y adolescentes.

Estos ejes permitirán a la provincia potenciar el programa Santa Fe+Conectada; acompañar el trabajo del gobierno en materia de acceso a derechos para las infancias en salud, justicia, educación, género, entornos libres de violencias, desarrollo deportivo y artístico, así como el trabajo con las comunidades en las regiones más necesitadas.

En la oportunidad, Perotti destacó el “deseo de que UNICEF siga profundizando su presencia en la provincia”, y “poder aprovechar al máximo los aciertos y también los errores de otros programas”. En este sentido, señaló que “ir al territorio es un enfoque clave. Seguramente con las situaciones que podamos coordinar, tendremos insumos para mejorar el aprovechamiento de los recursos y los programas exitosos”, señaló el gobernador.

Y agregó: “La satisfacción de la coordinación de los equipos y el querer vincular rápidamente las experiencias positivas de UNICEF en el mundo y, particularmente, en la Argentina con nuestro territorio para poder darle a nuestros niños, niñas y adolescentes el mejor acompañamiento en el futuro”, dijo Perotti, para luego agradecerle a la representante de UNICEF en Argentina por la jornada de trabajo y el inicio de actividades en común en los próximos años.

Por su parte, Luisa Brumana sostuvo que “los ejes principales del Programa de Cooperación con Argentina 2021-2025 responden a las necesidades de la niñez y adolescencia en Argentina, en el contexto presente”, y destacó “dos características: una es la intersectorialidad en la respuesta, al definir prioridades vemos que muchos de los desafíos tienen raíces interdisciplinares; y la segunda es la territorialidad”.

Por último, la representante de UNICEF en Argentina precisó que “las temáticas urgentes son la conectividad, y cómo son los sistemas de protección, creo que allí tenemos oportunidades inmediatas. Hablamos de una sociedad comprometida con la niñez y cuando hablo de sociedad, quiere decir de todos los actores, incluyendo los propios medios de comunicación, el sector privado, la propia familia, para que realmente, trabajando juntos, los derechos del niño se puedan cumplir. Entonces, esos son los grandes ámbitos, las prioridades de nuestros programas de cooperación que derivan de un análisis muy detallado de lo que está pasando con la niñez en el país”, concluyó Brumana.

**PROGRAMA MUNA**

Por la mañana, en el salón Blanco de Casa de Gobierno, las autoridades presentaron el Programa Municipio Unido por la Niñez y Adolescencia (MUNA), al cual adhirieron los municipios de Rafaela, Reconquista, Tostado, Rosario, Santa Fe y Venado Tuerto los que, a través de esta iniciativa, accederán a un sistema de incentivos y una estrategia de trabajo para fortalecer la gestión de las políticas dirigidas a infancias.

El objetivo es fortalecer las capacidades de gestión, ofrecer asistencia técnica y acompañamiento para que las administraciones públicas locales realicen autodiagnósticos, diseñen, implementen y monitoreen planes de acción integrales, participativos y con enfoque de derechos.

En ese marco, Perotti afirmó que vamos a “corresponder con nuestro acompañamiento a cada una de las iniciativas, que se podrán potenciar y realizar con los actores locales. Creemos profundamente en la descentralización, en el trabajo coordinado con los municipios, en el desarrollo territorial y la formación de recursos humanos aprovechando la cercanía. Estamos deseosos de poder aprovechar este programa plenamente”, sostuvo el gobernador.

Al respecto, Brumana reconoció que “para nosotros es un hito llegar a las provincias”, y explicó que “MUNA es un programa a través del cual otorgamos reconocimientos a los municipios que llevan a cabo acciones destinadas a la niñez a través de un proceso que nos parece fundamental, que empieza por un diagnóstico de las prioridades o de los desafíos prioritarios para la niñez y adolescencia. Y, a partir de ahí, una planificación estratégica para poder mejorar los ámbitos más problemáticos para este grupo”, dijo la representante de UNICEF en Argentina.

Finalmente, Brumana explicó que “esta es una primera instancia, una prueba piloto con un número acotado de municipios”, pero que en el futuro podrá extenderse a las localidades interesadas que “manifiesten su compromiso político a través de la firma de un convenio, y desde UNICEF la intención es apoyar con herramientas y capacitaciones para llevar a cabo todo el proceso”.

Mientras, el ministro de Gestión Pública, Marcos Corach, dijo que “tenemos que trabajar fuertemente en el compromiso con los niños, niñas y adolescentes. Esta iniciativa Muna renueva el proyecto del gobierno de la provincia para concretar un importante proyecto. Queremos que las ciudades crezcan, que se desarrollen y que tengan condiciones para que cada uno de los habitantes puedan quedarse y generar arraigo”, subrayó, e indicó que “queremos generar infraestructura y servicios en cada uno de los municipios”.

A su turno, la ministra de Educación, Adriana Cantero, señaló que “es un placer que nos podamos encontrar, compartir preocupaciones y ocupaciones en torno de las infancias y adolescencias, y renovar el compromiso por su bienestar con la posibilidad de desplegar políticas públicas que garanticen derechos de cuidado y protección para su crecimiento”.

Por último, la ministra de Igualdad, Género y Diversidad, Celia Arena, destacó que trabajar “con perspectiva de género y diversidad incluye la perspectiva de las infancias porque es un trabajo que tiene que ir absolutamente a la par. Hay una cuestión que es esencial, que es trabajar mancomunadamente en una forma multinivel y multiorganizacional de manera territorial”.

**PRESENTES**

De las actividades participaron también la ministra de Salud, Sonia Martorano; el ministro de Desarrollo Social, Danilo Capitani; las secretarias de Niñez, Adolescencia y Familia, Patricia Chialvo; de Cooperación Internacional e Integración Regional, Julieta De San Félix; y de Justicia, Gabriel Somaglia; los intendentes de Santa Fe, Emilio Jatón; de Rafaela, Luis Castellano; de Rosario, Pablo Javkin; de Reconquista, Enrique Vallejos; de Tostado, Enrique Mualem; representantes de la municipalidad de Venado Tuerto; y la defensora de Niñas, Niños y Adolescentes de la provincia, Analía Colombo.

En tanto, por UNICEF estuvieron el especialista en Inclusión Social y Monitoreo, Sebastián Waisgrais; la especialista en Educación, Cora Steinberg; la oficial de Protección, Sabrina Viola; la oficial de Políticas Sociales, María Fernanda Paredes; y el asociado en Comunicación, Matías Bohoslavsky.