---
category: La Ciudad
date: 2021-04-26T06:00:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/taxis.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Funcionamiento del transporte público de pasajeros en la ciudad con las restricciones
title: Funcionamiento del transporte público de pasajeros en la ciudad con las restricciones
entradilla: Ante la prohibición de circular en vehículos particulares desde las 21
  hasta las 6, los santafesinos deben optar por el transporte de taxis, remises y
  colectivos.

---
Desde el viernes 23, rige el decreto provincial 386/21 de restricciones a la circulación de personas en vehículos particulares en el horario de 21 a 6 (para aquellas que no están dentro del grupo de los denominados esenciales), y por ejemplo para ir a un bar o restaurante, que estarán abiertos hasta las 0, quienes quieran trasladarse deberán optar por el transporte público.

Desde la asociación de taxi metristas señalaron que reforzarán el servicio de taxis entre las 20.30 y las 0.30. Remarcaron que luego de la medianoche trabajarán con un 10% de la flota.

Por su parte desde UTA aseguraron que, a pesar de las nuevas medidas, se mantendrán las mismas frecuencias con las que vienen funcionando las líneas.