---
category: Agenda Ciudadana
date: 2020-12-27T12:26:42Z
thumbnail: https://assets.3dnoticias.com.ar/2712-ventas-came.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Prensa CAME'
resumen: Las ventas navideñas cayeron 10,1% interanual, según CAME
title: Las ventas navideñas cayeron 10,1% interanual, según CAME
entradilla: El ticket promedio, según los negocios encuestados, se ubicó este año
  en $ 1.800

---
Las ventas minoristas en Navidad cayeron 10,1% interanual, debido a la baja en los ingresos de los hogares, reuniones familiares menos numerosas y el comercio ilegal, informó hoy la Confederación Argentina de la Mediana Empresa (CAME).

«Dura Navidad para la mayoría de los comercios del país. Las ventas minoristas cayeron 10,1% frente a la misma fecha del año pasado, según la tasa de variación promedio en las cantidades informadas por los empresarios», indicó la entidad en un comunicado.

Según el relevamiento de CAME realizado en 1.500 comercios entre el 18 y el 24 de diciembre, el 63% de los negocios tuvo descenso de ventas, pero al mismo tiempo el 51% tuvo faltantes y señalaron que, «si hubieran tenido más mercadería, podrían haber vendido más».

Sin embargo, el 53% de los comercios vendió menos de lo esperado.

El ticket promedio, según los negocios encuestados, se ubicó este año en $ 1.800, un 39,9% arriba de 2019 ($ 1.283), que a precios constantes implica un incremento de 2,8%.

De esta manera, las ventas de Navidad quedaron por debajo de las de 2018, cuando se comercializó un 9% menos que el año anterior.

Para CAME, uno de los factores que produjeron la caída de las ventas fue el comercio ilegal.

«Artículos como indumentaria, calzados, bijouterie, joyería, ropa deportiva, textil blanco o jugueterías, se vieron muy perjudicados por el comercio ilegal en la mayoría de las ciudades medianas y grandes del país, notándose sobre todo en la de Buenos Aires y el Conurbano, donde se desataron batallas campales entre manteros y comerciantes», apuntó el informe.

En este sentido, agregó que «nunca se habían visto tantos manteros, vendedores por redes, y “saladas” y “saladitas” del país con tanta gente como en esta festividad».

Sumado a esto, la caída ocurrió por las reuniones menos numerosas debido a la pandemia de coronavirus y la retracción de ingresos en los hogares.

La baja más marcada la tuvo el rubro de joyería y relojería, con una merma del 23%.

<br/>

<span style="font-family: helvetica; font-size: 1.3em; font-weight: 800;"> **Accesorios**</span>

Según la entidad, este es un sector «con cada vez menos presencia en las ventas navideñas», debido a que «muchos de esos artículos se venden como accesorios en casas de otros productos, y otra por la abundancia de manteros en la calle que coparon la venta de ese ramo».

«Así, al revés del resto de los comercios, no tuvieron problemas de abastecimiento, tenían mercadería, pero no la pudieron vender», agregó.

A este rubro le siguieron librerías (-16,1%); videojuegos, consolas e informática (-15,8%); bijouterie y accesorios (-15,8%); electrodomésticos y artículos electrónicos (-12,8%); y calzado y marroquinería, (-11,9%).

Respecto a los electrodomésticos, en la merma en las ventas «se notó la caída del poder adquisitivo familiar, la falta de límites en las tarjetas de crédito, la resistencia a tomar muchas cuotas sin interés y el mayor control de gastos».

«Además de los faltantes de mercadería, en muchos locales comentaron que también se complicó el abastecimiento por problemas en la cadena de pago: si antes se podía pagar con cheques a 90 o 120 días, ahora los desembolsos debieron realizarse con transferencia bancaria inmediata», puntualizó la entidad empresaria.

En el caso de juguetes y rodados, la tasa de variación promedio informada por las tiendas fue de una baja del 9,8% anual.

«Al haber reuniones más reducidas, los regalos fueron menores, además de influir el comercio ilegal y los faltantes de mercadería, especialmente de juguetes importados, que son muy buscados para niños de entre 7 y 11 años», explicó CAME.

Entre los rubros que menos cayeron se ubicó indumentaria y lencería (-9,5%); perfumería y cosméticos (-8,8%); artículos deportivos y de recreación (-8,4%); textil blanco y ropa de cama (-7,5%); bazar y regalos (-6%); y alimentos y bebidas (-5,3%).

Este último rubro tuvo la mejor _performance_ de los relevados.

«Hubo menos abundancia de comida este año en la mesa navideña y los negocios del rubro lo notaron. Donde más se sintió fue en la categoría dulces», destacó el informe.

En este sentido, CAME remarcó que «salieron productos de menor costo y quienes buscaron alimentos importados, prácticamente no consiguieron».

El relevamiento remarcó que de todos los rubros relevados, ninguno arrojó valores positivos en la comparación interanual.