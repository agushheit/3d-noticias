---
category: Agenda Ciudadana
date: 2022-05-20T10:42:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/clases.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: "¿Reforma educativa sin marco legal y a cargo de docentes?"
title: "¿Reforma educativa sin marco legal y a cargo de docentes?"
entradilla: 'La agrupación Docentes por la Educación, publicó en sus redes sociales
  un informe sobre la Jornada Institucional Ministerial. '

---
En el informe expresa que _"La propuesta provincial consistió en una invitación a planificar, desde un horizonte ético- político que da sentido y direccionalidad a la vida institucional. Dictamina el documento del Ministerio de Educación de la provincia de Santa Fe._

_En este sentido es necesario preguntarnos quién determina ese horizonte ético político, ¿los docentes desde sus saberes organizacionales e institucionales? ¿Sólo desde las teorías pedagógicas curriculares críticas, emancipadoras? O ¿los documentos Acuerdos Marco de ministros?"_

En Santa Fe todo se hace desde las resoluciones ministeriales del gobierno de ministros que, muchas veces, se vuelven contradictorias y enmarañadas; entonces ¿desde dónde se “invita” a los docentes a construir una “planificación anual que diseñe proyectos pedagógicos inclusivos, plurales, organizando los contenidos y saberes en ejes de conocimiento que excedan la lógica anualizada” con evaluación (solo) formativa que, además, debe considerar los siguientes factores:

1-Justicia distributiva y construcción de mejores condiciones de educabilidad.

2-Los actores que conforman la comunidad escolar.

3-Los recursos materiales y simbólicos.

4-Espacios de enseñanza y aprendizaje significativos.

5- Justicia educativa

No podemos pasar por alto el quehacer docente y el malestar que se genera al recibir el material que la cartera educativa pone a disposición para la realización de las consignas propuestas para estas jornadas.

Nuevamente nos encontramos a la deriva y llenos de cuestionamientos. ¿Es la institución solamente la que definiría los conocimientos a distribuir? ¿No existen los contenidos curriculares nacionales y provinciales ya suficientemente adaptados? ¿Se ampliarán las plantas con nuevos actores en roles y funciones? ¿Se estaría habilitando a los docentes a demandar (como siempre) más recursos humanos, materiales y simbólicos? ¿Se van a dotar a las escuelas con laboratorios, salones de música, danzas, teatro, vestuarios, cantinas /comedores, campos de deportes? ¿Y no es dado pensar que la mejor justicia es fortalecer el sistema público de enseñanza con reglas claras, y autonomía institucional para realizar las adaptaciones curriculares prescriptas?

Además, el 17 de mayo, a 10 días del cierre del primer trimestre, el Ministerio de educación de la provincia de Santa Fe, envió una circular firmada por el Secretario de Educación Secundaria a las delegaciones regionales, la cual carece de precisión, está redactada en potencial y apela a expresiones tales como “nos encontramos diseñando formatos escolares con nuevas miradas (...)”. La pregunta es: ¿quiénes estamos diseñando nuevos formatos escolares? ¿Acaso las Jornadas que se están implementando son para los docentes, implícitamente, legitimemos estos “nuevos modelos educativos?

En otro párrafo de la circular puede leerse “(...) utilizaremos para las correspondientes acreditaciones, las escalas de calificaciones previstas en el decreto 181/09 considerando las buenas experiencias recogidas durante el fin de la Unidad Pedagógica 2020/2021, alentamos (sic) a seguir trabajando por Trayectos Curriculares, de manera colegiada, (...)”

¿En qué parámetros se basarán para decir que las experiencias fueron buenas? ¿Acaso buenas experiencias es sinónimos de buenos resultados? Y en todo caso, ¿a qué tipo de resultados nos estaríamos refiriendo? ¿A los porcentajes de promoción que anuncia la Ministra y que solo sirven para embellecer las estadísticas? ¿O a los resultados académicos deseables que les permitan a nuestros alumnos construir sus aprendizajes futuros sobre la base de conocimientos, procedimientos, hábitos y actitudes que los acerquen a los que necesitarán para titular y les sirvan de andamiaje para transitar el ingreso a estudios superiores o al mundo del trabajo de manera exitosa Más adelante habla de “identificar, ajustar y habilitar múltiples formatos y nuevas estrategias de enseñanza para lograr aprendizajes de calidad en el alumnado.” ¿Qué formatos? ¿Acaso cualquier formato es válido de por sí y apoya a la función propedéutica de la escuela secundaria? Hace tiempo nos venimos preguntando si las autoridades se han olvidado de cuáles son las funciones de la educación secundaria y del artículo 30 de la Ley de Educación Nacional N° 26206 que expresa: La Educación Secundaria en todas sus modalidades y orientaciones tiene la finalidad de habilitar a los/las adolescentes y jóvenes para el ejercicio pleno de la ciudadanía, para el trabajo y para la continuación de estudios.

Claramente con estas políticas no estamos ayudando a que se concreten ninguno de estos objetivos.

A continuación, la circular recuerda que “en el bienio 2020/2021 se llevó adelante la promoción acompañada de los estudiantes de primer año y con la promoción del resto del estudiantado en un sistema armado por Trayectos Curriculares, un sistema que han servido para fomentar la especulación, el desinterés y profundizar desigualdades.

Hemos visto alumnos con 6 espacios curriculares promover y otros con 3 repetir, entonces ¿dónde queda la tan mentada justicia curricular?

Antes de finalizar se nos pide “para aquellos estudiantes en promoción acompañada o con espacios pendientes de aprobación, les pedimos que las instancias de evaluación previstas en el calendario de los turnos de julio, septiembre y octubre, no se lleven adelante por tribunal examinador, sino que se establezcan dispositivos de evaluación situados similares a los que fueron contemplados en la Resolución Ministerial 223/20”. Para los olvidadizos nos permitimos recordar que dichos “dispositivos” constituían trabajos prácticos donde a los docentes se nos obligaba a evaluar una materia seleccionando únicamente dos temas centrales.

Otra situación a contemplar es la sobrecarga laboral a la que se nos somete a los docentes exigiéndonos que elaboremos, pongamos en práctica y luego evaluemos mediante estos dispositivos. ¿En qué tiempos?

¿Los docentes vamos a seguir de brazos cruzados viendo cómo nuestros alumnos egresan sin los conocimientos mínimos perdiendo así tiempo y oportunidades? ¿O en algún momento vamos a despertar del letargo, cerraremos la puerta del aula y nos dedicaremos a honrar aquello para lo que nos formamos?

Luego de analizar ambos eventos y los documentos pertinentes, surgen muchísimas dudas:

¿Estaríamos muy equivocados en pensar que se está proponiendo una transformación de fondo del sistema educativo santafesino? Si esto es así, ¿no se debieran convocar a otros actores sociales, políticos, de la producción y/o de los campos donde se insertarán los estudiantes.? ¿Las organizaciones escolares están lo suficientemente fuertes y receptivas para semejantes cambios? Sus esfuerzos (pos pandemia) hoy están definidos, en torno a la política que prescribe “La educación como política pública de cuidado de todos y todas, desafía a poner a disposición de los estudiantes, un abanico de alternativas y estrategias para crear condiciones que hagan viables los recorridos escolares sin repitencia, asegurando aprendizajes relevantes...”

Entonces, para finalizar, ¿es posible que se estuviera implantando una Reforma educativa desde las instituciones sin los marcos legales, pertinentes, en caso de Santa fe sin una Ley Provincial, aprovechando la coyuntura pos pandemia con lo que se justifica todo?