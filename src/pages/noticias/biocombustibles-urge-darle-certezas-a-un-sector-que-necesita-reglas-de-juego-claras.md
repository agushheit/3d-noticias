---
category: Agenda Ciudadana
date: 2021-04-13T09:11:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/juanchi.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Juan Martín
resumen: 'Biocombustibles: “Urge darle certezas a un sector que necesita reglas de
  juego claras"'
title: 'Biocombustibles: “Urge darle certezas a un sector que necesita reglas de juego
  claras"'
entradilla: 'El diputado de Juntos por el Cambio Juan Martín lo marcó como premisa
  en una reunión en la que participó este lunes con el Foro Empresario Región Centro. '

---
El diputado de Juntos por el Cambio Juan Martín lo marcó como premisa en una reunión en la que participó este lunes con el Foro Empresario Región Centro. Se manifestó a favor de prorrogar la ley de incentivo al sector que vence el mes próximo y a discutir un plan de desarrollo para el mediano y largo plazo. 

Este lunes, el diputado nacional de Juntos por el Cambio Juan Martín se manifestó a favor de prorrogar la ley de biocombustibles, que permite incentivos a ese sector clave de varias provincias, entre otras Santa Fe, y expresó su predisposición a discutir un plan de desarrollo para el mediano y largo plazo. Lo hizo durante una reunión de la que fue parte, convocado por el Foro Empresario Región Centro, de la que participaron representantes del Congreso Nacional por Santa Fe, Córdoba y Entre Ríos. Vale recordar que la prórroga de la ley tiene media sanción del Senado, pero no hay consenso aún para su aprobación en la Cámara de Diputados, principalmente por reticencias del kirchnerismo. 

“El sector de los biocombustibles es estratégico para las provincias que representamos, como son Santa Fe, Córdoba y Entre Ríos”, afirmó el legislador nacional radical. “Pero -apuntó- creemos que el desafío es poder discutir integralmente qué desafíos en materia energética y de desarrollo en el mediano y largo plazo tiene el país, para sintetizar una visión que no solamente sea sectorial de las provincias que representamos”. 

Juan Martín dijo que “como lo hicimos saber en la sesión especial que el oficialismo hizo fracasar, nuestro compromiso es replicar en Diputados lo que sucedió en el Senado con el acuerdo de todas las fuerzas políticas, y que podamos así darle certeza a un sector que necesita reglas de juego claras”. 

“Hay que entender que se debe discutir un régimen para el desarrollo del sector sabiendo que la realidad no es la misma a la de 2006, cuando se sancionó la ley que vence el mes que viene”, aclaró el legislador. “Nosotros -puntualizó- le hacemos saber al poder ejecutivo que tenemos el compromiso de sentarnos a discutir el mejor régimen de cara al futuro. Pero eso no puede darse entre gallos y medianoches con una fecha límite del régimen en menos de un mes. Hoy, sin una prórroga de la actual normativa, se encuentran en riesgo muchos puestos de trabajo que además están arraigados en cada una de nuestras provincias, trabajo genuino y de calidad que, reiteramos, requiere de certezas para seguir adelante más allá del 12 de mayo”. 

El diputado de Juntos por el Cambio subrayó que “la base de la discusión tiene que ser establecer reglas de juego claras en el corto plazo como para que también tengamos el compromiso de conjunto para discutir el mejor régimen de cara al futuro”. “Y claramente tienen que basarse premisas claras: que fomente un desarrollo productivo virtuoso en el interior del país, pero también una matriz energética diversificada, con fuente renovables y verdes, como se está haciendo hoy en todo el mundo y cómo debemos hacer para cumplir nuestros compromisos internacionales en materia ambiental”, recordó.