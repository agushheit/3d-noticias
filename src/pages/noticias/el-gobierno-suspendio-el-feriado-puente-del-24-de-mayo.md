---
category: Agenda Ciudadana
date: 2021-05-13T09:02:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/terminal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El gobierno suspendió el feriado puente del 24 de mayo
title: El gobierno suspendió el feriado puente del 24 de mayo
entradilla: Con la medida se pretende desincentivar un posible movimiento turístico
  en el país, que se ve afectado por la segunda ola de contagios de coronavirus.

---
El Gobierno nacional decidió eliminar el feriado puente establecido para el próximo lunes de 24 de mayo, que se sumaba al día de la Revolución, martes 25 de mayo. Según trascendió, se trata de una medida que decidió el ministerio de Turismo a fin de desincentivar la circulación que provoca el turismo y así limitar la propagación del coronavirus por el país.

El último mes de noviembre, el presidente Alberto Fernández había decretado “días feriados con fines turísticos”, en el Decreto 947/2020. Se trataba de una estrategia destinada a promover la actividad turística. Con la cancelación del feriado del próximo lunes 24, el fin de semana extra-largo quedaría interrumpido por un día laborable.

Según indicaron fuentes gubernamentales, el turismo no será suspendido, pero sí se apunta desincentivar un movimiento masivo de personas en el país. Hasta ahora, las restricciones a la circulación establecidas por el Gobierno Nacional para mitigar los contagios de coronavirus expiran el viernes 21.