---
category: Agenda Ciudadana
date: 2020-12-13T13:47:53Z
thumbnail: https://assets.3dnoticias.com.ar/ex-fiscal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Allanaron a un exdiputado provincial mencionado por Ponce Asahad
title: Allanaron a un exdiputado provincial mencionado por Ponce Asahad
entradilla: Darío Scattaglini declaró el miércoles pasado tras ser mencionado por
  el exfiscal detenido por recibir dinero en la causa de Juego Clandestino

---
El domicilio del exdiputado provincial del PJ Darío Scattaglini, en la ciudad de Santa Fe, fue allanado en la mañana de este sábado 12 de diciembre en el marco de la investigación por juego clandestino que aparece con ramificaciones hacia la política, según informa el diario La Capital.

**El nombre de Scattaglini apareció en la causa de juego ilegal durante las audiencias de agosto pasado** que costaron la destitución y la prisión para el exfiscal regional de Rosario Patricio Serjal y el exfiscal adjunto Gustavo Ponce Asahad.

En ellas se ventiló que el 10 de julio pasado un audio donde la secretaria del empresario de juego ilegal Leonardo Peiti recibió un llamado del ex legislador que le dijo que “Traferri”, por el senador justicialista Armando Traferri, se quería reunir con el capitalista de juego.

<br/>

## **Scattagini, tras ello, fue separado como asesor de la Cámara de Diputados.**

El miércoles pasado Scattaglini concurrió a la Agencia de Investigaciones de Delitos Complejos convocado por los fiscales Matías Edery y Luis Schiappa Pietra. Estos lo convocaron para que precise por qué razón llamó en nombre de Traferri para hablar con Peiti.

Luego de su declaración, los fiscales consideraron que había referencias mendaces de parte del exlegislador, por lo que pidieron un allanamiento para secuestrar su teléfono y otros soportes de contenidos. La requisa comenzó a realizarse a las 9 por efectivos del Organismo de Investigaciones (OI).

<br/>

## **La declaración de Ponce Asahad**

Según el exfiscal Ponce Asahad, que declaró en una audiencia imputativa hace ocho días, el senador sanlorencino participa de la estructura de juego ilegal, que según dijo tenía tres patas principales: el legislador, el desplazado fiscal regional Serjal y el empresario Peiti.

"La política se va a abroquelar y no va a soltarle la mano al senador Traferri. Estoy seguro de que Traferri se va a defender diciendo que la llamada de (el exdiputado provincial Darío) Scattaglini a la secretaria de Peiti obedece a un proyecto de ley por el cual querían incorporar a Peiti al sistema oficial de juego de la provincia. Lo sé porque me lo dijo el senador Traferri. La idea era manejar el juego oficial de Santa Fe mediante Peiti que él está preso y yo detenido. Era también en virtud de esto tener el manejo en esta provincia que es a lo que se dedica Peiti, que ha contado con protección política, y uno entre varios de sus protectores es el senador Traferri", sostuvo Ponce.

El jueves pasado, en una entrevista con La Capital, el abogado de Traferri, José Luis Vázquez, señaló que Scattaglini llamó a Peiti de parte de Traferri porque el empresario le había propuesto un proyecto para instalar máquinas de juego en los locales de la Lotería de Santa Fe y deseaba consultarlo al respecto. También dijo que hasta donde sabía ese era el único contacto entre el legislador de San Lorenzo y Peiti.

<br/>

## **Allanan al "Chino" Paz**

Este sábado fue allanada la vivienda de un alto jefe policial retirado que concluyó su carrera en San Lorenzo, Gonzalo “Chino” Paz,  en el marco de las investigaciones que enfocan el juego clandestino en la provincia. 

El operativo, que no encontró al oficial superior en ese inmueble, se debe a la relación presunta entre este y el ex asesor legal de la GUM Aníbal Porri, quien está reiteradamente aludido en las pesquisas por juego ilegal.

Tanto Paz como Porri fueron mencionados en su declaración hace ocho días por el exfiscal Gustavo Ponce Asahad, preso por proteger mediante el cobro de dádivas al empresario de juego Leonardo Peiti, en la audiencia en la que también aludió al senador Armando Traferri como actor importante en la estructura del juego clandestino. Ponce reveló que Porri, ex tesorero del Colegio de Abogados de Rosario, era quien trasladaba el dinero de las coimas pagado por Peiti.

En su testimonio, el exfiscal mencionó que Porri estaba muy preocupado por el destino de una camioneta que era usada por el ex jefe policial de San Lorenzo. Ponce Asahad declaró que Peiti había conformado una sociedad con policías retirados con los que trabajaban en zona de puertos de la región. Y planteó que una vivienda de Porri, valijero supuesto de Peiti, era usada por el Chino Paz como casa de fin de semana.