---
category: Agenda Ciudadana
date: 2021-03-29T06:00:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/monotributo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Avanza el proyecto que define los cambios del régimen de Monotributo
title: Avanza el proyecto que define los cambios del régimen de Monotributo
entradilla: Diputados lo aprobó por unanimidad y envió al Senado el proyecto de reforma.
  Actualiza topes por categoría.

---
La Cámara de Diputados aprobó esta noche por unanimidad y envió al Senado el proyecto de ley de reforma del régimen de Monotributo, que propone actualizar los topes de cada categoría, y crea un puente para facilitar el ingreso de contribuyentes al régimen general.

En el inicio del debate, el presidente de la comisión de Presupuesto y Hacienda, Carlos Heller señaló que "el contribuyente que pase voluntariamente al régimen general tiene cuatro años de beneficios: el 1ro puede descontar las compras de los 12 meses anteriores, el 2do el 50% del IVA, el 3ro el 30% y el 4to el 10%".

Por el radicalismo, el diputado Luis Pastori dijo que “este proyecto no es tan bueno como sí necesario. No es una reforma a la Ley de Monotributo sino una suerte de blanqueo hacia el pasado para determinados contribuyentes que se excedieron en los límites de facturación de su actividad, y una suerte de puente de plata o aterrizaje suave al pasar del Régimen Simplificado al Régimen General”.

**Cambios**

La iniciativa enviada por el Poder Ejecutivo, que contó con el voto positivo de 237 diputados, contempla la actualización para las escalas y montos del monotributo correspondientes al período 2020, que comenzarán a regir cuando se apruebe el proyecto

La reforma propiciada por la AFIP busca armonizar la transición entre el Régimen Simplificado para Pequeños Contribuyentes (RS) y el Régimen General (RG), tanto en términos administrativos como en los montos de las obligaciones que deben afrontar las personas monotributistas.

Para los casos de monotributistas cuya facturación superó el límite más alto del régimen hasta un 25%, se contempla que puedan mantenerse en su actual condición durante el período fiscal 2021.

Los que superaron ese monto se tendrá que pasar al régimen general y se establece una transición para que los contribuyentes puedan afrontar los pagos de Ganancias y de IVA.

De acuerdo al texto, podrán deducir además en el primer año el 50 por ciento del importe que le corresponde pagar de IVA, el 30 por ciento en el segundo y el 10 por ciento en el tercer año.

El proyecto aprobado consta de 17 artículos y establece un Régimen de Sostenimiento e Inclusión Fiscal para los sujetos comprendidos en el Régimen Simplificado para Pequeños Contribuyentes.

“Dudamos de la eficacia de este proyecto hacia el futuro. No estamos promoviendo el paso del Régimen Simplificado al General. Estamos dando muchas facilidades en el IVA más que en Ganancias, pero el problema está en Ganancias, que incide de una manera muy gravosa. Es buena la ley para solucionar problemas del pasado pero no tanto hacia el futuro”, señaló Pastori .

Por su parte, el mendocino José Ramón (Unidad para el Desarrollo), tercer y último orador del acotado debate que se extendió por espacio de algo más de treinta minutos, apuntó que "el monotributo si bien fue un sistema que simplificó y aceleró el comercio, hoy es un sistema que facilita e incluso fomenta la precarización laboral", tras lo cual agregó que "el Estado es el principal precarizador".

“Este es un problema gigantesco y uno de los pilares de la economía en negro, junto con la evasión fiscal. Las tasas son de terror; alcanzan fácilmente el 50% de los trabajadores. En Mendoza hay municipios donde llega al 80%", acotó el diputado cuyano.