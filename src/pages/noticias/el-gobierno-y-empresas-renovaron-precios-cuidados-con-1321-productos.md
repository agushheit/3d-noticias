---
category: Agenda Ciudadana
date: 2022-01-12T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/precios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Gobierno y empresas renovaron Precios Cuidados con 1321 productos
title: El Gobierno y empresas renovaron Precios Cuidados con 1321 productos
entradilla: 'El listado está ajustado a cada provincia. Tendrá una vigencia de tres
  meses. Incluye artículos de almacén, limpieza, librería, perfumería, cuidado e higiene
  personal, entre otros.

'

---
El Gobierno nacional en acuerdo con un centenar de compañías lanzó una nueva lista de “Precios Cuidados”, con 1.321 productos y ajustada a cada provincia que regirá por los próximos tres meses.

El programa consta de artículos de almacén, limpieza, librería, perfumería, cuidado e higiene personal, artículos para bebés, mascotas, frescos (lácteos, fiambres, pastas frescas, tapas de empanadas y de tartas), congelados y bebidas, entre otros.

Para cada jurisdicción se diseñó una propuesta en condiciones de poder ser cumplida por las compañías, las que se comprometieron a mantener abastecidos los comercios de cada área.

**El listado puede leerse en** [**https://www.argentina.gob.ar/precios-cuidados**](https://www.argentina.gob.ar/precios-cuidados "https://www.argentina.gob.ar/precios-cuidados")

Los ajustes de precio respecto a los que regían hasta el viernes pasado serán del orden de 6% a lo largo del período de vigencia.

Desde la Secretaría de Comercio a cargo de Roberto Feletti afirmaron que “si bien el acuerdo ya está vigente, la actual gestión entiende que es importante que las empresas que deciden formar parte del programa también lo expresen a través de este instrumento, lo que le da mayor certidumbre y trazabilidad a esta política pública”.

De allí que durante los últimos días se convocó a las firmas a suscribir un acto acuerdo para sellar el compromiso.

En un comunicado de prensa, Comercio destacó que la inclusión de “una amplia gama de productos esenciales como 45 variedades de leches (larga vida, infantiles, saborizadas, fluidas y en polvo), 22 presentaciones de yerbas, 6 panificados de molde, 24 tipos y tamaños de pañales descartables y 33 variedades de paquetes de pasta seca, entre otros artículos de primera necesidad.

Los productos de Precios Cuidados se comercializan en todo el país, en las principales cadenas de supermercados minoristas y mayoristas.