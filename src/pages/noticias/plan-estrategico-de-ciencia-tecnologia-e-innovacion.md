---
category: Estado Real
date: 2022-05-20T11:50:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-05-20NID_274748O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: PLAN ESTRATÉGICO DE CIENCIA, TECNOLOGÍA E INNOVACIÓN
title: PLAN ESTRATÉGICO DE CIENCIA, TECNOLOGÍA E INNOVACIÓN
entradilla: Se reunió el Consejo Científico, Tecnológico y de Innovación para presentar
  el Plan, realizado por más de 120 actores de 60 instituciones de la provincia durante
  todo el 2021.

---
El Consejo Científico, Tecnológico y de Innovación, integrado por los sectores público, privado y académico, mantuvo un encuentro con objetivo de presentar los principales resultados y acciones futuras del Plan Estratégico de Ciencia, Tecnología e Innovación, contemplado en la Ley Provincial N° 13.742, describir los ejes y los tres proyectos seleccionados desde la Secretaría de Ciencia, Tecnología e Innovación de la provincia para su ejecución, y anunciar la nueva etapa del Plan y la conformación de la Red Territorial de Innovación.

En la oportunidad, la secretaria de Ciencia, Tecnología e Innovación, Marina Baima, afirmó que “el hecho de presentar el Plan valida las iniciativas que surgirán a partir de esto, lo que permitirá escuchar sugerencias para difundir en todos los ámbitos, reforzar este trabajo diferencial que hemos hecho con todos los actores del ecosistema, y hacer que cada proyecto que salga de la Secretaría tenga socios estratégicos para la implementación de este Plan. La ciencia y la tecnología tienen impacto cuando salen de lo emergente: de cada universidad, instituto, centro tecnológico y del sector productivo”.

Por su parte, la coordinadora técnica del Plan Estratégico de Ciencia, Tecnología e Innovación de Santa Fe, Natalia Aniboli (INTI), sostuvo que “estamos trabajando para crear un sistema provincial de ciencia, tecnología e innovación fortalecido que se sustente en el talento y capacidades de sus actores, trabajando de forma integrada para posicionar a Santa Fe en el mundo, promoviendo el triple impacto en la calidad de vida de los santafesinos, basado en los principios de equilibrio territorial, transparencia, agilidad, eficiencia y sostenibilidad en el tiempo”.

En relación al Banco de Proyectos, se informó que dieron nacimiento al mismo 24 proyectos diseñados por los propios actores. Por otra parte, se anunció que se está trabajando en una plataforma web que contendrá información de los proyectos y del Plan CTI 2030.

**TEMAS DE AGENDA**

En la actividad, se expuso también la metodología elaborada para dar continuidad al Plan en la etapa de implementación y acciones focalizadas en territorios. Se planteó un trabajo territorial, haciendo foco en municipios y comunas, con el fin de implementar los proyectos generados en la etapa de co-construcción y diseño del Plan (realizado por más de 120 actores de 60 instituciones de la provincia durante 2021) y la posibilidad de sumar nuevas iniciativas a partir de las demandas, necesidades y oportunidades de cada territorio.

Se conocieron también los proyectos que están en etapa de implementación o próximos a ser implementados por la Secretaría de Ciencia, Tecnología e Innovación:

>> Red VICITEP: relacionado a los ejes estratégicos transversales de talento y vinculación tecnológica. El fin del mismo es incrementar la cantidad y calidad de vinculadores tecnológicos en todo el territorio provincial con diversas acciones.

>> Generación de un Sistema Provincial Informativo sobre Infraestructura y Equipamiento del SPCTeI. El fin del mismo es conocer los equipos de ciencia y tecnología con que disponen las instituciones del ecosistema, sus usos, etc.

>> Financiamiento para la mejora del equipamiento e infraestructura mediante ventanilla continua: del eje estratégico transversal Infraestructura y equipamiento. El fin es contar con una convocatoria abierta para que las instituciones que conforman el ecosistema de ciencia, tecnología e innovación puedan mantener operativos los equipos con los que cuentan.

En el transcurso del año se avanzará conformando mesas de trabajo por localidades las que estarán integradas por actores del sistema de ciencia, tecnología e innovación, del sector productivo, universidades y gobiernos locales. A cada una de ellas se asignará un facilitador que tenga un rol de convocar, incentivar el diálogo, y armar planes de trabajo. De esta manera, se conformará la Red Territorial de Innovación.

De la actividad participaron: el secretario de Industria, Claudio Mossuz; la directora de Promoción Científica de la Universidad Nacional de Rosario, Julia Cricco; el secretario de Ciencia y Tecnología de la Universidad Tecnológica Nacional (UTN) Facultad Regional Santa Fe, Jorge Vega; el secretario de Ciencia, Tecnología y Posgrado de la UTN Facultad Regional Reconquista, Walter Capeletti; la secretaria de Vinculación y Transferencia Tecnológica de la Universidad Nacional de Rafaela, Andrea Minetti; la directora del CCT Conicet Rosario, Sandra Fernández; el director del CCT Conicet Santa Fe, Carlos Piña; el director del INTA Centro Regional Santa Fe, Mario Garrapa; el presidente del Parque Tecnológico del Litoral Centro, Norberto Nigro; el presidente del Polo Tecnológico de Rosario, Ignacio Sanseovich; y el vocal en la Federación Industrial de Santa Fe, Gustavo Zenclussen.

Estuvieron presentes también el director provincial de Tecnología e Innovación, Enrique Bertini; y parte del equipo de coordinación de la Secretaría de Ciencia, Tecnología e Innovación: Marcelo Maisonnave, Agustín Stratta y Germán Veinticcinque.-