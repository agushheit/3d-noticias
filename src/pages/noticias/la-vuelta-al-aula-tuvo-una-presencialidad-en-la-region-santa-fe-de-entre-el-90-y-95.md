---
category: Agenda Ciudadana
date: 2021-07-11T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/VUELTACLASES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La vuelta al aula tuvo una presencialidad en la región Santa Fe de entre
  el 90 y 95 %
title: La vuelta al aula tuvo una presencialidad en la región Santa Fe de entre el
  90 y 95 %
entradilla: Así lo indicaron desde la Regional IV de Educación. El buen tiempo permitió
  que la asistencia en el nivel secundario sea pareja en todos los establecimientos.

---
Desde la Regional IV de Educación indicaron que el nivel de asistencia de alumnos en todos los niveles, en especial el secundario que regresó en esta capital el 1 de julio, dejó buenos registros.

Se trata de la vuelta a la "presencialidad cuidada" definida por el Ministerio de Educación. El objetivo de la cartera educativa consistió en que el alumnado tenga al menos dos clases presenciales antes del inicio del receso escolar.

Mónica Henny, titular de la regional, le dijo a UNO Santa Fe que "en escuelas pequeñas hubo presencialidad plena". Luego advirtió que "en las escuelas más numerosas tenemos hasta tres burbujas y en otras tenemos la bimodalidad alternando presencialidad plena y virtualidad".

En cuanto a la asistencia, le dijo a este medio que "tuvimos los valores normales, de entre el 90 y 95%, que son los valores históricos en educación en cuanto a asistencia media".

Y continuó: "Dentro de la nueva normalidad tuvimos buenos registros de asistencia, sobre todo esta última semana donde las temperaturas fueron mucho más agradables".

Destacó que no hubo "grandes índices de ausentismo, ni de docentes ni de alumnos ni asistentes escolares. En zonas rurales tampoco tuvimos lluvias que anegaran los caminos, lo que permitió el acceso a las escuelas rurales".

Henny recordó que desde "el 1 de julio se sumó en La Capital la secundaria, nosotros tenemos con presencialidad todo el nivel inicial hasta el secundario con obligatoriedad, lo que abarca las modalidades de primaria para adultos, secundarias para adultos (EEMPA) y escuelas especiales. En todas tuvimos buenos índices de presencialidad".

Recordó también que "en los días con temperaturas más bajas tuvimos buena asistencia, esto no fue un impedimento. Esto es porque las escuelas en su gran mayoría están equipadas, hay inversión en infraestructura, los equipos directivos que gestionan las escuelas han podido planificar las inversiones necesarias. No tenemos el 100% de la infraestructura necesaria, pero hemos logrado que se intensifique la inversión", agregó.

Comentó que hubo "algunos problemas en escuelas por el suministro de agua, donde se habían roto algunos caños. Lo pudimos solucionar prontamente y garantizamos la vinculación con los alumnos a través de la virtualidad".

**Presencialidad con más horas**

Adriana Cantero, ministra de Educación de la provincia, señaló este viernes que desde la cartera que comanda se está planificando para en una "presencialidad intensificada", que llevaría la actual jornada de tres horas y media a cuatro o cuatro horas y 15 minutos.

"Queremos recuperar más horas de clase, vamos a intensificar el seguimiento de las trayectorias que se han fragilizado más con una mirada puesta especialmente en la secundaria. Estamos pensando en incrementar la jornada para hacerla completa y propiciar otros espacios de trabajo con distintos grupos", señaló Cantero en declaraciones al programa "El primero de la mañana", de LT8.

"Nosotros estamos con una jornada reducida de tres horas y media y lo que estaríamos recuperando es la jornada de cuatro horas, cuatro horas y quince minutos, por lo cual todos recuperan una hora cátedra más diaria", especificó la ministra.

Las clases presenciales en la provincia retomaron hace poco más de dos semanas en el territorio santafesino y los últimos días de la semana anterior se habilitaron para las escuelas secundarias. Desde el Ministerio de Trabajo, y ante la mejora de la situación sanitaria, se evalúa extender el horario escolar.

Cantero, por otra parte, recordó que el último miércoles. en la asamblea del Consejo Federal de Educación, que reunió al titular de la cartera educativa nacional, Nicolás Trotta, y representantes de todas las jurisdicciones provinciales-, "se aprobó un plan de evaluación integral inédito en Argentina. Es salir de la cuestión excluyente de las pruebas estandarizadas, entenderlas como un hecho complejo y vincular muchas acciones como darles lugar a las evaluaciones de los aprendizajes, pero también a las políticas de enseñanza, a la organización escolar, a la voz de los alumnos y de los docentes".

Respecto de las pruebas Aprender, la titular de Educación expresó: "Acordamos que la primera quincena de diciembre sería la fecha más adecuada para la aprobación de una prueba censal en 6º grado. Y en este sentido el debate que tuvimos tiene que ver con la dificultad de tener todo el país afectado por la no presencialidad en diferentes momentos, con una gran heterogeneidad en cada provincia. Esta prueba censal requiere de esa condición de la presencialidad en simultáneo de los alumnos que serán evaluados en todo el país”, concluyó.