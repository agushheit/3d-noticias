---
category: Agenda Ciudadana
date: 2020-12-18T11:31:26Z
thumbnail: https://assets.3dnoticias.com.ar/1812atp.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: AFIP habilitó la inscripción al programa ATP para diciembre
title: AFIP habilitó la inscripción al programa ATP para diciembre
entradilla: Las empresas que requieran la asistencia estatal para el pago de los salarios
  de diciembre podrán registrarse al Programa de Asistencia de Emergencia al Trabajo
  y la Producción (ATP) hasta el 23 de diciembre inclusive.

---
La Administración Federal de Ingresos Públicos (AFIP) habilitó el sistema para que los empleadores se inscriban con su clave fiscal a través de la página web del organismo recaudador.

El **Programa ATP** forma parte de las políticas implementadas por el Gobierno para amortiguar el impacto económico de la pandemia del Covid-19, dijo la AFIP en un comunicado difundido a la prensa.

A través de la iniciativa, **el Estado invirtió más de 240.000 millones para sostener empleos e ingresos de las trabajadoras y los trabajadores del sector privado**.

Para hacer frente a las distintas realidades que atraviesa el entramado productivo argentino en el marco de la pandemia del COVID-19, el Programa ATP contempla los Créditos a Tasa Subsidiada y el Salario Complementario.

**Esos beneficios son complementados con los REPRO II que instrumentó el Ministerio de Trabajo, Empleo y Seguridad Social**.

Los tres mecanismos de asistencia para el pago de salarios se tramitarán a través del sitio web de la AFIP.

La posibilidad de acceder a cada uno de los beneficios depende del tipo de actividad desarrollada por la empresa y/o la variación nominal en su nivel de facturación.

Los empleadores que accedieron al Programa REPRO II en noviembre contarán el mismo beneficio para el pago de salarios de sus trabajadores y trabajadoras durante diciembre de manera que no será necesario que se inscriban al Programa ATP.

Empresas que no desarrollan actividades críticas y registran una variación nominal negativa en su facturación entre los meses de noviembre de 2020 y 2019.

Deben registrarse en la página web de la AFIP y una vez finalizado el plazo de inscripción podrán optar entre tramitar un Crédito a Tasa Subsidiada del 27% o solicitar el REPRO II que instrumenta el Ministerio de Trabajo, Empleo y Seguridad Social.

Los empleadores que al concluir el período de inscripción decidan tramitar el REPRO II deberán recabar y presentar a través del servicio web "Programa de Asistencia de Emergencia al Trabajo y la Producción – ATP" el último balance (en caso de que la empresa esté obligada a presentar balance) y un conjunto de datos económicos sobre la empresa que son requeridos por del Ministerio de Trabajo, Empleo y Seguridad Social.

La información será solicitada al finalizar el período de inscripción, pero las empresas pueden comenzar a producir la documentación en forma anticipada.

Empresas no afectadas en forma crítica que registran una variación nominal positiva en su facturación de 0% a 35%. Los empleadores (que tengan menos de 800 trabajadores) podrán solicitar un Crédito a Tasa Subsidiada del 33%.

Los créditos contarán con un período de gracia de 3 meses y el repago se realiza en 12 cuotas iguales y consecutivas.

Las empresas que cumplan las metas de creación de empleo establecidas por el Ministerio de Desarrollo Productivo podrán recibir un reintegro total o parcial al finalizar la devolución del crédito.

Empresas afectadas en forma crítica: aquellas firmas que registran una variación nominal negativa en su facturación entre los meses de noviembre de 2020 y 2019 accederán al Salario Complementario.

Cuando la variación de facturación nominal interanual sea entre 0% y 35% podrán acceder al beneficio del Crédito a Tasa Subsidiada, siempre que posean en su nómina menos de 800 trabajadores.