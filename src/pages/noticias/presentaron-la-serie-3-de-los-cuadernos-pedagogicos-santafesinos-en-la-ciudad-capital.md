---
category: Estado Real
date: 2021-09-17T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/CUADEERNO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Presentaron la serie 3 de los cuadernos pedagógicos santafesinos en la ciudad
  capital
title: Presentaron la serie 3 de los cuadernos pedagógicos santafesinos en la ciudad
  capital
entradilla: La ministra de Educación, Adriana Cantero encabezó el acto. La provincia
  invirtió $40 millones para la impresión de más de 1 millón de ejemplares.

---
El Ministerio de Educación de la provincia presentó en la ciudad de Santa Fe la tercera edición de los Cuadernos Pedagógicos destinados a los niveles inicial, primario y secundario y las modalidades técnicas y de educación para adultos, de gestión pública y privada.

La iniciativa, que está producida para favorecer el derecho a la educación, se realizó mediante una inversión de 40 millones de pesos para los 31 ejemplares de la Serie 3, de la que se imprimieron más de 1 millón de ejemplares.

En primer término, la ministra de Educación, Adriana Cantero, destacó que “ha habido mucho trabajo de este gobierno y de este Ministerio de Educación en pos del derecho a la educación, generando políticas públicas que posibiliten mejores condiciones para que todos los chicos y las chicas están en la escuela y para que todas y todos los docentes estén con posibilidades de desplegar su tarea”.

En ese mismo contexto, Cantero expresó que “implementamos el boleto educativo gratuito y el acompañamiento de las trayectorias más frágiles con todos los dispositivos que han servido para ir a buscar a los chicos que tenían debilitada su relación con la escuela. También hemos tenido otras políticas públicas como la evaluación de los edificios escolares y los más de 3.000 millones de pesos invertidos en infraestructura escolar; la planificación constante para ver cómo vamos a crecer porque en Santa Fe celebramos que en el ciclo lectivo 2021 tenemos casi 20.000 alumnos más. Ese es un hecho novedoso en tiempos de pandemia, lo que significa para nosotros mayor compromiso, porque ahora tenemos que generar los espacios qué se necesitan para que todos puedan estar, los equipamientos y los recursos, pero fundamentalmente la tarea silenciosa y cotidiana que haga que las chicas y los chicos encuentren en la escuela sentido y significado para poder aprender y para poder sostenerse”.

Por último, la ministra indicó que “para nosotros siempre estuvo presente la definición de una calidad educativa con justicia educativa, precisamente porque ese es el primer componente de la calidad, que todas y todos puedan estar en la escuela y el segundo componente de la calidad es que estén aprendiendo, es decir, apropiándose de eso que tiene la escuela para legar, que tiene la escuela para donar, para poder abrir mejores perspectivas para entender el mundo contemporáneo, el mundo complejo en el que vivimos y además tener la valentía de querer transformarlo”.

Por su parte, el secretario de Educación, Víctor Debloc, se refirió al concepto de igualdad expresando que “la pedagogía contemporánea considera a la igualdad como un principio que tienen que ser verificado todos los días, por ello el sistema, la política educativa y la escuela deben verificar que esto se cumpla para que el bien igualador, como lo son los Cuadernos, llegue a todos por igual”.

Por último, la subsecretaria de Desarrollo Curricular y Formación Docente, Patricia Petean, indicó que en el proceso de construcción de los materiales participaron “docentes de las distintas regiones educativa, maestras, maestros y profesores que aportaron más voces que otorgan una contextualización donde cada uno se va sentir reconocido en los paisajes de la provincia, en su historia y todo lo que hace a la constitución de nuestra identidad como santafesinos y santafesinas, con proyección hacia el país, hacia Latinoamérica y el mundo”.

El acto se llevó a cabo en el espacio Molino Fábrica Cultural y participaron, además, la subsecretaria de Educación Superior, Patricia Moscato; el director provincial de Investigación y Evaluación Educativa, Francisco Corgnalli, la delegada de la Región IV de Educación, Mónica Henny; coordinadores pedagógicos, supervisores, docentes, representantes gremiales y estudiantes de 5° año de la Escuela Técnica 647 “Dr. Pedro Lucas Funes” de la ciudad de Santa Fe.