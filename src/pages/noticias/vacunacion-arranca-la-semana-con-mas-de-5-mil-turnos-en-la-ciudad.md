---
category: Agenda Ciudadana
date: 2021-08-16T06:00:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Vacunación: arranca la semana con más de 5 mil turnos en la ciudad'
title: 'Vacunación: arranca la semana con más de 5 mil turnos en la ciudad'
entradilla: Este lunes feriado, tanto en la Esquina Encendida como en La Redonda,
  se entregaron turnos para la colocación de segundas dosis de la vacuna Astrazeneca.

---
Este lunes feriado, comienza la semana con más de 5 mil turnos otorgados en la ciudad de Santa Fe para la continuación del plan de vacunación contra el coronavirus. En ese contexto, tanto la Esquina Encendida como La Redonda, serán los centros vacunales donde se colocarán segundas dosis de la vacuna Astrazeneca.

En la Esquina Encendida se otorgaron 3.600 turnos y el horario de vacunación será de de 8 a 16:30 con segundas dosis de Astrazeneca divididas de la siguiente manera: 23 dosis para mayores de 60 años, 3.385 para personas de entre 18 a 59 años, 46 dosis para personal de salud, de 29 para personal de seguridad y 117 para docentes y auxiliares.

Mientras tanto, en La Redonda se otorgaron 1.600 turnos este lunes feriado en horario de 8 a 15:30. Las segundas dosis Astrazeneca serán destinadas a 52 mayores de 60 años, a 1.426 personas de entre 18 a 59 años, a 71 docentes y auxiliares, a 16 trabajadores del área salud y a 35 miembros de la cartera de seguridad.

La semana pasada más de 210 mil santafesinos y santafesinas completaron su esquema de inmunización contra el Covid-19. En ese marco, la ministra de Salud, Sonia Martorano, afirmó: "Nuestra meta es llegar al millón de personas con dosis completa. Tenemos orden del gobernador que vacuna que llega, vacuna que se coloca".

“La vacunación avanza a todo ritmo – agregó Martorano. El miércoles pasamos los 3 millones de dosis y estamos avanzando en segundas dosis. Seguro próximamente llegaremos al millón de personas con esquema completo”, afirmó con optimismo la funcionaria provincial quien también se mostró conforme con la vacunación a personas jóvenes. “Lo bueno es que se anotaron muchos menores, esperando vacunas, porque las que hay es para completar esquemas”, sumó.

En ese sentido, la ministra anticipó que “la semana próxima llegan más dosis de Sinopharm y Sputnik del primer componente, por lo que estaríamos llamando a menores de 21 años”.

**Combinación de vacunas**

Vale recordar que la recomendación de las autoridades es aceptar esta combinación ya que ha sido aprobada porque son seguras y eficaces.

“Aquellas personas que superaron los 90 días de haber recibido la primera dosis de Sputnik V, hoy están completando el esquema con Moderna, que es una excelente elección porque es una combinación de ARN mensajero y, como saben, en el mundo la combinación de vacunas es una de las maneras más eficientes y eficaces”, había señalado la ministra de Salud, Sonia Martorano.

**Variante Delta**

Más adelante, Martorano confirmó el avance del operativo ante el contexto nacional e internacional de la nueva cepa y dijo: “Estamos esperando la cepa Delta, hubo casos positivos en Santa Fe que fueron aislados y bloqueados", determinó. Y reconoció que están “muy preocupados por la Delta, porque no sabemos cómo se va a comportar acá. Por suerte pudimos aislar a los casos de quienes vinieron del exterior”.

"Es probable que el virus quede circulando y sea necesario un refuerzo, una tercer vacuna", anticipó sobre la posibilidad que se analiza en el mundo.