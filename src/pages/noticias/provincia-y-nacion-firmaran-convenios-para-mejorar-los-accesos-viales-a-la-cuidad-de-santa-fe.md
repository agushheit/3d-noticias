---
category: Estado Real
date: 2021-10-16T06:15:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/katopodis.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Provincia y Nación firmarán convenios para mejorar los accesos viales a la
  cuidad de Santa Fe
title: Provincia y Nación firmarán convenios para mejorar los accesos viales a la
  cuidad de Santa Fe
entradilla: La actividad, que se realizará este sábado a las 10 horas, estará encabezada
  por el gobernador Omar Perotti y el ministro de Obras Públicas, Gabriel Katopodis.

---
El gobernador Omar Perotti, y el ministro de Obras Públicas de la Nación, Gabriel Katopodis, firmarán convenios para mejorar los accesos viales a la ciudad de Santa Fe.

El presupuesto oficial es de $ 10.000.000.000, que será financiado por el Ministerio de Obras Públicas de la Nación, a través de la Dirección Nacional de Vialidad.

Los objetivos son mejorar la seguridad de las personas mediante iluminación y acondicionamiento de banquinas para propiciar mayor visibilidad; y mejorar la seguridad vial mediante adecuación de las instalaciones: protección lateral, central y acondicionamiento de calzada. La longitud de la obra es de 55 kilómetros, que se realizarán en dos etapas:

>> ETAPA I:  
\- Ruta Nacional (RN) N.º A007: tramo calle Lisandro de la Torre – Intercambiador Acceso Santo Tomé.  
\-RN N.º 11: tramo Intercambiador Acceso Santo Tomé – Ruta Provincial (RP) N.º 70.  
\-Intercambiador RP N.º 1 sobre RN N.º 168.

>> ETAPA II:  
\-RN N.º 168 – 25 km: tramo Acceso Túnel Subfluvial – Puente Gobernador Oroño.  
\-RN N.º A007 – 2,7 km: tramo Puente Gobernador Oroño – calle Lisandro de la Torre.  
\-RN N.º 11 – 7,6 km: tramo RP N.º 70 – Comuna de Candioti.

**DETALLES DE LA OBRA**  
La Red de Accesos de Rutas Nacionales a la ciudad de Santa Fe está comprendida por las Rutas Nacionales Nº 11, A007 y 168, que se vincula con la Ruta Provincial AP01 autopista Rosario Santa Fe, Ruta Provincial Nº 1, Ruta Provincial Nº 70 y el Acceso al Tunel Subfluvial.

Estos accesos tienen características de autopistas urbanas, vinculando un tránsito pasante Internacional del Mercosur con una dinámica propia de tránsito de una Región Metropolitana de la Capital Provincial.  
Esta red Vial conecta las capitales de dos Provincias Santa Fe y Entre Ríos y se vincula con el Corredor Bioceánico y el Puerto de Santa Fe.

La presente Obra trata completar tramos que no cuentan con obras de iluminación en la Ruta Nacional Nº 11 y poner en valor y adecuar las obras de iluminación existentes a una nueva tecnología utilizando artefactos lumínicos LED, además de la repavimentación del pavimento flexible existente y la reparación de losas deñadas en el pavimento rígido existente.

Además, reubicar instalaciones y/o construir nuevas a los efectos de adecuarse a las normas de Seguridad Vial vigentes.

Lugar de realización de la actividad: [https://www.google.com/maps?q=-31.606969833374023,-60.724220275878906&z=17&hl=es](https://www.google.com/maps?q=-31.606969833374023,-60.724220275878906&z=17&hl=es "https://www.google.com/maps?q=-31.606969833374023,-60.724220275878906&z=17&hl=es")