---
category: Agenda Ciudadana
date: 2021-01-07T09:30:30Z
thumbnail: https://assets.3dnoticias.com.ar/070121-Rossi-vacuna.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Rossi supervisó el inicio de la campaña de vacunación para personal de sanidad
  militar
title: Rossi supervisó el inicio de la campaña de vacunación para personal de sanidad
  militar
entradilla: El inicio de la vacunación se realizó en el hospital naval de la ciudad
  de Buenos Aires, y en esta primera etapa se aplica la vacuna al personal afectado
  a terapia intensiva.

---
El ministro de Defensa, Agustín Rossi, supervisó el inicio oficial del operativo de vacunación contra el coronavirus que en esta primera etapa alcanza al personal de intensivistas de la sanidad militar y que se llevó a cabo en el Hospital Pedro Mallo, dependiente de la Armada Argentina

«Estuvimos acompañando el inicio de la campaña de vacunación que se realizó en el hospital naval de la ciudad de Buenos Aires, y que en esta primera etapa se está aplicando la vacuna al personal afectado a terapia intensiva», señaló Rossi, quien estuvo acompañado por el jefe de la Armada, vicealmirante Julio Guardia, y el director del hospital, capitán de Navío Alberto Hugo Croci.

Según un comunicado difundido por la cartera de Defensa, Rossi afirmó que «la expectativa» del ministerio es «seguir adelante con este plan de vacunación para encontrarle un punto final a la pandemia».

En ese sentido, el funcionario adelantó que la campaña continuará durante toda la semana en el hospital naval y la semana que viene se sumará el Hospital Militar Central y el Hospital Aeronáutico.

La aplicación de la vacuna al personal de sanidad militar en esta primera etapa se inscribe en el plan nacional de vacunación contra la Covid -19 fijada por el Ministerio de Salud, que le otorga prioridad de inoculación a quienes trabajan en esa área en establecimientos públicos y privados, de manera escalonada según el riesgo de su actividad.

Desde Defensa, se indicó que en los próximos días se realizará el mismo operativo en el Hospital Militar Central, perteneciente al Ejército Argentino, y en el Hospital Aeronáutico Central de la Fuerza Aérea Argentina, ambos ubicados en Ciudad Autónoma de Buenos Aires.

Además, se continúa vacunando al personal de sanidad militar de todo el país, de acuerdo a los protocolos y criterios que establece cada provincia o distrito, indicaron los voceros de la cartera.