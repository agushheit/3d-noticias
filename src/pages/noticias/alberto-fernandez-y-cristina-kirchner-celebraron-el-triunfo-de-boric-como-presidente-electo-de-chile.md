---
category: Agenda Ciudadana
date: 2021-12-20T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/BORIC.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Alberto Fernández y Cristina Kirchner celebraron el triunfo de Boric como
  presidente electo de Chile
title: Alberto Fernández y Cristina Kirchner celebraron el triunfo de Boric como presidente
  electo de Chile
entradilla: 'Las dos máximas autoridades del país felicitaron al candidato del frente
  izquierdista Apruebo Dignidad.

'

---
El presidente Alberto Fernández y la vicepresidenta Cristina Kirchner celebraron hoy y felicitaron al candidato del frente Apruebo Dignidad Gabriel Boric Font tras la confirmación de su triunfo como presidente electo de Chile.

"Quiero felicitar a @gabrielboric por haber sido elegido presidente del querido pueblo de Chile. Debemos asumir el compromiso de fortalecer los lazos de hermandad que unen a nuestros países y de trabajar unidos a la región para poner fin a la desigualdad en America Latina", escribió en Twitter el jefe de Estado argentino cuando la tendencia a favor del candidato izquierdista en el balotaje ya era irremontable.

Por su parte, Cristina Kirchner se sumó a los festejos y recordó una frase que ella misma pronunció el 10 de diciembre pasado en Plaza de Mayo.

"El pueblo siempre vuelve y encuentra los caminos para hacerlo. Puede ser un partido, puede ser un dirigente hoy y otro mañana pero el pueblo siempre vuelve", expresó la vicepresidenta citándose a sí misma.

"Felicitaciones Presidente Gabriel Boric a usted y al pueblo de Chile", remató.

A diferencia de su antecesora en la Casa Rosada, el ex presidente Mauricio Macri había llamado a los chilenos a votar en el balotaje por el derechista Frente Social Cristiano José Antonio Kast, quien había sido el candidato más votado en la primera vuelta electoral.

Inesperadamente, ese resultado le generó al Gobierno argentino un conflicto diplomático con el vecino país a raíz de las polémicas declaraciones del embajador argentino en Chile, Rafael Bielsa, quien había definido a Kast como representante de una ultra derecha que "ha exhibido su anti argentinismo" y que tuvo "todo tipo de expresiones xenófobas".

Esas expresiones fueron rechazadas por el Gobierno de Sebastián Piñera, que a través de su Cancillería emitió un comunicado en el cual consideró que el diplomático argentino usó "términos inadecuados" que "representan una intromisión inaceptable en los asuntos internos de Chile".

En declaraciones radiales, Bielsa había señalado que "si uno toma como elemento de juicio las manifestaciones públicas, Kast ha exhibido su anti-argentinismo como una etiqueta más de las frases que pronuncia".

"Desde decirnos que hemos robado históricamente territorios, que tenemos que dejar de robar territorios a Chile, hasta todo tipo de expresiones xenófobas contra los argentinos, que yo las tengo archivadas, registradas, leídas y estudiadas", había agregado.

"La de Kast es una derecha rupturista, pinochetista, que no teme decir su nombre", había afirmado Bielsa en otra entrevista, al tiempo que sostuvo que al ahora ex candidato presidencial de Chile "se lo puede comparar con (Jair) Bolsonaro y (Donald) Trump".

Con la victoria de Boric por más de 11 puntos de diferencia, el episodio de tensión diplomática con Chile queda definitivamente atrás: todo tipo de referentes oficialistas y personalidades vinculadas al kirchnerismo y al progresismo saludaron el triunfo en las urnas del candidato de izquierda.

El premio Nóbel de la Paz Adolfo Pérez Esquivel fue uno de ellos: "Felicitaciones Chile Bandera de Chile!! La Democracia sólo mejora con más Democracia. Felicitaciones Presidente @gabrielboric por este contundente resultado, comienza otro capítulo de igualdad, ampliación de derechos e integración en Nuestra América".

El diputado nacional Leopoldo Moreau afirmó que "el triunfo de Gabriel Boric en Chile trasciende las fronteras de su país y despierta la esperanza de reconstruir una patria grande latinoamericana".

"Seguramente terminara con los resabios del Pinochetismo que todavía sobreviven en esa sociedad", acotó.

Su compañero de bancada, el también kirchnerista Itai Hagman felicitó a Boric y dijo que se siente "muy emocionado porque una nueva generación se abre paso en América Latina".

"Chile votó por respaldar el proceso constituyente y terminar con el pasado neoliberal y pinochetista. ¡Ahora se abren grandes desafíos para construir un Chile más justo!", tuiteó el diputado.