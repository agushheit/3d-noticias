---
category: Agenda Ciudadana
date: 2021-03-08T07:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/gas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Estiman que en invierno será necesario importar el 26% de la demanda de gas
title: Estiman que en invierno será necesario importar el 26% de la demanda de gas
entradilla: Así se desprende del informe técnico que el Gobierno nacional desarrolló
  como paso previo obligatorio a la realización de la audiencia pública, del próximo
  lunes 15 de marzo.

---
La Secretaría de Energía estimó que, para el período invernal de mayo a septiembre próximo, la demanda de gas alcanzará los 22.466 millones de metros cúbicos (MMm3), de los cuales será necesario importar el 26% proveniente de la producción de Bolivia y de la provisión de Gas Natural Licuado (GNL).

Así se desprende del informe técnico que el Gobierno nacional desarrolló como paso previo obligatorio a la realización de la audiencia pública, del próximo lunes 15 de marzo, para determinar el costo del gas natural a boca de pozo, y el porcentaje de cobertura que asumirá el Estado nacional para aliviar el traslado a los usuarios.  
Esta instancia de discusión pública se completará el martes 16 con el análisis de los costos de las dos transportistas y las nueve distribuidoras de gas natural que cubren todo el país, cuya convocatoria está a cargo del Ente Nacional Regulador del Gas (Enargas).

Las dos audiencias abarcan tres de los cuatro grandes componentes que integran la factura de gas que recibe el consumidor final y que se compone de alrededor de un 36% del costo de producción, un 13% del transporte y un 27% de la distribución, lo que se completa con un 24% de impuestos

Para la determinación de costos, tarifas y subsidios que requerirá el segmento de producción de gas, la Secretaría de Energía estimó que la demanda total de gas natural abastecida alcanzará en 2021 los 46.579 MMm3 llegando a los 24.114 MMm3 en el período estival y a los 22.466 MMm3 en el invernal, en este último caso de mayo a septiembre.