---
category: La Ciudad
date: 2021-01-06T11:16:02Z
thumbnail: https://assets.3dnoticias.com.ar/060121-jaton-martorano.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Martorano y Jatón se reúnen para analizar la situación sanitaria de la ciudad
  por Covid
title: Martorano y Jatón se reúnen para analizar la situación sanitaria de la ciudad
  por Covid
entradilla: La ministra de Salud de la provincia y el intendente de Santa Fe tendrán
  un encuentro este miércoles, a las 12 horas, en el Ministerio de Salud.

---
La ministra de Salud, Sonia Martorano, y el intendente de la ciudad de Santa Fe, Emilio Jatón, se reunirán este miércoles para analizar la situación sanitaria de la ciudad de Santa Fe en el marco de la pandemia por Covid-19. 

El encuentro está programado para las 12 horas, en la sede del Ministerio de Salud de la provincia, en Juan de Garay 2880 de la capital provincial. En la ciudad se vienen dando registros medianamente altos de contagios de coronavirus. Pero **este martes la cifra se elevó a 277 casos en la capital provincial**, un número que preocupa muchísimo a las autoridades sanitarias.

Si bien el encuentro estaba previsto con anterioridad, este martes Martorano realizó declaraciones públicas en Rosario donde apuntó a la ciudad de Santa Fe como una de las localidades que más le preocupa por el sostenimiento en el tiempo de una alta tasa de contagios de coronavirus. 

Además, la funcionaria dijo a la prensa que el problema en la capital provincial, además del Covid, es la accidentología y las riñas callejeras que generan una alta ocupación de camas críticas que se pueden necesitar para casos Covid.

Por ese motivo, en la reunión que mantendrán Martorano y Jatón **se espera que haya una agenda amplia de temas que abarque más allá de la situación sanitaria**. 

La ministra adelantó que una posibilidad es que se restrinja la circulación nocturna en Santa Fe para intentar reducir la cantidad de accidentes de tránsito. Mientras que otro de los puntos que puede formar parte del encuentro son los desbordes que se vivieron en los últimos días en las playas santafesinas. En ambos casos, **tanto para la restricción a la circulación vehicular nocturna como para evitar las aglomeraciones y peleas en las playas, es probable que se requiera del apoyo de fuerzas de seguridad**.

De todas formas, para planificar los controles a las situaciones que se viven en los soláriums de la ciudad se realizará otro encuentro –paralelo al de Jatón y Martorano– en la sede del Ministerio de Trabajo de la provincia. Allí participarán, este miércoles a las 12 horas, referentes del Sindicato de Guardavidas, de la cartera de Trabajo, de Seguridad de la provincia y del municipio.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700"> El anticipo de Martorano</span>**

La ministra se refirió este martes en Rosario a la decisión de Provincia de volver a Fase 1 a la ciudad de Tostado a partir del aumento de casos en esa localidad de 9 de Julio. 

«Lo que viene ocurriendo en Tostado es lo que viene ocurriendo en la provincia y en el país. Empezó (la ola de contagios) en el sur de la provincia con un epicentro clarísimo en la ciudad de Rosario, Venado Tuerto, Firmat. Todos esos casos fueron bajando y comenzó a subir hacia el centro, con Santa Fe, con Rafaela. Todavía estamos con casos altos, pero lógicamente ahora empieza a llegar al norte», dijo la funcionaria.

Luego, al ser consultada sobre qué otras localidades de la provincia podrían tener que retroceder de Fase, como en Tostado, Martorano respondió: «en Santa Fe tenemos momentos complicados que se están sosteniendo. Hay dos cosas, no solo es el Covid. La accidentología también ocupa muchas camas y por eso se habla mucho de restringir la circulación nocturna porque es cuando generalmente se producen más accidentes. Hoy muchas camas están ocupadas por riñas callejeras y accidentes y eso hace que cuando tengo un caso de Covid, no tengo camas».

En esas declaraciones, la ministra adelantó parte de la agenda que tendrá la reunión con el intendente santafesino, Emilio Jatón.