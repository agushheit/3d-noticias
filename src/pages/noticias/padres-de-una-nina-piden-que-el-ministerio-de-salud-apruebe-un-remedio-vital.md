---
category: Agenda Ciudadana
date: 2021-04-12T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/casorocatti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Padres de una niña piden que el Ministerio de Salud apruebe un remedio vital
title: Padres de una niña piden que el Ministerio de Salud apruebe un remedio vital
entradilla: Se trata de una pequeña de 2 años de edad que sufre una enfermedad degenerativa
  que necesita ser tratada con urgencia.

---
Fabián y Gisela, padres de la nena, iniciaron una campaña en redes sociales pidiendo la difusión del caso. Rápidamente, el rostro de Valentina empezó a aparecer en publicaciones de WhatsApp, Instagram, Facebook y Twitter de los usuarios rosarinos y de la región, apoyando a la familia en su búsqueda.

“Lo que queremos es la difusión máxima que nos puedan dar, sabemos que es imposible conseguir ese dinero en 15 días, solo queremos la difusión”, aclaró Fabián y comentó: “Al saber que está la cura, está aprobada en Argentina, nos vemos tan cerca que nos tiramos con esta campaña a ver si lo podíamos conseguir, sabíamos que algo teníamos que hacer”.

La enfermedad de Valentina, además de ser degenerativa, es genética. “Somos los dos portadores y la verdad que no la conocíamos. Se la diagnosticaron a los nueve o diez meses de vida, cuando empezamos a notar que ella tenía debilidad en las piernitas”, dijo el padre de la niña.

Valentina Rocatti sufre una atrofia muscular espinal degenerativa que deteriora los músculos del cuerpo que con el paso del tiempo afecta la capacidad para caminar, respirar y tragar.

**La patología tiene cura con medicación que debe suministrarse antes de los 2 años de edad, Valentina cumple los dos años el próximo 24 de abril.**

“La prepaga Galeno y le Ministerio de Salud de la Nación debe aprobar el medicamento de forma urgente “.