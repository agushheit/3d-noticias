---
category: Agenda Ciudadana
date: 2020-12-01T11:17:53Z
thumbnail: https://assets.3dnoticias.com.ar/DESPIDOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: El Gobierno extendió hasta el 25 de enero la doble indemnización por despidos
  injustificados
title: El Gobierno extendió hasta el 25 de enero la doble indemnización por despidos
  injustificados
entradilla: Lo hizo a través del Decreto 961/2020, publicado este lunes en el Boletín
  Oficial. Previamente, ya había prorrogado la prohibición de despidos y suspensiones.

---
Luego de haber prorrogado la prohibición de suspensiones y despidos sin justa causa o por falta o disminución de trabajo y razones de fuerza mayor, el Gobierno también extendió “hasta el 25 de enero de 2021″ la doble indemnización. Lo hizo a través Decreto 961/2020, publicado este lunes en el Boletín Oficial.

De esta manera, continuará vigente la medida que fue implementada por el presidente Alberto Fernández poco después de asumir al frente de la Casa Rosada y que, tras sucesivas prórrogas, vencía el próximo 17 de diciembre.

A diferencia de las veces anteriores, en esta ocasión la ampliación no fue por 180 días, sino hasta el 25 de enero del año que viene. Esta decisión ya había sido anunciada por el ministro de Trabajo, Claudio Moroni, durante una reunión que mantuvo a mediados de este mes con dirigentes de la CGT, en medio de tensiones entre las autoridades nacionales y la central obrera.

En ese encuentro, el funcionario también había confirmado que se iba a mantener la prohibición de los despidos y las suspensiones, que junto a la doble indemnización fueron los principales reclamos que le hicieron los dirigentes gremiales.

El Decreto de este lunes, firmado por el propio Alberto Fernández, el jefe de Gabinete, Santiago Cafiero, y todos los ministros, estableció la extensión de esta última medida que se enmarca dentro de la “emergencia pública en materia ocupacional”, declarada el 13 de diciembre de 2019.

Por lo tanto, durante este periodo “en caso de despido sin justa causa, la trabajadora afectada o el trabajador afectado tendrá derecho a percibir el doble de la indemnización” que le correspondería normalmente.

![](https://assets.3dnoticias.com.ar/MORONI.jpg)En el texto, al igual que las veces anteriores, el Gobierno aclaró que este beneficio no aplica para aquellas personas que hayan sido contratadas “con posterioridad a la entrada en vigencia” de la norma original ni para el Sector Público Nacional, “con independencia del régimen jurídico al que se encuentre sujeto el personal de los organismos, ni a sociedades, empresas o entidades que lo integran”.

Entre los considerandos, el Poder Ejecutivo señaló que “la crisis económica en que se encontraba el país se vio agravada por el brote del nuevo coronavirus”, por lo que la situación “exige prorrogar la oportuna adopción de medidas” tendientes a garantizar “que esta emergencia no les hará perder sus puestos de trabajo” a las personas.

Entre otros puntos, las autoridades remarcaron que esta decisión se toma " en el marco de las obligaciones asumidas por la República Argentina en el Pacto Internacional de Derechos Económicos, Sociales y Culturales, y con el objetivo de preservar la paz social”.

“Resulta indispensable continuar garantizando por imperio normativo la conservación de los puestos de trabajo por un plazo razonable, en aras de preservar la paz social y que ello solo será posible si se transita la emergencia con un Diálogo Social en todos los niveles y no con medidas unilaterales de distracto laboral, que no serán más que una forma de agravar los problemas causados por la pandemia”, sostuvo el Gobierno.

Por último, se destacó que, “tal como sucedió en el momento del dictado de la medida original y de su ampliación”, esta normativa busca “atender la situación de vulnerabilidad de los sectores más desprotegidos y, al mismo tiempo, evitar que se acreciente el nivel de desprotección de los trabajadores y de las trabajadoras formales”.

En su momento, Moroni explicó por qué habían prorrogado la doble indemnización cuando ya estaban prohibidos los despidos: “Quedaban una serie de situaciones como el despido indirecto, cuando el trabajador se da por despedido por incumplimientos graves del empleador. En ese caso si no prorrogábamos la doble indemnización iba a tener una indemnización simple”, indicó.

“Se da el caso también de empresas que no pueden reincorporar porque cierran. Entonces para completar la protección teníamos que mantener la doble indemnización”. “Lo que estamos protegiendo son esas situaciones que no quedaban alcanzadas por la prohibición de despidos”, agregó.

[Ver Decreto](https://assets.3dnoticias.com.ar/DECRETO-DESPIDOS.pdf)