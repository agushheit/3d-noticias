---
category: Estado Real
date: 2021-05-04T08:17:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/creditos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La Provincia entregó créditos a Pymes lideradas por mujeres y diversidad
  de Santa Fe y la Región
title: La Provincia entregó créditos a Pymes lideradas por mujeres y diversidad de
  Santa Fe y la Región
entradilla: Los mismos son canalizados por la Agencia para el Desarrollo de Santa
  Fe y la Región, a partir de un fondo rotatorio generado con aportes del Gobierno
  Provincial.

---
El Gobierno de la Provincia de Santa Fe, a través del Ministerio de Producción, Ciencia y Tecnología, entregó créditos a pymes lideradas por mujeres y a emprendimientos afectados por la emergencia sanitaria, préstamos que son canalizados por medio de la Agencia para el Desarrollo de Santa y la Región (ADER) y que provienen de un fondo rotatorio generado a partir de recursos aportados por el Estado provincial.

El financiamiento está dirigido a empresas de actividades comerciales, industriales y de servicios y, además, se instrumentó una línea específica para la inclusión financiera con perspectiva de género. En particular se otorgaron 22 créditos “de una primera etapa construida a partir de fondos que llegaron a la Agencia desde el Gobierno Provincial a través del Ministerio de Producción, Ciencia y Tecnología y con el acompañamiento en todo el proceso de la Secretaria de Estado de Igualdad y Género”, informó en la apertura de la actividad el presidente de ADER, Nicolás Cabo; al tiempo que agradeció el acompañamiento de la provincia y del senador Nacional Roberto Mirabella y detalló: “Los beneficiarios que reciben hoy el financiamiento son en su mayoría emprendimientos liderados por mujeres”.

Por su parte, el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina agradeció al Molino Fábrica Cultural por disponer sus instalaciones e indicó: “En un año el Gobierno Provincial transfirió más de 162 millones de pesos en recursos a estas entidades del territorio. A este fondo, que tiene alrededor del 95% de tasa de recupero en las 19 agencias y asociaciones para el desarrollo y que ya permitió financiar a más de 1300 pymes, estamos analizando ampliarlo. Hablamos de inclusión financiera y que muchos emprendimientos se han beneficiado, un sistema que le pone cara a las economías locales cuando el sistema bancario no responde a su demanda. Estas entidades están compuestas por actores públicos y privados lo que le da legitimidad en el territorio. Por eso aquí también hay que destacar el trabajo de entidades como la Unión Industrial, Apyme, el Centro Comercial y a la municipalidad de Santa Fe, que han articulado fuertemente con la provincia en el ámbito de la ADER”.

La secretaria de Estado de Igualdad y Género, Celia Arena, quien participó de la actividad destacó: “Este es un logro muy importante que surge de un trabajo conjunto. El año pasado realizamos capacitaciones con distintas instituciones, de esa actividad, como iniciativa de la ADER, salió la idea de instrumentar este financiamiento. Una de las grandes problemáticas de las mujeres para lograr su autonomía económica es la dificultad de acceso al crédito, lo que llamamos inclusión financiera. Hoy vemos como mayoritariamente están accediendo a los préstamos mujeres que desarrollan emprendimientos de diversos tipos, gastronómicos, artesanías y producción audiovisual, entre otros”.

“La decisión, tanto del Ministro Costamagna como del secretario de Desarrollo Territorial, Fabricio Medina, es que continuemos trabajando en conjunto para que la herramienta siga creciendo y desarrollándose, ya sea en ADER como en las demás agencias y asociaciones para el desarrollo de la provincia”, agregó.

Resaltando las medidas adoptadas por el Gobierno Provincial, el senador Nacional, Roberto Mirabella manifestó: “En esta coyuntura la mayor preocupación es sostener la actividad económica y el empleo. Se prorrogaron impuestos, se congelaron tarifas, se recibieron, desde Nación, la asistencia al trabajo y a la producción para más de 27 mil empresas beneficiando a 240 mil trabajadores de toda la provincia. En particular, esta herramienta, que también va en esta línea, democratiza el acceso al crédito con perspectiva de género. La buena noticia es que, además de los 22 créditos que se entregan hoy y los que restan otorgarse, la provincia ampliará este cupo poniendo a disposición un nuevo fondo”.

“Esto se suma a otro hito financiero para el sector productivo, como es el convenio que el gobernador Omar Perotti firmó con el Banco Nación para poner a disposición del sector productivo santafesino más de 26 mil millones de pesos. El acceso al crédito es la palanca fundamental para apuntalar la actividad y con ello sostener el empleo”, concluyó.

Finalmente, la concejala Jorgelina Mudallel precisó: “Hoy es un buen día porque más de 5 millones de pesos llegan a emprendedores de la ciudad y la región. Son ayudas que en contexto de pandemia permiten contar con herramientas generadas desde el Estado para que el aparato productivo de la provincia y la ciudad sigan en vigencia”.

**CONDICIONES PARA AMBAS LÍNEAS**

Los créditos son por un monto total de hasta 350 mil pesos con una tasa de interés del 13,5 % anual, un plazo total de 24 meses, un período de gracia de seis meses (para capital e intereses) y a 18 cuotas.