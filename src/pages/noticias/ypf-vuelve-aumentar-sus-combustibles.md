---
category: Agenda Ciudadana
date: 2021-05-14T08:21:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/nafta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: YPF vuelve aumentar sus combustibles
title: YPF vuelve aumentar sus combustibles
entradilla: Si bien la petrolera no brindó detalles acerca de cuál será el monto del
  aumento, trascendió que se ubicaría en torno al 5%.

---
El presidente de YPF, Pablo González, ratificó este jueves que los precios de sus combustibles aumentarán en los próximos días, pero garantizó que, en todo el año, el ajuste trepará al 28,1%, "por debajo de la inflación establecida en el Presupuesto Nacional". Si bien no brindó detalles acerca de cuál será el monto del aumento, trascendió que se ubicaría en torno al 5%.

Cabe recordar que la última suba fue el 17 de abril y casi un mes después la petrolera vuelve a subir el precio de sus combustibles.

"Estamos tratando de equilibrar el precio del surtidor que paga la gente, que tiene un componente bastante complejo a determinar, con el plan de inversiones que la Argentina necesita para poder poner en valor la producción de gas y petróleo", justificó el funcionario.

González explicó que el precio del barril de crudo "está cotizando a unos US$69 y si hubiésemos trasladado ese precio internacional directo al surtidor, hoy la gente estaría pagando $130 -en promedio- el litro de nafta".

"Por eso, hubo que equilibrar (la situación) y darle previsibilidad al sector, acompañar la política energética del Gobierno nacional y pedirle a la gente un esfuerzo, con la garantía de que son los últimos aumentos del año y que en función de esto YPF va a poder invertir lo que necesita e impedir que se importe combustible", enfatizó el titular de la petrolera, en declaraciones radiales.

Además, garantizó que este año "la nafta va a aumentar menos que la inflación establecida en el Presupuesto de la Nación", y estimó que ese ajuste será del 28,1%. Según dijo, "ese es el compromiso que hemos asumido y el plan que venimos preparando".