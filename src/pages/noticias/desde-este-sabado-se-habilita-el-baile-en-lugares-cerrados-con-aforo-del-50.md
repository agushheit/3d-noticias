---
category: La Ciudad
date: 2021-10-07T06:15:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/bailes.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Desde este sábado se habilita el baile en lugares cerrados, con aforo del
  50%
title: Desde este sábado se habilita el baile en lugares cerrados, con aforo del 50%
entradilla: Durante la tarde de este miércoles la provincia firmó el decreto que habilita
  a locales bailables a funcionar con baile, respetando el aforo del 50% y hasta las
  3 de la mañana.

---
A partir de las 00 de este sábado los salones de fiestas y los boliches estarán habilitados para funcionar con baile en espacios cerrados, con un aforo del 50 por ciento y hasta las 3 de la mañana. La resolución, que amplía el decreto firmado el viernes por el gobernador Omar Perotti, se dio a conocer este miércoles luego de los reclamos de empresarios del sector.

Si bien los boliches y salones de fiestas ya habían sido habilitados en el decreto firmado el viernes pasado, hasta este miércoles por la tarde solo se podía bailar en espacios abiertos, algo que muy pocos salones poseen, con lo cual se dificultaba la apertura. La llegada del fin de semana largo hizo que los empresarios presionaran para que la provincia aclarara este punto.

En tanto, continúa vigente que los salones que sí posean espacios abiertos podrán abrir para una capacidad máxima del 70 por ciento y entre las 20 y las 3 de la madrugada los viernes, sábados y vísperas de feriados. Los que ingresen deberán contar con al menos una dosis de la vacuna contra el coronavirus aplicada al menos 14 días antes.

"La gente quiere baile y no nos contrata si no lo hay", era lo que sostenían empresarios del sector bailable sobre las restricciones que regían hasta el momento, sosteniendo que la habilitación para bailar solo en espacios abiertos era imposible de llevar a cabo, ya que "prácticamente ninguno" cuenta con patio.