---
category: Agenda Ciudadana
date: 2021-05-18T08:24:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/leche.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa FADA*
resumen: Pan, leche, carne ¿Qué pasa con los precios?
title: Pan, leche, carne ¿Qué pasa con los precios?
entradilla: El precio del pan se multiplica por siete, desde que sale el trigo del
  campo hasta nuestra mesa. En el pan $29 son impuestos, en la leche $18 y en la carne
  la carga es de $149. ¿Una mochila en nuestras compras?

---
Publican un nuevo informe sobre **lo que pagamos cuando compramos pan, leche y carne.** Es el estudio semestral de “**Composición de Precios” de FADA** (Fundación Agropecuaria para el Desarrollo de Argentina).

Los últimos datos arrojan que, **el precio del pan se multiplica por siete** desde que sale como trigo del campo, hasta el pan de nuestra mesa. **De $135, precio promedio de un kilo de pan, $29 pesos son impuestos. La carga impositiva es mayor que lo que sale producir el trigo y hacer la harina para ese kilo de pan.**

**En el caso de la leche el precio se triplica** desde que sale del tambo, pasa por la industria, el comercio y llega a nuestro desayuno. **De $67 de precio promedio de un litro de leche, $18 corresponde al peso impositivo.**

**La carne** es el producto más afectado por la **“mochila”** de los impuestos, **de $531 de precio promedio por kilo, $149 son impuestos.**

“Algo llamativo de este estudio es que demuestra la alta **carga tributaria** que tienen **alimentos que son básicos,** como el pan, la carne o la leche. En promedio, **1 de cada 4 pesos que pagamos por estos productos, son impuestos que se acumulan a lo largo de la cadena. En el caso del pan, pesan más los impuestos, que la propia harina**”, destaca **David Miazzo**, economista jefe de FADA.

“Este trabajo ayuda a mostrar **qué hay detrás de los precios**. En momentos **donde la inflación se acelera** se tiende a discutir los precios, la cadena y los costos de los distintos productos, pero en realidad **el problema no es de un precio en particular.** Hoy es el problema del precio de la carne, mañana el combustible y pasado los alquileres. **El problema de fondo es el valor de la moneda,** cada vez se necesitan más pesos para comprar el mismo kilo de carne, de tomates, el litro de nafta o una remera”. “No es un tema de precios, **tenemos un problema macroeconómico.** Un profundo déficit fiscal que se financia **emitiendo pesos** que, sumado a **falta de confianza**, **se traducen en pesos que cada vez valen menos: eso es la inflación”,** define **Miazzo.**

Para comprender como se va componiendo el precio, se tiene en cuenta todos los **costos, impuestos y resultado que se obtiene en cada eslabón.** Por ejemplo, en el caso de la leche se considera desde el costo de la tierra del campo, de estructura, alimentación, sanidad animal, personal, etc. En la industria, la materia prima, logística, personal, gastos comerciales, transporte y en el comercio, también se incluyen sus propios costos operativos. En cada eslabón del circuito, los impuestos van cargando un alto porcentaje del precio, detalla Natalia Ariño, economista de FADA.

**Más datos y análisis, producto por producto**

**PAN**

FADA registra que el **consumidor paga un promedio de $135 por kilo de pan francés**. En ese precio, **el trigo representa $17 (13%) el molino $7 (5%), la panadería $82 (60%) y los impuestos $29 (22%).**

Al analizar el precio del **pan según qué lo compone**, se detecta que el 67% son costos, 22% impuestos y 11% ganancias. **De los impuestos, el 75% son nacionales, 22% provinciales y 3% municipales.** Impuesto a las ganancias e IVA concentran el 68% de los impuestos pagados por la cadena

**El precio del pan, paso a paso**: al realizar este informe, y basándonos en un kilo de pan, el productor vendió **el trigo a $19**. El **molino** lo compra e incluye sus propios **costos ($7), impuestos ($0,86)** y el margen neto **($0,02), y le vende la harina a $27 por kilo de pan** a la panadería. En este punto es en donde se produce el **mayor salto de la cadena** al incorporar sus **costos ($66),** los **impuestos** que paga **($14)** y **lo que se obtiene por la actividad ($15),** se llega a un precio del kilo de pan en mostrador de **$122**. **Al sumar $13 del IVA** llega **finalmente al consumidor, en promedio, un kilo de pan a $135.**

“Ese **salto que se da en la panadería** tiene una explicación, afirma Ariño, y es que en este eslabón **se combinan dos procesos: producción y comercialización**. Tiene un uso intensivo de mano de obra, costos de servicios y costos de alquiler. Adicionalmente, **las escalas de producción a lo largo de las cadenas son distintas**, mientras el trigo y harina se realizan a gran escala, **en la panadería es a menor escala, por lo que los costos unitarios son mayores**. A su vez, es la etapa que más costos presenta”.

**LECHE**

La **leche entera en sachet**, arroja un **precio promedio de $67**. De ese monto, **el tambo representa $24 (35%), la industria $22 (33%), el comercio $4 (6%) y los impuestos $18 (26%).** El maíz representa el 18,4% del precio de la leche cruda (tambo) y el 6,6% del precio final del sachet.

Del **precio final** pagado por el consumidor, el **89,2% son costos, el 26% impuestos y -15,2% son pérdidas de la cadena**. De esos impuestos, el 78% son nacionales, mientras que el 18% provinciales y 4% municipales. **El IVA representa el 67% de los impuestos.**

Cabe una aclaración especial respecto al IVA. “Los consumidores pagan el 21% de IVA sobre la leche, **cuando debería estar exenta por ley**. El problema está en que la ley contempla exenta la leche pasteurizada, producto que casi no se produce ni consume. En cambio, la leche que sí se produce y consume actualmente es la ultrapasteurizada, razón por la cual no queda exenta”, aclara **Miazzo**. “En un contexto donde **el 42% de la población es pobre y el 57% de los chicos menores de 15 años también lo son,** debería ser fundamental **mejorar el acceso a alimentos de primera necesidad con políticas que no generen distorsiones en el sistema productivo** y sea coherente con el tratamiento de otros alimentos, tomando como ejemplo, la carne y pan tienen alícuota reducida del 10,5%”, reflexiona el economista.

**El precio de la leche, paso a paso:** el **productor del tambo vendió a $24,2** el litro de leche. La **industria** la adquiere a ese precio y suma $**20,39** de costos **y $2,06** de impuestos por litro. Con un resultado de **$1,94**, el precio del sachet sale de la planta a **$48,59**. El **comercio** la compra y suma **$14,02** de costos de estructura, transporte y costos laborales, paga impuestos por **$3,25** y obtiene una pérdida de **-$10,24**. Finalmente, se suma el IVA que paga el consumidor por **$11,68** y se obtiene el precio final en góndola de **$67,31**. **La leche perdió frente a la inflación y el dólar**.

“**Cuando se analiza el sachet de leche**, se observa **que la cadena se enfrenta a una situación crítica en todos los eslabones:** tambo, industria y comercio, con precios que no alcanzan a cubrir costos (tambo), precios de venta contenidos, ocasionando un desfasaje con los costos (industria y comercio) y generando pérdidas económicas en casi todos los eslabones de la cadena, acentuado aún más en la etapa final”, comenta Ariño”. En el último eslabón, dependerá de la estrategia que persiga el comercio para compensar la pérdida por leche con otros productos lácteos con mejores márgenes.

**CARNE**

Se registra que la **carne tuvo un precio promedio de $531 por kilogramo**, de los cuales la **cría representa $158 (30%), el feedlot $141 (27%), el frigorífico $35 (6%), la carnicería $47 (9%) y los impuestos $149 (28%)**. El maíz representa el 15% del precio del novillo y 11% del kilo de carne al mostrador.

“Cuando se analiza la participación de cada eslabón, es importante destacar que, si bien la cría del ternero representa el 30% del precio, hay que tener en cuenta que es la etapa de la cadena más costosa por el tiempo que se requiere para tener listo un ternero,”, aclaró Ariño.

De los impuestos que tiene la cadena, **el 75% son nacionales** (impuesto a las ganancias, IVA, e impuesto a los créditos y débitos), **20% provinciales** (inmobiliario rural, ingresos brutos) y **5% municipales** (impuesto a la industria y comercio y tasa vial). El impuesto a las ganancias y el IVA, son los de mayor participación, que en conjunto representan el 65% de los impuestos totales.

Si se analiza toda la cadena de carne bovina, **desde la cría hasta el comercio, el 65% son costos de producción, 28% impuestos y 7% el resultado económico**, sobre el precio del kilo de carne. Este informe detectó que **la carnicería tuvo una caída en su margen**. “Esto se puede explicar por el techo que pone el poder de compra de los consumidores al momento de actualizar el precio de venta de la carne frente a los costos”, advierte Miazzo.

Al medir la composición de precios de los tres productos, este informe referencia a primeros meses del 2021, en base a datos de febrero de este año, respondiendo al tiempo que lleva recabar la información de distintas fuentes.

Para concluir, en relación al control de precios, **Miazzo** señala que **“la inflación en Argentina está ligada al déficit público, la emisión monetaria y la falta de confianza.** Por ello, **es imposible combatirla desde los controles de precios, los derechos de exportación o las restricciones para exportar.** Este tipo de medidas **actúan sobre la consecuencia, que es el aumento de los precios, pero no sobre las causas que generan esos aumentos.** Por ejemplo, el maíz representa el 11% del precio final de la carne. Si se decide subir 10 puntos a los derechos de exportación al maíz, se mejoraría el precio de la carne sólo un 1,1%, cuando la inflación mensual está cerca del 4%. Es decir, con una medida como esta se logra ahorrar, por única vez, sólo una semana de inflación. Algo similar sucede con el trigo y el pan, el trigo representa el 13% del precio del pan. Subir los derechos de exportación al trigo podría generar un impacto en el precio del pan de sólo el 1,3%, o sea poco más de una semana de inflación.

**Prensa y Comunicación FADA**

**Lic. Valeria Tosselli – Prensa y Comunicación FADA**

**(358) 5 161 974 – Fijo: (0358) 4 210 341**

[**valetosselli@gmail.com**](mailto:valetosselli@gmail.com) **-** [**vtosselli@fundacionfada.org**](mailto:vtosselli@fundacionfada.org)

**Carla Gargiulo – Prensa y Comunicación FADA**

**(358) 4 123 990 – Fijo: (0358) 4 210 341**

[**gargiulocarlab@gmail.com**](mailto:gargiulocarlab@gmail.com) **-** [**cgargiulo@fundacionfada.org**](mailto:cgargiulo@fundacionfada.org)

**Contactos para entrevistas:**

**Lic. Natalia Ariño – Economista FADA**

**(358) 424 7941**

[**narinio@fundacionfada.org**](mailto:narinio@fundacionfada.org)

**Sede Río Cuarto** **(Cba.):** San Martín 2593, Rio Cuarto, Córdoba.

**Lic. David Miazzo- Economista Jefe de FADA**

**(358) 4 295 485**

[**davidmiazzo@fundacionfada.org**](mailto:davidmiazzo@fundacionfada.org)

**Sede Buenos Aires**

**¿Qué es el estudio de Composición de Precios?**

David Miazzo explica que **este Informe de Composición de precios de FADA** es un seguimiento de tres cadenas: **carne, pan, leche.** Rastrea cómo se conforman los precios a medida que pasan por los distintos eslabones, en el **recorrido que va desde el campo hasta llegar al consumidor.** El estudio tiene por objetivo macro contribuir a la **transparencia** y a reducir la desinformación, para obtener la mejor foto posible de **por qué los alimentos valen lo que valen,** y así poder identificar dónde pueden estar los principales problemas de cada cadena.

El informe se publica con un rezago de dos meses por el tiempo que se requiere para recabar la información de las distintas fuentes, pero a la vez se utilizan herramientas de actualización de dichos datos.

Para poder construir estos datos, FADA cuenta con la colaboración de productores de trigo, ganaderos y tamberos, así como feedlots, frigoríficos, industrias lácteas, molinos harineros, supermercados, carnicerías y panaderías. Además de estas fuentes primarias, se basa en informes y estadísticas publicadas por INDEC, IPCVA, CNV, Revista Márgenes Agropecuarios, Asociación de Frigoríficos e Industriales de la Carne, Cámara Argentina de Feedlot, Precios Claros, Rosgan, Asociación de Supermercados Unidos y Subsecretaría de Lechería de Agroindustria.

**_*Acerca de FADA:_**

**_FADA (Fundación Agropecuaria para el Desarrollo de Argentina), es una institución sin fines de lucro que elabora, difunde y gestiona proyectos de políticas públicas. Sus estudios apuntan a impulsar el desarrollo de nuestro país para mejorar la vida de las personas._**

**_“Somos argentinos apasionados por Argentina”._**

[**www.fundacionfada.org**](http://www.fundacionfada.org/)