---
category: Agenda Ciudadana
date: 2021-08-29T06:15:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/RUSSIANDIRECT.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Este fin de semana se distribuyen 1 millón de dosis de componente 1 y 2 de
  Sputnik V
title: Este fin de semana se distribuyen 1 millón de dosis de componente 1 y 2 de
  Sputnik V
entradilla: Del componente 1 se entregará a los distritos un total de 584.500 dosis,
  mientras que del componente 2 se distribuirán 492.500.

---
El Gobierno distribuirá este fin de semana más de 1 millón de dosis de los componentes 1 y 2 de la vacuna Sputnik V contra el coronavirus, en su variante producida por el laboratorio argentino Richmond como las procedentes de la Federación Rusa, mientras que 1.500.000 inmunizaciones de Sinopharm llegarán a todas las provincias.

Entre este sábado y este domingo se distribuirán 1.077.000 dosis de la vacuna Sputnik V, de los cuales del componente 1 se entregará a los distritos un total de 584.500 dosis, mientras que del componente 2 se distribuirán 492.500, se informó oficialmente.

De acuerdo al esquema de distribución de la vacuna de origen ruso, y según el criterio dispuesto por el Ministerio de Salud de la Nación en base a la cantidad de población de cada distrito, a la provincia de Buenos Aires le corresponderán 225.600 dosis de componente 1 y 190.125 de componente 2.

La Ciudad Autónoma de Buenos Aires, recibirá 38.750 dosis del componente 1 y 32.125 dosis del componente 2; mientras que Catamarca, recibirá 5.400 y 4.500; Chaco, 15.600 y 13.500; Chubut, 8.000 y 6.800; Córdoba, 48.400 y 40.800; Corrientes, 14.400 y 12.000; Entre Ríos, 18.000 y 15.000; Formosa, 7.800 y 6.600; Jujuy, 10.000 y 8.400; La Pampa, 4.800 y 3.800; y La Rioja, 5.125 y 4.200.

A su vez, Mendoza, recibirá 25.800 dosis del componente 1 y 21.600 dosis del componente 2, mientras que Misiones 16.200 y 13.800; Neuquén, 8.750 y 7.200; Río Negro, 9.750 y 8.200; Salta, 18.375 y 15.600; San Juan, 10.125 y 8.400; San Luis, 6.750 y 5.600; Santa Cruz, 4.875 y 4.200; Santa Fe, 45.250 y 38.400; Santiago del Estero, 12.625 y 10.800; Tierra del Fuego, 2.250 y 2.250; y Tucumán, 21.875 y 18.600.

**Vacunas Sinopharm**

En tanto, de la distribución de las vacunas Sinopharm, que también se realizará desde los parámetros establecidos por la cartera sanitaria en función de la población de cada provincia, 580.800 dosis corresponderán a la provincia de Buenos Aires.

La distribución de las vacunas Sinopharm también se realizará desde los parámetros establecidos por la cartera sanitaria.

La distribución de las vacunas Sinopharm también se realizará desde los parámetros establecidos por la cartera sanitaria.

De esas inmunizaciones, recibirá 100.800 la Ciudad de Buenos Aires; 13.600, Catamarca; 40.000, Chaco; 20.800, Chubut; 124.800, Córdoba; 36.800, Corrientes; 45.600, Entre Ríos; 20.000, Formosa; 25.600, Jujuy; 12.000, La Pampa; 12.800, La Rioja; y 65.600, Mendoza.

Asimismo, recibirá 41.600 dosis de vacunas Sinopharm, Misiones; 22.400, Neuquén; 24.800, Río Negro; 47.200, Salta; 25.600, San Juan; 16.800, San Luis; 12.000, Santa Cruz; 116.800, Santa Fe; 32.000, Santiago del Estero; 5.600, Tierra del Fuego; y 56.000, Tucumán.

Según el Monitor Público de Vacunación, hasta esta tarde se distribuyeron 46.987.574 dosis de vacunas en todo el país, al tiempo que se aplicaron 41.625.410.

De ese total, hay 27.750.181 personas inoculadas con la primera dosis, y 13.875.229 cuentan con el esquema completo de inmunización.