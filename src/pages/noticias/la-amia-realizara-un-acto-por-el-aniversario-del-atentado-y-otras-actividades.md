---
category: Agenda Ciudadana
date: 2021-07-12T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La AMIA realizará un acto por el aniversario del atentado y otras actividades
title: La AMIA realizará un acto por el aniversario del atentado y otras actividades
entradilla: Será el 16 de julio y se desarrollara de forma virtual bajo el lema "Conectados
  contra la impunidad". Comenzará a la hora en la que explotó el coche bomba y podrá
  ser seguido desde las redes de la mutual AMIA.

---
La Asociación Mutual Israelita Argentina (AMIA) realizará el próximo viernes 16 de julio el acto central por el 27 aniversario del ataque terrorista perpetrado el 18 de julio de 1994, que provocó la muerte de 85 personas y más de 300 heridos, y la actividad este año se hará nuevamente de forma virtual bajo el lema "Conectados contra la impunidad".  
El acto, que se sumará a una serie de actividades que la mutual lleva adelante en redes sociales desde la semana pasada, se realizará dos días antes de la fecha en que se produjo la explosión para facilitar una mayor participación, al tratarse de un día hábil.

  
**La convocatoria**

El evento, que comenzará a las 9:53 del viernes, hora exacta en que un coche bomba explotó contra la sede de Pasteur 63 de la Ciudad de Buenos Aires, es convocado por la AMIA, la Delegación de Asociaciones Israelitas Argentinas (DAIA) y Familiares de las Víctimas.  
Las organizaciones invitaron a participar del evento a través de la consigna "Desde donde estés, decí presente. Sigamos haciendo memoria y exigiendo justicia", que podrá ser seguido desde las redes de la mutual AMIA on line en el canal de YouTube y en la página de Facebook.  
El titular de AMIA, Ariel Eichbaum, quien será orador en el acto en el que se nombrará a las víctimas mortales, convocó "a que nos acompañen el viernes 16, para renovar juntos el reclamo de justicia y castigo a los culpables" y advirtió que "como sociedad, no podemos aceptar convivir con la impunidad".  
"A 27 años del atentado, genera estupor el hecho de que el doloroso saldo de destrucción y muerte causado por el accionar terrorista no encontró aún sanción alguna", expresó Eichbaum y manifestó que "la falta de justicia es una asignatura pendiente de nuestra democracia que acrecienta el dolor y no permite aliviar una herida que se profundiza a medida que transcurre el tiempo".  
La mutual informó que quienes quieran acompañar el reclamo de memoria y justicia en el acto podrán participar de la recreación digital del momento en que se mencionan los nombres de las 85 víctimas, a través del uso de un filtro fotográfico disponible en el sitio www.amiamemoria.org/serparte, donde una guía explica cómo tomarse una foto y sumarse de modo virtual al homenaje.

  
**Otras actividades**

AMIA, en tanto, inició la semana pasada una serie de actividades conmemorativas, entre ellas la que denominó "Plaquetas", con la que por primera vez realiza un homenaje institucional a los 25 empleados de la AMIA que fallecieron en el atentado: para ello se colocaron placas con sus nombres en los sectores donde trabajaban.  
Así, con la presencia de familiares, se colocaron placas en el área de Socios y Desarrollo, donde se desempeñaban Marisa Raquel Said y Rosita Perelmutter; en el de Cultura, de la que formaba parte Abraham Jaime Plaksin; y en Programas Sociales, donde cumplían sus tareas Silvana Sandra Alguea de Rodríguez, Yanina Muriel Averbuch, Noemí Graciela Reisfeld y Marta Andrea Treibman.  
Las otras placas fueron instaladas en el área de Sepelios Comunitarios, a la que pertenecían Norberto Ariel Dubín, Fabián Marcelo Furman, José Enrique "Kuky" Ginsberg, Agustín Diego Lew, Ángel Claudio Ubfal y Rita Noemí Worona; y en Seguridad, donde prestaban sus servicios Naum Band, Carlos Isaac Hilu, Gregorio "Heshele" Melman, Ricardo Hugo Said y Mauricio Schiber.  
Otras se colocaron en el Centro de Documentación e Información sobre Judaísmo Argentino, en el que trabajaba Mirta Strier; y en la Secretaría General de AMIA, donde Naón Bernardo "Buby" Mirochnik cumplía sus funciones como mozo de la institución.  
Esos homenajes continuarán el próximo mes en el Servicio de Empleo, donde trabajaban Dora Belgorosky y Susy Wolynski de Kreiman; y en el área de Infraestructura, de la que formaban parte Carlos Avendaño Bobadilla, Jacobo Chemauel y Olegario Ramírez.  
En tanto, la muestra virtual "Retratos" fue presentada el jueves pasado y, bajo el título "Ese día", expone el trabajo de la fotógrafa Alejandra López, quien retrató a sobrevivientes del atentado, al que se puede acceder a través del catálogo virtual en el portal web: [http://esedia.amia.org.ar.](http://esedia.amia.org.ar. "http://esedia.amia.org.ar.")  
En redes sociales, en tanto, la mutual difundió videos con testimonios de familiares de las víctimas, con el lema "Vivirán tanto como nosotros, porque los recordamos", y otro realizado por el grupo de percusión "La Bomba de Tiempo" y el rapero XXL Irione, con el que busca llegar con su mensaje a las generaciones que no tienen memoria vivencial del atentado.  
En el marco del 27 aniversario del ataque a la sede de la AMIA, se realizará el próximo jueves 15 de julio el Foro de Parlamentarios contra el Terrorismo, organizado por el Congreso Judío Latinoamericano (CJL) y la presidencia de la Cámara de Diputados, que se realizará por videoconferencia y funcionará como marco para generar y fortalecer políticas que enfrenten el flagelo del terrorismo internacional.