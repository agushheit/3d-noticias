---
category: Estado Real
date: 2021-08-04T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/12AÑOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Comenzó la vacunación a menores de 18 años con factores de riesgo
title: Comenzó la vacunación a menores de 18 años con factores de riesgo
entradilla: El operativo se desarrolla de manera simultánea en toda la provincia.
  Las autoridades insisten en que los niños, niñas y adolescentes mayores de 12 años
  con comorbilidades se inscriban para poder inocularse.

---
Este martes, el Ministerio de Salud inició la vacunación contra el Covid-19 a menores de 18 años en toda la provincia. La población objetivo son personas mayores de 12 años y menores de 18 años con comorbilidades, ya que se ubican en los grupos priorizados, definido por la Comisión Nacional de Inmunizaciones y los comités de expertos, y la Sociedad Argentina de Pediatría.

En ese marco, la ministra de Salud, Sonia Martorano estuvo presente en el inicio de la vacunación que se llevó adelante en el Hospital de Niños Zona Norte de la ciudad de Rosario junto al secretario de Salud, Jorge Prieto; el presidente de la Sociedad Argentina de Pediatría, Omar Tabacco, y la directora del hospital, Mónica Jurado. Paralelamente, la secretaria de Primer y Segundo nivel de Salud, Marcela Fowler, hizo lo propio en el hospital de Niños Orlando Alassia, en la ciudad capital, para anunciar este operativo que comenzó este martes en toda la provincia.

Martorano aseguró que “este es un momento de mucha alegría ya que era muy esperado, teníamos todo preparado, pero no contábamos con las vacunas para esta edad. A partir de la aprobación por parte de la Asociación de Medicamentos europea, comenzamos a delinear este operativo. Se trabajó mucho en el COFESA, con las sociedades científicas y con la Sociedad Argentina de Pediatría lo cual nos brinda un marco de seguridad para evaluar a qué grupo iba a ser destinado este operativo”.

La funcionaria provincial detalló que la vacuna Moderna “es diferente a las que teníamos ya que es una plataforma de ARN mensajero y justamente por eso se puede utilizar en este grupo etario”. Y en ese sentido destacó que ingresaron 70.280 dosis a la provincia, “tenemos un universo estimado en 80 mil personas, pero inscriptos solo está el 25%”. Por este motivo, recomendó que "se comuniquen con él o la pediatra para ver si recomienda iniciar con la vacunación”.

Seguidamente, la titular de la cartera de Salud provincial sostuvo que “queremos invitar a todas las personas que estén en este grupo a inscribirse. Hay un equipo de farmacovigilancia y de pediatría que los va a ir contactando para analizar la evolución, y los síntomas posibles. Es un operativo que se realiza en un marco de mucha seguridad y responsabilidad”.

También agregó que "el 29 de diciembre, cuando comenzamos con el operativo de vacunación, teníamos dosis y no teníamos brazos. Sabemos que cuando comienza la vacunación también se inician las consultas por lo que no tenemos dudas de que se van a ir anotando en el registro”.

Cabe señalar que entre las comorbilidades se encuentran: obesidad, diabetes, enfermedades crónicas, tanto renales como pulmonares o cardíacas; quienes cuenten con el certificado único de discapacidad, personas con inmunodeficiencia, personas adolescentes embarazadas, entre otros factores que pueden chequearse ingresando a la página de la provincia [https://www.santafe.gob.ar/santafevacunacovid/inicio](https://www.santafe.gob.ar/santafevacunacovid/inicio "https://www.santafe.gob.ar/santafevacunacovid/inicio")

**Vacunarse, un acto de solidaridad**

Por su parte, el presidente de la Sociedad Argentina de Pediatría, Omar Tabacco, explicó que “en el mundo la otra vacuna aprobada tiene el mismo diseño que la Moderna, que es la de Pfizer. Los estudios están en curso también para niños y niñas más pequeños”.

“Es difícil encontrar un argumento para no vacunarse, ya que es una vacuna segura, eficaz y el riesgo es menor. Priorizamos este grupo porque la severidad del virus es de 6 o 7 veces mayor en posibilidades de convertirse en una forma grave. Por ello lo celebramos, probablemente más adelante se puedan continuar con adolescentes sanos y allí es importante explicar que es un acto de solidaridad para con sus padres, abuelos”, expresó.

**Continuidad de la vacunación**

Por último, Martorano subrayó que “hoy se están colocando 7.000 dosis en toda la provincia. Vamos a repetir también la vacunación puerta a puerta, en algunos centros o con personas que estén internadas, al igual que hicimos con las personas mayores”.

Para la ministra es “muy importante completar los esquemas de vacunación que tenemos ante la situación epidemiológica”. Al mismo tiempo confirmó que “se cuenta con las segundas dosis garantizadas. Se destinaron 1 millón 800 mil dosis para este grupo (entre primeras y segundas dosis). En Buenos Aires ya están las segundas dosis para este sector”.

Por su parte, Marcela Fowler recomendó a quienes tengan problemas para movilizarse por alguna patología o si están con internación domiciliaria, se comuniquen al 0800 555 6549 para reprogramar el turno y acceder al dispositivo de vacunación domiciliaria.