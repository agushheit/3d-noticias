---
category: Estado Real
date: 2021-07-31T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/LACTANCIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia celebra la semana de la lactancia materna
title: La provincia celebra la semana de la lactancia materna
entradilla: 'Desde este domingo y hasta el 7 de agosto se realizarán diferentes acciones
  desde el Ministerio de Salud de la provincia con el lema: “Proteger la lactancia
  materna es una responsabilidad compartida”.'

---
El Gobierno de la provincia, a través del Ministerio de Salud y la Dirección provincial de Niñez y Adolescencia recuerda que todos los años, a nivel mundial, se celebra durante la primera semana de agosto la Semana Mundial de la Lactancia Materna con el objetivo de promoverla e involucrar a toda la sociedad en el compromiso de sostener y acompañar a las familias en el proceso de generar los cimientos de un psiquismo y una alimentación saludable para toda la vida, que comienzan desde el nacimiento.

El lema de este año es “Proteger la lactancia materna es una responsabilidad compartida”, explicó la directora del área, Lic. Silvina Vaghetti quien afirmó que “es muy importante concientizar a la Comunidad sobre la necesidad de implementar ‘Espacios amigos de la Lactancia’ además de dar a conocer la importancia de apoyar la lactancia, alentando y valorando a las personas que amamantan y a sus hijos”.

**Espacios Amigos de la Lactancia**

“Los Espacios amigos de la Lactancia garantizan y superan los derechos establecidos por la Ley de Trabajo, algo que implementamos a nivel provincial en diferentes dependencias”, sostuvo la funcionaria provincial.

Para finalizar, Silvina Vaghetti explicó que “compartir tareas de cuidado y crianza, distribuir equitativamente quehaceres y responsabilidades es fundamental para comprender que la Lactancia es una responsabilidad compartida”.

**Redes de Apoyo**

La Lic. Pamela Wilkinson, psicóloga de la Dirección Provincial de Niñez y Adolescencia argumentó que "los inicios de la lactancia materna transcurren paralelamente al puerperio de la madre, en este momento tan vulnerable es importante el apoyo continuo y sostenido para el logro del establecimiento de la misma y de un adecuado vínculo de apego."

"Las redes de apoyo (pareja, familia, amigos, tribu), son claves para el éxito y sostenimiento de la lactancia materna”, explicó la licenciada.

**Objetivo Feminista**

La licenciada en psicología Melina Bielsa, también integrante de la Dirección, "La dependencia de cuidado ha sido tradicionalmente femenina y tradicionalmente desprestigiada. Reivindicar el valor y el prestigio de esta actividad es un objetivo feminista. Ergo, practicar hechos como la Lactancia Materna simboliza un ejemplo de actuación feminista y una hermosa manera de cambiar al mundo hacia sociedades más pacíficas", finalizó.

**La importancia de la alimentación**

Por su parte, la Méd. Ped. Florencia Fontana, pediatra del área reflexionó: “Con los años hemos aprendido mucho respecto a nuestra alimentación y la de nuestros hijos e hijas. Tenemos que tomar conciencia para llevar adelante una vida más saludable”.

“Es importante tener un buen comienzo con la alimentación humana, de nuestra propia especie y como a veces, puede haber dificultades para llevarla adelante, tenemos que capacitarnos y actualizarnos para poder acompañar a las familias en este camino desde distintos lugares: salud, escuela, comunidad, sociedad”, argumentó la especialista.

**Capacitaciones virtuales**

Como consecuencia de la pandemia las capacitaciones se realizan virtualmente con el desarrollo actual del curso Alimentación Infantil. “Es un buen comienzo para una vida saludable que sirve para formación de todo el personal que se desempeña con madres y niños, niñas y adolescentes lo cual también permite acreditar como Institución Amiga de la Lactancia”, que es una estrategia para apoyar la lactancia a nivel mundial que llevamos adelante desde esta Dirección para las instituciones tanto a nivel público como privado”, finalizó la pediatra Florencia Fontana.