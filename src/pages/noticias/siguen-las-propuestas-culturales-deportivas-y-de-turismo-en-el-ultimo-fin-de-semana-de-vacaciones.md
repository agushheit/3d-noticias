---
category: La Ciudad
date: 2021-07-24T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/CULTURA.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Siguen las propuestas culturales, deportivas y de turismo en el último fin
  de semana de vacaciones
title: Siguen las propuestas culturales, deportivas y de turismo en el último fin
  de semana de vacaciones
entradilla: El renovado Anfiteatro “Juan de Garay” del Parque del Sur se suma a la
  agenda con propuestas para las infancias y otras a puro rock

---
La Municipalidad informó sobre las actividades que disfrutaron los santafesinos en el receso invernal y que cierran este viernes, sábado y domingo.

La agenda cultural, turística y deportiva de la Municipalidad de Santa Fe se renueva para disfrutar del último fin de semana del receso escolar. Se desarrollan distintas propuestas en el Anfiteatro “Juan de Garay”, en el Mercado Progreso, en el Teatro Municipal, en las Estaciones y Jardines Municipales, y en los polideportivos. En todos los espacios se mantiene el aforo permitido y se pide a los públicos respetar los protocolos para sostener la distancia social y participar de los momentos de recreación de manera segura.

Paulo Ricci, secretario de Educación y Cultura de la Municipalidad, recordó que por la pandemia “el año pasado no hubo actividades culturales; ahora celebramos poder hacerlo y esperamos buenas noticias para seguir desarrollándolas”. Destacó que la participación de los públicos es muy buena en todos los espacios, acompañando al sector cultural.

“Todos coincidimos en que ha sido uno de los rubros más golpeados y uno de los más respetuosos de las medidas. Estamos trabajando en el área metropolitana para coordinar las aperturas y estudiando la situación para ampliar los cupos donde los lugares lo permitan, como, por ejemplo, en el Anfiteatro”, afirmó.

**Anfiteatro y Mercado Progreso**

Dentro de las actividades culturales, el escenario ubicado en el Parque del Sur (San Jerónimo y avenida Illia) recibirá hoy viernes al varieté circense de Compañía Lado B. La fiesta seguirá con Pim Pau, el proyecto argentino/brasilero de arte, música y educación para niños y adultos que se ha posicionado como una revelación por su interesante propuesta y su rápido crecimiento. Las puertas del Anfiteatro se abrirán a las 15.30 para el ingreso del público. La entrada es gratuita, pero se debe retirar previamente, hasta dos por persona, en la boletería del Teatro Municipal (San Martín 2020) de lunes a sábado, de 9 a 13, y de 17 a 21 horas; y los domingos, de 17 a 21.

El cierre de la programación será a puro punk rock en el Anfiteatro del Parque del Sur, el sábado 24 a partir de las 16.30. La jornada comenzará con la música y compositora santafesina Cami Bosch; le seguirán Las Hijas de la Viuda. La grilla continuará con Faünaversus y, por último, será el momento de Giovi, Julia Pagnutti y Mely Deville, integrantes de Vomitan Glitter, trío que milita y visibiliza en forma de canción las vivencias del colectivo travesti-trans. Al igual que para el viernes, la entrada es gratuita pero se debe retirar previamente en la boletería del Teatro Municipal y se entragarán hasta dos ticket por persona.

En el Mercado Progreso, la programación del espacio ubicado en Balcarce 1635 se desarrollará entre hoy viernes y el domingo 25, de 15 a 18 horas. La entrada es gratuita y se permitirá el ingreso del público por orden de llegada. En la primera tarde se podrá disfrutar del show del Mago Fernán seguido de “Dolly y el Hombre Sombra (Relato de dos mundos)” con dramaturgia y dirección de Soledad Maglier.

El Elenco del Teatro de Títeres Municipal abrirá el juego en la tarde del sábado con sus “Canciones animadas”. Luego, el grupo musical-escénico Paquito presentará “Aventuras en canciones”, con renovaciones en su repertorio y en la puesta en escena. En la tarde del domingo se podrá disfrutar de una nueva presentación del Mago Fernán, para seguir con el grupo de percusión corporal AIÉ Movimiento Cultural de Música Corporal, que reúne expresiones folklóricas argentinas y afrolatinoamericanas. Adrián Rosso, Mónica Giumelli y Silvia Nerbutti, del Grupo La Tramoya, invitarán después a la participación del público con “Comiclown”, el espectáculo de murga y rutinas de payasos.

**Teatro Adentro**

El Elenco del Teatro de Títeres Municipal retoma las funciones de “Teatro Adentro: una aventura tras bambalinas”, su creación más reciente, un recorrido por las instalaciones del coliseo “1° de Mayo” que estrenaron en marzo de este año. Dos personajes -Mónica y Robert Von Theatre- guían al público por la historia del emblemático escenario santafesino y sus misterios. Esta visita entre recuerdos, risas y suspenso se presentará el domingo 25 en tres funciones: a las 17, 18 y 19 horas.

Las entradas son gratuitas, pero se deben retirar previamente en la boletería, en los horarios mencionados anteriormente, ya que los cupos son limitados por protocolo sanitario. Habrá nuevas funciones el miércoles 28 de julio a las 17 y 18 horas; y el domingo 1 de agosto a las 17, 18 y 19.

En tanto, en la Sala Marechal se podrán disfrutar tres propuestas vinculadas a las artes circenses, la música y la narración oral. Las funciones comenzarán a las 17 y las entradas están a la venta en la boletería. El jueves 22, la Compañía Lado B presenta “Que no te la cuenten”, un espectáculo de circo para toda la familia. “Maravillas Maravillador” es la propuesta del viernes 23, con Marcela Sabio y Jorge Mockert, integrantes del grupo de narradores orales Puro Cuento. El grupo Circontando protagoniza la función del sábado 24, con su varieté de circo “Vamos a Jugar”. Las interpretaciones están a cargo de Ana Laura Municoy, Jeremías Blanco, Sara Krumm, Francisco Orellano y Esteban Sosa, también autor y director.

**Para jugar y practicar**

Durante la semana de vacaciones se sostienen las actividades deportivas y recreativas en el Parque del Sur, la Dirección de Deportes ubicada en la Costanera Oeste, el Parque Garay, la Estación Belgrano, el Polideportivo La Tablada, el Polideportivo Municipal de Alto Verde y el Centro Gallego.

La secretaria de Integración y Economía Social, Ayelén Dutruel, explicó que “la gama de propuestas es amplia, desde los deportes reglados hasta otros más relacionados con la recreación y los vínculos”. En este contexto, “creemos que la salud tiene que ser pensada desde la promoción, con una mirada integral, y los beneficios que brindan las actividades deportivas son sumamente importantes, por eso tomamos todas las medidas para poder sostenerlas, con burbujas e inscripciones previas”.

Durante las vacaciones, el área de Deportes municipal también estuvo acompañando a clubes y asociaciones deportivas en la protocolización de sus actividades, “principalmente a los clubes barriales, que son quienes más han sufrido la pandemia y sabemos que dan una contención inmensa en su territorio y a las juventudes de la ciudad”, describió la funcionaria.

En el último fin de semana de vacaciones, las actividades se desarrollarán el sábado, de 10 a 13, y el domingo, de 14 a 17 en el Centro de Deportes y el Parque del Sur, con propuestas gratuitas que van desde juegos en familia hasta newcom (voley para personas adultas mayores), pasando por clases abiertas de ritmos, mini voley en la playa y una invitación para jugar ajedrez.

También en las Estaciones municipales ubicadas en diferentes barrios, durante esta semana hubo una amplia oferta, en la cual se contaron meriendas con propuestas lúdicas, kermeses, proyección de la película “Alicia en el país de las maravillas”, búsqueda del tesoro en la plaza, bingos y picnics entre otras, que compartieron los chicos y sus familias.

**Mi ciudad como Turista**

El programa que propone a visitantes y santafesinos descubrir lugares emblemáticos de la capital provincial y sus historias continúa durante la segunda semana de receso de invierno con visitas guiadas gratuitas organizadas por la Municipalidad. De esta manera, de miércoles a domingos habrá diversas propuestas, siempre a las 16.30.

Hoy -viernes- será el turno de Paseo del Boulevard, con punto de encuentro en la Plaza Pueyrredón (bulevar Gálvez 1600). El sábado se realizará el Paseo Peatonal, con punto de encuentro en el Teatro Municipal (San Martín 2020). Otro imperdible es la Manzana Jesuítica (San Martín 1540), que abrirá sus puertas de martes a sábados, de 11 a 16, y las visitas guiadas serán a las 11 y a las 15. Tanto los circuitos de Mi Ciudad como Turista como la Manzana Jesuítica se pueden recorrer de forma particular, utilizando las audioguías de la Municipalidad de Santa Fe.

Los paseos se llevarán a cabo cumpliendo con los protocolos establecidos: se trabajará con grupos de hasta 10 personas y es obligatorio el uso de barbijos. Acompañarán los guías profesionales identificados con las sombrillas verdes.

Otra opción es visitar la Reserva Ecológica de la Ciudad Universitaria, ubicada sobre la Costanera. La entrada es gratuita, de miércoles a domingos de 9.30 a 17.30 y las visitas guiadas -por orden de llegada- son de viernes a domingos de 14 a 17 horas.