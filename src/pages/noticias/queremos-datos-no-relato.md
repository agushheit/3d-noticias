---
category: Estado Real
date: 2022-02-17T10:53:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/learning-g3d6702d7d_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: "“Queremos datos, no relato”"
title: "“Queremos datos, no relato”"
entradilla: 'En un comunicado, “Docentes por la educación” cuestionan al gobierno
  provincial por que:  “A  días del comienzo del ciclo lectivo no tenemos diagnostico,
  datos oficiales, chicos en las aulas ni fondos para buscarlos”'

---
En dicho comunicado,  afirman que “la ministra y el gobernador aseguren que se redujo a la mitad el abandono interanual, que hablen de ‘números no preocupantes de deserción’, que manden a buscar casa por casa a los intendentes y presidentes comunales a los alumnos que terminaron 7mo y no se inscribieron en el secundario y que hayan celebrado la concurrencia de 31 mil chicos al programa ”Verano activo” no hace mas que seguir mostrando que no tienen planificación en materia educativa.”

Otro dato no menor es que, con la circular 8/21 de secundaria,  (cuestionada en noviembre) al eliminar las mesas de exámenes y proponer los trabajos interdisciplinarios por trayectos mal inventados, no consideraron que en febrero los docentes con mayor antigüedad toman posesión el día 17, quedando una semana (con suerte 4 horas cátedras) para acompañar el alumno en los aprendizajes pendientes. Nunca pensaron en los alumnos. Nunca hubo planificación”

![](https://assets.3dnoticias.com.ar/docentes por ed comun.png)