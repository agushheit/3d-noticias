---
category: Agenda Ciudadana
date: 2021-06-25T08:08:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/comercio.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: INDEC
resumen: En un año, la desocupación creció en el Gran Santa Fe un 45 por ciento
title: En un año, la desocupación creció en el Gran Santa Fe un 45 por ciento
entradilla: Según el Indec, la tasa de desocupación del primer trimestre fue del 9%,
  contra el 6,2% de igual período del año pasado. Según el organismo, se sumaron ocho
  mil desocupados.

---
Según el Instituto Nacional de Estadísticas y Censos, la tasa de desocupación en el gran Santa Fe alcanzó el 9 por ciento en el primer trimestre de este año. Registró un crecimiento de 1,2 por ciento con relación al último trimestre del 2020, cuando el Instituto informó una desocupación del 7,8 por ciento.

Si la comparación es con igual período del año pasado, se desprende que la desocupación creció un 45 por ciento en el aglomerado Santa Fe. En este caso, el escenario comparativo está marcado por las consecuencias de la pandemia y cuarentena.

Es que apenas nueve días del primer trimestre del 2020 estuvieron afectados por la cuarentena y aislamiento estricto. El Indec midió durante ese lapso de tiempo una desocupación del 6,2 por ciento, contra el nueve por ciento del primer trimestre pero de este año.

El Indec informó que en el aglomerado Santa Fe, y en los primeros tres meses del año, se contabilizaron 22 mil desocupados; tres mil más que el trimestre inmediato anterior y ocho mil más que durante el mismo período pero de un año atrás (un aumento del 57 por ciento).

La tasa de desocupación del Gran Santa Fe es mayor al promedio del país. A nivel nacional, y también en términos interanuales, la situación fue diferente. Según el Indec, se registró un descenso. La tasa de desocupación alcanzó al 10,2% al término del primer trimestre del año, por debajo del 10,4.% de igual período del año, y del 11% con que cerró el 2020.

**Números nacionales**

En tanto, la subocupación demandante se ubicó al término del primer trimestre en el 8,7% frente al 8,2% de igual período del 2020, mientras que la no demandante fue del 3,2% frente al 3,5% anterior.

Estas diferencias se dieron en medio de un avance del Producto Bruto Interno (PBI) de 2,5% durante el primer trimestre del corriente año respecto a igual período de 2020, y de 2,6% frente al trimestre anterior.

El informe del Indec detalló que en el primer trimestre de 2021, la tasa de actividad (TA) -que mide la población económicamente activa (PEA) sobre el total de la población– alcanzó el 46,3%.

En tanto, la tasa de empleo (TE) -que mide la proporción de personas ocupadas con relación a la población total- se ubicó en 41,6%; y la tasa de desocupación (TD) -personas que no tienen ocupación, están disponibles para trabajar y buscan empleo activamente, como proporción de la PEA- se ubicó en 10,2%.