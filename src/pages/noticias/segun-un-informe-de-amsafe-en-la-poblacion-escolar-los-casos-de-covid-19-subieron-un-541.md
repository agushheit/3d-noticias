---
category: Agenda Ciudadana
date: 2021-05-15T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMSAFE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Según un informe de Amsafé, en la población escolar los casos de Covid-19
  subieron un 541%
title: Según un informe de Amsafé, en la población escolar los casos de Covid-19 subieron
  un 541%
entradilla: Amsafé presentó un documento  que afirma que en la población escolar los
  casos subieron 541%, mientras que en el resto se ubica en torno a 350%. Piden "condiciones
  seguras para enseñar y aprender en clases".

---
El debate por el impacto que tienen las clases presenciales en la evolución de los contagios por coronavirus sumó una herramienta de debate. Un informe de la Asociación del Magisterio de Santa Fe (Amsafé), en base a datos oficiales, señala que la franja etaria de 0 a 17 años (población escolar) es la más afectada en términos relativos por la segunda ola.

El trabajo, con tablas y gráficos, indica que "en las últimas semanas los casos diarios pasaron de 400 a más de 1700" en la provincia y en promedio. Sin embargo, ese salto de 425% difiere si se divide a la población por edades.

La media de febrero para el universo de hasta 17 años era de 17 casos y en abril pasó a 92: un aumento de 541%, muy por encima del promedio total. El resto de las franjas estarias oscila entre 333% y 388%.

El informe de Amsafé provincial y Ctera no afirma de forma categórica la relación entre presencialidad y nuevos casos de covid-19 pero señala: "Al analizar el crecimiento de casos por franjas etarias obtenemos la tabla 1, donde se puede observar el promedio de casos diarios para el mes de febrero (previo al inicio de clases) y el promedio en el mes de abril. El rango entre 0 y 17 años presentó el aumento relativo más alto".

"Los contagios se van multiplicando a lo largo y ancho de la provincia. El crecimiento se está dando de manera exponencial provocando la saturación del sistema de salud", añade el documento y refuerza: "Las escuelas son lugares donde todos sus miembros hacen esfuerzos denodados por cumplir y sostener los protocolos, pero la escuela sola no puede. La escuela es parte de una comunidad y si esa comunidad tiene un alto nivel de casos positivos, los niños, niñas, docentes y asistentes escolares están en riesgo".

Con respecto a los efectos de ese escenario sanitario, Amsafé advierte: "Dicho crecimiento demora en verse reflejado en la cantidad de internaciones y muertes, aunque ya se observa una tendencia en aumento. Se pasó de un promedio de 5 muertes diarias en el mes de marzo a más de 15 entre fines de abril y principios de mayo. Esto implica que se triplicaron las muertes diarias en un periodo de un mes".