---
category: Agenda Ciudadana
date: 2020-11-28T14:03:00Z
thumbnail: https://assets.3dnoticias.com.ar/PEROTTI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'ASPO '
title: 'Perotti: “Vamos a seguir con un distanciamiento  social, preventivo y obligatorio
  duro, con controles y protocolos”'
entradilla: El gobernador participó de un nuevo encuentro del Comité de Expertos donde
  afirmó que “pensar que salimos del Aislamiento Social, Preventivo y Obligatorio
  (ASPO) porque está todo resuelto, es un error”.

---
El gobernador Omar Perotti junto con la ministra de Salud, Sonia Martorano, encabezaron este viernes un nuevo encuentro del Comité de Expertos en la prevención y tratamiento del Covid 19.


En la oportunidad, el gobernador afirmó que “mantener este esquema de coordinación público-privado nos ha permitido atender a todos los santafesinos y santafesinas”, al tiempo que adelantó que la mayoría de las provincias vamos “a seguir con un Distanciamiento Social, Preventivo y Obligatorio (DISPO) duro, con controles y protocolos”.


“Pensar que salimos del Aislamiento Social, Preventivo y Obligatorio (ASPO) porque está todo resuelto es un error. Vamos a seguir con habilitaciones, que tienen que ver con el aire libre y el calor, pero todo con protocolos”, aseguró.

## Reuniones durante las fiestas de fin de año


El mandatario provincial aprovechó el encuentro con los destacados profesionales en la salud para afirmar que “vamos a ir preparando las reuniones sociales para las fiestas, porque inevitablemente se van a dar. Hay que ser claros en los mensajes. En las fiestas no van a ser 10 o 20 las personas que se reúnan esa noche, sino todos los contactos previos que cada uno de los participantes tuvo antes de ese encuentro. 

Con lo cual, hay que tomar precauciones varios días antes, 48 o 72 horas antes de las fiestas y habrá que extremar los cuidados”, agregó el gobernador.


Y en ese sentido agregó: “Va a estar permitido juntarse, pero lo tenemos que hacer con la mayor responsabilidad posible. Debemos entender que hay que tomar todos los cuidados posibles, apelando a que se vienen días en los que nos vamos a juntar con los que más queremos”.

## Vacunación

Además, Perotti adelantó que se están llevando a cabo reuniones con el “Ministerio de Defensa de la Nación para organizar un esquema de logística de vacunación. 

En la provincia tenemos buena estructura de especialistas en frío y los hemos consultado para garantizar todas las alternativas. Hay vacunas con más necesidad de frío y otras con menos, con lo cual tenemos que estar preparados para las dos”.


“Hay una idea de cómo alcanzamos a todas las personas que son factores de riesgo, considerando mayores de 60 años. Un elemento que ayuda a identificar eso es el padrón electoral”, afirmó el mandatario provincial.


Por último, el gobernador remarcó que, durante la época de verano, “vamos a tener mucha gente en la provincia, particularmente los santafesinos. 

Estamos reforzando en muchos lugares los controles viales y sanitarios, para resguardar de la mejor manera toda una zona de Santa Fe hacia el norte en la costa, que va a tener un nivel de movimiento realmente importante en cabañas y lugares de pesca. Allí vamos a tener doble tarea en el verano, de estar muy presentes en esas zonas, particularmente cuando nos alejamos de Rosario y Santa Fe”.


Por último, y tras la finalización de la videoconferencia, la ministra de Salud de la provincia, Sonia Martorano sintetizo la experiencia de la tarde del viernes señalando que “nos reunimos con el gobernador Omar Perotti y el equipo de infectólogos, médicos y distintos asesores del ámbito de la ciencia y la tecnología con los que evaluamos el panorama sanitario de la provincia”. 

La titular del área sanitaria provincial explicó que “en esta oportunidad evaluamos la actual situación que nos presenta el virus, los diferentes indicadores, y las posibilidades de continuar en una DISPO (Distanciamiento Social, Preventivo y Obligatorio) con controles estrechos. 

También pusimos en valor la vacunación y los primeros alcances de la logística a desarrollar a partir de enero para una vacunación masiva y que tiene que desarrollarse en el menor tiempo posible” concluyó Martorano.