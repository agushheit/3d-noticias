---
category: Agenda Ciudadana
date: 2021-03-02T05:49:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/discurso.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La oposición calificó el discurso como "vergonzoso y provocador"
title: La oposición calificó el discurso como "vergonzoso y provocador"
entradilla: "Los legisladores de Juntos Por el Cambio criticaron el discurso del Presidente
  en el marco de la apertura de sesiones en el Congreso.\n\n"

---
Legisladores nacionales de Juntos por el Cambio que participaron de la Asamblea Legislativa en el Congreso fustigaron este lunes el discurso del presidente Alberto Fernández, al que calificaron de "vergonzoso y provocador" y destacaron que fue "una oportunidad perdida de convocar a todos los sectores" para debatir las soluciones a los problemas del país.

"Por el contrario, se dedicó todo el tiempo a echar culpar y a demonizar a la gestión anterior", sostuvo por LT10 Federico Angelini, diputado nacional de Juntos Por el Cambio. Respecto de los proyectos que anunció opinó que "una cosa es lo que anuncia y otra cosa es lo que termina sucediendo con el tiempo y las internas que tiene el propio gobierno". 

**Otras repercusiones** 

"Me pareció un discurso vergonzoso y provocador. El presidente de un Gobierno que le robó las vacunas al personal de salud y a los mayores para vacunar a los chicos de La Cámpora y a los amigos del poder, habla de que nadie se salva solo. Pide aplausos para el personal de salud al mismo tiempo que les roba las vacunas. Habla de una denuncia penal contra el Gobierno anterior cuando el gobierno actual se está endeudando al triple del gobierno anterior. Es una vergüenza", arremetió el diputado macrista Fernando Iglesias, quien protagonizó un cruce con Fernández en el recinto, mientras el jefe de Estado leía su mensaje.

Luego de que el legislador macrista lo interrumpiera en reiteradas ocasiones, Fernández le dijo que había tenido "cuatro años para hablar".

"Tengo derecho a hablar, soy diputado. Él tiene todo el derecho de hablar y decir cosas. Lo que me gustaría es que hablara de lo que tiene que hablar, del vacunatorio vip, de los 51.000 muertos. Habla del golpe de Bolivia y se olvida de  la dictadura de Venezuela. El único proyecto claro que tiene es avanzar contra la Justicia y así lo ha anunciado con ese tribunal para evitar que los asuntos lleguen a la Corte Suprema", contestó Iglesias en declaraciones a NA.

Por otra parte, rechazó el anuncio de que el Estado iniciará una querella contra la administración de Mauricio Macri por fraude al Estado y malversación de fondos a raíz del endeudamiento con el FMI.

"Este gobierno se endeudó el año pasado por 20.0000 millones de dólares al mismo tiempo que remataba 10.000 millones que le habíamos dejado de reservas. Es el triple de endeudamiento", comparó.

En diálogo con NA, la diputada radical Brenda Austin advirtió que el presidente "se la pasó agrediendo constantemente" en todo su discurso, "sin hacer una real autocrítica" y "poniendo la culpa afuera".

"Mi balance no es positivo. El Presidente habló a lo largo de sus discurso en distintos pasajes de una convocatoria al dialogo y la unidad, y lo hizo agrediendo constantemente no sólo a la oposición, sino a la Justicia, a los medios, a los empresarios.

La acusación velada todo el tiempo de que buscan detonar el país. Sin reconocer primero los errores propios, sin una real autocrítica", sostuvo.

"Ayer la gente marchó en todo el país por la indignación del vacunatorio Vip. El Presidente volvió a hablar de errores cuando son tipos penal contenidos en el Código penal", criticó la cordobesa.

Por otra parte, lamentó que el jefe de Estado no haya hablado de "la tragedia más triste, cruel y fuerte que dejó la pandemia que fue que un millón y medio de chicos dejaron la escuela", en tanto que también cuestionó las herramientas mencionadas por el presidente para contener la inflación.

"Las propuestas para contener la inflación son la vuelta al pasado. Reivindicó el control de precios. Desconoce los problemas estructurales. Así como cuando habla del empleo reivindica la prohibición de despidos", se quejó.

"Aquellas cosas que son evidentes, que están en retina de gente y que son las preocupaciones cotidianas no las nombró. Fue una oportunidad perdida del presidente Fernández de convocar a todos los sectores para sacar el país adelante", resaltó.

Por su parte, el diputado mendocino Luis Petri se sumó al rechazo al mensaje de Fernández en la Asamblea Legislativa y habló de una "brutal embestida del Presidente contra el Poder Judicial en pos de la impunidad K".

"El Congreso y por último la Corte serán los campos de batalla para defender la República", aseguró el legislador radical, quien alertó que el Gobierno "va por el Consejo de la Magistratura instando su reforma y la creación de un tribunal federal de arbitrariedad".

"Buscan licuar a la Corte con este nuevo tribunal y tomar por asalto al Consejo", agregó.

"Está claro que para este Gobierno no somos todos iguales, algunos vacunados vip, otros eximidas de llevar barbijo", dijo el mendocino en referencia a que la vicepresidenta Cristina Kirchner se encontraba en el recinto sin tapabocas.

"La credibilidad del Gobierno se deteriora por la improvisación, la falta de información, los privilegios y  el Vacunatorio VIP, no ponga excusas Presidente, pida perdón", exclamó.

En tanto, Alfredo Cornejo (UCR) indicó a NA que Fernández "perdió una gran oportunidad de llamar a la unidad nacional" a partir de un "discurso negacionista de los problemas".

Por la Coalición Cívica, Lucila Lehmann consideró que "el Presidente dio un discurso confrontativo que aumenta la polarización". 

"No hizo mención al mayor destrozo económico que sufrió la Argentina en relación al resto del mundo y tampoco planteó un plan económico con una mirada hacia el futuro. Fernández llevó la pobreza al 46% pero parece que tampoco tiene la culpa y deliberadamente llama ‘errores’ al Vacunatorio VIP. No es un delito para el Presidente quitarle la vacuna a quién lo necesita para hacer política, es sólo un ‘error’. Causa indignación y preocupación", remató.

En representación del lavagnismo, Alejandro "Topo" Rodríguez (Consenso Federal) opinó que fue "un discurso en general correcto" si bien "eligió algunos anuncios para la pelea".

"No hubo autocríticas y faltaron propuestas consistentes para crear trabajo en el sector privado", cuestionó, aunque repartió críticas y apuntó contra Juntos por el Cambio: "Un puñado de representantes del macrismo vino a gritar y agredir. Son vestigios de una grieta que se agota".