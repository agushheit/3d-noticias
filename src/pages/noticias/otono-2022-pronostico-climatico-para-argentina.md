---
category: Agenda Ciudadana
date: 2022-03-04T09:42:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/clouds-gc13c02837_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Servicio Meteorologico Nacional
resumen: OTOÑO 2022 | Pronóstico climático para Argentina
title: OTOÑO 2022 | Pronóstico climático para Argentina
entradilla: Luego de un verano muy caluroso y seco, con récords en varias zonas del
  país, te contamos cuál es la tendencia de temperaturas y precipitaciones para el
  trimestre marzo-abril-mayo 2022 en Argentina.

---
Como todos los meses, compartimos la nueva actualización del Pronóstico Climático Trimestral (PCT) que muestra la tendencia de temperatura media y precipitación esperada para el próximo trimestre.

Al elaborar este producto se considera que el comportamiento de la lluvia y la temperatura puede tener tres categorías posibles: inferior a lo normal, normal y superior a lo normal. Sin factores que modifiquen la circulación atmosférica típica del trimestre, cada categoría tiene la misma probabilidad de ocurrencia: un 33,3 %. Pero cuando actúan forzantes atmosféricos, como El Niño o La Niña, la circulación planetaria cambia y la ocurrencia de alguna de esas categorías se hace más -o menos- probable. 

Pronóstico de temperaturas

Se observa para el trimestre marzo-abril-mayo 2022 una mayor probabilidad de que las temperaturas sean normales o superiores a lo normales, es decir, más cálido que lo habitual sobre gran parte del país.

En tanto, sobre las provincias del NOA (Noroeste Argentino), Cuyo y sur de Patagonia hay mayor probabilidad de ocurrencia de temperaturas dentro del promedio para la época.

Pronóstico de precipitaciones

En cuanto a las precipitaciones, el pronóstico que involucra al otoño 2022 nos indica que hay alta probabilidad de que se registre un trimestre más seco que lo habitual sobre la región del Litoral y el este de Buenos Aires. 

Además, es más probable que las lluvias estén dentro del promedio o inferiores a lo normales en Córdoba, el oeste de Santa Fe, oeste de Buenos Aires, este de La Pampa y centro-norte de Patagonia.

Por otro lado, sobre las provincias del NOA y en el extremo sur de Patagonia pueden registrarse lluvias por encima del promedio para la época, mientras que hacia el norte del país se espera que estén dentro del rango normal. 

Cabe destacar que el pronóstico de precipitaciones para este trimestre no es favorable para el noreste de nuestro país, considerando la situación crítica de los últimos meses. Además, continuaría el fenómeno de La Niña gran parte del otoño, lo cual repercute en el clima de la región.