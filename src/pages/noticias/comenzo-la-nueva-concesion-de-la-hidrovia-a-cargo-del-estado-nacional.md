---
category: Agenda Ciudadana
date: 2021-09-12T06:15:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/HIDROVIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Comenzó la nueva concesión de la Hidrovía a cargo del Estado Nacional
title: Comenzó la nueva concesión de la Hidrovía a cargo del Estado Nacional
entradilla: La Administración General de Puertos (AGP) estará a cargo de controlar
  y gestionar durante los próximos 12 meses la vía navegable más importante de la
  Argentina.

---
La nueva concesión de la Hidrovía Paraguay Paraná se inició a partir del primer minuto de este sábado con el Estado Nacional, a través de la Administración General de Puertos (AGP), a cargo de controlar y gestionar durante los próximos 12 meses la vía navegable más importante de la Argentina, que atraviesa siete provincias.

Esta nueva modalidad contará además con un Ente Nacional de Control y Gestión de la Vía Navegable, creado a fines de agosto por el Gobierno, y del que participan además del Poder Ejecutivo Nacional, representantes de Buenos Aires, Santa Fe, Chaco, Formosa, Misiones, Corrientes y Entre Ríos.

El viernes al cerrar el día, la concesión privada que administró la Hidrovía en los últimos 26 años llegó a su fin, luego de un proceso iniciado en 2020 cuando el presidente Alberto Fernández lanzó un Acuerdo Federal para abordar el desarrollo de la ruta fluvial que en la Argentina cuenta con 1.635 kilómetros, sobre un total de 3.400 que tiene el canal que se comparte con Bolivia, Brasil, Paraguay y Uruguay.

El viernes al cerrar el día, la concesión privada que administró la Hidrovía en los últimos 26 años llegó a su fin

En 1995, el gobierno menemista otorgó la concesión de las tareas de mantenimiento, dragado y balizamiento del Paraná al consorcio Hidrovía, integrado por la empresa belga Jan de Nul y la argentina Emepa.

Esta concesión venció el 30 de abril último, pero se estableció una prórroga que duró hasta el viernes, para iniciar un proceso que permita definir el formato o modalidad de operación de la principal vía navegable troncal.

En este marco, el Presidente, a través del decreto 427/2021, decidió el 1 de julio último dejar a la Hidrovía bajo control estatal, más precisamente a manos de la AGP.

"Es un paso adelante. Si bien es una medida transitoria, da certeza de lo que va a pasar con la Hidrovía. Y da tiempo al Gobierno a preparar un proyecto de licitación, en lugar de hacer algo a las apuradas", destacó a Télam Guillermo Misiano, presidente del Grupo PTP, uno de los principales operadores portuarios del Paraná.

PTP tiene el manejo de terminales sobre la Hidrovía en las ciudades bonaerenses de San Nicolás, Ramallo y Lima, Nueva Palmira (Uruguay) y Villeta (Paraguay), y está en proceso de construir dos nuevos puertos: uno en la Zona Franca Santa Fe de Villa Constitución y el otro en Brasil.

Misiano consideró que la decisión del Gobierno respecto de la vía navegable "es importante para planificar las inversiones que se van a hacer", y señaló que "si la Hidrovía cambia de 33 a 40 pies de profundidad, habrá que invertir en los puertos para adaptarse".

Añadió que "también los importadores y exportadores deberán adaptar toda la cadena logística a la nueva profundidad", pero destacó que esto conllevará una "consecuente reducción de costos" para el comercio.

"En lo personal tengo expectativas muy positivas, porque este paso que se dio, en el contexto que venía la situación, complicada por la pandemia, es certero. Es una decisión inteligente que da una señal clara, al mercado y a todos los actores, de hacia dónde el Gobierno quiere ir con la Hidrovía", afirmó Misiano.

Por su parte, la presidenta de la Cámara de la Industria Naval Argentina (CINA), Silvia Martínez, destacó a esta agencia que espera que el proyecto de desarrollo que tiene el Gobierno para la Hidrovía apunte a "defender la industria nacional, fomentar las economías regionales y promover el empleo argentino".

Para eso, señaló que es preciso "buscar e impulsar sinergias entre el sector público-privado como motor del crecimiento nacional", y también sostuvo que se debe "considerar al transporte marítimo como un actor relevante de la economía nacional, en su condición de dador de empleo, y socio colaborativo de otros rubros como el petroquímico, minero, turismo y ferroviario".

Además, consideró que es necesario "recuperar la bandera argentina de buques marítimos, fluviales y de pesca en la participación de la Hidrovía".

En la medianoche las autoridades de la AGP junto con el subsecretario de puertos, Leonardo Cabrera, participaron del acto que marcó el cierre la concesión de Hidrovía S.A.

En la medianoche las autoridades de la AGP junto con el subsecretario de puertos, Leonardo Cabrera, participaron del acto que marcó el cierre la concesión de Hidrovía S.A.

En ese sentido, pidió "apoyar a los astilleros y talleres nacionales dedicados a la construcción, reparación y mantenimiento de flota, y a los proveedores de insumos, como las pymes naval partistas, mediante la implementación de políticas navieras concretas y sustentables que garanticen la competitividad de los buques y armadores argentinos".

Asimismo, Martínez propuso "garantizar la navegabilidad y seguridad de la Hidrovía, a través de la reactivación o creación de una empresa de dragado que mantenga las vías navegables en buenas condiciones", y evaluó que hay que "cuidar el medioambiente, a través de la descarbonización del transporte y dragado con barcos a gas natural licuado (GNL)".

En tanto, el secretario general del Centro de Patrones y Oficiales Fluviales, de Pesca y de Cabotaje Marítimo, Julio González Insfrán, destacó a Télam la existencia de otros órganos que intervendrán en la gestión y administración de la Hidrovía, como el Ente Nacional de Control y la Comisión Bicameral de Seguimiento Control de la Licitación y Funcionamiento de la Hidrovía.

En ese sentido, indicó que "todo esto en conjunto con la AGP y la Subsecretaría de Puertos y Vías Navegables, va a influir sobre la Hidrovía".