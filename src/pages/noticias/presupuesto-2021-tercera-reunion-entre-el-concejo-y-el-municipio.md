---
category: La Ciudad
date: 2020-12-12T12:16:22Z
thumbnail: https://assets.3dnoticias.com.ar/concejo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'PRESUPUESTO 2021: Tercera reunión entre el Concejo y el Municipio'
title: 'PRESUPUESTO 2021: Tercera reunión entre el Concejo y el Municipio'
entradilla: Concejales y concejalas se reunieron vía online con funcionarios del Ejecutivo
  Municipal para evacuar dudas e interrogantes sobre el Presupuesto 2021 y la Ordenanza
  Tributaria.

---
En el tercer encuentro, que se dio de manera virtual, el cuerpo legislativo local dialogó con los funcionarios del Ejecutivo Municipal, el secretario de Gobierno Nicolás Aimar; la secretaria de Hacienda Carolina Piedrabuena, y la secretaria de Obras y Espacio Público, Griselda Bertoni.

El encuentro encabezado por el presidente del Concejo Leandro González, contó con la presencia de las concejalas y concejales: Mercedes Benedetti, Luciana Ceresola, Federico Fullini, Paco Garibaldi, Guillermo Jerez, Inés Larriera, Valeria López Delzar, Sebastián Mastropaolo, Laura Mondino, Jorgelina Mudallel, Carlos Pereira, Juan José Saleme, Lucas Simoniello, María Laura Spina y Carlos Suárez.

<br/>

# **Tercer encuentro de trabajo**

Fue el 16 de noviembre cuando el intendente Emilio Jatón presentó a los ediles el Presupuesto en la Estación Belgrano. Luego, el primero de diciembre los concejales y las concejalas recibieron a Nicolás Aimar y Carolina Piedrabuena en el recinto de Sesiones para discutir detalles del Mensaje.

En esta jornada, se sumó la secretaria de Obras y Espacio Público con su equipo de trabajo, para brindar detalles solicitados oportunamente por los ediles, para evaluar, estudiar y analizar distintos puntos importantes, los cuales, en los próximos días, se pondrán a consideración en el recinto del Concejo.

Por su parte, el presidente del Concejo Leandro González, destacó la importancia de una nueva instancia de trabajo que ayuda a estudiar y analizar el Mensaje.

<br/>

<hr style="border:2px solid red"> </hr>

“Es fundamental el diálogo y el análisis sobre esta herramienta primordial para que el Ejecutivo gestione y mejore la calidad de vida de santafesinos y santafesinas. Fue un año complejo y difícil y sabemos que los meses venideros nos tienen que encontrar trabajando juntos para salir adelante. Por eso la discusión sobre estas dos Ordenanzas habla de la voluntad de todas las partes para llevar tranquilidad a los vecinos y vecinas, como así también, seguir mejorando día a día el funcionamiento de la ciudad”.

<hr style="border:2px solid red"> </hr>