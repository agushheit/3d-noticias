---
category: La Ciudad
date: 2020-12-05T12:03:54Z
thumbnail: https://assets.3dnoticias.com.ar/leandro-gonzalez.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El Concejo Municipal reeligió a Leandro González como presidente del cuerpo
title: El Concejo Municipal reeligió a Leandro González como presidente del cuerpo
entradilla: 'Así, el intendente Emilio Jatón se garantiza tener un interlocutor que
  "baje línea" y coordine el trabajo con los concejales. Consenso político y diálogo
  es lo que destacan los ediles. '

---
Fue una asunción serena y tranquila. Las gradas del recinto estuvieron casi vacías. Apenas solo dos banderas colgadas. Una rezaba "#Encuentro: las personas somos el corazón de las ciudades". La otra afirmaba "Ideas que convencen. Ejemplos que movilizan". Ambas en apoyo al presidente reelecto.

Los ediles cada uno con su tapaboca y respetando el distanciamiento. Dentro de la sala estuvieron los secretarios del cuerpo, el intendente Emilio Jatón y algún funcionario cercano y no mucho más. El resto del Departamento Ejecutivo municipal vía zoom junto a invitados especiales. El propio gobernador Omar Perotti se encargó de hacer llegar su propia salutación.

En ese contexto, asumió por segunda vez consecutiva la presidencia del Concejo Municipal Leandro González. Sin rosca política, sin reuniones a último momento, sin la necesidad de contar y recontar una y otra vez los votos con los que se cuentan. Algo que, por ejemplo, sucedió el año pasado (a esta misma altura), donde el peronismo mostró sus diferencias y no pudo retener la presidencia, logrando el ascenso del edil radical en lo que fue una sesión "caliente".

Por segundo año consecutivo, el concejo contará con la conducción política de una persona que es muy importante para la estructura del intendente Jatón. Es que González forma parte de la mesa chica de las decisiones que toma el mandatario santafesino, con quien comparte no sólo la pertenencia al espacio político del Frente Progresista (aunque provengan de partidos distintos) sino además la forma de ver y expresar la política.

El hablar pausado, en un tono conciliador sin levantar la voz y con respeto político más el respeto a las instituciones, son cualidades que el intendente ve reflejadas en el militante radical.

Pero, además, González ha mostrado resultados positivos para la gestión de Jatón en un año difícil atravesado por la pandemia. Logró que se apruebe el presupuesto municipal para este año, logró apoyos para concretar la emergencia en salud por el Covid 19 y que el cuerpo apoye las ayudas económicas para distintos sectores en el contexto actual, entre otros puntos. Es decir, el acompañamiento del Concejo Municipal a la gestión del Frente Progresista ala socialista se hizo notar favorablemente, mérito que se destaca aún más si se tiene en cuenta la conformación heterogénea del cuerpo, que cuenta con cuatro bloques (FPCyS, PJ, JxC y Barrio 88) y seis sub bloques.

## **Consenso y diálogo, lo que todos destacaron**

Leandro González asumió su segunda presidencia del cuerpo con 16 votos a favor y 1 abstención. Es una tradición que el postulante al cargo no participe de la votación activamente.

Con una contundente votación a su favor y sin contrincantes, González escuchó cómo sus pares se deshicieron en elogios no solo por la forma de conducir políticamente sino también por su condición humana.

La concejala socialista Laura Mondino expresó que "proponer al concejal Leandro González para que siga al frente del concejo es reafirmar el trabajo que venimos realizando, en la búsqueda de consenso y el diálogo como forma para llegar a los acuerdos".

En tanto que el concejal radical de Juntos por el Cambio, Carlos Suarez dijo que " en su primer año en la presidencia le tocó lidiar con un cuerpo complejo políticamente hablando. Y hoy, gracias a los acuerdos alcanzados, los vecinos de Santa Fe tienen respuestas de este concejo".

En tanto que su correligionario Lucas Simoniello tuvo un discurso más personal y hasta se animó a "tutearlo", rompiendo el protocolo con el que se venía expresando. "No nos equivocamos cuando hace un año te elegimos. Quienes te conocemos sabemos lo buena gente que sos y el militante político comprometido. Te pusiste el 'concejo al hombro' y por eso hoy te reelegimos".

A su turno, el concejal del PJ Sebastian Pignata reconoció que " es una de las personas que más conoce el concejo. Se preparó mucho para llegar donde llegó y es merecido que continúe".

Para el representante de Barrio 88, Guillermo Jerez, "usted honró el valor de la palabra y la vocación democrática que tuvo este cuerpo. Méritos más que suficientes para poder reelegirlo".

## **Las vicepresidentas también reelectas**

En su primer año al frente del legislativo municipal, González estuvo acompañado en las vicepresidencias por dos mujeres de distintos espacios políticos: Luciana Ceresola del PRO y Jorgelina Mudallel del PJ, allegada al gobierno provincial. En esta ocasión, ambas también fueron elegidas nuevamente para ocupar dichos cargos: Ceresola estará en la vicepresidencia primera y Mudallel en la segunda. "El consenso fue más importante que logramos en el año y es un compromiso que debemos asumir el querer seguir trabajando de esta manera" dijo la militante justicialista.

Al final de la sesión preparatoria, Leandro González dijo que "No hay forma de conducir sino se escucha lo que tienen para decir todos los concejales. Es por eso que estamos orgullosos del trabajo que estamos haciendo".

## **El año legislativo en la ciudad**

A los proyectos enviados por el Ejecutivo municipal y votados afirmativamente, hay que sumarles otros de mucha importancia y que se lograron con mucho consenso político y debates a la altura de las circunstancias. Por ejemplo, la aprobación del uso del cannabis medicinal y la posibilidad de que se empiecen a realizar estudios en la ciudad, son un ejemplo de lo que logró la presidencia de González. Otro proyecto de relevancia aprobado fue el de balcones gastronómicos, donde se discutió con los sectores involucrados la implementación y terminó siendo aprobado. O el observatorio municipal sobre Violencia de Género que terminó resolviéndose en fecha de conmemorarse el Día Internacional de la Lucha contra la No Violencia hacia la Mujer, donde se prevé que el municipio recoja datos y realice estadísticas para tener más información sobre este flagelo.

Solo queda como proyecto importante a discutir en lo que queda del año el presupuesto económico 2021, que se estima será votado en sesiones extraordinarias y aprobado. Un poroto que le podemos ir sumando a la presidencia de Leandro González.

***

#### **Por José Villagrán**. Seguilo en [Twitter](https://twitter.com/joosevillagran "twitter")