---
layout: Noticia con imagen
author: Nota de la redacción de 3DNoticias
resumen: Transporte de larga distancia
category: La Ciudad
title: Cómo será el protocolo en la terminal de Santa Fe tras la habilitación de
  los micros de larga distancia
entradilla: Siguiendo las normativas nacionales y provinciales se puso en marcha
  la implementación de los protocolos para la apertura de la terminal. El
  detalle de las medidas.
date: 2020-11-21T12:52:29.311Z
thumbnail: https://assets.3dnoticias.com.ar/terminal-santa-fe.jpg
---
La Estación Terminal de Ómnibus “Manuel Belgrano” ultima detalles para su reapertura, luego de que se diseñaron y aprobaron los protocolos correspondientes para el retorno del servicio de transporte de pasajeros de media y larga distancia para todo el país y que en la provincia de Santa Fe se hizo efectivo a través del decreto Nº 1.440 que lleva la firma del gobernador Omar Perotti.

La norma establece en su artículo 1º que "en el desarrollo de los servicios de transporte automotor y ferroviario interurbano de pasajeros de jurisdicción nacional, deberán aplicarse los protocolos respectivamente elaborados por el Comité de Crisis Prevención Covid-19 para el Transporte Automotor y el Comité de Crisis Prevención Covid-19 para el Transporte Ferroviario, creados por la Resolución Nº 60 de fecha 13 de marzo de 2020 del Ministerio de Transporte de la Nación, los que dicte y actualice la Comisión Nacional de Regulación del Transporte (CNRT); las especificaciones complementarias del presente Decreto y los protocolos provinciales vigentes dictados de conformidad con los lineamientos y recomendaciones del Ministerio de Salud de la Provincia".

# [](<>)**El protocolo**

* ### **LOS USUARIOS**

El ingreso a la Estación será con tapabocas obligatorio y aplicación de sanitizante.

Se recomienda mantener la distancia de 2 metros con el resto de los pasajeros.

Ingresar a la Terminal 15 minutos antes a la partida de su micro, no mucho tiempo antes.

No se podrá ingresar con acompañantes ya que no se permitirá la permanencia a quienes no cuenten con pasaje adquirido.

Ingreso y egreso diferenciados.

Puertas para ingreso, por el hall central, y egreso por el ala sur, de personas al edificio.

Dársenas organizadas de modo tal que los ascensos de pasajeros se concreten entre las plataformas 1 y 17, mientras que los descensos se hagan entre la 18 y la 28.

* ### **EN EL INTERIOR**

Instalación de tótems sanitizantes con solución indicada por el Ministerio de Salud.

Limpieza exhaustiva permanente.

Control de temperatura en el ingreso al predio.

Instalación de vinilos de separación en las filas de boleterías y comercios.

Habilitación de una sala exclusiva de aislamiento para casos sospechosos de Covid-19.

Además, una posta informativa del Cobem se ubicará dentro del inmueble para recabar información y prestar asistencia en caso de que sea necesario.

* ### EN EL PREDIO

Se dispondrá de un espacio en planta baja para la Comisión Nacional de Regulación del Transporte (CNRT) con vista directa a las plataformas, a fin de mejorar la capacidad de control sobre los coches.

La guardia privada concretará un seguimiento en las dársenas para evitar la aglomeración de personas y promover el distanciamiento.

Se colocará información en todo el predio sobre prevención del Covid-19, mediante cartelería informativa y medios audiovisuales.

* ### LAS EMPRESAS DE TRANSPORTE

Deberán informar sus horarios y destinos a la administración de la Terminal por nota o correo electrónico y con 48 horas de anticipación.

Tendrán que presentar una declaración jurada en la que conste el cumplimiento de los protocolos.

Se les solicitará que tengan la nómina de los pasajeros con información personal en disponibilidad inmediata.

Llevar adelante el registro de los pasajeros, con sus datos personales, temperatura al momento del ascenso, detalles sobre el origen y el destino, y constatación del permiso habilitante para circular.

* ### LOS NEGOCIOS

Los horarios serán los mismos que regulan al comercio minorista de la capital provincial.

La apertura de las boleterías que expenden pasajes estará sujeta a la decisión de cada empresa.

Se deberá cumplir con la normativa municipal, provincial y nacional para funcionar de acuerdo a las habilitaciones otorgadas.