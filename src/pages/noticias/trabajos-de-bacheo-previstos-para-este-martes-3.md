---
category: La Ciudad
date: 2021-04-27T08:01:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/bacheos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Trabajos de bacheo previstos para este martes
title: Trabajos de bacheo previstos para este martes
entradilla: La Municipalidad informa las calles por las que avanza el cronograma de
  tareas previsto en el Plan de Bacheo 2021. Puede haber cortes de circulación y desvíos
  de colectivos a medida que se desarrolla la obra.

---
En enero pasado, la Municipalidad puso en marcha un plan de bacheo para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

En la zona del micro y macrocentro, está previsto que los trabajos se concreten en:

* 3 de Febrero, entre 1° de Mayo y 4 de Enero
* 3 de Febrero, entre Urquiza y 4 de Enero
* Suipacha, entre 1° de Mayo y 9 de Julio
* San Jerónimo, entre Lisandro de la Torre y Juan de Garay
* San Jerónimo, entre Corrientes y Moreno
* 25 de Mayo, entre Suipacha y Gobernador Crespo
* Moreno, entre 4 de Enero y 1° de Mayo
* 4 de Enero, entre Eva Perón e Irigoyen Freyre
* 4 de Enero, entre Suipacha y Crespo
* 4 de Enero, entre Obispo Gelabert y bulevar Pellegrini
* Crespo, entre 4 de Enero y Urquiza
* La Rioja, entre Francia y Saavedra
* San Jerónimo, entre La Rioja y Tucumán
* En tanto, en avenidas troncales, la obra continuará por:
* Aristóbulo del Valle y French, mano norte-sur
* Aristóbulo del Valle, entre avenida Gorriti y Los Paraísos, mano sur-norte

El municipio informa que todas las tareas se realizan siempre en sentido del tránsito. Del mismo modo, indica que puede haber cortes de circulación y desvíos de colectivos, a medida que avanza la obra y se realizan intervenciones mayores.

Además, se detalla que la realización de los trabajos está sujeta a las condiciones climáticas.

**Por el operativo de vacunación**

La Municipalidad comunica que, en el marco del operativo de vacunación que se realiza en la ciudad, habrá controles en el tránsito vehicular, de 8 a 18 horas, en las inmediaciones de:

* La Esquina Encendida, en la intersección de Facundo Zuviría y Estanislao Zeballos