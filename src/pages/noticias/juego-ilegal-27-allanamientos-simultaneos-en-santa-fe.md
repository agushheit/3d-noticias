---
category: Agenda Ciudadana
date: 2020-11-30T11:15:19Z
thumbnail: https://assets.3dnoticias.com.ar/CASINO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: MPA'
resumen: 'Juego ilegal: 27 allanamientos simultáneos en Santa Fe'
title: 'Juego ilegal: 27 allanamientos simultáneos en Santa Fe'
entradilla: e realizaron en Santa Fe; Santo Tomé; Rincón; Sauce Viejo; Gálvez; Rafaela;
  El Trébol y Cañada Rosquín. Hubo 18 detenidos, de los cuales dos son policías.

---
En el marco de una investigación por juego ilegal presencial y on line, entre los últimos minutos de ayer y toda la madrugada de hoy se realizaron 27 allanamientos simultáneos en ocho ciudades: Santa Fe; Santo Tomé; Rincón; Sauce Viejo; Gálvez; Rafaela; El Trébol y Cañada Rosquín.

  
 En total, **fueron detenidas 18 personas, de las cuales dos son policías**.

Se secuestraron diversos elementos de interés para la investigación, entre ellos, tableros y fichas de juego; dispositivos digitales de almacenamiento de información; constancias de movimientos bancarios; teléfonos celulares; libretas con anotaciones de movimientos económicos; dinero en efectivo y dos armas de fuego.

Los fiscales a cargo de la investigación son Federico Grimberg, Marcelo Nessier y Ezequiel Hernández. Los operativos se llevaron adelante con agentes de la Agencia de Investigación Criminal y del Organismo de Investigaciones del Ministerio Público de la Acusación (MPA).

**La modalidad del delito era presencial y online**.

  
 