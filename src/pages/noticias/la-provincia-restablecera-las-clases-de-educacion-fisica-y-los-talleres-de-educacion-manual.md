---
category: Estado Real
date: 2021-08-13T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/talleres.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia restablecerá las clases de educación física y los talleres de
  educación manual
title: La provincia restablecerá las clases de educación física y los talleres de
  educación manual
entradilla: La medida entrará en vigencia en la semana próxima y fue comunicada a
  las instituciones escolares mediante la Circular 21 del 12 de agosto.

---
El ministerio de Educación, en virtud de los datos emanados en los últimos informes del ministerio de Salud, en donde se observa una mejoría progresiva de los indicadores epidemiológicos en nuestra provincia, ha decidido que desde la semana próxima se habiliten las clases de Educación Física y de los Talleres de Educación Manual, así como también, la Formación Profesional y las propuestas curriculares dictadas en contra turnos.

La decisión se dispuso porque la mejora de las variables sanitarias permite una ampliación de las actividades que recuperen presencialidad, de modo que se concrete la idea de la gestión que lleva adelante el gobernador Omar Perotti, de que todos los avances respecto de los índices de la pandemia, se traduzcan en beneficios para garantizar el derecho a la educación de todos nuestros chicos y chicas.

En este contexto, la cartera educativa manifiesta que todas las iniciativas registradas en la Circular 21 deben concretarse con estricto cumplimiento de protocolos que respeten el distanciamiento social, uso de barbijos, ventilación de los espacios compartidos, lavado frecuente de manos y cuidados en ingresos, egresos y circulación, que forma parte de las prácticas que nos han garantizado durante este tiempo la vuelta a la presencialidad en el contexto sanitario que estamos atravesando.