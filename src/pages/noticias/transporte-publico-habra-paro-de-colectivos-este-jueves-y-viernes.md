---
category: Agenda Ciudadana
date: 2021-05-27T08:45:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivo1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: LT10
resumen: 'Transporte público: habrá paro de colectivos este jueves y viernes'
title: 'Transporte público: habrá paro de colectivos este jueves y viernes'
entradilla: La medida de fuerza alcanza al interior del país. Los choferes reclaman
  aumento salarial y un trato igualitario con respecto a Buenos Aires, que recibió
  4.000 millones de pesos para pagar incrementos de sueldos.

---
La Unión Tranviarios Automotor (UTA) anunció un paro de colectivos para este 27 y 28 de mayo en todo el interior del país, en reclamo de aumentos salariales. La medida de fuerza abarcará los servicios urbanos e interurbanos y tendrá vigencia desde las 0:00 del jueves y por 48 horas. 

Además de Santa Fe y Rosario, se plegarán a la huelga Córdoba, Comodoro Rivadavia, Tierra del Fuego, Río Gallegos, Tucumán, Mar del Plata, Salta, Entre Ríos, Neuquén, Jujuy, Catamarca y La Rioja, La Pampa, Formosa, San Luis, Corrientes, San Juan, Bariloche, Bahía Blanca, Chaco, Río Negro, Santiago del Estero, San Nicolás de los Arroyos y Trelew. 

En diálogo con LT10, el secretario general de la delegación local de UTA, Osvaldo Agrafogo, comentó que hoy se desarrolló una última reunión, de tantas realizadas durante cuatro meses, y que nuevamente “los empresarios no acercaron ninguna propuesta” de suba de sueldos.

Además, señaló que en el Área Metropolitana de Buenos Aires ya se firmó el acuerdo con los trabajadores, gracias a que el Gobierno nacional aportó 4.000 millones de pesos más en subsidios para que las compañías puedan afrontar los aumentos salariales. Así, actualmente el AMBA recibe 15.000 millones de pesos mensuales, y el interior en su totalidad, 1.500 millones de pesos. “Nos consideramos de segunda”, se quejó Agrafogo.

El gremialista reconoció la crisis económico- financiera del sector, agravada por la falta de recaudación desde el comienzo de la pandemia, y pidió que entre el Municipio, la Provincia y la Nación “busquen una solución” y definan “qué tipo de transporte quieren para el interior del país”.