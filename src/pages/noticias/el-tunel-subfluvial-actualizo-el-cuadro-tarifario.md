---
category: Estado Real
date: 2021-02-01T07:02:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/tunel.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: El Túnel Subfluvial actualizó el cuadro tarifario
title: El Túnel Subfluvial actualizó el cuadro tarifario
entradilla: A partir de este lunes, el paso que conecta a las provincias de Santa
  Fe y Entre Ríos actualizará las tarifas que no se modificaban desde Julio de 2019.

---
El Túnel Subfluvial Raúl Uranga - Carlos Sylvestre Begnis modificará el actual cuadro tarifario, en vigencia desde el mes de Julio de 2019, a partir de las 0 horas del lunes 1 de febrero. Esta medida resulta indispensable para garantizar el correcto mantenimiento del viaducto, su seguridad y la continuidad de las obras indispensables para ello.

Asimismo, desde el Ente del Túnel indicaron que “no recibe subsidios ni subvenciones de ninguna clase, como si ocurre con otros corredores viales”; y destacó que “se mantienen vigentes las resoluciones que otorgan el 50% de descuento para el uso frecuente de automóvil y el 20% al transporte de carga y de pasajeros de empresas con domicilio en las provincias de Santa Fe y Entre Ríos”.

**NUEVAS TARIFAS**

>> Vehículos de hasta 2 ejes y menos de 2,10m altura y ruedas simple. Motos, autos, Pick-Up, ambulancias, combis o trafics abonarán 90 pesos.

>> Vehículos de 3 y 4 ejes y menos de 2,10m de altura y ruedas simples. Autos o pick-up con remolque (lancha, casa rodante, acoplados de menos de 2,10 de altura), casas rodantes auto propulsadas, pick-up de 3 ejes, 200 pesos.

>> Vehículos de 2 ejes y más de 2,10 de altura. Camiones, Ómnibus, Maquinaria agrícola o Vial y cualquier otro tipo de vehículo con las características antes mencionadas, 360 pesos.

>> Vehículos de 3 ejes y más de 2,10m de altura. Camiones, Ómnibus, Maquinaria agrícola o Vial y cualquier otro tipo de vehículo con las características antes mencionadas, 530 pesos.

>> Vehículos de 4 ejes y más de 2,10m de altura. Camiones, Ómnibus, Maquinaria agrícola o Vial y cualquier otro tipo de vehículo con las características antes mencionadas, 580 pesos.

>> Vehículos de 5 ejes y más de 2,10m de altura. Camiones, Ómnibus, Maquinaria agrícola o Vial y cualquier otro tipo de vehículo con las características antes mencionadas, 700 pesos.

>> Vehículos de 6 ejes y más de 2,10m de altura. Camiones, Ómnibus, Maquinaria agrícola o Vial y cualquier otro tipo de vehículo con las características antes mencionadas, 750 pesos.

>> Tarifa adicional por cruce de asfalto, 3.000 pesos.

>> Tarifa adicional por cortede tránsito, 3.000 pesos.