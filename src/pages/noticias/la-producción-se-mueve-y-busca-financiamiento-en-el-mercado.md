---
layout: Noticia con imagen
author: "Fuente: La Capital"
resumen: Mercado Argentino de Valores
category: Agenda Ciudadana
title: La producción se mueve y busca financiamiento en el mercado
entradilla: El presidente del Mercado Argentino de Valores, Alberto Curado,
  contó que las pymes comenzaron a reactivarse y buscan instrumentos para
  financiarse.
date: 2020-11-15T14:57:48.477Z
thumbnail: https://assets.3dnoticias.com.ar/mercado-de-valores.jpg
---
El Mercado Argentino de Valores (MAV) ha tenido un crecimiento muy importante desde su creación y este año está jugando un papel clave en el financiamiento de las pymes, en una etapa muy difícil signada por la crisis desatada por la pandemia, sumada a la que ya se traía de arrastre. Pese a ello, “venimos con un crecimiento a lo largo de estos años de un 20% en el volumen operativo, y lo decimos en moneda dura, y para esta fecha ya llevamos negociado en lo que va del año $122 mil millones, siendo que el año pasado fueron $128 mil millones en total. Es decir que el volumen, a pesar de las inclemencias que estamos pasando, se mantiene bien, estamos operando bastante y creemos que vamos a poder seguir en la misma línea de lo que venimos haciendo”, señaló su flamante presidente, Alberto Curado.

“En un año complejo, bastante difícil para la actividad económica producto principalmente de la pandemia, tenemos la obligación y el compromiso de seguir brindando el mejor proyecto de financiación para pymes. Tuvimos que adaptarnos a los cambios de la complejidad del momento. El producto más demandado, el más operado a lo largo de este año es el e-cheq, el cheque electrónico, que se desarrolló tremendamente a partir del momento del aislamiento, del distanciamiento, de todo el proceso del Covid”, comentó el directivo.

En ese sentido, recordó que “antes la vedette era el cheque de pago diferido”, pero a partir de la emisión y la negociación del e-cheq “las pymes han desarrollado y han encontrado en este producto la principal fuente de negociación y de financiamiento, con una ventaja superlativa con respecto al resto que es la posibilidad”.

**Repunte**

Respecto del seguimiento de la actividad económica, el presidente del Mercado Argentino de Valores afirmó que “lo peor ya pasó”.

“Sabemos que este año ha sido muy difícil para la industria, para las pymes, con una recesión muy importante, pero creemos que ya hemos empezado a revertir esta curva y tenemos instrumentos en el mercado de financiamiento para que podamos ayudar a que la reversión sea mucho más notable”, indicó al tiempo que observó que en la región, “especialmente en el cordón industrial, se empieza a notar mayor avidez para poder financiarse”.

“Hay mayor avidez por acercarse al mercado de financiamiento para las pymes y eso conlleva a una mejora en la producción. Hay muchas ganas también de empezar a revertir el problema porque empezó a haber una luz allá al final del túnel que creo que en algún momento para todo el sector agroindustrial se puso mucho más complejo y no veíamos dónde estaba la salida”, relató.

Curado destacó que el MAV integra una mesa de trabajo con la Comisión Nacional de Valores (CNV), con la Secretaría de Pymes (Sepym) y el Ministerio de la Producción. “Constantemente estamos evaluando alternativas, nuevos productos, hemos presentado y estamos en discusión ahora con CNV sobre dos productos para agregar a la negociación de pagaré uno atado al valor de la soja y otro atado al índice de la Cámara de la Construcción”.

Creo que lo peor ya ha pasado y nuestro objetivo es para adentro, hacia nuestros agentes, incentivar toda la actividad hacia afuera, brindándoles herramientas tecnológicas

En ese sentido, explicó que este instrumento es ideal “para el inversor que quiera tener una rentabilidad y que estaba acostumbrado a guardar granos o a guardar en ladrillos, para que tenga la posibilidad de ingresar al mercado de capitales financiando actividad productiva y cubriendo sus espaldas tanto con la variación del precio de la soja como contra la variación del índice de la construcción”.

Por otra parte, este año el MAV participó en la emisión de letras de la Municipalidad de Rosario. Al respecto, Curado recordó el espíritu federal del mercado, y dijo que se buscan “unir las puntas, unir los cabos de alguna forma entre lo que es la financiación que necesitan todas las provincias y municipios del país, a través del lanzamiento de letras, obligaciones negociables”.

Finalmente, el máximo directivo del MAV hizo algunos adelantos respecto de cómo planea su gestión al frente del mercado. “Quienes me conocen saben que soy positivo en todos mis análisis. Creo que lo peor ya ha pasado y nuestro objetivo es para adentro, hacia nuestros agentes, incentivar toda la actividad hacia afuera, brindándoles herramientas tecnológicas de manera que tengan el mejor soporte para que las empresas vengan al mercado tanto a colocar sus excedentes como a buscar financiamiento, sabiendo que están haciéndolo de la manera más transparente posible, con objetivos de cumplimiento rápido”.