---
category: Agenda Ciudadana
date: 2021-02-12T06:00:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/atef.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La reunión paritaria con ATE y UPCN se reprogramó para el próximo miércoles
title: La reunión paritaria con ATE y UPCN se reprogramó para el próximo miércoles
entradilla: La reunión se había acordado para este viernes al mediodía en Casa de
  Gobierno. Pero  se reprogramó para la semana entrante.

---
Debido a la convocatoria que hizo el presidente de la nación, Alberto Fernández, del Consejo Federal de Educación en la residencia de Olivos, el ministro de Trabajo de la provincia, Juan Manuel Pusineri, informó que la reunión paritaria con los gremios de ATE y UPCN se reprogramó para el miércoles 17 al mediodía en Casa de Gobierno.

En un principio, y luego del primer encuentro paritario del jueves pasado, las partes habían acordado volver a reunirse este viernes.