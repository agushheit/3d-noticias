---
category: Deportes
date: 2021-01-11T10:35:19Z
thumbnail: https://assets.3dnoticias.com.ar/union-patronato.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Por Marisa Lemos para 3DNoticias
resumen: El Tate no pudo con Patronato y se quedó sin final en esta fase
title: El Tate no pudo con Patronato y se quedó sin final en esta fase
entradilla: Unión cayó 2-1 ante el equipo paranaense y logró sonreír en el final del
  torneo.

---
En una tarde calurosa santafesina, Unión esperaba el milagro de golear a Patronato, si el puntero Rosario Central perdía, pero el Patrón llegó para arruinarle la fiesta ganándole al equipo tatengue.

El encargado de abrir el marcador fue Gustavo Canto, a los 10 minutos del primer tiempo, tras un córner de Nicolás Delgadillo. Segundo, pifió un cierre luego de otro centro de Delgadillo y mandó la pelota a su propio arco, pero al minuto trece llegó el tanto de Mauro Luna Viale para igualar ventajas ante el equipo visitante. Tras un error del local, al minuto 32 llegó el gol en contra del juvenil Franco Calderón.

Cabe destacar que el Tate no salió a demoler al visitante, pero sí fue el protagonista del juego y el que más buscó, aunque con ideas un tanto ajustadas. Se acercó básicamente con desbordes y centros para los delanteros Juan García y Franco Troyansky. Y de un envío desde la derecha llegó la igualdad parcial de Mauro Luna Diale, que aprovechó en despeje corto de Oliver Benítez y cruzó rasante el balón al palo derecho.

En los minutos finales, Patronato se abrazó a la victoria que no había logrado durante todo el torneo, en el cual perdió 7 veces y empató 4. Era, hasta ayer, el único que equipo que no conocía la victoria.

En tanto, Unión de Santa Fe se quedó sin la chance de estar puntero en su zona. En parte, este resultado le servirá para plantearse ciertos aspectos que le serán útiles para un futuro no tan lejano.