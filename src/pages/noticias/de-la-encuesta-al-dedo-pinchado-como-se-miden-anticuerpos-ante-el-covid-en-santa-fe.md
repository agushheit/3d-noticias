---
category: La Ciudad
date: 2021-09-26T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/censo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: " Cómo se miden anticuerpos ante el Covid en Santa Fe"
title: Cómo se miden anticuerpos ante el Covid en Santa Fe
entradilla: Científicos locales ya analizan qué niveles de protección inmunológica
  tiene la ciudadanía, frente el coronavirus. La información podrá disgregarse por
  género y franjas etarias, entre otras variables.

---
En un novedoso estudio, científicos de varios institutos de investigación del Conicet Santa Fe, becarios y estudiantes colaboradores de la UNL comenzaron a reunir datos que servirán para determinar qué niveles de anticuerpos -el grado de protección- tiene frente al SARS-CoV-2 y a la enfermedad generada, el Covid-19, la ciudadanía de esta capital, vacunada o no.

¿Para qué servirá tener una "radiografía serológica" en la población de esta capital? Para "medir al enemigo", en este caso el virus. La información que se recabe se sistematizará, y así los datos echarán luz sobre qué franjas etarias tienen más protección que otras; si las personas no vacunadas son en efecto menos vulnerables que las que sí lo están, e incluso si las vacunas anti Covid disponibles sostienen en el tiempo su capacidad protectora.

"Esto es importante porque nos permitiría reconocer al virus y en qué medida el sistema inmunológico lo ataca. Medir anticuerpos nos posibilitará ver la forma en que el cuerpo humano reconoce algo como externo (el virus) y lo tiene que repeler porque no le es propio", le explica a El Litoral el Dr. Carlos Piña, director del CCT Conicet Santa Fe, investigador del Centro de Investigación Científica y de Transferencia Tecnológica a la Producción (CICYTTP), y uno de los que integran el grupo de investigación.

El trabajo consta de dos fases. La primera empezó este lunes 20 de septiembre e implica una exhaustiva labor territorial en la cual estudiantes visitan los 1.000 domicilios de la ciudad seleccionados al azar para la muestra del estudio. Hablan con sus residentes, les explican los alcances del estudio y les dejan un folleto sobre el cuestionario, que se responde on line y de manera totalmente confidencial: no se pide ningún dato personal ni sensible.

El folleto tiene un código (que referencia al domicilio), una dirección de email, una dirección web (ambas vías de comunicación son oficiales del Conicet) y un número de celular para que las personas accedan al cuestionario on line a responder. Nadie del grupo territorial que integra la investigación ingresa a ningún domicilio: se trabaja desde la puerta. También está la posibilidad de responder por papel.

En el cuestionario se pregunta, por ejemplo, cuántos convivientes (grupo familiar) hay en el domicilio; qué tipo de trabajo realizan quienes tienen empleo, y qué otras actividades realizan el grupo familiar; el grupo sanguíneo de cada persona; si se atuvieron o no a las restricciones sanitarias establecidas a lo largo de la pandemia; si hay personas "esenciales" (trabajadores de la salud, de seguridad, etcétera), entre muchas otras.

"Todas estas respuestas nos servirán luego para analizar si las personas encuestadas que no están vacunadas pudieron haber tenido Covid o no. Y a la gente que sí fue inoculada, se le pregunta sobre qué vacuna recibió, cuántas dosis, cuándo las recibió, para también correlacionar el nivel de anticuerpos que pudieran tener con el tipo de vacuna que le haya tocado", añade Piña.

**Pequeño pinchazo en el dedo**

A medida que se van recabando y sistematizando todos los datos de las encuestas sobre esos mil domicilios, de cada uno de éstos se podrán hacer hasta tres tomas de sangre, con un pequeño pinchazo en el dedo y nunca ingresando al domicilio, porque la muestra se puede tomar desde la puerta. Es decir, que se podría contar con un promedio de 3 mil tres dosajes de anticuerpos a analizar luego en laboratorio.

"La intención es, con las muestras de sangre, hacer una diferenciación de edades. Por ejemplo, en una casa donde convivan una pareja, un niño o niña de primaria y un adolescente en etapa universitaria, se podrá abarcar a un menor, un joven y un adulto. De esta forma, podríamos cubrir el mayor abanico de diversidad etaria posible en cada domicilio, lo cual nos posibilitaría evaluar las diferencias en el nivel de anticuerpos", describe el investigador.

Allí entran las variables a analizar: los grupos sanguíneos, el género de los convivientes, los rangos etarios, el tipo de vacuna que recibieron los que fueron inoculados, el tiempo que hace que las recibieron, si una persona no salía tanto a la vía pública (en medio de las restricciones sanitarias) y si los otros sí (por trabajo o estudio). Y en función de todo este volumen de información, se medirá los niveles de anticuerpos, disgregados en esas variables.

Es importante que la gente que recibió el folleto informativo "se ponga en contacto con los especialistas que trabajan en nuestro equipo, a través de las vías de comunicación que allí se detallan. Y que no tenga miedo en absoluto: la encuesta es totalmente confidencial, las muestras se toman desde la calle, nadie ingresa a ninguna casa. Porque para nosotros, lo importante es reunir todos los datos para avanzar en el estudio, pero primero es clave obtener toda esa información", subrayó Piña.

En breve se comenzará con las tomas de muestras de sangre, adelantó el científico. Aquí se pondrá a disposición de las personas si quieren ir al laboratorio, o bien si desean que vaya alguien del equipo a tomarle la muestra sin ingresar a su domicilio: desde la puerta, subraya.

\-Terminado el estudio, ¿es posible que el equipo de investigación logre determinar un valor porcentual, un parámetro que permita decir si la población de la ciudad de Santa Fe tiene niveles altos, medios o bajos de protección de anticuerpos contra el Covid-19?, preguntó El Litoral al Dr. Piña.

\-Si sale todo bien es muy probable que sí, y que eso se pueda informar luego. Pero el alcance de este estudio es bastante más complejo que eso. Contando con todos los datos y los dosajes analizados, podríamos decir que dentro de una determinada edad hay más anticuerpos o no, por ejemplo, que en otras; o si es que el estado de protección de una determinada vacuna decae en el tiempo o se mantiene estable en siete, ocho meses, incluso un año.

Además, se podría determinar, dentro de aquellas personas que no fueron vacunadas, qué porcentaje tuvo contacto con el virus, si tiene anticuerpos o si ya no los tiene, transcurrido un tiempo prolongado (por ejemplo, una persona que padeció de Covid en marzo o abril del año pasado).

Y de las personas vacunadas, el estudio podría arrojar qué porcentaje de esa población se considera que tiene anticuerpos y qué nivel tiene; cuándo crece el nivel, cuándo se estabiliza… Es más complejo que decir: "Estamos bien, regular o mal" como sociedad santafesina respecto de la protección frente al virus.

**EQUIPO DE INVESTIGACIÓN**

El proyecto "Estudios serológicos para detectar anticuerpos contra SARS-CoV-2 en la ciudad de Santa Fe: estimando la magnitud del contagio y patrones de propagación" (convocatoria Demes) es financiado por la Agencia Santafesina de Ciencia Tecnología e Innovación de la provincia de Santa Fe, mediante el programa "Demandas estratégicas en el marco de la emergencia sanitaria".

De la investigación colaborativa (que es dirigida por el Dr. Pablo Beldoménico) participan más de 60 investigadores, docentes, profesionales y becarios del Centro de Investigación Científica y de Transferencia Tecnológica a la Producción (CICYTTP, CONICET-Prov. ER-UADER); del Instituto de Ciencias Veterinarias del Litoral (ICIVET Litoral, CONICET-UNL); del Instituto de Salud y Ambiente del Litoral ISAL (UNL-CONICET).

También, del Instituto de Matemática Aplicada del Litoral (IMAL, CONICET-UNL); del Instituto de Estudios Sociales (INES, CONICET-UNER); de la Facultad de Ciencias Veterinarias (FCV-UNL); de la Facultad de Bioquímica y Ciencias Biológicas (FBCB-UNL), y del Instituto Nacional de Enfermedades Respiratorias Dr. Emilio Coni. También se cuenta con el apoyo de la Municipalidad local y colaboran como voluntarios estudiantes avanzados de la Facultad de Bioquímica y Ciencias Biológicas de la UNL.