---
category: Agenda Ciudadana
date: 2021-02-19T07:52:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/0800.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: "¿Qué reclaman los santafesinos?"
title: "¿Qué reclaman los santafesinos?"
entradilla: Un análisis del Sistema de Atención Ciudadana Municipal arroja que más
  del 50% de los reclamos son por temas de alumbrado público e higiene.

---
Como parte del proceso de reforma y modernización del Estado, la Municipalidad de Santa Fe creó en 2008 el Sistema de Atención Ciudadana en 2008. Desde entonces sumó canales de comunicación y amplió su presencia en la vida cotidiana de los santafesinos. Así se integró en un mismo servicio la atención presencial, telefónica y digital; y de esta manera comenzó a funcionar el 08007775000, una línea gratuita las 24 horas los 365 días del año, que surgió con el fin de agilizar las gestiones y orientar a la ciudadanía. Desde la puesta en función hasta el mes de noviembre de 2019 hubo más de 540.000 solicitudes gestionadas.

Durante el primer año de la gestión de Emilio Jatón, más de la mitad de los reclamos recibidos por el municipio fueron por iluminación e higiene ambiental. A las claras está la falta de iluminación, los pastos crecidos y la basura acumulada o sin recolectar en la zona del centro y los bulevares como para inferir que en los barrios la situación es igual o peor.

En el periodo comprendido entre diciembre de 2019 a noviembre 2020, el total de reclamos ascendió a 64246 y según se informa desde el Área de Atención Ciudadana, Comunicación y Relaciones Comunitarias, perteneciente a la Dirección de Derechos y Vinculación Ciudadana de la Municipalidad de la Ciudad de Santa Fe, esa cantidad representa “un 12% menos con relación al mismo periodo del año anterior”, unos 9000 reclamos menos. ¿Esto significa que hay menos “problemas” para reclamar? No, claramente hay mucho por hacer y poco presupuesto, como siempre.

El vecino estaba acostumbrado a que llamaba al 0800, o se comunicaba mediante redes sociales, realizaba su reclamo y el municipio en un tiempo prudencial respondía. De diciembre de 2018 a noviembre de 2019 se resolvieron 50103 reclamos de los 73337 recibidos.

Esta vez, cuando consultamos el número de reclamos solucionados, el municipio responde que “no es posible proporcionar un número certero, debido a la magnitud, variedad y complejidad de los registros como de los temas abordados, los que se diversificaron aún más en el contexto de la pandemia por Covid 19…” ¿Cómo puede ser que anteriormente tenían forma de contabilizar los reclamos resueltos y ahora “es muy complejo”? En fin.

De acuerdo con la poca información que pudimos obtener, los distritos con mayor cantidad de reclamos son los distritos Centro y Este, dos zonas que se caracterizan por contener una población de clase media, media/alta. ¿Por qué hacemos esta aclaración? Porque quizás los recursos con los que cuentan hacen posible la comunicación para realizar el reclamo. Ahora bien, existiendo barrios con tantas necesidades de obras de infraestructura y necesidades básicas, ¿no reclaman? Al menos raro suena, ¿no?

![](https://assets.3dnoticias.com.ar/distritoss.png)

Las vecinales están cansadas de reclamar, los vecinos de los barrios están cansados de intentar buscar respuestas a sus problemáticas, pero en muchos casos el cansancio los venció y dejaron de pedir soluciones. Porque no llegan, porque no responden los mensajes, porque no contestan el teléfono, porque no resuelven las faltas. La pandemia agudizó las necesidades y retrasó todos los trabajos que debían llevarse adelante en la ciudad. Se entiende las limitaciones por la falta de personal, pero las declaraciones de vecinos que no paran de quejarse siguen llegando: “hace meses reclamamos por las luces y nos dicen que no tienen presupuesto”, “hace semanas que no pasa el recolector de residuos por mi barrio”, “no limpian las cunetas ni los desagües y cuando caen dos gotas nos inundamos y antes no era así”.

[![Reclamo1](https://assets.3dnoticias.com.ar/19-02-21-01.webp)](https://www.facebook.com/Vecinal.Nueva.Santa.Fe/posts/2960269400859469 "Abrir en Facebook")

[![Reclamo2](https://assets.3dnoticias.com.ar/19-02-21-02.webp)](https://www.instagram.com/p/CLF0p8sHEkK/ "Abrir en Instagram")

![](https://assets.3dnoticias.com.ar/19-02-21-03.webp)

***

## **Distintos canales**

El sistema de Atención Ciudadana (SAC) comprende distintos canales de comunicación en los cuales los ciudadanos pueden hacer reclamos, denuncias, quejas y consultas en relación con lo que sucede en la ciudad, así como recibir asistencia para la realización de trámites relacionados con el municipio.

Se pueden hacer gestiones por la línea gratuita 08007775000, por correo electrónico a través de [informes@santafeciudad.gov.ar](mailto:informes@santafeciudad.gov.ar) y [reclamos@santafeciudad.gov.ar](mailto:reclamos@santafeciudad.gov.ar). Además, por atención presencial en el Palacio Municipal y las oficinas de distrito; por redes sociales, por formularios web y web chat; pedir turnos de licencia de conducir; sacar claves de Seom, entre otros.

El principal objetivo de esta política pública era estar lo más cerca posible y escuchar los reclamos y organizar a partir de eso las actividades pensando en el vecino, que es el sentido único de todos los servicios públicos del municipio.

Hoy, ¿los vecinos se sienten escuchados? ¿Ustedes que opinan?