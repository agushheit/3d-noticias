---
category: La Ciudad
date: 2022-01-15T06:00:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Colectivos en tensa negociación: la suba del boleto no se define hasta el
  "lunes o martes"'
title: 'Colectivos en tensa negociación: la suba del boleto no se define hasta el
  "lunes o martes"'
entradilla: 'El municipio decidió tomarse más días para seguir analizando qué aumento
  le autorizará a los empresarios del transporte, que pidieron en diciembre llevar
  el boleto con SUBE a $78,36: hoy cuesta $42,35. '

---
Cuanto todo indicaba que este viernes iba a conocerse el nuevo valor del boleto del transporte de pasajeros por colectivos en la ciudad, el municipio -que debe autorizar qué incremento se autorizará a los empresarios del sector, que solicitaron llevar la tarifa plana con SUBE hasta la friolera suma de 78,36, pesos, un 86% más que los 42,35 pesos actuales-, el municipio adelantó a El Litoral que la decisión se tomará en los próximos días. “Lunes o martes… Faltan elementos para tomar la decisión”, dijeron desde el Palacio Municipal.

 En contexto, en la nota presentada el 2 de diciembre, los empresarios de las tres firmas que operan en esta capital -Autobuses Santa Fe SRL, Recreo SRL y Ersa Urbano S.A.- detallaron la estructura de costos actual y explicaron por qué ahora solicitan un aumento tan elevado. El último incremento se produjo hace un año, en enero de 2021.

 “Nos queremos tomar unos días más para seguir evaluando los números y la estructura de costos antes de definir qué monto se autoriza. Ya se recibió del órgano de control el dictamen favorable para que el municipio puede redeterminar la tarifa. Pero queremos tomar una decisión responsable”, le dijo a este diario Andrea Zorzón, directora de Movilidad del municipio.

 Con todo, la negociación se tensa y seguirá la incógnita del nuevo valor hasta la semana que viene. Hoy, como adelantó El Litoral, hubo otra reunión con referentes de las empresas: “Ellos están planteando un pedido de aumento por encima del 85%, pero tenemos que analizar muy bien los cálculos de costos y los subsidios”. Sobre este último punto “aún no tenemos el esquema de reparto de subsidios para el interior del país, pero Amba sí lo tiene ya. La capital sigue siendo discriminada por Nación”, fustigó la funcionaria.

 El principal objetivo del gobierno local es, según Zorzón, mejorar la calidad del servicio de colectivos. “Durante la pandemia se vio afectado (el subsistema); eso repercutió en la cantidad de pasajeros, y hoy estamos en la mitad de las transacciones anuales (boletos abonados) respecto de 2019”, es decir, la prepandemia. Habló de mejorar el estado general de la flota y sumar más colectivos activos, lo que repercutiría en una mayor frecuencia de líneas.

 Por último, El Litoral le preguntó a la directora Movilidad si hay un parámetro de aumento tentativo a otorgar desde el municipio. No quiso arriesgar cifras: “No está clara la decisión todavía, no quiero dar una información anticipada. El contexto, tomando todos los factores, es muy incierto”, cerró Zorzón.