---
category: La Ciudad
date: 2021-03-30T07:33:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/santoto.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santo Tomé
resumen: Cortes de tránsito por obras frente a la parada de micros en Av. 7 de Marzo
title: Cortes de tránsito por obras frente a la parada de micros en Av. 7 de Marzo
entradilla: Son tres las interrupciones que se producen en la tarde de este lunes
  para poder realizar trabajos de pavimentación en cercanías del punto de parada de
  los colectivos de media y larga distancia.

---
La Municipalidad de Santo Tomé informó que esta tarde se realizan cortes de tránsito por obras de pavimento frente al parador de colectivos de media y larga distancia ubicado en la avenida 7 de Marzo. 

Las interrupciones se producen desde las 16:00 en las intersecciones de Centenario y Av. 7 de Marzo, Av. Luján y 25 de Mayo e Irigoyen y Pasaje Hernández. 

Las tareas de que se efectúan son de hormigonado de la plataforma en el espacio de detención de los micros. El objetivo es mejorar la transitabilidad y consolidar la infraestructura vial en un sector crítico de la calzada debido al desgaste que generan los vehículos de gran porte que estacionan y transitan a diario. Los arreglos se suman al bacheo ejecutado en el mes de febrero sobre la avenida, desde el Puente Carretero hasta su intersección con Av. Luján.

Por este motivo, se solicita a los conductores de vehículos circular con precaución y respetar las indicaciones de los inspectores municipales.