---
category: Agenda Ciudadana
date: 2020-12-05T11:55:02Z
thumbnail: https://assets.3dnoticias.com.ar/EPE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'EPE: construcción y remodelación de 7.300 metros de líneas aéreas de baja
  tensión en Llambi Campbell'
title: 'EPE: construcción y remodelación de 7.300 metros de líneas aéreas de baja
  tensión en Llambi Campbell'
entradilla: Se presentaron 3 ofertas para la renovación de casi 5 kilómetros de redes
  de baja tensión, con un presupuesto que supera los $ 10 millones.

---
El Directorio de la Empresa Provincial de la Energía abrió este viernes en su sede central de la ciudad de Santa Fe, en formato virtual, los sobres correspondientes a la licitación pública 3501, para la construcción y remodelación de 7.300 metros de líneas aéreas de baja tensión en la localidad de Llambi Campbell, departamento La Capital, con un presupuesto oficial de $ 10.479.909,94.

Del acto participaron el Presidente de la EPE, Mauricio Caussi, el senador provincial por el departamento La Capital, Marcos Castello, el Presidente Comunal de Llambi Campbell, Adrián Tagliari y representantes de las empresas cotizantes.

Se trata de la remodelación de 4.900 metros de líneas convencionales de baja tensión por nuevos cables preensamblados y la construcción de 2.400 metros de ese tendido, para una mejor calidad del servicio.

En la oportunidad presentaron ofertas las firmas SEI Servicios SA por $ 10.531.667,44, Bauza Ingeniería, $ 15.624.473,11 y MEM Ingeniería SA, $ 13.115.025,51, cotizaciones que serán evaluadas por una comisión intergerencial, que sugerirá la empresa adjudicataria de los trabajos.

El titular de la EPE, Mauricio Caussi, señaló que “el objetivo de esta obra, es mejorar el abastecimiento de electricidad, incrementando la confiabilidad del sistema de energía en esta zona del departamento La Capital y acotó que las tareas tienen un plazo de ejecución de 7 meses”.

Por su parte, el senador por el departamento La Capital, Marcos Castelló, destacó “el esfuerzo que está realizando el Gobierno Provincial, a través de la EPE, para concretar obras que son de suma importancia para la calidad de vida de los vecinos de Llambi Campbell”. Señaló que “la pandemia no es excusa para continuar trabajando para concretar los proyectos en realidades concretas”.

Asimismo, el presidente Comunal de Llambi Campbell, Adrián Tagliari, dijo que “es una gran noticia para nuestra localidad. Terminamos un año complejo con la gestión de esta obra eléctrica, que significa mejor servicio y mayor seguridad para nuestros vecinos”.

## **Fondo de electrificación rural**

La Ley Nº 13.414 creó el Fondo de Electrificación Rural (FER) con el objeto de dotar del servicio eléctrico al sector rural, en particular de las regiones y pobladores más aislados del territorio santafesino, “que sean declaradas de electrificación rural obligatoria y en las que exista un estado de necesidad de infraestructura eléctrica”, sean pobladores rurales con o sin explotaciones agropecuarias, plantas industriales radicadas en la zona rural y centros urbanos con menos de 3000 habitantes.