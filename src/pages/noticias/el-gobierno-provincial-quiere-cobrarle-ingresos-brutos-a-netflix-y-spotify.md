---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: Pagarán Ingresos Brutos
category: Agenda Ciudadana
title: El gobierno provincial quiere cobrarle Ingresos Brutos a Netflix y Spotify
entradilla: A comienzos de esta semana, el Poder Ejecutivo ingresó al Senado un
  proyecto de ley tributaria que impulsa, entre otras modificaciones, el cobro
  de Ingresos Brutos a las plataformas de contenidos como Netflix y Spotify.
date: 2020-11-11T22:05:08.442Z
thumbnail: https://assets.3dnoticias.com.ar/spotify.jpg
---
Así lo contempla el proyecto ingresado a la Legislatura por el Ejecutivo. "Es justo, es lo que corresponde. No podemos gravar con Impuesto Inmobiliario a un vecino y no a una plataforma multinacional como Netflix", justificó el titular de API.

En diálogo con LT10, el titular de API, Martín Ávalos, explicó que la "gravabilidad" de esos servicios está establecida en el Código Fiscal desde 2016, por lo que esta iniciativa de la gestión Perotti llega para "modernizar y ampliar la normativa a todo lo que tiene que ver con la economía digital".

De todas formas, aclaró que la intención es que no sean los usuarios sino las prestadoras las que paguen el tributo.

"Así como hay sectores que están muy afectados por la situación de pandemia, hay otros que han triplicado, cuadruplicado o sextuplicado sus ingresos y que manifiestan abiertamente una capacidad contributiva que corresponde que sea gravada. Es justo, es lo que corresponde. No podemos gravar con Impuesto Inmobiliario a un vecino y no gravar una plataforma multinacional como Netflix", argumentó Ávalos.

Sobre el mecanismo establecido para poder cobrar el tributo -considerando que son empresas que no tienen siquiera domicilio fiscal en Argentina-, el titular de API explicó que serán las administradoras de las tarjetas de crédito y otros medios electrónicos de pago los que funcionen como agentes de retención.

"El usuario no va a pagar el tributo, sino que la administradora de la tarjeta, cuando le remitan el dinero, le retendrá el tributo para destinarlo a la Provincia de Santa Fe", aseguró.