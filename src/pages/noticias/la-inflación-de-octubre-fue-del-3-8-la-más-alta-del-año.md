---
layout: Noticia con imagen
author: "Fuente: LT10 / NA"
resumen: Inflación de Octubre
category: Agenda Ciudadana
title: La inflación de octubre fue del 3,8%, la más alta del año
entradilla: De esta manera, los precios al consumidor acumulan una suba
  interanual del 37,2%, informó este jueves el organismo.
date: 2020-11-13T16:59:27.105Z
thumbnail: https://assets.3dnoticias.com.ar/inflacion.jpg
---
La inflación de octubre escaló al 3,8%, el nivel más elevado del año, informó este jueves el Indec.

En comparación con igual mes de 2019, el Índice de Precios al Consumidor (IPC) acumuló un incremento del 37,2%, muy inferior al 50,5% registrado en octubre de 2019, mientras que en lo que va del año registró una suba del 26,9%.

Tal como habían anticipado en los últimos días las consultoras privadas, la inflación se aceleró en octubre y superó al 3,3% de marzo.

\-Aumentos en productos Estacionales y al incremento autorizado en productos incluidos dentro del programa Precios Máximos

\-Subas autorizadas en combustibles, y algunos servicios en el interior del país impulsando un incremento en Regulados (1,5% mensual).

\-También se atribuye al comportamiento de Alimentos y Bebidas, que subió 4,8% mensual debido al incremento autorizado en los productos de Precios Máximos por la secretaría de Comercio. Frutas y Verduras fueron los segmentos con mayor variación.

\- Las prendas de vestir y calzado experimentaron una suba de 6,2% mensual.

\- Asimismo, se registraron aumentos en Equipamiento y Mantenimiento del Hogar (4,5% mensual) y en Transporte (4,2% mensual.) por el incremento autorizado en combustibles en octubre, más el arrastre de la suba autorizada en septiembre, sumado a algunos incrementos en el transporte público en algunas ciudades del interior.

![](https://assets.3dnoticias.com.ar/tweet-indec.jpg "INDEC")

Ver en [Twitter](https://twitter.com/INDECArgentina/status/1326963036909920256?)