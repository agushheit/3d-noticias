---
category: Agenda Ciudadana
date: 2021-02-02T06:54:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Arrepentidos: las empresas de larga distancia seguirán recibiendo subsidios
  nacionales'
title: 'Arrepentidos: las empresas de larga distancia seguirán recibiendo subsidios
  nacionales'
entradilla: No habrá cambios “al menos por los próximos meses” en el esquema del Fondo
  Compensador, dijo Osvaldo Miatello La decisión se tomó tras una reunión virtual
  con el Ministro de Transporte nacional, Mario Meoni.

---
A pocos días de conocida la nueva reglamentación nacional del Fondo Compensador 2021 para los servicios de transporte de pasajeros por colectivos de todo el interior del país -donde se excluía del reparto de subsidios a las empresas interurbanas con recorridos de más de 60 kilómetros-, el secretario de Transporte provincial, Osvaldo Miatello, confirmó que "las empresas de larga distancia seguirán percibiendo los subsidios nacionales".

La decisión surgió tras una reunión virtual que el secretario santafesino, al igual a otros funcionarios de las áreas de transporte del resto de las provincias, mantuvo con el Ministro de Transporte nacional, Mario Meoni. Transcurrido ese encuentro se resolvió que la distribución de subsidios seguirán "como se estaba liquidando a diciembre de 2020; es decir, que las empresas interurbanas de larga distancia con recorridos de más de 60 kilómetros seguirán percibiendo subsidios nacionales".

Esta medida no quiere decir que en un futuro no haya cambios, aclaró Miatello. "Pero hoy y al menos por los próximos meses, se seguirán recibiendo los fondos". Más adelante, autoridades jurisdiccionales mantendrán nuevos encuentros con funcionarios de Nación que integran la cartera de Transporte, donde se intercambiará información y se revisarán criterios.

"Pero lo importante era parar esto ahora, para que las empresas interurbanas puedan seguir recibiendo los subsidios. Porque de lo contrario (si se quitaban los fondos), iba a ser muy difícil mantener el servicio de larga distancia", valoró el secretario. 