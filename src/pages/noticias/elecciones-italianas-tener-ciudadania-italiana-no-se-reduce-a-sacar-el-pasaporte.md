---
category: La Ciudad
date: 2021-10-27T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/flor.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Elecciones italianas: “Tener ciudadanía italiana no se reduce a sacar el
  pasaporte”'
title: 'Elecciones italianas: “Tener ciudadanía italiana no se reduce a sacar el pasaporte”'
entradilla: 'Entre el 15 de noviembre y 3 de diciembre, los ciudadanos italianos que
  viven en la provincia de Santa Fe deberán votar, vía correspondencia, en las elecciones
  del Comité de los Italianos en el Exterior (Com.It.Es). '

---
_Entre el 15 de noviembre y 3 de diciembre, los ciudadanos italianos que viven en la provincia de Santa Fe deberán votar, vía correspondencia, en las elecciones del Comité de los Italianos en el Exterior (Com.It.Es)..Desde el Consulado General de Rosario alientan a la participación._

Los ciudadanos italianos nativos o con doble ciudadanía podrán participar, entre el 15 de noviembre y 3 de diciembre vía correspondencia, en las elecciones del Comité de los Italianos en el Exterior (Com.It.Es). Desde el Consulado General de Rosario estiman que son 4 mil en toda la provincia e instan a la participación activa.

Se trata de un organismo de representación de la colectividad antes las autoridades consulares que trabajan para la integración de la comunidad italiana y la difusión de la cultura del país europeo a través de distintas actividades. De los comicios pueden participar mayores de 18 años que hayan solicitado votar antes del 3 de noviembre, que se encuentren inscriptos en el AIRE (Anagrafe degli Italiani Residenti all’Estero) y que sean residentes en la circunscripción consular en fecha anterior al 3 de junio.

"La principal función de los representantes es fomentar la cultura y la lengua italiana en la región que son elegibles", explicó a 3D Noticias Florencia Morere, Vicepresidenta de la Asociación Lombarda de Santa Fe y postulante por la lista MIRES (Movimiento de Italianos Residentes en el Exterior Solidarios).

En esta ocasión, las nóminas de candidatos lograron ser "más federales", ya que integran a personalidades de todo el NEA a diferencia de años anteriores en que los elegibles se concentraban en Rosario. "Tener referentes locales nos permitirá conseguir, a través del Consulado General de Rosario, actividades educativas y culturales que difundan la cultura italiana en Corrientes, un hecho que hasta ahora estaba más reservado para la ciudad santafesina", explicó Morere.

Las listas no tienen afiliaciones políticas o partidarias, sino que se trata de simples ciudadanos con intenciones de extender las costumbres de la comunidad en el país que habitan.

De la circunscripción consular de Rosario dependen el Norte de Buenos Aires, Santa Fe, Entre Ríos, Chaco, Formosa, Misiones y Corrientes. Com.It.Es también brinda indicaciones sobre el sistema previsional, sobre cómo recibir asistencia social/médica o cómo efectuar ciertos trámites consulares. Y también fomenta intercambios recreativos y deportivos.

**¿Cómo obtener más detalles?**

Los italo-argentinos que residen en Santa Fe pueden comunicarse a través de las redes sociales del Mires Argentina o por teléfono al 0342 - 154771015 , para conocer más sobre los comicios. " Tener la ciudadanía italiana no se reduce a sacar el pasaporte. Tenemos obligaciones como italianos así como argentinos; pero también tenemos muchos derechos y posibilidades y desde el Mires queremos que se aprovechen por eso es tan importante que nos involucremos.” instó Morere.

Los interesados que vivan en localidades del interior pueden dirigirse a la página web del Consulado General de Rosario (consrosario.esteri.it/consolato_rosario/es/) para conocer más detalles del trámite necesario para participar. Morere reiteró que existe tiempo hasta el 3 de noviembre para remitir el formulario con datos personales. Luego, a partir del 13 de noviembre, recibirán en su casa boletas y el certificado electoral.

El formulario se completa con los datos, se firma y se adjunta una fotocopia del DNI. Puede ser enviado por e-mail, fax, correo o entregado personalmente en el consulado.

La comunidad italiana en Argentina es una de las congregaciones extranjeras más representativas del país desde el siglo XIX . Según estimaciones, aproximadamente el 50% de la población, unos 27 millones, es de origen italiano.

**La historia de los tanos**

La oleada de los inmigrantes comenzó en 1870, una tendencia que siguió hasta la década de 1960. En 1887, los italianos representaron el 60,4% de toda la inmigración, disminuyendo luego con el aumento porcentual de españoles.

El efecto de la mudanza de los italianos al nuevo país fue fundamental para el establecimiento de la sociedad argentina.

Hay influencias de la cultura italiana que son evidentes hasta en la actualidad. Argentina es la nación, fuera de Italia, con mayor porcentaje de italianos.

Aunque se estima que Brasil posee unos 33 millones de descendientes, Argentina fue el país latinoamericano que más recibió inmigrantes italianos, y el segundo de América después de Estados Unidos. Se estima que entre 1870 y 1970 arribaron 2,9 millones de italianos.