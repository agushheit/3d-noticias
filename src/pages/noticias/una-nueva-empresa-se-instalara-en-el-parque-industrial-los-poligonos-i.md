---
category: La Ciudad
date: 2021-07-31T06:00:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/poligono.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Una nueva empresa se instalará en el Parque Industrial Los Polígonos I
title: Una nueva empresa se instalará en el Parque Industrial Los Polígonos I
entradilla: 'El intendente Jatón le entregó el boleto de compra-venta de su lote a
  la firma López Ingeniería. La Municipalidad invertirá el monto obtenido por la venta,
  en mejoras de infraestructura para ese parque. '

---
Este jueves, el intendente Emilio Jatón llegó hasta Los Polígonos I donde hizo entrega del boleto de compra-venta de un lote a la empresa López Ingeniería para Lácteos S.R.L. La firma, con una trayectoria de 50 años en el mercado, tiene sus instalaciones en barrio Guadalupe de la capital provincial y a partir de ahora comenzará a construir su sede en el Parque Industrial.

La fracción de terreno, individualizada como unidad funcional U17, tiene una superficie de 1500 metros cuadrados. La parcela será destinada exclusivamente a la instalación de una planta industrial, dedicada a la fabricación de equipamiento para productos lácteos, de recubrimientos y tratamientos antihongos para la maduración de quesos, de agentes gelificantes y de aditivos.

Recibieron el boleto, dos de los cinco socios gerentes de la compañía, Marcelo Damianovich y María Lucrecia Ramella. A partir de ahora, iniciarán los trabajos tendientes a ejecutar y poner en marcha el proyecto de radicación durante los próximos seis meses, abonar el precio convenido en la ordenanza correspondiente y obtener la habilitación municipal para su funcionamiento. De este modo, se estima que, en un año y medio, comenzaría a producir en el Parque Industrial.

La Municipalidad, por su parte, invertirá el monto obtenido por la venta, en mejoras de infraestructura para Los Polígonos I. En ese sentido se indicó que se proyecta avanzar con obras de apertura de calles, movimiento de suelo, ejecución de cordones cunetas y estabilizado granular entre cordones.

Del mismo modo, se realizarán desagües pluviales, bocas de tormenta con sus conexiones a través de caños de hormigón prefabricados y evacuación de los excedentes pluviales hacia el reservorio. Además, se prevé la iluminación en la calle, con la colocación estratégica de un gabinete de comando y medición para el encendido y apagado del sistema, y la colocación de 11 columnas de alumbrado público.

**El último lote**

El secretario de Producción y Desarrollo Económico de la municipalidad, Matías Schmüth, aseguró que la entrega del boleto de compra-venta “nos pone muy contentos porque se está adjudicando un lote a una empresa santafesina con 50 años de trayectoria, que apuesta al crecimiento”. En ese sentido, mencionó que “en el marco de una situación en la que estamos acostumbrados, por la realidad económica y la pandemia, a dar noticias desalentadoras, esta es una que nos alienta y mucho porque en materia económica estamos hablando de una pyme con 20 empleados de forma directa e indirecta, que va a trasladar su planta a este Parque Industrial”.

Por otra parte, mencionó que la inversión realizada por López Ingeniería para la compra del lote “va a ser reinvertida en el parque y vamos a satisfacer demandas en materia de infraestructura, que tienen que ver con la construcción de cordón-cuneta y nueva iluminación. Esto se suma a trabajos que ya concretamos, como mejoras hídricas y ampliación del ingreso para los vehículos de gran porte”.

En cuanto a la compra-venta de terrenos en el Parque, el secretario mencionó que uno de los pedidos del intendente Emilio Jatón es acompañar a las empresas interesadas en adquirir terrenos, las cuales deben atravesar un proceso antes de radicarse. En ese sentido, una comisión evaluadora compuesta por la Unión Industrial, el consorcio del Parque Industrial y las universidades con sede en la ciudad, evalúan el proyecto presentado por la firma.

“López Ingeniería presentó un proyecto de ampliación que fue evaluado por la comisión, recibió el visto bueno y se pudo generar la operación de venta del terreno”, describió Schmüth. No obstante, recordó que toda la normativa está plasmada en la ordenanza correspondiente, que establece incluso, el precio de venta de los lotes.

Finalmente, mencionó que la entregada este jueves es la última parcela disponible en Los Polígonos I. Por lo tanto, la Municipalidad encabeza las gestiones para lograr la habilitación provincial de Los Polígonos II, ubicado a unos 300 metros, y consolidar el área industrial y productiva de la capital santafesina.

**La industria**

Por su parte, Marcelo Damianovich, uno de los socios gerentes de la firma, detalló que se dedican a la fabricación de ingredientes para la industria láctea y de la alimentación. Según describió, “nació hace 50 años como ingeniería para lácteos y con el tiempo se fue transformando. Hoy somos productores de ingredientes para la industria láctea y alimenticia en general, y distribuidores de envases para la industria láctea”.

En un principio, la empresa instalará parte de la producción en el Parque Industrial, para radicarse luego por completo. “La idea de instalarnos acá surgió por un crecimiento en la producción que estamos experimentando, con base en algunos proyectos nuevos que tenemos a futuro. Actualmente estamos instalados en un lugar muy céntrico y el Parque Industrial nos pareció una opción excelente”, contó.

Del acto también participaron el presidente de la Unión Industrial de Santa Fe, Aleandro Taborda; y el presidente del consorcio del Parque Industrial Los Polígonos I, Carlos Ingino.