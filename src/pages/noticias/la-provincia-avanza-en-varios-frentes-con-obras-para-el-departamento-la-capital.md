---
category: Estado Real
date: 2021-02-15T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/obrascapital.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia Avanza En Varios Frentes Con Obras Para El Departamento La Capital
title: La Provincia Avanza En Varios Frentes Con Obras Para El Departamento La Capital
entradilla: La secretaria de Obras Públicas, Leticia Battaglia, realizó una recorrida,
  junto a funcionarios de su gabinete para observar el avance de las obras en la ciudad
  de Santa Fe.

---
La secretaria de Arquitectura y Obras Públicas, Leticia Battaglia, junto a autoridades provinciales y locales, recorrió las obras en marcha que avanzan en la ciudad de Santa Fe, se trata de: la Escuela Provincial de Artes Visuales "Prof. Juan Mantovani", el Centro de Acción Infantil "La Juntada" y, el Centro de Salud Mendoza Oeste.

Durante la recorrida, Battaglia afirmó que “con el objetivo de impulsar la reactivación económica en la provincia y de concluir las obras tan necesarias para los ciudadanos, el gobernador Perotti nos encomendó avanzar, no solo con la infraestructura en salud, sino también con aquellas que tiene que ver con educación, desarrollo social y seguridad”.

“La transformación de este edificio de la escuela de arte es total, va a significar un gran impacto para la comunidad educativa, y va a permitir aumentar el cupo de ingresantes, restaurando espacios y logrando una armonía con el edificio histórico. Esta obra ya tiene un avance superior al 80 por ciento”, graficó la funcionaria provincial.

Más adelante, se refirió al Centro de Acción Infantil "La Juntada" y al Centro de Salud Mendoza Oeste, que presentan un avance superior al 55% y 34% respectivamente: "Con el edificio de ‘La Juntada’ esperamos que para junio esté totalmente terminado, obviamente siempre dependiendo de esta situación de pandemia. Es un edificio que data de los años 50' y nunca había sido remodelado, por eso la importancia del avance de esta obra y que los vecinos puedan volver a disfrutarlo”, sostuvo Battaglia.

Finalmente, recorrieron el Centro de Salud, “una obra muy necesitada para el barrio ya que atiende más de 40 mil consultas. El proyecto contempla la construcción de ocho consultorios, un sector administrativo, farmacia y office para el personal", detalló la funcionaria del Ministerio de Infraestructura, Servicios Públicos y Hábitat.

Finalmente, la secretaria mencionó las obras que se están desarrollando en la ciudad de Santa Fe, como los edificios que pertenecen a la Administración Pública, la imprenta oficial y la secretaría de Tecnologías para la gestión, en los cuales la desfuncionalización permitirá un mejor uso de los espacios para todos los trabajadores.

**Presentes**

De la actividad, participaron el subsecretario de Planificación, Omar Romero; el secretario privado, Andrés Dentesano; y los concejales locales, Juan José Saleme, Jorgelina Mudallel, y Federico Fulini.