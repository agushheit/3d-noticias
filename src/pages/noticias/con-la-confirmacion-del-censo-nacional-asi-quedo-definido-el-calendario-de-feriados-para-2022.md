---
category: Agenda Ciudadana
date: 2022-01-30T10:06:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/cac.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Cámara Argentina de Comercio
resumen: Con la confirmación del censo nacional, así quedó definido el calendario
  de feriados para 2022
title: Con la confirmación del censo nacional, así quedó definido el calendario de
  feriados para 2022
entradilla: El relevamiento de hogares será el miércoles 18 de mayo y para ese día
  regirán las mismas normas que en el descanso dominical.

---
El Gobierno hizo oficial la realización del censo nacional de población, hogares y vivienda de la ronda 2020 y lo fijó para el miércoles 18 de mayo. Asimismo, estableció que esa fecha sea feriado nacional, por lo que regirán las mismas normas legales de descanso dominical. Es decir, que el empleado que preste tareas durante el feriado, percibirá el doble de una jornada habitual, a diferencia de los considerados “días no laborables”, en los que se trabaja según el criterio del empleador, y de hacerlo, no se percibe el doble.

Por otro lado, se informó que el censo podrá ser presencial durante la jornada establecida o de forma digital desde el 16 de marzo y quienes opten por completar el formulario online podrán responder, por vivienda, las 56 preguntas sin entrar en contacto con un censor. 

Calendario de feriados

Febrero tendrá como único feriado inamovible el lunes 28 por carnaval, misma razón para el 1º de marzo. Ese mes también tendrá al jueves 24 como feriado por motivo del Día Nacional de la Memoria, la Verdad y la Justicia. En el caso de abril, serán feriados los días sábado 2, por el Día del Veterano y de los Caídos en la Guerra de Malvinas; y el viernes 15 (Viernes Santo). En tanto que serán días no laborables el 14 (Jueves Santo); 16, 17, 22 y 23 (primeros dos y últimos dos días de la Pascua Judía, respectivamente, solo aplicable para quienes profesen esa religión); y el 24, por ser el Día de la acción por la tolerancia y el respeto entre los pueblos (aplicable solo a personas de origen armenio).

En mayo serán feriados los días domingo 1º y miércoles 25, por el Día del trabajador y el Día de la Revolución de Mayo, respectivamente. El lunes 2 será no laborable por la fiesta de la Ruptura del ayuno del sagrado mes de Ramadán (aplicable para los habitantes que profesen la religión islámica).

Junio tendrá dos feriados. El 17 se conmemora el Paso a la inmortalidad del general Don Martín Miguel de Güemes. Asimismo, el lunes 20 es feriado inamovible por el Paso a la inmortalidad del general Manuel Belgrano.

En julio será feriado inamovible el sábado 9 debido a la celebración del Día de la Independencia. En esa jornada también se conmemora la Fiesta del Sacrificio y el sábado 30, el Año Nuevo Islámico (ambos días no laborables para quienes profesan el islam). Agosto, en tanto, tendrá un solo feriado trasladable el lunes 15 por motivo de conmemorar el Paso a la inmortalidad del general José de San Martín (17 de agosto). 

Septiembre tendrá dos días no laborables el lunes 26 y martes 27 con motivo de la celebración del Año Nuevo Judío (para quienes profesen esa religión). También como celebración del pueblo judío, el martes 5 de octubre se celebra el Día del Perdón, de carácter no laborable. En octubre también se estableció el viernes 7 como feriado con fines turísticos y el 10 es feriado trasladable por el Día del Respeto a la diversidad cultural (12 de octubre).

En noviembre se celebra el Día de la Soberanía Nacional el domingo 20, que será feriado trasladable, por lo que el lunes 21 será feriado con fines turísticos. En diciembre habrá tres feriados: el jueves 8 por la inmaculada concepción de la virgen María y el domingo 25, por Navidad, serán feriados inamovibles, en tanto que el viernes 9 será feriado con fines turísticos.

Con la incorporación del censo, así quedará el calendario de feriados para 2022 al que, debe aclararse, para los trabajadores mercantiles comprendidos en el CCT 130/75 también tendrá al 26 de septiembre como un día asimilable a un feriado nacional (en lugar de día no laborable) por motivo del Día del Empleado de Comercio, establecido así a partir de la ley 26541/09. Se estima que no se traslade ya que cae lunes.