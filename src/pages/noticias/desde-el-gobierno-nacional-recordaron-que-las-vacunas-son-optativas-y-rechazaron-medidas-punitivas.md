---
category: Agenda Ciudadana
date: 2021-08-16T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/nopunish.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Desde el Gobierno nacional recordaron que las vacunas son "optativas" y rechazaron
  medidas punitivas
title: Desde el Gobierno nacional recordaron que las vacunas son "optativas" y rechazaron
  medidas punitivas
entradilla: 'Tanto Santiago Cafiero como Cecilia Todesca Bocco confirmaron que la
  inmunización no es obligatoria. '

---
El Gobierno nacional recordó que la vacunación contra el coronavirus es "optativa" y rechazó el carácter punitivo que desde la Unión Industrial Argentina (UIA) plantearon para que apliquen las empresas a los empleados que desistan de inmunizarse.

El jefe de Gabinete, Santiago Cafiero sostuvo este jueves que "no debería existir ninguna política sanitaria dictada desde las empresas", al rechazar la iniciativa del titular de la UIA, Daniel Funes de Rioja, quien había planteado que las empresas no abonen los salarios a los trabajadores que no se vacunen contra el coronavirus.

En igual sentido se expresó la vicejefa de Gabinete, Cecilia Todesca Bocco, quien restó importancia a los dichos del dirigente empresario al considerar que "la mayor parte de la población argentina entiende la importancia de la vacunación".

En una jornada en la que CGT volvió a rechazar la iniciativa, también se manifestaron en el mismo sentido desde la Cámara Argentina de Comercio y Servicios (CAC); así como el presidente del Banco de Inversión y Comercio Exterior (BICE), José de Mendiguren.

"El salario no puede estar en juego por no vacunarse", dijo el secretario general de la CGT, Héctor Daer, en declaraciones a Radio La Red, quien fue tajante al afirmar que "no corresponde" descontar salario y recordar que se trata de una vacunación "optativa".

"Vamos a continuar con la vacunación de modo optativo", reforzó Daer y añadió que existe "una gran aceptación por parte de la población" de la inmunización contra el coronavirus por lo que "no puede haber una política de sanción, vinculada a eso, más si la vacuna es optativa".

Ayer, desde la CGT ya se habían rechazado las expresiones de Funes de Rioja al considerar que imponer sanciones de ese tipo "no es el camino adecuado".

Esta semana, Funes de Rioja -en una rueda de prensa realizada tras la reunión de junta directiva de la UIA- opinó sobre la situación de aquellos trabajadores que no deseen vacunarse y expresó que "en esos casos cesa la dispensa y también la remuneración".

En sus declaraciones de esta mañana, Cafiero sumó que en la Argentina existe "una gran aceptación en el tema de la vacunación" y que el país "viene con una gran cultura de calendario de vacunas, algo que se amplió durante el gobierno de Cristina (Fernández de Kirchner)".

Incluso el ministro coordinador destacó que "Argentina es uno de los países que más viene vacunando".

En este sentido, precisó que "en la actualidad tenemos una cobertura del 80 por ciento de mayores de 18 que ya tienen al menos una dosis contra el coronavirus, lo que en breve se completará".

"Así que hay una gran aceptación, y por otra parte, no puede haber una política económica, o de sanción, si la vacuna es optativa", insistió el funcionario nacional.

Luego, al recibir formalmente las dosis de Sputnik V elaboradas por Laboratorios Richmond en la Planta de Operaciones de Logística Farmacéutica de Andreani, en el partido bonaerense de Malvinas Argentinas, se pronunció de la misma manera.

Por su parte, la vicejefa de Gabinete, Cecilia Todesca Bocco, consideró que "la mayor parte de la población argentina entiende la importancia de la vacunación", que "no es solo para uno sino también por los demás", con lo cual restó trascendencia al planteo realizado desde la UIA.

En ese marco, la funcionaria subrayó la importancia de "encontrar los mecanismos para que los empresarios y empresarias y sus trabajadores se pongan de acuerdo" en cuanto a la "vuelta al trabajo", en referencia al regreso a la presencialidad en el marco de la actual etapa de la pandemia de coronavirus.

"Por suerte en la Argentina no estamos como en otros países que hay un grupo muy grande que rechaza la vacuna", señaló la funcionaria en declaraciones formuladas esta mañana a Radio 10, en las que subrayó que, en el país, "¨la gente entiende la importancia de la vacunación".

Todesca Bocco reafirmó que se va "a continuar con la vacunación de modo optativo", como ocurre a nivel global.

"La mayor parte de la población argentina entiende que la vacuna no es sólo para uno pero también por los demás", completó Todesca Bocco, y luego recordó que "más del 80% de mayores de 18 años" poseen la primer dosis y "20% del total de la población" ya tiene la segunda inoculación.

**La CGT**

El miércoles la central obrera se expresó a través de un comunicado firmado por su secretario de Prensa y Comunicación, Jorge Sola, en contra de "castigar en un derecho laboral a quienes debería solicitarse un compromiso colectivo o social respecto del plan de vacunación" y aseguró que "no es para nada el camino adecuado".

Para reafirmarlo, se pronunció hoy en el mismo sentido Daer, quien dijo que "lo que tiene que hacer la UIA es pensar cómo" se va a "recuperar" el país y "cuándo" se tendrá "un país con desarrollo".

En este punto, agregó: "Lo de no pagar los sueldos a los que no se vacunen es un debate absurdo. Habla mucho de dónde cayó la UIA".

**La UIA**

Funes De Rioja había planteado, a partir del avance de la inmunización, que las empresas podrán considerar la eliminación de la dispensa a los trabajadores que decidan no inocularse y, ante la imposibilidad de incorporarlos al ámbito laboral, de no pagarles.

"Aquel que está con primera dosis ya puede ser convocado, pero se genera el problema con los que no quieren vacunarse. En ese caso nadie puede obligarlos, pero nadie puede obligar a insertarlos en un medio laboral con riesgo para otros trabajadores y sus familias. En esos casos, debe cesar la dispensa y la remuneración", había afirmado el dirigente de la UIA.

En tanto, el presidente de la CAC, Mario Grinman, manifestó hoy la discrepancia de esa entidad respecto de postura de Funes de Rioja.

"Somos muy respetuosos del derecho individual de la autodeterminación, si alguna persona decide no vacunarse no está cometiendo ningún delito, más allá de que el escenario ideal sería que el 100% de los argentinos estemos todos vacunados", dijo en declaraciones a Radio La Red.

"Si el Estado considera que las personas que no se vacunan son un riesgo para la comunidad en general, va a tener que actuar en consecuencia, pero eso es potestad del Estado, el empleador no tiene potestad si el empleado se presenta a su trabajo de impedirle trabajar, y menos aún de descontarle el salario, porque eso seguramente va a terminar en un litigio judicial y eso es muy malo para todos", expresó.

En cambio, consideró que "hay que llamar a la solidaridad y a la responsabilidad".

En tanto, De Mendiguren, quien es miembro de la Junta Directiva de la Unión Industrial Argentina (UIA), subrayó que "no es legal" la propuesta de Funes de Rioja y que fue una "opinión personal" y no un planteo de la entidad.

"¿Puedo yo tomar esta medida como industrial? No, definitivamente no", afirmó a radio Del Plata, al tiempo que remarcó: "No puedo obligar a vacunar ni puedo no pagar el sueldo al que no lo quiera hacer. Esto es legal, no es un tema opinión".

"Yo no conozco en la Argentina trabajadores que no se quieran vacunar, ni en mi fábrica, ni en la de mis amigos. No es la generalidad", aseguró De Mendiguren.