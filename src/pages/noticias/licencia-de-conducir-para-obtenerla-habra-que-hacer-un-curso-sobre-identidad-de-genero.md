---
category: Agenda Ciudadana
date: 2021-02-25T06:32:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/carnet.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Licencia de conducir: para obtenerla habrá que hacer un curso sobre género'
title: 'Licencia de conducir: para obtenerla habrá que hacer un curso sobre género'
entradilla: La Agencia Nacional de Seguridad Vial dispuso un nuevo requisito. Se trata
  de un curso que incluye contenidos sobre masculinidades, patriarcado, femicidios
  y crímenes de odio.

---
La Agencia Nacional de Seguridad Via**l** (ANSV) dispuso que para obtener la licencia de conducir en nuestros país se deberá completar un curso sobre género. El mismo contendrá temáticas como patriarcado, femicidios y acceso y participación de las mujeres en el sector de transporte.

También se enseñará sobre género, roles y estereotipos, identidad de género, violencia de género, tipos y modalidades de violencia. Además se brindará formación sobre conceptos como patriarcado y heteronormatividad.

_«En la convicción que el recurso cultural se muestra como un aspecto de vital influencia en lo que respecta a la incorporación de normas de género relativamente cristalizadas, se entiende necesario incorporar en el curso obligatorio para el otorgamiento de la Licencia Nacional de Conducir un módulo que contemple la temática en cuestión y promueva valores de igualdad y la deslegitimación de la violencia contra las mujeres en la conducción de vehículos, la vía publica, la seguridad vehicular, y todo lo relativo a la materia_», detalla una resolución publicada este miércoles en el Boletín Oficial.