---
category: Agenda Ciudadana
date: 2021-03-09T07:07:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/sain1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Por las críticas descalificadoras a la Policía, piden el juicio político
  a Sain
title: Por las críticas descalificadoras a la Policía, piden el juicio político a
  Sain
entradilla: Lo hicieron desde la Coalición Cívica. Además, le solicitaron al gobernador
  Omar Perotti que le pida "de manera urgente la renuncia" al ministro de Seguridad

---
A raíz del audio que se filtró del ministro de Seguridad de la Provincia, Marcelo Sain, donde insulta con términos racistas y descalificadores a integrantes de la policía de la provincia, la Coalición Cívica (CC) solicitó este lunes el juicio político al ministro de Omar Perotti.

El planteo de la diputada nacional por Santa Fe, Lucila Lehmann, y de su par provincial electo, Sebastián Julierac Pinasco –que no asumió su banca porque una mujer ocupó su lugar para que haya paridad en el bloque de Juntos por el Cambio–, fue difundido a través de un comunicado. E incluye un pedido al gobernador Omar Perotti para que solicite "de manera urgente" la renuncia al funcionario.

“Los rosarinos se creen que son todos porteños”, “a mí me chupa un huevo todo, los rosarinos, los santafesinos, los de Reconquista”, “son unos negros pueblerinos" y "si Dios atendiese en Santa Fe, estamos hundidos todos", descerrajó el ministro.

“Como diputada nacional por Santa Fe le exijo al gobernador que solicite de manera urgente la renuncia a Sain. No sólo por la ineficiencia y falta de resultados a la hora de cuidar la seguridad de todos sino por los insultos y agravios hacia los comprovincianos", enfatizó Lehmann.

Sain aseguró que se trata de un audio privado de vieja data ("no es una escucha judicial") y aclaró que lo que se escucha no es su opinión sobre los santafesinos en general, sino que estaba hablando de una persona en particular "que no tenía ganas de trabajar".