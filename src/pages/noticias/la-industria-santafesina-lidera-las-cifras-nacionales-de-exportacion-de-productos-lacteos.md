---
category: Agenda Ciudadana
date: 2021-01-18T09:00:28Z
thumbnail: https://assets.3dnoticias.com.ar/exportacion_leche.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La industria santafesina lidera las cifras nacionales de exportación de productos
  lácteos
title: La industria santafesina lidera las cifras nacionales de exportación de productos
  lácteos
entradilla: La leche en polvo es el principal producto, secundando los quesos.

---
Luego de una recuperación mayor al 25 por ciento durante el 2020, las exportaciones santafesinas de productos lácteos lideran el mercado nacional, principalmente impulsadas por el crecimiento en un 70% de los despachos de leche entera en polvo, con un volumen 42.9 toneladas, y un valor de 129 millones de dólares.

Entre dichos productos, el queso azul santafecino representa la totalidad de las exportaciones de ese producto que ofrece Argentina al mundo, el mozzarella un 90 por ciento, pasta blanda un 80 por ciento y pasta dura poco más de la mitad; representando un valor de 179,3 millones de dólares en total.

Los destinos más dinámicos en 2020 fueron Argelia, Chile, Uruguay, Rusia y Taiwán; mientras que en Colombia y China se abrieron nuevos mercados. Contrariamente, Brasil, Paraguay y Perú recortaron su demanda.

**ECONOMÍA REGIONAL**

El sector lechero santafesino es conocido por ser uno de los más importantes motores de la economía provincial.

La cuenca lechera central forma parte de la cuenca lechera más importante de Latinoamérica, donde no sólo se concentra la mayor cantidad de explotaciones de productos lácteos, sino también un importante número de empresas lácteas.

Estas empresas, alrededor de cien (entre ellas, Nestlé, Williner, CORLASA, Saputo, Verónica, García Hnos, SANCOR, La Ramada, RAMOLAC y  MILKAUT), poseen una importante capacidad para el secado de la leche, que ha posicionado a la provincia como principal exportadora de leche en polvo.

El liderazgo de Santa Fe se manifiesta en el 40 % del total de la leche en polvo exportada, lo que representa un poco más del 50 % de los dólares ingresados al país por este concepto.

También se destaca el dulce de leche, producto con una creciente demanda de comunidades argentinas y latinas existentes en el mundo, y también por los sectores gastronómicos y de postres helados.

![](https://assets.3dnoticias.com.ar/exportacion_leche_1.jpeg)

![](https://assets.3dnoticias.com.ar/exportacion_leche_2.jpeg)