---
category: Agenda Ciudadana
date: 2021-02-19T06:09:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/trotta.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Gremios docentes pidieron una aumento salarial de más del 30%
title: Gremios docentes pidieron una aumento salarial de más del 30%
entradilla: Fue durante el primer encuentro de la paritaria nacional, La semana próxima
  volverán a reunirse las partes. Poco antes el Presidente se recibió a representantes
  sindicales.

---
Los sindicatos docentes pidieron esta tarde una suba salarial "superior al 30%" durante la primera reunión de la paritaria nacional del sector y la semana próxima habrá un nuevo encuentro para tratar de llegar a un acuerdo.

"Nos han planteado un aumento salarial superior al 30 por ciento. Nosotros vamos a hacer una propuesta acorde a la necesidad de los maestros. Escuchamos la propuesta y se abre un proceso de negociación", sostuvo el ministro de Educación, Nicolás Trotta, en declaraciones a la prensa al finalizar la reunión.

Trotta había adelantado que "no habrá techo" en la negociación salarial y deseó que "a partir del diálogo se pueda responder a las expectativas de los docentes que también han tenido un año muy difícil, en un año que ha sido muy difícil para la economía argentina en general, y para los docentes en particular".

Asimismo, recordó que al concluir la negociación en noviembre pasado los integrantes de la mesa se comprometieron a "una nueva reunión en febrero".

"Y ahora, como entonces, queremos volver a dar a una señal de claridad de que pretendemos que la paritaria nacional docente sea una herramienta para encauzar los debates pendientes en algunas jurisdicciones", resaltó el titular de la cartera educativa.

El ministro coordinó la reunión virtual desde el Palacio Sarmiento, sede de la cartera educativa, y participaron sindicalistas de CTERA, SADOP (docentes privados), Confederación Argentina de la Educación (CEA), Unión de Docentes Argentinos (UDA) y la Asociación de Magisterio de la Enseñanza Técnica (AMET).

Poco antes de esta reunión, el presidente Alberto Fernández recibió en Casa Rosada a los referentes de CTERA, el gremio mayoritario de los docentes, Hugo Yasky, Roberto Baradel y Sonia Alesso.