---
category: Agenda Ciudadana
date: 2021-06-06T06:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/SANTAFE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Provincia por provincia: cómo avanza la campaña de vacunación'
title: 'Provincia por provincia: cómo avanza la campaña de vacunación'
entradilla: El gobierno nacional ya distribuyó en el país 17.495.490 dosis de vacunas
  para hacer frente a la pandemia, según el registro el Monitor Público de Vacunación
  del Ministerio de Salud de la Nación.

---
Las provincias avanzan en la implementación del operativo nacional de inmunización contra la Covid-19 en todo el territorio argentino mediante la ampliación de franjas etarias, la incorporación de grupos poblacionales sin factores de riesgo y la vacunación por demanda espontánea, sin turno, en varias jurisdicciones.

El gobierno nacional ya distribuyó en el país 17.495.490 dosis de vacunas para hacer frente a la pandemia, según el registro el Monitor Público de Vacunación del Ministerio de Salud de la Nación, que sigue en tiempo real la aplicación de las vacunas contra el coronavirus.

Hasta la fecha, se aplicaron 13.730.521 dosis: 10.733.428 personas recibieron la primera y 2.997.093 completaron el esquema de vacunación con los dos componentes.

En este marco, las corresponsalías de Télam reportan refuerzos en el calendario de vacunación de las provincias, con nuevas convocatorias y progresos en la cobertura.

**Santa Fe**

Finalizó la vacunación de todos los mayores de 60 años inscriptos en el registro oficial, así como la totalidad del personal de seguridad y residentes de geriátricos, y alcanzó el 70% de las personas con discapacidad anotadas. También recibió inmunizaciones el 98% del personal de salud y la mayoría de los docentes titulares; aunque faltan los reemplazantes y trabajadores de escuelas artísticas, indicaron desde el Ministerio de Salud.

Según voceros de la cartera, la provincia inició la semana pasada el proceso de vacunación de personas con comorbilidades entre 18 y 59 años, que espera concluir "a mediados de junio".

**Buenos Aires**

El gobierno bonaerense abrió este sábado la "vacunación libre" para los mayores de 70 años sin inscripción previa, con solo presentar el DNI en los centros de vacunación.

Además, permite que todos los grupos etarios se inscriban para recibir la vacuna y luego asigna turnos de acuerdo con las prioridades epidemiológicas.

**Neuquén**

Ya vacunó al 90% de la población objetivo y comenzó a implementar en mayo jornadas de vacunación a demanda.

Según estimaciones oficiales, este fin de semana uno de cada cuatro neuquinos estará inmunizado con la primera dosis, en tanto comenzó a vacunarse a personas mayores de 55 años sin factores de riesgo y a trabajadores de los 24 rubros considerados esenciales.

También se habilitó la inscripción para personas de 50 a 55 años sin factores de riesgo, que serán convocadas a los vacunatorios en los próximos días.

**Salta**

El secretario general de la Gobernación, Matías Posadas, anunció que este fin de semana "se llevará adelante un plan intensivo de vacunación en todas las áreas regionales de la provincia para los salteños mayores de 50 años", sin turno, que deben concurrir a los lugares de vacunación a "demanda espontánea y con DNI".

Además, continúa la inoculación a mayores de 18 años con factores de riesgo, que se suman al personal sanitario, de seguridad y los docentes, entre otros.

La titular del programa de Inmunizaciones, Adriana Jure, aclaró que la inoculación de "quienes tienen algún factor de riesgo se sigue haciendo por turno".

**Jujuy**

Inició el mes de junio con la vacunación a personas desde 40 años en adelante, con o sin patologías, sin inscripción web y de acuerdo con la terminación del DNI, en los centros de salud de la provincia más cercanos al domicilio y "con prioridad para la segunda dosis", informó el gobierno.

Por otro lado, continúa esta semana la aplicación de la primera dosis de Sputnik-V a adultos mayores de 60 años y a personas de 18 a 59 años de edad con factores de riesgo.

Las personas entre 18 y 39 años de edad con comorbilidad también pueden vacunarse en los puntos abiertos, aunque se requiere certificado médico con no más de tres meses de vigencia.

**Tucumán**

El director de Gestión Sanitaria del Sistema Provincial de Salud (Siprosa), Miguel Ferré Contreras, informó que ya se vacunó a "más del 50% de las personas de entre 50 y 55 años, grupo que comenzó con la inoculación el fin de semana pasado" y que se esperaba concluir en estos días.

En tanto, el jueves se habilitó la inscripción para las personas de 45 a 49 años, con o sin factores de riesgo, con 50 mil turnos disponibles para este sábado y domngo en la provincia.

**Misiones**

La provincia ya alcanzó al 92% de la población objetivo y vacunó al 100% del grupo entre 18 a 59 años con discapacidad o comorbilidades, y a la totalidad del personal de salud.

Además, hace dos semanas comenzó la vacunación de mayores de 40 años, con y sin comorbilidades, que tramitaron su turno en la aplicación Alegra MED.

Los mayores de 60 años, que ya fueron inmunizados en un 86%, se vacunan sin turnos, con la presentación del DNI.

También se cubrió el 38% del personal estratégico y al 97% del personal docente.

Esta semana, las autoridades sanitarias avanzaron con vacunatorios móviles en la vía pública.

**Mendoza**

Esta provincia amplió el miércoles pasado la inscripción para asignar turnos de vacunación a personas mayores de 50 años en general, sin enfermedades de base, mientras se avanza en la vacunación de trabajadores de la educación mayores de 30 años.

En tanto, la semana pasada se habilitó la inscripción para mayores de 18 años que formen parte de alguno de los grupos de riesgo.

Asimismo, se vacunará a más de 1500 trabajadores del Poder Judicial en fecha a confirmar, y el personal vinculado a los servicios públicos comenzará a recibir sus vacunas.

**Córdoba**

La provincia mediterránea comenzó esta semana la vacunación para personas mayores de 18 años con comorbilidades e inició en estos días el proceso de inmunización para ciudadanos mayores de 40 años sin patologías previas.

El registro para inscribirse está abierto a través de la página Ciudadano Digital.

**Entre Ríos**

Sigue con la inmunización de personal educativo docente y no docente, y de fuerzas federales; y la semana próxima podrán anotarse personas sin comorbilidades entre 50 y 59 años.

También se inoculó, con al menos una dosis, al total del personal de salud; al total de adultos mayores y personal de geriátricos y residencias e instituciones de larga estadía, y al total de los usuarios de instituciones monovalentes de salud mental, tanto públicas como privadas.

A su vez, se inoculó al 82,8% de adultos mayores de 70 años estimados en un principio; el 79,6% de personas entre 60 y 69 años; un 35% entre 18 y 59 años con factores de riesgo; y 28.219 personas consideradas "personal estratégico".

**Santiago del Estero**

La provincia vacuna actualmente a personas entre 50 y 59 años con o sin factores de riesgo, mientras la próxima semana se iniciará la inmunización a personas de 45 a 49 años, con o sin comorbilidades, en Capital y Banda; y desde los 40 a 49 años en el interior provincial, según detalló a Télam la titular del área de Inmunizaciones, Florencia Coronel.

También se vacuna a personas entre 18 y 59 años con factores de riesgo.

Asimismo, "ya se vacunó, al menos con la primera dosis, a mayores de 60, docentes del nivel Inicial, Primario y Secundario y fuerzas de seguridad, mientras que se completó con las dos dosis al personal de salud'', indicó Coronel.

**San Luis**

Está vacunando a docentes, bomberos y policías; a jóvenes entre 18 y 49 años con patologías de base, y a quienes pidieron reprogramar turnos asignados.

Desde el primer envío de vacunas, San Luis organizó un plan de inmunización continuo: "dosis que llega, dosis que se aplica", según fuentes ministeriales que indicaron que, de este modo, el viernes fueron convocados "6.632 vecinos de 16 localidades y parajes".

El formulario de inscripción se completa en la página vacunacioncovid.sanluis.gov.ar/salud.

**Chubut**

Comenzará la semana próxima a vacunar a la población comprendida entre los 50 y 59 años, tengan o no factores de riesgo, para lo cual "estamos llamando a todos los incluidos en esa franja etaria para que se inscriban" indicó a Télam el ministro de salud, Fabián Puratich.

La provincia ya completó el plan de vacunación de la primera dosis para los mayores de 60 años, población con comorbilidades entre 50 y 59, y avanza en la vacunación de policías, docentes y auxiliares de la educación.

**Tierra del Fuego**

Comenzó a vacunar a personas de más de 50 años sin factores de riesgo, mientras sigue inoculando a personal estratégico (docentes y miembros de fuerzas de seguridad mayores de 45 años) tras finalizar con el personal de salud y la franja etaria de mayores de 60 años y de 18 a 59 años con antecedentes.

**Río Negro**

La cartera sanitaria provincial informó que ya recibieron las dos dosis los grupos priorizados: personal de Salud, personal estratégico y personas entre 18 y 59 años con factores de riesgo.

Además, ya recibieron al menos una dosis 87,8% de las personas mayores de 60 años incluidas en el plan de vacunación, que representan el 53% de la población objetivo total del plan y es uno de los más priorizados en la campaña.

**Santa Cruz**

La semana culminó con la vacunación a personas a partir de los 50 años con comorbilidades o factores de riesgo, con turnos otorgados en la página oficial.

El Ministro de Salud y Ambiente, Claudio García destacó que "llegar a la aplicación de más de 100.000 vacunas, con aproximadamente un 30% de ese número de segundas dosis aplicadas, representa un logro importante desde el punto de vista sanitario".

**La Rioja**

El Ministerio de Salud comenzó esta semana con la vacunación de las personas de entre 18 y 59 años, con antecedentes o comorbilidades, inscriptas en la página oficial del gobierno.

La provincia inoculó al 76,36% de las personas de más de 60 años, 43.47 % de las personas de 18 a 59 años con enfermedades de riesgo, 53,63 % del personal estratégico, y un 93,13% del personal de salud.

**Corrientes**

Inició este viernes la vacunación a personas de entre 40 y 45 años de edad y dispuso la continuidad de las inscripciones a todos los grupos etarios alcanzados por el plan.

Ya se inoculó a personas mayores de 45 años de edad, mayores con obesidad, pacientes oncológicos, trasplantados, inmunodeprimidos, policías, ex combatientes de Malvinas, bomberos y docentes.

Inmuniza a personas mayores de 50 años sin distinguir factores de riesgo y amplía la convocatoria al grupo etario entre 49 y 18 años.

Inmuniza a personas mayores de 50 años sin distinguir factores de riesgo y amplía la convocatoria al grupo etario entre 49 y 18 años.

**Catamarca**

Los nodos de vacunación del interior provincial continúan con la aplicación de primeras dosis a la población nacida entre 1962 y 1967, por año de nacimiento, de manera decreciente.

El gobernador Raúl Jalil explicó que "ya no hay más esenciales por vacunar; vamos a avanzar con los vacunados con comorbilidades, hipertensos, obesos, y diabéticos, que representan el 90% del uso de la terapia intensiva en la provincia"; y adelantó que "próximamente se vacunará a menores de 50 años de edad".

**Formosa**

El gobierno formoseño informó que, luego de vacunar grupos de riesgo de personas nacidas entre 1980 y 1990 en Formosa capital, habilitó la inscripción para residentes nacidos entre 1991 y 2003 que integren los grupos de mayor riesgo.

El jueves 10, en tanto, se inoculará a los residentes de las localidades de Bajo Hondo, La Libertad, Lamadrid, Fortín Pilcomayo, Guadalcazar, Río Muerto, Puerto Irigoyen y Sombrero Negro nacidos entre 1961 y 1965.

Y el domingo 13 de junio recibirán la vacuna los adultos nacidos entre 1961 y 1965 de las localidades de Laguna Yema, Pozo del Mortero, Sumayen, Los Chiriguanos y El Simbolar.

El personal docente y de seguridad de las ciudades de Clorinda y Formosa ya fueron vacunados.

**San Juan**

La provincia avanza con el operativo de vacunación para docentes, ya con la segunda dosis; y con la primera dosis para el personal docente y no docente universitario.

También inocula con la primera dosis a personas entre 18 y 55 años con comorbilidades.

**La Pampa**

Inmuniza a personas mayores de 50 años sin distinguir factores de riesgo y amplía la convocatoria al grupo etario entre 49 y 18 años, en orden decreciente, con priorización de personas con comorbilidades, además embarazadas con factores de riesgo.

Los turnos se otorgan previa inscripción en: [https://noscuidamos.lapampa.gob.ar](https://noscuidamos.lapampa.gob.ar "https://noscuidamos.lapampa.gob.ar")

**Chaco**

En la ampliación del rango etario, la provincia vacuna actualmente con primeras dosis a mayores de 50 años, que pueden acercarse a los vacunatorios del Gran Resistencia sin turno, con DNI en mano.

También está en curso la inmunización de las personas mayores de 45 años, que deben confirmar su turno en el sitio web Elijo Vacunarme.