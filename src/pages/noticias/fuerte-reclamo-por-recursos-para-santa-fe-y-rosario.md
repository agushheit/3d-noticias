---
layout: Noticia con imagen
author: "  Fuente: El Litoral"
resumen: Reclamo por recursos
category: Agenda Ciudadana
title: Fuerte reclamo por recursos para Santa Fe y Rosario
entradilla: 'Cuestionaron que el gobierno nacional y el provincial generen un
  excedente en los presupuestos, calculando de menos los ingresos. Y que lo
  hagan para usarlo de manera "concentrada y discrecional". '
date: 2020-11-20T11:27:49.775Z
thumbnail: https://assets.3dnoticias.com.ar/juntos-x-el-cambio.jpg
---
**Representantes de Juntos por el Cambio en el Congreso de la Nación, la Legislatura santafesina y los Concejos Deliberantes de Santa Fe y Rosario formularon un fuerte reclamo por la inclusión de ambas ciudades en el Fondo de Obras Menores y también un aporte especial para atender la crisis del transporte**.

En conferencia de prensa, el diputado nacional Federico Angelini, el diputado provincial Julián Galdeano y los concejales santafesinos Carlos Suárez y Luciana Ceresola, y rosarinos Carlos Cardozo y Anita Martínez, junto a otors legisladores, hicieron notar que tanto el presupuesto nacional como el provincial subestimaron los ingresos, con lo cual habrá un excedente en ambas jurisdicciones, que no debe ser utilizado de manera "concentrada y discrecional", sino a través de los órganos deliberativos y para atender las necesidades reales de la comunidad.

En el poblado recinto del hotel céntrico, en una disposición que jugó al límite con las pautas de distanciamiento, los legisladores de Juntos por el Cambio presentaron un documento emitido por la coalición (ver aparte) y reclamaron al gobierno nacional y provincial habilitar instancias de diálogo para promover los consensos necesarios para afrontar la crisis.

"Si las fuerzas mayoritarias no se ponen de acuerdo va a ser muy difícil salir de esta crisis en la que Argentina viene desde hace tiempo, pero que en estos últimos 8 meses de ha incrementado muchísimo", sentenció Angelini. Para el legislador nacional, hubiese sido una gran oportunidad para ello la discusión por el presupuesto.

"Estábamos convencidos de que tendría que haber sido una hoja de ruta, un mensaje muy fuerte para los distintos sectores económicos para marcar el rumbo de cara a 2021 y la salida de esta situación tan grave", explicó. Pero, no obstante, relató que "no encontramos en el gobierno nacional receptividad para ese diálogo. Encontramos un presupuesto que una vez más nos muestra que el kirchnerismo apuesta a la discrecionalidad, a que las provincias vayan a pedirles por favor que les den recursos, a que dependan demasiado del favor del gobierno nacional y no que tengan independencia. También vemos en el presupuesto datos irreales y vemos una discriminación a la provincia de Santa Fe".

## **Incertidumbre**

Al respecto, el diputado provincial Julián Galdeano remarcó que "asistimos a una situación de la Argentina extraordinariamente compleja en lo económico, en lo social, y sobre todo con incertidumbre sobre el rumbo del gobierno. Vemos con preocupación que haya una política de calcular por debajo los ingresos del Estado. Ésto permite que los recursos que excedan lo presupuestado se distribuyan con discrecionalidad, siempre privilegiando los del mismo signo político. Y vemos con preocupación que esto se repita en la provincia de Santa Fe en el presupuesto 2021. Nuevamente hay un subcálculo de ingresos, que va a permitir que el Ejecutivo concentre recursos que no están previstos, que luego se pueden asignar con muchísima arbitrariedad", argumentó.

Para el legislador radical "tiene que haber transparencia, previsibilidad y equidad en la distribución de fondos a municipios y comunas, y vemos con preocupación que los municipios de Santa Fe y Rosario afrontan muchísimas dificultades. Se encuentran en un atolladero en la atención de los gastos que demanda su funcionamiento, en el transporte, y entendemos que es una oportunidad para que el Ejecutivo provincial convoque a la oposición. Desde Juntos por el Cambio reiteramos nuestra predisposición para llevar adelante el diálogo; sea para discutir el modelo institucional, sea para discutir acerca de los futuros comicios de 2021, pero sobre todo para ver cual es el rumbo y el plan que los santafesinos tenemos de acá en adelante. Que no sea el plan de la incertidumbre, de la discrecionalidad, del ajuste por el ajuste mismo, sino un programa que atienda la verdadera agenda de la ciudadanía, que es la educación, la vuelta a clase, la seguridad, la asistencia a la obra pública". "Calculamos en 70 mil millones de pesos los ingresos que no están calculados en el presupuesto de la Provincia. Tampoco está el convenio que ya tiene principio de ejecución para soportar el déficit de la Caja de Jubilaciones, entre 10 y 12 mil millones de pesos se calculan también allí. De modo tal que sin duda va a haber recursos extra en el Presupuesto provincial, que van a tener que tener un destino asignado por la Legislatura provincial, que para eso existe y para eso sanciona la ley de Presupuesto. Entendemos vital que parte de esos recursos se destinen a atender necesidades de los principales municipios de la provincia, que han sido postergados. El Fondo de Obras Menores para Rosario y Santa Fe es una asignatura pendiente, que vamos a pelear; además de un fondo para el transporte urbano-interurbano que tan castigado está", resumió Julián Galdeano.

### A continuación, fragmentos del documento emitido por Juntos por el Cambio.

"...Mientras la sociedad vive y sufre la crisis sanitaria, económica y social más importante de los últimos tiempos, el gobierno pretende desviar la atención mediante el intento de modificar nuestro sistema electoral, eliminando las PASO con el fin de coartar la participación de la ciudadanía en la elección de sus representantes.

Desde Juntos por el Cambio, nos manifestamos dispuestos a dialogar acerca de los comicios en 2021, pero advertimos que cualquier modificación requerirá del consenso de todas las fuerzas políticas, tanto a nivel nacional como provincial. En el caso de Santa Fe, advertimos sobre el retroceso que significaría para todos los santafesinos la vuelta al sistema de lemas (...)

"...Las iniciativas impulsadas por el Ejecutivo Nacional asfixian aún más a los contribuyentes con un incremento en impuestos, que sabemos terminarán pagando los consumidores, trabajadores y productores en un país ya golpeado ferozmente por el parate económico de la cuarentena más extensa del mundo.

La discusión por el impuesto a las grandes fortunas, el intento de imponer nuevos tributos a las provincias a través de la derogación del consenso fiscal, la presión para que se desista de los reclamos judiciales que las provincias tienen aún en la Corte, la falta de federalismo y el manejo discrecional de los recursos, son clásicas postales del Kirchnerismo gobernante.

A su vez, vemos con preocupación que el gobernador Omar Perotti no demuestre el carácter que se requiere para defender ante Nación los derechos de los santafesinos, tras haber sido nuestra provincia una de las más perjudicadas en el proyecto del Presupuesto para 2021, en la que lamentablemente se perdió una oportunidad de alcanzar consensos en el Congreso (...)".

## Punteo

"Dos números representan lamentablemente la realidad de la ciudad de Rosario: 87 días de paro del transporte urbano de pasajeros y 186 muertes violentas en la ciudad de Rosario", dijo Carlos Cardozo, y puso el acento del reclamo en el Hospital Regional, que "es una cáscara vacía con pisos de tierra. Y lo necesitamos ya".

"Vamos a ingresar un proyecto para pedir a los legisladores de la provincia que se incorpore al Fondo de Obras Menores. Esto no solo tiene que ver con pelear por recursos que nos corresponden, sino porque es una cuestión de justicia y equidad, que son valores que para Juntos por el Cambio son centrales. Incorporar al Fondo de Obras Menores a Santa Fe y Rosario saldaría una deuda histórica que tiene la Provincia con estas dos ciudades, y mejoraría la calidad de vida de todos los vecinos", resumió Carlos Suarez.

Luciana Ceresola puso el reclamo en proporción y enumeró los ítems a atender: "En Rosario y Santa Fe viven, estudian y trabajan casi la mitad de los habitantes de la provincia.Y es por eso que necesitamos estos fondos, que en realidad van a beneficiar a todos. En obras, pero también en materia de seguridad, bacheo, luces led, cámaras y lectores de patentes. Las usurpaciones están asolando a los principales centros urbanos. Y necesitamos poner en condiciones las escuelas y los espacios públicos para las nuevas exigencias del covid".