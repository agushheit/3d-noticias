---
category: La Ciudad
date: 2021-01-13T07:54:09Z
thumbnail: https://assets.3dnoticias.com.ar/13121-ruta-168.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: UNO Santa Fe'
resumen: Denuncian piedrazos a vehículos y mala iluminación en la RN 168 a la altura
  de La Guardia
title: Denuncian piedrazos a vehículos y mala iluminación en la RN 168 a la altura
  de La Guardia
entradilla: Los vecinos indicaron que los ataques a piedrazos contra los autos que
  circulan por la zona crecieron en los últimos días. Reclaman más patrullaje e iluminación.

---
Los episodios de inseguridad en los distintos ingresos y egresos de la ciudad de Santa Fe hacia conductores y vehículos, se registraron en aumentos en las últimas semanas. 

A la conocida situación de incidentes con piedrazos en zona de circunvalación y autopista Santa Fe-Rosario, ahora se suma el sector de la Ruta Nacional 168, a la altura de La Guardia, donde muchos vecinos denuncian el accionar de delincuentes sobre los pasos peatonales, contra los autos que transitan por la ruta en horas de la noche.

«Ya es un lamentable y desgraciado hábito a cuál los conductores y vecinos de la zona se tuvieron que acostumbrar en los últimos meses», denunciaron vecinos de La Guardia y de Colastiné.

«Existe un tramo crítico de la Ruta 168, que es a la altura de La Guardia, en el ingreso a Ruta 1 y también de la zona de los cruces peatonales y vehiculares donde se producen los hechos de inseguridad contra los vehículos», aseguraron los vecinos.

«Desde hace tiempo se vienen sucediendo actos de vandalismo con colocación de obstáculos concretos y de gran tamaño en la calzada para que los vehículos detengan su marcha. También piedrazos constantes hacia los cristales de los autos; es una situación de gravedad que se agrande por la falta de iluminación, que les brinda un contexto ideal a los delincuentes para llevar adelante sus atracos», continuaron relatando los vecinos alertados por la creciente situación de inseguridad.

Según contaron los habitantes de la zona, «desde el atardecer y durante toda la noche, son constantes los ataques a los vehículos, ya sean autos, colectivos o motos».

«Al ser una calzada que permite parar en la banquina y que no presenta regresos tanto para Santa Fe como Paraná, es el lugar indicado para los delincuentes, porque los conductores no tienen escapatoria, y muchos de ellos, al no conocer la zona o pecar de exceso de confianza, detienen la marcha para dar aviso al 911; es en ese momento cuando son abordados por los ladrones», aseguraron los vecinos.

«Los damnificados no denuncian los episodios porque no conocen la zona y no se animan a ingresar a la comisaría de La Guardia», denunciaron finalmente los habitantes de La Guardia y Colastiné.