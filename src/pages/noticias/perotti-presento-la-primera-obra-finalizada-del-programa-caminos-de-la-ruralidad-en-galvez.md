---
category: Estado Real
date: 2021-10-03T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/RURALIDADGALVEZ.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti presentó la primera obra finalizada del programa caminos de la ruralidad
  en Gálvez
title: Perotti presentó la primera obra finalizada del programa caminos de la ruralidad
  en Gálvez
entradilla: " “Creemos plenamente en la capacidad del sector productivo, y el Estado
  tiene que estar a la par si le importa el trabajo y la producción, con hechos y
  con obras”, indicó Perotti."

---
El gobernador de la provincia, Omar Perotti, participó este sábado de la presentación de la primera obra culminada del programa Caminos de la Ruralidad, en esta oportunidad, la traza de Gálvez, que contempló una inversión final de 15.300.000 pesos y la mejora de 8.000 metros.

En el evento, el gobernador destacó que “se trata de un hecho importante para Gálvez, y también para toda la provincia. Porque si algo tiene Santa Fe, es una potencialidad enorme en los sectores productivos, que necesitan de la actividad diaria, y que se manifiesta si hay posibilidad de circular”.

Asimismo, Perotti señaló que “esta infraestructura nos está permitiendo priorizar la educación y los sectores estratégicos. El agropecuario es uno de ellos y por eso hay que acercarles infraestructura e inversiones”.

Además, indicó que “son bienvenidas este tipo de obras que hoy se multiplican por varios departamentos: Castellanos, Las Colonias, San Jerónimo, Vera, General Obligado, Villa Constitución, San Justo, San Cristóbal y San Martín; con un presupuesto que supera los 1.100 millones de pesos”.

Por último, el mandatario provincial manifestó que “creemos y confiamos plenamente en la capacidad del sector productivo, y el Estado tiene que estar a la par si le importa el trabajo y la producción; y la mejor forma de demostrarlo es con hechos y obras”.

El intendente de Gálvez, Mario Fissore, precisó que por este camino “sale nuestra riqueza, nuestra producción láctea, ovina, agrícola; y nos permite el acceso a nuestras escuelitas de campo. Hoy esto es una realidad, gracias al programa Caminos de la Ruralidad, y a un trabajo muy fuerte, mancomunado diría, de lo público con lo privado, de los productores rurales y del Consorcio Vecinal Caminero. Entre todos demostramos que podemos hacer realidad los anhelos y necesidades que tenemos”.

A su turno, el director del Consorcio Vecinal Caminero, Augusto Paschetta, indicó que “esta es una obra que se empujó mucho, tanto desde el consorcio, como desde la sociedad rural, y en conjunto con el municipio. Es una obra que nos llevó muchísimo esfuerzo y dinero, tanto desde lo público, como lo privado”.

Por su parte, la directora de la escuela Rural Nº 891, Noemí Escalada, manifestó que “este acontecimiento quedará guardado en la memoria de toda la comunidad educativa” y agradeció “la puesta en valor de una gran obra que beneficia a la zona rural, como es el ripiado, y que permite un mejor acceso en los días de lluvia a los docentes y alumnos, que tendrán igualdad de oportunidades”

Por último, el gerente general de la empresa avícola Pollo de Oro SA, Germán Serri, recordó que “esto se empezó a proyectar hace unos 10 años, y es una caricia al alma para todos. Para los privados y para lo público, que vieron que sirve mancomunar esfuerzos para el bien común. Es un buen ejemplo para seguir replicando, este es el camino” concluyó Serri.

**Detalles de la traza**

En las parcelas linderas a la traza, se desarrollan actividades tamberas, ganaderas y granarias.

Entre ellos, hay dos tambos con una alta productividad, y una carga ganadera de una a casi cuatro cabezas por hectárea.

En lo referente a la lechería, se producen 5,4 millones de litros, por un importe mayor a 178 millones.

Asimismo, la actividad granaria en la zona cuenta con 19 productores, con rendimientos, en algunos casos, superiores a la media provincial. En la actualidad se registran casi 2 mil hectáreas de soja con una producción total de 7 mil toneladas, 244 hectáreas de trigo, con una producción de 944 toneladas, y 866 hectáreas de maíz con 7.132 toneladas de producción.

Sumando los rindes de la lechería y la actividad granaria, en la zona se generan aproximadamente unos 210 millones de pesos.