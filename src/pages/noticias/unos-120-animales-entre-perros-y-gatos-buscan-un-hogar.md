---
category: La Ciudad
date: 2021-10-13T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/ADOPTAUNPERRITE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Unos 120 animales, entre perros y gatos buscan un hogar
title: Unos 120 animales, entre perros y gatos buscan un hogar
entradilla: Son varias instancias en las que se puede ayudar. Actualmente más de 120
  animales, entre perros, gatos y hasta gallos fueron rescatados de malos tratos.

---
Mientras juegan o mueven la cola; esperan. Son más de 120 los perros y gatos que aguardan que los adopten. Cada uno de esos animales de compañía fueron rescatados de malos tratos en diferentes operativos que realizó el municipio. Hoy están a la espera de paseos y diversión.

En ese sentido, el Instituto Municipal de Salud Animal (Imusa) lleva adelante diferentes instancias para que los animales de compañía rescatados puedan estar en un espacio adecuado. Se trata, por un lado, de un sistema de hogares de tránsito donde puedan estar cuidados y protegidos hasta que, por otro lado, se encuentre un hogar que quiera adoptarlos.

En total, son 60 los animales que actualmente están en tránsito, el resto están a disposición para que los vecinos los puedan adoptar. El primer acercamiento al sistema de adopción, en general, es a través de las redes sociales del Imusa. Posteriormente, los interesados se acercan a la sede de Obispo Gelabert 3691, completan una ficha con los datos del futuro adoptante donde constan algunos datos referidos a las condiciones de guarda con las que cuenta en el nuevo hogar.

Una vez completada esas instancias, las personas conocen al animal de compañía mediante algunos juegos o incluso un paseo corto. Posteriormente, se brinda un tránsito con una acompañamiento de voluntarios, para verificar cómo está el animal de compañía en su nuevo domicilio; si la familia se siente cómoda. De reunirse esas condiciones, se concreta la adopción permanente.

**Hogares de tránsito, una necesidad**

Por otra parte, a raíz de la gran cantidad de operativos que lleva adelante la Municipalidad para erradicar el maltrato animal, se requieren personas dispuestas a guardar en tránsito a los animales rescatados hasta que la Justicia ponga a disposición su resguardo. Cabe aclarar, que en todos los casos, primero son atendidos por veterinarios de la Municipalidad. Además de perros y gatos, actualmente hay más de 30 gallos que se utilizaban para riñas.

Las personas que estén interesadas en colaborar se pueden contactar a través de las redes sociales de Imusa, o acercarse personalmente a la sede del Parque Garay (Obispo Gelabert 3691).

**Una vuelta por el parque**

Por otro lado, para que la estadía en el Imusa o en los diferentes lugares de guarda sea más amena, se pone en marcha la posibilidad de que las personas que deseen compartir un momento de juego o de paseo por el Parque Garay con los perros lo puedan hacer. De lunes a viernes, de 8 a 17, se brinda esta posibilidad de compartir juntos a los canes. A cada uno de los interesados se les brinda una correa y un collar para que puedan pasear o jugar durante un tiempo. “Nuestro objetivo es brindarles a los perros un espacio de recreación mientras estén acá”, indicó Oscar Cardozo, trabajador del Instituto Municipal de Salud Animal.