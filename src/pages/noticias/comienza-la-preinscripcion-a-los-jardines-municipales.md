---
category: La Ciudad
date: 2021-10-02T06:00:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/JARDINES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Comienza la preinscripción a los Jardines Municipales
title: Comienza la preinscripción a los Jardines Municipales
entradilla: Las familias podrán completar el trámite entre el 4 y el 22 de octubre,
  de manera presencial, en el Jardín Municipal de su barrio.

---
La Municipalidad convoca a familias interesadas en que sus niños y niñas, de entre 45 días y tres años, asistan a los Jardines Municipales en 2022. El período de preinscripción se extenderá entre el 4 y el 22 de octubre.

Esta primera etapa consiste en una preinscripción que se realiza en forma presencial en el Jardín Municipal del barrio, de lunes a viernes, de 9 a 11 y de 14 a 16 horas. Para completar el trámite, el adulto deberá llevar el DNI del niño o niña a preinscribir.

Según se explicó, esta instancia está destinada a niños y niñas que no asisten actualmente a los Jardines Municipales. Terminado el período de preinscripciones, las familias que por alguna razón no se acercaron al Jardín, podrán anotarse en la lista de aspirantes a ingreso.

**Jardines de puertas abiertas**

En el contexto de la emergencia sanitaria, organizar el sistema escolar para las primeras infancias fue un desafío. Esa complejidad llevó a la construcción de protocolos, gracias a un trabajo comprometido del equipo directivo, el grupo interdisciplinario y las diferentes áreas que debían comunicar a las familias las nuevas formas de funcionamiento y llevarlas adelante.

Desde el año pasado, el equipo interdisciplinario y las directoras zonales construyeron una nueva organización, basada en las indicaciones para el cuidado de la salud con las consecuentes modificaciones de asistencia, horarios, procesos de adaptación de niñas y niños, modalidades de trabajo y propuestas pedagógicas. A través de “Aprendizajes compartidos” y “Encuentros por las infancias” se trabajó en pos de la innovación pedagógica compartiendo, junto a docentes y asistentes, los conocimientos que permitieron ampliar el campo educativo. Las escenografías para jugar, los espacios de alfabetización visual y la distribución narrativa en cada uno de los jardines son algunos de los objetivos proyectados e iniciados.

Este año, luego de un 2020 signado por la pandemia, la rigurosa organización del municipio para el cuidado de la salud, permitió que más de 1400 niñas y niños vuelvan a las salas. Asimismo, se prevé para el fin del ciclo lectivo, la realización de actos, en cada uno de los 17 Jardines Municipales, para los 583 egresados de las salas de tres años.

La subsecretaria de Gestión Cultural y Educativa, Huaira Basaber, aseguró que “los Jardines siempre están organizados para abrirse”. Y agregó que por indicación del intendente Emilio Jatón y para que pueda sostenerse el servicio y su calidad, se generaron aperturas con nuevas vinculaciones con la comunidad, tal es el caso del dispositivo Veredas para Jugar, el sostenimiento del vínculo en la virtualidad, los reemplazos necesarios y los nuevos concursos que permitan ampliar el cupo de docentes por escalafón.

“En un contexto de pandemia, esto significa un trabajo minucioso, ordenado y sostenido, en el marco de un plan de renovación pedagógica y administrativa que pudo elaborarse con todas las áreas y con el gremio”, explicó la funcionaria. Basaber remarcó, además, que toda la labor realizada, así como también la apertura de preinscripciones, “pudieron lograrse porque el equipo trabajó en forma permanente, sosteniendo los vínculos durante la virtualidad y retomando la presencialidad cuando fue posible, con la esperada revinculación”.

Respecto de la modalidad en que se desarrollarán las tareas durante el año próximo, la subsecretaria sostuvo que se trabajará como hasta ahora, teniendo en cuenta el contexto sanitario.

**Jardines Municipales**

Los espacios de educación del municipio se ubican en: Alto Verde (Roberto Galarza 246, frente a Plaza La Paz), La Guardia (Roberto Moyano S/N), Chalet (Roberto Arlt y Roque Saenz Peña), Atilio Rosso (Paseo Borges y Aguado), Barranquitas Sur (Iturraspe y Lamadrid), Pro Mejoras Barranquitas (Artigas 4100), Las Flores (Regimiento 12 de Infantería y Europa), San Lorenzo (Juan Díaz de Solís 1410), Loyola Sur (Furlong y Pedroni), San Agustín (Aroca y Chaco), Roca (Callejón Roca y Rivadavía), Varadero Sarsotti (Benteveo y Hornero), Villa Hipódromo (Roque Saenz Peña 6150), Abasto (Abipones y Demetrio Espinosa), Coronel Dorrego (Matheu y Alberdi) y Facundo Zuviría (Facundo Zuviría 8100).