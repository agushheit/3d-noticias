---
category: La Ciudad
date: 2021-01-22T03:58:48Z
thumbnail: https://assets.3dnoticias.com.ar/alumbrado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad puso en marcha el plan de iluminación
title: La Municipalidad puso en marcha el plan de iluminación
entradilla: Incluye la recuperación del alumbrado público existente, la actualización
  tecnológica de las luminarias y un nuevo sistema de iluminación mediante colocación
  de columnas con equipos LED en varios barrios de la ciudad.

---
Desde comienzos de año, la Municipalidad intensificó la atención de los reclamos relacionados con el alumbrado público, en el marco de un Plan de Iluminación que se prevé implementar durante todo el primer semestre de 2021. La iniciativa, que fuera presentada a los concejales cuando se oficializó el proyecto de Presupuesto para el año en curso, tiene como premisas la recuperación de alumbrado existente, incluyendo el mantenimiento de la red, y la nueva iluminación para barrios que actualmente no poseen un servicio eficiente.

En ese sentido, se prevén intervenciones de dos tipos: la actualización tecnológica, mediante el recambio de luces de sodio por LED, y el nuevo sistema de alumbrado mediante columnas y equipos LED.

Respecto del primero de los puntos, ya está en marcha la licitación para la compra de 250 artefactos LED que comenzarán a instalarse en el bulevar Gálvez para recambiar las existentes y solucionar el problema actual que hace que varias luminarias no enciendan. Posteriormente se continuará por calle Rivadavia y avenida Alem, donde la situación es similar. Esto permitirá reunir aparatos suficientes para ir sumando otros corredores, tal es el caso del Parque Garay, avenida Gorriti, entre otros.

La secretaria de Obras y Espacio Público, Griselda Bertoni, afirmó que el plan inició con los llamados a licitación y que se trabajará intensamente para que las tareas culminen en el primer semestre.

Sobre las intervenciones de nueva iluminación, la primera etapa contempla cinco sectores que serán beneficiados, al menos en un principio: Scarafía, donde se instalarán 100 columnas con luminarias LED con una inversión cercana a los 10 millones de pesos; Liceo Norte, donde se instalarán 61 columnas con una inversión de $ 7.500.000; Santa Marta, con 84 columnas con una inversión de $ 9.500.000; Siete Jefes, cuyo proyecto consiste en colocar 310 columnas con un monto de inversión cercano a los 31 millones de pesos; y Guadalupe, al noreste, donde se instalarán 405 columnas con sus equipos LED, cableado y tableros, con una inversión cercana a los $ 40 millones.

A esto se suman las inversiones en columnas con artefactos led en barrio Roma, con un monto de $ 9.000.000; Fomento 9 de Julio, por $ 10.000.000; y en el entorno de los hospitales de la ciudad, por $ 5.000.000. En paralelo se desarrollan los proyectos para La Vuelta del Paraguayo, los barrios Candioti Norte y Candioti Sur, y el cuadrante sur de la ciudad, estudiándose en cada caso, la forma pertinente de financiamiento.

Hoy, la ciudad cuenta con 30 mil unidades de iluminación, de las cuales menos de la mitad son columnas. Cabe aclarar que iluminar mediante columnas mejora la calidad de la prestación, al tiempo que disminuye la frecuencia del mantenimiento, lo cual posibilita estar al día con muchas más áreas de la capital provincial.

Por otra parte, el municipio presentó un proyecto ante la Nación para recibir el financiamiento que permita dotar de iluminación al barrio San Agustín. El plan, en el marco de la iniciativa “Argentina Hace I”, fue aprobado y se espera la comunicación oficial de Nación para avanzar en la licitación, por un monto que ronda los 26 millones de pesos.

**Mantenimiento de la red existente**

En primera instancia se trabaja en reparar todo lo que actualmente no funciona tanto en materia de equipos, como sobre sectores completos. Se trata de acciones básicas para el mantenimiento, como es el caso de la reparación del artefacto, el cableado, los tableros o las células fotoeléctricas.

Si bien el mantenimiento del alumbrado público se realiza desde el comienzo de la gestión, Bertoni explicó que “inicialmente encontramos una ausencia total de repuestos, de equipos y de todos los elementos necesarios para generar las reparaciones, por lo que una de las primeras acciones fue comprar todo lo necesario para comenzar con esa actividad”. Según mencionó, en el mes de marzo llegaron las primeras adquisiciones que permitieron empezar a reparar lo dañado, aunque con un impacto de retraso cercano a los 6 meses. Esto acumuló entre 8 mil y 10 mil reclamos.

Superada la compleja instancia inicial sobrevino la pandemia de COVID-19, lo que volvió a generar dificultades. A partir de la definición de la iluminación como actividad esencial, se reorganiza el personal para prestar este servicio, con los cuidados necesarios para los operarios. Esto permitió ir recuperando capacidad operativa, alcanzando en los primeros 15 días de enero una duplicación de las respuestas promedio de los meses anteriores: en ese plazo se completaron 800 intervenciones en luminarias, por lo que se estima llegar a las 1500 a fin de mes.

Cabe mencionar que las cuadrillas funcionan todos los días de la semana, de lunes a lunes, en doble turno. Hoy se trabaja con cuatro camiones hidrogrúa, al tiempo que se reparan otros que se encuentran dañados. No obstante esto, se iniciaron gestiones para la compra de una nueva hidrogrúa que permita sumar una cuadrilla extra, haciendo más eficiente la respuesta.

**“Hay que saldar una deuda”**

Bertoni indicó que la intención es “que todo el esfuerzo municipal se ponga en el bacheo y la iluminación, porque es lo que el vecino necesita en su vida de todos los días”. En ese sentido, señaló que el pedido del intendente Emilio Jatón pasa por responder con celeridad a las demandas relacionadas con el municipalismo básico. Según dijo, “es una obligación que no se puede dejar de lado”.

La secretaria mencionó que “si bien celebramos las grandes obras de infraestructura o edilicias, la iluminación es una deuda y hay que saldarla. Entonces, hay problemas que debemos resolver rápidamente”, agregó.

Por último, acotó que para “responder con celeridad al vecino en su vida cotidiana, se deben dar soluciones prácticas, porque una de nuestras premisas esenciales es que los santafesinos y las santafesinas puedan transitar con comodidad por las calles y veredas de nuestra ciudad”.

![PLAN_DE_ILUMINACION_(1)](https://ci6.googleusercontent.com/proxy/9w5ZIorzG7qh0kS1yMlqzUoSJCm4r0GMtweS_qnE6fys3Wdko5iVorDcQVUsdaUmDUZXd2_9dyhsajktTfr62hy48lNRQdsr0pFKD1HlNUgcKgTURtih6iynV13HIeHl6Pj7JVbrFSxECgfJ=s0-d-e1-ft#https://app2.dopplerfiles.com/Users/239327/Campaigns/11448849/PLAN_DE_ILUMINACION_(1).jpg)

![PLAN_DE_ILUMINACION__(2)](https://ci6.googleusercontent.com/proxy/FQGWx3zK5uy11aG0pEUelczHLM3WWPz9zpYZ8a2OuyywNwwzZcJOeOCjfZ10kIaOXl2k0eCWtvbekiKsEIiDZnk7yVKbFckcInDfTgaO9d1a51gtT-X1Otl6nYCChpJencOIYhIi6rqP_yGxyg=s0-d-e1-ft#https://app2.dopplerfiles.com/Users/239327/Campaigns/11448849/PLAN_DE_ILUMINACION__(2).jpg)

![PLAN_DE_ILUMINACION__(3)](https://ci6.googleusercontent.com/proxy/UiAyOCTFdz9GU0dR_vX9l9VZGzoLcIJr2oFXqw-SrJIt_Asd4yWL99qqgTr0WnkD6ozEbfkYF1usWl9pkQA_425A75eyQGUJ1SbVkA2fR5OKm1P4KPvtGSuPjPxUvSG13WQtIo7D0mRoX3UkAw=s0-d-e1-ft#https://app2.dopplerfiles.com/Users/239327/Campaigns/11448849/PLAN_DE_ILUMINACION__(3).jpg)

![PLAN_DE_ILUMINACION__(4)](https://ci4.googleusercontent.com/proxy/EN77OiXbTNUb6ClJk6Fb0N1T3BM2zE4g2qd7Xopq8DjQdaVIQp4MObV64fF9-4pOq9I6SBeTnuXGtAvb1hWQuHuD6gqAkC7mbMKHXc5vgqSgZ7ldxljxMQkScUXLfr38VdAPqoT3wZSDIIsHng=s0-d-e1-ft#https://app2.dopplerfiles.com/Users/239327/Campaigns/11448849/PLAN_DE_ILUMINACION__(4).jpg)

![PLAN_DE_ILUMINACION__(5)](https://ci3.googleusercontent.com/proxy/1ngdIwvuFm9awog7kZYfuKCz5EDNR31B4ERwj0HFWmX2qx0dLSCwC4ZJB50z6Iv4z_NvUvUrZeRLXBKrt4vyC0LHJhK3y9WGuPdr1iO5Rskc6I-A1K06SB4kjD9TUNlTpWmJQPwiH9jNg5A-vQ=s0-d-e1-ft#https://app2.dopplerfiles.com/Users/239327/Campaigns/11448849/PLAN_DE_ILUMINACION__(5).jpg)

![PLAN_DE_ILUMINACION__(6)](https://ci4.googleusercontent.com/proxy/xAJKmAUpy3nRq_djeIy2VX9kMSWsDz0-Jp1MUsV_l32tcXq_i80jDmnK7iW3sxRO0DCV7YklJkXd9pc_5SCJO2YZYlJ0k2y0DFbPWgo_PV512akQ9U3GySfeVUPpJgXJnu6U8ECEXAVbwmTr4A=s0-d-e1-ft#https://app2.dopplerfiles.com/Users/239327/Campaigns/11448849/PLAN_DE_ILUMINACION__(6).jpg)