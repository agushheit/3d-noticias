---
category: Agenda Ciudadana
date: 2020-11-27T10:51:08Z
thumbnail: https://assets.3dnoticias.com.ar/PLAN-SOCIAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Obras Privadas
title: La provincia autoriza las obras privadas que ocupen más de 30 trabajadores
  de la construcción
entradilla: Lo hizo a través del Decreto provincial N.º 1497. Es para todo el territorio
  provincial.

---
El gobierno de Santa Fe, a través del Decreto Nº 1497, autorizó en todo el territorio provincial los servicios vinculados a obras privadas que ocupen más de 30 empleados de la construcción, más profesionales o contratistas de distintos oficios.

Para ello, se solicitarán las siguientes condiciones:

* Autorización previa del Ministerio de Trabajo, Empleo y Seguridad Social mediante solicitud, la cual deberá incluir un protocolo particular para la obra.
* Autorización de las autoridades municipales del lugar donde se encuentra localizada la obra, tanto en relación a la cantidad de personas a ocupar de manera simultánea como del protocolo particular a aplicar.
* Que el empleador o la empleadora garanticen el traslado de los trabajadores y las trabajadoras, sin la utilización del servicio público de transporte de pasajeros. Para lo cual deberá disponer o contratar servicios de transporte y organizar los viajes de manera tal de cumplimentar las reglas generales de prevención sanitaria.

[Leer el decreto](https://assets.3dnoticias.com.ar/decreto-obras-en-construccion.pdf "Decreto")