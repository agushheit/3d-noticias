---
category: Agenda Ciudadana
date: 2021-01-27T10:25:01Z
thumbnail: https://assets.3dnoticias.com.ar/uva.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Créditos UVA: el nuevo tope y cómo se descongelarán las cuotas a partir
  de febrero'
title: 'Créditos UVA: el nuevo tope y cómo se descongelarán las cuotas a partir de
  febrero'
entradilla: El descongelamiento vence el 31 de enero. Habrán actualizaciones que irán
  del seis al nueve por ciento.

---
El ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi, aseguró que las cuotas de los créditos basados en las Unidades de Valor Adquisitivo (UVAs) no superarán “el 35 por ciento del ingreso del grupo familiar” y a partir del mes de junio se ajustará según el crecimiento salarial.

Ferraresi aseguró que “ningún crédito va a superar el 35 por ciento del ingreso del grupo familiar” e informó que “tras 10 meses congelamiento, para febrero el aumento para los que tomaron crédito por menos de 120.000 UVA se ajustará un 6% y para los de más de 120.000 UVA, un 9%”.

“Con un aumento salarial promedio del 35% hemos cuidado a los más vulnerables en este aspecto”, señaló Ferraresi, al tiempo que aclaró que a partir de junio “la fórmula UVA no existirá más sino que ajustará por crecimiento salarial”.