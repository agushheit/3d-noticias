---
category: La Ciudad
date: 2021-10-12T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/disenia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Abren inscripción para gastronómicos en La Diseña: cómo anotarse'
title: 'Abren inscripción para gastronómicos en La Diseña: cómo anotarse'
entradilla: "Se desarrollará en la Estación Belgrano del 15 al 22 de diciembre. Por
  la mejoría en la situación sanitaria se habilita un sector al aire libre para comida
  y bebida.\n\n"

---
En diciembre se llevará a cabo una nueva edición de La Diseña, la feria de emprendedores locales de la ciudad de Santa Fe. Como es habitual, tendrá lugar en las instalaciones de la Estación Belgrano, Belgrano 1100 de la capital provincial. 

 Para esta versión, los organizadores estipularon en 160 la cantidad de emprendimientos que podrán participar, dada la mejoría de las condiciones sanitarias y las consecuentes flexibilizaciones gubernamentales. 

 En esta oportunidad, la feria se desarrollará del 15 al 22 de diciembre, en la previa de las fiestas de fin de año, y contará con un espacio exclusivo de gastronomía. Según lo dispuesto por el municipio este sector se ubicará al aire libre sobre el estacionamiento Este de la Estación Belgrano (calle Avellaneda). 

 **Inscripción**

 Los interesados en participar del patio de comidas, podrán inscribirse hasta el 26 de octubre en el siguiente link: [https://forms.gle/R85wzMkkvApU19oKA](https://forms.gle/R85wzMkkvApU19oKA "https://forms.gle/R85wzMkkvApU19oKA")

 Cabe mencionar que cada puesto contará con la provisión eléctrica necesaria y un espacio de 3×4 m, aproximadamente, para montar su estructura o ubicar su stand o food truck. 

 Según informó el gobieno local, la selección se realizará según los siguientes criterios: Formalización del emprendimiento; Estructura disponible para la prestación del servicio; Oferta gastronómica; Experiencia en participación de eventos de similares características.

 Cabe destacar que se dará prioridad a los emprendimientos gastronómicos de la ciudad de Santa Fe. El día 10 de noviembre de 2021 se notificarán los emprendimientos seleccionados. Para más información, comunicarse a través del correo electrónico emprendedores@santafeciudad.gov.ar