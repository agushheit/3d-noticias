---
category: Agenda Ciudadana
date: 2021-07-11T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/AFI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: La AFI inició actuaciones para determinar responsabilidades de sus agentes
  con el golpe en Bolivia
title: La AFI inició actuaciones para determinar responsabilidades de sus agentes
  con el golpe en Bolivia
entradilla: A través de un comunicado, la agencia consignó que la investigación apuntará
  a determinar si a sus agentes "se les requirieron esas tareas" por parte de la gestión
  de Mauricio Macri.

---
La Intervención de la Agencia Federal de Inteligencia (AFI), a cargo de Cristina Caamaño, informó que "luego de haber tomado conocimiento del envío de armamento policial a la República Plurinacional de Bolivia", inició "una investigación interna con el objeto de determinar si personal del organismo desplegado en ese país realizó algún tipo de actividad que pudiese haber aportado a la consumación del Golpe de Estado" contra el entonces presidente Evo Morales.

A través de un comunicado, la AFI consignó que la investigación apuntará a determinar si a sus agentes "se les requirieron esas tareas" por parte de la gestión de Mauricio Macri.

"Asimismo, se aclara que todo avance será informado en primer término al presidente de la Nación, Alberto Fernández, para que tome la decisión que estime conveniente, en función de la gravedad de los hechos denunciados", señaló la AFI.

El canciller boliviano, Rogelio Mayta, denunció el jueves que la administración de Macri proveyó de manera clandestina "material bélico" al régimen de Jeanine Añez para reprimir protestas sociales ante el golpe de Estado contra Evo Morales, lo que llevó al presidente Alberto Fernández a expresar públicamente su "dolor y vergüenza".

Mayta, quien dio un detallado informe sobre el armamento que el Gobierno de Juntos por el Cambio habría proporcionado a Añez y recordó que la represión de 2019 tuvo su punto máximo en las llamadas "masacres de Sacaba y Senkata".

Un informe oficial y preliminar del Ministerio de Seguridad que conduce Sabrina Frederic constató "inconsistencias" en el envío de material y armamento policial por parte del gobierno de Macri a Bolivia y esa cartera adelantó que la información será actualizada en las próximas horas para "esclarecer" las denuncias realizadas por el gobierno boliviano.

Frederic estableció un plazo no mayor a 72 horas para realizar un "análisis exhaustivo sobre las posibles responsabilidades de autoridades y agentes federales" en los hechos.

El informe oficial dado a conocer da cuenta de "inconsistencia entre lo autorizado por la Agencia Nacional de Materiales Controlados (el exRenar) y lo autorizado y registrado por la Policía boliviana".

Este documento se suma a las declaraciones del ministro de Defensa de Bolivia, Edmundo Novillo, quien anunció anoche que está confirmada la existencia del original de la carta que el exjefe de la Fuerza Aérea boliviana envió al exembajador argentino Normando Álvarez García agradeciendo el envío de material policial.

Según el documento dado a conocer por la cartera de Frederic, el 11 de noviembre de 2019 la Dirección de Logística de la Gendarmería argentina, "en cumplimiento de lo ordenado por su director nacional, Gerardo José Otero, solicitó que se autorice el uso del material y equipamiento por parte del Personal de la Agrupación Fuerzas Especiales Alacrán de esa fuerza en el territorio de la República de Bolivia".