---
layout: Noticia con imagen
author: "Fuente: Diario UNO"
resumen: "Regreso de Jardines "
category: Estado Real
title: Cómo es el protocolo para el regreso de los jardines maternales en la provincia
entradilla: La ministra de Salud Sonia Martorano confirmó que las actividades de
  los jardines maternales se retomarán el 1 de diciembre. Alguno de los puntos
  más relevantes.
date: 2020-11-22T14:16:26.120Z
thumbnail: https://assets.3dnoticias.com.ar/jardin.jpg
---
La ministra de Salud de la provincia confirmó durante el reporte diario por coronavirus de Santa Fe la reapertura de algunas actividades. Confirmó que **a partir del primero de diciembre se autorizará la apertura para los jardines maternales de la provincia**, un reclamo que el sector solicitaba desde hace bastante tiempo.

Además, con respecto a ello dijo que la semana que viene se realizarán reuniones con los titulares de los espacios para definir protocolos y medidas de seguridad.

En cuanto a los protocolos y las medidas preventivas que los jardines deberán tener en cuenta para su reapertura se puede decir que **se permitirá el ingreso de menores de hasta tres años y se les pide a las instituciones realizar una entrevista previa con la familia para saber cómo se encuentra el pequeño luego de la situación de aislamiento de los pasados meses**, además de informarles cómo será el protocolo en esta nueva reapertura.

Por otro lado, las familias deberán presentar una declaración jurada en la que se deje constancia que toman conocimiento del protocolo y recomendaciones respecto al Covid-19 que se establece desde la institución. También se requerirá para la incorporación de niños un certificado de buena salud por el médico pediatra, donde se especifique que no presenta enfermedades de base conocidas que pudieran llevar a una peor evolución en caso de contraer coronavirus.

**No podrán acudir al centro pequeños que se encuentren con alguna sintomatología compatible con el Covid-19** (temperatura mayor a 37,5 °C, tos, dolor de garganta, diarrea, vómitos, dolor de cabeza, erupciones en piel u otras manifestaciones que sugieran la enfermedad) o que hayan estado en contacto con algún familiar enfermo o personas posiblemente infectadas en los últimos 14 días.

**En cuanto a la ocupación de los jardines, no se deberá superar  el 50% del factor ocupacional de la institución.** En caso de que se dispongan nuevos espacios para el trabajo con grupos de niños, esto podrá variar. Se permitirá flexibilizar la distribución de chicos del jardín y el armado de los grupos si el número de los mismos o los espacios de la institución así lo requiriera. En todos los casos se mantendrá el grupo lo más reducido posible y con la mayor estabilidad en función de la asistencia con el fin de evitar nuevos contactos.

Como criterio para la organización de los establecimientos se dispuso que es conveniente plantear grupos fijos de compañeros, de un **número reducido** de nenes –burbujas socializantes–, con un mismo docente, minimizando el contacto entre los diferentes grupos. Se sugiere comenzar con grupos de seis alumnos por docente, pudiéndose ampliar a 10 si la curva epidemiológica muestra un descenso de casos.

**Se recomienda el uso de barbijo** **(preferiblemente quirúrgico) por parte del personal educativo durante toda la jornada laboral. La utilización de mascara facial únicamente no reemplaza la protección brindada por el barbijo. Constituyen elementos de protección personal complementarios y no sustitutivos.**

Otra de las cuestiones en la que se hace hincapié son los elementos que los niños traerán de casa. Cada familia traerá al jardín una bolsa con el material que le sea requerido por la institución (mudas de uso en el jardín, pañales, chupetes, objetos de uso personal, baberos desechables, etc). Estos elementos permanecerán en la institución, que se hará cargo de su custodia y desinfección si es preciso. Se procurará que los niños no traigan juguetes de casa. Dada la importancia de los objetos de apego en un momento tan sensible, se valorará en cada caso la necesidad de los mismos: de necesitarlo, la familia se comprometerá a traerlo perfectamente limpio y éste permanecerá –de ser posible– en el jardín hasta el fin de este nuevo período de integración o adaptación, siendo desinfectado de manera diaria tras la finalización de la jornada.

**No se habilitará el uso de piletas o piletines**. En caso de necesidad podrán habilitarse juegos de agua, principalmente si las temperaturas estivales son altas, evitando que los mismos sean realizados en días u horarios con bajas temperaturas

Se suspenderá el uso de cuadernos de comunicaciones y se continuará con la comunicación virtual para conocer necesidades y aspectos que hacen al cuidado y atención del chiquito (sueño, alimentación, estado de ánimo). Se dejarán recomendaciones para el uso responsable de redes, evitando informaciones falsas o imprecisas.