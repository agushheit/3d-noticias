---
category: Agenda Ciudadana
date: 2021-07-31T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/GNC.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El GNC aumentará en agosto y advierten que en Santa Fe el precio está desfasado
  un 20%
title: El GNC aumentará en agosto y advierten que en Santa Fe el precio está desfasado
  un 20%
entradilla: Desde la Cámara de Expendedores de Combustibles y Afines de Santa Fe afirmaron
  que las estaciones trabajan a pérdida, con un precio atrasado respecto al de Paraná,
  Rafaela y Córdoba.

---
Desde la Cámara de Expendedores de Combustibles y Afines de Santa Fe (Cecasf), afirmaron a UNO que en agosto se aumentará el precio de venta del GNC en la capital santafesina. El impacto podría ser de hasta el 15% y no se descartan otros aumentos en lo que resta del año.

El vocal de Cecasf, Dalmiro Saux, en diálogo con UNO Santa Fe afirmó que "seguramente habrá un aumento en los primeros días de agosto. En cuanto a la magnitud cada uno hace sus cuentas, el precio no es uniforme ni está fijado". Actualmente el precio del metro cúbico de GNC en las estaciones de servicio santafesinas ronda entre $43 y $44, mientras que la vecina localidad de Paraná mantiene precios por encima de los $50.

La falta de regulación que existe con respecto a la fijación de un precio de venta determinado del metro cúbico de GNC provoca que cada estacionero de Gas para uso combustible ponga el precio que considere justo. Si bien desde la Federación Argentina de Expendedores de Nafta del Interior (Faeni), se impone que se debe comercializar GNC a la mitad de precio que el costo del litro de nafta Súper en la YPF de esa ciudad, la realidad es que este valor varía de una región a la otra. Esto genera disparidades en el precio de venta del GNC comercializado en una ciudad u en otra.

A propósito de esto, Saux indicó: "En Santa Fe venimos con el precio muy reprimido. Comparativamente a ciudades como Paraná, Córdoba o Rafaela estamos entre un 15 a 20% abajo". Los precios en Rosario también figuran con un atraso significativo según lo expuesto por el integrante de Cecasf. El último aumento en Santa Fe ocurrió en mayo, con un incremento del 30%.

**Un negocio "a pérdida"**

En la formación del precio del GNC para la carga de combustible no solo incide el costo del litro de nafta súper, sino también estudios de costos, sumado a la injerencia de la oferta y demanda. Esto explica que, cuando el litro de Súper ronda los $100 en la capital santafesina ($50 la mitad), el metro cúbico de GNC esté por debajo de esa línea. "En Santa Fe hay muchas estaciones, lo que genera que haya mucha competencia y provoca que el precio no pueda moverse por riesgo a quedar afuera del mercado", graficó Saux.

Sumado a esto, Saux afirmó que al precio al que se está vendiendo GNC en Santa Fe "no tenemos utilidad, vamos a pérdida y no es rentable". Y concluyó: "Necesariamente habrá un aumento de precios porque si no esto es como una olla a punto de estallar, para equilibrarse a los precios de la región (Entre Ríos, Rafaela o Córdoba), que son precios bastante razonables".