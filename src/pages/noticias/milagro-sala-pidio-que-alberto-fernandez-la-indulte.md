---
category: Agenda Ciudadana
date: 2021-02-16T08:48:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/milagro-sala.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: Milagro Sala pidió que Alberto Fernández la indulte
title: Milagro Sala pidió que Alberto Fernández la indulte
entradilla: La referente de la Tupac Amaru está presa desde 2016 por amenazas a policías
  y asociación ilícita. La Corte Suprema dejó firme la semana pasada la sentencia
  por la primera causa.

---
La líder de la Tupac Amaru, Milagro Sala, detenida desde 2016 por amenazas a policías y causas de corrupción, le pidió al presidente Alberto Fernández que "indulte" a ella y "a otros compañeros" que se encuentran presos.

"No solo a mí, sino a todos los compañeros", respondió la dirigente jujeña al ser consultada por esa posibilidad en El Destape Radio.

"Armaron 16 causas y en el 90% son los mismos testigos. Y los mismos testigos tienen cargos en el Senado, en el Congreso o son funcionarios de Gerardo Morales. ¿Entonces, cómo se soluciona esto? Porque esto no es Milagro Sala únicamente, acá lo estamos padeciendo los jujeños", continuó.

Durante la entrevista, Sala también le pidió a Fernández que actúe como el flamante presidente de Bolivia, Luis Arce: "Si Alberto hubiese tomado las cosas desde un principio, apenas asumió... Lo mismo que hizo Arce en Bolivia, que sacó esa Suprema Corte y comenzó a meter presos a todos los que mataron a los compañeros. Lamentablemente eso no pasó en Argentina".

"En Argentina la derecha no descansa, avanza, el neoliberalismo siguió avanzando. Si eso no lo entienden los que gobiernan... Estas cosas son las que te indignan", continuó.

En la misma línea se había expresado un rato antes la abogada Graciana Peñafort, directora general de Asuntos Jurídicos del Senado: "Hay determinadas situaciones donde la Corte maneja la impunidad, el indulto sería una manera de ver cómo sigue. Como restablecimiento del valor justicia. Si no, estamos aceptando como legítimos los fallos de una Corte que no lo son". Y siguió: "En el caso muy puntual de Milagro Sala, Alberto debería indultarla".

La semana pasada, la Corte Suprema de Justicia dejó firme una condena a dos años de prisión por amenazas a policías, en el marco del caso conocido como "Causa de las Bombachas", en el que en un principio había resultado absuelta. 

El máximo tribunal dejó firme la condena: "La parte recurrente no ha satisfecho el recaudo que exige demostrar la relación directa e inmediata de la pretensa cuestión federal con lo efectivamente resuelto".

Se trata de la primera condena en contra de Sala en adquirir firmeza, mientras que hay otras que aún no fueron revisadas. Milagro Sala está detenida, ahora en prisión domiciliaria, desde 2016 cuando encabezó una protesta contra el Gobierno jujeño de Gerardo Morales.

La dirigente había sido absuelta en esta causa por el beneficio de la duda en un fallo emitido por el Tribunal en lo Criminal 2 de Jujuy en diciembre del 2017, pero luego la Cámara de Casación Penal revocó esa sentencia y la condenó a tres años y dos meses de prisión efectiva y finalmente el Superior Tribunal de Justicia de esa provincia confirmó la condena y redujo la pena a dos años.

La Corte Suprema de Justicia es el principal blanco de sus críticas, ya que del máximo tribunal depende el aval, o no, a los recursos de queja interpuestos por la defensa de Sala para que la liberen, en el marco de la denominada causa "Pibes Villeros", por la que se la condenó a 13 años de prisión. Sin embargo, como la sentencia no está firme, la Corte todavía puede torcer la situación y autorizar el reclamo.

El expediente que más compromete a Sala tiene que ver con el proceso en el que se la encontró responsable de liderar una asociación ilícita y desviar más de $ 60 millones que habían sido destinados para la construcción de viviendas sociales, de 2011 a 2015, durante el gobierno de Cristina Kirchner.

Entre otras investigaciones en su contra, hasta octubre pasado estuvo imputada en la denominada "megacausa", también por sospechas de corrupción en el manejo de fondos del Estado, en la que además estaba involucrado el ex gobernador jujeño Eduardo Fellner.