---
category: Estado Real
date: 2021-02-17T08:57:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuela.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Más de 90.000 alumnos vuelven hoy a clases presenciales en la provincia de
  Santa Fe
title: Más de 90.000 alumnos vuelven hoy a clases presenciales en la provincia de
  Santa Fe
entradilla: Desde este miércoles 17 de febrero, las y los estudiantes de 7° grado
  en educación primaria y de 5° y 6° año en secundaria y técnica, retornarán a las
  aulas con el objetivo de cerrar contenidos educativos.

---
Este miércoles 17 de febrero  unos 43.000 alumnos de primaria y 50.000 de secundaria (entre orientadas y técnicas), retoman las clases presenciales con el objetivo de cerrar los contenidos educativos del año 2020 y el gobierno de la provincia, a través del Ministerio de Educación, ratificó la aplicación de protocolos sanitarios en las aulas santafesinas.

En ese marco, el subsecretario de Educación Secundaria, Gregorio Vietto, aseguró que la vuelta a la presencialidad es para “cerrar el ciclo lectivo 2020 con los denominados grupos prioritarios; esto es con los séptimos grados para escuelas primarias y quintos años para las escuelas secundarias orientadas y sextos años para las escuelas de enseñanza técnica”.

En esa línea, subrayó: “El dispositivo de afianzamiento de aprendizajes que se dio en el año 2020 se producirá desde hoy 17 de febrero hasta el 12 de marzo inclusive, y es importante destacar que en este periodo no va a haber alternancia; el sistema de alternancia será a partir del 15 de marzo”.

Y añadió: “En este período para grupos esenciales, los alumnos van a asistir todos los días y vamos a garantizar el distanciamiento de 1,5 metros, lo que va a implicar en muchas escuelas, desdoblar los grupos”.

Asimismo, explicó que “las burbujas se organizarán de acuerdo a la matrícula de cada escuela y los desdoblamientos se harán si no se pueda garantizar el distanciamiento. Por ejemplo: si tenemos un grupo de 10 alumnos y el espacio permite el distanciamiento de 1,5 metros, no habrá desdoblamiento, pero si tenemos una división de 30 alumnos y un espacio que no nos permite la distancia sanitaria, se va a desdoblar y se designará un aula a cada subgrupo”.

Y remarcó: “La institución entera se va a abocar a la presencialidad de estos grupos; todos los docentes de esa institución de van a abocar a la presencialidad de estos grupos prioritarios”.

**APORTES**

Finalmente, Vietto recordó que el gobierno de la provincia “hizo una inversión gigantesca para poner en condiciones las escuelas. De hecho, la semana pasada todas las escuelas recibieron 70.000 pesos del Fondo de Atención de Necesidades Inmediatas (FANI) para ultimar los detalles de la vuelta a la presencialidad, que se prepara desde julio de 2020, donde los docentes recibieron una capacitación, teniendo en cuenta la dinámica de la pandemia”.

“A esto se suma -concluyó el funcionario- que a fines del 2020 todas las escuelas recibieron otro FANI de 70.000 pesos para poner en condiciones las escuelas con respecto al protocolo Covid-19”.