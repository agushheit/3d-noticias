---
category: Agenda Ciudadana
date: 2021-09-17T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/INFLACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La inflación de agosto en Santa Fe fue del 2,8% y acumula en un año 53,2%
title: La inflación de agosto en Santa Fe fue del 2,8% y acumula en un año 53,2%
entradilla: Así lo informó el Instituto Provincial de Estadísticas y Censos. Se ubicó
  tres décimas por encima del nivel nacional, que fue de 2,5%.

---
El nivel general del Índice de Precios al Consumidor (IPC) de Santa Fe registró en agosto un aumento de 2,8% con relación al mes anterior y un acumulado del 53,2% en la medición interanual, informó hoy el Instituto Provincial de Estadísticas y Censos (IPEC).

En tanto, en lo que va de este año la suba de precios asciende al 33 por ciento.

Así, la inflación santafesina se ubicó tres décimas por encima del nivel nacional, que de acuerdo al Instituto Nacional de Estadísticas y Censos (Indec) fue en agosto de 2,5%.

El acumulado interanual de esta provincia, entre agosto de 2020 y el mes pasado, llegó al 53,2%, según informó hoy el organismo.

El capítulo “Alimentos y bebidas”, en tanto, se ubicó por debajo del nivel general, la mostrar un aumento de 2 puntos, mientras que “Vivienda y Servicios Básicos” cayó un 0,2%.

De acuerdo al informe, como contrapartida el capítulo “Educación” exhibió una suba de 7 puntos y “Esparcimiento” un 5,3%.