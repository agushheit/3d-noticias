---
category: Agenda Ciudadana
date: 2020-12-30T10:50:59Z
thumbnail: https://assets.3dnoticias.com.ar/29122020-vilma-ibarra-txt.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Infobae'
resumen: 'Aborto legal: los próximos pasos para que la ley entre en vigencia'
title: 'Aborto legal: los próximos pasos para que la ley entre en vigencia'
entradilla: Vilma Ibarra, secretaria Legal y Técnica y redactora del proyecto, se
  refirió a los tiempos para la promulgación y la reglamentación de la normativa.

---
Pasadas las 4 de la mañana y con 38 votos a favor, 29 en contra y una abstención, el Senado nacional sancionó la lay de legalización del aborto. Tras muchos años de lucha por parte de los colectivos de mujeres, la frustración por el resultado negativo de la votación en el mismo recinto dos años atrás y luego de doce horas de tenso debate, las militantes «verdes» soltaron toda su algarabía. Entre ellas, Vilma Ibarra, secretaria Legal y Técnica del gobierno de Alberto Fernández y redactora del proyecto que hoy se convirtió en ley, se mostró muy emocionada.

[![Mira el video](https://assets.3dnoticias.com.ar/3012-ibarra-video.webp)](https://www.youtube.com/watch?v=Vu8Siopsr9g "Reproducir")

Tras el desahogo inicial, la funcionaria volvió a tomarse un minuto frente a las cámaras de los medios de comunicación presentes en el Congreso y se refirió a los próximos pasos para que la normativa entre en vigencia. «Primero hay que promulgar y hay diez días hábiles para promulgarla. Después, seguramente con un trabajo muy importante de las distintas áreas de Mujeres (en alusión al Ministerio de Mujeres, Género y Diversidad), de Salud y demás, vamos a trabajar en una buena reglamentación”, expresó Ibarra en diálogo con el canal TN.

Y si bien no precisó el tiempo que llevará comenzar a aplicarla, afirmó: «Queremos implementarla con rapidez, con cuidado y con diálogo con los distintos sectores».

Con respecto a la reglamentación, un compromiso del presidente Alberto Fernández había destrabado el voto positivo de algunos legisladores durante las primeras horas de la tarde. En ese sentido, el ex gobernador de Río Negro y senador nacional Alberto Weretilneck y el peronista entrerriano Edgardo Kueider venían pidiendo un pequeño cambio al texto sobre la interrupción voluntaria del embarazo. Sus votos garantizarían la aprobación de una ley que, según las previsiones, tendría una votación muy ajustada.

Así, el anuncio de un acuerdo que sumaría a ambos senadores, y quizás a alguno más, fue realizado en el inicio del debate en el Senado por la pampeana Norma Durango: con acuerdo del Poder Ejecutivo se prometió el veto parcial del artículo 4 inciso B, que habilita la posibilidad de la realización de un aborto después de la semana 14 en caso de violación o en el caso de que estuviera en riesgo la vida o «la salud integral» de la persona gestante.

Como dijo claramente Durango, la palabra «integral» generaba preocupación y tras consultar al Poder Ejecutivo el Frente de Todos se comprometió a un veto parcial que significaría quitar la palabra «integral» del texto que tanto para Weretilneck como para Kueider podía habilitar todo pedido de embarazo.

Anteriormente, Ibarra había manifestado su alegría «porque esta ley que hoy se consigue es un logro de muchas décadas, muchas luchas, muchas mujeres de distintos partidos políticos, y hoy la política se hizo cargo de una demanda». Asimismo, remarcó su deseo de que «nunca más haya una mujer muerta en un aborto clandestino» y agregó que «es una ley que nos vuelve más iguales como país, nos saca a las mujeres de la clandestinidad, queremos que no haya más mujeres con perforaciones de útero porque tuvo práctica insegura».

Además, resaltó que espera «que los sistemas de salud reciban a las mujeres y las cuiden, y las traten con dignidad». Entre lágrimas, Ibarra junto a las otras funcionarias declaró que estaban «muy emocionadas»: «Es una lucha de muchos años, muchas mujeres murieron, hoy la política se puso a la cabeza y le demuestra a las mujeres que nosotras tenemos autonomía, derechos y nos reconocen en igualdad, es un gran día para muchas de las mujeres en Argentina».