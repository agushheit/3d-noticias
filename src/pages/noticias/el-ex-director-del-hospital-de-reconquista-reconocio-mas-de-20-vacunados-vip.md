---
category: Agenda Ciudadana
date: 2021-03-03T06:58:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/reconquista.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: El ex director del Hospital de Reconquista reconoció más de 20 vacunados
  VIP
title: El ex director del Hospital de Reconquista reconoció más de 20 vacunados VIP
entradilla: Además, señaló que Susana Visciglio, representante de la cooperadora del
  Hospital, fue la responsable de armar la lista que tuvo irregularidades.

---
Tras el escándalo que se generó en Reconquista por los vacunados VIP, que terminó con el desplazamiento del director, LT10 habló con Gilberto Fabián Nuzzarello, médico que estaba a cargo de dicho establecimiento.

En primer lugar, el Dr. Nuzzarello sostuvo que “yo nunca tuve conocimiento de la lista que tenía personas que no correspondían por protocolos. Susana Visciglio, representante de la cooperadora del Hospital, fue la encargada de confeccionar la lista de los trabajadores no profesionales del efector de salud. Yo estaba de licencia cuando sucedió esto. Yo, como dueño del circo me tengo que hacer cargo de los payasos”.

Además, el ex director del Hospital agregó que “soy el único que renunció junto al representante del estado. El resto está cómodamente sentado en su lugar”.

Consultado sobre cuántos vacunados VIP hubo, el Dr. Nuzzarello manifestó que “son alrededor de 20/25 personas. Algunos farmacéuticos de la ciudad que no tienen ningún vínculo con efectores de salud”.