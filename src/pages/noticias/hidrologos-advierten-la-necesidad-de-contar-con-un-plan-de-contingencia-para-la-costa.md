---
category: La Ciudad
date: 2021-12-27T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/costasantafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Hidrólogos advierten la necesidad de contar con un Plan de Contingencia para
  la Costa
title: Hidrólogos advierten la necesidad de contar con un Plan de Contingencia para
  la Costa
entradilla: "Son unos 70 mil los santafesinos que viven en zonas vulnerables defendidas
  por terraplenes. La provincia encargó a la UNL un Plan de Manejo Sustentable de
  la laguna Setúbal y sus alrededores.\n\n"

---
El río Paraná está hoy muy bajo, en extremo, apenas unos 50 centímetros por arriba del cero en el hidrómetro del Puerto local. Y la laguna Setúbal en su desembocadura sobre la Costanera santafesina es casi un manchón de greda. Pero ¿qué puede llegar a ocurrir cuando el agua vuelva a subir y alcance niveles de alerta (5,30 m) y evacuación (5,70m) en las zonas comprendidas por el valle de inundación a donde en las últimas décadas se levantaron varios barrios hoy habitados por unas 70 mil personas? Esto es desde Colastiné y La Guardia al sur, hasta Arroyo Leyes, al norte. Como también Alto Verde. ¿Son suficientes los terraplenes de defensa? Y, en caso de que algo falle, ¿cómo se evacua a toda esa gente?

 Estos son algunos de los interrogantes que los hidrólogos de la Universidad Nacional del Litoral pusieron sobre la mesa de discusión conformada a principio de mes, tras un acuerdo firmado con el gobierno provincial, a través del ministerio de Ambiente y Cambio Climático, para la conservación de los recursos naturales en el ambiente urbano de la zona de la laguna Setúbal y sus alrededores. "Si bien la laguna es uno de los símbolos representativos del paisaje de este área, no es el único elemento potente", apunta el hidrólogo Enrique Mihura, director del Programa Ambiente y Sociedad (PAS) de la Secretaría de Extensión Social y Cultural de la UNL.

 Lo que a priori parecía un estudio para proteger la contaminación por plásticos y la naturaleza, va más allá y aborda estas otras cuestiones de fondo, como son el proceso de urbanización sobre el humedal y todo lo que ello conlleva. El objetivo ahora es elaborar un Plan de Manejo Sustentable de la laguna Setúbal y sus alrededores, delimitada al Norte por el Arroyo Leyes, al Sur por los arroyos Negro y Espinillar (límite del ejido de la ciudad de Santa Fe), al Este por el límite interprovincial del río Paraná, y al Oeste por la costa oeste de la laguna sobre la Costanera. Un espacio de unos 685 kilómetros cuadrados. Más grande que la ciudad de Santa Fe y Paraná juntas.

 Con esa premisa, se conformó un equipo interinstitucional y multidisciplinario para lograr un abordaje integral de las problemáticas. Y se prevé realizar un estudio detallado del sistema para establecer las bases de un Plan de Ordenación Territorial. Los especialistas destacan que el territorio en cuestión se enmarca dentro de la denominada Micro Región Insular Santa Fe, San José del Rincón y Arroyo Leyes (MRI. Ver mapa), según el Programa de Ambiente de la UNL. La misma integra territorios atravesados por riachos, arroyos y zonas de islas de los cuales más del 70% corresponde a humedales.

 El acuerdo contempla avanzar en mecanismos de planificación urbano-regional basados en la comprensión de la integralidad del sistema, contemplando el territorio como una unidad, evitando así la sectorización y fragmentación. Se empezará a trabajar en un programa que lleve adelante este proceso de transformación respaldándose en el marco de la Ley de Ambiente y Desarrollo Sustentable de la provincia, que está vigente desde el año 2000 pero que nunca se aplicó de forma plena por falta de recursos de financiamiento.

 "En ese espacio hay una serie de conflictos socio ambientales, producto de la forma en que se ha transformado ese territorio", advirtió Mihura. "Hay que encontrar una propuesta socioeconómica y ambiental que potencie el territorio, aumente la resiliencia y vuelva sustentable todo ese espacio", manifestó, y agregó "también los negocios económicos, y a su vez que conserve y cuide el ambiente, así como su paisaje y biodiversidad".

 -¿Cómo describe esta microregión insular Santa Fe?

 -Se trata de un ecosistema muy rico, que está inserto en el valle de inundación del río Paraná, y tiene una enorme potencialidad, es extraordinario. Al igual que ocurre en la región más al sur, a donde continúa este mismo paisaje: el nuevo sitio Ramsar Delta del Paraná, con dos parques nacionales (a la altura de Diamante y Coronda) -dijo Mihura.

 Mircro Región Insular Santa Fe (abarca la ciudad y la Costa). Es el territorio que comprende el Plan de Manejo Sustentable que elaboran especialistas de la UNL, a pedido del gobierno provincial, para el cuidado del ambiente.

 **Desarrollos sustentables**

 El proyecto para proteger esta microregión contempla un proceso de corto, mediano y largo plazo. Para la UNL se trata de un gran laboratorio a cielo abierto que tiene muy entusiasmados a los científicos y especialistas de diferentes disciplinas. Quieren discutir políticas públicas y tecnologías sobre cómo llevar adelante procesos de desarrollo socioeconómico con restricciones que los transformen en sustentables.

 "Una preocupación importante es el aumento de vulnerabilidad hídrica que tiene esta microregión", advierte el especialista de la UNL. "Tenemos medidas crecidas extraordinarias que existe la posibilidad cierta de que vuelvan a ocurrir", agrega, en relación a las inundaciones de 1983 y 1992. Y advierte que "no se está utilizando la tecnología apropiada para urbanizar ese área (los barrios de la Costa)", dice Mihura. "Esto conlleva una transformación que aumenta la vulnerabilidad", alerta el especialista, a sabiendas de que se trata del mayor conflicto a abordar en el trabajo. Es que en toda esa zona "pasamos de tener 35 mil habitantes a los 70 mil actuales", grafica el hidrólogo. "Ha cambiado mucho la realidad".

 El Plan de Contingencias debe contemplar una evacuación de las alrededor de 70 mil personas que viven hoy en la Costa en 10 horas.

 Pero además esta ocupación se da en terrenos privados que están junto a otros que son fiscales, sobre lo que hay competencias locales, provinciales y nacionales. Entonces, "ello plantea el desafío de articulación de políticas públicas y acuerdos programáticos", dice Mihura. "Queremos generar un vehículo de diálogo y negociación para alcanzar esos acuerdos entre los privados y los gobiernos".

 "Hay que tener en claro los riesgos que hay cuando se construye sobre un humedal -continúa el hidrólogo-. El que lo habita tiene que saber que es un valle de inundación. Nosotros como universidad tenemos la responsabilidad de decirlo. Porque hay una mejor forma de manejar el territorio, pero hacen falta acuerdos".

 -¿Cómo se debería proteger la zona contra inundaciones?

 -El territorio tiene hoy un nivel de transformación que nos ubica en una mirada que hay que tener en cuenta para partir de allí. Son áreas inundables con muy pocos sectores elevados por sobre la cota. Lo primero que se debería haber hecho para avanzar con la urbanización es partir de esos sectores con cota elevada no inundables. Hay una batería de medidas para aplicar. Por ejemplo, se podrían haber elevado los terrenos lindantes a esas cotas altas de manera selectiva, porque no se puede levantar el terreno en cualquier parte.

 -¿Cómo influye la traza de la ruta provincial 1?

 -Es al menos discutible que su traza cruce por el medio de lo urbanizado, podría haberlo circunvalado.

 -¿Los terraplenes de defensa son suficientes?

 -Los recintos (terraplenes) se suelen construir para generar centros de prestación de servicios. Pero cuando se levantan con mayor extensión y se urbaniza adentro se crea una falsa sensación de seguridad. La gente que vive adentro está en riesgo. Entonces tenemos que ocuparnos ahora en ver cómo mejoramos esta situación con la que nos encontramos.

 **Servicios**

 Además de esto, el especialista menciona otras carencias que presenta la microregión, como es hoy el manejo de los residuos que se generan, los efluentes cloacales e industriales y la infraestructura de servicios. "Todo esto tiene un nivel de precariedad", dice, "sin pretensión de alarmar a nadie, pero hay que hacerlo".

 -¿Los vecinos de la Costa son conscientes de que habitan una zona de riesgo hídrico?

 -Sin dudas que existe un desconocimiento. Me ha tocado participar de reuniones vecinales en las que tuve que explicarles que viven en el valle de inundación del Paraná, y no es que no puedan hacerlo, pero tienen que saberlo y adecuarse a la realidad cultural y del paisaje. Con esto no se quiere generar una estampida de preocupación. Pero queremos resolver la situación, con diálogo y acuerdos. Porque si ocurre una emergencia el problema será de todos.

 -¿Qué tiene que saber aquella persona que piensa en construir hoy en la Costa?

 -En las zonas inundables hay que levantar casas en palafito (sobre columnas) o flotantes, con otro tipo de tecnología. Porque si seguimos construyendo recintos (defensas) alteramos el paisaje, cerrando una zona de llanura inundable, y esto termina en un conflicto.

 -¿Existe alguna obra prioritaria a realizar en la zona?

 -La elaboración de un Plan de Contingencias. Porque si después de esta bajante del río Paraná viene una crecida y se genera un inconveniente, hay que determinar cómo se evacua a las entre 60 y 70 mil personas que allí habitan. Y en cuanto a las medidas estructurales, se debe mejorar todo el sistema de terraplenes, porque adentro vive gente por debajo de la cota de inundación. Y si se rompe un terraplén se inunda todo en 10 horas. Luego, no se deben seguir construyendo nuevos terraplenes. Ahora van a construir otro en Arroyo Leyes, que debería ser para defender bienes pre existentes, y no para generar nuevos planes de urbanización. Por último, también se necesitan mejorar los servicios públicos. Y el manejo de los residuos sólidos urbanos.

 -Y, en particular, ¿qué se piensa hacer para preservar la laguna Setúbal?

 -Es un espejo de agua muy significativo no solo desde lo cultural y por la idiosincrasia, sino también por su biodiversidad. Debería tener un mayor nivel de cuidado que el actual. Se necesita un manejo de otro tipo, sin microbasurales ni contaminación por plásticos, ni incendios o usos indebidos.

 **Sustentabilidad**

 Todos estos aspectos están siendo trabajados por los especialistas, que recién están arrancando un proceso de largo aliento, con la intención de permanecer en el área por mucho tiempo. Desde el punto de vista técnico o científico, pretenden encontrar una tecnología para el buen manejo del área y lograr así transformarla en un territorio no problemático, con una marca y un manejo sustentable.

 En paralelo, buscarán con el trabajo los parámetros para determinar qué tipo de estrategias son más recomendables para administrar áreas inundables similares. Porque en el resto del curso del Paraná hay situaciones muy parecidas en las que luego se podrían aplicar estas políticas, teniendo en cuenta el desarrollo de tecnologías para la gobernanza. Y ello es un punto que interesa sobremanera a la UNL.

 **Preocupación por el terraplén Garello**

Los especialistas de la UNL citan en un informe presentado durante un Seminario interdisciplinario sobre el Plan de Manejo Sustentable desarrollado este mes, la preocupación de los vecinos de la costa por el estado actual del terraplén de defensa Garello (Ruta 1 - Km 3,5), a la altura de la Toma de Agua sobre el río Ubajay, en la confluencia con la desembocadura sobre el Colastiné. Un video explica cómo podría inundarse esa zona por falta de mantenimiento del terraplén.

 