---
category: La Ciudad
date: 2021-06-19T06:15:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/REGISTRO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Registro Civil: Analizan sumar puestos de documentación y bajar la espera
  de 6 meses para un turno'
title: 'Registro Civil: Analizan sumar puestos de documentación y bajar la espera
  de 6 meses para un turno'
entradilla: 'El sistema on line solo ofrece fechas a diciembre de este año. Desde
  el Registro aseguran que la cantidad de trámites atendidos es similar al 2019, previo
  a la pandemia. '

---
Santafecinos advirtieron que tienen problemas para solicitar turnos online para realizar trámites en el Registro Civil de Santa Fe, sobre todo para renovar o solicitar el Documento Nacional de Identidad (DNI) o el Pasaporte. Cuando el "turnero" digital funciona, para el trámite mencionado te ofrece un turno para el 17 de diciembre (corroborado este jueves por la mañana), es decir, una espera de seis meses para obtener el DNI y/o Pasaporte, documentos esenciales para la vida civil de un ciudadano.

Francisco Dallo, subsecretario de Registros sobre Personas Humanas y Jurídicas de la Provincia, dialogó con un medio local y explicó por qué sucede esto y los trámites que atienden en las oficinas. "Del 7 de junio al 18 de junio armamos una mini burbuja con dos equipos de trabajo, donde se redujeron actividades que son consideradas no esenciales: matrimonios; uniones convivenciales; certificado de domicilio; partidas bilingües; asesoría letrada y el tema de marginales", indicó Dallo y agregó que la atención que se hizo en estas últimas semanas fue para "los turnos otorgados para la solicitud de entrega de DNI y para partidas digitales como la búsqueda de archivo. Además, seguimos trabajando con: defunciones; nacimientos y reconocimientos; mesa de entrada, recepción de oficios judiciales y expedientes administrativos".

Tras el 2020, año marcado casi en su totalidad por la pandemia y largos meses de restricciones estrictas, la demanda para realizar trámites postergados creció durante el 2021. Para ejemplificar el crecimiento en la atención y solicitud de trámites, el subsecretario indicó: "En la ciudad de Santa Fe, en mayo de 2019 se tramitaron 3.798 DNI; en mayo de 2021 se tramitaron 3.100 DNI; en mayo de 2020 tramitamos 700 DNI. Hay una demanda altísima, a lo cual nosotros vamos a responder implementando más puestos de documentación y reforzando el área de documentación".

El funcionario informó que reforzar esta área de atención servirá para "traer aquellos turnos brindados para septiembre u octubre para más adelante y achicar la dilación entre la solicitud del turno y la fecha en que se otorga". Si bien aún no es preciso en qué lugar llevar adelante estos puestos, Dallo comentó que "la idea es en el mediano plazo largar nuevamente el programa Santa Fe Más Cerca y también proyectamos reforzar con puestos de DNI en el mismo Registro".