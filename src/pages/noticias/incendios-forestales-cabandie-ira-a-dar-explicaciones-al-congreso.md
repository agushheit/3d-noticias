---
category: Estado Real
date: 2022-02-22T09:35:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/fire-g0c39f6867_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Bicameral.com.ar
resumen: 'Incendios forestales: Cabandié irá a dar explicaciones al Congreso'
title: 'Incendios forestales: Cabandié irá a dar explicaciones al Congreso'
entradilla: los presidentes del  bloque del Frente de Todos, José Mayans, y del interbloque
  de JxC, Alfredo Cornejo, mantuvieron contactos en los cuales acordaron la presencia
  de Cabandié en el transcurso de esta semana.

---
Los incendios forestales ya consumieron el 10% del suelo de la provincia de Corrientes y la lucha de los brigadistas a contrarreloj para combatirlos no cesa. En ese marco, un grupo de senadores del interbloque de Juntos por el Cambio solicitó la última semana la presencia del ministro de Ambiente, Juan Cabandié, para brindar explicaciones. Durante el fin de semana, según pudo saber _parlamentario.com_ los presidentes del Frente de Todos, José Mayans, y del interbloque de JxC, Alfredo Cornejo, mantuvieron contactos en los cuales acordaron la presencia de Cabandié en el transcurso de esta semana.

De tal manera, según informaron las fuentes consultadas, el funcionario se presentará este miércoles a las 17 ante la Comisión de Ambiente y Desarrollo Sustentable del Senado, cuyos integrantes fueron ya confirmados a través de un decreto conocido este lunes por la tarde. Al frente de la comisión sería confirmada la senadora del Pro Gladys González, quien la presidía hasta el 10 de diciembre pasado, en tanto que la vicepresidencia sería para la rionegrina Silvina García Larraburu.

La confirmación de la realización de la reunión se dio en horas de la noche. Primero, cuando al caer la tarde se difundió un decreto de la Presidencia del Senado anunciando la constitución de la Comisión de Ambiente y Desarrollo Sustentable, y más tarde el secretario Parlamentario del Cuerpo, Marcelo Fuentes, anunció oficialmente la convocatoria de la reunión, que tendrá lugar en el Salón Illia del Senado de la Nación, con el objeto inicial de constituir la comisión y luego recibir al ministro Cabandié.

Los senadores de Juntos por el Cambio expresaron la semana pasada su “más profunda preocupación frente a los incendios que están castigando a diversas provincias de nuestro país”, y apuntaron contra el Gobierno: “Frente a tamaña emergencia instamos al Gobierno nacional para que, a través de las áreas correspondientes, actúe de manera inmediata y aumente los esfuerzos en pos de trabajar por el fin de los incendios”.