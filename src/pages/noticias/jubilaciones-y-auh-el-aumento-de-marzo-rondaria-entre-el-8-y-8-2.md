---
category: Agenda Ciudadana
date: 2021-02-11T07:03:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/jubilados.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Clarín
resumen: 'Jubilaciones y AUH: el aumento de marzo rondaría entre el 8% y 8,2%'
title: 'Jubilaciones y AUH: el aumento de marzo rondaría entre el 8% y 8,2%'
entradilla: Se ubicaría por debajo de la inflación esperada para los 3 primeros meses
  de 2021, que tendría un piso del 11%.

---
El primer aumento de las jubilaciones, pensiones y demás prestaciones sociales del año, que regirá a partir de marzo, rondará entre el 8 y 8,2%. Será el primer aumento en base a la nueva fórmula aprobada en diciembre y comprende a 18 millones de personas.

Fuentes del Gobierno informaron que el aumento ronda esos porcentajes e indicaron que si hubiese continuado la formula del Gobierno anterior, el incremento sería del 7,16%. El anuncio oficial se conocerá la semana próxima.

El incremento que se otorgará en marzo y que se mantiene sin cambios hasta mayo sería inferior a la inflación esperada para el primer trimestre de este año, que superaría el 11%. 

Como la fórmula de movilidad no tiene una garantía o un “piso” que indique que la suba jubilatoria no puede ser menor a la inflación del trimestre anterior ( + 11,4%)  con el aumento de marzo habrá una caída en el ingreso real de las 18 millones de personas incluidas en el régimen.

En consecuencia, **el haber mínimo,**  que cobra poco más de la mitad de los **jubilados y pensionados** del régimen general, **pasaría de $19.035 a $20.577** - un aumento de $ 1.542 . bY la - que cobran **los padres** de 4,3 millones de chicos y adolescentes,  subiría de $3.717 a $ 4.018: + 301 pesos.

En tanto, las 1.400.000 **pensiones no contributivas** ( 70% del haber mínimo)  **pasarían a $ 14.404** y **las 170.000 PUAM** (Pensión Universal para el Adulto Mayor ( 80% del haber mínimo) **a $ 16.462.**

El porcentaje de aumento de los haberes fue calculado por el especialista en Seguridad Social Guillermo Jáuregui, de acuerdo a las cifras de variación de los salarios que informó el INDEC y de la recaudación tributaria que va a la Seguridad Social, según AFIP.

Esas dos variables se suman por mitades y arrojan el aumento de las jubilaciones, pensiones y demás prestaciones sociales, como Asignaciones familiares o AUH, que debe aplicarse a partir del 1ª de marzo hasta el 31 de mayo.

Aún no se difundió la evolución del padrón de los beneficiarios del sistema, otra variable de la fórmula, pero se estima que su impacto en la fórmula es marginal. En síntesis, los números del 95% o más de la fórmula ya son conocidos.

Las cifras oficiales marcan que de los dos índices salariales que integran la fórmula de movilidad el Índice trimestral de Salarios del INDEC ( 9,4% ) arrojó un porcentaje superior al del RIPTE de Seguridad Social ( 8%) y de ambos la fórmula indica que se elige el mayor.

Como se toma por mitades, el primer componente de la fórmula da 4,7%.

La otra mitad de la fórmula se compone de la recaudación impositiva que va a la Seguridad, básicamente el 100% del impuesto a los débitos y créditos, el 70% de la parte impositiva del Monotributo y el del IVA. En este caso de la recaudación se detraen los reintegros a las exportaciones y el saldo se distribuye: 11% a ANSES, que a su vez se distribuye: 6,27%: a las Provincias cuyas cajas previsionales no fueron transferidas a la Nación y 93,73% a ANSES.

Jáuregui calculó que, para el trimestre, esa recaudación fue el 7% %. La mitad es 3,5 % .

En consecuencia, y como aun falta el dato de la evolución del número de beneficiarios, el aumento previsional se ubicaría entre el 8 y el 8,2%.

 O sea, 4,7% de salarios más 3,5% de la recaudación tributaria que va a la ANSeS.

Este aumento no incluye a los jubilados y pensionados de regímenes especiales, como docentes, docentes universitarios, Luz y Fuerza, Poder Judicial,  que disponen de índices propios.