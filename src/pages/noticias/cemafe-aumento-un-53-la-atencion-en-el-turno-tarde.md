---
category: La Ciudad
date: 2021-06-29T09:34:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/cemafe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'CEMAFE: aumentó un 53% la atención en el turno tarde'
title: 'CEMAFE: aumentó un 53% la atención en el turno tarde'
entradilla: El dato se desprende de un informe que demuestra el esfuerzo realizado
  sumando especialidades y prácticas médicas.

---
Las autoridades del Centro de Especialidades Médicas, CEMAFE, realizaron un informe y balance que demuestra la evolución de la atención médica en este centro que, incluso durante la pandemia, pudo cumplir con este desafío de aumentar y mejorar el servicio.

Las autoridades explicaron que “en el año 2019 la atención después de las 12 hs. no superaba el 30%, hoy llegamos a un 53%, alcanzando una de las metas propuestas y sumando 5 nuevas especialidades”.

Sebastián Calvet, director de Gestión Institucional, detalló: “Cuando asumimos una de las metas que nos propusieron desde el Ministerio de Salud y del trabajo que comenzamos, por pedido de la ministra Sonia Martorano, con el secretario de Salud, Jorge Prieto ,y la subsecretaria de 1 y 2do nivel de Salud, Marcela Fowler, fue hacer funcionar el Cemafe tanto de mañana como de tarde. Hasta ese momento la oferta de turnos era en un 70 y 80 % de mañana y solo 20 a 30 % después de las 12 del mediodía”, explicó.

Vale destacar que el CEMAFE cuenta con especialidades migradas de los grandes hospitales, donde la atención de los consultorios externos se ofrecían por la mañana, “por lo tanto teníamos que cambiar esta cultura de trabajo y sabíamos que no iba ser una tarea sencilla”, recordó Calvet.

**EL DESAFÍO AVANZAR DURANTE LA PANDEMIA**

El director de Gestión Institucional relató que “nos tocó asumir la dirección ya decretada la emergencia sanitaria por Covid, entonces creímos que era el momento para empezar a trabajar con las especialidades. Gracias al esfuerzo conjunto de los y las profesionales y equipos de salud que pudieron reorganizarse, comenzamos a trabajar en estas necesidades. Sin la predisposición y colaboración de ellos no hubiese sido posible”, sostiene.

**LOS NÚMEROS**

Desde la dirección destacan que hoy “llegamos a un 47% de disponibilidad de turnos para las especialidades de 7:30 a 12 hs. y un 53% después de las 12 y hasta las 18 hs. Sumamos nuevas especialidades que migraron del Cullen y se incrementaron las prácticas quirúrgicas en cantidad y complejidad, nos pudimos adaptar con un gran esfuerzo de todo el equipo de salud del CEMAFE a los consultorios disponibles ya que desde hace un tiempo tuvimos que incorporar camas de internación clínica y cuidados intensivos no covid”.

En el informe que presentan se evidencia que, respecto al 2019: Disminuye la ocupación en el turno mañana, se duplicó el turno siesta y se triplicó el turno tarde.

Sebastián Calvet es optimista para la era post Covid: “Esperamos volver a la normalidad de la oferta asistencial ni bien recuperemos los consultorios que hoy se encuentran ocupadas por camas de internación”.

**LA EVOLUCIÓN DE LA ESTRATEGIA**

En el año 2019 había un total de 212 profesionales agrupados en 26 especialidades. De los cuales 151 asistían en el turno mañana, 40 tenían agendas a la siesta y 22 en el turno tarde.

Se agregaron 5 especialidades: Cirugía Cabeza y Cuello, Cirugía General, Cirugía Vascular Periférica, Neurocirugía. Esto arrojó el resultado para el 2020 un total de 208 profesionales agrupados en 30 especialidades. De los cuales 145 asistían en el turno mañana, 88 tenían agendas a la siesta y 66 en el turno tarde.

Este año se agregó el equipo de la Unidad de Atención de Personas Sordas y se separó la especialidad Mastología de Ginecología.

El resultado actual es un total de 223 profesionales agrupados en 32 especialidades. De los cuales 138 asisten en el turno mañana, 82 tenían agendas a la siesta y 73 en el turno tarde.