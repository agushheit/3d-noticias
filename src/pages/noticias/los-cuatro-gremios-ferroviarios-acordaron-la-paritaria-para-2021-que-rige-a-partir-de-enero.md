---
category: Agenda Ciudadana
date: 2021-01-14T10:14:20Z
thumbnail: https://assets.3dnoticias.com.ar/trenes-argentinos.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: Los cuatro gremios ferroviarios acordaron la paritaria para 2021, que rige
  a partir de enero
title: Los cuatro gremios ferroviarios acordaron la paritaria para 2021, que rige
  a partir de enero
entradilla: Los trabajadores del sector percibirán sumas no remunerativas del 10,
  14 y 18 por ciento en enero, febrero y marzo próximos y otra mejora del 18 % a partir
  de abril.

---
Los cuatro gremios ferroviarios y las empresas de la actividad acordaron en el Ministerio de Trabajo la paritaria para este año, por lo que **los trabajadores percibirán una mejora salarial del 18 por ciento desde abril próximo y sumas no remunerativas del 10, 14 y 18 por ciento, en este mes, en febrero y en marzo**, informaron las fuentes gremiales.

Además, luego de un extenso encuentro en la cartera laboral, las organizaciones gremiales y los funcionarios convinieron la continuidad del pago de los 4 mil pesos mensuales remunerativos que los trabajadores perciben por el decreto 14/20, bajo el concepto «Adicional remunerativo acuerdo 2020», según el acta del 29 de octubre.

Las sumas no remunerativas obtenidas para enero, febrero y marzo, permitieron alcanzar un acuerdo del 18 por ciento de mejora para el primer trimestre del año, a la que se sumará otro 18 por ciento desde abril, con revisión desde el 13 de ese mes.

En total, la paritaria de los gremios y las empresas logró una **recomposición real del salario de bolsillo de aproximadamente un 34 por ciento**, indicaron los voceros.

También se consensuó un reajuste paritario extraordinario, que será liquidado de forma no remunerativa y por única vez, no regular ni habitual, para todo el personal activo.

El personal de las categorías del Grupo A percibirá un reajuste extraordinario de cuatro mil pesos; el del B, cinco mil; el del C, seis mil y el del Grupo D, siete mil pesos en febrero próximo.

Los sindicatos reclamarán, a partir del 13 de abril próximo, que el pago de los cuatro mil pesos sea incorporado a las grillas salariales por integrar el decreto oficial 14/20.

Además, sobre el salario bruto total, incluidas las bonificaciones habituales y regulares, los viáticos y la antigüedad vigente, se convino el pago no remunerativo de un 10 por ciento para enero, un 14 para febrero y un 18 para marzo próximos, además de otra mejora del 18 por ciento para abril, en tanto las partes acordaron que esa última mejora se incorpore a las grillas salariales, lo que incrementará el haber bruto total y el básico de convenio.

Sindicalistas y autoridades de las empresas se reunirán nuevamente desde el 13 de abril para evaluar «las condiciones salariales de los trabajadores», aunque convinieron ante el ministro de Trabajo, Claudio Moroni, mantener las condiciones de la paz social.

En el encuentro participaron Marcelo Bellotti y Gabriela Marcello, secretario de Trabajo y directora nacional de Relaciones y Regulaciones del Trabajo, respectivamente, y los dirigentes Sergio Sasia (Unión Ferroviaria), Omar Maturano (La Fraternidad), Carlos Galeano (jerárquicos de Apdfa) y el titular de los señaleros (ASFA), Enrique Maigua.

También lo hicieron Abel De Manuele, Jefe de Gabinete de Asesores de Transporte, y Ana Castellani, secretaria de Gestión y Empleo Público de la Jefatura de Gabinete.

Las empresas que firmaron el acuerdo fueron Sofse, Belgrano Cargas y Logística S.A., ADIF y Decahf, según confirmaron las fuentes de los sindicatos y de la cartera laboral.

«Es un acuerdo paritario muy importante en el actual contexto, porque la negociación colectiva continúa abierta y será revisada en abril», afirmó Sasia, en tanto Maturano sostuvo que «es preciso tener en cuenta que los ferroviarios no perdieron ni un solo puesto de empleo, sino que incorporaron cientos de trabajadores en las empresas».