---
category: La Ciudad
date: 2021-10-04T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/jofmu.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Concejo Municipal sancionó la creación del "Sello Verde Santa Fe"
title: El Concejo Municipal sancionó la creación del "Sello Verde Santa Fe"
entradilla: "Es para reconocer a emprendimientos, comercios, y empresas locales que
  lleven adelante programas de buenas prácticas ambientales. La norma fue impulsada
  por Jorgelina Mudallel (Bloque PJ).\n\n"

---
El Concejo Municipal sancionó por Ordenanza la creación del "Sello Verde Santa Fe", destinado a incentivar el reconocimiento a todas aquellos emprendimientos, comercios, PyMEs y empresas locales de la ciudad que cumplan con buenas prácticas ambientales y que se traduzcan en un impacto positivo o en mejoras en los procesos de producción y consumo local.

 La certificación será renovable cada dos años, previa evaluación de la autoridad Municipal de aplicación y se entregará un Sello Verde, certificación oficial que identifica a la empresa como entidad comprometida en la ciudad con el medio ambiente y que será difundido dentro de los medios de comunicación oficiales del municipio.

 **"Promover la agenda ambiental"**

"Buscamos alentar a través de esta ordenanza a todos los emprendimientos, comercios, PyMEs y empresas locales que tengan los temas ambientales y cuidado del entorno local dentro de su agenda. Debe ser una política de estado el fomento de buenas prácticas ambientales en la rama productiva, con un enfoque sustentable", señaló Mudallel, impulsora de la iniciativa.

 "Estas temáticas tienen que ser parte de la vida diaria, mediante normas específicas y concretas. Queremos incentivar por medio de este reconocimiento a todas aquellas empresas locales que aún no trabajan estas temáticas para que comiencen a hacerlo", explicó.

 "Santa Fe debe poseer una agenda ambiental ambiciosa; crear conciencia sobre los usos eficientes de la energía, el agua y los recursos naturales para alinear las políticas medioambientales locales a los Objetivos de Desarrollo Sostenible (ODS) 2030, y que estas temáticas que tanto se hablan a nivel internacional comiencen a ser parte de nuestro entorno inmediato como ciudad y región metropolitana", finalizó.