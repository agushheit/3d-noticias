---
category: Cultura
date: 2022-01-28T12:04:03-03:00
layout: Noticia con imagen
author: 3d noticias
resumen: Vuelven los domingos artísticos al Anfiteatro Martín Miguel de Güemes.
title: Vuelven los domingos artísticos al Anfiteatro Martín Miguel de Güemes.
entradilla: A partir del 30 de enero, el anfiteatro municipal Martín Miguel de Güemes
  volverá a ser escenario cada domingo del ciclo de espectáculos artísticos para toda
  la familia que organiza la Municipalidad de Santo Tomé.
thumbnail: https://assets.3dnoticias.com.ar/vuelven-domingos-anfiteatro.jpg
link_del_video: ''

---
La reapertura de este tradicional espacio será a partir de las 18 horas, con la actuación del Taller de Ritmos Latinos Mexer dirigido por Gisela Galarza; la presentación de una obra de teatro para niños a cargo de Cristian Gutiérrez; y el cierre al ritmo de la música tropical de la mano de Nico Cisneros.

Los espectáculos gratuitos estarán acompañados por la Feria de Artesanos del Río, configurando una propuesta ideal para amenizar los encuentros de amigos o las salidas familiares al aire libre.

Para la Municipalidad de Santo Tomé, el impulso de este ciclo artístico y cultural constituye una forma de generar alternativas atractivas para el encuentro y el esparcimiento, promoviendo la apropiación del espacio público por parte de los vecinos y vecinas.