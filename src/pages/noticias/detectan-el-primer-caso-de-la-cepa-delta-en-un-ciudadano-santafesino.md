---
category: La Ciudad
date: 2021-07-18T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/delta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Detectan el primer caso de la cepa Delta en un ciudadano santafesino
title: Detectan el primer caso de la cepa Delta en un ciudadano santafesino
entradilla: Se trata de un turista que regresó desde Estados Unidos -es decir, es
  portador con antecedente de viaje-. Está aislado en Ezeiza.

---
Se trata de un turista que regresó desde Estados Unidos -es decir, es portador con antecedente de viaje-. Está aislado en Ezeiza.

El Ministerio de Salud de la provincia de Santa Fe, a través de la Dirección de Epidemiologia, informó que ha sido identificada una variante Delta del SARS-CoV-2 -nuevo coronavirus- en un viajero con residencia en el territorio santafesino.

Se trata de una persona proveniente de Estados Unidos, que fue detectada como “positiva” en Ezeiza en el momento de su arribo, por lo que realizó el aislamiento pertinente en un hotel de la ciudad de Buenos Aires, “con el control médico adecuado que posteriormente le otorgó el alta epidemiológica”, aseguraron desde la cartera sanitaria provincial.

Para evitar la introducción y diseminación de esta nueva Variante de Preocupación (VOC, por sus siglas en inglés), todos los viajeros provenientes del exterior deben arribar al país con un test de PCR realizado 72 horas previas al viaje. Una vez en el país se les realiza nuevamente la prueba en Ezeiza, y en caso de arrojar un resultado negativo, deberá cumplir el aislamiento estricto (sin contacto con personas que no hayan viajado) en su domicilio u otro lugar destinado para el mismo durante 7 días.

Durante ese período serán monitoreados por si presentaran síntomas y se les realizará una nueva prueba PCR, pudiendo culminar el aislamiento si la misma resulta negativa.