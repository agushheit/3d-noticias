---
category: Agenda Ciudadana
date: 2021-10-20T06:15:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/precioscare.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Feletti: "Va a ser un Precios Cuidados mucho más amplio, con más del doble
  de productos"'
title: 'Feletti: "Va a ser un Precios Cuidados mucho más amplio, con más del doble
  de productos"'
entradilla: 'Desde este miércoles entra en vigencia una resolución para que los valores
  de productos de consumo masivo vuelvan al estipulado para el primer día del mes. '

---
El secretario de Comercio Interior, Roberto Feletti, anunció que a partir del miércoles entrará en vigencia una resolución con la que se pondrá en marcha el plan para retrotraer los precios de productos de consumo masivo a los valores vigentes al 1 de octubre pasado.  
  
En un contacto con la prensa, el funcionario dijo que en el Gobierno "hay una vocación sostenida de seguir dialogando" con las empresas y advirtió que el comunicado de prensa que emitió la Copal "no ayuda" en las conversaciones que se vienen llevando adelante.  
  
El secretario de Comercio Interior, Roberto Feletti, aseguró este martes que el nuevo programa de unos 1.400 productos con precios congelados que se publicará el miércoles en el Boletín Oficial será equivalente al vigente programa de Precios Cuidados pero "mucho más amplio, con más del doble de productos".  
  
Al respecto, en un contacto con la prensa, dijo que su objetivo es "asegurar por los 90 días que vienen cierta estabilidad en los precios para facilitar la expansión del consumo".  
  
El Gobierno publicará este miércoles en el Boletín Oficial una resolución a través de la cual se retrotraen al 1 de octubre y se congelan hasta el 7 de enero próximo los precios de unos 1.432 productos de consumo masivo, mientras continúan las reuniones con empresarios del sector para ampliar el listado de artículos, informó este martes por la noche el secretario de Comercio Interior, Roberto Feletti.  
  
En un comunicado, la Secretaría de Comercio Interior estableció la "fijación temporal de precios máximos de venta al consumidor para todos los productores, comercializadores y distribuidores" de 1.432 productos de consumo masivo en todo el territorio nacional.  
  
La decisión fue tomada en el marco de la ronda de negociaciones impulsada por la Secretaría con todos los actores que integran la cadena de producción y comercialización de los diversos rubros que conforman la canasta anunciada.  
  
El parte oficial señaló que "en los encuentros mantenidos, los funcionarios subrayaron que el crecimiento económico proyectado para este año, sumado al aumento de la circulación producto del inicio de la pospandemia, el hecho de que el último trimestre es estacionalmente el de mayor consumo y el potencial de incremento que aún tiene la capacidad instalada en la industria, conforman un conjunto de condiciones que habilitarán un aumento de las ventas".  
  
La cartera de Comercio Interior destacó que "la medida de retrotraer los precios de los productos en cuestión al 1 de octubre pasado y de mantenerlos vigentes hasta el 7 de enero de 2022 fue consensuada por un amplio porcentaje de los representantes de las empresas participantes (tanto productoras como comercializadoras), que así lo hicieron saber a las autoridades de la Secretaría".  
  
"El objetivo de la resolución es garantizar la competencia equitativa en todo el universo de las empresas que formaron parte del diálogo, evitando conceder privilegios al grupo minoritario que decidió oponerse al acuerdo", expresó el comunicado.  
  
"A partir de ahora está vigente una resolución que se conocerá mañana en el Boletín Oficial que contiene casi 1.500 productos en todo el país con los precios al 1 de octubre y que es de obligatorio cumplimientos para todas las cadenas productoras y comercializadoras", había dicho horas antes Feletti, en un contacto con la prensa.  
  
El parte oficial agregó que "la lista acordada comprende un diverso espectro de 1.432 artículos, que incluye tanto primeras marcas como productos de pequeñas y medianas empresas. Por otra parte, la resolución garantiza que en las 23 provincias y en la Ciudad Autónoma de Buenos Aires, la canasta contendrá necesariamente los siguientes rubros: Almacén, Limpieza e Higiene y Cuidado Personal".  
  
Feletti se mostró satisfecho con el volumen de la canasta lograda y con el diálogo entablado con el sector empresario durante lo que fue su primera semana al frente de la dependencia. Sin embargo, lamentó que "en el complejo escenario que viene atravesando la sociedad argentina desde 2015 a la fecha, que se vio profundizado por la pandemia, haya fracciones de la cúpula empresarial que no sean conscientes de sus privilegios y se nieguen a adoptar una actitud colaborativa y contemplativa de la dura situación que vive hoy el pueblo argentino".  
  
A pesar de esta actitud de la minoría mencionada, Feletti remarcó que seguirá "abierto al diálogo y con voluntad de construir acuerdos lo más abarcativos posibles que permitan atender la necesidad de la ciudadanía", pero enfatizó que "el principal objetivo de gestión es dar una respuesta rápida al problema que hoy representa el precio de los alimentos para la mayoría de los hogares" señaló el comunicado.  
  
La resolución tiene lugar luego de una serie de reuniones mantenidas lunes y martes con titulares de distintas empresas y presidentes de distintas entidades productoras y comercializadoras de alimentos, entre ellas el titular de la Coordinadora de las Industrias de Productos Alimenticios de Argentina (Copal), Daniel Funes de Rioja.  
  
**Comunicado de la Copal**

La Copal emitió un comunicado en el ratificó su "voluntad de colaboración y diálogo para el acuerdo". Sin embargo, advirtió que "en la convocatoria recibida no están garantizadas las condiciones para conciliar las posibilidades de los sectores frente al pedido de estabilización de precios".  
  
"A pesar del esfuerzo realizado por las empresas en enviar sus propuestas de participación, las mismas no fueron tenidas en cuenta, así como tampoco fue considerado el pedido de generar un espacio de intercambio en lo inmediato, que permita clarificar las realidades y posibilidades de los distintos sectores y encontrar en conjunto un acuerdo sostenible", dijeron desde la Copal.  
  
Pero Feletti, al informar de su decisión, aseguró que advirtió "bastante consenso entre las empresas". "De los casi 1.500 productos incluidos en la lista hay observados por las empresas 139, de los cuales 68 corresponden a observaciones de una sola empresa", detalló.  
  
Si bien no aclaró de qué empresa se trata sí dijo que fue convocada y que espera que "en estos días" pueda efectivizarse el encuentro.  
  
"Queremos asegurar por los 90 días que vienen cierta estabilidad en los precios para facilitar la expansión del consumo. Vemos un proceso de expansión de consumo y queremos que eso alcance a los asalariados y los sectores de ingresos medios", afirmó Feletti.  
  
Y agregó: "Tengo mucha confianza y apelo a la responsabilidad empresaria. No parece un gran esfuerzo para unas 60 empresas mantener precios por 90 días de 1.400 y pico de productos. No vamos a producir un quiebre en sus planes de negocios sino que aseguramos una expansión de consumo y que ellos ganen por cantidad".