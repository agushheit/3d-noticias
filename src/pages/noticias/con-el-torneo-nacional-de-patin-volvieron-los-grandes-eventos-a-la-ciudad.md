---
category: La Ciudad
date: 2021-11-13T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/PATIN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Con el Torneo Nacional de Patín, volvieron los grandes eventos a la ciudad
title: Con el Torneo Nacional de Patín, volvieron los grandes eventos a la ciudad
entradilla: "Cerca de 1800 participantes y 10.000 visitantes de 15 provincias colmaron
  la capacidad hotelera. El evento culmina este sábado.\n\n"

---
Durante siete días, los andenes de la Estación Belgrano se convirtieron en una doble pista de patín, en un acontecimiento histórico para la disciplina. En ese sentido, esta mañana se realizó un balance sobre los datos que deja hasta el momento el Campeonato Argentino de Patín Artístico.

Más de 1800 deportistas, 10.000 visitantes, capacidad hotelera colmada y, como si fuera poco, óptimos resultados deportivos para las patinadoras santafesinas, son algunos de los detalles que mencionaron en rueda de prensa Ayelén Dutruel, secretaria de Políticas de Cuidados y Acción Social del municipio; y Franco Arone, director de Turismo municipal; y Fany Ynga, presidenta de la Federación de Patinaje Artístico.

Al respecto, Dutruel destacó la importancia de “apoyar la iniciativa desde el municipio con todo lo que necesita: desde el espacio físico, hasta el acceso a la gastronomía y el alojamiento. Sabemos que Santa Fe es una ciudad de eventos y queremos potenciar eso. Por aquí han pasado más de 10.000 personas para visitar este evento que llega a la recta final y queremos agradecer a la Confederación y a la Federación de Patín, por abrirnos la posibilidad de colaborar”.

Por su parte, Arone se refirió al alcance turístico que tuvo la competencia: “Con actividades como estas, se consolida el perfil turístico de la ciudad. Tuvimos ocupación plena en la hotelería y fundamentalmente en gastronomía, comercios y técnica para eventos. El turismo genera un derrame muy importante en la economía de la ciudad”, agregó.

En consonancia, el funcionario destacó que la capital provincial “estuvo a la altura de las circunstancias en un escenario maravilloso como la Estación Belgrano y con la decisión del intendente Emilio Jatón de seguir consolidando el perfil turístico de la ciudad”.

Por otra parte, repasó que esta semana también se disputa el Campeonato Argentino Juvenil de Rugby, “un evento nacional que también fue muy importante para la ciudad y que generó mucho movimiento. Hoy podemos decir con orgullo que la ciudad vuelve a tener este tipo de eventos. Y son noticias alentadoras, pensando en lo que viene”, concluyó.

**Ciudad cordial**

Fany Ynga, autoridad máxima y referente del patín artístico, destacó: “Ya estamos culminando el torneo; nos queda hasta el mediodía de mañana. Fueron jornadas espectaculares, con clasificaciones para el Sudamericano”. Del mismo modo, agradeció la colaboración del municipio: “Fuimos a diferentes lugares y no tuvimos nunca el acompañamiento que tuvimos aquí. Ver a las autoridades trabajando diariamente al lado nuestro es admirable”, concluyó.

“Ojalá todos pensaran como lo hace Santa Fe en el deporte, después de una pandemia, y en el turismo, un sector que ha sufrido muchísimo. Aquí hubo cerca de 2000 patinadores con sus padres y con sus hijos, que compran en los quioscos, en las farmacias y en todos los negocios de la zona”, remarcó Ynga.

**Orgullo local**

Mariela Spinosi, integrante del equipo santafesino Vanguardia indicó que “se disfrutó muchísimos este torneo, en todas las categorías recibimos grandes puntajes. Estuvimos trabajando desde agosto y conseguimos que el municipio nos apoye muchísimo”, consignó.

“Nos enorgullece un montón cómo salió el evento: por primera vez en la historia, un Campeonato Nacional se hace en dos pistas simultáneas, eso no existía y fue gracias al municipio, a los papás y a los patinadores que han colaborado muchísimo”, finalizó.