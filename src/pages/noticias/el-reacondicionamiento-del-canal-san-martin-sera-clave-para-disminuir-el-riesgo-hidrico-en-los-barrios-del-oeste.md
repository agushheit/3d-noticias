---
category: Agenda Ciudadana
date: 2021-03-16T05:12:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/daniela-questa.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Santo Tomé al Día
resumen: '"El reacondicionamiento del Canal San Martín será clave para disminuir el
  riesgo hídrico en los barrios del oeste"'
title: '"El reacondicionamiento del Canal San Martín será clave para disminuir el
  riesgo hídrico en los barrios del oeste"'
entradilla: Lo afirmó la intendenta Daniela Qüesta, luego de que el gobierno provincial
  anunciara que la obra se licitará el próximo 31 de marzo, con un presupuesto superior
  a los 36 millones de pesos.

---
Daniela Qüesta mostró su satisfacción por el anuncio de la licitación de una obra de desagües clave para la zona oeste de Santo Tomé. "Estamos felices porque el reacondicionamiento del Canal San Martín es una obra que venimos solicitando a la Provincia desde el año 2009, ya que por su elevado costo es una inversión que está por fuera de las posibilidades presupuestarias del Municipio”, manifestó.

Qüesta sostuvo que “estos trabajos tan esperados van a beneficiar especialmente a los vecinos del cordón oeste de la ciudad, en particular los de barrio Lourdes y Los Hornos, quienes padecen problemas crónicos de anegamiento cada vez que ocurren lluvias de magnitud”.

Al respecto, explicó que, debido al desnivel natural del terreno con pendiente hacia el río, esos sectores también son los más afectados cuando las precipitaciones desbordan los canales rurales del Departamento Las Colonias y el desagüe de la autopista, como se evidenció en las últimas lluvias extraordinarias de diciembre de 2019.

Las gestiones para el reacondicionamiento del canal se iniciaron en 2009 e incluyeron un estudio conjunto entre el Municipio y la Provincia, además de trabajos de limpieza que se realizaron tiempo más tarde. En 2016, el Municipio volvió a impulsar el proyecto a través de diferentes notas elevadas al Ministerio de Infraestructura y la Secretaría de Recursos Hídricos, logrando que la iniciativa adquiera el rango de proyecto ejecutivo en 2018. Por su parte, luego del cambio de autoridades de provinciales, y atendiendo al fenómeno climático mencionado anteriormente, en 2020 el gobierno municipal reiteró el pedido de intervención en el desagüe.

**Detalles de la obra**

Según se informó, los trabajos comprenderán la limpieza del desagüe a cielo abierto y la instalación de alcantarillas de hormigón armado. De esta forma se incrementará la capacidad de escurrimiento del tramo de 27 kilómetros cuadrados provenientes de la zona ubicada al oeste de la autopista, disminuyendo los problemas de anegamiento.

Cabe señalar que la obra cuenta con un presupuesto oficial de $36.125.747,09 y un plazo de ejecución de 6 meses. Por su parte, el acto licitatorio se desarrollará el miércoles 31 de marzo, a las 10 horas, en la sede costanera del Ministerio de Infraestructura ubicada en Avenida Alte. Brown 4751 de la ciudad de Santa Fe.