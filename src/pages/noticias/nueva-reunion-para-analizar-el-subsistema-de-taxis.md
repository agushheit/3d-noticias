---
category: La Ciudad
date: 2021-11-24T06:00:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/taxis.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Nueva reunión para analizar el subsistema de taxis
title: Nueva reunión para analizar el subsistema de taxis
entradilla: "El cumplimiento del esquema nocturno, la mejora del servicio y el incremento
  de los costos, fueron algunos de los ejes del encuentro.\n\n"

---
Esta mañana se concretó un nuevo encuentro entre funcionarios del municipio e integrantes de la Sociedad de Taximetristas Unidos con el objetivo de analizar la estructura de costos del subsistema de transporte. Al finalizar la reunión, que se realizó en la Estación Belgrano, Lucas Crivelli, subsecretario de Movilidad y Transporte del municipio, brindó precisiones sobre lo acordado: “Estas reuniones son muy propositivas. En este caso, la idea fue analizar los costos que se llevan adelante para la prestación del servicio y, obviamente, discutir las cuestiones vinculadas con esa prestación”.

“Esperamos analizar estas variaciones y las cuestiones que nos proponen desde la Sociedad para ver cómo seguimos trabajando en conjunto”, indicó Crivelli, y aclaró que “por el momento no están definidos los precios nuevos. Hay una Ordenanza, la N° 11.661 que determina el valor en función del costo del litro de nafta Premium. Ese valor hoy es referencial, por lo que durante el encuentro, se presentaron las estructuras de costos para ver si el precio del litro de nafta es representativo”.

En esa línea, el funcionario indicó que desde el municipio se analizarán los costos exhibidos por la Sociedad de Taximetristas Unidos “para ver cómo avanzamos en ese sentido; es posible que se dé un proceso de actualización de precios porque sabemos que hubo una importante variación en los costos. También entendemos que hay un sistema de paritarias que, en el caso de los taxis, es representativo en función de lo que vale la tarifa, es decir que no hay paritaria para los trabajadores, sino que es un proporcional de la tarifa”.

**Con los números en la mesa**

Por su parte, el presidente de la Sociedad de Taximetristas Unidos de Santa Fe, Damián Cóceres, aseguró que “el balance de esta reunión es positivo y seguramente vamos a seguir encontrándonos”.

Por otra parte, se refirió al incremento registrado en los costos de tener un auto en la calle: “Presentamos todo lo que avala la Ordenanza N° 11.661 y, además, hicimos una estructura de costos que nos da más de un 37,5%. Sabemos que en todo el país está pasando lo mismo en cuanto a los precios, pero también al servicio, que están faltando choferes”.

En ese sentido, Cóceres enumeró “los costos de los insumos que usamos: una cubierta aumentó el 50%”, ejemplificó. “Los autos, que son nuestra herramienta de trabajo, de mayo hasta hoy aumentaron un 45%; y el GNC, en el mismo período, subió 47%”, dijo por último.