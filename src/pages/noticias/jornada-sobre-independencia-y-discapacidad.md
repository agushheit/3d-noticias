---
category: La Ciudad
date: 2021-12-04T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/DISCAPACIDADDISCA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jornada sobre independencia y discapacidad
title: Jornada sobre independencia y discapacidad
entradilla: Se realizará, en forma virtual, el martes 7 de diciembre, de 18 a 20.
  El encuentro es organizado por la Municipalidad en el marco del Día Internacional
  de los Derechos de las Personas con Discapacidad.

---
La Municipalidad invita a participar de la jornada “Asistencia personal, apoyo para la vida independiente”, que organiza en el marco del Día Internacional de los Derechos de las Personas con Discapacidad. Además la Comisión Asesora Municipal de Discapacidad de Santa Fe elaboró un documento en el marco de esta importante fecha que se puede consultar [aquí](https://santafeciudad.gov.ar/secretaria-de-politicas-de-cuidados-y-accion-social/accesibilidad-y-personas-con-discapacidad).

Por otro lado, la actividad prevista para el martes 7 de diciembre está destinada a profesores de educación física, docentes y personas ligadas a espacios de formación. La capacitación será de 18 a 20, con modalidad virtual. Los interesados pueden inscribirse en discapacidad@santafeciudad.gov.ar .

Esta iniciativa se enmarca en las estrategias para promover el desarrollo pleno de las personas con discapacidad, hace hincapié en el modelo de vida independiente, en el modelo social de la discapacidad, buscando igualdad, dignidad y libertad. Vivir de manera independiente implica ejercer el derecho a elegir, a tener control sobre la propia vida, tomar decisiones, participar en la comunidad, no estar institucionalizado y contar con los apoyos necesarios para que esto sea posible.

Las personas que expondrán durante el encuentro son: María Victoria Rey, secretaria de Políticas de Cuidado de la Municipalidad de Santa Fe; Gabriela Troiano, integrante de Relavin ( Red Latinoamericana de Vida Independiente); María Victoria Elicabe, Integrantes de la Asociación Azul; Julián Vázquez, presidente de la Asociación Azul; Julián Volpi, Asistente Personal y Valeria López Delzar, concejala de la ciudad de Santa Fe. La encargada de moderar el panel será Gabriela Bruno, subdirectora municipal del Programa de Accesibilidad y Derechos para las Personas con Discapacidad.

**Derechos**

Cabe señalar que la Convención Internacional sobre los Derechos de las Personas con Discapacidad en su artículo 3 plantea entre sus principios generales el respeto de la dignidad inherente, la autonomía individual, incluida la libertad de tomar las propias decisiones, y la independencia de las personas. Asimismo, en el artículo 19 “Derecho a vivir de forma independiente y a ser incluido en la comunidad” ratifica la importancia de que las personas con discapacidad puedan vivir de forma independiente a partir de la plena inclusión y participación en la comunidad en igualdad de condiciones que las demás.

“Es por ello que es fundamental que puedan recibir servicios de apoyo de la comunidad, incluida la asistencia personal que sea necesaria para facilitar su existencia y su inclusión en la comunidad y para evitar su aislamiento o separación de ésta”, explicó la subdirectora del Programa municipal de Accesibilidad.