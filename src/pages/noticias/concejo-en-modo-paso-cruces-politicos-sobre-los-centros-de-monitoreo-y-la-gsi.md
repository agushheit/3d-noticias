---
category: La Ciudad
date: 2021-08-27T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/monitoreo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Concejo en "modo PASO": cruces políticos sobre los Centros de Monitoreo
  y la GSI'
title: 'Concejo en "modo PASO": cruces políticos sobre los Centros de Monitoreo y
  la GSI'
entradilla: "Se plantearon dudas respecto de la cantidad de operarios activos en el
  centro de control de cámaras de vigilancia provincial del 911 -también en el municipal-,
  y el rol de la Guardia de Seguridad Institucional. \n\n"

---
Tras la movilización de vecinalistas que llamaron a "enlutar" la ciudad por el avance del delito y la reciente reunión entre el Ministro de Seguridad, Jorge Lagna, y el intendente Emilio Jatón, el Concejo santafesino recogió el tema "caliente" de la inseguridad en esta capital. Claro: no faltó un tinte preelectoral en las declaraciones en el recinto, puesto que gran parte de los ediles son precandidatos de cara a las PASO del 12 de septiembre próximo.

Primer punto: el funcionamiento del Centro de Monitoreo provincial. Se aprobó un expediente sobre tablas con un pedido de informes por el cual el Concejo deberá solicitar al gobierno provincial que dé detalles sobre qué cantidad de personal efectivo que se cuenta para cumplir con la actividad de monitoreo en el 911; si este personal al cual se le adjudica dicha tarea goza del carácter de personal policial, o en su caso, que preparación para actuar poseen.

También se requirió saber cuál es la cantidad de cámaras con la que cuenta este sistema de monitoreo en la ciudad, y cuál es el estado de funcionamiento de éstas; cómo se llevan a cabo las tareas de acción frente a las emergencias que se puedan presentar; cómo se articulan acciones en conjunto al Ejecutivo Municipal para generar mejores efectos de esta tarea de control, y cuáles son los resultados.

Pero el asunto no quedó sólo en las cámaras que deben velar por la seguridad de los santafesinos. La discusión se disparó hacia las competencias del gobierno local para frenar el delito, el funcionamiento del Centro de Monitoreo municipal, la falta de luminarias y la supuesta "inacción" de la Guardia de Seguridad Institucional (GSI), de la cual, en principio, funcionarían cuatro móviles de los más de 10 que tiene, según advirtió un edil opositor.

**¿Desarticulado el 911?**

Quien agitó las aguas sobre este tema -hoy muy a flor de piel en la ciudadanía- fue Carlos Suárez (UCR-Juntos por el Cambio), impulsor del pedido de informes sancionado. Cuestionó la "poca resolución" del encuentro entre el titular de la cartera de seguridad y el jefe el Palacio Municipal: "Las conclusiones de la anterior reunión, el 30 de marzo de 2021, fueron intensificar los controles de motos y remises, y unificar los sistemas de monitoreo. Y los resultados de la última reunión (por este miércoles) fueron los mismos: no tiene sentido. La gente necesita que cada uno se ponga a hacer lo que tiene que hacer", criticó.

Otra de las preocupaciones es que "se desarticuló el Centro de Monitoreo provincial en cuanto al recurso humano", dijo. "Esto implica que no se monitorean todas las cámaras que hay en la ciudad. Es lo que nos dicen los vecinos". Suárez afirmó que más de una vez en la cual que se "activa" una alerta en los grupos de WhatsApp vecinales, "los efectivos policiales demoran horas en llegar hasta el lugar, en casos de, por ejemplo, un conductor en moto y en actitud sospechosa, merodeando una cuadra".

En diálogo con la prensa, Juan J. Saleme (Frente de Todos) dijo no querer entrar en "discusiones estériles" que no lleven a ningún lado sobre si se desarticuló o no el Centro de Monitoreo de la provincia. "Lo cierto es que se incorporó tecnología, pero hay un plan que estaba desactualizado desde antes, con sistemas informáticos de 2013, totalmente obsoletos".

Y en aras de buscar soluciones en este sentido, "le propondremos al presidente del Concejo (Leandro González) que el director de Tecnologías del Ministerio de Seguridad (Fernando Villares) asista a una reunión con los concejales para que nos comente cuáles fueron las incorporaciones tecnológicas, qué es lo que falta en el 911 y cuál es el plan para poner en marcha el Centro de Operaciones Policiales. Hay una cuestión de desinversión histórica que se irá resolviendo", aseguró Saleme.

**"Hacer una vaquita"**

Suárez puso luego el foco en la GSI: "Antes al menos notábamos a las camionetas de la Guardia haciendo recorridas. Esto no se está viendo hoy. Y nos enteramos de que funcionan sólo cuatro de más de una decena de móviles. En el actual Presupuesto Municipal no se inyectaron recursos para fortalecer a la Guardia".

Inés Larriera, también de la UCR-Juntos por el Cambio, apuntó que hay problemas con las cámaras de la Peatonal (del Centro de Monitoreo municipal). "Muchas cosas dejaron de hacerse en materia de seguridad dentro de las competencias distritales. Es incontable la cantidad de reclamos por luminarias; me sorprende cuando me entero de que los vecinos hacen 'vaquitas' para comprar los focos, y otra persona de la cuadra pone su escalera alta para así poder cambiar la luminaria... Además, no se están entregando los botones de alerta", advirtió.