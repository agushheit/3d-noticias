---
category: Agenda Ciudadana
date: 2022-03-02T17:27:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-03-02NID_273933O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: PRIMERA CONVOCATORIA DEL AÑO PARA EL REGISTRO ÚNICO DE ASPIRANTES A GUARDA
  CON FINES ADOPTIVOS
title: Primera convocatoria del año para el Registro Único de Aspirantes a Guarda
  con fines adoptivos
entradilla: 'Será hasta el próximo 10 de marzo mediante un trámite totalmente gratuito
  y en línea. '

---
Este lunes, en la sede del Registro Único de Aspirantes a Guarda con fines adoptivos, la ministra de Gobierno, Justicia y Derechos Humanos, Celia Arena junto a la directora del registro, Magdalena Galli Fiant, informaron sobre la apertura del primer período de inscripción del año 2022.

Será hasta el 10 de marzo, mediante un trámite totalmente gratuito y on line, enviando el Formulario de Inscripción F1 a las casillas de correo electrónico del Registro, “registros@santafe.gov.ar” y “registrosrosario@santafe.gov.ar”

El RUAGA Provincial continúa profundizando los cambios implementados para mejorar el sistema de inscripción y evaluación de aspirantes, optimizar los registros informáticos, y agilizar las búsquedas de familias para las niñas, niños y adolescentes en situación de adoptabilidad. Para esta segunda mitad de la gestión, los objetivos más ambiciosos, se relacionan con intensificar el trabajo con la Secretaría de Niñez y con el Poder Judicial.

En ese marco, Arena destacó que en “esta dinámica que tiene el RUAGA, en la que se trabajó en simplificar los trámites, poder dar el puntapié para la convocatoria que comenzó ayer y continúa hasta el día 10 de marzo, es un hecho que nos alienta a seguir profundizando” .

“Para nuestro gobernador, Omar Perotti, este tema es muy importante, siempre lo hablo con el Secretario de Justicia y la Directora del RUAGA, de promover medidas como una ficha de inscripción más simple, para poder tener cuatro convocatorias en el año, con un equipo de profesionales propios, que hagan una evaluación e intercambio con las personas que deciden adoptar, lo cual es muy importante”, agregó la ministra.

Además, Arena instó a quienes estén interesados en el proceso de adopción a iniciar el mismo, señalando que “hoy estamos aquí, dando inicio a esta primera convocatoria, llamando y convocando a todas las personas que tengan la decisión de llevar adelante una adopción, que tenga ese espacio de amor para formar una familia, como dice la convocatoria, con la asistencia de equipos para cada situación que se vaya planteando”.

A su turno, Galli Fiant expresó que “en este primera parte de la gestión, el objetivo fue re organizar el organismo, el sistema informático, capacitar y dignificar los profesionales propios del registro y ahora en esta segunda parte los objetivos son más ambiciosos”.

“Vamos a seguir profundizando lo que está planteado, seguir con nuevas estrategias de búsqueda cuando son casos difíciles, profundizando objetivos a mediano y largo plazo, que es que el RUAGA, cumpla un rol eficaz en la meta final, que es que los chicos que están en una institución o familia solidaria, puedan finalmente integrarse a una familia adoptiva”, sumó la funcionaria.

“Creemos que es importante que intensifiquemos comunicación con dos áreas fundamentales, por un lado la Secretaria de Niñez, planteando la necesidad de que el RUAGA participe del Consejo Provincial de Niñez, Adolescencia y Familia, junto con otros organismos y ONG porque tenemos cosas para decir dentro de ese consejo, que asesora en materia de niñez, adolescencia y familia”, enfatizó seguidamente Galli Fiant.

Asimismo, agregó: “Por otro lado tenemos que intensificar los lazos con el Poder Judicial, tenemos presentado una nueva estructura del organismo, que no significa sumar más peso dentro de la organización estatal , sino al contrario, hacerla más ágil, y que nos permita el objetivo final de que los chicos se integren a las familias”.

Para finalizar la directora destacó que “crecieron los inscriptos registrados que pasaron trámites y evaluaciones por parte de los equipos, y queremos poner el eje en lo fundamental que es la espera de los chicos, la espera de los que se inscriben es importante, y los acompañamos con talleres y el programa RUAGA ACOMPAÑA, pero no debemos perder de vista el tiempo de los niños, niñas y adolescentes que esperan ser integrados a una familia”.