---
category: La Ciudad
date: 2021-09-03T06:15:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Se vienen más flexibilizaciones horarias para comercios y gastronómicos en
  Santa Fe
title: Se vienen más flexibilizaciones horarias para comercios y gastronómicos en
  Santa Fe
entradilla: El ministro de Trabajo, Juan Manuel Pusineri mencionó que el decreto que
  regirá desde este viernes apunta a flexibilizar horarios de comercios, gastronómicos
  y en mayor aforo de espectáculos deportivos.

---
Este viernes vence el decreto provincial con las normas de convivencia que rigieron durante las últimas dos semanas en Santa Fe, por lo que ya se están delineando las nuevas flexibilizaciones. Sobre esto, el ministro de Trabajo, Juan Manuel Pusineri adelantó que el decreto contemplará estirar horarios de apertura para comercios y gastronómicos, además de un mayor aforo en espectáculos deportivos.

En diálogo con los medios, Pusineri manifestó: "La agenda viene siendo conformada en los últimos días con las solicitudes. Estamos trabajando con nuevas flexibilizaciones horarias sobre todo para el comercio de cercanía para extender el horario de funcionamiento. También habrá novedades sobre el horario de circulación".

**Gastronómicos y espectáculos deportivos**

Sobre los horarios de apertura de bares y restoranes, el ministro indicó que "analizando pedidos de gastronómicos ya hubo un avance en el último decreto y ahora hay un nuevo pedido. Todo lo que podamos ir avanzando en aperturas al momento propicio con el aval de la cartera sanitaria lo vamos a ir haciendo".

Consultado por si se evalúa desde el gobierno provincial ampliar el aforo de algunos eventos deportivos, Pusineri indicó: "Algunos espectáculos deportivos requieren de un mayor aforo y eso está en agenda".

**Baile en burbuja**

El rubro de los salones de eventos, uno de los más golpeados por la pandemia, espera novedades en los próximos días sobre lo que pueda decidirse luego de la esperada "prueba piloto". Se realizará un simulacro de evento bailable en donde se utilizará el método de burbuja como protocolo de prevención ante posibles contagios.

Pusineri se hizo eco de esto y sostuvo: "Allí vamos a hacer la prueba piloto con control de Salud, de Producción y de Trabajo. Ustedes saben que la instancia del baile es la que más complejidad trae, no solo en Santa Fe sino también en el mundo".

"Hay una posibilidad de baile en burbujas que están planteando los titulares de los salones de eventos. Luego de ver los resultados de esa prueba vamos a evaluar la posibilidad de ir atendiendo ese pedido", concluyó el titular de la cartera laboral luego de afirmar que no estará contemplado una definición en el próximo decreto.