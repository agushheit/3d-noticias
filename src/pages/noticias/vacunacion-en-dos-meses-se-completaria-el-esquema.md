---
category: Agenda Ciudadana
date: 2021-06-26T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/MARTORANO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Vacunación: "En dos meses se completaría el esquema”'
title: 'Vacunación: "En dos meses se completaría el esquema”'
entradilla: Lo afirmó la ministra de Salud de la provincia de Santa Fe Sonia Martorano

---
La ministra de Salud Sonia Martorano brindó una conferencia de prensa para detallar los avances en el operativo de vacunación contra el coronavirus, se mostró confiada con el avance de la campaña en Santa Fe y la llegada frecuente de vacunas. Calculó que en poco más de dos meses se podría completar el esquema de vacunación.

"Podemos pronosticar que en dos meses se podría estar completando (el esquema de vacunación hasta los 18 años), quizás en un poquito más, todo depende de la llegada de vacunas”, observó Martorano.

La ministra destacó que con la cantidad de vacunas que están llegando y la ampliación de los operativos, todo indicaría que para septiembre se podría llegar a cubrir a toda la población vacunable, esto es a los mayores de 18 años.

Actualmente, detalló, el ritmo de inmunizaciones es de 40.000 aplicaciones diarias y se avanza en la franja de 45 a 40 años. Aseguró que se seguirá bajando hasta lo 18 y advirtió que aún no hay un criterio que avale la vacunación a personas más jóvenes aún.

Con todo, alertó contra caer en el entusiasmo de “la inmunidad del rebaño”. Advirtió que todavía se está muy lejos de esa situación, que haber sido inmunizados no elimina la necesidad de mantener los cuidados del barbijo, la higiene y el distanciamiento. Además, señaló que “el virus no tiene un comportamiento que habla que va a extinguirse, sino que va mutando”. Y en ese sentido hizo un fuerte llamado de atención a los santafesinos que viajan al exterior para que respeten la cuarentena “porque las cepas van a ingresar del exterior”.

“Hay más que nunca hay que mantener todos los cuidados”, dijo.