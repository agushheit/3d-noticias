---
category: Agenda Ciudadana
date: 2021-11-25T06:00:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILLETERA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Billetera Santa Fe seguirá hasta fin de año y en diciembre evaluarán cómo
  continuará en 2022
title: Billetera Santa Fe seguirá hasta fin de año y en diciembre evaluarán cómo continuará
  en 2022
entradilla: 'Por el momento, el gobierno no evalúa elevar el tope de reintegro de
  5.000 pesos. Notificarán sobre "cinco o seis" empresas de supermercados que habrían
  aumentado los precios congelados.

'

---
El secretario provincial de Comercio Interior, Juan Marcos Aviano, dio precisiones sobre como continuará el programa Billetera Santa Fe en lo que será su quinta etapa desde su lanzamiento. Adelantó que la billetera virtual seguirá con un tope de reintegro plantado en $5.000, el cual era uno de los puntos en los que se especulaba con modificaciones teniendo en cuenta el avance de la inflación en el bolsillo de los santafesinos.

Además planteó que varias cadenas de supermercados incumplieron el acuerdo de congelamiento en los precios de 1.495 productos acordado por el gobierno nacional con sus pares provinciales. Desde la provincia avisaron que notificaran a Nación, desde donde se tomarán las decisiones al respecto.

**Billetera Santa Fe, con tope de $5.000**

Sobre el programa Billetera Santa Fe, Aviano indicó: "Vamos a continuar con los mismos términos y condiciones hasta el 31 de diciembre. En diciembre nos queremos dedicar a evaluar cómo instrumentar la quinta etapa a partir del año 2022. El gobernador ha planteado continuarlo; de que esté toda la gestión como herramienta tecnológica, como instrumento de compra y de venta muy importante. Esperemos que sea una herramienta que perdure en el tiempo".

**Aumentos de precios**

En diálogo con las cámaras de kioscos y supermercados, el funcionario reconoció que se vieron reflejados aumentos de precios en productos abarcados en el congelamiento propuesto por el gobierno nacional. Sobre esto sostuvo: "Estamos recibiendo la información para notificar básicamente al gobierno nacional y que tome medidas en el asunto. Haciendo retraer esos aumentos. Estamos hablando de cinco o seis empresas".

"En las últimas dos semanas se están viendo algunas listas con aumentos en supermercados. Son las principales firmas cuyos artículos no están contemplados en el congelamiento, en este anclaje, que en el caso de Santa Fe son 1.465 productos. Estamos haciendo un relevamiento de eso para notificar al gobierno nacional en el caso de bebidas, bebidas alcohólicas. Listas que se están presentando hasta con un 20 por ciento de aumento", continuó sobre esto Aviano.

"Sabemos que a fin de año puede haber algún movimiento pero entendemos que debe intervenir el gobierno nacional porque no hay fundamentos, ni factores que ameriten esos incrementos", indicó en relación a esto.

"No hay un congelamiento en la carne"

Frente a la consulta por los aumentos registrados en la carne, el funcionario respondió que "es un tema más complejo que lo está tomando nuestro ministerio en el diálogo con las cámaras frigoríficas, desde la Secretaría de Agroalimentos y por supuesto el gobierno nacional".

"Entendemos que tiene que haber una mesa de trabajo y de diálogo. Hubo algunos factores que por supuesto son atendibles que podrían haber generado un aumento en la carne. Lo que queremos evitar son los abusos que por supuesto ocurren", manifestó Aviano.

De cara a la proximidad de las fiestas y el consumo de carne previsto para estas fechas, el secretario de Comercio Interior concluyó: "Nosotros estamos permanentemente inspeccionando, controlando. Cuando hay incumplimientos aplicamos el procedimiento de sanción. En el caso de la carne, no hay un congelamiento".