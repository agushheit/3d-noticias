---
category: Deportes
date: 2021-05-05T08:57:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/clásico.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia realizó la reunión operativa por el clásico santafesino
title: La provincia realizó la reunión operativa por el clásico santafesino
entradilla: Junto al municipio y los dirigentes de ambos clubes acordaron el dispositivo
  de seguridad para el partido de Colón-Unión. Habrá sanciones para los hinchas que
  incumplan las restricciones.

---
Este martes, la provincia, a través del ministerio de Seguridad, realizó la reunión operativa por el clásico santafesino, a jugarse el próximo fin de semana en Santa Fe.

Al respecto, el subsecretario de Seguridad Preventiva, Diego Llumá aclaró: "Seremos estrictos con los controles en toda la ciudad para evitar cualquier tipo de aglomeración, festejo o reunión para ver el partido o festejar el triunfo de cualquiera de los equipos".

"Volvimos a solicitar a AFA y a Superliga que se pueda jugar a las 21 horas para ajustarse a las condiciones críticas que atraviesa la provincia actualmente por la pandemia", continuó el funcionario.

Además señaló: "Tenemos dispuesto un dispositivo de contención para los posibles grupos que se puedan formar alrededor del estadio o en el recorrido de los colectivos de ambos equipos, queremos evitar que vuelva a suceder lo de Rosario".

Por último, el subsecretario indicó que “vamos a emitir sanciones a los simpatizantes que incumplan las restricciones, van a tener que cumplir el derecho de admisión. Llamamos a la responsabilidad individual pero cuando esto no suceda estará el estado y el ministerio de Seguridad para aplicar las reglas".

**EL OPERATIVO**

Debido a las restricciones vehiculares y la situación de pandemia, habrá un total de 14 cortes alrededor del estadio de Colón y se dispondrá de 120 efectivos para el operativo de seguridad.

Además, habrá agentes de la Tropa de Operaciones Especiales patrullando en la zona de los bulevares y efectivos en ambos colectivos para redirigir los recorridos ante posibles aglomeraciones.