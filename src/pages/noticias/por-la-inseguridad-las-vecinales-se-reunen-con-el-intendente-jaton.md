---
category: La Ciudad
date: 2021-03-12T07:10:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/seguridad.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: Por la inseguridad, las vecinales se reúnen con el intendente Jatón
title: Por la inseguridad, las vecinales se reúnen con el intendente Jatón
entradilla: 'El tema central: la ola de delitos en distintos puntos del ejido urbano.
  También serán recibidas por autoridades del Ministerio de Seguridad.'

---
Este viernes desde las 8.30, integrantes de la Red de Vecinales por la Seguridad de la ciudad se reunirán con el intendente Emilio Jatón en la Estación Belgrano. Habrá dos representantes vecinalistas por distrito (participarán unas 16 vecinales), como medida de observancia al protocolo Covid. El temario: la escalada de delitos que no da tregua en distintos sectores de la capital, pero seguramente se hablará de otros temas más barriales, como la falta de luminarias.

Y a las 11, en el Salón de los Bomberos, otro grupo de vecinalistas se reunirá con autoridades del Ministerio de Seguridad (no estará el ministro Marcelo Sain, pero sí funcionarios “de jerarquía”, supo este diario). “Serán reuniones de trabajo, pero esperamos concretar algo para frenar la escalada de delitos que azota a muchos barrios de la ciudad”, confió a El Litoral una fuente de la Red.

Las vecinales anotadas que confirmaron su presencia en el encuentro con las autoridades de la cartera de Seguridad son, al momento, Unión Progreso y Libertad de Barranquitas; Vecinal El Tránsito; Vecinal El Chaquito; Vecinal Villa California; Vecinal Favaloro; Fomento 9 de Julio; Vecinal Juan de Garay; Cabaña Leiva; Vecinal Candioti Sud; Vecinal Guadalupe Noreste; Vecinal Altos del Valle y Vecinal Schneider.