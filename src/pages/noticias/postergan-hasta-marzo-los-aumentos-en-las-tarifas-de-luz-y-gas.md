---
category: Agenda Ciudadana
date: 2020-12-18T11:21:18Z
thumbnail: https://assets.3dnoticias.com.ar/1812gas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Postergan hasta marzo los aumentos en las tarifas de luz y gas
title: Postergan hasta marzo los aumentos en las tarifas de luz y gas
entradilla: Mediante un DNU se dio inicio a la renegociación de tarifas vigentes con
  las prestadoras de los servicios  de gas natural por red y de transporte y distribución
  de energía eléctrica que estén bajo jurisdicción federal.

---
El Gobierno oficializó este jueves el inicio de la renegociación de la revisión tarifaria integral vigente correspondiente a las prestadoras de los servicios públicos de transporte y distribución de energía eléctrica y gas natural por red que estén bajo jurisdicción federal. Al respecto, se mantiene el congelamiento de los precios por 3 meses más.

El Gobierno avanzará con las negociaciones en el marco de lo establecido en el artículo 5° de la Ley N° 27.541 de Solidaridad Social y Reactivación Productiva en el Marco de la Emergencia Pública.

Así lo estableció el [Decreto de Necesidad y Urgencia (DNU)  1020/2020](https://assets.3dnoticias.com.ar/488479642-aviso-238668.pdf "DNU") que lleva la firma del presidente Alberto Fernández y del pleno del gabinete nacional.

Se estableció que el plazo de la renegociación no podrá exceder los dos años desde la fecha de entrada en vigencia del DNU, debiendo suspenderse hasta entonces los acuerdos correspondientes a las respectivas Revisiones Tarifarias Integrales vigentes con los alcances que en cada caso determinen los Entes Reguladores, atento existir razones de interés público.

El proceso de renegociación culminará con la suscripción de un Acta Acuerdo Definitiva sobre la Revisión Tarifaria Integral, la cual abrirá un nuevo período tarifario según los marcos regulatorios, dice el [DNU](https://assets.3dnoticias.com.ar/488479642-aviso-238668.pdf "DNU").

El Gobierno encomendó al Ente Nacional Regulador del Gas (Enargas) y al Ente Nacional Regulador de la Electricidad (Enre) la realización del proceso de renegociación de las respectivas revisiones tarifarias.

El [DNU ](https://assets.3dnoticias.com.ar/488479642-aviso-238668.pdf "DNU")aclara que dentro del proceso de renegociación podrán preverse adecuaciones transitorias de tarifas o su segmentación, según corresponda, propendiendo a la continuidad y normal prestación de los servicios públicos involucrados.

Los acuerdos definitivos o transitorios de renegociación deberán formalizarse mediante actas acuerdo con las concesionarias o licenciatarias y los titulares del Enargas y del Enre, así como del ministro de Economía, Martín Guzmán, quienes los suscribirán «ad referendum» del Poder Ejecutivo.

# **Congelamiento de tarifa**

En el artículo 11, el Gobierno prorrogó por 90 días el congelamiento de las tarifas previsto en la Ley de Solidaridad Social y Reactivación Productiva que estaba próximo a vencer.

Por otra parte, se extendió la intervención del ENRE y del ENARGAS por un año o «hasta que se finalice la renegociación de la revisión tarifaria».