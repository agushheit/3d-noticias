---
category: Estado Real
date: 2021-12-31T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/costamagna.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Producción, Salud y Trabajo mantuvieron un encuentro con representantes empresariales
  y laborales de la provincia
title: Producción, Salud y Trabajo mantuvieron un encuentro con representantes empresariales
  y laborales de la provincia
entradilla: En diálogo con más de 120 entidades santafesinas, los ministros Costamagna,
  Martorano y Pusineri plantearon la necesidad de retomar el estricto cuidado de protocolos
  sanitarios.

---
Este jueves, los ministerios de Producción, Ciencia y Tecnología, Salud, y Trabajo, Empleo y Seguridad Social mantuvieron un encuentro de trabajo con entidades de todas las actividades económicas de la provincia. En la oportunidad, los respectivos ministros Daniel Costamagna, Sonia Martorano y Juan Manuel Pusineri compartieron con los representantes sectoriales de 121 entidades provinciales el panorama sanitario, productivo y laboral de Santa Fe.

En este marco, Costamagna afirmó: “Reiteramos el agradecimiento por haber podido trabajar juntos de forma articulada, en una situación cargada de complejidades. Esto ha permitido que Santa Fe haya seguido a la vanguardia de las distintas actividades productivas y estar a la altura de los acontecimientos”.

Además, el ministro destacó el trabajo articulado entre las tres carteras presentes: “Estamos los tres ministerios porque es nuestra forma de trabajar. Es un marco difícil, en el cual la agenda de salud estará por encima de otras. Deseamos plantearnos el desafío de seguir trabajando juntos en el 2022, cuidando fundamentalmente a los trabajadores y siendo muy cautelosos con las cuestiones de salud, a los fines de que podamos seguir manteniendo el alto funcionamiento de todos los rubros productivos de la provincia”.

Por su parte, Martorano dio cuenta pormenorizada de la situación sanitaria que atraviesa la provincia en relación a la pandemia de Covid 19: “Sabíamos que íbamos a tener un aumento de casos con el aumento de aforo a los eventos masivos. Lo que nos genera preocupación no es tanto la situación actual sino el rápido aumento. Estamos haciendo un gran número de testeos en toda la provincia, pero la positividad que hace dos semanas era del 2 por ciento anteayer dieron 22 por ciento, 26 ayer, y al privado le dio 40”.

Asimismo, respecto del nivel de vacunación, la titular de la cartera sanitaria aseguró: “El 80 por ciento de la población tiene dos dosis, es un buen número, pero no es nuestro techo porque seguimos vacunando”. Además, acerca de la disponibilidad del sistema de salud, la ministra explicó que no se ve una incidencia del aumento de casos en la ocupación de camas.

Finalmente, Pusineri aseguró: “Todo lo que hacemos es para continuar con la recuperación de nuestra actividad económica. A mayor cantidad de casos hay mayor probabilidad de que esto termine impactando en el sistema sanitario. Necesitamos el compromiso de volver a mirar los protocolos, de volver a aplicar aquellas reglas que conocemos, ajustar los controles hacia el interior, cumplir con las instancias de aislamiento de casos positivos o contactos estrechos. Nosotros vamos a apostar a un esquema lo menos restrictivo posible, pero tenemos que volver a hablar de medidas que tengan que ver con lo sanitario. Una de esas medidas fue el pase sanitario y ahora tenemos que mirar hacia dentro de los establecimientos para ver cómo estamos con los protocolos.”

**PARTICIPANTES**

Participaron de la reunión representantes de entidades agropecuarias, cámaras frigoríficas, bolsas de comercio, asociaciones de pymes lácteas, cámaras de comercio exterior, instituciones del comercio mayorista y minorista, hoteleros y gastronómicos, oficinas municipales de defensa del consumidor, entidades del transporte y la industria, centros económicos, polos tecnológicos, parques industriales, agencias e instituciones para el desarrollo, entes portuarios, entre otras asociaciones ligadas a todo el arco productivo de la provincia de Santa Fe.