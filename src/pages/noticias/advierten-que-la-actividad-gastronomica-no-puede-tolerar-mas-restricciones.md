---
category: La Ciudad
date: 2021-06-28T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/barVACIO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Advierten que la actividad gastronómica no puede tolerar más restricciones
title: Advierten que la actividad gastronómica no puede tolerar más restricciones
entradilla: Desde la Asociación Empresaria Hotelera Gastronómica de Santa Fe junto
  al gremio Turismo, Hoteleros y  de Gastronómicos afirmaron que es insostenible mantener
  los negocios sin la posibilidad de extensión de los horarios.

---
El vencimiento del decreto y la no apertura de la actividad gastronómica después de las 20 hs. con atención al público, es el golpe final al sector que ya suma numerosos establecimientos cerrados con la consiguiente pérdida de puestos laborales.

Luego que de que el Gobierno Provincial anunciará el viernes que la actividad gastronómica seguirá con los mismos horarios, desde la Asociación expresaron en un comunicado que “desde marzo del 2020 la gastronomía no trabaja con normalidad, con continuas restricciones, con capacidad reducida y horarios limitados. Son 15 meses de trabajo que sólo permiten generar deudas. Los bares y restaurantes, en el mejor momento, han trabajado con el 50 % de capacidad y hasta las 2 am, luego trabajaron con el 30 % y con horarios de cierre que oscilaron entre las 19 hs. y las 23 hs., la mayor parte del tiempo alternando entre estos últimos dos”.

Por otro lado explicaron que “la limitación impuesta por la Provincia no es el único impedimento para la actividad, también lo es la falta de organización, la imprevisión respecto de otros eventos masivos que generan incremento de contagios según las declaraciones de la propia Ministra de Salud y todo ello impide el más básico planeamiento que dejan a los gastronómicos sin posibilidad de planificar la cantidad de materia prima que necesitarán, debido a que las medidas se anuncian sin anticipación, antes bien al minuto final de cada inicio de fin de semana que es sabido representa la mayor posibilidad de trabajo”.

Además, hay establecimientos que han reacondicionado espacios, modificado su infraestructura y adquiridos equipos de calefacción que funcionen en lugares ventilados con el fin de cumplir con todo lo exigido por el protocolo y ser lugares seguros. No es posible trabajar sobre la incertidumbre constante y la falta de anuncio de las medidas que luego nos enteramos que estaban adoptadas negativamente, pero se difunden tardíamente incrementando el desconcierto y la angustia del sector. Enfatizan en el comunicado presentado el viernes.

Por último, dejaron un mensaje contundente:

QUEREMOS TRABAJAR, todos somos esenciales si de eso dependen miles de puestos de trabajo, respetamos los protocolos y no queremos sumar más negocios a la lista de los cerrados.