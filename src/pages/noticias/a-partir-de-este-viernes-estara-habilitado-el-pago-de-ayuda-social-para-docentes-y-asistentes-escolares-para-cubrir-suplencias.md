---
category: Estado Real
date: 2021-01-08T10:18:47Z
thumbnail: https://assets.3dnoticias.com.ar/08-01-21-docentes.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Pago de ayuda social para docentes y asistentes escolares para cubrir suplencias
title: A partir de este viernes estará habilitado el pago de ayuda social para docentes
  y asistentes escolares para cubrir suplencias
entradilla: Se trata de aquellas personas que hayan registrado desempeño durante el
  2019 hasta el quinto lugar en los escalafones y no fueron incluidos en los pagos
  realizados anteriormente.

---
El Gobierno de Santa Fe, a través de Ministerio de Desarrollo Social, anunció que desde este jueves y por 10 días hábiles estará habilitado el pago de la ayuda social compensatoria por el monto de 10 mil pesos correspondientes a noviembre y diciembre para los docentes y asistentes escolares para cubrir suplencias de cargos. Se trata de aquellas personas que hayan registrado desempeño durante el 2019 hasta el quinto lugar en los escalafones y no fueron incluidos en los pagos realizados anteriormente.

En total son 208 nuevos beneficiarios que cumplen con lo dispuesto en el Decreto N.º 1177 del 26 de octubre de 2020 y que no estaban integrados en el anexo A del Decreto mencionado, siendo incluidos a pedido del Ministerio de Educación de Santa Fe.

**Los beneficiarios tienen 10 días hábiles para efectuar el cobro a través de las sucursales de Santa Fe Servicios más próximas a su domicilio y están siendo informados vía mail por personal del Ministerio de Desarrollo Social.**