---
category: Agenda Ciudadana
date: 2021-01-31T07:43:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/01012021-Enacom.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Perfil
resumen: La Justicia falló otra vez en contra del aumento en los servicios de telecomunicaciones
title: La Justicia falló otra vez en contra del aumento en los servicios de telecomunicaciones
entradilla: Claudio Ambrosini, presidente del ENACOM, afirmó que el montó facturado
  en exceso por las empresas será restituido como crédito en la próxima factura.

---
Este viernes la Justicia rechazó una medida cautelar de las empresas de telecomunicaciones que estipulaba un aumento en el precio del servicio. 

Esto generó más tensión entre las empresas y el gobierno nacional, conflicto que viene en aumento desde que los servicios de telecomunicaciones se decretaron esenciales.

En este contexto, el equipo de RePerfilAr se comunicó con Claudio Ambrosini, presidente del ENACOM, quien habló sobre el fallo judicial a favor del ENACOM y las negociaciones a futuro con dichas empresas de telecomunicaciones.

 “El aumento autorizado por el ENACOM para enero era del 5%. Sin embargo, algunas empresas como Telecom decidieron empezar a facturar con 20% de incremento, por eso decidimos intimarlos”, aseguró Ambrosini, quien luego completó afirmando que el 15% facturado en exceso se reintegrará como crédito en la próxima factura. 

Luego, el funcionario afirmó que las 3 medidas cautelares presentadas por las empresas de telecomunicaciones fueron rechazadas por la Justicia. Asimismo, el titular del ENACOM dijo que estarán recibiendo a las empresas en los próximos días para escuchar sus reclamos.