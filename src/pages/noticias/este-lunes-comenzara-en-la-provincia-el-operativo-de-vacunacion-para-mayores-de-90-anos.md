---
category: Estado Real
date: 2021-03-01T07:30:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNCION.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Este lunes comenzará en la provincia el operativo de vacunación para mayores
  de 90 años
title: Este lunes comenzará en la provincia el operativo de vacunación para mayores
  de 90 años
entradilla: Después de finalizar con la vacunación a 17 mil personas de la primera
  dosis en todos los geriátricos de la provincia, el Ministerio de Salud anunció que
  el cronograma continuará en la primera semana de marzo.

---
Este lunes 1 de marzo comenzará en la provincia el operativo de vacunación contra el coronavirus para los mayores de 90 años que se hayan inscripto en el registro oficial, y se hará con dosis de la vacuna Covishield.

Así lo informó el Ministerio de Salud de la provincia que dio a conocer también que el cronograma de aplicación de las vacunas está sujeto a la disponibilidad de las mismas, y que finalizó el cronograma implementado en los geriátricos de todo el territorio provincial. En la ciudad de Santa Fe, el operativo se llevará a cabo en la Esquina Encendida, ubicada en Estanislao Zeballos y Facundo Zuviría. En ese lugar habrá 15 puestos y los anotados deberán concurrir de 9 a 17.

Mientras tanto, en la ciudad de Rosario el operativo se desarrollará en el ex predio Ferial La Rural ubicado en Bv. Oroño 2493, y contará con 30 puestos de vacunación en turno mañana de 8 a 13. También se aplicarán las dosis en el Centro Municipal de Distrito Noroeste Olga y Leticia Cossettini, en Av. Provincias Unidas Bis 150. En este caso habrá 5 puestos y será de 8 a 12 y de 13 a 18.

La vacunación se concretará en otras localidades de la provincia. En Venado Tuerto el operativo se desplegará en el Hospital Alejandro Gutiérrez, en Santa Fe 1311, de 7 a 17. Lo propio sucederá en la ciudad de Rafaela donde se vacunará a mayores de 90 años con turnos confirmados en el Hospital Jaime Ferre, de Lisandro de la Torre 737, de 12 a 20.

Finalmente, en la ciudad de Reconquista, la inoculación se llevará a cabo en el Hospital Olga Stucky de Hipólito Yrigoyen 1951, de 12 a 18; y en la Región Salud de esa localidad ubicada en Iriondo 1540, en el horario de 8 a 13.