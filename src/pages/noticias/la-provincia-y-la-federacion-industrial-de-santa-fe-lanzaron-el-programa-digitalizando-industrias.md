---
category: Estado Real
date: 2020-11-28T13:48:29Z
thumbnail: https://assets.3dnoticias.com.ar/INDUSTRIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Programa Digitalizando Industrias
title: La provincia y la Federación Industrial de Santa Fe lanzaron el Programa Digitalizando
  Industrias
entradilla: La iniciativa que apunta a lograr una modernización y transformación digital
  del aparato productivo santafesino a partir de la actuación conjunta y coordinada
  del sector público y privado.

---
El ministro de Producción, Ciencia y Tecnología, Daniel Costamagna participó del lanzamiento del Programa Digitalizando Industrias, iniciativa que apunta a lograr una modernización y transformación digital del aparato productivo santafesino a partir de la actuación conjunta y coordinada del sector público y privado. En ese marco se rubricó un convenio entre el Gobierno Provincial y la Federación Industrial Santafesina (FISFE) con el objetivo de llevar adelante tareas de investigación, análisis e instrumentación del Programa.

El acuerdo fue firmado en la sede de FISFE y prevé, entre otras acciones, la intervención de todas las empresas santafesinas, universidades, centros productivos y organizaciones científico-tecnológicas, asistencia para la implementación de proyectos de Industria 4.0, promover el conocimiento y la implementación de herramientas de apoyo para generar la digitalización industrial y desarrollar el conocimiento y la competitividad para impulsar exportaciones.

Al acto de presentación y firma del acuerdo asistieron también el presidente de FISFE, Víctor Sarmiento, el secretario de Industria, Claudio Mossuz, el subsecretario de Pequeña y Mediana Industria e Innovación, Marcelo Comelli, el director Provincial de Infraestructura y Fortalecimiento Industrial, Marcelo Cogno y demás autoridades de la entidad industrial. En tanto que vía streaming participaron referentes del sector de distintos puntos de la provincia.

Durante la presentación Costamagna destacó: “En términos productivos, Santa Fe ha sido la provincia del país menos castigada por la pandemia. Ese logro, en parte, es por la fortaleza de sus instituciones y el cumplimiento de protocolos sanitarios”. En tanto que Mossuz agregó que el Programa prevé “el desarrollo de una web de autodiagnóstico, libre, gratuita y de fácil acceso, para detectar el grado de madurez digital que presentan las mipymes santafesinas; ofrecer una guía de referencia u hoja de ruta para conocer el camino y emprender el camino de la transformación digital; y promover una red de aprendizaje y desarrollo”.

Por su parte, Víctor Sarmiento, indicó: “La digitalización de la industria es muy necesaria, y también queremos dar un paso en la federalización. Por eso es importante la firma de este convenio con el Ministerio de Producción, Ciencia y Tecnología, del que serán parte cada una de las las Cámaras a nivel local. Acá mostramos que la articulación del sector público privado no se queda en una expresión de deseo. Ahora, queda plasmar esto en toda la provincia y en cada fábrica”.