---
category: Agenda Ciudadana
date: 2021-02-15T07:25:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/inflacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: AFG
resumen: "“Hay inflación y la actitud de las organizaciones sociales no lo va a resolver”,
  reconocen desde el Movimiento Evita"
title: "“Hay inflación y la actitud de las organizaciones sociales no lo va a resolver”,
  reconocen desde el Movimiento Evita"
entradilla: Así lo admitió Fernando “Chino” Navarro, dirigente de la organización
  y funcionario nacional.

---
Desde el Movimiento Evita admitieron que la campaña para relevar los valores de los productos que integran el programa Precios Cuidados no puede contener la suba de precios, sino a juntar información para que la Secretaría de Comercio pueda "controlar precios y abusos".

Así lo aseguró este domingo Fernando "Chino" Navarro, quien es dirigente de la agrupación y secretario de Relaciones Parlamentarias, Institucionales y con la Sociedad Civil, de la Jefatura de Gabinete.

"Hay inflación y la actitud de estas organizaciones sociales no lo va a resolver", reconoció el dirigente en diálogo con radio Mitre. Y agregó que los controles de precios "se hacen en este país desde tiempo inmemorial".

El viernes pasado, militantes del Evita, Barrios de Pie y la Corriente Clasista y Combativa (CCC) relevaron los valores de los productos que integran el programa Precios Cuidados de la Secretaría de Comercio en hipermercados y supermercados de todo el país, afirmaron esas agrupaciones, en el marco de la campaña de “Organización Comunitaria para la reconstrucción argentina”

"Todo ese material que se recogió en todo el país se va a hacer un informe y aportar a la secretaria de Comercio, Paula Español, que es el ámbito institucional del Gobierno para controlar precios y abusos", señaló Navarro.

Y adelantó que también realizarán reuniones con organizaciones de defensa del consumidor, para continuar con esta campaña de "defensa del bolsillo de los argentino, para futuras acciones".

La iniciativa cobró fuerza horas después de que el Indec publicara el dato de inflación de enero, 4% y fue duramente criticada desde la oposición por Patricia Bullrich.

"Usar a los piqueteros para controlar precios es inaceptable. Solo genera rencor, enfrentamiento social y le da a un grupo una herramienta de poder típica del modelo Chavista", tuiteó la titular del PRO. Y le pidió al presidente Alberto Fernández que no incentive estas prácticas que agigantan "la grieta".

Navarro evitó responderle a la dirigente opositora y afirmó que no le gusta el término "piquetero", sino prefiere hablar de "control de precios de mujeres y hombres que prestan un servicio muy importante de organizaciones sociales".