---
category: Agenda Ciudadana
date: 2021-04-29T07:48:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/corach.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Corach: “Estamos al límite en el sistema sanitario, en recursos humanos,
  en oxígeno y no vamos a poder atender a todos si seguimos en esta tendencia”'
title: 'Corach: “Estamos al límite en el sistema sanitario, en recursos humanos, en
  oxígeno y no vamos a poder atender a todos si seguimos en esta tendencia”'
entradilla: Así lo manifestó el ministro de Gestión Pública, Marcos Corach, al referirse
  a la situación epidemiológica que atraviesa la provincia.

---
El ministro de Gestión Pública de la provincia, Marcos Corach, informó que “en la provincia está al límite el sistema sanitario. También los recursos humanos y el oxígeno, si seguimos esta tendencia va a llegar el momento en que no podamos atender a todos. Hay que bajar los contagios y tomar conciencia. Lo que menos quisiera uno es que alguien no encuentre una cama para atenderse”.

“La población tiene que saber que el límite de aumento de camas ya es muy finito. Eso implica, como mínimo, que se cumplan las restricciones”, detalló el funcionario provincial y amplió: “Tuvimos que suspender las cirugías en el sistema privado en toda la provincia. En el sistema público estaban suspendidas desde el 1° de abril”.

“Sabemos que en los trabajos, donde está todo protocolizado, no se producen los mayores contagios. Es necesario cumplir con los protocolos y no socializar”, puntualizó el titular de la cartera de Gestión Pública.

**Asistencia a sectores afectados**

Con respecto a la asistencia económica para los sectores afectados en el marco de la pandemia, Corach sostuvo que “el gobernador le planteó en su momento a la Nación que, si se cierran sectores, hay que hablar de asistencia. Aún hoy la mantenemos para sectores afectados por la pandemia y que nunca pudieron abrir, como los organizadores de eventos”.

**Clases presenciales**

“Por alguna razón fueron prioritarios los docentes a la hora de definir quiénes iban a ser vacunados. Entonces si hoy están vacunados y las escuelas están cumpliendo los protocolos, es lo que nosotros decimos desde el principio, una de las cosas a preservar además del sistema sanitario es la presencialidad en las escuelas y el sistema productivo, esas siguen siendo nuestras prioridades. La presencialidad la vamos a sostener lo máximo posible”, finalizó Corach.