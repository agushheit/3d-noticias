---
category: Agenda Ciudadana
date: 2021-06-03T08:03:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/elecciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: El Senado aprobó la postergación de las Paso y de las elecciones legislativas
title: El Senado aprobó la postergación de las Paso y de las elecciones legislativas
entradilla: La Cámara alta de la Nación aprobó por mayoría la postergación por un
  mes para los comicios. Ahora se espera que provincia adhiera al nuevo calendario
  electoral

---
El Senado de la Nación aprobó este miércoles, sin debate y con el apoyo del oficialismo y de la mayoría de la oposición, el proyecto de ley que posterga por un mes las Paso y las elecciones legislativas. La iniciativa obtuvo 56 votos a favor, dos en contra y dos abstenciones, y no fue puesta a discusión porque se manifestaron problemas técnicos en la sesión especial, puntualmente en la conexión webex de la videoconferencia. Ahora, se espera que el gobernador Omar Perotti emita el decreto provincial donde adhiere al cronograma electoral nacional.

Los bloques mayoritarios del Senado estuvieron de acuerdo en posponer el cronograma electoral por aproximadamente un mes, con el objetivo de evitar aglomeraciones de público en los centros de votación durante el invierno y cuando las cifras de contagios de coronavirus son todavía altas.

Los inconvenientes técnicos se generaron cuando se estaba por votar el proyecto de ley sobre Impuesto a las Ganancias de Sociedades, inmediatamente anterior al de las elecciones.

La sesión estuvo parada durante más de veinte minutos porque, según informaron fuentes parlamentarias, no funcionaba el audio que se emitía desde el estrado del hemiciclo del Senado que en ese momento estaba ocupado por la presidenta provisional del cuerpo, la peronista santiagueña Claudia Ledesma Abdala de Zamora.

Cuando estaban por convertir en ley ese proyecto, que busca aliviar la carga fiscal para las pequeñas y medianas empresas, al proponer una estructura de alícuotas marginales escalonadas con tres segmentos en función del nivel de ganancia neta imponible acumulada, los senadores que estaban en sus provincias dejaron de recibir el sonido desde Buenos Aires.

La sesión se detuvo durante varios minutos durante los cuales los técnicos se dedicaron a resolver el problema, mientras el ministro de Economía, Martín Guzmán, esperaba en el recinto a que se terminara de votar la iniciativa que modificaba la Ley de Impuesto a las Ganancias.

Cuando se avanzó sobre la discusión del proyecto para postergar las elecciones, el jefe de la bancada oficialista, el formoseño José Mayans, solicitó que el tema sea votado sin discusión en virtud del acuerdo parlamentario que se había llegado con la oposición para respaldar el tema.

“Vamos a obviar el debate por un acuerdo al que llegamos entre todas las bancadas, ya que este tema viene con acompañamiento por unanimidad”, remarcó Mayans, ante el asentimiento de su coterráneo, el presidente del interbloque de Juntos por el Cambio, Luis Naidenoff.

La moción que superó la votación fue cuestionada por uno de los dos senadores que votó en contra, el peronista salteño Juan Carlos Romero, un aliado de la bancada macrista, quien mencionó que no estaba de acuerdo “con cambiar las reglas” y enfatizó: “Ya bastante nos cercenaron la palabra”.

“Levantar la mano con 24 o 30 pantallas no es una votación”, insistió Romero, cuya postura fue acompañada en el voto negativo por su compañera de interbloque, la neuquina Lucila Crexell.

En tanto, quienes se abstuvieron fueron el representante de PRO de Entre Ríos, Alfredo De Angeli; y la riojana Clara Vega, que también integra el Interbloque Parlamentario Federal.

Antes de que se tomara la decisión de suspender el debate, se había dispuesto una lista de oradores corta, que estaba conformada por la presidenta de la Comisión de Asuntos Constitucionales, la peronista santafecina María de los Ángeles Sacnun; sus compañeros de bancada, Mario Pais y Ana Almirón; y los mencionados Naidenoff y Romero.

Antes de que se pusiera el tema a votación, fuentes del oficialismo habían confirmado la presencia del ministro del Interior, Eduardo “Wado” De Pedro, quien no llegó a acercarse al Senado de la Nación ante el apuro de los legisladores.

De Pedro fue el funcionario que facilitó el acuerdo con la oposición a través de varias reuniones que mantuvo también con la Cámara Nacional Electoral y los apoderados de los partidos políticos.

En sus fundamentos, el proyecto que posterga por un mes el cronograma electoral plantea que "el aplazamiento de la celebración de los comicios a meses con temperaturas más elevadas permitirá optimizar la ventilación de los locales de votación y la implementación de los protocolos sanitarios".

La iniciativa también hace referencia a que, "durante ese lapso de tiempo adicional, se continuará avanzando con la masiva campaña de vacunación que lleva adelante el Estado Nacional junto a las veinticuatro jurisdicciones, coadyuvando a un mejor cuidado de la mayor cantidad posible de argentinos y argentinas que deben cumplir con su deber cívico".

Además, el proyecto establece que esta reforma electoral “no podrá ser modificada ni derogada durante el año calendario en curso en tanto regula un derecho público subjetivo de los partidos políticos, instituciones fundamentales del sistema democrático, a elegir sus candidatos a los cargos electivos previstos en la Constitución Nacional”.