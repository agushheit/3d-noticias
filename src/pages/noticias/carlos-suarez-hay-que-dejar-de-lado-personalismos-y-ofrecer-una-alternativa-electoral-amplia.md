---
category: La Ciudad
date: 2020-12-27T13:33:07Z
thumbnail: https://assets.3dnoticias.com.ar/2712-carlos-suarez1.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: "«Dejar de lado personalismos y ofrecer una alternativa electoral amplia»"
title: 'Carlos Suárez: «Hay que dejar de lado personalismos y ofrecer una alternativa
  electoral amplia»'
entradilla: El edil del bloque UCR-Juntos por el Cambio comienza a transitar su último
  año de mandato. Dice que suspender las Paso «le haría muy mal al sistema democrático».

---
Propone trabajar para generar un Frente más grande: «Hay decepción» en los vecinos. Sus ejes: medio ambiente, educación, seguridad y transporte.

<br/>

**<span style="font-family: helvetica; font-size: 1.2em; font-weight: 600;"> Fragmentos del diálogo con el Concejal:</span>**

—**Este año tan particular de pandemia no fue electoral, pero el año que viene sí lo es y en diciembre termina su mandato como concejal. Hay intención de algunos sectores de suspender las paso. ¿Cuál es su opinión?**

—Creo que este tipo de planteos tienen que ver más con el oportunismo político que con mejorar la calidad de la democracia. Está desubicado desde cualquier punto de vista porque no es cambiando las reglas de juego como uno va a mejorar la condición sanitaria, y tenemos ejemplos de todo el mundo de que las elecciones se pueden hacer. Le haría muy mal al sistema democrático alterar de esa forma las reglas de juego, se engaña a la ciudadanía planteando que tiene que ver con la pandemia. Quien tenga la birome y el poder va a armar las listas conforme su gusto y _piacere,_ quitando posibilidades de competir dentro de cada espacio político. Esto le hace mal a la democracia y a los vecinos que se quedan sin algo a lo que se acostumbraron, a elegir a sus candidatos, y esto estaría vedado solamente porque le conviene a quien tiene la posibilidad de ejercer mucha influencia en el armado de la lista.

—**¿Cuál es el rol que va a tener la oposición y, en particular, el bloque que usted representa el año próximo?**

—Creo que la oposición está llamada a tener un rol de muchísima responsabilidad. En nuestra provincia tenemos que trabajar juntos por ofrecer a los vecinos una alternativa distinta a la gestión actual que, de paso, creo que los santafesinos deben estar lamentando las decisiones electorales que se tomaron, incluso más allá de la pandemia porque en nuestra provincia están pasando muchas cosas que no tienen que ver con ella. Por eso hay que ofrecer una alternativa electoral basada en una nueva coalición electoral y de gobierno, más amplia de lo que fueron las experiencias anteriores, porque está visto que piensan parecido y por lo menos se pueden poner de acuerdo en algunos puntos vertebradores de lo que puede ser una gestión de gobierno. Y cuando están separados no alcanza electoralmente para poder gobernar. Entonces, hay que trabajar juntos para generar un frente más grande del que existía en el Frente Progresista y Juntos por el Cambio. Pero primero hay que dejar de lado personalismos y supuestos principios ideológicos, porque en definitiva cuando uno analiza gestiones, tanto de Juntos por el cambio como del Frente Progresista, vemos que los ejes pasan por los mismos lugares. Desde el punto de vista teórico de las ideologías hay diferencias, pero cuando aplican en la práctica y se ejerce la gestión, no son tantas. Reitero: hay que dejar de lado personalismos y tratar de trabajar todos juntos para ofrecer a los vecinos una alternativa electoral potente, que represente valores democráticos y republicanos, que son cosas que las gestiones de gobierno nacional y provincial dejan muy de lado.

—**¿Le pone nombre propio a esos personalismos que dice hay que dejar de lado?**

—No, porque veo en propios –dentro de la UCR– y en extraños –de otros partidos– situaciones similares. Yo valoro a todos, pero también veo en muchos de esos referentes el personalismo. Y en este momento se necesita que se depongan las inquietudes personales para avanzar en un armado más colectivo.

—**En este contexto ¿cuáles son sus aspiraciones?**

—Tengo aspiraciones de grupo con relación a que queremos sostener el espacio que representamos en la ciudad, pero no anteponemos esa circunstancia a la posibilidad de conformar un gran frente electoral y ofrecer una alternativa electoral a los vecinos y vecinas de Santa Fe. Esa es nuestra prioridad por estas horas. No tengo una aspiración personal, si el grupo decide que yo tengo que ocupar este lugar, no tengo problema, pero no es el ordenador. Queremos colaborar en el armado para un espacio más amplio que hoy la ciudadanía espera que tengan los espacios políticos y que las actuales gestiones, particularmente la nacional, degradan constantemente.

—**¿Y a nivel local, cómo evalúa la gestión del intendente?**

—Creo que los vecinos tenían otra expectativa con el intendente. Hay decepción por eso, pero yo tengo la aspiración de que el año que viene, vacuna mediante, todo sea mejor.

—**¿Cuáles son los ejes y valores por los que el espacio político que integra quiere retener su lugar en el Concejo y crecer en la ciudad?**

—El medioambiente tiene que ser ordenador de todos los temas que aborda el municipio. La educación; fuimos muy duros en la discusión del presupuesto por la falta de asignación de recursos a algunas políticas que en Santa Fe eran políticas de estado: los jardines municipales y seguridad con el equipamiento que ya tiene, desde el centro de monitoreo hasta las cámaras diseminadas en toda la ciudad. Este presupuesto asigna muy pocos recursos para profundizar eso. De hecho, los presupuestos de educación y de producción crecen muy por debajo de lo que el presupuesto mismo prevé como inflación. Eso nos preocupa porque el municipio se modernizó cuando asumió algunas funciones que no le eran propias, como educación y seguridad, y encontró cómo hacerlo porque era una demanda ciudadana.

—**El transporte será, seguramente, un tema central del año próximo.**

—Vamos a tener que dar una discusión muy de fondo con el transporte y discutir cómo funciona el sistema, ya no solo la variable que siempre le preocupa al vecino, y está bien porque tiene que ver con su bolsillo, que es el valor del boleto. Y aquí hago una salvedad: es una discusión a la que le falta una pata porque como el sistema de transporte en el país está subsidiado, tendríamos que empezar a discutir los subsidios, porque si estuvieran asignados equitativamente y no con la discrecionalidad que hoy se le da a provincia de Buenos Aires, indudablemente sería otra la discusión porque varía mucho con este punto que es central en el funcionamiento del sistema. Estamos discutiendo sobre el resto: mayor o menor cantidad de líneas, recorridos, frecuencias y ver cómo le podemos dar a la ciudad un mejor sistema. Hay que ver también cómo generamos un transporte más multimodal, que tenga en cuenta siempre lo ambiental, en el cual el colectivo puede ser el eje troncal, pero que se incorporen también los otros subsistemas y nuevos medios de movilidad más sustentables. Para eso es importante rediscutir todo, con espacios para bicicletas, monopatines eléctricos, en especial porque vamos hacia un sistema de colectivos en el que va a haber reducción.

<br/>

**<span style="font-family: helvetica; font-size: 1.2em; font-weight: 600;"> Su balance</span>**

La agenda que teníamos prevista, que era profundizar algunos ejes de trabajo como la movilidad sustentable, la materia ambiental y el urbanismo y planeamiento, quedaron de lado producto de la pandemia. En marzo-abril, cuando ya veíamos que el sector más afectado iba a ser el productivo y comercial, priorizamos ver cómo podíamos generar herramientas de acompañamiento. Este trabajo tuvo un final feliz cuando logramos, en conjunto con el Ejecutivo, sancionar una serie de normas que llevaron algo de alivio a ese sector.

También trabajamos con algunos temas puntuales que no estaban vinculados con la pandemia como usurpaciones, por ejemplo la de atrás del CIC de Facundo Zuviría, hasta que se logró desalojar, con un buen trabajo articulado de Nación, Provincia y Municipio, que ha dado solución a todas las partes. Por un lado, ha demostrado que no puede ser a través de un hecho ilegal la forma en que se resuelve una necesidad concreta y real, como la falta de viviendas y de acceso al suelo. Y, por otro lado, se le dio solución a quienes tienen esa necesidad, que en una primera etapa será momentánea y transitoria, hasta que la política pueda permitir el acceso al suelo. Estuvimos cerca de la toma en Los Alisos, en barrio El Pozo; es una situación que nos preocupa mucho junto con La Carbonilla. Son las dos materias pendientes que hay que abordar lo antes posible porque se van de las manos y se extienden.