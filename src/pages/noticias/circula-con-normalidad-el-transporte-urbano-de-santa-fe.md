---
category: La Ciudad
date: 2021-07-13T08:42:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Circula con normalidad el transporte urbano de Santa Fe
title: Circula con normalidad el transporte urbano de Santa Fe
entradilla: 'Del interurbanos, sólo prestan servicios las empresas Continental y Recreo.
  El resto de las compañías interurbanas siguen negociando con sus trabajadores. '

---
Tras cinco días sin servicio público de pasajeros la Unión Tranviarios Automotor levantó el paro de transporte urbano y circula con normalidad desde las 0hs de este miércoles. Mientras que las líneas del interurbano solo las empresas Continental y Recreo retomaron los recorridos. El resto de las compañías siguen negociando. 

Según la información a la que pudo acceder LT10, se acordó el pago a los trabajadores que se divide de la siguiente manera. Un 25% del sueldo a escala vieja se depositará este martes. Luego habrá un pago de otro 25% de sueldo el jueves. Se depositará el aguinaldo a escala vieja y luego se abonará la diferencia de sueldo y aguinaldo a escala nueva.

Por otro lado, Municipalidad ofreció realizar un aporte, correspondiente al Boleto Educativo Gratuito, tal como lo dispone la ordenanza que en junio declaró la emergencia del sistema hasta fin de noviembre. Son 5 millones de pesos, que ayudan a destrabar el conflicto pero de ninguna manera son la solución.

"Venimos trabajando durante todos estos días para que los trabajadores puedan tener su sueldo depositado más allá del dinero adeudado de Nación para el pago de los haberes", explicó Lucas Crivelli subsecretario de movilidad y Transporte de la ciudad.

Crivelli destacó que "se decidió reconducir ingresos y destinar cinco millones de pesos durante seis meses que irán para recomponer los salarios de los trabajadores en esta crisis que se està viviendo". 

Igual destacó que "si bien esto es una solución momentánea es importante el ingreso de dinero desde Nación para el correcto funcionamiento del servicio en la ciudad. 

**El paro**

El motivo del paro, según un comunicado de UTA, es "la falta de pago de haberes a los trabajadores".

Marcelo Gariboldi, representante del gremio, explicó por LT10 que tomaron la decisión de iniciar la medida de fuerza por "la deuda que tienen (las empresas) con los trabajadores, una diferencia importante del salario y el aguinaldo también".