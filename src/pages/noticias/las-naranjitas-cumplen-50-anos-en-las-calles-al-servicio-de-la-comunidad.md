---
category: La Ciudad
date: 2020-12-04T10:55:56Z
thumbnail: https://assets.3dnoticias.com.ar/naranjita.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=r3SnDkDgR0U&feature=youtu.be
author: 3DNoticias
resumen: Las “Naranjitas” cumplen 50 años en las calles al servicio de la comunidad
title: Las “Naranjitas” cumplen 50 años en las calles al servicio de la comunidad
entradilla: 'La Brigada Femenina de Tránsito fue creada en Santa Fe Capital el 3 de
  diciembre de 1970 y fue pionera en el país. '

---
A medio siglo de su conformación, compartimos historias y experiencias de mujeres que desempeñan o desempeñaron su tarea con orgullo.

En las esquinas del microcentro, en las horas pico en las que el tránsito se torna más conflictivo, ellas están para facilitar la circulación de peatones y conductores, con su inconfundible uniforme naranja. También, suelen visitar escuelas para promover la educación y la seguridad vial desde las infancias.

Este jueves 3 de diciembre, la Brigada Femenina de Tránsito, integrada por las emblemáticas “naranjitas”, cumple 50 años en Santa Fe capital. Creada en 1970, fue la primera brigada de su tipo en el país y la segunda a nivel mundial, después de España, con el fin de controlar el tránsito en la ciudad. En un principio, fueron convocadas para trabajar como Guías de Turismo. Hoy se desempeñan en la pista de examen del Parque Garay, en el depósito de vehículos retenidos, desde 1980 ofrecen Educación Vial en escuelas e instituciones y, por supuesto, siguen dirigiendo el tránsito.

Para celebrar medio siglo de historia, este jueves se dejará inaugurada en el hall del Palacio Municipal una muestra que recupera testimonios y recuerdos de su trabajo a través de fotografías, archivo periodístico, y elementos como insignias y vestimenta tradicional. La muestra podrá ser visitada a lo largo de una semana.

## **La pionera**

Liliana Herrero integra la primera camada de mujeres que se incorporó a la Brigada Femenina de Tránsito en 1970. Después de 39 años de servicio, hace ya más de una década que es jubilada municipal.

Como requisito para concursar al puesto, recuerda que solicitaban en el examen teórico “tener conocimiento de las calles de la planimetría, de monumentos, la historia de la ciudad, de las plazas. La función era ser guías de turismo”. El examen práctico se hizo con la presencia del intendente y todo el cuerpo municipal: "te tomaban exámen de tu presencia, de tu forma de desenvolverte, decían ciertas preguntas y vos tenías que contestarlas".

Cuando empezó a trabajar rememora que “decidieron ponernos el color naranja, que era el color internacional del tránsito, y salíamos a la calle en parejas, nos mandaban a distintas zonas de la ciudad para que la gente se nos acerque, nos pregunte direcciones, lugares para ir a conocer. Les dábamos las indicaciones para que pudieran llegar y los de Turismo los asesoraban sobre su estadía en la ciudad”.

Liliana revela que el trabajo de las primeras naranjitas dio origen al conocido lema Santa Fe, la ciudad cordial: "para cumplir la función en la brigada, tenías que tener vocación de servicio. Se debía una atención para con la gente. La gente se acercaba a nosotras, nos preguntaban cosas y nosotras teníamos que contestar".

En la década del ‘70, era la policía la que tenía a su cargo la dirección del tránsito en el macrocentro santafesino, a través de una brigada especializada que se disolvió por los sucesivos retiros y jubilaciones. Para atender la situación, la Municipalidad creó la Dirección de Tránsito “que ya tenía en ese entonces un cuerpo de inspectores, los famosos zorros que le decían”, repasa Liliana, quien da cuenta del desconcierto que generó pasar de ser guías turísticas a directoras de tránsito, tanto para muchas de las mismas trabajadoras como para los ciudadanos.

"No todo el mundo estuvo de acuerdo, incluso hubo compañeras que lloraban porque no querían ir a dirigir el tránsito. Fue una experiencia que acatamos. Incluso, a veces se producían ciertos inconvenientes porque vos estabas parada por primera vez con un uniforme y un silbato, a la gente le llamaba la atención y tenías que hacer que sigan y circulen, porque se quedaban todos parados, iban en autos y te sacaban fotos. Así empezamos y seguimos hasta el día de hoy", cuenta.

## **El uniforme, todo un símbolo**

Del original naranja al rojo. Del rojo otra vez al histórico naranja. El uniforme de la Brigada Femenina de Tránsito es un emblema que, con el correr de los años, experimentó modificaciones, atendiendo a la tendencia de la moda, y los deseos y posibilidades de las gestiones municipales de turno.

Entre 1992 y 2007, la Brigada vistió su uniforme diario de color rojo. Liliana Herrero explica que “el cambio de color se dio porque en ese momento había inconvenientes con las licitaciones por el color.

Las empresas que se dedicaban a hacer textiles no tenían las telas. A raíz de eso, se decidió cambiar el color por algo más común o normal, para dentro de lo que es la moda, que fue el color rojo. La gente lo aceptó tranquilamente.

De ver un color naranja a ver un color rojo, había diferencia, pero una imponía su presencia en la esquina”. Sobre el cambio, recuerda que “nosotras no estuvimos convencidas en su momento porque era un color que tenía una representación, pero las disposiciones las tomaba el Departamento Ejecutivo, así que tuvimos que aceptarlo”.

A pedido de las trabajadoras, en 2008 la gestión municipal de aquel entonces restituyó el histórico color naranja al uniforme, después de 15 años.

“Yo me pongo el uniforme y es como que me transformo. Siempre quiero estar lo mejor posible”, comparte María Moyano que, por su trayectoria, conoció todas las variantes posibles.

“El uniforme rojo no me gustó porque me inicié en el naranja y porque me gustó el color también. Creo que nos sacaban un poco de identidad poniéndonos el rojo, además como soy unionista nos ponían rojo y negro, y quedaba recontra mal”, cuenta entre risas y remata: “Así que cuando volvió el naranja estaba más feliz”.

Ivon Turn ingresó a la Municipalidad en octubre de 1992 para dirigir el tránsito y en la actualidad se desempeña como Supervisora de la sección de Educación Vial.

Sobre sus experiencias con el uniforme recuerda los tiempos en que se incorporó (y luego quedó en desuso) la camisa con moño: “El moñito siempre fue una discordia porque una siempre se lo olvidaba. La supervisora de turno tenía que comprar los rollos de cinta roja para poder darnos porque no podíamos salir, teníamos que salir todas iguales”.

Con ese precepto que cumplir, más de una vez llegaron a salir calzando botas en pleno verano santafesino.

Ivon también trae al presente los momentos en que vistió el uniforme de gala, en celebraciones como el aniversario de la Fundación de Santa Fe, en las que los mosquitos y el calor era también protagonistas: “Tuve el honor y el placer de usar el uniforme de gala color naranja, que se usaba con chaleco y con unas charreteras diferentes a lo que es el rojo. La tela era otra, la trama era más linda, otra caída tenía”.

## **Respeto y orgullo**

Las naranjitas forjaron, con el paso del tiempo, un vínculo especial con la comunidad.

"La gente nos respetó mucho siempre y hasta ahora, creería yo. Ahora no trabajo más en la calle pero creo que al respeto lo ganamos mucho, en mucho tiempo de trabajo", considera Liliana que ya está retirada.

María Moyano concursó para naranjita en 1980: eran 100 aspirantes para disputar 10 cargos. Ella entró en segundo lugar y comenzó a trabajar el 5 de enero de 1981, con la categoría de Inspectora de Tránsito. “Ahí comenzó todo y realmente lo llevé y lo llevo en el alma. El 5 de enero del año que viene voy a cumplir 40 años de servicio. He tenido otras oportunidades de ir a otras direcciones, pero se ve que el uniforme era para mí. Mi vocación era y es la calle. Me siento muy cómoda y libre, es algo que a uno le gusta, sino ya me hubiese ido a otro lado”.

María también da su mirada al plantear que la relación entre las naranjitas y los demás agentes de tránsito es totalmente distinta: "Creo que somos o estamos más allegadas a la comunidad porque la actividad de cada uno es distinta. El que está en Seom va directamente a sancionar, y nosotras estamos en la parte visible de las buenas. Entonces, es distinto el trabajo".

“El respeto hacia nosotras no cambió mucho. Lo que pasa que nosotras, al estar en una esquina, imponemos una autoridad. Entonces nos hacen caso, o nos ven y dicen: ‘Cuidado que está la naranjita’, o la inspectora, o la zorra, porque también nos dicen zorras”, cuenta Ivon, quien ejerce con orgullo su trabajo: “Primero, soy y sigo siendo inspectora y eso no lo voy a cambiar”.

El vínculo entre las naranjitas con las niñas y niños es siempre un capítulo en esta historia de medio siglo que no se termina de escribir. “Los chicos aman tocarnos, nos saludan”, cuenta María y sigue: “Había una señora que llevaba siempre una nenita de 2 o 3 años y siempre nos saludaba. Un día pasa la mujer y dice: 'Ella quiere tocarla'. Le agarro la manito y tenía tal emoción que a nosotras nos conmovió, porque cómo puede ser que ellos nos vean a nosotras igual que cuando se va a dar una charla a algún colegio, somos lo más".