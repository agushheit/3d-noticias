---
layout: Noticia con imagen
author: Nota de la Redacción de 3DNoticias
resumen: "Alacranes: prevenir y actuar"
category: La Ciudad
title: Recomendaciones para prevenir y actuar ante la picadura de alacranes
entradilla: Las lluvias y el calor son dos factores que inciden en la aparición
  de alacranes durante la temporada estival. Es por eso que se brindan consejos
  para conocer cómo prevenir su presencia y proceder ante picaduras.
date: 2020-11-22T13:45:24.764Z
thumbnail: https://assets.3dnoticias.com.ar/alacran.jpg
---
Ante la llegada del calor y la temporada de lluvias, la Municipalidad de Santa Fe brinda información sobre características y mecanismos de prevención y actuación ante la picadura de alacranes, que suele darse en forma accidental o como mecanismo de defensa al ser encontrados en patios, o en el interior de viviendas, por sentirse amenazados por la presencia humana.

Algunas especies de alacranes son considerados de importancia sanitaria debido a la peligrosidad que presentan sus picaduras, ya que resultan muy tóxicas para niños, ancianos y personas adultas alérgicas o con alguna complicación de salud de base o inmunodeprimidos.

Los alacranes son animales mayormente solitarios y de hábitos exclusivamente nocturnos, salen de noche a cazar sus presas para alimentarse o para aparearse. **Durante el día permanecen ocultos en tuberías, cañerías, sistema cloacal, rejillas y desagües, contrapisos, detrás de azulejos, en grietas de paredes, leña, materiales acumulados como maderas, ladrillos, chatarra y en huecos de la corteza de los árboles.** Ocasionalmente pueden encontrarse entre la ropa y los zapatos, o debajo de los trapos de piso.

Los alacranes tienen como característica que son predadores, es decir que se alimentan especialmente de otros insectos (como cucarachas, grillos, etc) o arañas, acechando a sus presas en las cercanías de sus refugios. Durante la caza utilizan las pinzas para la aprehensión y el aguijón con su veneno para inmovilizar a la presa.

## **Prevenir su presencia**

Para disminuir la presencia de alacranes en el ámbito domiciliario y peri-domiciliario se sugiere seguir una serie de recomendaciones, entre las que se destacan **evitar la acumulación de escombros, ladrillos, tejas, leña y maderas cerca de la vivienda**, a fin de reducir la disponibilidad de lugares de refugio. En caso de hallar alacranes en esos espacios, se debe evitar juntarlos con las manos. Además, se sugiere realizar limpiezas periódicas y no almacenar basura para reducir la cantidad de insectos que sirven de alimento a los escorpiones.

Durante el día, en caso de removerse en el jardín los lugares donde puedan refugiarse los alacranes, como macetas, madera o leña, es aconsejable utilizar guantes para evitar la picadura directa sobre la piel. También es importante evitar que los niños jueguen en los lugares donde se almacenan este tipo de materiales.

Además, se sugiere **revisar y sacudir las prendas de vestir y el calzado antes de su uso**, especialmente si han quedado en el suelo, como así también verificar la ropa de cama antes de acostarse o acostar un bebé o niño. En muebles de dormitorio se recomienda colocar cinta de embalar, como es el caso de las patas de la cama o cuna, ya que proporciona una superficie bien lisa, que el alacrán no puede trepar.

Otro mecanismo a tener en cuenta para prevenir la aparición de alacranes es **tapar las grietas de los revoques en las paredes especialmente si éstas son de ladrillos huecos**. También, para impedir el ingreso de estos arácnidos a la vivienda, se recomienda la colocación de burletes y tela metálica en puertas y ventanas.

Finalmente, se sugiere **sellar la tapa de la cámara de las cloacas o colocar malla metálica en las rejillas de desagüe de la casa**, como también **no caminar descalzo**, especialmente de noche, en lugares donde sea posible la existencia de alacranes.



## **Qué hacer ante picaduras**

Ante una picadura, es importante **concurrir lo antes posible al centro de asistencia médica más cercano y, en lo posible, llevar el alacrán para ser identificado**.

Ante esta situación se recomienda no realizar tratamientos caseros: el único tratamiento específico es el uso de antiveneno, que debe utilizarse con el soporte médico adecuado para tratar las complicaciones que se producen en el envenenamiento.

En caso de haberse recolectado algún ejemplar, se recomienda reservarlo en lugar oscuro (no exponerlo al sol, ya que las radiaciones solares lo matan) y acercarlo al ex Hospital Italiano, ubicado en Dr. Zavalla 3399 (donde funciona la Región de Salud Santa Fe).