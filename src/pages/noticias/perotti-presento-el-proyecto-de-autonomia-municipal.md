---
category: Estado Real
date: 2021-01-28T10:28:47Z
thumbnail: https://assets.3dnoticias.com.ar/autonomia.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Perotti presentó el proyecto de autonomía municipal
title: Perotti presentó el proyecto de autonomía municipal
entradilla: "El primer encuentro para abordar la temática se realizó bajo la modalidad
  mixta (presencial y virtual). Conformarán una comisión \n para consensuar una ley.
  Jatón y Javkin manifestaron su acuerdo con la iniciativa."

---
El gobernador Omar Perotti encabezó este miércoles un encuentro junto a intendentes y presidentes comunales de la provincia, a los que les presentó su proyecto de autonomías municipales.

El primer mandatario santafesino estuvo acompañado por los ministros de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman; y de Gestión Pública, Marcos Corach.

De manera presencial participaron el intendente de la ciudad de Santa Fe, Emilio Jatón; de Rosario, Pablo Javkin; de Rafaela, Luis Castellano; de Reconquista, Enrique Vallejos; de Esperanza, Ana Meiners; de Vera, Paula Mitre; y de Cañada de Gómez, Stella Clerici. El resto de los mandatarios locales lo hicieron de manera virtual, en el marco de las disposiciones sanitarias por la pandemia de Covid.

Tras la convocatoria se conformará una comisión política y técnica a los fines de consensuar los temas y la reforma de la legislación actual.

Luego de la reunión, funcionarios provinciales y los intendentes de Santa Fe y Rosario ofrecieron una conferencia de prensa. 

En ese contexto, el ministro Sukerman dijo que la autonomía municipal no es solo "una cuestión formal", sino que para el gobierno es una herramienta "para mejorar la calidad de vida de los ciudadanos y ciudadanas". 

Además, recordó que es un mecanismo que solamente tres provincias del país no lo tienen incorporado en sus constituciones: Santa Fe, Mendoza y Buenos Aires. 

Finalmente, tanto Jatón como Javkin manifestaron su acuerdo con la iniciativa. "Nos parece muy importante y muy bien que se empiece esta discusión porque va a ser larga", celebró el titular del Ejecutivo santafesino.