---
layout: Noticia con imagen
author: Por Clara Blanco
resumen: "Unión ganó en Sarandí "
category: Deportes
title: Unión ganó en Sarandí y es escolta de la Zona 1
entradilla: El Tatengue venció a Arsenal por 3 a 2 con un doblete de Juan Manuel
  García y el gol en contra de Jhonatan Candia. Para el local descontó el mismo
  Candia y Mateo Carabajal.
date: 2020-11-21T00:15:15.843Z
thumbnail: https://assets.3dnoticias.com.ar/union-arsenal.jpg
---
Unión viajó a Sarandí por la cuarta fecha de la Copa Liga Profesional y se trajo una victoria a Santa Fe. El Tatengue superó a Arsenal por 3 a 2 con dos goles de Juan Manuel García y un tanto en contra de Jhonatan Candia. Mientras que para los locales marcaron Candia y Mateo Carabajal.

Hasta el primer tiempo fue un juego de igual a igual, donde hubo ataque en ambos arcos. Pero el que primero llegó al gol fue Arsenal a los 36 minutos por el uruguayo Jhonatan Candia. Pero el Tatengue no tardó en responderle y a los 42, cuando se moría la primera mitad, Juan Manuel García aprovechó un grosero error del arquero del Viaducto para empujar esa pelota al fondo de la red. De esta manera, Unión iguala 1 a 1 con Arsenal tras el primer tiempo.

En el complemento, el Tate entró más activado y aprovechó los errores de un Arsenal que se comenzó a mostrar distraído. Apenas habían pasado 10 minutos de la segunda mitad cuando por un tiro de esquina del Rojiblanco, el uruguayo Candia cabeceó contra su propio arco. Ese tanto fue el detonante para que Unión siga creciendo ante Arsenal.

Nuevamente, a los 26 del segundo tiempo, volvió a marcar Juan Manuel García y ponía el 3 a 1 para su equipo. Unión manejaba el partido, pero minutos después el club santafesino se quedó con 10 jugadores por la expulsión por doble amarilla a Leonel Bucca. Por esta pérdida y el ingreso de Alan Ruiz, ex Colón, el local comenzó a hilvanar algunos pases y llegó el descuento. A 10 minutos del cierre del partido Mateo Carabajal anotó el segundo tanto de Arsenal, pero a los de Rondina no les alcanzó para igualar el marcador.

Con esta victoria Unión llega a 7 puntos y se ubica segundo en la tabla de posiciones de la Zona 1, por debajo de Atlético Tucumán con puntaje ideal, y grandes chances de avanzar a la Fase Campeonato.