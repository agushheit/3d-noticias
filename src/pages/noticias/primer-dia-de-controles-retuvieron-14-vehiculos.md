---
category: La Ciudad
date: 2021-04-11T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/controles.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: " Primer día de controles: retuvieron 14 vehículos"
title: 'Primer día de controles: retuvieron 14 vehículos'
entradilla: En distintos operativos se desarticularon tres fiestas clandestinas, se
  labró un acta a un bar y se desactivó un evento en un local por no estar autorizado.

---
Desde este viernes 9 de abril  y hasta el próximo viernes 30 de abril, se encuentran vigentes tanto en la ciudad de Santa Fe como en el resto del territorio provincial, nuevas restricciones para contribuir en la baja de contagios y prevención sanitaria en cuanto la llegada de la segunda ola de coronavirus al país. Por lo tanto, se realizaron diversos operativos de control.

Desde el municipio anunciaron la realización de procedimientos en bares, transporte público, además de establecer en distintos puntos de la ciudad en conjunto con la policía de la provincia y las fuerzas federales, controles de tránsito.

Según lo informado por fuentes de la Municipalidad, durante las últimas horas del viernes y las primeras de sábado se desarticuló un evento en la zona del Puerto con la participación de 25 personas que no contaban con autorización. Además, y a través de distintos llamados realizados al 0800-777-5000 se desactivaron tres fiestas clandestinas en viviendas particulares: una en Colastiné con 30 personas; otra por calle San Martín en inmediaciones de La Peatonal con 15 y otra en calle Derqui 3400 con 26 personas.

Por otra parte, también informaron que se retuvieron 14 vehículos: siete automóviles y siete motocicletas, dentro de los cuales hubo cuatro conductores que dieron positivo en el control de alcoholemia.

También se realizó una cédula de notificación en un bar por faltas en el protocolo y hubo actuaciones por ruidos molesto, uno de esos operativos terminó con la confirmación de un acta.

La policía y el Ministerio de Seguridad serán los encargados de controlar a la gente que circule por la ciudad entre las 0 y las 6 y serán quienes soliciten el certificado oficial que habilita para circular y aplicará sanciones si esto no se cumple.

La sanción será desde una notificación hasta la retención del vehículo. En las fiestas clandestinas se podrá demorar a los organizadores, identificar a todos los participantes y retirar los instrumentos musicales si hay bandas.

En tanto, desde la cartera nacional afirmaron que las provincias que lo requieran serán reforzadas con efectivos federales para hacer cumplir las medidas dispuestas.

En los controles se solicitarán los permisos correspondientes. En caso de que los automovilistas no cuenten con el mismo, se notificará al Ministerio Público de la Acusación (MPA), que determinará si corresponde retener el vehículo o solo realizar una notificación a quien circule entre las 0 y las 6 sin el permiso oficial.

Quienes cuenten con permiso de circulación pueden visualizarlo en la app Cuidar, Mi Argentina, descargarlo o imprimirlo, y eso será suficiente para garantizar la legalidad del traslado. Si algún usuario desinstaló la aplicación Cuidar, no hace falta que vuelva a tramitar el permiso. En caso de volver a descargar Cuidar, se debe indicar el DNI y tras hacerse el autodiagnóstico, lo volverá a visualizar.