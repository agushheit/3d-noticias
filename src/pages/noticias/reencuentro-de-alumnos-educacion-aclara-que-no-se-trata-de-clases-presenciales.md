---
category: Agenda Ciudadana
date: 2020-11-30T10:56:41Z
thumbnail: https://assets.3dnoticias.com.ar/ALUMNOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: LT10'
resumen: 'Reencuentro de alumnos: Educación aclara que no se trata de clases presenciales'
title: 'Reencuentro de alumnos: Educación aclara que no se trata de clases presenciales'
entradilla: Ante las críticas de los gremios docentes, el gobierno asegura que se
  trata de posibilitar encuentros entre estudiantes y docentes, y no del retorno a
  las aulas.

---
El gobierno provincial había anticipado la semana pasada que podrían retornar los niños y jóvenes a las escuelas por el lapso de dos semanas.

Este anuncio provocó el rechazo de los gremios docentes que en su totalidad manifestaron su oposición a la medida.

El viernes, por LT10 María José Marano, delegada seccional adjunta de Amsafé La Capital, sostuvo que las condiciones sanitarias, edilicias de las instituciones, y la de los docentes no están dadas para retornar.

Sin embargo, posteriormente el gobierno provincial aclaró, a través de la ministra de Educación Adriana Cantero que "no se trata de clases presenciales sino de breves encuentros en pequeños grupos para retirar los cuadernos que orientarán la tarea que sigue y al mismo tiempo, dar oportunidad de encuentro de los estudiantes con sus compañeros y docentes".

En ese sentido, la funcionaria manifestó: “Hemos transitado todo un año difícil y muchas veces hemos expresado que comprendemos que los más jóvenes tienen menos recursos simbólicos para sostenerse en una relación articulada en la distancia. Interpretamos el deseo de los estudiantes expresado en los múltiples encuentros virtuales que con ellos sostuvimos".

Además, agregó que en un proceso de continuidad "pusimos siempre primero el cuidado de la salud y la necesaria preparación para la oportunidad de recuperar el vínculo presencial" remarcó Cantero.