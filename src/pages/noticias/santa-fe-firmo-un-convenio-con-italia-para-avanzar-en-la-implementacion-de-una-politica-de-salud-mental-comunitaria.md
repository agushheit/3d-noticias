---
category: Estado Real
date: 2021-06-15T21:01:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/salud-mental.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Santa Fe firmó un convenio con Italia para avanzar en la implementación de
  una política de salud mental comunitaria
title: Santa Fe firmó un convenio con Italia para avanzar en la implementación de
  una política de salud mental comunitaria
entradilla: "“Es un hito para Santa Fe”, destacó la directora de Salud Mental, Celina
  Pochettino."

---
El Ministerio de Salud de Santa Fe, a través de la Dirección Provincial de Salud Mental, firmó un convenio de cooperación técnica con la COPERSAMM (Conferencia permanente por la salud mental en el Mundo- Conferencia Basaglia), denominado “Hacia la transformación del modelo de atención en salud mental”.

En la reunión participaron el secretario de Salud, Jorge Prieto, y la directora Salud Mental de la provincia, Celina Pochettino. “Es un hito para la provincia de Santa Fe porque este es un convenio con Italia, con la Conferencia Permanente por la Salud Mental en el Mundo, que representa la experiencia pionera de la transformación de las políticas de salud mental a escala internacional”, sostuvo Pochettino.

“Este convenio tiene como objetivo la cooperación técnica con la provincia para la implementación de las transformaciones en el modelo asistencial en salud mental e integral basado en el despliegue de una red de servicios comunitarios”, detalló y remarcó: “Es un honor para nosotros contar con este equipo pionero en las políticas de salud mental en el paradigma asistencial hacia la salud mental comunitaria, en el asesoramiento para la implementación del plan estratégico”.

**Ejes de abordaje incluidos en el convenio marco**

La directora de Salud Mental expresó que los ejes consisten en: “Acompañarnos en la transformación de los hospitales monovalentes, manicomiales, ya se han iniciado las comisiones de sustitución para desarrollar los proyectos de transformación de acuerdo a la ley 26657”.

“Otro de los ejes consiste en la formación y capacitación para los y las trabajadoras del campo de salud en la provincia”, detalló la funcionaria provincial.

Por último, “desplegar proyectos que hacen al acceso a derechos, a habitar, a sociabilizar, a trabajar”.

**Plan estratégico, participativo y territorial**

Para contextualizar el trabajo que se viene realizando, Pochettino remarcó que “tenemos una planificación estratégica desde el Ministerio a través de la dirección de Salud Mental de la construcción participativa y regional de la red de servicios a la comunidad, que es el modelo que sustituye al paradigma actual, un modelo que persiste, a pesar de las experiencias interesantes que ya se desarrollan en la provincia. Por ello necesitamos constituir un plan estratégico. La experiencia basagliana es clave para esto”.

“Contar con Trieste y estos máximos referentes para acompañarnos en esta transformación es clave” argumentó la funcionaria quien agregó que “también se desarrollarán rondas de trabajo porque consideramos que es un plan participativo y de allí que universidades, sindicatos, Colegios y fundamentalmente usuarios y usuarias del sistema de salud podrán construir con nosotros este proyecto por regiones este proceso de transformación en la Salud Mental”.

**Precedente**

Antecede a este acuerdo una Carta de intención firmada por la ministra Sonia Martorano y reuniones de trabajo con el equipo de COPERSAMM, en las que participó Franco Rotelli, referente de la experiencia italiana, Giovanna del Giudice presidenta de la Conferencia, Michela Vorig de COSM (Consorcio Operativo de Salud Mental Cooperativas Sociales), Roberto Colapietro y Diana Mauri, entre otros integrantes de trayectoria de la experiencia italiana de transformación en salud mental de Trieste.

El Ministerio de Salud contará a partir de este acuerdo con la cooperación técnica de la COOPERSAMM para desplegar políticas de salud y salud mental y su implementación efectiva de acuerdo al marco de la ley 26657: hacia un modelo de atención con base comunitaria y territorial. Por esto se encuentra comprometido en la construcción de su Plan provincial, proponiendo los Planes de sustitución de los hospitales Monovalentes como disparadores del proceso, en el marco del Plan Nacional de Salud Mental.