---
category: La Ciudad
date: 2021-08-10T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/formlab.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se encuentran abiertas las preinscripciones para cursos de formación laboral
title: Se encuentran abiertas las preinscripciones para cursos de formación laboral
entradilla: La Municipalidad abrió la convocatoria para los cursos que se dictarán
  en el segundo semestre de este año. Serán gratuitos y con modalidad presencial.

---
Los cursos de empleo forman parte de la política en capacitación laboral que impulsa la Municipalidad de Santa Fe con el objetivo de adquirir y actualizar los conocimientos y habilidades de las personas para mejorar sus perfiles laborales y generar más oportunidades en la búsqueda de empleo. Hay tiempo hasta el 15 de agosto para preinscribirse al correo: capacitaciones.empleo@santafeciudad.gov.ar La propuesta buscará capacitar a 1.500 santafesinos y santafesinas para desempeñarse en trabajos en relación de dependencia y para el desarrollo por cuenta propia.

Al respecto, la secretaria de Integración y Economía Social, Ayelén Dutruel, explicó que “es fundamental generar opciones de trayectorias educativas y formativas que permitan contravenir la desigualdad actual, teniendo como horizonte la integración social de los y las santafesinas”. Por dicho motivo, explicó que, en un esfuerzo conjunto de las distintas áreas municipales, se lanza esta propuesta, que tiene como eje central abrir procesos formativos y educativos con fuerte orientación en oficios, que permitan aprender con otros, intercambiar saberes, y validarlos.

Las clases comenzarán en septiembre y se extenderán hasta fin de año, con nuevos inicios cada mes y más de una docena de cursos disponibles. Entre los mismos, se destacan los siguientes: auxiliar de electricista domiciliario; auxiliar de plomero e instalador sanitarista; auxiliar de gasista en mantenimiento de cocinas, calefones y estufas; auxiliar de instalador de aire acondicionado; auxiliar de reparador de aire acondicionado; instalador y mantenimiento de filtros de piscinas; auxiliar de panadería; maquillador/a social; reparador de PC de escritorio; cuidador/a de adultos mayores; cuidador/a de niños y niñas; asistente personal de personas con discapacidad, y trabajador/a de casas particulares.

Estas propuestas se dictarán en la sede de la Oficina de Empleo y en distintas Estaciones Municipales de la ciudad de Santa Fe. Al respecto, el director de Empleo y Trabajo Decente, Guillermo Cherner, sostuvo que “desde el municipio avanzamos en la elaboración de una agenda que priorice la capacitación profesional, con posibilidad de formarse presencialmente en los distintos barrios de la ciudad y de poder fortalecer las herramientas de inserción laboral de los santafesinos y las santafesinas que están en la búsqueda de un empleo, priorizando el fomento del trabajo decente y de calidad, y asegurando la igualdad de oportunidades más allá del lugar de residencia de las personas interesadas".

Asimismo, es importante remarcar que siguen disponibles los distintos servicios de orientación laboral que se brindan desde la Oficina de Empleo, en un trabajo conjunto con la Red Federal de Servicios de Empleo del Ministerio de Trabajo, Empleo y Seguridad Social de la Nación.

**Programa Estaciones**

Las clases se llevarán adelante en distintas Estaciones Municipales y en la sede de la Oficina de Empleo, con el objetivo de acercar la propuesta a los distintos barrios santafesinos.

El programa Estaciones, se propone el trabajo con las juventudes de los barrios de la ciudad desde una perspectiva de derechos, habilitando la escucha y la participación en la búsqueda y construcción de un proyecto de vida. Esto implica un acompañamiento donde los y las jóvenes puedan significar y resignificar su historia y contextos sociales.

En este sentido, Dutruel consideró que “las Estaciones Municipales, se constituyen en el escenario propicio para esta iniciativa por su cercanía territorial, que permite una propuesta más accesible, situada y distribuida de manera equilibrada en la ciudad”. De esta manera, “cada vecino y vecina podrá participar de los trayectos formativos en sus propios barrios”, finalizó.

**La voz de la experiencia**

María Laura Baez fue alumna del curso de Maquillaje Social que se dictó en febrero y marzo de este año. Ella se mostró muy entusiasmada con los aprendizajes obtenidos y la posibilidad de aplicarlos en el corto plazo: “La experiencia fue hermosa, la profesora sabía mucho y nos lo pudo transmitir en cada clase. Para todas las que cursamos fue muy productivo el curso, fue intensivo, y muchas ya podemos dedicarnos al maquillaje”. Además, compartió que le pareció importante haber recibido asesoramiento sobre cómo poder publicitarse a través de las redes sociales para llegar a más clientes.

**Consultas e inscripciones**

Los cupos son limitados y hay tiempo hasta el 15 de agosto para preinscribirse al correo: capacitaciones.empleo@santafeciudad.gov.ar. Es requisito ser mayor de 18 años.