---
category: Agenda Ciudadana
date: 2021-06-13T06:00:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/INCENDIOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Coordinan acciones para prevenir incendios en la zona de islas
title: Coordinan acciones para prevenir incendios en la zona de islas
entradilla: El Ministerio de Medio Ambiente de la provincia, Protección Civil y la
  Guardia Rural Los Pumas profundizaron esquemas de trabajo.

---
Autoridades del gobierno de Santa Fe mantuvieron una nueva reunión de coordinación, bajo el puente Rosario - Victoria, con la participación de autoridades del Ministerio de Ambiente y Cambio Climático, de la subsecretaría de Seguridad Preventiva y de la secretaría de Protección Civil, con el objetivo de profundizar el esquema de trabajo conjunto que se prevé para los próximos meses en relación a los posibles incendios en la zona de islas.

En ese sentido, la Ministra de Ambiente y Cambio Climático, Erika Gonnet, comentó que “en relación al año anterior la provincia de Santa Fe ha tenido un gran enriquecimiento en lo referente a la prevención de incendios. Estamos trabajando de forma interministerial junto al Ministerio de Gobierno a través de Protección Civil, y al Ministerio de Seguridad con la Guardia Rural Los Pumas”.

Gonnet resaltó además que “se han incorporado 15 nuevos brigadistas que recibirán una capacitación del Servicio Nacional de Manejo del Fuego. Todo eso se suma a los aviones hidrantes con los que contamos y a la red de faros de conservación en la que venimos trabajando”. Además, destacó que hace una semana, para seguir articulando con Nación, se reunieron en la isla Sabino Corsi con integrantes del Gobierno Nacional y del Gobierno de Entre Ríos para coordinar acciones de forma efectiva.

Por su parte, el subsecretario de Seguridad Preventiva, Diego Lluma, agregó que Los Pumas realizarán un patrullaje intensivo con lanchas. “En la zona sur vamos a agregar tres de estos medios que van a estar haciendo recorridos de 16 horas por jornada para la identificación temprana de focos, pero también, para la identificación de personas que pudieran ser los responsables de este daño, este perjuicio al ambiente, y también en su disuasión”.

**SEDE RIBERA**

La reunión se realizó en la Sede Ribera, “un espacio en donde vamos a estar trabajando de forma articulada con Protección Civil y el Ministerio de Seguridad junto a otros actores. En este lugar se va a establecer una sala de monitoreo y, de forma permanente, la Los Pumas van a tener una base para divisar lo que pasa a lo largo de la isla frente al Gran Rosario”, explicó Gonnet. Sobre la Sede Ribera, el secretario de Protección Civil, Roberto Rioja, agregó que “va a ser de muchísima importancia, esto nos va a permitir trabajar con mucha más calidad y rapidez”.