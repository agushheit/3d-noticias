---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: Renuncia de Bielsa
category: Agenda Ciudadana
title: 'Renuncia de Bielsa: "Estaba cuestionada por el kirchnerismo y por la
  presidencia"'
entradilla: El politólogo Carlos Fara; la ministra saliente "tenía baja
  operatividad" y por eso era cuestionada.
date: 2020-11-12T13:23:41.964Z
thumbnail: https://assets.3dnoticias.com.ar/bielsa.jpg
---
Se conoció este miércoles por la tarde la renuncia de la ministra de Desarrollo Territorial y Hábitat, la santafesina María Eugenia Bielsa.

Sería reemplazada por Jorge Ferraresi (59), quien desde 2009 es intendente del partido bonaerense de Avellaneda, uno de los distritos de la provincia que está pegado a la Ciudad de Buenos Aires.

Sobre la salida de Bielsa, el politólogo Carlos Fara manifestó por LT10 que es la confirmación de un rumor que venía desde hace tiempo. "No solamente estaba cuestionada desde el kirchnerismo sino desde la propia presidencia de la Nación por su baja operatividad, entonces obviamente es un momento para hacer un movimiento que satisfaga a varias partes", opinó.

En cuanto a Ferraresi, sostuvo que es un funcionario "encolumnado con Cristina (Kirchner)", pero más allá de eso consideró que para el ministerio que deja Bielsa "se necesita alguien con mucha muñeca política y eficiente".

El analista agregó que el actual intendente de Avellaneda es "un buen gestor y muy activo", lo que eran dos cosas que, a su entender, no tenía Bielsa, a quien "le faltaba potencia política, conocimiento del territorio y eficiencia en la gestión".

Se trata del primer alejamiento de un integrante del gabinete que se produce durante el gobierno de Alberto Fernández, a menos de un año de la asunción.

Si bien el propio Presidente, semanas atrás, descartó cambios, no se sabe si habrá nuevas renuncias o reemplazos de funcionarios, lo que es inherente a la política en sí.