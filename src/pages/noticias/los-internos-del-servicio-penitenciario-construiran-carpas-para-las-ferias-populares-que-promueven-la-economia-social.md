---
category: Estado Real
date: 2021-01-26T10:00:25Z
thumbnail: https://assets.3dnoticias.com.ar/presos1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno Santa Fe
resumen: Los internos del Servicio Penitenciario construirán carpas para las ferias
  populares que promueven la economía social
title: Los internos del Servicio Penitenciario construirán carpas para las ferias
  populares que promueven la economía social
entradilla: Se firmó un convenio de cooperación. Las unidades cumplen con los protocolos
  sanitarios requeridos y permitirán jerarquizar estos espacios de trabajo que se
  encuentran en numerosas localidades de la provincia.

---
El Ministerio de Desarrollo Social de la provincia y la Secretaría de Asuntos Penitenciarios, dependiente del ministerio de Gobierno, Justicia, Derechos Humanos y Diversidad, firmaron un convenio de cooperación para la construcción de carpas que serán destinadas a las ferias populares que se desarrollan en diferentes sectores de la provincia.

El acto se realizó en Casa de Gobierno y del mismo participaron los secretarios de Asuntos Penitenciarios, Walter Gálvez, y de Prácticas Socio Comunitarias del Ministerio de Desarrollo Social, Ignacio Martínez Kerz; el subsecretario del Servicio Penitenciario de la Provincia, Héctor Acuña; y el director de Economía Solidaria, Daniel Ríos, entre otros.

Este convenio para la construcción de las carpas por parte del personal del Servicio Penitenciario “tiene que ver con jerarquizar las ferias populares en toda la provincia y darles un mejor funcionamiento”, explicó Martínez Kerz y amplió: “Estas ferias de trabajadores de la economía popular se dan en todo el territorio provincial y estas carpas que se van a construir cumplen con los protocolos sanitarios que fueron aprobados: cuentan con un ingreso y una salida, tienen un espacio muy amplio para trabajar, y están bien ventiladas”.

Esto va a permitir “fortalecer este espacio de trabajo que representa a más del 35 por ciento de los trabajadores de la provincia que están en la informalidad”, dijo el funcionario, quien agregó que inicialmente “las carpas van a estar instaladas de manera itinerante en Rosario, Santa Fe, Venado Tuerto, Reconquista y Rafaela”, para después ampliarse a otras localidades.

Asimismo, Kerz manifestó que la instalación de las carpas “viene acompañada de bajadas de energía, provisión y suministro de electricidad segura, baños químicos y todo lo que hace a que sea un espacio cómodo”, cumpliendo con los protocolos dispuestos para el funcionamiento de estos espacios.

**CAPACITACIÓN Y OFICIOS**

Mientras, Gálvez añadió que “estamos teniendo una sobrepoblación importante en el Servicio Penitenciario, con lo cual toda acción, colaboración y capacitación relacionadas con oficios, con herramientas, con el hacer, estamos tratando de desarrollarlo en colaboración con todas las áreas del gobierno provincial”, al tiempo que mencionó que “intentamos mostrar hacia afuera al Servicio Penitenciario y las actividades que realiza”.

El secretario de Asuntos Penitenciarios agregó que “a través del Instituto Autárquico Provincial de Industrias Penitenciarias (Iapip) y del Servicio Penitenciario, nuestra colaboración es poder ofrecer la mano de obra necesaria para la construcción de esta infraestructura”, lo que también “es una manera de interesar al interno en lo que se hace afuera, y en un área tan importante como la economía social y solidaria”.

Estos módulos “se estarán construyendo en el Iapip, en sus talleres de Coronda y en Las Flores, también en talleres de otras unidades penitenciarias de la provincia, pero esto se centrará en Coronda; además, el Servicio Penitenciario tiene talleres en la Dirección General, donde también se van a realizar trabajos”.

**CAPACIDAD DE COLABORACIÓN**

Por su parte, Acuña expresó que “tenemos dos áreas para implementar la modalidad de trabajo, una son los talleres del Iapip, y la otra son los propios talleres de cada unidad penitenciaria. Esto nos permite conjugar una apertura del Servicio y una colaboración con todo el gobierno y también capacitar, formar en oficios y darle una salida laboral a los internos para cuando recuperen la libertad”, y remarcó que “el Servicio Penitenciario tiene una gran capacidad para colaborar con la economía social”.

Por último, Ríos manifestó que “esto era necesario, porque hay una gran población que vive de la economía popular, con mucha informalidad, y es hora que el Estado comience a colaborar y fortalecer ese sector mientras se reactiva el aparato económico y se generan los puestos de trabajo suficientes”.