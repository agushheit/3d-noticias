---
category: Agenda Ciudadana
date: 2021-05-08T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/fernandez.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Fernández anunció que casi 400.000 personas más serán beneficiarias de la
  Tarjeta Alimentar
title: Fernández anunció que casi 400.000 personas más serán beneficiarias de la Tarjeta
  Alimentar
entradilla: Se incrementarán hasta un 100% los montos del beneficio, ampliando a $12.000
  para los hogares de hasta tres hijos. Se incluirán a los niños de hasta 14 años
  inclusive.

---
Este viernes se llevó a cabo la quinta reunión del Consejo Federal Argentina contra el Hambre, con eje en los niños y en la situación alimentaria, en el Museo del Bicentenario de Casa Rosada. Luego de la reunión, en conferencia de prensa el Presidente Alberto Fernández anunció que se ampliarán los montos de la Tarjeta Alimentar hasta un 100% y se incluyó 400.000 personas como nuevos beneficiarios.

Hasta este viernes, la Tarjeta Alimentar la percibían alrededor de 1,5 millones de personas y el 70% de los beneficiarios viven en siete provincias: Buenos Aires agrupa al 36% de titulares, y le siguen Córdoba, Santa Fe, Tucumán, Mendoza, Chaco y Salta. A partir de este anuncio, la Tarjeta Alimentar llegará 1,9 millones de familias y significará una inversión de $250.000 millones.

Con el anuncio del Presidente Alberto Fernández, la Tarjeta Alimentar la podrán percibir las familias con hijos de hasta 14 años inclusive. Se mantendrá el monto en $6.000 para quienes tienen un hijo, se incrementará a $12.000 para quienes tengan dos o más hijos, y se extenderá el beneficio de $12.000 mensuales para las madres que tengan siete o más hijos que perciban una Pensión No Contributiva.

“Lo hemos logrado con el aporte solidario que tan maltratado fue en los medios, pero que fue pagado por el 80% y les quiero agradecer a cada uno de los que pagó, porque en un momento de emergencia ellos fueron solidarios, y los argentinos debemos tener gratitud con ellos, que acumularon riqueza trabajando, produciendo, heredando, pero cuando les pedimos un esfuerzo, lo hicieron. Cuando les pedimos ayuda, estuvieron”, sostuvo el Presidente.

"Estamos viviendo un momento singular en Argentina. Nada me preocupa más que el hambre de los argentinos, y más me preocupa cuando veo que 6 de cada diez de los chicos y las chicas menores de 14 años están debajo de la línea de pobreza”, continuó Alberto Fernández.

"Nosotros vamos a seguir haciendo el esfuerzo para sacar a la Argentina de la pobreza y el hambre. Cuénteme el resto qué esfuerzo van a hacer para sacar a la Argentina de la pobreza", concluyó el jefe de Estado, destacando se propuso terminar "con la puja distributiva" de unos pocos.