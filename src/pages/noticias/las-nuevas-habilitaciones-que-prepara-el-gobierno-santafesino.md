---
category: Agenda Ciudadana
date: 2021-06-25T08:17:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid-peatonaljpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Las nuevas habilitaciones que prepara el gobierno santafesino
title: Las nuevas habilitaciones que prepara el gobierno santafesino
entradilla: El anterior DNU vence este viernes. El ministro de Trabajo adelantó cuáles
  serán las prioridades para la etapa que comenzará a regir el sábado 26 de junio.

---
En pocos días termina el lapso estipulado por el DNU provincial que firmó Perotti sobre las actividades que estaban permitidas en Santa Fe desde el 11 de junio. Las mismas estarán vigentes hasta el viernes 25 y el gobierno provincial está evaluando las habilitaciones y restricciones que regirán por las próximas dos semanas.

Según adelantó a LT10 el ministro de Trabajo Juan Manuel Pusineri la prioridad está puesta en la vuelta a la presencialidad en las escuelas bajo la modalidad de burbuja, tal como se vinieron dictando las clases desde el inicio de este ciclo lectivo.

El funcionario indicó que "es lo primero que está en agenda" para que los alumnos retornen aunque sea bajo la bimodalidad antes de las vacaciones de invierno.

Si bien, se fue avanzando en algunas localidades que no tienen riesgo epidemiológico por coronavirus, y se habilitaron las burbujas en el nivel inicial se espera que avancen las habilitaciones sobre las ciudades de Santa Fe y Rosario.

Pusineri dijo que "hay expectativa de retorno", pero aclaró que será "siempre que contemos con el aval sanitario y las recomendaciones de Salud que tendremos antes de realizar los anuncios".

**Otras posibles habilitaciones**

Dentro de las nuevas medidas se evalúa la ampliación del horario de cierre de los locales gastronómicos que actualmente es a las 19 y se podría extenderse hasta las 21 o a las 23. 

Además, podría decretarse la habilitación de la pesca deportiva y la actividad náutica. Y también se analiza extender el horario de circulación.

**¿Qué puede pasar?**

Según el ministro, estos son los puntos que se están estudiando antes que el gobernador haga los anuncios sobre la nueva etapa de convivencia en pandemia. Sin embargo, señaló que giran en torno a los pedidos que fueron recibiendo desde distintos sectores económicos.

Pero más allá de las solicitudes de estos rubros y sus necesidades, Pusineri aseguró que "si hay que inclinarse por una apertura será la de las escuelas. Resulta difícil justificar que avanzamos en las flexibilizaciones cuando no podemos avanzar en la presencialidad".

Pusineri precisó que los anuncios serán este viernes, luego de recibir las recomendaciones de los expertos de Salud, y las medidas comenzarían a regir el próximo sábado.