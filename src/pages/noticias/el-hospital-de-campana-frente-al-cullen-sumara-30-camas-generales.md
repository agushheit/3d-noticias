---
category: Estado Real
date: 2021-04-30T12:13:58Z
thumbnail: https://assets.3dnoticias.com.ar/hospital1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: El hospital de campaña frente al Cullen sumará 30 camas generales
title: El hospital de campaña frente al Cullen sumará 30 camas generales
entradilla: Así lo confirmó la ministra de Salud durante su recorrida por las instalaciones.
  “Nos enfrentamos a un virus muy contagioso y agresivo”, insistió.

---
Este martes arribó a la ciudad de Santa Fe el hospital de campaña que se está armando en el Liceo Militar, frente al hospital José María Cullen, tal como lo había anunciado el presidente Alberto Fernández. En ese contexto, este jueves la ministra de Salud, Sonia Martorano, recorrió las instalaciones junto al director del Cullen, Juan Pablo Poletti; el oficial de enlace del Ejército Nacional, Sergio Zovi; y el gerente de la Región Norte de Aguas Santafesinas, Jorge Loreficcio; entre otros funcionarios.

En ese marco, la titular de la cartera sanitaria explicó que el ingreso de internación “promedio es de 46 años y llegan 12 pacientes por día a terapia”. Es por ello que experó “necesitábamos de este apoyo por lo que agradecemos mucho al ministro de Defensa de la Nación, Agustin Rossi, que puso todo a disposición, y a todo el personal del Liceo que está trabajando”.

Martorano explicó que el hospital de campaña consta “de 4 pabellones, es decir, el ingreso, acceso, administración, se pueden colocar hasta 50 camas (ubicadas en diferentes salas), office de enfermería, office de médicos y médicas, los servicios de apoyo y la conexión directa entre el hospital de campaña y el hospital Cullen”.

“Deben resolverse algunas cuestiones logísticas de energía, agua, la circulación y conexión con el hospital pero consideramos que probablemente la semana próxima puede estar en funcionamiento”, anticipó la ministra.

Por su parte, Poletti agradeció “al Ministerio de Defensa, al personal del Liceo Militar y a través de ello los nexos con la EPE, Aguas Santafesinas y la Municipalidad ya que hay que llevar adelante diferentes logísticas para poder conectar un hospital con otro”.

El objetivo que tiene en claro el director del hospital es “sumar -en lo inmediato- 30 camas al hospital para poder garantizar la atención a todo aquel santafesino que requiera”.

Por el momento, Poletti confirmó que “se van a tratar pacientes no Covid y no críticos. Va a ser manejado por el mismo personal del hospital, como si fuese una sala más y va a estar conectado al sistema informático del hospital, a la admisión y a la farmacia. De esta manera podemos aprovechar todos los recursos que hoy tiene el hospital Cullen como tomografías, resonancia, laboratorio”.

“Las camas que desocupemos en el hospital general las vamos a agrupar en los pacientes no Covid y de esta manera podremos liberar una nueva sala Covid con bocas de oxígeno, que es lo que hoy vemos que podemos necesitar a futuro”, anticipó.

**Contexto sanitario**

La titular de la cartera sanitaria recordó que el “personal del hospital JM Cullen recibe a pacientes de todo el centro norte. Hemos derivado pacientes a Rosario con este sistema de regionalización”.

“El aumento de casos va de la mano con el aumento a nivel nacional. El número de ocupación de camas es muy alto”, sostuvo Martorano y recordó que “esta es una segunda expansión porque hay un límite, es finito y también tiene que ver con el recurso humano, hay que disminuir el contagio y evitar toda socialización”.

“La gente tiene que comprender que nos enfrentamos a un virus muy contagioso y muy agresivo, en horas una persona joven entra a un respirador y el tiempo que permanece en terapia es mayor”, finalizó la ministra.

**Nuevo Lote de Sputnik V.**

Por otro lado, cabe señalar que este jueves ingresó a la provincia un lote de 9.000 primeras dosis de vacunas Sputnik V contra el Covid-19.

De esta forma, el total de primeros componentes recibidos asciende a 584.800 dosis. Esto permite seguir avanzando con la población objetivo de 1.200.000 santafesinos y santafesinas.