---
category: Agenda Ciudadana
date: 2021-06-07T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/SPUTNIK.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Llega por Aerolíneas Argentinas el primer lote del principio activo para
  la fabricación de vacunas
title: Llega por Aerolíneas Argentinas el primer lote del principio activo para la
  fabricación de vacunas
entradilla: Además de las vacunas el avión traerá el principio activo para que pueda
  comenzar a producirse la vacuna desarrollada por el Instituto Gamaleya en el Laboratorio
  Richmond ubicado en el partido bonaerense de Pilar.

---
Un vuelo de Aerolíneas Argentinas saldrá este lunes hacia Moscú para traer el primer lote del principio activo de la vacuna Sputnik V para comenzar a ser producida en el país, así como una nueva partida de dosis de los componentes 1 y 2 del medicamento contra el coronavirus.

Esas dosis reforzarán, junto a las llegadas durante la semana anterior, el Plan de Vacunación contra el coronavirus que lleva adelante el Gobierno Nacional.

La partida del vuelo fue confirmada en Twitter por el presidente de la compañía, Pablo Ceriani, quien escribió: "Nuevo vuelo a Moscú, más dosis de vacunas para los argentinos y argentinas. Nuestra operación número 24 parte mañana 02am para traer al país más @sputnikvaccine. Es fundamental el ritmo con el que están llegando las vacunas para continuar con la campaña de vacunación", escribió Ceriani en la red social.

En tanto, el Gobierno nacional informó que "esta madrugada partirá hacia Moscú un vuelo de Aerolíneas Argentinas que traerá dosis 1 y 2 de Sputnik V y el primer lote de su principio activo para comenzar la producción en el país".

Es la vigesimocuarta operación que realiza la empresa de bandera en busca de inmunizaciones contra la Covid-19, y el primero que traerá el principio activo para que pueda comenzar a producirse la vacuna desarrollada por el Instituto Gamaleya en el Laboratorio Richmond de la Argentina.

Esta firma farmacéutica, ubicada en el partido bonaerense de Pilar, llevará adelante la formulación, filtrado y envasado con el fin fortalecer el Plan Estratégico de Vacunación que despliega el Gobierno nacional en todo el país y ampliar su acceso en América Latina.

"Empezamos ya el trabajo concreto para empezar a producir en la Argentina la Sputnik V", celebró el presidente Alberto Fernández el viernes último en una videoconferencia con su par ruso, Vladimir Putin.

"Estamos muy conformes con los logros que hemos alcanzado con esa vacuna porque millones de argentinos han visto preservar su vida gracias al desarrollo científico de Rusia, en el que siempre confiamos", dijo el mandatario argentino.

En tanto, la llegada de otra partida de vacunas de AstraZeneca, que se esperaba para este lunes, fue reprogramada por el laboratorio para fines de la semana que se inicia por una observación operativa.

En la semana que concluyó arribaron al país 2.148.600 dosis de la vacuna AstraZeneca, de producción conjunta con México, mientras Aerolíneas Argentinas -en su decimoctavo servicio desde Moscú- trajo 815.150 dosis de Sputnik V, el mayor cargamento que la compañía de bandera transportó desde la Federación Rusa en un solo vuelo. Así, la Argentina superó los 18,4 millones de inoculaciones recibidas contra el coronavirus.

**Las dosis recibidas**

Hasta ahora la Argentina ya recibió 18.450.150 dosis de vacunas: 8.933.895 Sputnik V (7.793.735 del componente 1 y 1.140.160 del componente 2), 4.000.000 de Sinopharm, 580.000 AstraZeneca – Covishield, 1.944.000 AstraZeneca a través del mecanismo Covax y 2.992.200 AstraZeneca-Universidad de Oxford cuyo principio activo se produjo en la Argentina.

En relación al grupo etario, entre las personas de más de 80 años, el 78% ya fue vacunado con la primera dosis, y el 23,8% con las dos dosis; entre las personas de 70 a 79 años, el 86,6% ya fueron vacunados con la primera dosis, y el 23,4% con las dos; entre los de 60 a 69 años, el 83,3% ya fue vacunado con la primera dosis, y el 15,5% con las dos. Finalmente, entre las personas de 20 a 59 años, el 17% ya recibió la primera dosis y el 6,2% ambas.

Con este flujo de llegada de vacunas, ayer se volvió a superar el récord de vacunaciones diarias con 356.454 inoculaciones aplicadas en todo el país, de acuerdo a los datos del Ministerio de Salud de la Nación y, considerando los últimos siete días, desde el jueves 27 hasta ayer inclusive, se realizaron en todo el país 1.823.824 aplicaciones de las distintas vacunas.

**Las dosis aplicadas**

Según el Monitor Público de Vacunación, el registro online que muestra en tiempo real el operativo de inmunización en todo el territorio argentino, hasta esta mañana fueron distribuidas en todo el país 17.670.690 vacunas y las aplicadas totalizan 14.092.493: 11.071.365 personas recibieron la primera dosis y 3.021.128 ambas.

En cuanto al vuelo de Aerolíneas, se trata de la vigesimocuarta operación de la empresa en búsqueda de vacunas contra el Covid-19, dieciocho de las cuales tuvieron como destino a la Federación Rusa, en tanto que las cinco restantes fueron a Beijing.

El vuelo número AR1062, a cargo del Airbus 330-200 matrícula LV-GIF, tiene previsto partir a las 2 de la madrugada del lunes y estima aterrizar en el Aeropuerto Internacional de Sheremetievo alrededor de la medianoche rusa, las 18 en nuestro país.

El regreso, bajo el número AR1063 está programado para las 4.30 del martes en Rusia, las 22,30 de mañana, en Argentina y el arribo al Aeropuerto Internacional de Ezeiza será a las 16.30 del martes.

"Es muy positivo que sigan llegando vuelos con este ritmo porque permite que la campaña de vacunación siga acelerándose. En Aerolíneas estamos muy felices de ser parte de la solución", señaló Ceriani.

Actualmente, las vacunas entregadas por el Fondo de Inversión Directa de Rusia (RIDF) y desarrolladas por el Instituto Gamaleya, son transportadas en contenedores del tipo "thermobox", de a 5 dosis por vial y refrigeradas a una temperatura de entre 18 y 20 grados bajo cero.

**Los otros vuelos de Aerolíneas**

Aerolíneas Argentinas lleva completados 18 vuelos a la Federación Rusa en los cuales fueron trasladadas un total de 8.951.440 de dosis.

Además, se realizaron 5 vuelos hacia Beijing, República Popular China en los que arribaron al país 3.659.200 de vacunas de Sinopharm.

De esta manera, en 23 vuelos completados hasta ahora, Aerolíneas Argentinas aportó 12.610.640 dosis de vacunas contra el Covid-19.

En lo que hace a la producción local de vacunas, el presidente de Laboratorios Richmond, Marcelo Figueiras, confirmó que la actual planta del laboratorio ubicada en Pilar tiene capacidad para realizar la formulación, filtrado y envasado de hasta 500 mil dosis de la vacuna Sputnik V por semana, "unos 2 millones por mes", siempre sujeto a la cantidad del principio activo que se envíe desde Rusia.

"Si todo sale bien la producción comienza la semana que viene; en este momento están fermentando en Moscú el principio activo del componente 1 de la vacuna y para antes de fin de mes podríamos tener las primeras dosis en la calle".

El empresario aclaró que "siempre está la posibilidad, en un proceso tan complicado como éste que algo surja que demore el trámite", pero destacó que "esperamos que no haya ningún inconveniente".