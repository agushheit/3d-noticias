---
category: Agenda Ciudadana
date: 2021-08-31T06:15:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/AFIP.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Monotributistas solicitaron más de 80 mil créditos a tasa cero en menos de
  72 horas
title: Monotributistas solicitaron más de 80 mil créditos a tasa cero en menos de
  72 horas
entradilla: Más de 1,5 millones de monotributistas pueden solicitar los créditos y
  las condiciones para acceder son las mismas que el año pasado, aunque se modificaron
  los parámetros de facturación.

---
La página web de la Administración Federal de Ingresos Públicos (AFIP) recibió y procesó más de 80 mil solicitudes de pequeños contribuyentes para acceder a los créditos a tasa cero, en los tres primeros días de vigencia del sistema, según informaron fuentes oficiales.

La herramienta fue diseñada para apuntalar la recuperación y acompañar a los pequeños contribuyentes con créditos por hasta 150 mil pesos que serán acreditados por las entidades bancarias seleccionadas en un solo desembolso en una tarjeta de crédito de cada monotributista.

A diferencia de 2020, ahora se fijaron límites menos restrictivos ya que pueden tramitar un crédito a tasa cero quienes hayan facturado hasta un 20% más que el piso de su categoría. Con los nuevos parámetros se incluye a quienes tuvieron caídas (2020) y se amplía el universo hasta quienes incrementaron su facturación un 20% (2021).

La normativa permite así que 6 de cada 10 monotributistas "puros" (aquellos que no tienen otra fuente de ingresos por estar en relación de dependencia o por jubilación) están habilitados para iniciar los trámites.

En aquellos casos que el contribuyente no cumple con las condiciones previstas en la normativa el sistema le informa los motivos por los cuales no le corresponde acceder.

En cuanto al monto máximo de los créditos, este será fijo y dependerá de la categoría en la que se encuentre cada monotributista.

Para la categoría A se podrán solicitar hasta 90 mil pesos de crédito; para la categoría B, hasta 120 mil; y para el resto de las categorías, hasta 150 mil pesos.

El monto mínimo son 10 mil pesos y los fondos se acreditarán en una tarjeta de crédito en un solo desembolso. Quienes no posean tarjeta de crédito deberán seleccionar el banco con el que operan habitualmente para continuar con la tramitación. El año pasado se emitieron más de 249.000 tarjetas a monotributistas que solicitaron los créditos a tasa cero.

Los beneficiarios contarán con 6 meses de gracia y la devolución se realizará en12 cuotas sin intereses.

Las fuentes oficiales detallaron que el servicio web de la AFIP estará habilitado hasta el 31 de diciembre de 2021 y las gestiones ante las entidades bancarias elegidas para solicitar el crédito podrán finalizarse hasta el 20 de enero de 2022.

El trámite se inicia a través de la página web de la AFIP con clave fiscal y clickear en la solapa "Crédito Tasa Cero". Al ingresar al servicio las personas podrán ver el monto del crédito que le corresponde de acuerdo a su categoría y deberán seleccionar la entidad financiera donde está emitida su tarjeta de crédito para finalizar el trámite.

Una vez finalizado el trámite cada monotributista deberá contactar a la entidad bancaria seleccionada en un plazo no menor a las 24 horas. Algunos bancos tienen aceitados los mecanismos para otorgar los créditos en forma virtual y otros requieren que el monotributista se presente en una sucursal.

Aquellos monotributistas que accedieron a un crédito a tasa cero en 2020 pueden volver a solicitarlo, en tanto quienes estén en una situación de mora, la entidad financiera destinará una parte del nuevo crédito para cancelar el saldo adeudado del préstamo anterior.

La normativa establece que para acceder a un crédito los monotributistas deberán no prestar servicios en el sector público nacional, provincial o municipal; no percibir ingresos por estar en relación de dependencia o por jubilación; y no estar en situación crediticia 3, 4, 5 o 6 informada por el BCRA.h