---
category: Estado Real
date: 2021-06-28T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/GENERO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se desarrolló el último encuentro del seminario internacional “Avances y
  Desafíos de Políticas Públicas para la Igualdad de Género en América Latina”
title: Se desarrolló el último encuentro del seminario internacional “Avances y Desafíos
  de Políticas Públicas para la Igualdad de Género en América Latina”
entradilla: Organizado por el Programa de Cooperación Triangular Costa Rica – España,
  el Instituto Nacional de las Mujeres de Costa Rica y el Gobierno de la Provincia
  de Santa Fe

---
El Gobierno de Santa Fe, a través de la Secretaría de Estado de Igualdad y Género, el Instituto Nacional de las Mujeres (INAMU) y la Secretaría de Cooperación Internacional e Integración Regional llevaron adelante el cuarto encuentro del Seminario Internacional “Avances y desafíos de políticas públicas para la Igualdad de Género en América Latina”. Esta jornada abordó las políticas ambientales y el Desarrollo Sostenible con perspectiva de género.

El Instituto Nacional de las Mujeres (INAMU) y el Ministerio de Igualdad, Género y Diversidad de la provincia de Santa Fe, iniciaron un proceso de cooperación para la construcción y seguimiento de Políticas Públicas para la Igualdad de Género que concluirá con la formulación del Plan Estratégico Provincial de Igualdad 2020-2030 de Santa Fe. Este proyecto de cooperación triangular se enmarca en la Cuarta Fase del Programa de Cooperación Triangular Costa Rica-España para el fortalecimiento de las capacidades de la Provincia de Santa Fe.

Durante el último encuentro expusieron: Erika Gonnet, Ministra de Ambiente y Cambio Climático de Santa Fe; Lorena Aguilar, Ex Viceministra de Relaciones Exteriores de Costa Rica y María Elena Herrera, Coordinadora Técnica de la Estrategia Nacional REDD+ de Costa Rica. Moderó el encuentro: Pamela Castillo Barahona, Ex Viceministra de Ambiente del Ministerio de Ambiente y Energía del Gobierno de Costa Rica.

Durante el encuentro, la Ministra de Ambiente y Cambio Climático de Santa Fe; Erika Gonnet expresó: "Santa Fe es la primera provincia del país en tener una ley de acción climática y esta ley se realizó con perspectiva de género. Nosotros destacamos esto porque creemos que es un gran disparador de acciones. Si bien la problemática del cambio climático es global, la respuesta requiere un enfoque local que incluya la realidad de las mujeres y disidencias de la provincia". Y continuó: "nos referimos a acciones concretas con perspectiva de género en el fomento de la movilidad activa, el empleo verde, la economía circular, la sustentabilidad energética, el cuidado y protección de la biodiversidad, entre otras".

Por su parte, Lorena Aguilar, sostuvo que: “de 143 países, de acuerdo al Banco Mundial, 90% tienen al menos una ley que restringe la igualdad económica de las mujeres. En solamente 28 países las mujeres tenemos los mismos derechos legales que los hombres a la tenencia de la tierra. De acuerdo al último estudio del Bank of America se pierde en riqueza, en capital humano, por desigualdad de género 160 billones de dólares al año”. Luego detalló: “la economía verde no es igualitaria. Sin embargo, es el sector de las energías renovables en el que más mujeres se han incorporado, en relación a otras industrias energéticas adicionales. Eso se convierte en un área muy importante para la recuperación post covid”.

Al momento de exponer, María Elena Herrera manifestó que: “en Costa Rica sólo el 15% de las tierras tituladas se encuentran en manos de mujeres. Y ahí tenemos una gran desventaja de la participación de éstas en diferentes actividades”. Además, hizo mención al programa “Más mujeres, más natura”, que tuvo su origen en el año 2020 en Costa Rica y con el cual se buscó: “promover una serie de acciones marco para, dentro de las instituciones, poder visibilizar más la participación de las mujeres”, según las propias palabras de Herrera.

**Acerca del programa**

El proyecto Cooperación Triangular Costa Rica - España - Argentina forma parte de la IV fase del Programa Cooperación Triangular Costa Rica-España que es co-financiado con el apoyo de la Agencia Española de Cooperación para el Desarrollo (AECID) e implementado por el Instituto Nacional de las Mujeres (INAMU) y el Gobierno de Santa Fe. El Programa Cooperación Triangular Costa Rica-España promueve la Cooperación Sur-Sur en el ámbito latinoamericano y caribeño, e incentiva, particularmente, la cooperación desde un país de renta media como Costa Rica hacia otros países del istmo y Caribe.

En el marco del proyecto de Cooperación Triangular Costa Rica-España-Argentina, todos los miércoles de junio se llevó a cabo el Seminario Internacional: Avances y desafíos de políticas públicas de Igualdad de Género en América Latina con el objetivo de que se adquieran insumos para el mejoramiento de las políticas, así como, material valioso para la elaboración del Plan Estratégico Provincial de Igualdad 2020-2030 de Santa Fe.

En este seminario se destacaron cuatro ejes temáticos que fueron definidos en función de asuntos que se consideran medulares para alcanzar la igualdad y que se trabajan en la región en el marco de las políticas de igualdad de género y que hoy en día cobran mayor importancia debido a la afectación por la pandemia mundial por la COVID 19, principalmente en el ámbito económico, de bienestar y seguridad personal.

Con este último encuentro concluyó el Seminario Internacional “Avances y desafíos de políticas públicas para la Igualdad de Género en América Latina”. Al respecto, Constanza Estepa, coordinadora técnica por el Gobierno de Santa Fe del Proyecto Triangular señaló: “definimos nuestra visión de las políticas públicas desde una mirada asociativa y no competitiva. Así llegamos a convertirnos en el único proyecto seleccionado por la Cooperación Triangular para ser implementado en nuestro país”, en el mismo sentido expresó: “durante el Seminario Internacional construimos y afianzamos más de 400 relaciones que nos permitirán nutrirnos con nuevos aprendizajes y ampliar el campo de acción a través de lógicas colaborativas e inclusivas en relación a las políticas públicas. De esta manera, asumimos el desafío de convertir nuestros sueños en un Plan Estratégico Provincial 2030 respetuoso de los derechos humanos y la igualdad de géneros”.