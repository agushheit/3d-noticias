---
category: Agenda Ciudadana
date: 2021-03-30T07:26:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/trans.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: 'Polémica por la creación de un EEMPA para travestis y trans: "Eso no es
  igualdad"'
title: 'Polémica por la creación de un EEMPA para travestis y trans: "Eso no es igualdad"'
entradilla: Alejandra Ironici, militante por los derechos de la comunidad LGBTIQ,
  dijo que la propuesta sería como "hacer un ghetto" y que, por el objetivo, el objetivo
  debe ser la inserción de travestis y trans en la sociedad.

---
El Gobierno de Santa Fe anunció la creación de una secundaria para transexuales, travestis y disidencias, y la polémica no se hizo esperar.

“No creo que sea por el lado que debamos de ir”, cuestionó por LT10 Alejandra Ironici, militante por los derechos del colectivo LGBTIQ, quien se manifestó en contra de “hacer un ghetto” para ese colectivo.

“Lo que tenemos que lograr es que la sociedad empiece a respetarnos. Yo no quiero una universidad para Alejandra Ironici o una universidad para las travas. Yo quiero una universidad que a mí me contenga y me contemple. Quiero un barrio donde me den una vivienda por el hecho de ser quien soy. No que hagan como en Estados Unidos que hay un barrio gay alejado de un montón de cosas, y que a nosotras también nos terminen estigmatizando y violentando aun más”, explicó.

“Eso no es la igualdad. Los sistemas educativos deben ser permeables”, completó.

Ironici admitió que la comunidad travesti- trans tiene “determinados códigos” que le son propios, pero rechazó que ello deba traducirse en generar una escuela exclusiva. “Eso sería darle más lugar a que la sociedad nos siga juzgando y nos sectorice más todavía”.

Ironici no descartó que algunas compañeras trans apoyen la iniciativa del gobierno, pero abogó porque la idea sea “querer estar insertadas en la sociedad”.

“Si no, decimos ‘vamos a hacer un plan de vivienda para las trans. ¿A dónde? Allá en el confín del mundo, donde no hay nadie, las tiramos y las amontonamos’. Y eso es doble violencia”, cerró.