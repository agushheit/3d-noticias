---
category: Agenda Ciudadana
date: 2021-09-25T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/RETENCIONES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno elimina las retenciones a las exportaciones de servicios a partir
  de 2022
title: El Gobierno elimina las retenciones a las exportaciones de servicios a partir
  de 2022
entradilla: Así lo anunció el ministro de Economía en el marco de una reunión con
  representantes de Argencon, Accenture, Globant, PWC, Ernest & Young, Tax&Legal,
  Laboratorios Bagó y el INVAP, informó esta tarde la cartera económica.

---
Guzmán cuestionó a la gestión de Juntos por el Cambio, al sostener que “haber puesto derechos de exportación para el sector en el año 2018 se hizo desde una mirada más bien fiscalista y no productiva”, y aseguró que el Gobierno seguirá trabajando para “dar previsibilidad sobre el año 2022” para que el sector cuente con más certezas en sus planes de inversión y exportación.  
  
Por su parte, Kulfas resaltó el dinamismo del sector exportador de servicios y la capacidad de crear empleo de calidad de las empresas que lo componen.  
  
Por este motivo, el titular de Desarrollo Productivo sostuvo: “Apostamos a que este sector duplique sus exportaciones, duplique la cantidad de gente trabajando, apuntando a fortalecer la oferta de chicos y chicas que trabajen en el sector, con capacitación”.  
  
Guzmán y Kulfas estuvieron acompañados por el secretario de Política Tributaria, Roberto Arias, y la subsecretaria de Economía del Conocimiento, María Apólito.  
  
Arias insistió en el carácter fiscalista que tuvo la decisión de la administración anterior de gravar a las exportaciones de servicios y destacó que “en ningún lugar del mundo se aplican derechos aduaneros a los servicios”.  
  
La Ley de Solidaridad Social y Reactivación Productiva, estableció en primer lugar una reducción de la alícuota al 5% que rigió durante todo el 2020, mientras que durante 2019 había sido entre 11% y 7%.  
  
A fines de ese año, con el decreto N° 1034/2020 reglamentario de la Ley de Economía del Conocimiento, se estableció que a partir del año 2021 aquellas empresas adheridas al Régimen de Promoción de la Economía del Conocimiento no pagarían derechos de exportación.  
  
A partir del 1 de enero de 2022, tanto inscriptos como no, en el Régimen no pagarán derechos de exportación