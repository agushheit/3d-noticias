---
category: La Ciudad
date: 2020-12-04T11:28:36Z
thumbnail: https://assets.3dnoticias.com.ar/CARAVANA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Multitudinaria caravana por la inseguridad
title: Multitudinaria caravana por la inseguridad
entradilla: 'Tal como estaba previsto, este jueves se realizó una nueva caravana por
  la inseguridad vigente en la capital provincial. '

---
La iniciativa la tuvo la Subcomisión de Seguridad de la "Vecinal B° Roma", que extendió la invitación a toda la ciudad. La Red de Vecinales brindó su apoyo, al igual que los deliveries, la Cámara de Kiosqueros y vecinos autoconvocados.

"Todos los días tenemos hechos de inseguridad. Las metodologías varían. Siempre andan en moto, sin patente. El último sábado, a las 8.30 de la noche, a un vecino de Tucumán al 3900 le quisieron robar. 

No pudieron porque el señor empezó a los gritos y salieron otras personas. Ahí eran 3 en dos motos. 

Ese mismo día, también a un auto que estaba parado le rompieron la luneta y le robaron una mochila y herramientas. Y esto es algo que ocurre en todos los barrios de la ciudad", comentaba en la previa Jorge Tira, el Presidente de la Sub Comisión de Seguridad de la Vecinal Barrio Roma.

Impunidad, robos que no cesan, presencia policial insuficiente en todos los barrios a cualquier hora del día se pide ayuda. Los reclamos se multiplican, pero las respuestas no llegan.

Hay barrios que están azotados por la inseguridad. “A cualquier hora se escuchan tiros. Barranquitas es un ejemplo de ello. Barrio Roma también está complicado, también Guadalupe. 

Nada cambió, por el contrario, la pandemia empeoró estas situaciones también”, comentó Ileana Alvarenga, una de las vecinas autoconvocadas.

  
 