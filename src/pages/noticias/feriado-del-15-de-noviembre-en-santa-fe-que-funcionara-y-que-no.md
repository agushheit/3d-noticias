---
category: La Ciudad
date: 2021-11-14T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERIADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Feriado del 15 de noviembre en Santa Fe: qué funcionará y qué no'
title: 'Feriado del 15 de noviembre en Santa Fe: qué funcionará y qué no'
entradilla: " El próximo lunes 15 de noviembre es feriado administrativo en todo el
  territorio de la Provincia, con motivo de la conmemoración de la fundación de la
  ciudad de Santa Fe. \n"

---
La Ley establece en su artículo 2º que ese día el Gobierno de la Provincia establecerá su sede en Cayastá, departamento Garay, en el sitio donde se han descubierto las ruinas de la primitiva ciudad de Santa Fe.

Por tratarse de un asueto administrativo, el próximo lunes 15 no abrirán las oficinas públicas. Tampoco habrá clases. En cuanto al comercio, la industria y los bancos, la ley aclara que la apertura es de “carácter optativo”.

 **Servicios en la ciudad capital**

 La Municipalidad informa los horarios y servicios que se prestarán durante el feriado del lunes 15 de noviembre, oportunidad en que se conmemorarán los 448 años de la fundación de Santa Fe.

 En ese sentido, la recolección de basura y el barrido de calles serán prestados con normalidad por las empresas Cliba y Urbafe.

 Asimismo, los Eco Puntos también funcionarán en los horarios y espacios habituales: la Dirección de Deportes en la Costanera Oeste (avenida Almirante Brown 5294), la Estación Belgrano (bulevar casi Dorrego); y el Parque Alberdi (Rivadavia y Cortada Falucho). En estos espacios se brinda atención personalizada de lunes a domingos, de 9 a 19 horas.

 **TRANSPORTE Y ESTACIONAMIENTO**

Por otra parte, el transporte urbano de colectivos tendrá frecuencias similares a los días sábados. Y con respecto al Sistema de Estacionamiento Medido (SEOM), no estará operativo.

 **CEMENTERIO**

En cuanto al Cementerio Municipal, se informa que los horarios de visita e inhumaciones  serán de 7.30 a 12.30.

 **MERCADO NORTE**

Finalmente, cabe señalar que el tradicional Mercado Norte, ubicado en Urquiza y Santiago y Santiago del Estero, abrirá sus puertas de 9 a 13 horas.

 