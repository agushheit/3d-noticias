---
category: La Ciudad
date: 2021-06-26T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASSA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Analizan sumar bombas a las tomas de agua para que no peligre el suministro
title: Analizan sumar bombas a las tomas de agua para que no peligre el suministro
entradilla: 'Desde Aguas Santafesinas informaron que hasta el momento están activas
  los 8 dispositivos de captación ubicados en las plataformas de Colastiné Norte y
  la cercana al Puente Oroño. '

---
El río en el puerto de Santa Fe marcó este viernes al mediodía 27 centímetros, sin variaciones respecto a las últimas 24 horas. Con este panorama de bajante pronunciada del Paraná, Aguas Santafesinas (Assa) analiza día a día el escenario que acontece en sus tomas de agua para que no peligre el suministro del servicio.

“El mayor desafío es garantizar la cantidad de agua. El hecho de la bajante que persiste y que, aparentemente, en julio va a tener su pico máximo y no se sabe cuándo va a repuntar, hace que nos tengamos que preparar. Por ahora no tenemos afectación del servicio, pero es una posibilidad que la tengamos y que no podamos captar el agua que necesitamos para distribuir”, indicó Guillermo Lanfranco, gerente de Relaciones Institucionales de Aguas Santafesinas.

Assa cuenta con unas 10 bombas de succión de agua entre la toma ubicada en la costa de Colastiné Norte en la desembocadura con el río Ubajay (3 bombas en servicio); y la toma Hernández en río Santa Fe (5 bombas activas), situada entre el Puerto local y el Puente Oroño.

“Buscamos todos los medios para asegurar la captación en nuestras tomas. Las bombas instaladas funcionan, pero también analizamos agregar bombas extras sobre la misma plataforma de toma”, mencionó Lanfranco y explicó que “mientras haya posibilidad de montar bombas dentro del mismo muelle no se necesita otra estructura adicional. Hoy tenemos capacidad para montar más bombas, por eso analizamos esa posibilidad; o trabajar sobre las que ya están instaladas y bajarlas, literalmente, para que sigan teniendo capacidad hidráulica, ya que si las dejas muy arriba toman aire”.

Al consultarle si la estructura de succión está operativa, el gerente de Relaciones Institucionales sostuvo: “Toda la capacidad que tenemos está trabajando, y captamos el agua que necesitamos. Pero en vistas a los escenarios que se pueden presentar, estamos viendo de reforzar los medios de captación”.

En cuanto a obras complementarias que podría demandar la falta de caudal, como por ejemplo trabajos de dragado, indicó que “en principio no es necesario porque todavía tenemos ´fondo\`, hasta hoy no está previsto. A veces la modificación del lecho tampoco es buena porque se desprende sedimento y no lo hemos tomado como un método, además los muelles de las tomas están ubicados en un lugar adecuado para la tarea”.