---
category: Agenda Ciudadana
date: 2021-01-20T09:43:42Z
thumbnail: https://assets.3dnoticias.com.ar/lilita.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de La Nación
resumen: 'Elisa Carrió: "Vamos a pedir la intervención federal de Formosa"'
title: 'Elisa Carrió: "Vamos a pedir la intervención federal de Formosa"'
entradilla: El Senador Luis Naidenhoff presentó un Habeas Corpus colectivo, y Elisa
  Carrio, en un video en Twitter, informó de su pedido de intervención federal

---
La exdiputada Elisa Carrió difundió en redes sociales un audio en el que alerta sobre la "gravísima violación de derechos humanos en Formosa" y aseguró que, desde su espacio político, solicitarán "la intervención de Comisión de Derechos Humanos, una visita in situ y la intervención federal del poder ejecutivo de provincia de Formosa".

Según la dirigente, la creación de un centro de aislamiento de la provincia "viola el tratado sobre la tortura, el derecho humano a circular y otra multiplicidad de derechos humanos básicos previstos en la convención Interamericana de derechos humanos".

[![](https://assets.3dnoticias.com.ar/tuit_lilita.jpg)](https://twitter.com/elisacarrio/status/1351576094286237696?s=20 "Twitter")

***

Sobre la imposibilidad de entrar a la provincia, Carrió dijo: "Cualquier persona que deba viajar a Formosa para visitar a un familiar puede interponer un habeas corpus en cualquier jurisdicción federal del país".

El gobernador de Formosa, Gildo Insfrán, y el ministro de Gobierno provincial, Jorge Abel González, fueron denunciados penalmente días atrás por hacinamiento y "condiciones inhumanas" en un centro de aislamiento de la provincia para personas con coronavirus.

La abogada Gabriela Neme, quien representa a un grupo de pacientes que fueron atendidos en el Centro de Aislamiento Preventivo del Estadio Cincuentenario, dijo en diálogo con TN: "He radicado la denuncia penal contra el gobernador de la provincia y contra el ministro de Gobierno, representante y cabeza de la mesa de emergencia, porque es aberrante lo que está viviendo esta gente".

Además, expresó que el Estadio Cincuentenario está preparado como un centro de asistencia para personas con Covid positivo, pero también hay gente con diagnóstico sin resultado después de cuatro días. "Están propagando la pandemia con gente sana, y por eso ya pedí que a las personas que no tienen coronavirus las saquen de inmediato de allí", agregó.

Por su parte, el senador nacional Luis Naidenoff, de la UCR, presentó “…un habeas corpus colectivo por el hacinamiento y las condiciones inhumanas de las personas alojadas en centros de aislamiento de Formosa”.

***

[![](https://assets.3dnoticias.com.ar/tuit_naidenof.jpg)](https://twitter.com/luisnaidenoff/status/1351529899958349827?s=20 "Twitter")

Naidenoff argumenta que el Estado provincial de Formosa “decidió alojar a cientos de personas de distintas edades y sexo, algunos de ellos con PCR positivo para Covid 19 y otros a la espera de resultados o con PCR negativos”.

Antes de ello, la Dra. Ana Gabriela Neme, concejala formoseña, denunció penalmente al Gobernador Gildo Insfran y al Ministro de Gobierno Jorge Abel González, por el delito del “art 205 del Código Penal, por propagación de epidemia, al juntar gente sin diagnóstico o negativo, con personas diagnosticadas con covid, personas de riesgo y niños y niñas” (Twitter @gabrielaneme)

No es la primera vez que esto ocurre. El 6 de  noviembre, Amnistía  internacional publicaba en su sitio web ([https://amnistia.org.ar/](https://amnistia.org.ar/personas-varadas-en-formosa-amnistia-internacional-exige-medidas-urgentes-y-respetuosas-de-los-derechos-humanos/ "Amnistia")) “La realidad de los formoseños varados en el país es inhumana. Tanto las autoridades provinciales como nacionales deben dar una respuesta a los ciudadanos que esperan la demorada autorización para cruzar las fronteras. El Estado nacional no puede escudarse en la estructura federal del país para deslindar su responsabilidad ni su obligación y compromiso con los derechos humanos de todas las personas. A más de 200 días de la declaración de la emergencia sanitaria, deben tomarse medidas urgentes para que las personas puedan volver a su casa”, sostuvo Mariela Belski, directora ejecutiva de Amnistía Internacional Argentina.

Mario Rubén Ledesma, de 23 años, murió ahogado en octubre en el río Bermejo cuando intentaba cruzar a nado desde Chaco a Formosa para ver a su hija, con quien no tenía contacto desde febrero.