---
category: Agenda Ciudadana
date: 2020-12-24T10:50:22Z
thumbnail: https://assets.3dnoticias.com.ar/2412protesta-medicos.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Un sector de los trabajadores de la salud extiende el paro otras 24 horas
title: Un sector de los trabajadores de la salud extiende el paro otras 24 horas
entradilla: El Sindicato de Profesionales de la Salud (Siprus) emitió este miércoles
  23 de diciembre un comunicado en el que informó que el paro que realizan se extendería
  otras 24 horas.

---
Al igual que los trabajadores estatales, de Assa y el Enres, los trabajadores de la salud nucleados en Amra y Siprus realizaron una medida de fuerza en rechazo al ofrecimiento salarial realizado por el gobierno provincial.

En todos los casos, el paro era por 24 horas (se cumple este miércoles), pero desde Siprus decidieron prolongarlo un día más, por lo que también **paran este 24 de diciembre, garantizando guardias mínimas**.

En el mensaje difundido, desde el sindicato indicaron que «ante la contundencia de la medida de fuerza realizada hoy por todos los gremios estatales, el gobierno provincial decidió como respuesta modificar el asueto que desde hace años se otorga en la provincia todos los 24/12. No alcanzó con no realizar ningún ofrecimiento en la paritaria, ni la humillación que sufrimos todo el año los que estuvimos poniendo el cuerpo a la pandemia. No alcanzó con negarnos el descanso que necesitamos. Esta medida tiene por objetivo ponernos de rodillas. Quieren que aceptemos que las cifras en negro vinieron para quedarse, que vamos a cobrar por debajo de la inflación. Y al que no le gusta, esta es la respuesta».

«Rápido resolvieron esta medida -prosigue el comunicado-, mientras por otro lado ni siquiera hay una nueva fecha de paritaria. No se trata de un día más o menos de trabajo. Se trata de lo que pretende el gobierno de Perotti para el sistema de salud pública. Si los trabajadores vamos a aceptar o si vamos a resistir el ajuste».

La nota de Siprus aclara que, desde el gremio «dejamos en claro que no aceptamos que se avance contra nuestros derechos. Informamos que el CDP ha definido continuar con la medida de fuerza mañana. Y si no hay convocatoria a paritarias se tomarán medidas más contundentes la semana que viene. La salud pública no es un negocio, es un derecho de la población, y los trabajadores vamos a defenderla».