---
category: Estado Real
date: 2021-02-22T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/denguesantafe.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia realizará un ciclo de reuniones interdisciplinarias para abordar
  problemáticas Santafesinas
title: La Provincia realizará un ciclo de reuniones interdisciplinarias para abordar
  problemáticas Santafesinas
entradilla: El primer encuentro del ciclo, organizado por la Secretaría de Ciencia,
  Tecnología e Innovación, se realizará de forma virtual el jueves 25 de febrero,
  a las 10, y la temática girará en torno al dengue.

---
El Ministerio de Producción, Ciencia y Tecnología, a través de la Subsecretaría de Proyectos Científicos y Tecnológicos de la Secretaría de Ciencia, Tecnología e Innovación, realizará “Proyectemos Juntos”, un ciclo de reuniones virtuales interdisciplinarias para el abordaje de problemáticas santafesinas.

El primer encuentro se realizará el jueves 25 de febrero a las 10 horas, en modalidad virtual, y se titulará: “Dengue: un análisis transversal”.

Para participar de la actividad, las personas interesadas deberán completar el formulario a modo de inscripción haciendo CLIC ACÁ. Posteriormente, se enviará un link de Google Meet para el acceso a la reunión donde cada expositor o expositora deberá explicar brevemente acerca de su línea de trabajo.

**El Ciclo**

El ciclo de reuniones “Proyectemos Juntos” tiene como objetivo generar un espacio de encuentro y vinculación de distintas áreas de la sociedad que estén atravesadas por una problemática determinada.

Al respecto, la subsecretaria de Proyectos Científicos y Tecnológicos, Eva Rueda, destacó que “lo que nos une (Juntos) es alguna situación que, para resolverla, necesita tener un análisis conjunto desde diferentes aristas y miradas”.

“En esta oportunidad, ‘Dengue’ es un tema que es tratado por diferentes grupos de investigación a lo largo del territorio de la provincia de Santa Fe, por el Ministerio de Salud y algunas empresas privadas. Por ello, queremos tener un espacio donde podamos debatir políticas públicas basadas en I+D+i para diagramar soluciones (Proyectemos) para estas problemáticas”, agregó la funcionaria.