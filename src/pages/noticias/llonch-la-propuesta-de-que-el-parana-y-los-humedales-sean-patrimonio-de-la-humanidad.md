---
category: Agenda Ciudadana
date: 2020-12-15T12:00:09Z
thumbnail: https://assets.3dnoticias.com.ar/lonch.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Llonch: la propuesta de que el Paraná y los Humedales sean patrimonio de
  la Humanidad'
title: 'Llonch: la propuesta de que el Paraná y los Humedales sean patrimonio de la
  Humanidad'
entradilla: Luego de recibir el acta de Apoyo del Concejo de Rosario, el ministro
  de Cultura de la Provincia de Santa Fe manifestó que trasladará la iniciativa al
  gobernador Perotti.

---
El ministro de Cultura, Jorge Llonch, recibió este lunes de manos del director de la cátedra del Agua y del Centro Interdisciplinario del Agua de la Facultad de Ciencia Política de la UNR, Anibal Faccendini, la **Declaración de Apoyo del Concejo Municipal de Rosario para que el río Paraná y los Humedales sean declarados personas no humanas sujetas a derecho**.

La entrega fue realizada a ambas márgenes del Paraná, en la ex Zona Franca de Bolivia, donde funciona el club Náutico Rosario, y en el área denominada El Banquito, en la zona de islas. Estuvieron presentes los alumnos de la Cátedra del Agua, Camila Fernández y Rodolfo Martínez.

El funcionario indicó: “**Vamos a presentarle al gobernador Omar Perotti el proyecto para que la Unesco declare al río Paraná y los Humedales como Patrimonio Mundial de la Humanidad**, porque consideramos que hay intereses muy concretos que quieren apropiarse de un recurso tan vital como lo es el agua”.

En ese sentido, Llonch expresó que “hasta el siglo XVIII se subastaban personas, en el siglo XIX, en las Provincias Unidas del Río de la Plata, existía la esclavitud. Ahora vienen por el agua, y no podemos permitir que ese recurso, que en el caso de la cuenca del Paraná brinda vida a 200 millones de personas, sea subastado como una mercancía más, que cotice en las bolsas como si fuera un _commodity_”.

Para el ministro, “el agua y los humedales son bienes de la naturaleza con fuerte incidencia en nuestra cultura, en los distintos géneros musicales, como el chamamé por ejemplo”. Además, fundamentó “la importancia de la declaración en **la formación de una nueva cultura ciudadana con la naturaleza**”, y comentó la posibilidad de realizar -junto con la cátedra del Agua- el proyecto generado por dicha entidad de la Nueva Cultura Ciudadana, en el ámbito de la casa, el barrio, la ciudad y la región.

Por su parte, Faccendini agradeció “la actitud positiva del ministro Jorge Llonch, por la recepción de la Declaración de Apoyo del HCM Rosario para que el Paraná y los Humedales sean personas jurídicas, es decir personas no humanas sujetas a Derecho”.

Asimismo aclaró que “actitudes como la de Llonch son muy importantes para proteger el Paraná y los Humedales de la depredación, y también para evitar la mercantilización del agua, como lo está haciendo Wall Street. **El agua y el aire son derechos humanos y bienes comunes públicos. Están fuera de todo mercado**. Los derechos NO tienen precio”.

Por último, tanto Llonch como Faccendini, como símbolo de la Nueva Cultura Ciudadana, pusieron los pies en el agua del Paraná y los Humedales.