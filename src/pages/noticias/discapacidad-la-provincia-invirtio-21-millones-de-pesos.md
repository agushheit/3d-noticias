---
category: Estado Real
date: 2020-12-28T10:46:32Z
thumbnail: https://assets.3dnoticias.com.ar/2812-discapacidad.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Discapacidad: la provincia invirtió 21 millones de pesos'
title: 'Discapacidad: la provincia invirtió 21 millones de pesos'
entradilla: Adquirió sillas de ruedas especiales para encefalopatías, posturales,
  y audífonos cubriendo una demanda pendiente desde 2018.

---
La provincia de Santa Fe, a través de la subsecretaría de Inclusión para Personas con Discapacidad y de la secretaría de Administración del Ministerio de Salud, invirtió $ 21.809.000 en distintos elementos **destinados a personas con discapacidades severas, sin obra social**.

Específicamente se trata de sillas de ruedas con características especiales, posturales, y para personas con encefalopatías por un monto total de $ 8.809.000. Asimismo, de audífonos para personas sordas o hipoacúsicas por un valor de $ 13.000.000, entre otros elementos ortopédicos.

Al respecto, el subsecretario de Inclusión para Personas con Discapacidad de la provincia, Patricio Huerga, destacó que «de este modo estamos dando respuesta a una demanda que estaba pendiente».

Por otra parte, persistía una demanda insatisfecha de estos elementos que databa de años antes de que comenzara la actual gestión, impactado en las personas con discapacidad de más escasos recursos.

«Pensamos que una persona que necesita una silla de ruedas la necesita ya, no dentro de dos años. Afortunadamente en 2020 pudimos ir comprando y otorgando sillas de ruedas comunes. Pero las especiales son muy costosas y no nos fue fácil. Pero ahora la provincia hizo un esfuerzo muy grande y logramos resolverlo», reflexionó y concluyó Patricio Huerga.