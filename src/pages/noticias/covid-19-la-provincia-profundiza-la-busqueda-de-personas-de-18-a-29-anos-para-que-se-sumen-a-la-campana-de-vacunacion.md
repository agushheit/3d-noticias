---
category: Estado Real
date: 2021-07-18T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNATEWACHIN.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid-19: la provincia profundiza la búsqueda de personas de 18 a 29 años
  para que se sumen a la campaña de vacunación'
title: 'Covid-19: la provincia profundiza la búsqueda de personas de 18 a 29 años
  para que se sumen a la campaña de vacunación'
entradilla: El operativo se desarrolla en toda la provincia y apunta a todos los jóvenes
  de esa franja etaria para que se inscriban en el registro de vacunación provincial

---
El Ministerio de Salud de la provincia comenzó un operativo de búsqueda y concientización territorial, casa por casa, de la población joven de 18 a 29 años que todavía, y por diversas razones, no se inscribió al plan de vacunación contra el Coronavirus.

“La campaña de inmunización avanza a muy buen ritmo, y ya estamos vacunando a menores de 30 años, una población muy amplia a la que queremos llegar, es la que mayor cantidad de contagios reporta actualmente”, explicó Sebastián Torres, coordinador de Dispositivos Territoriales del Ministerio de Salud.

“Vacunarnos es el pasaporte para poder volver a encontrarnos con nuestros amigos y amigas, para disfrutar de esos tiempos y espacios que antes disfrutábamos”, enfatizó y apeló: “Por eso les pedimos que por favor se inscriban, que se saquen todas las dudas que puedan tener sobre las vacunas, si las tienen, estamos dispuestos a ofrecerles toda la información necesaria”.

Posteriormente, precisó que de esta franja etaria sólo se registró el 60 por ciento “y por eso estamos haciendo estos operativos de búsqueda junto con los centros de salud barriales”.

**Dejar atrás la pandemia con cada vacuna**

En relación a lo anterior, Sebastián Torres explicó que “por lo que estuvimos investigando hay muchas causas por las que no se

“Queremos que comprendan que la vacuna es un modo de comenzar a dejar atrás la pandemia, de reducir las complicaciones por Covid-19 y disminuir los contagios”, interpeló y animó a los jóvenes el coordinador de Dispositivos Territoriales.

Asimismo, informó que “ya se realizaron operativos similares como el de hoy (viernes) por la mañana en el barrio Las Flores (de Rosario). Del mismo modo, si encontramos a personas de edades más adultas sin vacunarse, también las invitamos a inscribirse”.

Otras de las razones –destacó Sebastián Torres– es que “el operativo de vacunación avanzó tan rápido desde junio hasta ahora, que muchos jóvenes aún no se han inscripto. Llegó el momento, es tiempo de que se sumen, de que se anoten”.

En relación a ello, precisó que esta semana llegaron 325.300 vacunas, por lo que la provincia envió más de 220.000 turnos. “Particularmente hoy enviamos más de 100.000 turnos para la semana que viene”, puntualizó Torres.

Y concluyó: “Seguimos inoculando a buen ritmo, por eso, iniciamos este tipo de operativo territorial para poder sumar más inscriptos”.