---
category: Agenda Ciudadana
date: 2021-10-20T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/BACHEOCOSTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Trabajos de iluminación y bacheo previstos para este miércoles
title: Trabajos de iluminación y bacheo previstos para este miércoles
entradilla: La Municipalidad informa el avance del cronograma de tareas previstas
  en los planes de Bacheo y Santa Fe se Ilumina. A medida que se desarrollan los trabajos,
  puede haber cortes de circulación y desvíos de colectivos.

---
En el marco del plan Santa Fe se Ilumina, la Municipalidad concreta tareas tendientes a recuperar el alumbrado existente en toda la ciudad, para llegar a fin de año al 40% de la ciudad con luces led. Cuadrillas municipales trabajan en la colocación de nuevas columnas de iluminación y artefactos led en distintos barrios y avenidas, como así también en el mantenimiento de la red actual de la ciudad.

En ese sentido, se trabajará en la colocación de nuevas luminarias en:

* Los barrios: Siete Jefes, Guadalupe, Roma, Fomento 9 de Julio, Yapeyú y Ceferino Namuncurá
* “Avenidas de tu barrio”: Cochabamba, entre Fray Cayetano Rodríguez e Iturraspe

Santa Fe se ilumina es un programa municipal a través del cual para fin de año el 40% de la ciudad tendrá luces led

**Bacheo**

Por otra parte, el municipio avanza con el plan de bacheo diseñado para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

Actualmente se trabaja en:

* Ecuador y La Paz
* 25 de Mayo y Santiago del Estero
* Necochea y Sargento Cabral
* Necochea e Ituzaingó
* Marcial Candioti y Gobernador Candioti

Según se informa, todas las tareas se realizan siempre en sentido del tránsito. Puede haber cortes de circulación y desvíos de colectivos a medida que avanzan las obras. Además, se detalla que los trabajos están sujetos a las condiciones climáticas.