---
category: Agenda Ciudadana
date: 2021-03-11T06:43:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/turismo2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias y agencias
resumen: Vuelve el PreViaje para después de las vacaciones de invierno
title: Vuelve el PreViaje para después de las vacaciones de invierno
entradilla: Además, habrá descuentos para turistas de la región. El programa que reintegra
  el 50% de lo comprado en turismo para el verano 2022.

---
Las autoridades nacionales junto con los sectores turístico y gastronómico ya analizan la puesta en marcha de distintas iniciativas para reactivar el sector este año de pandemia se vio afectado por la escasez de turistas.

La imposibilidad de viajar llevó a la quiebra a muchas empresas y cientos de despidos, además de una temporada veraniega que movilizó argentinos pero no fue suficiente para recuperar lo perdido por la crisis de la pandemia.

Entre diciembre y marzo, se fueron de vacaciones casi 13 millones de argentinos. Si bien el número de viajeros por el territorio nacional no es menor, no fueron suficiente los $201 mil millones que movilizaron en términos económicos para el sector, ya que se sintió la reducción de los ingresos que sufrieron los argentinos por la pandemia.

Así las cosas, el Gobierno ya definió que volverá a incentivar al sector con otro PreViaje, tal como anticipó este portal a fines de 2020. Pero no será una única iniciativa, sino que se trabaja en tres líneas diferentes.

**Previaje en temporada baja**

La más avanzada es la que se aplicará en los meses siguientes a las vacaciones de invierno: PreViaje Escapadas. 

"Aún no se definió los destinos que serán incluidos para este beneficio, pero sí que se llevará adelante para viajes cortos post invierno en determinados días y para recorrer ciertas ciudades", adelantaron desde el Ministerio de Turismo de la Nación.

**Previaje verano 2022**

Éste es un proyecto que quiere replicar el próximo año la cartera de turismo tras la enorme adhesión que tuvo en 2020, pero aún no cuenta con el presupuesto suficiente para instrumentarlo. Dependerá de los fondos que logre obtener de Nación en lo que resta del año para ver cuánto devuelven para vacacionar el año que viene, y si incluyen a toda la Argentina o sólo algunos destinos.

**Descuentos turismo regional**

Este proyecto está pensado para el último cuatrimestre de 2021 y atado siempre al avance del plan de vacunación de los países limítrofes - se exigirá pasaporte sanitario para entrar a la Argentina (es decir, certificado de las dos dosis de la vacuna contra el coronavirus aplicadas), esto ya lo resolvió el Gobierno, - y a cómo evolucione el control de la pandemia en la región.

Si la situación para la última parte del año está controlada en los países vecinos, y en la Argentina, la cartera de Turismo negocia con las diferentes aerolíneas y hoteles para otorgarles un porcentaje del valor del pasaje y del hospedaje, de esa forma, que el turista regional tenga un descuento que lo incentive a venir a visitar nuestro país.