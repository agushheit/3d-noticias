---
category: La Ciudad
date: 2021-03-31T07:20:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/jatón.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Jatón se reunió con supermercadistas para analizar esta nueva etapa del Covid-19
title: Jatón se reunió con supermercadistas para analizar esta nueva etapa del Covid-19
entradilla: El intendente mantuvo un encuentro virtual con supermercadistas para conversar
  sobre los protocolos vigentes y reforzar las acciones que protejan a los trabajadores
  y los consumidores.

---
El intendente de Santa Fe, Emilio Jatón, mantuvo este martes por la tarde una reunión virtual con representantes de la Cámara de Supermercados y Autoservicios de Santa Fe y de dos cadenas del rubro. En el encuentro se abordó la situación epidemiológica por el Covid-19 y se habló de la necesidad de reforzar los protocolos de bioseguridad vigentes.

En la ocasión, Jatón dijo que “no es momento de relajar las medidas” de cuidado y remarcó que se abre una nueva instancia de trabajo con los diversos sectores del comercio y el trabajo. “La situación en los países vecinos es complicada y, ante esa situación, creemos que la suma de las responsabilidades individuales ayuda al conjunto de la ciudadanía”, agregó.

El objetivo de este tipo de encuentros, que continuarán durante estos días, es reforzar las acciones que tiendan a la protección de los trabajadores y los consumidores frente a la pandemia de coronavirus, sin que ello genere más dificultades en la economía. “Vemos que hay un relajamiento general y debemos trabajar todos juntos para evitar el incremento de los contagios”, explicó.

Entre las medidas que se van a reforzar están las del uso de barbijo obligatorio, el distanciamiento social, la sanitización y la ventilación cruzada. Esto último, según el director de Salud municipal, César Pauloni, disminuye el riesgo de contagio y formará parte de las recomendaciones para esta etapa del año.

“Nos ponemos a disposición para trabajar en los protocolos que sean necesarios ajustar a la nueva realidad”, indicó el secretario de Producción y Desarrollo Económico, Matías Schmüth. A su vez, remarcó que es importante aprovechar la experiencia de este año de pandemia para construir decisiones que todos respeten y cumplan.

Por último, los supermercadistas agradecieron la instancia de trabajo que abrió la Municipalidad de Santa Fe y remarcaron la voluntad de conversar todos los temas que sean necesarios para fortalecer y mejorar las medidas de bioseguridad en sus locales.

**Participantes**

De forma virtual, por la Cámara de Supermercados y Autoservicios de Santa Fe y zona, participaron su presidenta María Elizabeth Raffín, Gabriel Silva, Osvaldo Fabro, Maximiliano Kilgellman, Eduardo Antoniazzi y Jorge Asner.

En esta oportunidad, también asistieron José Luis Fleita, en representación de Coto; y Marcelo Puente, por Walmart.

![](https://assets.3dnoticias.com.ar/supermercados.jpg)