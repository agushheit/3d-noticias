---
category: Estado Real
date: 2021-07-17T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/REGISTRO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Los Santafesinos gestionaron más de 75 mil trámites de partidas a través
  del sistema online del Registro Civil
title: Los Santafesinos gestionaron más de 75 mil trámites de partidas a través del
  sistema online del Registro Civil
entradilla: Se trata de un sistema que permite acceder a documentación a través de
  internet, brindando mayor rapidez, de manera cómoda y sencilla.

---
El gobierno de la provincia, a través del Sistema de Partidas Online del Registro Civil, puso en funcionamiento meses atrás un sistema online para gestionar documentaciones que permite a los interesados realizar trámites sin necesidad de asistir a las oficinas del organismo. Esto permitió que, en lo que va del año, se digitalizaran más de 75 mil trámites en todo el territorio.

Al respecto, el subsecretario de Registros de la provincia, Francisco Dallo, brindó detalles sobre los últimos datos estadísticos que se dieron a conocer desde el Registro Civil respecto a los trámites de DNI, matrimonios, divorcios, nacimientos y partidas que los santafesinos y santafesinas realizaron de manera online hasta el 30 de junio del 2021. “Desde el comienzo de la gestión, el gobernador Omar Perotti, y el secretario de Justicia, Gabriel Somaglia, nos solicitaron ponernos como objetivo la virtualidad y digitalización, la cercanía de los servicios del Registro Civil con la ciudadanía. Por ello, en diciembre del año pasado comenzamos a trabajar fuertemente en la digitalización de las partidas”.

En ese sentido, Dallo remarcó que “estamos convencidos que ese es el camino y lo demuestran los más de 75 mil trámites que se hicieron para la solicitud de partidas online. Creemos que, si bien no es un proceso inmediato, la gente aún tiene que familiarizarse con el sistema y tiene que funcionar más fluidamente”. Y agregó: “Tenemos que liberar cupos de cantidad de trámites por día, que se hará cuando contemos con más recursos humanos. Estamos en el camino del aprendizaje del ciudadano en el uso de la herramienta”.

Por último, el funcionario reconoció que “también es importante tener la Ley de Conectividad para que todos los santafesinos cuenten con una conexión adecuada, para entre otras cosas, accedan a esta herramienta que el Estado brinda para hacer sus trámites en forma más rápida, sencilla y con menos traslados”.