---
category: La Ciudad
date: 2021-11-26T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/pan.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El precio del pan aumentó entre un 10% y 12% en la ciudad de Santa Fe
title: El precio del pan aumentó entre un 10% y 12% en la ciudad de Santa Fe
entradilla: "Es ante la suba de la materia prima que se utiliza para la fabricación
  de los productos. El kilo costará entre $ 190 y $ 210.\n\n"

---
Desde este jueves el precio del pan sufrió un incremento de entre un 10% y 12%. El kilo oscila entre $ 190 y $ 210.

 Al respecto, Jorge Spasitch del Centro de Panaderos de Santa Fe, señaló que la medida se debe al significativo aumento que sufrió la materia prima que se utiliza para la fabricación de los productos.

 En los últimos meses "ha sido notable la suba en el precios de la harina en un 25%, así como también en la levadura, y la margarinas que aumentó alrededor de un 80%, siendo esto último lo que más incremento sufrió".

 Spasitch, remarcó que “dicho aumento es un valor estimativo que se acuerda según los costos que relevamos en la Sociedad de Panaderos, pero después cada industria panaderíl fija sus propios costos”.

 Cabe recordar, que se trata del tercer aumento que registra el pan en lo que va del año, equivalente entre un 38% y 40%. El último fue en el pasado mes de mayo.