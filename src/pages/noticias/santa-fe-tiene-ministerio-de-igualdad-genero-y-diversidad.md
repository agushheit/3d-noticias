---
category: Agenda Ciudadana
date: 2021-05-31T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/MINISTERIO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe tiene Ministerio de Igualdad, Género y Diversidad
title: Santa Fe tiene Ministerio de Igualdad, Género y Diversidad
entradilla: La Legislatura de la provincia aprobó el proyecto de ley de Omar Perotti
  para transformar a la Secretaría de Estado de Igualdad y Género en Ministerio.

---
La actual secretaria de Estado de Igualdad y Género de la provincia, Celia Arena expresó: “Este es solo un paso más en el camino que demuestra la voluntad política de esta gestión de hacer de esta provincia un lugar donde se aceptan las diferencias, pero no las desigualdades”.

Y agregó: “La igualdad y la lucha contra la violencia por motivos de género constituyen una política de Estado para esta gestión y así lo dejó en claro el gobernador Omar Perotti en su discurso de apertura de sesiones. Tenemos la firme convicción de abordar estos problemas desde sus causas estructurales, con políticas institucionalizadas, transversales y que tengan llegada a cada rincón de la provincia y en ese camino nos encontramos”

**Sobre el proyecto de ley**

Durante la visita de la ministra Elizabeth Gómez Alcorta el pasado martes 13 de abril el gobernador anunció su intención de enviar el proyecto de ley para que la Secretaría de Estado de Igualdad y Género se transforme en ministerio, hecho que se concretó días después.

El 29 de abril el proyecto recibió la media sanción de la Cámara de Senadores de la provincia y finalmente este jueves fue ley la creación del nuevo Ministerio de Igualdad, Género y Diversidad de la provincia de Santa Fe.

**Coronavirus: La cuarentena se despide en 18 provincias**

**PAGINA12**

La reapertura de actividades económicas y sociales ya está en marcha en todos los distritos donde la pandemia de coronavirus está controlada.

El aislamiento es reemplazado por el distanciamiento físico obligatorio. Según el Ministerio de la Producción, en 19 provincias la economía está en un 80 por ciento de su capacidad. Las únicas excepciones son el AMBA, gran parte del Chaco y zonas puntuales de Córdoba, Río Negro y Chubut.

Jugar un partido de fútbol, recorrer un shopping, ir a la peluquería y hasta juntarse con familiares y amigos es una realidad que existe en muchas partes del país y es invisible al discurso anticuarentena que se amplifica desde la Ciudad de Buenos Aires. “Hay que tener una mirada más federal de la situación”, plantea a PáginaI12 Santiago Cafiero, jefe de gabinete de ministros la Nación. “Desde que comenzó la cuarentena hemos aplicado, junto a gobernadores e intendentes de todos los partidos, una reapertura progresiva y responsable. Ahora dimos un paso muy importante que es levantar el aislamiento obligatorio en 18 provincias para reemplazarlo por el distanciamiento físico obligatorio, que va a ser la nueva normalidad hasta que se encuentre la vacuna”, afirma.

Este lunes comienza una nueva fase de la cuarentena administrada para enfrentar al coronavirus en el área metropolitana de Buenos Aires, Chaco, Gran Córdoba, algunas ciudades de Río Negro, como Roca y Bariloche, y en Trelew, Chubut. Pero en el resto de la Argentina la situación es diferente y se dispuso un proceso gradual de desconfinamiento con protocolos sanitarios. "A diferencia de lo que sucede en el Área Metropolitana de Buenos Aires, en más del 85 por ciento del país no hay circulación comunitaria del virus y fuimos avanzando en ese sentido”, afirma Cafiero. A nivel nacional, fueron liberadas 62 actividades, mientras que a nivel provincial se otorgaron permisos a más de 300.

“En el AMBA, por razones estructurales como la aglomeración, el control es más difícil y ahí se concentra el 90 por ciento de los casos. Sin embargo, no se puede condenar al resto de las provincias a estar aisladas cuando la epidemiología en esos lugares está controlada”, subraya el presidente de la Sociedad Argentina de Infectología, Omar Sued.

En lo que respecta al nivel económico “en 19 provincias la actividad está superando el 80 por ciento”, detalla Guillermo Merediz, secretario PYME del Ministerio de Producción. “En una parte importante del país, comercios, fábricas y talleres comienzan a tener una paulatina recuperación productiva, siempre siguiendo protocolos y cuidados sanitarios para evitar inconvenientes", agrega. Según un informe de la UIA realizado a fines de abril, más de la mitad de las empresas no estaban produciendo y el 63 por ciento tenía caídas en las ventas mayores al 60 por ciento. “En estos momentos, volvimos a una realidad previa a la pandemia, en donde el nivel de actividad está en un 80 por ciento”, asegura José Urtubey, vocal de la UIA.

En cuanto a la construcción, luego del derrumbe del 75,6 por ciento interanual en abril, el gobierno nacional autorizó, a pedido de los gobernadores, la reanudación de las obras en el ámbito privado y se reactivaron algunas obras públicas. “Abril fue un piso, creemos que en mayo la situación ha mejorado”, afirma Iván Szczech, presidente de la Cámara Argentina de la Construcción. “En el ámbito privado hay más de quince provincias que empezaron obras y todos los días vamos dando un paso más hacia una mayor actividad. En cuanto a la obra pública, a nivel nacional viene con mayor ritmo, en cambio en las provincias hay una restricción financiera por la falta de ingresos que hace que se aboquen más a pagar sueldos que hacer obras”.

“La situación económica no tiene que ver estrictamente con la cuarentena sino con la pandemia”, asegura Merediz. “En muchos comercios que han retomado su actividad, las ventas han sido muy bajas producto de la escasa movilidad y su reapertura no trajo necesariamente un fuerte repunte”. De acuerdo con la UIA, las empresas enfrentan una caída importante de la demanda, incluso en las actividades consideradas esenciales, donde el consumo se focaliza en ciertos productos puntuales. En el mismo sentido, la Cámara Argentina de Shoppings remarca que ya reabrieron doce grandes centros comerciales en diferentes provincias (Neuquén, Salta, Misiones, Chubut, Tucumán, Corrientes, Jujuy, San Juan, San Luis, Río Negro, Entre Ríos y Tierra del Fuego), sin embargo, las ventas son mínimas. “En las localidades y provincias donde han abierto, la concurrencia del público es muy baja debido al poco desplazamiento personal. Las ventas no llegan al 20 por ciento de lo habitual y son un paliativo en un escenario crítico”, explica Mario Nirenberg, presidente de la Cámara que nuclea a estos centros comerciales.

En el ámbito gastronómico, en tanto, los locales de todo el país abrieron sus puertas para ofrecer alimentos y bebidas, ya sea en la modalidad de delivery, “take away” o con permanencia en el local como sucede en las provincias de Catamarca, Corrientes, Entre Ríos, Formosa, Chaco, Jujuy, La Pampa, Santa Cruz, Mendoza, Santa Fe y Neuquén, estas últimas dos habilitadas el viernes. En el caso de Mendoza, por ejemplo, desde el 28 de mayo se aplica un protocolo gastronómico que fija un horario de atención de 7 a 23, grupos de hasta 6 personas y reservas anticipadas con número de DNI dependiendo el día.

“Hoy el país está partido en dos. En la mayor parte, prácticamente todos los sectores de la economía se fueron habilitando, con excepción de los que generan un alto grado de concentración de personas. En el AMBA, en cambio, ha sido más diverso, pero un sinnúmero de sectores y actividades industriales se han habilitado. No hay una regla general porque se estructuraron en función de las solicitudes de los intendentes”, afirma Merediz.

Pero no todo es economía. Los permisos también apuntan a descomprimir las consecuencias sociales del aislamiento. En este sentido, en doce provincias se habilitaron encuentros familiares y de amigos en domicilios particulares con determinados protocolos sanitarios. Es el caso de San Luis, Chubut, Corrientes, Jujuy, Salta, Mendoza, Tierra del Fuego, Catamarca, Entre Ríos, San Juan, Misiones y La Pampa. Por ejemplo, en la provincia gobernada por Sergio Ziliotto se anunció el miércoles pasado que se autoriza para los fines de semana y feriados el reencuentro social con un límite de diez personas, así como la posibilidad de viajar hacia distintas localidades del distrito.

No obstante, los protocolos de apertura exigen que los resultados acompañen, caso contrario la orden es retroceder en las medidas. Eso ocurrió, por ejemplo, en Chubut donde el gobierno se vio obligado dar marcha atrás y desautorizar los encuentros sociales en gran parte de la provincia por el aumento de casos. Lo mismo sucedió en Córdoba capital en el mes de mayo, en Villa Ocampo en la provincia de Santa Fe, en Santiago del Estero donde se aisló a toda la ciudad de Suncho Corral, en los pueblos Fraile Pintado y Calilegua en Jujuy, en Mocoretá, provincia de Corrientes, que deberá volver a fase 1, y en la ciudad de Orán en Salta que regresó a una cuarentena estricta.

Para los amantes del fútbol 5, en Salta las canchas agotaron sus reservas. La provincia también habilitó el funcionamiento de gimnasios y clubes deportivos, al igual que Corrientes y La Pampa. En tanto, Mendoza consiguió ayer la autorización del gobierno nacional. Por su parte, La Rioja dio luz verde para actividades al aire libre, así como para la apertura de canchas de paddle, tenis y campos de golf. En total son 19 las provincias que habilitaron las actividades deportivas como el running y el ciclismo. En la Ciudad de Buenos Aires se habilitará a partir del lunes desde las 20 y hasta las 8 de la mañana.

“Los resultados de todo este proceso son muy buenos --afirma Cafiero--. Comprobamos un alto grado de responsabilidad social y por eso estamos avanzando. Sin embargo, todavía no superamos el problema porque el virus sigue circulando y aún no tenemos vacuna.”