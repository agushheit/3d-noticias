---
category: Agenda Ciudadana
date: 2021-03-23T07:23:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid-peatonaljpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario UNO
resumen: Los factores que preocupan a Salud y que podrían condicionar la ferocidad
  de la segunda ola
title: Los factores que preocupan a Salud y que podrían condicionar la ferocidad de
  la segunda ola
entradilla: Los casos comienzan a crecer y encienden las alarmas en la cartera sanitaria.
  No descartan que la nueva oleada de contagios, sea similar a la primera. Inquietan
  algunos indicadores de riesgos.

---
"Hay signos de que estamos llegando a la segunda ola de coronavirus”, anticipó días atrás la Ministra de Salud de Santa Fe Sonia Martorano. La cartera sanitaria observa con preocupación no solo el crecimiento de casos diarios, sino también los indicadores de riesgos que, de permanecer en niveles altos, podrían anticipar un futuro "en problemas".

El director de la Región Santa Fe de Salud, Rodolfo Roselli, se mostró inquieto por los factores que podrían determinar la intensidad de la segunda ola. En ese sentido, habló sobre el relajamiento en cuanto a los cuidados que se observan en la ciudad capital e hizo referencia a las personas que vuelven de viaje (especialmente del exterior) y a las nuevas variantes (cepas) de Covid-19, muchos mas feroces que la anterior.

"Hay un número creciendo, progresivo, de nuevos casos confirmados", dijo el funcionario a UNO. Reconoció que si bien la suba es lenta (hoy se detectan entre 40 y 50 casos por día) hay preocupación por "una parte de la sociedad que no guarda las medidas de precaución y protección".

En esa línea, destacó "grandes aglomeraciones en la costanera santafesina, mucha gente en plazas; sin utilización de barbijos y distancia; sin ningún tipo de cuidado".

Hasta el domingo (21/03/21) la Región Salud Santa Fe (que concentra a los departamentos La Capital, Las Colonias, San Justo, Garay, sur de San Javier y San Jerónimo) acumulaba un total de 52.798 positivos desde el inicio de la pandemia. Dicha cantidad representa el 23,4 por ciento de los casos a nivel provincia (224.814).

Informó a este medio que la suba de contagios ya comienzan a impactar en el 0800-555-6768. "Estamos en más de cinco mil llamadas diarias", comentó y aclaró que el promedio de consultas diarias es de 3.166.

"Otra alarma son los viajeros y la presencia de las nuevas variantes de Coronavirus, sobre todo de Brasil. En la primera ola, observábamos a China como una preocupación y la teníamos a miles de kilómetros; pero ahora las tenemos al lado de nuestras fronteras", apuntó.

Sobre las nuevas cepas, Roselli destacó que "tienen un potencial de infección más alto porque tienen una velocidad de transmisión más alta y mayor letalidad".

Para el director de la Región Santa Fe de Salud, toda esta situación "pone un panorama de una segunda ola de mucha preocupación". Indicó además que en la zona "ya podemos estar transitando los comienzos" de ese crecimiento de contagios.

Consultados sobre si las consecuencias podrían ser similares a la primera oleada de infectados, con jornadas de casos superiores a los 350 casos, consideró: "Claro que sí. Además, estamos en un período donde comenzaron a aumentar el número de consultas por enfermedades respiratorias, tanto en niños como en adultos".

Con relación a la posibilidad de poner nuevas restricciones (que podrían afectar la actividad económica) para limitar la circulación de personas, el funcionario comentó: "Yo no sé en cuanto a ponerle límites a la actividad económica. Lo que sí creo es que se van a tener que tomar medidas, probablemente de restricción; restricción social, especialmente con los viajeros. Y continuar y acentuar las indicaciones de siempre en cuanto a reuniones sociales, familiares".