---
layout: Noticia con imagen
author: .
resumen: Moratoria en Santo Tomé
category: Agenda Ciudadana
title: "Santo Tomé: lanzan una moratoria para todos los tributos locales"
entradilla: El plan de regularización estará vigente a partir del 16 de
  noviembre y durante un plazo de 90 días, con planes de pago de hasta 36 cuotas
  y quitas de intereses de hasta el 70%.
date: 2020-11-13T17:26:12.636Z
thumbnail: https://assets.3dnoticias.com.ar/santo-tome-moratoria.jpg
---
La Municipalidad de Santo Tomé pondrá a disposición de los vecinos un Plan de Regularización Tributaria que abarcará las deudas vencidas al 30 de septiembre de 2020 de todos los derechos, tasas y contribuciones locales.

La moratoria estará vigente a partir del 16 de noviembre y durante un plazo de 90 días, con planes de pago de hasta 36 cuotas y quitas de intereses de hasta el 70%.

**SOLICITUD DE TURNOS**

Los interesados ya pueden solicitar un turno a través de la página web www.santotome.gov.ar para acercarse a la Municipalidad y optar por el plan de pago más conveniente de acuerdo a sus necesidades.

Exclusivamente para el Derecho de Registro e Inspección (DREI), los turnos se deberán solicitar telefónicamente al 4752599, o bien por WhatsApp a la línea 342 4065986, de lunes a viernes de 7.00 a 13.00.

**MEDIDA EXCEPCIONAL**

“Se trata de una medida excepcional, que hace más de 15 años no se implementaba en la ciudad, y que se fundamenta en el actual contexto de pandemia y crisis económica que ha impactado en los ingresos tanto de los ciudadanos como del propio municipio”, destacó la secretaria de Hacienda y Administración, Claudia Pascual.

“Por todo ello, desde el gobierno municipal entendimos que era necesario acercar a los vecinos un plan para que puedan regularizar su situación tributaria, con alternativas de pago que se adapten a las posibilidades de cada contribuyente”, explicó la funcionaria.

“Al mismo tiempo, apuntamos a obtener recursos indispensables para el funcionamiento del Estado municipal, que a pesar de la caída de recursos debe seguir prestando servicios, pagando sueldos y haciendo frente a sus obligaciones”, completó.

**TRIBUTOS INCLUIDOS**

* Tasa General de Inmuebles (TGI)
* Tasa de Urbanizaciones Privadas (TUP)
* Tasa de Inmuebles Rurales (TIR)
* Contribución por Mejoras
* Tasa Retributiva Servicios Sanitarios (TRSS)
* Derechos de Cementerio
* Derecho de Registro, Inspección e Higiene (DReI)
* Derechos de Edificación

**OPCIONES DE PAGO**

1) Para la Tasa General de Inmuebles, Tasa de Urbanizaciones Privadas, Tasa de Inmuebles Rurales, Tasa Retributiva de Servicios Sanitarios, Contribución por Mejoras y Cementerio:

* Contado, con quita del 70% de intereses.
* Hasta 12 cuotas, con quita del 30% de intereses por mora y financiación del 1,5 % mensual.
* De 13 a 24 cuotas, con quita del 15% de intereses por mora y financiación del 2,0% mensual
* De 24 a 36 cuotas, sin quita y con financiación del 2,0% mensual.

2) Para el Derecho de Registro e Inspección

Con las mismas modalidades de pago detalladas en el punto anterior.

Además, se establecen facilidades para abonar la deuda en concepto de multas, con reducciones de hasta el 30% y financiación en hasta 36 cuotas, dependiendo del convenio elegido.

3) Para el Derecho de Edificación

Con reducción de alícuotas de hasta el 30% y financiación en hasta 36 cuotas para la regularización de planos, dependiendo del convenio elegido.

**FORMAS DE PAGO**

Los convenios podrán abonarse de la siguiente manera:

* En efectivo
* Con tarjeta de crédito en hasta 6 cuotas (con beneficios de contado)
* Cuotas en efectivo y medios electrónicos de pago (incluyendo Link y Banelco).
* Débito automático con tarjeta de crédito, con reducción de la tasa de financiación a la mitad.