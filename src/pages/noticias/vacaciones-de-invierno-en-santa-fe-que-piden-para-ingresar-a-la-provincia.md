---
category: Agenda Ciudadana
date: 2021-07-07T08:45:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/turismo2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Vacaciones de invierno en Santa Fe: qué piden para ingresar a la provincia'
title: 'Vacaciones de invierno en Santa Fe: qué piden para ingresar a la provincia'
entradilla: Anticiparon que no se pedirán PCR negativas, y que sólo con la aplicación
  Cuidar se podrá hacer turismo en la provincia de Santa Fe.

---
Desde la secretaría de Turismo de la provincia de Santa Fe, confirmaron que habrá temporada de invierno en estas vacaciones que están pronto a llegar.

En diálogo con LT10, Alejandro Grandinetti, secretario de Turismo explicó que, a diferencia del 2020, este año habrá temporada, por lo que es un motivo para “festejar”.

Además, detalló que aquellas personas que quieran ingresar desde otras provincias, sólo van a necesitar contar con la aplicación Cuidar. De esta forma, no se pedirán PCR negativa ni ningún otro tipo de permiso para circular y hospedarse.

Por otro lado, para los santafesinos, destacó la vigencia de la Billetera Santa Fe que continúa con descuentos de hasta el 30%, alentando el turismo interno o de micro cercanía.

“Invitamos a recorrer la provincia y cuidarnos mucho” reforzó Grandinetti. Además, esperan que, en los próximos días, se evalúen ampliaciones de las actividades como fue la habilitación de la pesca recreativa el pasado fin de semana.

Con respecto a lo que tiene que hacer un santafesino cuando va a otras provincias y vuelve, aseguró que se sugiere realizar un aislamiento voluntario. “No está decretado como obligatorio, pero es una sugerencia muy recomendable, evitar contactos a la vuelta y el teletrabajo” reforzó.

Cabe recordar, que en la provincia de Santa Fe siguen vigentes restricciones en la circulación nocturna y la apertura de bares y restaurantes, a las cuales deberán adaptarse los turistas.