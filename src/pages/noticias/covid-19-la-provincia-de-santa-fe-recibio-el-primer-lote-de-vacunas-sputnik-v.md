---
category: Estado Real
date: 2020-12-29T14:33:57Z
thumbnail: https://assets.3dnoticias.com.ar/2912-vacunas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Covid -19: La provincia de Santa Fe recibió el primer lote de vacunas Sputnik
  V '
title: 'Covid -19: La provincia de Santa Fe recibió el primer lote de vacunas Sputnik
  V '
entradilla: Se trata de 12 mil dosis, que estarán destinadas a personal de salud.
  Una entrega idéntica está programada para el lunes próximo.

---
Este lunes, en el marco de la lucha contra el Covid-19, la ministra de Salud de Santa Fe, Sonia Martorano, recibió -en el Hospital Eva Perón, de la ciudad de Granadero Baigorria-, el primer lote de vacunas Sputnik V, provistas por Nación. Esta primera entrega consistió en 12 mil inoculaciones de primera dosis, en tanto que para el próximo lunes se espera una cantidad similar, todas ellas destinadas a personal de salud. «Es emocionante que, a menos de un año de la pandemia, gracias a la tecnología, la ciencia, la fuerza y la pasión, ya contemos con una vacuna», afirmó la funcionaria.

Asimismo, la titular de la cartera sanitaria brindó precisiones del operativo de vacunación: «Esta es una prueba piloto. Son pocas dosis, pero es muchísima la logística en la que hay que trabajar. Con estas 12 mil que llegan hoy estaremos afinando cada detalle del proceso, ya que en enero el país recibirá otros 4 millones de dosis, y tenemos que estar preparados».

En tanto que, sobre los destinatarios de este primer lote, detalló: «Mañana comenzamos con 21 puntos de vacunación, con unidades que tienen terapia intensiva, la primera línea, ya que se trata de personal crítico e hipercrítico. Todo el personal está expuesto, pero este es el que más ha costado cubrir cuando hemos tenido contagios. Esto ocurre en el mundo y es por eso que se comienza con este segmento. Y se continuará con servicios de emergencia y con aquellos que trabajan en los laboratorios de biología molecular Covid».

Respecto de los plazos estimados para la inmunización de la sociedad, Martorano afirmó que «en enero, febrero y marzo vamos a trabajar con la población objetivo, que son 1,2 millones de personas que integran la franja más vulnerable. Con esto vamos a poder reducir internaciones y disminuir la morbilidad y la mortalidad». Y explicó, además, que en primera instancia se dará prioridad al «personal de salud, seguridad y educación, mayores de 60 y personas de entre 18 y 59 años con comorbilidades».

«De esta manera -continuó- habremos alcanzado un 25% o un 30% de la población de la provincia, pero, para tener inmunidad necesitamos un mínimo de 60%, por lo cual, después de marzo, comenzaremos con la vacunación masiva, lo cual nos llevará seguramente hasta julio».

Finalmente, en torno del personal afectado al operativo de vacunación, la funcionaria apuntó: «Tenemos 1.500 personas vacunando y más de 4.000 voluntarios que están terminando la capacitación, entre registradores, vacunadores y preparadores de vacunas, entre otros».

Por su parte, el director del hospital Eva Perón, Jorge Kilstein, ponderó la llegada de la vacuna: «Para nosotros esto es algo histórico; en menos de un año nos vimos teniendo que enfrentar una situación fuera de lo común, con una pandemia que arrasó al mundo y, ahora, en diciembre, tenemos la vacuna. No hemos visto esto nunca antes, estamos muy contentos».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 700">DISTRIBUCIÓN</span>**

Los 21 puntos de aplicación de las vacunas Sputnik V están determinados por los efectores de salud segundo y tercer nivel con áreas de terapia intensiva. Se encuentran en las localidades de Venado Tuerto, Firmat, Rosario, Armstrong, Cañada de Gómez, Granadero Baigorria, Casilda, Villa Constitución, Rafaela, Santa Fe y Reconquista. En todas estas localidades se realizó la entrega de dosis en el transcurso de la jornada de hoy.