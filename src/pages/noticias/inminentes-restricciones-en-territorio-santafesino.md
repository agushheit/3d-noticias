---
category: Agenda Ciudadana
date: 2021-04-21T07:22:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/restricciones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario El Litoral
resumen: Inminentes restricciones en territorio santafesino
title: Inminentes restricciones en territorio santafesino
entradilla: El gobernador Perotti mantuvo este martes, junto a sus ministros Corach
  y Martorano, una videoconferencias con los expertos que lo asesoran ante la pandemia. 

---
El gobernador Omar Perotti mantuvo este martes por la tarde una reunión virtual con el equipo de epidemiólogos que habitualmente lo asesoran para definir posibles restricciones en territorio santafesino. Estuvo acompañado por el ministro de Gestión Pública, Marcos Corach y la titular de la cartera de Salud, Sonia Martorano.

Tras el encuentro, desde el gobierno provincial comunicaron que acordaron una serie de aspectos clave ante la segunda ola del coronavirus:

* Preocupación por el porcentaje de camas críticas ocupadas.
* Habrá que tomar medidas antipáticas porque no se ha cumplido con los protocolos y cuidados requeridos, por gran parte de la sociedad.
* Las clases por el momento hay que mantenerlas.
* Cancelar encuentros sociales, culturales, religiosos, sobre todo en espacios cerrados.
* Evaluar de reducir el horario de cierre de lugares de esparcimiento.
* Ir hacia el teletrabajo todo lo que se pueda.
* La segunda ola es mucho más fuerte, hay que tomar conciencia de la gravedad.

Por ende, se prevé que en las próximas horas se oficializarán nuevas medidas para contener la crítica situación. Cabe recordar que pese a la invitación presidencial, Santa Fe se mantuvo al margen del cierre más severo impuesto para AMBA desde el pasado fin de semana.  

En ese sentido, este martes el Ministerio de Salud reportó 2.050 nuevos casos de Covid-19 en la provincia de Santa Fe, la cifra más alta desde octubre de 2020. A su vez, se informaron 26 nuevos fallecidos, que elevó el acumulado a 4.367. En tanto, ascendió a 256.120 el número de contagiados desde el inicio de la pandemia.