---
category: Estado Real
date: 2021-04-24T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/FER´PEROT.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Capitales Alternas: Perotti y Fernández anunciaron obras por más de $76
  mil millones para la Provincia'
title: 'Capitales Alternas: Perotti y Fernández anunciaron obras por más de $76 mil
  millones para la Provincia'
entradilla: "“Esta forma es la que nos posibilita una Argentina más federal; construir
  una Santa Fe que acepte las diferencias, pero no las desigualdades”, aseguró el
  gobernador durante el encuentro celebrado en Rosario"

---
El gobernador de Santa Fe, Omar Perotti, y el presidente de la Nación, Alberto Fernández, encabezaron este viernes la tercera reunión de Gabinete Federal en la ciudad de Rosario, en el marco del proyecto de Capitales Alternas.

Durante el encuentro anunciaron obras para toda la provincia por más de $76 mil millones. Los proyectos incluyen obras hídricas y sanitarias; la restauración del Monumento a la Bandera, de infraestructura universitaria, reparación y repavimentación de rutas, entre otras.

“Hoy Rosario se convierte en una de las capitales alternativas de la Argentina y lo tiene muy merecido, porque más allá de no tener el título, es una ciudad que hizo mucho por la Argentina”, señaló Omar Perotti.

“La presencia del Presidente y todo su gabinete no solamente nos permite ir cumpliendo los compromisos de campaña, sino que le da contenido a la palabra federalismo”, aseguró el gobernador de la provincia durante el encuentro realizado en la sede de de Gobierno de Rosario.

Además, Perotti indicó que la iniciativa de Capitales Alternas y los proyectos anunciados “van a tener una implicancia decisiva en la estructura del país, porque empiezan a aparecer obras”. Y agregó que su objetivo es “generar arraigo y que las personas puedan desarrollarse en el lugar que nacieron”.

“Lo que hoy está ocurriendo aquí no es casualidad, es un compromiso asumido por el Presidente, que asumimos juntos, de llevar adelante un país más federal, un país que busque un proyecto de desarrollo que nos equilibre poblacionalmente y que la integre territorialmente”, resumió el gobernador santafesino.

Finalmente, agradeció al Presidente “por el esfuerzo de más federalismo y por los aportes. Lo que Santa Fe recibe lo potencia y lo devuelve multiplicado, esta es la idiosincrasia del santafesino, de trabajar, producir y aportar. Sin dudas reclamamos, pero lo hacemos con el convencimiento de que lo que recibimos lo devolvemos multiplicado. Y gracias por entender que esta forma es la que nos posibilita una Argentina más federal; construir una Santa Fe que acepte las diferencias, pero no las desigualdades”, concluyó.

**“Una Argentina más igualitaria”**

“Los rosarinos tienen muchas cosas para estar orgullosos”, aseguró el presidente Alberto Fernández. Y expresó: “Tomé la idea de sacar al Gobierno Nacional de la ciudad de Buenos Aires, involucrarlo en el territorio, porque no es lo mismo mirar las estadísticas de la provincia en un despacho de Buenos Aires que venir al territorio; así, uno entiende todo de otro modo”.

“Viví obsesionado, desde que fui jefe de Gabinete, en cómo hacíamos más igual a la Argentina, porque la Argentina es muy distinta”, dijo el Presidente, quien graficó: “Hay provincias más ricas y hay provincias más necesitadas, pero en todas viven argentinos y argentinas, y en todas debemos llevar nuestras políticas para que las posibilidades de desarrollo ocurran en cualquier lugar del país”.

“Santa Fe, esa provincia tan rica, capaz de generar tanta riqueza, capaz de aportar el 8 por ciento del producto bruto, tiene en el norte una dificultad de desarrollo y nosotros debemos prestarle atención a todo eso, porque en ese norte viven argentinos y argentinas”, señaló.

Del encuentro participaron el jefe de Gabinete, Santiago Cafiero; los ministros del Interior, Eduardo Wado de Pedro; de Transporte, Mario Meoni; de Obras Públicas, Gabriel Katopodis; y de Defensa, Agustín Rossi; de Agricultura, Luis Basterra. También estuvieron la vicegobernadora, Alejandra Rodenas; los ministros provinciales de Gestión Pública, Marcos Corach; de Gobierno, Justicia, Derechos Humanos y Diversidad, Roberto Sukerman; de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; de Producción, Ciencia y Tecnología, Daniel Costamagna; y de Seguridad, Jorge Lagna. Además participaron los intendentes de Rosario, Pablo Javkin; y de Santa Fe, Emilio Jatón; legisladores naciones y provinciales, entre otras autoridades.

**Capitales Alternas**

El Programa Capitales Alternas nomina a 24 ciudades del país para realizar allí reuniones con parte del Gabinete Nacional, autoridades locales y organizaciones de la sociedad civil, con el objetivo de federalizar la participación en la toma de decisiones y agilizar los encuentros entre funcionarios locales y nacionales.

Los distritos designados como capitales alternas son La Matanza y General Pueyrredón (Buenos Aires), Rosario (Santa Fe), Río Cuarto (Córdoba), Bariloche (Río Negro), Comodoro Rivadavia (Chubut), Concordia (Entre Ríos), Orán (Salta), Río Grande (Tierra del Fuego), General Pico (La Pampa), Guaymallén (Mendoza) y las capitales de Formosa y San Luis.

También, Oberá (Misiones), Tinogasta (Catamarca), Roque Sáenz Peña (Chaco), Goya (Corrientes), San Pedro (Jujuy), Chilecito (La Rioja), Cutral Có (Neuquén), Caucete (San Juan), Caleta Olivia (Santa Cruz), La Banda (Santiago del Estero) y Monteros (Tucumán).