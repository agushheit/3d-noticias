---
category: Agenda Ciudadana
date: 2021-12-29T06:00:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAMMNES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Lammens: "El Previaje hay que consolidarlo como una política pública"'
title: 'Lammens: "El Previaje hay que consolidarlo como una política pública"'
entradilla: "\"Los números confirman la presunción que teníamos cuando asumimos: que
  el turismo iba a ser uno de los puntales del crecimiento económico de la Argentina,
  del desarrollo de los próximos años\".\n\n"

---
El despegue del turismo, que registró picos mayores a la prepandemia en materia de volúmen de negocios y número de viajeros; un presupuesto septuplicado y la implementación del programa oficial Previaje para el sector; y la búsqueda de un liderazgo en el mercado internacional se destacan en el balance 2021 y las perspectivas de gestión para el año entrante que expuso el ministro de Turismo y Deportes, Matías Lammens, ante Télam como parte del ciclo de entrevistas que esta agencia pública de noticias está realizando a los integrantes del Gabinete Nacional.  
  
\- Télam: Este año comenzó con 15 millones de turistas en verano, 13 en invierno, más de cuatro millones en un fin de semana largo ¿Cuáles son los números para fin de año?  
  
\- Matías Lammens: Estamos terminando el año con números muy positivos, Los últimos fines de semana, tanto el de octubre como el de noviembre con valores muy por encima, no solamente de 2018 y 2019 sino de los últimos 10 años. Así que estamos muy contentos, entusiasmados con lo que va a ser la temporada. Sabemos que va a tener niveles de ocupación prácticamente del 100%. Esos logros nos llenan de orgullo, nos motivan y nos invitan a seguir trabajando. Pero además confirman la presunción que teníamos cuando asumimos: que el turismo iba a ser uno de los puntales del crecimiento económico de la Argentina, del desarrollo de los próximos años. Desde el Estado nacional hemos hecho una inversión enorme para sostener este sector, para que pueda llegar al otro lado de la orilla.  
  
\- T: Este año empezó con un presupuesto en materia de turismo que prácticamente triplicó al del anterior ¿Esa tendencia se mantiene hacia el futuro?  
  
\- ML: Vamos a terminar diciembre septuplicando el presupuesto inicial, muy apalancado en Previaje, que fue un programa extraordinario, que funcionó muy bien en todo el país, que es impresionante la repercusión positiva que ha tenido en las economías locales, sin ninguna duda, pero también en los usuarios. Más de 3 millones de argentinos y argentinas usaron ese programa, Se fueron de vacaciones con ese programa y resultó una experiencia virtuosa desde todo punto de vista: para la economía nacional porque moviliza ahorro privado, descomprime la demanda de dólar y hace más eficiente el gasto público porque el Estado Nacional de cada peso que invierte recupera 8,3.  
  
\- T: ¿Cómo acompañan ustedes el capítulo de inversiones?  
  
\- ML: Tenemos un proyecto que conlleva cambiar la lógica tradicional de promoción que tenía este Ministerio, que era promocionar los destinos (turísticos) de la Argentina. Nosotros decíamos que con eso no alcanzaba, que hacía falta una Secretaría de Desarrollo Turístico que pensara en infraestructura para el sector, en inversiones, en generar las condiciones para que los privados se animaran a invertir, que acompañara este crecimiento. Hemos hecho un programa de infraestructura pública que no existió nunca en la historia en materia de turismo: más de 3.000 millones de pesos, financiados por el Impuesto PAIS (Impuesto Para una Argentina Inclusiva y Solidaria); obras en más de 150 localidades. Vamos a enviar una ley al Congreso en marzo, que trata justamente de generar certidumbre para la inversión privada y también generar algunos beneficios.  
  
\- T: ¿Qué posibilidades de prosperar tiene este proyecto en el nuevo Congreso, sin que haya trabas?  
  
\- ML: Yo soy muy optimista. La idea es que la ley llegue al final de un largo recorrido en el que hay que hablar con todos los sectores políticos, los gobernadores, el sector privado. Este proyecto tiene que ser votado por unanimidad. Todos los argentinos nos hemos dado cuenta, en especial después de la pandemia, de la importancia que tiene el turismo para el país: para las economías regionales, como ingreso de divisas, como gran generador de empleo.

\- T: Desde la apertura al turismo internacional, el 1 de noviembre pasado, se retornó a la conectividad aérea por ahora con Estados Unidos, Europa y Latinoamérica ¿Se piensa ampliar a otras regiones?  
  
\- ML: Sí, por supuesto. Nosotros tenemos algunos mercados que consideramos estratégicos. El turismo de los países vecinos y regionales es el principal ingreso que tenemos, pero tenemos apuntados algunos otros mercados; uno de ellos es China. Nosotros hicimos un convenio antes de la pandemia con CTrip, la principal empresa de viajes de turismo de China, con más de 400 millones de usuarios. Tenemos un plan de incentivo para la llegada del turismo chino, de promoción (de la Argentina) en China. Estamos trabajando con aerolíneas para tener código compartido con algunas empresas de aviación de China que puedan venir directamente a la Argentina.  
  
\- T: Se había comenzado a trabajar con países vecinos para facilitar recorridos regionales a los turistas de mercados exóticos ¿Se retomaron esas acciones?  
  
\- ML: Sí, la semana pasada tuve una reunión con mi par de Uruguay, para trabajar en promoción conjunta sobre todo en los destinos de larga distancia. Luego con una aerolínea peruana, y les decía que para esos viajes, de China por ejemplo, hay un producto que es Machu Picchu, un producto que es Cataratas y un producto que es Calafate, Ushuaia y la Antártida. Sin ninguna duda hay una oportunidad de trabajar en forma conjunta en la región.  
  
\- T: Desde la perspectiva sanitaria, ¿qué se debe tener en cuenta para los viajes?  
  
\- ML: En principio, la ministra (de Salud, Carla) Vizzotti, a través de una medida administrativa, confirmó el pase sanitario, que, lejos de ser un impedimento, un requisito más, es un gran cuidado de todos. Hace que más del 80% y pico de la población que está vacunada pueda hacer las acciones que estaba acostumbrada a hacer y que aquellos que todavía no se vacunaron se tengan que vacunar.

\- T: Mucha gente hace turismo interno al no poder viajar al exterior por la pandemia, la devaluación, la limitación de financiar gastos con tarjeta Si se acaba la pandemia y mejora la economía, teniendo en cuenta el histórico déficit en el balance de turismo internacional, ¿se puede mantener la actual tendencia al turismo interno?  
  
\- ML: Ese es el gran objetivo, que se logra no impidiendo que los argentinos salgan sino que vengan más turistas. En eso estamos trabajando y tenemos resultados extraordinarios. En términos de demanda, algunos números nos sorprenden hasta a nosotros mismos. Antes, los principales países de los que venían eran los limítrofes; hoy, tercero está Estados Unidos, y eso habla de la demanda que tiene Argentina en el Hemisferio Norte. Nosotros tenemos un plan de desarrollo en Europa, específicamente en Italia y España, pero Argentina tiene que trabajar en un plan de largo aliento, que incluya formación, capacitación. El año que viene vamos a tener un plan muy ambicioso en ese sentido, de formación en idiomas, en servicios, para cada uno de los sectores de la cadena turística.  
  
\- T: Sobre las tarjetas, usted dijo que se buscaría que la gente no se sintiera perjudicada por no poder financiar sus gastos en el exterior.  
  
\- ML: Lo primero a señalar es que se trata de una decisión que obedece a una situación económica y a la decisión del Gobierno y la responsabilidad de cuidar los dólares que tenemos en el Banco Central. No es una medida intempestiva, irracional; tiene que ver con cuidar los dólares que necesitamos para crecer como país; que necesita la industria para proveerse de insumos, que necesitan las pymes para poder seguir creciendo.  
  
Entendemos que una industria como el turismo se puede ver perjudicada porque para tener buena conectividad y muchos asientos necesitamos vuelos de ida y vuelta, con gente que vaya y que venga, así que estamos trabajando. Ya hubo reuniones entre las cámaras y el Banco Central, así que creo que vamos a llegar a una solución.  
  
**Las previsiones sobre Previaje quedaron cortas**

El ministro Matías Lammens aseguró que las previsiones más optimistas en torno al Previaje quedaron cortas este año, luego de que ese plan septuplicara lo facturado en su primera edición y, por estos días, tenga la perspectiva de llegar a los 75.000 millones de pesos en los últimos días de 2021.  
  
\- T: Después que Previaje sextuplicara los ingresos de 2020 a principios de diciembre, 60.000 millones de pesos contra 10.000 millones, usted dijo que se podría septuplicar antes de fin de año. ¿Se logró?  
  
\-ML: Ya estamos. Llegamos a 70.000 millones. Como le comenté al Presidente (Alberto Fernández), todas las estimaciones del Previaje quedan cortas, por suerte. Creo que vamos a estar arriba de los 75.000 millones de pesos para fin de año. Es un éxito contundente, rotundo, porque es un gran incentivo de la demanda, que moviliza ahorro privado también, entonces el Estado recauda en las dos instancias: en la primera compra que hace el turista y vuelve a hacerlo con la tarjeta que nosotros le damos.  
  
\- T: ¿Y en lo social?  
  
\- ML: Saliendo de la cuestión presupuestaria, la cuestión material, a mucha gente le posibilitó irse de vacaciones, gente que no pensaba irse, que tenía dudas o no llegaba con el dinero, esta fue la herramienta que le posibilitó decir "bueno, me tomo unos días con la familia, con mis amigos…".  
  
Y de esos tres millones de personas que los usaron, 300.000 son beneficiarios de Pami. Porque los adultos mayores fueron quienes peor la pasaron, quienes más cuidados tuvieron que tener, más tuvieron que estar guardados.  
  
El plan funcionó de una manera extraordinaria para todos los sectores sociales, para todos los sectores económicos.  
  
\- T: ¿Cuál fue el impacto del Previaje en lo laboral?  
  
\- ML: Es un programa que genera empleo. Estamos viendo los indicadores de noviembre de la recuperación de empleo en localidades turísticas y hay algunas que están en 1,5 en términos de ocupación y tiene que ver, sin ninguna duda, con el Previaje. Eso explica el crecimiento del presupuesto en turismo y explica también la visión estratégica que tenemos del turismo como uno de los sectores de recuperación importante para la Argentina.  
  
El turismo tiene una enorme posibilidad de generación de divisas, una gran capacidad de recuperación del empleo, rápidamente, ya lo estamos viendo en estos números que le comentaba. Así que hacemos inversiones, desde lo público, generando las condiciones para que el sector privado también invierta. Tenemos la convicción de que necesitamos que el sector privado acompañe el crecimiento de la Argentina; no alcanza sólo con el Estado presente.  
  
**TRES OBJETIVOS**

\- T: ¿Puede mencionar tres objetivos de su gestión para 2022?  
  
\- ML: Te diría tres y tres, en Turismo y en Deportes. En Turismo nuestra principal obsesión es el turismo receptivo, poder tener una buena situación sanitaria mundial, para definitivamente consolidar a Argentina como un líder mundial. Estamos haciendo todo lo posible para que Argentina, que ya es líder regional, se convierta en uno de los líderes mundiales, para que la recuperación de los 7 millones de turistas que vinieron en 2019 se vea rápido. La Organización Mundial del Turismo estima que va a ser para 2025, nosotros creemos que en 2023 podemos estar alcanzando los niveles que teníamos en 2019.  
  
Consolidar lo que está pasando con el turismo interno es otro gran desafío para el año que viene. Pese a la pandemia este ha sido un gran año en esta materia. Hemos tenido muy buenos niveles de ocupación, con el estímulo del Previaje, y eso hay que consolidarlo, hay que dejarlo como una política pública, de Estado.  
  
Y el tercero es seguir trabajando el turismo social. Hemos hecho una obra maravillosa en Chapadmalal y Embalse, que son los dos grandes complejos de turismo social de la Argentina, donde pasaron millones de argentinos, y los estamos recuperando en forma total, como estaban 70 años antes. Eso no había pasado nunca en la historia de Argentina desde el primer gobierno de Perón, y lo estamos poniendo en valor, a los nueve hoteles de Chapadmalal, los siete de Embalse, con una inversión que es histórica. El plan de turismo social empieza a incluir la cooperación con otras provincias a través del Ministerio, para que la gente pueda ir a otros destinos que quiera prácticamente a costo cero.  
  
\- T: ¿Y en Deportes?  
  
\- ML: En Deportes, seguir consolidando la federalización del deporte, y mantener una impronta muy fuerte con los clubes. Y en términos de alto rendimiento, recuperar el financiamiento del Enard, del 1% que tenía por ley antes del gobierno de Macri, que se quitó en 2017 y 2018, que es fundamental.

El área Deportes de la cartera a cargo de Matías Lammens tuvo un gran logro en 2021 en lo social, con el Plan Clubes en Obras, que llegó a más de 3.000 de estas instituciones, mientras se preparan importantes eventos de turismo deportivo como atractivo internacional y local, según lo anunciado a Télam por el ministro.  
  
\- T: ¿Cuál es el balance de las acciones para los clubes deportivos de provincias?  
  
\- ML: El Plan Clubes en Obras fue fabuloso, con una capilaridad extraordinaria: más de 3.000 clubes en todos el país que pudieron hacer obras, mejorar las instalaciones para que vayan chicos y chicas. Es un plan por el cual me siento orgulloso. Tiene menos marketing que el Previaje, que fue utilizado por 3 millones de argentinos y tuvo un impacto económico más fuerte, pero fue la mayor inversión del Estado nacional en clubes.  
  
Nunca existió una inversión igual, y en obras que son palpables, como en Cafayate, Salta, donde estuve en un club en el medio de la montaña que habían hecho los vestuarios con Clubes en Obras. Estuve Las Termas de Río Hondo, Santiago del Estero, en un club que también hizo los vestuarios y ahora están poniendo las luces, a partir de ese programa oficial. Y eso redunda en mejor calidad, en que los chicos estén más tiempo dentro del club y menos en la calle.  
  
Este mes terminamos de confirmar, con el Fonplata, financiamiento internacional, una inversión de 25 millones de dólares, la mayor de la historia, en centros de alto rendimiento deportivo en el interior del país. Federalizar el deporte, llegar a cada una de las regiones con centros de alto rendimiento deportivo.  
  
Con la Universidad de San Martín hicimos un relevamiento de la práctica de deportes en la Argentina; no había estudios, censos, estadísticas de cuánto deporte se hace, qué deportes, dónde, cuántas veces por semana. Es la primera vez que el Estado nacional tiene estadística, que es importante para hacer política pública.  
  
Vemos que no estábamos equivocados cuando decíamos que los clubes son el corazón de la política deportiva de la Argentina; hay que seguir apostando ahí; hay que hacer un gran trabajo con el Ministerio de Salud, que es otro de los objetivos para el año que viene, llegar a cada uno de esos clubes, esas provincias y tener la posibilidad de que todos los chicos y chicas argentinas se hagan todos los chequeos que hagan falta. En términos de medicina preventiva es una inversión que hace el Estado nacional, un ahorro a largo plazo.  
  
\- T: ¿En cuanto a las actividades deportivas como atractivo?  
  
\- ML: Es muy importante. Turismo y deporte tienen una sinergia natural, y Argentina es uno de los grandes organizadores de esos eventos, en la región. Tiene todas las condiciones para serlo, en términos de infraestructura, en términos deportivos también. Argentina, en todos los deportes de equipo, es potencia. Tenemos todas las condiciones para consolidarnos como uno de los líderes en eventos deportivos, que son grandes generadores de turismo.  
  
El año que viene tenemos los juegos Odesur en Rosario, en abril; los World Skate Games, al que vienen más de 20.000 competidores, en Buenos Aires y en San Juan; el Moto GP, en Santiago del Estero, que tracciona gente de todas partes del mundo, sobre todo de Brasil, de Paraguay, de Chile, que vienen y se moviliza no solamente Santiago del Estero y Las Termas de Río Hondo, sino que el impacto llega hasta Salta, Jujuy, muchos lugares del interior del país que se quedan sin plazas por la gente que viene al Moto GP.  
  
Ahora en febrero vamos a tener en la Ciudad de Buenos Aires un ATP 250, un torneo de tenis de los más importantes del mundo; van a jugar dos top 10, va a jugar el Peque Schwartzman, es probable que Del Potro también; los ojos del mundo tenístico y del mundo en general van a estar puestos en la Ciudad de Buenos Aires, que es lo que generan los grandes eventos deportivos.