---
category: Estado Real
date: 2021-02-01T06:03:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/sain.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia dictará un curso sobre Seguridad Pública para municipios y comunas
title: La provincia dictará un curso sobre Seguridad Pública para municipios y comunas
entradilla: El mismo estará a cargo del ministro de Seguridad, Marcelo Sain, y estará
  dirigido a funcionarios de los municipios y comunas que integran la Mesa de Coordinación
  en Seguridad Local.

---
El gobierno de Santa Fe, a través del Ministerio de Seguridad, iniciará este lunes una capacitación sobre Seguridad Pública, dirigida a funcionarios de los municipios y comunas que integran la Mesa de Coordinación en Seguridad Local. La misma será virtual y constará de diez encuentros, con un total de 20 horas de cursado.

Al respecto, el subsecretario de Seguridad Local y Abordajes territoriales, Joaquin Chiavazza, precisó que “esta será la primera capacitación que se realice en la provincia, tanto el gobernador Omar Perotti, como el ministro Sain, nos solicitaron trabajar junto a todos los municipios, porque acá no importan los colores políticos, sino trabajar para todos los ciudadanos de la provincia. Ya tenemos inscriptos más de 50 funcionarios de municipios de toda la provincia, y estamos muy entusiamados”, dijo.

La capacitación apunta a que cada punto del territorio provincial conozca sobre la realidad actual de la Seguridad en Santa Fe, la modernización integral que se llevará a cabo en la Policía provincial y en el sistema de control policial, la coordinación con gobiernos locales, la prevención del delito, las bandas criminales y el análisis sobre las principales problemáticas que hoy afectan la seguridad de la provincia.

**PROGRAMA**

Apertura este lunes 1 de febrero a cargo del Ministro de Seguridad, Marcelo Sain.

**_Bloque 1: El sistema de seguridad pública_**

_Unidad 1:_ La modernización del sistema de seguridad pública de Santa Fe Contenidos: Diagnóstico, objetivos y alcance de los proyectos de reforma del sistema de seguridad y policial en Santa Fe. Proyecto de Ley de Seguridad Pública; Proyecto de Ley del Sistema Policial; Proyecto de Ley del Control del sistema policial.

_Unidad 2:_ Las problemáticas de la seguridad pública Contenidos: La inseguridad como problema público. La producción de información en el Observatorio de Seguridad Pública de la Provincia de Santa Fe. Inseguridad objetiva y subjetiva. Delitos, violencias, conflictos. Las tendencias y evolución de la criminalidad común en Argentina y en Santa Fe. Nuevas formas de marginalidad urbana, violencia y seguridad.

_Unidad 3:_ Mercados ilegales y criminalidad organizada. Contenidos: El narcotráfico como modalidad del crimen organizado. Conceptualización y marco normativo. Tráfico internacional de drogas y mercados internos. ¿Vacíos de Estado o Estado ilegal? Orden clandestino y Estado paralelo en Argentina y Santa Fe.

_Unidad 4:_ Producción, gestión y análisis de la información criminal Contenidos: Procedimientos de producción, gestión y análisis de la información. Técnicas de identificación de fuentes, recolección, procesamiento y análisis de datos y producción de informes. Herramientas informáticas y manejo de software para análisis criminal.

_Unidad 5:_ Políticas y gobierno de la seguridad Contenidos: Las políticas de seguridad: conceptualización. Dilemas políticos y nuevas culturas del control del delito. El gobierno de la seguridad pública e institucionalidad política en Argentina. Desgobierno político, delegación y autogobierno policial. Marco constitucional, legal y gubernamental de la seguridad pública en la República Argentina y en la Provincia de Santa Fe.

_Unidad 6:_ Policía Contenidos: La policía como institución y la actividad policial. Funciones y tareas policiales. Análisis de la policía desde la teoría organizacional. Modelo tradicional de policía: su crisis y colapso. Reforma y cambio institucional en las policías: nuevo profesionalismo policial y nuevas formas de organización y funcionamiento policial.

**_Bloque 2: El abordaje local de la seguridad_**

_Unidad 7:_ Seguridad y gobiernos locales Contenidos: La inseguridad en la agenda social y política local. Las nuevas tendencias en materia de descentralización y desconcentración territorial de la seguridad. El rol de Municipios y Comunas en la gestión de la seguridad: atributos y limitaciones. Violencias y conflictividades locales. Las etapas de la construcción de una estrategia de seguridad local.

_Unidad 8:_ Prevención local del delito Contenidos: La prevención del delito: historia y conceptualizaciones. Enfoque epidemiológico: factores de riesgo y protección. Estrategias de prevención del delito: enfoque situacional y enfoque social. Políticas sociales y políticas de prevención social del delito. Modalidades de participación comunitaria en seguridad. Intervenciones locales en prevención del delito, estándares internacionales y buenas prácticas.

_Unidad 9:_ La cooperación y coordinación interinstitucional local provincial Contenidos: La creación de las Mesas de Coordinación Institucional en Seguridad Local. Objetivos, funcionamiento y alcance. Intervenciones basadas en evidencia. La Unidad de Análisis Local. La relación Policía - Municipio. Evaluación del desempeño policial. Experiencias internacionales a escala local.