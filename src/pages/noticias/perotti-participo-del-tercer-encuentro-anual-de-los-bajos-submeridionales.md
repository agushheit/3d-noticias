---
category: Estado Real
date: 2021-03-27T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/perotti.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti participó del tercer encuentro anual de los Bajos Submeridionales
title: Perotti participó del tercer encuentro anual de los Bajos Submeridionales
entradilla: "“Estas obras buscan lo que interpretamos del federalismo: que logre el
  arraigo de nuestra gente”, afirmó el gobernador en Santiago del Estero."

---
El gobernador de Santa Fe, Omar Perotti, participó este viernes en la ciudad de Santiago del Estero, del Tercer Encuentro Anual del comité interprovincial por los Bajos Submeridionales, integrado por las provincias de Chaco, Santa Fe y la provincia anfitriona, con el objetivo de abordar una serie de obras en esa región.

La reunión, que se realizó en el Centro de Convenciones Fórum de la capital santiagueña, fue encabezada por el ministro del Interior Eduardo “Wado” de Pedro, su par de Obras Públicas de la Nación, Gabriel Katopodis, y el secretario general de Consejo Federal de Inversiones (CFI), Ignacio Lamothe. También, participaron el gobernador Gerardo Zamora y, de manera virtual, el gobernador chaqueño, Jorge Capitanich.

En la oportunidad el mandatario santafesino resaltó “el objetivo común que es poner en marcha una región de alto interés para nuestras provincias”, y en ese sentido destacó que “no solamente vamos a tener una buena señal para Chaco, Santiago del Estero y Santa Fe, sino para toda la Argentina”.

“Es una señal –continuó – no solamente por lo que podemos llegar a recuperar desde hacer sustentable ambientalmente esa región, sino además en la incorporación productiva en el resguardo para los miles de hectáreas que tenemos hoy desaprovechadas o mínimamente utilizadas”.

Luego, Perotti se refirió a la necesidad de hacer estudios de alto impacto climático en esa región: “Nos ha motivado a los tres gobernadores ver una problemática profunda, con necesidad de cambios estructurales que no se resuelven fácilmente y menos si nos toca un año de pandemia, por eso hay que empezar”.

Además, remarcó que “esa decisión claramente la ha interpretado el gobierno nacional en acompañarnos y el presidente de la Nación en llevar adelante el planteo de un país mucho más federal. Estas obras claramente buscan lo que deseamos e interpretamos del federalismo: el arraigo de nuestra gente. Toda esa zona se ha vaciado, se ha trasladado a los grandes centros de nuestra provincia”, graficó.

“Por eso –siguió-, estamos plenamente convencidos de que cada obra que hagamos en nuestro norte, cada obra que se avance y permita un mejor aprovechamiento de los Bajos de manera sustentable, no solamente nos va a arraigar a nuestra gente en el norte, es una muy buena noticia para los centros más poblados de nuestra provincia y del país”, finalizó el gobernador.

**Pensar en el Federalismo**

A su turno, el ministro de Obras Públicas de la Nación, Gabriel Katopodis, expresó que “esta reunión marca la seriedad, responsabilidad y rigurosidad con la que hemos encarado este tema. Fue en esta provincia donde iniciamos este proceso y donde retomamos el camino, proceso que acumula muchas reuniones de trabajo y definiciones importantes, que hoy pretendemos consolidar para tener una agenda que contemple los proyectos, objetivos e inversiones”.

“De esta manera, podemos pensar el federalismo en la Argentina desde otro lugar, donde cada uno de estos ámbitos interjurisdiccionales, con problemáticas específicas y concretas, también sean parte de la agenda federal que estamos construyendo”, concluyó Katopodis.

Por su parte, el ministro del Interior, Eduardo “Wado” de Pedro, felicitó a los equipos técnicos “por el trabajo, el buen ritmo que traemos, la constancia y dedicación. Sabíamos que cuando diseñábamos la solución de los problemas hídricos vinculados a esta región entendíamos también que los antecedentes no eran muy buenos, porque es un problema que lleva muchísimos años sin solución, y la verdad que por decisión de los gobernadores y del Presidente de la Nación, el Ministerio de Obras Públicas tiene entre sus principales prioridades la solución de este problema”.

“Resolver esto va a generar mayor productividad, mayor empleo, va a dinamizar la economía de tres provincias, y ese el objetivo central de nuestro gobierno. Hay una decisión y una voluntad de terminar, de una vez por todas, con estos problemas de infraestructura y generar empleo”, sostuvo.

Seguidamente, el secretario general de Consejo Federal de Inversiones (CFI), Ignacio Lamothe, comentó lo ocurrido en los últimos 30 días, y dijo que “en aquella oportunidad tomamos conocimiento de cómo eran los términos de referencias que se venían trabajando dentro del Comité, y conformamos un equipo dentro del CFI, con ingenieros hidráulicos, civiles y agrónomos para perfeccionar este trabajo y agregar algunos componentes que faltaban”.

“Sumamos 4 anexos a esos términos de referencia que esta semana fueron aprobados”, dijo Lamothe, quien destacó “la velocidad con la que trabajamos y la celeridad con la que se entendieron todas las partes”.

**Presentes**

Cabe señalar que del encuentro también participaron el secretario de Infraestructura y Política Hídrica, Carlos Rodríguez; la secretaria de Provincias, Silvina Batakis; el subsecretario de Políticas para el Desarrollo con Equidad Regional, Martín Pollera; el secretario titular del Instituto Nacional del Agua, Carlos Bertoni; integrantes del Comité Interjurisdiccional, funcionarios nacionales y provinciales, entre otros.

Además, acompañaron al gobernador Perotti; la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; la secretaria de Gestión Federal, Candelaria González del Pino; el secretario de Recursos Hídricos, Roberto Gioria; de Empresas y Servicios Públicos, Carlos Maina, y de desarrollo Territorial y Arraigo, Fabricio Medina; el subsecretario de planificación y gestión de la Secretaría de Recursos Hídricos, Carlos Scioli; y el director Provincial de Vialidad, Oscar Ceschi; y el funcionario de Vialidad Daniel Ricotti.

**Plan de Obras**

El objetivo es mejorar el drenaje de las aguas en épocas de grandes lluvias, que provocan inundaciones en la región de los Bajos, afectando la producción agropecuaria y anegando las ciudades de la zona. El proyecto prevé una inversión total cercana a los $ 6.000 millones (entre fondos nacionales y provinciales). Cabe recordar que el año pasado se suscribió el convenio marco en la provincia de Chaco que habilitó una serie de obras que permitió, posteriormente, que los equipos técnicos avanzaran en los proyectos ejecutivos.

Al respecto, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, afirmó que “esta reunión manifiesta la voluntad de que este proyecto se concrete. Esto tiene contenido y continuidad. Ya tenemos previstas reuniones de trabajo bilaterales, recordemos que la provincia de Santa Fe está aguas abajo, con lo cual recibe aguas tanto de Santiago del Estero como de Chaco. Las reuniones bilaterales son para acordar aquellas obras que beneficien a las dos provincias. Esto tiene que hacerse de forma inmediata”.

Por último, la secretaria de Gestión Federal, Candelaria González del Pino, afirmó que “hace más de un año se tomó la decisión política de avanzar en las obras de infraestructura fundamentales y necesarias para el área de Bajos Submeridionales. En este tercer encuentro los equipos trabajaron sobre obras que no presentan ningún tipo de conflicto y que pueden avanzarse directamente. Todo esto con un presupuesto de $2.100 millones que se van a comenzar a ejecutar a la brevedad. El CFI acompaña este proceso en el diseño del plan director”.

**Manejo Coordinado y racional de los recursos hídricos**

El primer encuentro entre las tres provincias y la Nación se concretó el 5 de febrero de 2020, en Resistencia, Chaco, donde se firmó un convenio para avanzar en las acciones que articulen recursos y capacidades destinadas a la solución de los problemas estructurales que afectan a las tres provincias que integran la cuenca.

En esa oportunidad, se avanzó en el diseño, planificación y ejecución de un Plan Director de Intervenciones, a ejecutar por parte del Consejo de Gobierno del Comité Interjurisdiccional de la Región Hídrica de los Bajos Submeridionales (CIRHBAS).

El segundo encuentro se realizó el 19 de febrero de este año en la Casa de Santa Fe, en la Ciudad Autónoma de Buenos Aires (CABA), donde se planteó una agenda de trabajo con plazos concretos y esquemas de financiación. También se establecieron pautas a seguir y se definieron tres líneas principales: de las obras a encarar de manera inmediata; el Plan Director de los Bajos Submeridionales, que refiere a la planificación de todas las acciones a tomar en el sistema hídrico; y una tercera que consiste en elaborar los proyectos de las obras necesarias a realizar en el corto y mediano plazo.