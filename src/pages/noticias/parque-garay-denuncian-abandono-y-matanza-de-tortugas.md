---
category: La Ciudad
date: 2022-01-25T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/parque.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Parque Garay: denuncian abandono y matanza de tortugas'
title: 'Parque Garay: denuncian abandono y matanza de tortugas'
entradilla: 'Desde la vecinal Parque Juan de Garay se mostraron preocupados e indignados
  con la falta de atención que presenta el pulmón verde. Lagos con agua verde y sucia,
  falta de iluminación y ataque a los animales.

  '

---
El Parque Garay presenta por estos días un paisaje desolador, desprolijo y triste. Desde la vecinal denunciaron en las últimas horas el abandono y la falta de mantenimiento por parte del municipio local, observando los lagos con agua verde y sucia, iluminación nula y hasta la matanza de tortugas de agua.

El Parque Juan de Garay, junto al Parque Federal y al Parque del Sur, son los pulmones verdes más importantes de la ciudad y más visitados para la recreación por los santafesinos. Justamente el Parque Garay, "hoy presenta un grado de abandono tan notorio que nos entristece a todos los vecinos", indicaron desde la vecinal que lleva el nombre del espacio verde.

"Existe una falta de compromiso muy grande de la municipalidad para el mantenimiento diario y la puesta en valor del parque Garay. En las últimas horas realicé una recorrida por la zona y es muy triste el estado de abandono. El polideportivo está totalmente destruido y saqueado por los delincuentes en el área de los baños. Hoy es una estructura oscura, sin mantenimiento y peligrosa que se convierte en un lugar propicio para que ocurran episodios de inseguridad", denunció, Graciela Pieroni, integrante de la vecinal Parque Garay.

"Vemos que el arbolado del parque no es mantenido y con las últimas tormentas muchísimas ramas cayeron en distintas zonas del parque. La gestión municipal está al tanto de nuestros reclamos pero evidentemente no acciona al respecto. La pandemia está pasando y ya se terminaron los tiempos de poner excusas", sostuvo Pieroni y continuó agregando: "A mí me han mirado a la cara funcionarios de este gobierno municipal, hace ya un año, y me prometieron la puesta en valor del Parque Garay; todavía estamos esperando. Uno quiere creer que algo están haciendo, pero ya pasaron dos largos años en los cuales las obras de mantenimiento no aparecieron. No estamos hablando de una plaza, estamos hablando de uno de los pulmones verdes más importantes de la ciudad".

En el mes de septiembre del año 2021, desde el municipio anunciaban el comienzo de obras de saneamiento y limpieza de los lagos del Parque Garay. La Municipalidad ubicó en ese momento un camión desobstructor que comenzó con la limpieza de los lagos. "Vimos gente trabajando en la limpieza de los lagos, pero la verdad es que fue simplemente un anuncio para la foto, porque como vecinos y vecinalistas sabemos muy bien que significa y cuales son los trabajos de saneamiento de los lagos y piletones, cosa que no pasó. Acá no se vaciaron los lagos ni se limpiaron los fondos, solamente en una semana realizaron una limpieza superficial. Además, no sabemos si las bombas funcionan o no, porque el agua se observa muy verde y sucia. No estamos frente a un mantenimiento que sea serio y que merezca el parque", subrayó la integrante de la vecinal.

"Otro tema que preocupa a los vecinos, es la falta total de iluminación, además de cables rotos y colgando de columnas con todo el riesgo que eso implica para los vecinos y usuarios del parque Garay. Y lo más triste de todo, que genera una indignación muy grande de todos los vecinos, es la matanza de tortugas que se registró en los últimos días, donde encontramos varios animales muertos con todo su caparazón destrozado", finalizó la referente vecinalista del Parque Garay, Graciela Pieroni.