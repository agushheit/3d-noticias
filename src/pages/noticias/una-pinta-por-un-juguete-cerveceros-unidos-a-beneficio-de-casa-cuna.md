---
category: La Ciudad
date: 2020-12-19T11:14:59.000+00:00
thumbnail: https://assets.3dnoticias.com.ar/1912-pinta.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: "«Una pinta por un juguete»: cerveceros unidos a beneficio de Casa Cuna"
title: "«Una pinta por un juguete»: cerveceros unidos a beneficio de Casa Cuna"
entradilla: En vísperas de Navidad, la propuesta invita a llevar un juguete en buenas
  condiciones o realizar una donación monetaria a cambio de una pinta de cerveza elaborada
  por cerveceros artesanales santafesinos.

---
Este domingo se desarrollará “Una pinta por un juguete”, a beneficio de Casa Cuna en la zona del Faro de la Costanera Oeste. Ante la llegada de la Navidad, se invita a ser parte de este evento que propone la entrega de un juguete en buen estado o una suma de dinero simbólica a depositar en una urna a cambio de una pinta de cerveza artesanal elaborada por productores locales.

***

<span style="color: #EB0202;">**El evento se realizará este domingo 20 de diciembre de 17 a 23 horas en la zona del Faro de la Costanera Oeste.**</span>

***

El evento surge como una iniciativa de un grupo de cerveceros caseros de la ciudad que cocinaron tres estilos diferentes de cervezas que van a estar en canillas el domingo. También se sumaron a la causa otras cervecerías locales como Comillas, Okcidenta, Palo & Hueso, Cuántica y Medieval.

También habrá un stand dedicado exclusivamente a la fabricación de cerveza artesanal casera, donde se brindará información del proceso, uso de materias primas e incluso se podrá dejar los datos para una serie de capacitaciones gratuitas que se llevarán a cabo durante el verano.

Para asistir al evento  **será obligatorio el uso de barbijo o cubrebocas y se recomienda respetar el distanciamiento social y los protocolos vigentes**  en el marco de la pandemia por Covid-19.

La propuesta cuenta con el apoyo de la Municipalidad de Santa Fe, la Asociación Civil de Cerveceros Santafesinos y la Cámara de Cerveceros Artesanales de Santa Fe.

Para más información se puede consultar el Instagram [@cervezasolidariasantafe](https://www.instagram.com/cervezasolidariasantafe/ "Cerveza Solidaria Santa Fe").