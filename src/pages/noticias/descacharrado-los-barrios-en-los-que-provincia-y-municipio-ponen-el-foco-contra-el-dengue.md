---
category: La Ciudad
date: 2021-11-05T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/DENGUE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Descacharrado: los barrios en los que provincia y municipio ponen el foco
  contra el dengue'
title: 'Descacharrado: los barrios en los que provincia y municipio ponen el foco
  contra el dengue'
entradilla: 'La provincia inició la campaña de prevención en las zonas que han tenido
  el mayor número de casos de dengue. Advierten que el mosquito transmisor está en
  toda la ciudad.

'

---
De cara a una nueva temporada de mosquitos en la ciudad de Santa Fe, la municipalidad y la provincia profundizan las políticas sanitarias para prevenir el dengue en la ciudad capital a través del descacharrado domiciliario.

El subsecretario de Salud municipal, Juan Picatto explicó que a través de las Asociaciones de Higiene Urbano de la ciudad van a comenzar las acciones de descacharrado en los barrios.

Informó que "el cronograma empieza por el barrio San Pantaleón y luego sigue por Alto Verde. La presencia del mosquito Aedes Aegypti está en toda la ciudad de Santa Fe. Nosotros tenemos un sistema de monitoreo durante todo el año con la presencia del vector y tenemos presencia del mismo con los primeros calores; sobre todo a partir del mes de agosto. Pero son los movimientos normales y típicos del año".

Por su parte, el directo de la región de Salud Santa Fe, Rodolfo Roselli, señaló que "desde mediados de octubre que comenzamos nuevamente con la actividad territorial. “Se realizan tareas interministeriales junto a Desarrollo Social, con quienes inicialmente trabajamos en el barrio Pompeya y ahora estamos en San Martín", indicó.

Añadió que van a comenzar "con los barrios que han tenido el mayor número de casos de dengue en el 2.019 y 2.020. Comenzamos con Pompeya, son 52 manzanas, se visitó casa por casa para realizar las actividades preventivas de descacharrado".

Explicó que "estamos en la época del año ideal para el desarrollo del mosquito. Altas temperaturas, lluvias intermitentes y 12 horas de luz. Son condiciones óptimas para que los huevos a fines de marzo y abril comiencen a desarrollarse.

"En estos momentos el mosquitos (Aedes Aegypti) no está contaminado con el virus pero es el momento oportuno para las acciones preventivas", destacó.

Por su parte, Mariana Maglianese, responsable de Control de Vectores de la Región Santa Fe, comentó que en barrio Pompeya “terminamos hace 10 días. Allí encontramos que el año pasado había un 88% de viviendas de bajo riesgo y el 12% restante de mediano y alto riesgo para crecimiento del mosquito que transmite el dengue. Asimismo, en estos días obtuvimos que el 95% de las viviendas es de bajo riesgo y sólo el 5% de mediano y alto. Disminuimos ese potencial a cifras bajas por el descacharrado”.

Para cerrar, Maglianese puntualizó: “Al haber menos cantidad de mosquitos, si viene una persona enferma, el brote va a ser menor, más controlable y de menor riesgo. La modalidad es igual a años anteriores: equipos jóvenes con mucha empatía, buscan abordar la vivienda, piden permiso para llegar al patio, así se hace en todo el barrio para cubrir toda la zona”.