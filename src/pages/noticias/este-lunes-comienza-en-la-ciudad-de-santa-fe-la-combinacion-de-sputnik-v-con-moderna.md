---
category: La Ciudad
date: 2021-08-08T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/moderna.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Este lunes comienza en la ciudad de Santa Fe la combinación de Sputnik V
  con Moderna
title: Este lunes comienza en la ciudad de Santa Fe la combinación de Sputnik V con
  Moderna
entradilla: Los turnos, que comenzaron a llegar este fin de semana, deben ser confirmados
  en caso de aceptar a la combinación. La cartera sanitaria envió más de 27 mil turnos.

---
Desde el próximo lunes, la provincia de Santa Fe comenzará con la combinación de vacunas contra el coronavirus para completar esquemas.

Así lo confirmaron fuentes del Ministerio de Salud a la prensa. Indicaron que durante este fin de semana se enviaron turnos en esta ciudad capital ofreciendo, como segundo componente, dosis de Moderna. El objetivo que se propuso el Ministerio de Salud es citar, a nivel provincial, a un total de 27.600 santafesinos

Cabe recordar que como la combinación de vacunas es voluntaria, la provincia aplicará Moderna solo a quienes acepten intercambiar dosis de laboratorios, experiencia que ya fue aprobada por la Nación y otros países del mundo tras diversos estudios.

El mensaje que recibieron quienes fueron vacunados con Sputnik V hace más de tres meses y aguardan por la segunda dosis, dice textualmente: "Usted está recibiendo su turno de 2do. componente con una dosis de vacuna Moderna. Solicitamos lo confirme. En caso contrario, sus turnos serán reprogramado sujeto a disponibilidad de dosis".

"Los turnos son para aquellos que tienen más de 90 días desde la aplicación de la primera vacuna y los mayores de 50 años con comorbilidades”, adelantó la titular de la cartera sanitaria, Sonia Martorano.

Martorano, destacó que “ya tenemos más de 2.800.000 vacunas aplicadas en la provincia de Santa Fe, lo que implica que a esta hora llevamos colocadas 2.160.000 primeras dosis y 700.000 segundas dosis; eso representa que, del total de población objetivo priorizada, el 30 por ciento tiene esquema completo”.

Sobre el avance de la campaña en la provincia, Martorano enfatizó que con los que recibieron la vacuna Sinopharm “ya tenemos asegurada la provisión de la segunda dosis; y estamos completando esquemas entre los 21 y 28 días de la primera aplicación. Del mismo modo que Oxford/AstraZeneca, a los 56 días, u 8 semanas”.

**La eficacia de Sputnik con Moderna**

Martorano advirtió que hay un cuello de botella con relación a las Sputnik V. Igualmente, advirtió: "Debemos hacer una aclaración importantísima: la formulación de la Federación Rusa, desde su concepción, surgió como una vacuna de combinación. Teníamos el primer elemento que era el adenovirus S26 y, el segundo, el adenovirus S5. Es decir, que se previó que combinando las vacunas de distintos laboratorios, pero hechas con la misma tecnología eran igual, o más eficaces, que sin combinar, generando todavía más anticuerpos”.

Y profundizó: “Esto tiene antecedentes paradigmáticos: Alemania combina AstraZeneca/Oxford (que es igual en tecnología a Sputnik) y que está demostrando que funciona como una gran aceleradora o booster de anticuerpos. Es decir, primero, es muy segura. Segundo, muy eficaz”.

Es por ello que el Ministerio de Salud enviará este lunes próximo 27.600 turnos donde se comunicará a estas personas que llevan 90 días esperando la segunda dosis de Sputnik, que podrán ponerse una segunda, pero de Moderna.

Las personas podrán confirmar ese turno. Si no lo confirman, serán inmunizadas en el momento en que esté disponible el componente 2 de la vacuna de la Sputnik V. Queda librado a la voluntad individual, pero el consejo de las autoridades sanitarias tanto provinciales como nacionales es aprovechar las ventajas de la combinación.

Finalmente, Martorano enfatizó que la máxima prioridad es completar los esquemas. “Eso va a realizarse con Moderna. Porque las de Oxford/AstraZeneca se reservan para usar sin combinar, del mismo modo que Sinopharm, habiendo muchísima disponibilidad de ambas en el país”, explicó.