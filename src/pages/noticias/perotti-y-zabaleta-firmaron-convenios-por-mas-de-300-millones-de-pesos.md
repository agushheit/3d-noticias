---
category: Estado Real
date: 2021-09-30T06:00:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/PROYECTO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti y Zabaleta firmaron convenios por mas de 300 millones de pesos
title: Perotti y Zabaleta firmaron convenios por mas de 300 millones de pesos
entradilla: En la ciudad de Santa Fe, el gobernador y el ministro de Desarrollo Social
  de la Nación, rubricaron acuerdos para la construcción de 15 playones deportivos
  y la adquisición de herramientas y materiales de trabajo.

---
El gobernador Omar Perotti y el ministro de Desarrollo Social de la Nación, Juan Zabaleta, firmaron este miércoles un convenio para la construcción de 15 playones deportivos en la provincia de Santa Fe, con vereda perimetral y estación de gimnasio, en los que el gobierno nacional invertirá $122.460.000, para materiales y mano de obra.

El acuerdo, que se rubricó en club Atlético Las Flores II de la ciudad de Santa Fe, surge ante una propuesta del gobierno provincial, y se enmarca en un proyecto sociocomunitario denominado Mejor Barrio, que tiene por finalidad fomentar acciones que apoyen el desarrollo económico y social local.

Además, en el marco del Plan Incluir, el gobernador entregó $ 2.308.214 al presidente del club Las Flores II, para el proyecto de construcción de una cancha de césped sintético de fútbol 5; y en el marco del programa Accionar, el ministro de Desarrollo Social de la provincia, Danilo Capitani, le otorgó un subsidio de $150.000 para obras en las instalaciones.

En la oportunidad, el gobernador Perotti destacó el trabajo que realizan los clubes y la “posibilidad de sumar un playón, porque muchas veces se construían, pero los clubes quedaban sin poder mejorar su infraestructura”.

Además, el gobernador agradeció al ministro nacional “por haber permitido que podamos ampliar estos playones, para que estos clubes crezcan y los chicos se sigan formando. Las instalaciones se van a mejorar, pero lo que no tiene que cambiar es la calidad humana de ustedes que ha permitido que esto siga creciendo”, afirmó el gobernador.

Por su parte, el ministro Zabaleta sostuvo que “esto va a ser parte de la enorme cantidad de playones que va a construir Omar Perotti en Santa Fe. Este playón deportivo tiene que ver con la decisión del presidente de la Nación, Alberto Fernández, de poder tener en cada barrio, en cada rincón de la Argentina, en cada club, un playón deportivo para que las pibas y los pibes puedan practicar deportes en las mejores condiciones. Incluir derechos es lo que nos mueve todos los días a seguir trabajando”, finalizó.

En tanto, el ministro Capitani agradeció las “gestiones del gobernador a nivel nacional, que hoy nos permiten firmar un convenio por más de 15 playones para la provincia. Esta es una política que venimos siguiendo desde que asumimos de fortalecer a las instituciones deportivas, a los clubes de barrio, que contienen a tantos chicos, chicas y adolescentes, para la práctica de un deporte, pero por sobre todas las cosas, para lograr esa inclusión y ese desarrollo que necesitan hoy”.

Por último, el presidente del Club Atlético Las Flores II, Alberto Garau, agradeció la presencia de las autoridades y sostuvo que “este playón es algo muy importante para 450 niños y niñas, jóvenes y adolescentes del club, y también vamos a tener la posibilidad que más gente de esta institución pueda hacer otra disciplina, no sólo fútbol o vóley. Pasamos por una pandemia que nos afectó a todos, pero el Estado nacional y provincial, a través de distintos programas, siempre estuvieron presentes”, finalizó Garau.

**POTENCIAR TRABAJO**

Previamente, en el centro de salud de barrio Las Lomas, el gobernador Perotti y el ministro Zabaleta firmaron una addenda del convenio rubricado en 2020 de adhesión al programa Potenciar Trabajo, que amplía el aporte a $ 199.300.000. Dicho programa tiene como objetivo contribuir a mejorar el empleo y generar nuevas propuestas productivas con el fin de promover la inclusión social plena para personas que se encuentran en situación de vulnerabilidad social y económica.

Luego, Perotti y Zabaleta entregaron 15 guantes, 15 delantales, 15 protectores oculares, 15 protectores auriculares, 3 máscaras, 1 soldadora Inverter 255A, 2 amoladoras angulares 155 mm y 1 taladro de banco con mandril 15 mm, al taller de carpintería y herrería de la Asociación Ramón Carrillo, en el marco del Santa Fe Más.

En la oportunidad, Perotti afirmó que “vamos a seguir trabajando, codo a codo, con el gobierno nacional, con recursos provinciales, ayudando al municipio, porque han quedado obras muy grandes sin hacerse, y la única forma de hacerlas es abordarlas en conjunto”.

“Les pedimos que nos acompañen a seguir trayendo al barrio posibilidades de crecimiento. Creemos que la política se hace de esa manera, con gente que cree y se compromete, con más obras, con mejores condiciones de vida y con más trabajo”, agregó el gobernador.

A su turno, Zabaleta resaltó la importancia de "las organizaciones sociales en esta etapa de reconstrucción de la Argentina”; y señaló que "como dice el Presidente, hay que trabajar todos los días para construir una sociedad más justa e igualitaria".

"Esto no es una ayuda, estos son derechos, nosotros venimos a poner en marcha y a garantizar derechos en la Argentina; por eso hoy estamos acá con una addenda de 200 millones de pesos para seguir comprando herramientas y materiales, porque derecho, inclusión e igualdad es lo que nos mueve todo los días", concluyó Zabaleta.

En tanto, el ministro de Desarrollo Social de la provincia, Danilo Capitani, sostuvo que “estos programas tienen que ver con la posibilidad de acercar a los jóvenes estas capacitaciones que les permiten en el futuro tener un trabajo para sustentarse. Vamos a seguir incrementando el trabajo para llegar a todos, junto a distintas organizaciones sociales”.

Por su parte, el senador por el departamento La Capital, Marcos Castelló, indicó que “cuando era candidato hablaba de humanizar la política. Humanizar la política es esto, es llevar soluciones y certezas adonde en otro momento, había incertidumbres. Hoy aquí, hay un centro de salud, una posta de la policía, hay un gran proyecto, la construcción de Gobernador Menchaca, que va a traer un bienestar a mas de 100 mil santafesinos”.

**PROYECTOS DE IGUALDAD**

Seguidamente, el Ministerio de Igualdad, Género y Diversidad firmó actas acuerdo con el grupo de Promotoras Ambientales del Movimiento de Trabajadoras Excluidas, la Voz de las Mujeres, y el equipo de fútbol Las Sabaleritas de la comunidad Quom, mediante los cuales recibirán un aporte de 50 mil pesos para la compra de insumos y materiales necesarios; y asumen el compromiso de coordinar acciones conjuntas para desarrollar proyectos de igualdad y prevenir las violencias por motivos de género.

En ese marco, la ministra de Igualdad, Género y Diversidad, Celia Arena, precisó que “este fue, hace un año y medio atrás, uno de los primeros lugares donde se puso en marcha el Plan Incluir, un operativo multiagencial, de mucha profundidad, donde intervienen diversos ministerios. Particularmente, desde el Ministerio de Igualdad, en colaboración con distintas organizaciones, llevamos adelante un programa de saneamiento con promotoras ambientales”, integrado por mujeres del barrio, para realizar un diagnóstico y un relevamiento, que va más allá de lo ambiental, indicó Arena; y resaltó que estas mujeres “hoy son más de 40”.

Por último, la ministra destacó que “todas estas obras, y el trabajo que hacemos con todas las personas de la comunidad es la forma de que este Estado presente esté para siempre, la idea es que se unan, trabajen en red, para producir estas mejoras; con una clara decisión política del gobernador de trabajar en profundidad este tipo de respuestas”.

**PRESENTES**

Participaron también de las actividades, el secretario de Articulación de Políticas Sociales de la Nación, Gustavo Aguilera; los secretarios de Prácticas Socio Comunitarias de la provincia, Ignacio Martínez Kerz, y de Deportes, Florencia Molinero; los concejales por la ciudad de Santa Fe, Jorgelina Mudallel y Juan José Saleme; en representación del Movimiento Los Sin Techo, José Ambrosino y José Luis Zalazar; entre otros referentes de instituciones beneficiarias.