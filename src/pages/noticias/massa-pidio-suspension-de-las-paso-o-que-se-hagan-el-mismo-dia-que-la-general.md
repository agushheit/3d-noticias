---
category: Agenda Ciudadana
date: 2021-04-05T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/MASSA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Massa pidió suspensión de las PASO o que se hagan "el mismo día que la general"
title: Massa pidió suspensión de las PASO o que se hagan "el mismo día que la general"
entradilla: El presidente de la Cámara de Diputados sostuvo que tiene "como convicción
  que la política tenga la capacidad de resolver sus diferencias puertas adentro".

---
El presidente de la Cámara de Diputados, Sergio Massa, sostuvo este domingo que "preferiría que se votara en un solo día" mediante un "acuerdo de las fuerzas políticas que evite la realización de las PASO, o con la realización de las PASO el mismo día" que la elección general.

Massa sostuvo que tiene "como convicción que la política tenga la capacidad de resolver sus diferencias puertas adentro" y de este modo "no someta a los argentinos a tener que ir dos veces a votar".

En diálogo con el programa "El cohete a la Luna", de Radio Del Plata, Massa dijo que "preferiría que se votara en un solo día" a través de un "acuerdo de las fuerzas políticas que evite la realización de las PASO, o con la realización de las PASO el mismo día que la general".

Pidió en este sentido "tener en cuenta que es una elección parlamentaria, legislativa" y no se eligen cargos ejecutivos.

Acerca de la posibilidad de un diálogo con la oposición, Massa explicó que el tema ya se trató con líderes parlamentarios de Juntos por el Cambio "planteando diferentes posibilidades".

Pero "la dinámica de toma de decisiones" de la coalición opositora "por tener repartidas las cuotas de liderazgo, hace que se vean obligados a buscar consensos internos", abundó.

"La pandemia es un dato objetivo de la realidad" y el presidente de la Cámara Baja advirtió que es necesario reconocer la "pelea geopolítica mundial" por las vacunas y consideró que "el esfuerzo más grande lo tenemos que poner en la recuperación económica de la Argentina y la vacunación".

También destacó que en el Frente de Todos "tenemos la particularidad de poder dialogar, aún en los temas que pensamos distinto y parte del secreto de la unidad tiene que ser que respetemos el mano a mano en los ámbitos privados y que en lo público sostengamos la idea que tomamos como camino".

Sobre el Covid-19, Massa observó que "el crecimiento de casos en esta segunda ola se está viendo día a día" y añadió que "hay una mutación de cepas que hace que aparezca en los jóvenes con más virulencia", por eso la necesidad de priorizar el combate a la pandemia.

También se refirió a los proyectos que plantean una renovación de la Justicia. "Esperamos que de alguna manera haya alguna propuesta de modificaciones del Poder Judicial por parte de la oposición", dijo Massa, pero consideró que "hay poca predisposición hasta ahora".

"Esperamos que la oposición nos haga su propuesta de la reforma judicial", afirmó entonces. Y añadió que el Presidente (Alberto Fernández) genuinamente dijo que está dispuesto a escuchar" esas propuestas alternativas.

Sobre el trabajo parlamentario dijo que "cuando el debate no es respetuoso, la gente termina perdiendo el respeto al Parlamento como institución".