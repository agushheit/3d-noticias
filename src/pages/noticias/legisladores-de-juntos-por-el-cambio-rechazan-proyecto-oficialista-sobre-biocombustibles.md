---
category: Agenda Ciudadana
date: 2021-04-27T06:37:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/biodisel1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ximena García
resumen: Legisladores de Juntos por el Cambio rechazan proyecto oficialista sobre
  biocombustibles
title: Legisladores de Juntos por el Cambio rechazan proyecto oficialista sobre biocombustibles
entradilla: Ante el inminente vencimiento de la Ley Nro. 26.093 de Biocombustibles,
  diputados y diputadas de todo el arco político de las provincias del centro norte
  del país manifiestan la necesidad de darle tratamiento.

---
Ante el inminente vencimiento de la Ley Nro. 26.093 del Régimen de Regulación y Promoción para la Producción y Uso Sustentables de Biocombustibles, cuya prórroga aprobó el Senado en forma unánime, diputados y diputadas de todo el arco político provenientes de las provincias del centro norte del país han puesto de manifiesto la necesidad de darle tratamiento en la Cámara Baja en reiteradas ocasiones. En todas las oportunidades, la conducción del oficialismo ha negado la discusión.

Bajo este contexto y faltando solo 15 días para el vencimiento de la actual ley, un grupo de legisladores del Frente de Todos ingresó un proyecto que, en palabras de la Diputada Nacional Ximena Garcia, referente del tema en Santa Fe, “claramente atenta contra el desarrollo de los biocombustibles, particularmente contra las PyMES que producen biodiesel, localizadas en su mayoría en la provincia de Santa Fe.”

En este sentido, legisladores de Juntos por el Cambio de Santa Fe, expresaron su rechazo por los términos de este nuevo proyecto de ley, planteando que representa un claro retroceso a los avances logrados en materia de reemplazo de combustibles fósiles por biocombustibles, los que reducen sustancialmente los gases efecto invernadero, en línea a los compromisos firmados por nuestro país en torno a la mitigación del cambio climático.

El proyecto del FdT baja a la mitad el corte actual del biodiesel del 10% al 5%, pudiendo la autoridad de aplicación reducirlo al 3%, cuando la ley debería incluir una progresividad creciente de utilización de biodiesel y bioetanol en los cortes de gasoil y nafta (incentivando la comercialización de B100 y la incorporación de motores Flex que admiten mayores proporciones de bioetanol).

Por otro lado, limita la instalación y/o ampliación de empresas productoras de biocombustibles con destino a mezcla mínima obligatoria, afectando principalmente a las pequeñas empresas que no podrán crecer, impactando directamente el desarrollo de las economías regionales.

Además, no prevé una fórmula que fije precios y actualizaciones contemplando los costos de producción; todo queda en manos de la autoridad de aplicación. Esta situación, que en distintos momentos a paralizado la industria por falta de rentabilidad, genera incertidumbre en un sector donde hay invertidos alrededor de 3.000 millones de dólares y pone en riesgo cerca de 200.000 puestos de trabajo generados en forma directa e indirecta.

A su vez, la propuesta no promociona la investigación, la innovación, la inversión y la producción de biocombustibles de segunda generación que se realiza a través de desechos orgánicos que son altos contaminantes del ambiente y que pueden aprovecharse para generar energías limpias.

Al respecto comentó García, “el proyecto del oficialismo no piensa en el futuro, pone un corsé al desarrollo de los biocombustibles y a la posibilidad de que Argentina se coloque a la par de los países que lideran la lucha contra el cambio climático. Por lo tanto, pedimos prorrogar la Ley Nro. 26.093 y discutir una nueva ley, superadora y que incluya los desafíos que el país y planeta tienen por delante.”