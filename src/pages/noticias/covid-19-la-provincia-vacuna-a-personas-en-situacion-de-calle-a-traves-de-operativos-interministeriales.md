---
category: Estado Real
date: 2021-07-29T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACALLE.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid-19: la provincia vacuna a personas en situación de calle a través
  de operativos interministeriales'
title: 'Covid-19: la provincia vacuna a personas en situación de calle a través de
  operativos interministeriales'
entradilla: " Se realiza, además, un abordaje interministerial con el objetivo de
  ampliar derechos a quienes tienen un acceso limitado a los múltiples servicios del
  Estado."

---
El Ministerio de Salud de la provincia se encuentra realizando, en las ciudades más pobladas, múltiples operativos territoriales garantizando la vacunación contra el Coronavirus a personas que están en situación de calle, en diversas condiciones de vulnerabilidad, y tratando de ampliar derechos a quienes tienen un acceso limitado a los múltiples servicios del Estado.

Al respecto, la subsecretaria de Primer y Segundo Nivel de Atención en Salud Centro- Norte, Marcela Fowler, expresó: “La vacunación contra el Coronavirus en distintas poblaciones en situación de vulnerabilidad son tanto un fìn y como un medio en sí mismos porque si bien la prioridad y la urgencia es inmunizar, estos despliegues son una oportunidad inmejorable para otros abordajes que habilitan la garantía de otros derechos”.

“En cada salida a territorio de manera interministerial (junto a Desarrollo Social, Anses, Pami, Registro Civil, entre otros), se generan distintas oportunidades para la construcción de ciudadanía: se hace promoción y prevención en salud; intentamos referenciarlos a refugios nocturnos u otros dispositivos de ayuda social; evaluamos su situación previsional y alimentaria; se realizan o actualizan DNI; entre otras intervenciones siempre y cuando esto es posible respetando la voluntad de cada persona”, prosiguió Fowler.

Por otra parte, recordó que de manera conjunta con la Municipalidad de Santa Fe se vienen visitando refugios nocturnos aplicando primeras o segundas dosis de vacunas contra el Covid-19. También, dialogando con trabajadores y trabajadoras sexuales, entre otras intervenciones focalizadas y territoriales en el marco de las estrategias generales de prevención del virus.

**Un Estado presente en cada barrio y centro de salud**

Por su parte, el coordinador provincial de Dispositivos Territoriales del Ministerio de Salud, Sebastián Torres, se refirió también a las múltiples tareas barriales y en centros de atención primaria de la salud en donde aseguró “se hace un trabajo minucioso casa por casa, cuerpo a cuerpo, para que el Estado esté presente mediante la vacunación, pero también asegurando otros servicios con intervenciones interministeriales y multiagenciales”. En los centros de salud la vacunación se descentraliza para ampliar el rápido acceso.

“Las estrategias que nos damos por fuera de los vacunatorios centrales o de referencia, en el territorio, son muchas. La campaña de inmunización contra el Coronavirus nos demandó y demanda llegar a cada vecino que, por cuestiones geográficas, económicas, culturas, de conectividad, entre otras, no pudieron inscribirse ni inocularse”, explicó Torres.

Y ejemplificó: “Equipos interministeriales y un móvil sanitario recorren zona por zona, relevando y vacunando, u ofreciendo soluciones de cercanía referenciándolos a los efectores de salud más próximos a donde viven y donde son adscriptos”.

“Estos operativos se realizan en barrios y también en diversas localidades; y los equipos de otros ministerios que nos acompañan aprovechan para resolver diversas cuestiones no estrictamente de salud; o para agendarlas en búsqueda de soluciones integrales”, continuó explicando el coordinador de Dispositivos Territoriales.

Finalmente, en relación a las intervenciones nocturnas en la calle, Torres explicó que las mismas “también son interministeriales; en ellas se establecen vínculos con las personas y se les ofrece acceder a la vacunación si todavía no recibieron alguna dosis, explicándoles las ventajas, al tiempo que se trabaja en soluciones inmediatas de otros problemas que se agravan en época de frío. Se les ofrecen alimentos, ropa de abrigo, y Desarrollo Social intenta referenciarlos a refugios o lugares en donde se dan respuestas a múltiples problemas”.