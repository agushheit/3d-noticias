---
category: Estado Real
date: 2021-09-10T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/GENERO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La provincia trabaja en el fortalecimiento de las áreas locales de igualdad,
  género y diversidad
title: La provincia trabaja en el fortalecimiento de las áreas locales de igualdad,
  género y diversidad
entradilla: El Ministerio de Igualdad, Género y Diversidad suscribió acuerdos con
  localidades del departamento San Lorenzo y San Jerónimo. Además, se firmaron los
  convenios para la creación de Puntos Violetas

---
El Gobierno de Santa Fe, a través del Ministerio de Igualdad, Género y Diversidad, suscribió, este jueves, con localidades del departamento San Lorenzo y San Jerónimo convenios en el marco del Programa de fortalecimiento y/o creación de áreas locales de igualdad, género y diversidad.

Además, se firmaron convenios con Puerto San Martín y Monje para la creación de Puntos Violetas, y se llevó adelante un operativo del Plan Acompañar, dentro del trabajo articulado entre los gobiernos provincial, nacional y local.

Al respecto, la ministra de Igualdad, Género y Diversidad, Celia Arena, recordó que “estos convenios se originan a principio de año cuando convocamos a los 365 gobiernos locales de la provincia a que nos presenten su proyecto institucional, por una instrucción muy clara de nuestro gobernador Omar Perotti que siempre nos anima a estar muy en contacto con las autoridades de cada localidad”.

“Lo que tenemos que hacer y hacemos es acompañar y ser el respaldo para los gobiernos de cada localidad, que son, en definitiva, a quienes los vecinos y vecinas eligen para que conduzcan sus destinos”, señaló la ministra.

“Siempre decimos que no tenemos políticas enlatadas, tenemos programas, líneas de acción, que llevamos adelante con la convicción de que es fundamental trabajar sobre políticas de igualdad, reducción de brechas y oportunidades, y eso lo pusimos a disposición de los gobiernos locales”, concluyó Arena.

En primer término, en la localidad de Puerto San Martín, se llevó adelante la firma de los acuerdos con las autoridades de Capitán Bermúdez, Carcarañá, Coronel Arnold, Fray Luis Beltrán, Fuentes, Luis Palacios, Pujato, Ricardone, Roldán y San Jerónimo Sud. El acto estuvo encabezado por la ministra Arena y el intendente local, Carlos De Grandis.

En tanto, la firma de los acuerdos con las localidades de Barrancas, Bernardo de Irigoyen, Centeno, Coronda, Desvío Arijón, Gálvez, López, Pueblo Irigoyen, San Genaro y Monje, departamento San Jerónimo, se realizó en Monje. El acto fue encabezado por la secretaria de Mujeres, Género y Diversidad, Florencia Marinaro; la subsecretaria de Fortalecimiento Territorial en Igualdad, Género y Diversidad, Soledad Zalazar; y el intendente local, Pedro Severini.

**Más de 80 convenios firmados**

Desde que comenzó el recorrido por las distintas localidades se suscribieron alrededor de 80 convenios con municipios y comunas de los departamentos San Jerónimo, Garay, La Capital, Iriondo, Belgrano, Rosario, 9 de julio, Constitución, Rufino, Castellanos, General López, Caseros, San Justo, San Javier y Vera.

El Ministerio lleva adelante el programa de Fortalecimiento y creación de áreas de Igualdad, Género y Diversidad para que municipios y comunas desarrollen un proyecto institucional de políticas vinculadas a la promoción de derechos; la prevención y atención en violencias por motivos de género; la diversidad sexual; y los cuidados, de acuerdo a las necesidades de cada territorio, impulsando las visiones de los gobiernos locales.

A comienzos de este año se convocó a participar de este programa a los 365 municipios y comunas de la provincia. Esto implica la recepción de recursos técnicos y económicos para trabajar políticas concretas, adaptadas a cada territorio, que aporten a la construcción de sociedades más igualitarias.

**Puntos Violetas**

Los Puntos Violetas serán espacios de igualdad de derechos que se construirán en 40 localidades de la provincia, en el marco del Plan Incluir, donde las mujeres y personas de la diversidad sexual podrán acceder a los programas, iniciativas y políticas públicas del Ministerio de Igualdad, Género y Diversidad.

En cada uno de los 40 Puntos Violetas, distribuidos estratégicamente en todo el territorio provincial, funcionarán espacios de contención y asesoramiento, de articulación con organizaciones y asociaciones para proveer de herramientas teóricas y prácticas para el acceso al trabajo, la producción, el empleo y la autonomía económica de mujeres y disidencias; programas de ATR Juventudes y propuestas culturales, entre otras iniciativas. Además, allí se trabajará en la prevención y el abordaje integral de situaciones de violencia por motivos de género.