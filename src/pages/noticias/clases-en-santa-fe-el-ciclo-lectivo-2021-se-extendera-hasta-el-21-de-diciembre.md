---
category: Agenda Ciudadana
date: 2021-09-15T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/CLASESEXTENDED.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Clases en Santa Fe: el ciclo lectivo 2021 se extenderá hasta el 21 de diciembre'
title: 'Clases en Santa Fe: el ciclo lectivo 2021 se extenderá hasta el 21 de diciembre'
entradilla: Lo adelantó este martes la ministra de Educación, Adriana Cantero y no
  descartó que "algún grupo prioritario" retome el contacto con el sistema educativo
  en febrero de 2022.

---
La ministra de Educación, Adriana Cantero adelantó este martes que, como consecuencia de la pandemia de coronavirus, las clases terminarán el 21 de diciembre próximo en todo el territorio provincial y no descartó que "algún grupo prioritario" retome el contacto con el sistema educativo en febrero de 2022, tal como se realizó el año pasado, para reforzar contenidos.

En cuanto a la promoción de los estudiantes, indicó que se continuará lo acordado en el Consejo Federal de Educación. "La evaluación será cualitativa, conforme al acuerdo nacional firmado por todos los ministros de educación del país, que propone un proceso que mire un bienio como algo 'continuo'. El último acuerdo federal marca parámetros que tienen que ver con la mirada integral del porcentaje de vinculación e interacción con la escuela. Este último trimestre tiene un enorme peso en la evaluación", explicó.

Al ser consultada sobre cómo se procederá este año a la promoción de materias , Cantero enumeró las acciones que se están desarrollando: "Estamos en pleno despliegue del plan nacional Acompañar, que implica la identificación de escuelas con trayectorias debilitadas a las que se les asignarán recursos humanos y materiales en este trimestre, en todos los niveles; el trabajo en los Sábados Activos con los terceros y cuartos años de secundario; el plan Egresar Sin Adeudar para el último año del secundario; y las tutorías robustecidas para el nivel primario y el básico de la secundaria", subrayó.

"No nos conformamos con que hayan expresado su voluntad de volver a las aulas, como en este año que registramos casi 20.000 estudiantes más en el sistema educativo. Tenemos que ser capaces de sostenerlos en sus trayectorias y a eso se lo hace con aprendizaje y por lo tanto el cuaderno es una ayuda para los docentes y para los estudiantes en ese proceso de aprendizaje", concluyó.

**Presencialidad**

Por otra parte, Cantero aseguró que se encuentran trabajando para alcanzar el 100% de presencialidad plena en todas las escuelas santafesinas y remarcó que la provincia se encuentra en el "nivel más alto de todo el país".

Las declaraciones tuvieron como marco el lanzamiento de la 3ª edición de los Cuadernos Pedagógicos producidos por la cartera provincial. Se trata de 31 cuadernos para los niveles inicial, primario y secundario, y las modalidades técnicas y de educación para adultos.

Tras la presentación, Cantero destacó el trabajo de los docentes en el regreso a la presencialidad escolar: "Quiero agradecer al trabajo de todos los educadores santafesinos que pudieron hacerle frente a un tiempo de tanta movilidad en la gramática escolar en un año y medio", manifestó en conferencia de prensa.

Aseguró que Santa Fe "es una de las provincias que mayor nivel de presencialidad plena está mostrando en estos días" y señaló, al día de hoy, el 95 por ciento de los alumnos ya volvieron a las aulas.

En ese sentido, dijo que "sólo queda un porcentaje que necesita excepcionalidad al protocolo para poder recuperar la presencialidad diaria", y adelantó que "la semana que viene los estaremos asistiendo con el acceso a los medidores de dióxido de carbono y el acceso a los testeos frecuentes para cumplir con los requerimientos que pide el protocolo cuando se necesita achicar la distancia entre los bancos".

Con respecto a las escuelas en las que se registraron contagios de Covid-19, la ministra explicó que en esos casos se "procede conforme a lo que establece la línea 0800 Escuela. Los protocolos están vigentes y las escuelas aplican estos procedimientos, que están aceitados de acuerdo a las indicaciones de las autoridades de Salud".

La cartera presentó la tercera serie de Cuadernos Pedagógicos, de los que se imprimieron más de un millón de ejemplares con una inversión de 40 millones de pesos. Foto: Gentileza

**Contribución a la calidad educativa**

La titular de Educación precisó que la idea del material es contribuir a un conocimiento en relación con el contexto de los estudiantes de la provincia. "Para nosotros el cuaderno es un elemento más de la calidad educativa con la que trabaja la provincia de Santa Fe y todos los chicos y las chicas en la escuela; es el primer indicador de calidad de un sistema educativo, el segundo indicador es que las y los alumnos estén aprendiendo. Por ello los cuadernos santafesinos expresan un modo de enseñar en relación con un conocimiento que se construye en vínculo y no un conocimiento fragmentado que no contribuye a la comprensión con que de verdad se construye el aprendizaje".

La tercera edición de los cuadernos continúa las Series 1 y 2, por las cuales se distribuyeron dos millones de ejemplares en todo el territorio provincial. "Las chicas, los chicos y los maestros de todo el país se han pronunciado a favor de la necesidad de materiales impresos. Por eso Santa Fe inició rápidamente el proceso de diseño y construcción, porque el material impreso es el más utilizado en este contexto de pandemia en donde no todos pueden acceder a internet. Los cuadernos santafesinos reafirman su calidad a la hora de contribuir al sistema educativo ya que provincias vecinas mostraron interés para reproducirlo y emplearlo en sus establecimientos", indicó Cantero al respecto.

Por su parte, el secretario de Educación, Víctor Debloc, se refirió al valor pedagógico de la producción editorial: "La pedagogía contemporánea cada vez viene más marcada por el concepto de la igualdad y por eso decimos que el cuaderno es un bien igualador para cada estudiante, y tenemos tres testimonios de eso, el boleto educativo gratuito y el boleto educativo rural; la telefonía celular para alumnos de primer año de todas las escuelas rurales y las 3 ediciones de cuadernos pedagógicos".

**Paritarias**

Cantero aseveró que en los próximos días serán convocados los gremios docentes para una nueva reunión paritaria: "Estamos cumpliendo el acuerdo paritario firmado en marzo, que implicaba el último aumento en septiembre. La convocatoria se hará para el último trimestre del año". De esta manera, se reanudará el diálogo salarial luego de que la Casa Gris rechazara el pedido de adelanto para discutir incrementos.