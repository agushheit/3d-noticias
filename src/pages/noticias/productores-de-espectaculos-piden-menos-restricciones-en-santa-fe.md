---
category: Agenda Ciudadana
date: 2021-09-30T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/PRODUCTORES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Productores de espectáculos piden menos restricciones en Santa Fe
title: Productores de espectáculos piden menos restricciones en Santa Fe
entradilla: Según indican desde el sector, la actividad cultural en vivo es uno de
  los pocos rubros que siguen trabajando con fuertes limitaciones que no permiten
  avanzar en la recuperación económica

---
Luego de los anuncios nacionales de aperturas y flexibilizaciones respecto a los espectáculos, la Cámara de Productores de la Provincia de Santa Fe puso en tema su pedido público a la Provincia de Santa Fe y las municipalidades tomando como precedente lo sucedido en los últimos días alrededor de todo el país acompañar en este nuevo decreto que se renovará el próximo viernes 1 de octubre.

 Según indican desde el sector a través de un comunicado, "la actividad cultural en vivo es uno de los pocos rubros (como los salones de fiestas y boliches bailables) que siguen trabajando con fuertes restricciones y limitaciones que no permiten avanzar en la recuperación económica del sector y, sin embargo, puertas adentro de esta actividad se ven fuertemente limitados a la capacidad, si puede estar el público de pie o bailar, distanciamiento que obligan a forzar altos costos de producción y demás, que hacen totalmente incongruente respecto a lo que sucede puertas afuera".

 "Las provincias de San Juan, Córdoba, Tucumán, Ciudad de Buenos Aires (en sus terrenos y espacios propiedad de Nación como el CCK, Tecnópolis, Saldías, entre otros), ya han implementado la eliminación del sistema de burbujas, autorizado la concurrencia del público al 100% y manteniendo los protocolos razonables y aconsejados por el Ministerio de Salud (uso de barbijo y toma de temperatura), y asimismo el pasado fin de semana han realizado fiestas y espectáculos de aproximadamente 5.000 personas bajo esta “vieja modalidad”, tales como Fiesta Bresh en Forja (Córdoba), Fiesta Bresh en San Juan, y un evento de la semana de las juventudes en el autódromo de San Juan", añaden en el documento. "Esta misma semana - continúan - , según fuentes oficiales, se sumarán las provincias de Misiones, Corrientes, Chaco y Entre Ríos, entre otras".

 En tanto, informan que CaPESF ha solicitado al Gobierno de la Provincia de Santa Fe los siguientes puntos:

\- Apertura al 100% de los aforos en espacios abiertos y cerrados.

\- Extensión horaria para la realización de espectáculos en sintonía con los horarios solicitados por el sector gastronómico (domingo a jueves hasta las 02hs y para los días viernes, sábados y vísperas de feriado hasta las 03hs).

\- Eliminar la exigencia del sistema de “burbujas” en shows de pie, las cuales representan un riesgo frente a los planes de evacuación, y permitir explícitamente la modalidad “de pie” o “baile” en nuestros espectáculos, ya que se encuentran implícitamente prohibidas. Este pedido está motivado por el mejoramiento de la situación epidemiológica, el alto grado de vacunación y el cumplimiento por parte de todos los productores de espectáculos de la región, sumado a casos de aperturas masivas que han demostrado no ser ejes ni disparadores de contagios

 "Reiteramos nuestra convicción de que la situación actual podrá revertirse solamente trabajando en forma conjunta entre lo público y lo privado con medidas acordes que favorezcan las actividades comprometidas como la nuestra, y estemos alineados en las aperturas y flexibilizaciones generales dentro de la Provincia, donde carece de sentido la exigencia a los shows respecto a la vida cotidiana que estamos atravesando el resto de la población fuera de los mismos", concluye el comunicado.