---
category: La Ciudad
date: 2021-05-06T08:23:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/hospital-militar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Operativo covid: se intensifican los trabajos en avenida Freyre'
title: 'Operativo covid: se intensifican los trabajos en avenida Freyre'
entradilla: La Municipalidad de Santa Fe realizará un corte de tránsito y peatones
  frente al Cullen para facilitar la instalación de la manga que lo comunicará con
  el hospital militar.

---
La Municipalidad informa que, a partir del jueves, avenida Freyre al 2100 estará cortada en ambas manos. En el marco del Operativo Covid, se dispuso el desvío del tránsito y de peatones para que se puedan continuar las tareas de montaje de la rampa que vinculará al hospital “José María Cullen” con el centro asistencial reubicable del predio del Liceo Militar “General Manuel Belgrano.

Para garantizar una comunicación segura entre ambos nosocomios es necesario que no se pueda circular en vehículos ni caminando por esa zona. Por lo que desde mañana, ambas manos de avenida Freyre estarán cerradas al paso frente al nosocomio.

Esto implica que al desvío que ya realiza la línea 16 (en el sentido sur-norte) se suman los nuevos recorridos de las líneas 9 y 14, que quedan de la siguiente manera:

* Línea 9: de recorrido habitual por Mendoza a Saavedra, Juan de Garay, 4 de Enero y Mendoza, a recorrido habitual.
* Línea 14: de recorrido habitual por avenida Freyre a Primera Junta, Zavalla, Mendoza, Saavedra y Monseñor Zaspe, a recorido habitual.

  Por otro lado, las paradas de las líneas serán:


* Línea 9: avenida Freyre y Primera Junta, avenida Freyre y Mendoza, Mendoza y Saavedra, Saavedra y Lisandro de la Torre, y Juan de Garay y Francia.
* Línea 14: Mendoza y avenida Freyre, Mendoza y Saavedra, Saavedra y Lisandro de la Torre, Saavedra y Juan de Garay, Saavedra y Moreno, y Monseñor Zaspe y San Lorenzo.

En tanto, se recuerda que sigue vigente para la Línea 16 la modificación de su recorrido de la siguiente manera: avenida Freyre, Moreno, Francia, Primera Junta y San Lorenzo, hacia recorrido habitual. Las paradas se ubicarán, entonces, en avenida Freyre y Moreno, Saavedra y Moreno, Francia y Corrientes, Francia y Lisandro de la Torre, Francia y Mendoza, y Primera Junta y San Lorenzo.