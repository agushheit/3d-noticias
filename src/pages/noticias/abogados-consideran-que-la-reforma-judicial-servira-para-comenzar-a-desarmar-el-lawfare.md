---
category: Agenda Ciudadana
date: 2021-03-07T06:30:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/COMODORO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Abogados consideran que la Reforma Judicial servirá para comenzar a desarmar
  el "lawfare"
title: Abogados consideran que la Reforma Judicial servirá para comenzar a desarmar
  el "lawfare"
entradilla: Con media sanción del Senado, la reforma propone fusionar los 12 juzgados
  penales del Fuero Federal con los 11 en lo Penal Económico y, además, duplicarlos
  para llegar así a 46 tribunales.

---
Abogados que litigan en la Justicia Federal consideraron este sábado que el proyecto de Reforma Judicial, con media sanción del Senado, servirá para desarmar el 'lawfare' y para designar magistrados y fiscales con un nuevo perfil, a la altura de las demandas sociales.  
"El capítulo que contiene las disposiciones sobre el bien obrar de los jueces es el mejor soporte, hasta aquí, para deconstruir el 'lawfare'", señaló a Télam Eduardo Barcesat, letrado querellante en innumerables causas en la Justicia Federal y ordinaria.  
El constitucionalista y activista por los Derechos Humanos se refirió así al decálogo de 'reglas de actuación' para los magistrados, incluido en el capítulo IV, artículo 72, de la reforma aprobada por el Senado en agosto pasado.

Esos diez puntos disponen, por ejemplo, que el magistrado debe mantener un "trato equivalente" entre las partes; ejercer su función libre de interferencia y comunicar al Consejo de la Magistratura cualquier intento de "influencia indebida" por parte de personas, grupos de poder o miembros de otros poderes de la Nación, entre otras disposiciones.

Por su parte, la abogada **Natalia Salvo**, abogada laboralista pero también interviniente en expedientes federales, coincidió con Barcesat y evaluó que "lo más interesante de la reforma es el proceso de transparencia en la selección de los jueces, que va a ser filmado, y también la incorporación de las cuestiones de género".  
"Cuando el presidente Alberto Fernández habló del control cruzado (en la apertura de sesiones legislativas del Congreso, el pasado lunes 1º de marzo), se refería a que necesitamos más control sobre un poder que, fácticamente, se está auto-controlando: no tiene transparencia, no conocemos sus declaraciones juradas, es un poder vitalicio", planteó la letrada.  
En cuanto a la resistencia a los cambios que se proponen desde el Ejecutivo, Barcesat consideró que las motivaciones "son políticas" porque, a su entender, el proyecto implícito de esos magistrados es "la continuidad del 'lawfare'".  
"El poder concentrado tiene sus referentes en los partidos, los que gritaban en la Asamblea Legislativa, y también los tiene en la Justicia, que es el poder más permeable que encontró el poder concentrado para desplegar su proyecto", razonó por su parte Salvo.  
  
Para la abogada, la reforma se impondrá por el "descrédito de la Justicia, su lentitud, el hecho de que no llega, además de la evidencia de que tenemos una Corte Suprema rica y juzgados pobres".  
  
**Reforma Judicial**

La reforma impulsada por la gestión Fernández-Fernández propone fusionar los 12 juzgados penales del Fuero Federal con los 11 en lo Penal Económico y, además, duplicarlos para llegar así a 46 tribunales, en lo que seria un nuevo esquema de Justicia Federal Penal.  
La propuesta, que ya logró media sanción de la Cámara Alta, incorpora además modificaciones al sistema de subrogancias y a los procesos para la elección de jueces.  
Respecto del proyecto original enviado por el Ejecutivo, el Senado decidió no unificar el fuero Civil y Comercial con el Contencioso Administrativo Federal, pero sí crear un tribunal de resolución de los conflictos de competencia de ambos fueros.  
El Senado también resolvió mantener el examen escrito en los concursos de selección de magistrados, la obligatoriedad de audiencias públicas para transparentar ese proceso y que los sorteos de las causas sean grabados, bajo apercibimiento de nulidad.