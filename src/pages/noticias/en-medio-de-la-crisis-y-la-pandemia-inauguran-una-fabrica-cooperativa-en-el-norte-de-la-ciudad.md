---
category: La Ciudad
date: 2021-10-16T06:15:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/COOP.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: En medio de la crisis y la pandemia, inauguran una fábrica cooperativa en
  el norte de la ciudad
title: En medio de la crisis y la pandemia, inauguran una fábrica cooperativa en el
  norte de la ciudad
entradilla: Tiene 45 trabajadores que desarrollan productos con certificación nacional
  en una nueva cooperativa ubicada entre los barrios Santa Rita y Santa Marta.

---
Este sábado en Blas Parera al 9326 abrirá la cooperativa Tosco Polo Productivo. Se trata de una fábrica compuesta de 45 trabajadores del área de la construcción y productos relacionados. "Anunciamos un punto de llegada, que viene de la mano de la formalización definitiva como cooperativa de trabajo, y la certificación técnica de nuestro producto para ser comercializado en todo el país", expresan desde la empresa.

La fábrica tiene cuatro unidades productivas. La primera es de producción de adoquines para pavimento y vereda, la segunda producción es de bloques de hormigón, la tercera rama realiza herrería semi-industrial y la última hace reciclado y servicio socio-ambiental. Además, cuentan con equipo técnico de obra pública. En relación a los trabajos de herrería, producen puestos de feria, gazebos plegables, juegos para plazas, soportes para escenarios, entre otros. Los bloques se venden para construcciones familiares o pequeñas obras, al por menor.

Algunos de los trabajos de herrería realizados por la fábrica cooperativa Tosco Polo Productivo.

Tosco Polo Productivo nació hace cuatro años con trabajo organizado desde una perspectiva de economía popular, de la mano de la Unión de Trabajadores y Trabajadoras de la Economía Popular (UTEP). Los trabajadores que componen la fábrica son de diferentes barrios populares de la ciudad como El Pozo, Roma, Acería, Barranquitas, Villa Hipódromo, Cabal, Pompeya, 29 de Abril, Santa Marta, entre otros.

Sobre la inauguración, desde la UTEP señalan a este medio: "Es gracias a la organización y autogestión de sus trabajadores en el camino por la conquista del trabajo digno, hoy abraza la labor de medio centenar de compañeras y compañeros oriundos de los barrios populares de nuestra ciudad. El motor cotidiano es el anhelo por hacer del cooperativismo una práctica cotidiana solidaria y asociativa, un proyecto de vida profundamente comunitario que al mismo tiempo que demuestre capacidad productiva de escala, levante la bandera por tierra, techo y trabajo para todos y todas".

"Son años de organización, autogestión, fracasos, aciertos, tropiezos, llantos, abrazos, enojos, y emociones de todo tipo, pero con una inquebrantable prepotencia de trabajo y convicciones a prueba de balas. Nacida, parida y forjada al calor de la resistencia al descarte del sistema capitalista. Hicimos trinchera en el abrazo compañero, la organización comunitaria, la economía popular y la obstinada lucha por el trabajo digno", expresan.

Y concluyen: "Anunciamos un punto de llegada, que viene de la mano de la formalización definitiva como cooperativa de trabajo, y la certificación técnica de nuestro producto para ser comercializado en todo el país. Pero al mismo tiempo punto de partida, para rebrotar, tomar impulso y salir a correr el horizonte de lo posible a los codazos o a los abrazos, para demostrar que es posible construir proyecto de vida mediante la organización popular, comunitaria y autogestiva".