---
category: La Ciudad
date: 2021-09-28T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASSA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Corte programado en el suministro de agua potable para este miércoles a la
  madrugada
title: Corte programado en el suministro de agua potable para este miércoles a la
  madrugada
entradilla: Aguas Santafesinas informa a los usuarios de la ciudad de Santa Fe que
  durante la madrugada de este miércoles 29 a partir de las 00:00 y hasta las 03:00
  se interrumpirá el suministro de agua potable.

---
El motivo es la concreción de trabajos impostergables de mantenimiento en los tableros de energía de la toma Hernández de captación de agua del río Santa Fe.

Simultáneamente, se ejecutarán otros trabajos por lo que registrará una baja en la presión del servicio entre las 03:00 y las 06:00 en la zona Noroeste de la ciudad.

En este caso el motivo es la renovación de dos válvulas de sectorización de la red primaria de distribución en Av. Facundo Zuviría e Iturraspe, y en 4 de Enero e Iturraspe.

El sector comprendido por estos trabajos programados está delimitado por Facundo Quiroga, Iturraspe, Av. Gral. Paz y Av. Circunvalación Oeste.

Estos trabajos fueron programados en horario nocturno para minimizar las molestias a los usuarios.

Posteriormente podría registrarse turbiedad la cual se supera dejando circular agua por unos minutos.