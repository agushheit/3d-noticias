---
category: La Ciudad
date: 2021-08-09T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/casa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Nancy Balza / El Litoral
resumen: El sueño de la casa propia es la ecuación más difícil de resolver
title: El sueño de la casa propia es la ecuación más difícil de resolver
entradilla: 'La primera cifra es la cantidad de inscriptos para un proyecto de viviendas
  en una ciudad santafesina; la segunda, las unidades efectivamente disponibles. '

---
La demanda de viviendas supera ampliamente la oferta de proyectos provinciales y ante cada anuncio oficial, sea de la jurisdicción que fuera, las consultas se disparan. Fue así con la novedad de 108 unidades habitacionales en barrio Villa Setúbal de esta capital y lo es en cada licitación que se pone en marcha: los números del título corresponden a la ciudad de Esperanza cuando hubo 1125 personas aptas para acceder a 20 viviendas.

En Rufino, fueron 458 postulaciones para 12 casas. "No hay manera de ser justos", concluye el director de Vivienda y Urbanismo de la provincia José Kerz, en diálogo con El Litoral. Es que la necesidad del techo propio se palpa en cada uno de los planes que ofrece el Estado, cualquiera sea su jurisdicción. Y la posibilidad real de cumplir ese sueño es bastante esquiva.

Aún así, Kerz evalúa que "sin la asistencia estatal hay un universo enorme de población que no podría acceder a una vivienda".

El 27 de julio pasado autoridades del gobierno provincial, incluida la ministra de Infraestrucura, Servicios Públicos y Hábitat, Silvina Frana y el secretario de Hábitat, Vivienda y Urbanismo Amado Zorzón, presentaron la construcción de 108 viviendas en el barrio Villa Setúbal de esta ciudad. Y las consultas "explotaron", tanto en los comentarios de las publicaciones, donde un buen número de personas preguntaban cuándo, cómo y cuánto, como en la propia repartición oficial donde, en una semana, se apuntaron 6.243 inscripciones en el registro digital con un pico importante el miércoles, es decir, el día siguiente al anuncio.

Para Kerz, construir planes habitacionales es una experiencia "linda y dolorosa a la vez: para las 20 viviendas que sorteamos (por streeming) en Esperanza se habían inscripto 1125 grupos familiares aptos. Es decir que ya habían pasado el cedazo del sistema". Lo bueno es que habrá 20 nuevas viviendas, pero "después del sorteo quedaron 1105 sin esa posibilidad; y eso solo en Esperanza".

Por encima de esa pequeña estadística, el registro digital de Vivienda tiene un total de 120 mil inscriptos en toda la provincia. "Son personas que alquilan o viven con otro familiar o presentan distintas situaciones, pero que en definitiva no tienen su casa propia", resume Kerz.

"No voy a pasar a la historia por esta frase, pero vivienda se construye donde hay terreno", advierte "Por un lado está la política de tierra, pero por el otro el aprovechamiento de los terrenos tiene que ser un imperativo de cualquier política de vivienda".

\- ¿Y hay terrenos?

\- Tenemos algunos. Hubo un momento en que había una política de stock de terrenos. Ahora el criterio es construir viviendas en todo terreno que sea propiedad de Viviendas respetando las identidades de los barrios y las disposiciones del gobierno local. Pero construir.

\- ¿Las viviendas que se construyen son para familias? Hay otras poblaciones, como personas mayores, que también demandan una casa.

\- Primero tenemos que construir para grupos familiares; ese es el imperativo legal en los términos de la legislación nacional. Cuando hacemos vivienda de un dormitorio estamos focalizando en recién casados, personas mayores o, por ejemplo, dos hermanos (es decir, personas que pueden convivir con un solo dormitorio). Por eso la oferta es de viviendas de uno, dos o tres dormitorios. Vamos a tratar de incluir en algunos proyectos, otras de cuatro dormitorios pensadas no solo para una familia numerosa sino también para familias ensambladas.

**Del dicho al hecho**

\- ¿Cómo es el procedimiento desde que una persona se inscribe el registro hasta que recibe una casa?

\- Cuando una persona con domicilio en Santa Fe se inscribe al registro digital de la vivienda, lo hace para todos los planes de la ciudad. Luego, cuando estamos en el 65, 70 % de la obra anunciamos la apertura de la inscripción y se publican las condiciones. ¿Por qué en ese momento? Porque es cuando sabemos cuánto va a salir la obra. Allí se definen las condiciones para acceder: cantidad de sueldos mínimos y máximos, por ejemplo).

Luego se hace la primera selección para cotejar que los inscriptos cumplan con todos los requisitos. Se elabora el padrón provisorio, se publica y se abre un plazo para consideraciones especiales de las personas interesadas. Todo eso se analiza y se confecciona el padrón definitivo que se vuelve a publicar. Allí se llama a sorteo. Uno de los últimos fue, precisamente, para las 20 viviendas de Esperanza que se transmitió por streeming con una cantidad de 700 personas conectadas. (¿Éxito de rating o altísimo interés por acceder a un techo propio? La segunda opción parece la más adecuada).

Se sortean igual cantidad de titulares que de suplentes que son los preadjudicatarios: se convoca a los titulares para que presenten la documentación y, en caso de que no cumplan con los requisitos se llama a un suplente.

Luego se hacen los boletos de compraventa y se entregan las viviendas.

A la vez estamos entregando escrituras. Son los dos extremos de este proceso: por un lado, se entrega la llave a quien empieza a gozar de esa vivienda (y a pagar las cuotas) y por el otro se entrega la escritura a quien durante años cumplió con su pago.

**El proyecto de Villa Setúbal**

Luego del anuncio de 108 viviendas en un sector de Villa Setúbal, las consultas se dispararon, pero también las reacciones. "Tuvimos una reunión larga con vecinos. Hay opiniones en contra de la obra, con argumentos que puedo entender, pero no comparto. Esos son terrenos de Vivienda y Urbanismo que están desde hace años disponibles para construir viviendas. Además, la Universidad Tecnológica pidió varias veces autorización para construir una estructura que permita desarrollar su actividad", explica Kerz.

"Convocamos a la Tecnológica, llegamos a un acuerdo y se decidió privilegiar la construcción de viviendas pero sin olvidar una institución que de alguna forma marca el barrio porque desde siempre a esos terrenos les decimos 'los terrenos de atrás de la Tecnológica' ", explicó.

"Se hizo un proyecto muy bueno, no se pidió ninguna excepción y profesionales de la Tecnológica, que van a pagar la obra en los mismos términos que el resto, colaboran en la parte de equipamiento".

En definitiva, "es una inversión de mil millones de pesos que, si no se vuelca en la ciudad, se va a gastar en otro lado. Mientras tanto, tenemos las licitaciones en marcha: la primera el 23 y la segunda el 30 de agosto".