---
category: Agenda Ciudadana
date: 2021-02-28T06:30:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/lARRETS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Tras sus diferencias por críticas a la vacunación en CABA, JxC busca abroquelarse
  contra el gobierno
title: Tras sus diferencias por críticas a la vacunación en CABA, JxC busca abroquelarse
  contra el gobierno
entradilla: Bullrich criticó a Quirós y Macri salió a defender a Horacio Rodríguez
  Larreta en una semana en que Juntos por el Cambio expresó diferencias que quieren
  ser limadas.

---
Después de que las denuncias al Gobierno por las irregularidades en la vacuna contra el coronavirus volvieran a mostrar la distancia entre "duros" y "moderados" en Juntos por el Cambio, y en una semana en la que Horacio Rodríguez Larreta quedó en el centro de la escena tras su sorpresivo viaje en avión privado a Buzios, la coalición opositora se esforzó por unificar criterios y buscó abroquelarse en su crítica al oficialismo.

El primero en mostrar esa reacción fue el expresidente Mauricio Macri, que esta semana salió a defender a Rodríguez Larreta tras la imputación que había recibido el jefe de Gobierno porteño por presunta "privatización" de la campaña vacunatoria en CABA.

La acusación contra el alcalde porteño comenzó el martes pasado, cuando el jefe de Gobierno y su ministro de Salud, Fernán Quirós, fueron denunciados por ceder dosis de las vacunas contra la Covid-19 a obras sociales y prepagas y, también, por la presunta inmunización en un local de Parque Chacachuco ligado a la UCR.

Las dos denuncias quedaron a cargo del juez federal Ariel Lijo y tuvieron sus primeras medidas de prueba a finales de semana, luego de que el miércoles último el fiscal Carlos Stornelli les diera impulso, imputara inicialmente a Rodríguez Larreta y Quirós y solicitara un procedimiento en la cartera de Salud de la Ciudad.

Mientras un sector busca hacer eje en los planteos parlamentarios y lograr avances concretos en sus reclamos, otra corriente interna de la coalición PRO-UCR-Coalición Cívica prefiere hacer hincapié en los discursos que apunten al oficialismo.

Además, como sucedió durante 2020, una nueva marcha contra el Gobierno terminará de mostrar quiénes se ubican a uno y otro lado de la brecha opositora.

Esa disyuntiva volvió a quedar clara hoy por la tarde en Plaza de Mayo, alrededor de la quinta presidencial Olivos y en las ciudades de Córdoba y en Mendoza, donde diversas organizaciones se convocaron para reclamar contra el Gobierno en repudio al episodio de las vacunas en el Ministerio de Salud y el Hospital Posadas.

Como lo hizo en las anteriores, Bullrich había anunciado su presencia, al igual que los diputados de ala dura Waldo Wolff y Fernando Iglesias, aunque también se pudo ver al senador nacional Martín Lousteau (UCR-CABA) y al economista de extracción radical Ricardo López Murphy, entre otras caras conocidas.

"Este sábado marcharemos en paz, por la angustia de los adultos mayores, por el sentimiento de injusticia de los que están en primera línea contra el Covid y por la indignación que provoca que la oligarquía k se apropie de la vacuna. A esta nueva movilización ciudadana, Yo voy", había publicado Bullrich, quien ejerce la presidencia del PRO y al mismo tiempo marca el ritmo de la confrontación más beligerante con el Frente de Todos.