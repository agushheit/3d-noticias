---
category: Estado Real
date: 2021-02-01T06:06:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/isep.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno Santa Fe
resumen: La provincia duplicó el número de ingresos al Instituto de Seguridad Pública
  (ISeP)
title: La provincia duplicó el número de ingresos al Instituto de Seguridad Pública
  (ISeP)
entradilla: Más policías en la calle, más mujeres en la Policía.

---
La provincia de Santa Fe duplicó el número de ingresantes al Instituto de Seguridad Pública (Isep), lo que redundará en más polícas en las calles. Además, del total de ingresos el 60% son mujeres.

Al respecto, el subsecretario de Formación y Capacitación, Andrés Rolandelli, aseguró que "el gobernador Omar Perotti, por intermedio del ministro de Seguridad, Marcelo Sain, nos pidió que estemos abocados en dos aspectos importantes para los próximos policías, la capacitación constante y ampliar el cupo de ingresantes a 1660 para este año, mejorando la infraestructura de las sedes, lo que ya estamos haciendo. Llevamos agua potable y cloacas al ISEP Recreo, que no tenía, y estamos avanzando en las obras en la nueva sede con la que va a contar la provincia en la localidad de Murphy, que será inaugurada próximamente”.

"Estas reformas tanto institucional como edilicias son la base de la transformación que queremos dejar a las actuales y a la nueva camadas de policías de la provincia, Estamos trabajando a mediano y largo plazo, para tener la policía que se merecen los santafesinos. El ministro Sain tiene como prioridad sacar a la calle policías preparados, con recursos, con vocación y con una fuerte presencia femenina que es lo que estamos viendo de acuerdo a los números de ingresos y egresos de los últimos años, con un 60% más de mujeres", detalló Rolandelli.

Según el informe confeccionado por la Subsecretaría de Formación y Capacitación, en 2018 no ingresaron mujeres; en 2019 ingresaron 235; en el 2020, 330; y para 2021 está proyectado el ingreso de 900 mujeres y 700 hombres, duplicando el número total de cadetes del Isep.

Por su parte, la jefa de Policía de la provincia, Emilce Chimentti, aseguró que "estamos satisfechas con los números de ingresantes al Instituto, y sobre todo con los policías que egresaron hace unos días, ya que estos efectivos se incorporan en un año muy complejo, por la pandemia, lo que va a mejorar la prevención en las calles.

“Las mujeres policías estamos teniendo una participación muy importante estos últimos años, con más protagonismo y participación en el armado de estrategias, y ocupando cargos superiores como el que ejerzo. Seguramente muchas de estas mujeres van a seguir el mismo camino", aseguró Chimentti.

**Los números**

De acuerdo al último informe realizado por la subsecretaría de Formación y Capacitación, que depende del Ministerio de Seguridad, para la carrera de Policía en 2018 se postularon poco más de 4 mil personas e ingresaron solo 635 hombres y por decisión política ninguna mujer; mientras que para este año se estima que de los más de 12 mil postulantes que hubo ingresen 1660, 920 mujeres y 740 hombres.

**El Isep**

El Instituto de Seguridad Pública (ISeP) es una herramienta esencial para seleccionar y formar a los aspirantes a ingresar a la Policía de la provincia, y para capacitar y actualizar al personal policial que integra la fuerza.

Cuenta con dos delegaciones, centro-norte y sur, con sede en Santa Fe y Rosario respectivamente, y próximamente se inaugurará una tercera en la localidad de Murphy con una capacidad para 200 alumnos. El funcionamiento de una institución de este tipo en el sur provincial permitirá generar recursos humanos con vínculo de arraigo en la región, mejorando las condiciones laborales de los efectivos policiales que hoy en día son provenientes, en su mayoría, del norte santafesino.