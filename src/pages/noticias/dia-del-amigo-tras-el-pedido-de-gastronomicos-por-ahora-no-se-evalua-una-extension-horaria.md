---
category: La Ciudad
date: 2021-07-15T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMIGOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Día del Amigo: tras el pedido de gastronómicos, "por ahora" no se evalúa
  una extensión horaria'
title: 'Día del Amigo: tras el pedido de gastronómicos, "por ahora" no se evalúa una
  extensión horaria'
entradilla: 'Empresarios gastronómicos de la ciudad solicitaron extender el horario
  de cierre hasta la medianoche el próximo martes 20. '

---
A raíz de las celebraciones por el Día del Amigo, los empresarios gastronómicos de la ciudad solicitaron una extensión horaria del cierre de sus comercios hasta la medianoche el próximo martes 20.

Lo hicieron a través de una nota dirigida al ministro de Trabajo de la provincia, Juan Manuel Pusineri, y a la cual tuvo acceso UNO Santa Fe. En la misma, le piden "con carácter de urgencia la extensión del horario de trabajo" para esa fecha.

Consultado por este medio, el titular de la cartera de trabajo fue contundente respecto al pedido de los empresarios gastronómicos: "Por ahora no se está evaluando ninguna modificación en las medidas de convivencia vigentes".

En la actualidad, el rubro solo puede funcionar de lunes a jueves hasta las 21, y viernes y sábados hasta las 23. En la misiva, los referentes de los rubros, fundamentan: "Este pedido se debe a que las personas se reunirán en sus domicilios particulares teniendo en cuenta la restricción horaria de atención de los establecimientos gastronómicos".

En esa línea, continúan: "Creemos que con en este requerimiento contribuimos a ayudar en esta situación ya que somos parte de la solución".

La nota en relación a los festejos por el Día del Amigo, que lleva la firma de los presidentes de la Cámara de Restaurantes, Joaquín Echague, y de Bares, Agustín Macinsky, agrega que el sector realizará "con la municipalidad de Santa Fe una campaña de concientización del cumplimiento de los protocolos".

"A la espera de una respuesta" sobre la extensión horaria, la nota finaliza: "Nos parece apropiado el horario de 18 a 0 sería el más conveniente por lo mencionado con anterioridad".