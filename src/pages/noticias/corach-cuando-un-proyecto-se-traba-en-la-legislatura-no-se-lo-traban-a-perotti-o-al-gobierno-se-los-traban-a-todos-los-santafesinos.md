---
category: Estado Real
date: 2021-01-20T09:23:50Z
thumbnail: https://assets.3dnoticias.com.ar/corach.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Corach: “Cuando un proyecto se traba en la Legislatura no se lo traban a
  Perotti o al gobierno, se los traban a todos los santafesinos”'
title: 'Corach: “Cuando un proyecto se traba en la Legislatura no se lo traban a Perotti
  o al gobierno, se los traban a todos los santafesinos”'
entradilla: El nuevo ministro de Gestión Pública brindó declaraciones sobre su rol
  y la continuidad de los proyectos estratégicos que lleva adelante el gobierno provincial.

---
Marcos Corach, luego de asumir como ministro de Gestión Pública, brindó este martes declaraciones sobre el rol que ocupará dentro del gabinete de ministros y la continuidad de los proyectos estratégicos que lleva adelante el gobierno provincial. “No se trata de un nuevo gobierno sino que es una continuidad ya que vamos a seguir trabajando en lo que el gobernador ha definido como proyectos estratégicos desde el día que asumió”, expresó Corach.

Consultado sobre las críticas de la oposición por la falta de diálogo con la Legislatura, el ministro replicó que “no faltó diálogo, los vínculos existieron y también existió de nuestra parte mucho trabajo. Asumí hace seis meses en la Secretaría de Articulación de Políticas Públicas y dialogué con la Legislatura pero los diálogos no son exclusivos con el Poder Legislativo, también construimos vínculos con el Poder Judicial, con las instituciones y los ciudadanos de a pie y, de ahora en adelante, vamos a profundizar los diálogos con todos”.

Del mismo modo, remarcó que cuando un proyecto se traba en la Legislatura “no se lo traban a Perotti o al gobierno, se los traban a la provincia y esto redunda directamente en la calidad de vida de cada uno de los ciudadanos. Vamos a trabajar para mejorar la vida de todos y haremos lo que tengamos que hacer, trataremos de destrabar estos proyectos de ley, veremos cuáles son los mecanismos; se pondrá toda la creatividad para resolver esta situación que afecta a la provincia”.

Asimismo, Corach aseguró que “tengo un mandato que me ha dado el gobernador y que tenemos todos sus funcionarios, y que en lo personal asumo con enorme convicción: la connivencia entre la política y el delito debe encontrar en nosotros un límite. Ya han sido muchos años de destrucción de lo público a causa de este flagelo de mezclar a las mafias con los gobiernos”.

“Santa Fe tiene un potencial enorme. Lo sabemos cada uno de los habitantes de su suelo. Los que estamos en cargos que nos asignan una responsabilidad mayor tenemos que lograr que todo eso que es potencial se vuelva efectivo y que podamos avanzar juntos hacia el futuro que soñamos”, remarcó el nuevo ministro de Gestión Pública.

**PROYECTOS DE CONECTIVIDAD Y SEGURIDAD**

Sobre el proyecto de conectividad, Corach detalló que “vamos a pasar las respuestas que requieren los legisladores por escrito, como lo solicitó la oposición, y después va a ir un funcionario a la Legislatura para seguir avanzando en este proyecto tan importante”.

Finalmente, el ministro se refirió a los tres proyectos de seguridad que están parados en la Legislatura. “Queremos que se muevan y vamos a hacer todo lo que esté a nuestro alcance para que esto pase pero también esperamos una correspondencia. Estos proyectos han sido discutidos por el ministro Sain y sus funcionarios con universidades, nos reclaman que hay falta de diálogo pero nosotros hemos dialogado y trabajado estas leyes con un montón de instituciones”.