---
category: La Ciudad
date: 2021-10-23T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/santafack.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Alertan sobre el crecimiento desordenado de la ciudad: "Falta una planificación"'
title: 'Alertan sobre el crecimiento desordenado de la ciudad: "Falta una planificación"'
entradilla: El decano de la Facultad de Arquitectura, Diseño y Urbanismo reflexionó
  sobre el crecimiento sin planeamiento de la ciudad y evaluó los aportes de la carrera
  de arquitectura en Santa Fe

---
Según los expertos de arquitectura y urbanismo, la Ciudad de Santa Fe atraviesa una problemática común a otras de Sudamérica: el crecimiento desordenado y la falta de planificación en su diseño.

En diálogo con el programa Ahí Vamos (de 9 a 12 por FM 106.3 La Radio de UNO) el decano de la Facultad de Arquitectura, Diseño y Urbanismo (FADU - UNL), Arq. Sergio Cosentino, explicó cómo sin planificación urbanística, el mercado inmobiliario va generando la oferta a partir de la extensión desordenada de las ciudades.

“Nosotros en Santa Fe somos testigos de cómo la ciudad crece hacia el norte, de forma indefinida parece, y ya se funde con con otros municipios como en el caso de Recreo, o escapa por la Ruta 1”, dijo el especialista, refiriéndose a la ciudad capital como una “mancha” que se extiende hasta Colastiné y Rincón.

Según el decano de la FADU, constituye un fenómeno característico de ciudades latinoamericanas. “La verdad es que falta una planificación, una organización, faltan organismos que trabajen en torno a esta problemática”, opinó.

En esta línea, dio la bienvenida al nuevo Código de Preservación del Patrimonio Urbano Arquitectónico y al Código de Habitabilidad sancionados por el Concejo de Santa Fe, ya que la legislación anterior databa de 1976, en plena dictadura militar.

Y dio un ejemplo de las consecuencias del crecimiento desordenado: “La inundación del 2003 nos puso de frente a un problema que es que gran parte de la Ciudad Santa Fe eran terrenos inundables, con una idea artificial de contención en la avenida Circunvalación oeste. Una pequeña brecha en esa circunvalación terminó inundando un tercio de la ciudad y dentro de ese tercio de la ciudad había industrias y, ni más ni menos, un hospital de niños”.

Cosentino explicó que existe una puja de intereses entre la academia y los desarrollos inmobiliarios frente a la creación de viviendas, ya que sobre todo la universidad pública, que se financia con ingresos públicos, tiene como objetivo fomentar el principio de equidad e igualdad en el ciudadano.

“El concepto que nosotros manejamos de ciudad corresponde a teorías que buscan generar el acceso a los bienes de la ciudad, y ese es el contrapunto entre los otros agentes que intervienen en el territorio, que es la especulación inmobiliaria. A veces la falta de buenas políticas públicas terminan concentrando mucho más el desarrollo del urbano en áreas consolidadas o desarrollan planes riesgoso”, dijo el decano.

En este sentido, destacó que desde hace tiempo la carrera en Santa Fe es de Arquitectura y Urbanismo, por lo que ha consolidado el área de conocimiento dentro del plan de estudio y se han desarrollado centros de investigación específicos, con una consciencia prácticamente homogénea entre estudiantes y profesionales de, por ejemplo, la metropolización de las ciudades.

“Creemos que estamos trabajando en torno a que esto se haga carne en la generación no solo de los arquitectos que trabajan profesionalmente, sino también en un montón de arquitectos que se van incorporando en las oficinas técnicas del estado”, expresó.

Según el directivo, hay cerca de 2.500 estudiantes de arquitectura y aproximadamente 400 docentes. Ya se han graduado más de 3 mil arquitectos en la Universidad Nacional del Litoral.

“Es un eje principalísimo en la formación el tema de ciudades sostenibles, con acceso para todos los ciudadanos de los beneficios de la vida en la ciudad. Esto es: espacio público, las áreas centrales”, afirmó el arquitecto.

Con respecto a la pandemia, dijo: “Es muy raro lo que pasa porque se dice que la universidad está cerrada, pero en realidad lo que hizo la UNL es migrar a formatos virtuales de enseñanza y eso generó, paradójicamente, la condición de poder retener a muchos estudiantes”.

“Con la enseñanza a distancia mucha gente que por la crisis económica podría haber abandonado los estudios pudo continuarlos en el ámbito de la virtualidad”, destacó.

Finalmente, anunció que la facultad está en proceso de recuperar presencialidad, y que si se mantienen los indicadores de antes de la cuarentena, promediándolos con el índice de asistencia actual, deberían estar ingresando a la facultad en 2022 unos 900 estudiantes, calculando que de la carrera de arquitectura serían aproximadamente 500.