---
category: Deportes
date: 2021-01-13T07:14:38Z
thumbnail: https://assets.3dnoticias.com.ar/colon-talleres.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Ganó, pero no le alcanzó
title: Ganó, pero no le alcanzó
entradilla: Talleres gritó victoria en Santa Fe tras vencer 2-1 a Colón, pero no le
  alcanzó para jugar la final de la Copa Diego Maradona. La victoria de Banfield (4-1)
  ante San Lorenzo, lo dejó fuera de carrera.

---
En un compromiso parejo y pesar del temporal, el sabalero se enfrentó con Talleres de Córdoba en el marco de la jornada 5 de la Copa Diego Armando Maradona 2020.

El marcador se abrió a los 32 minutos con un tanto que, sin duda, será uno de los mejores del torneo argentino. Centro al área de **Pochettino** y **Diego Valoyes** de volea, la clavó en el arco de Colón. Ni bien arrancó el segundo tiempo, **Talleres** volvió a golpear. El tiro de esquina **Nahuel Tenaglia** aumentó la cuenta para Talleres en Santa Fe.

Casi diez minutos más tarde (58), Colón le puso emoción al partido. **Tomás Sandoval** anotó el descuento para el «sabalero» y posteriormente dejó escapar claras oportunidades para empatar el partido.

Al final, **Talleres** mantuvo su ventaja, pero al conocer la victoria de Banfield sobre San Lorenzo, sus jugadores no tuvieron otra opción más que retirarse resignados a sus vestuarios.

Colón, debe mejorar su efectividad para evitar que se escapen los puntos en casa.

# **🔵⚪ Formación de Talleres**

Marcos Díaz; Hincapie, Nahuel Tenaglia, Rafael Pérez y Enzo Díaz; Federico Navarro e Ignacio Méndez; Franco Fragapane, Tomás Pochettino y Diego Valoyes; Carlos Auzqui.

# 🔴⚫ **Formación de Colón**

Leonardo Burián; Facundo Garcés, Bruno Bianchi y Gonzalo Piovi; Alex Vigo, Rodrigo Aliendro, Federico Lértora, Goez y Gonzalo Escobar; Chancalay y Luis Miguel Rodríguez.