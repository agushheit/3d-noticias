---
category: Estado Real
date: 2021-04-28T08:44:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/cuidadores.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia abrió la inscripción para la capacitación online en buenas prácticas
  sobre el uso de medicamentos
title: La provincia abrió la inscripción para la capacitación online en buenas prácticas
  sobre el uso de medicamentos
entradilla: Destinado al personal técnico de residencias para personas mayores de
  corta y larga estadía.

---
El Ministerio de Desarrollo Social, a través de la Dirección Provincial de Personas Mayores, abrió la inscripción para la capacitación online "Buenas Prácticas en Almacenamiento de Medicamentos" destinada a trabajadores y trabajadoras de residencias públicas y privadas de todo el territorio provincial.

Al respecto, el ministro de Desarrollo Social, Danilo Capitani, expresó: “Las personas mayores son un eje central en nuestro gobierno provincial. Por eso, como Estado los acompañamos con políticas activas que llevamos adelante con mucha presencia en el territorio, con programas y capacitaciones para que nuestros trabajadores tengan más y mejores herramientas para su atención y cuidado”.

Por su parte, la directora Provincial de Personas Mayores, Lucía Billoud, indicó: “La capacitación está destinada a las áreas técnicas del sector enfermería que realizan esta tarea específica en las residencias de corta y larga estadía de personas mayores, públicas provinciales y municipales/comunales, y privadas con y sin fines de lucro de todo el territorio provincial”.

“El objetivo es formar a personal capacitado en el manejo y almacenamiento de medicamentos y en brindar información clara a los residentes para que tengan el pleno conocimiento del tratamiento farmacológico que llevan a cabo en miras a mejorar su calidad de vida”, concluyó.

**FORMAR PARA EL CUIDADO**

La capacitación estará a cargo de el Colegio de Farmacéuticos y abordará  diferentes temáticas: envejecimiento; derecho a la información y el consentimiento informado; uso seguro de medicamentos; efectos del fármaco; cambios fisiológicos de las personas mayores; pluripatología; polifarmacia; reacciones adversas; medicamentos potencialmente inapropiados y adherencia al tratamiento.

Además, se desarrollará una Guía de Autocuidado destinada a las personas mayores, con el objetivo de brindar herramientas para que puedan informarse de forma adecuada sobre sus tratamientos.

La modalidad de cursado es virtual y se realizará a través de la plataforma meet. Se realizarán 24 encuentros para participantes de todas las localidades con un cupo máximo por fecha de 100 participantes.

Los interesados podrán inscribirse hasta el 16/5 ingresando aquí: [https://forms.gle/GMpEVWXEnDywwxbv8](https://forms.gle/GMpEVWXEnDywwxbv8 "https://forms.gle/GMpEVWXEnDywwxbv8")