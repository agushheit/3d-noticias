---
layout: Noticia con imagen
author: 3DNoticias
resumen: Aguas Santafesinas
category: Agenda Ciudadana
title: Aguas Santafesinas solicita hacer un uso responsable del agua potable
entradilla: Es necesario evitar derroches innecesarios de agua.
date: 2020-11-24T12:14:01.516Z
thumbnail: https://assets.3dnoticias.com.ar/assa.jpg
---
**Aguas Santafesinas recuerda la necesidad de realizar un uso responsable y solidario del agua potable**, ante la ola de calor que se refleja en una alta demanda de este servicio esencial.

La empresa está trabajando con toda la capacidad de sus sistemas de captación, potabilización y distribución de agua potable. Por eso **es necesario evitar derroches innecesarios**.

### **PILETAS DE LONA**

Se debe evitar la renovación del agua de las piletas de esparcimiento en forma innecesaria.

Una pileta de lona llena de 5.000 litros equivale al consumo diario de agua potable de 25 personas. Con la dosificación diaria de hipoclorito de sodio (lavandina) o con una pastilla de cloro sólido podemos conservarla en adecuado estado por varios días (ver infografía adjunta).

También se debe lavar los pies antes de ingresar a la pileta, retirar la basura de la superficie y cubrirla con una lona o plástico mientras no se use para evitar el ingreso de polvo u hojas que afectan el estado de conservación del agua.

### **BUENAS PRÁCTICAS**

* No utilizar el agua potable en actividades que pueden postergarse, en particular las que demandan importante cantidad de agua: lavado de autos y veredas, regado de jardines, lavarropas, llenado o renovación del agua de piletas de esparcimiento.
* Una manguera con salida continua de agua gasta 500 litros por hora: evitar su uso.
* Sólo lavar las veredas los días y horarios autorizados por la Municipalidad. Utilizar baldes o mangueras provistas de sistemas de corte (gatillo o interruptores o pulsadores) para evitar el derroche. Tampoco lavar vehículos ni arrojar aguas servidas a la vía pública.
* No se permiten las bombas “chupadoras” conectadas en forma directa a la red, que causan riesgos y perjuicios directamente a las cañerías de sus instalaciones internas y de sus vecinos.
* No se debe dejar que el agua corra innecesariamente al lavar los platos, al lavarse los dientes o al bañarse, una ducha de 10 minutos consume 80 litros de agua.
* Controlar las pérdidas en canillas, tanques de agua y otras instalaciones, un depósito de inodoro con deficiencias desperdicia 4.500 litros por día.
* En caso de localizar una fuga de agua potable en la vía pública, los usuarios pueden notificarla a través del **Centro de Atención 24 horas por Whatsapp: +54 341 695-0008**.