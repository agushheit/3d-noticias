---
category: Agenda Ciudadana
date: 2021-08-05T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/COMBINETABIS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Se podrá combinar Sputnik con Moderna y AstraZeneca para completar el esquema
  vacunatorio
title: Se podrá combinar Sputnik con Moderna y AstraZeneca para completar el esquema
  vacunatorio
entradilla: La ministra de Salud aseguró que Argentina está "en condiciones de avanzar
  en las 24 jurisdicciones para intercambiar diferentes vacunas" para completar el
  esquema de inmunización.

---
La ministra de Salud, Carla Vizzotti, anunció que Argentina está "en condiciones de avanzar en las 24 jurisdicciones para intercambiar diferentes vacunas, empezando con Sputnik V con Moderna y AstraZeneca", para completar el esquema de inmunización de quienes aún no hayan recibido la segunda dosis contra el coronavirus.

Lo indicó al presentar en la Casa de Gobierno los resultados del estudio colaborativo de combinación de vacunas contra la Covid-19.

**La combinación de vacunas**

Vizzotti dijo que serán priorizadas para recibir la segunda dosis las personas que lleven más tiempo de espera desde que recibieron la primera y aquellas que tengan más de 50 años y con comorbilidades para recibir las primeras combinaciones de inmunizadores.

Sostuvo además que, según un estudio en el Reino Unido de Gran Bretaña, la combinación de Astrazeneca y Moderna "es una posibilidad".

La ministra detalló que "la combinación de vacunas será opcional" para quienes hayan recibido la primera dosis de la Sputnik V y estén aguardando la segunda, y ratificó que la idea es que "agosto es el mes de la segunda dosis" para completar los esquemas de inmunización de toda la población.

También remarcó que "está asegurada" la cobertura en Argentina del 60% de personas mayores de 50 años vacunadas con dos dosis de fármacos contra el coronavirus, de acuerdo a la proyección que establece que se necesitan 2,3 millones de dosis para alcanzar ese objetivo, y agregó: "Tenemos la aspiración de superar esa marca".

Además, puntualizó que el millón y medio de vacunas del laboratorio Moderna llegado al país empezará a distribuirse este jueves a cada una de las 24 jurisdicciones para "iniciar el aceleramiento" del plan de inmunización, con la posibilidad de ser combinadas, y que la semana que viene se hará lo propio con un millón de dosis del laboratorio Richmond.

También adelantó que sigue llegando "la sustancia activa para el componente 2" de Sputnik V, y otros ocho millones de dosis de Sinopharm en agosto.

La funcionaria pidió continuar "manteniendo los esfuerzos" para atrasar "lo más posible" la circulación comunitaria de la variante Delta de coronavirus, aunque aclaró que todavía "no es predominante en Argentina".

Vizzotti recordó que se recomienda "desestimar viajes a lugares donde haya circulación viral, sobre todo de la variante Delta", en un momento en el que en la Argentina se está desacelerando el crecimiento de casos de coronavirus y avanzando en la vacunación de la población.

La funcionaria explicó que la Argentina "trabaja en la evidencia para el país y la región", a través de un estudio colaborativo con la Federación Rusa, y que se implementó en la Ciudad de Buenos Aires, y en las provincias de Buenos Aires, La Rioja, San Luis y Córdoba.

El estudio "es un protocolo multicéntrico, una combinación de muchas posibilidades, con distintas dosis, en la cual se incorpora a Moderna a este estudio y se sumará a Cansino, para generar evidencia en ese sentido", analizó Vizzotti, quien señaló que tuvo el aval del Comité de expertos y del Consejo Federal de Salud.

"Por los anticuerpos que genera", la ministra calificó al estudio de "satisfactorio" por los resultados "alentadores", y anticipó que esa prueba "sigue adelante, con 1.800 sujetos voluntarios, para generar evidencia y un sistema robusto" en ese sentido.

Vizzotti aclaró que los contratos con los distintos laboratorios "son estimativos" y contó que ninguno de ellos "cumplió con el cronograma, no por falta de voluntad, sino haciendo un enorme esfuerzo para mandar a todos los países".

Descartó por el momento la posibilidad de aplicar una tercera dosis, como hacen varias regiones, debido a "que inclusive con una dosis disminuyeron las hospitalizaciones y las muertes", con diez semanas consecutivas de baja de casos positivos y ocho semanas de menos decesos.

En cuanto al cupo de argentinos o residentes que están en el exterior y quieran regresar advirtió que se determinará a través del próximo Decreto de Necesidad y Urgencia y la Decisión Administrativa, atendiendo como "muy relevante la variante Delta, que no es predominante en la Argentina y se trabaja para atrasar lo más posible".

Por su parte, el ministro de Salud bonaerense, Nicolás Kreplak, celebró "la cooperación y el trabajo multicéntrico que nos permite tomar decisiones de buena calidad", que "son confiables y además aportan al mundo".

Y refirió que los estudios realizados sobre la efectividad de las vacunas que se aplican en Argentina no mostraron "ningún episodio de efectos adversos", en tanto que la combinación de dos laboratorios para la primera y segunda dosis en los voluntarios tuvieron "la misma eficacia" que si se hubiese dado dos dosis de la misma inmunización.

Kreplak informó que este jueves comunicarán las posibilidades de "optar con el componente homólogo o con la combinación", y remarcó que con las distintas vacunas que se aplican en el país "no hay evidencia que con el paso del tiempo la eficacia de la vacuna caiga" para desestimar también por el momento la posibilidad de aplicar una tercera dosis.

En tanto, su par porteño, Fernán Quirós, se pronunció en el mismo sentido al destacar la "experiencia local para que todas las jurisdicciones" tomen "decisiones en base a la esa experiencia" y "vincularla con la internacional, con datos reales", para saber "cuales son las vacunas que se pueden intercambiar.

Tras anunciar que la ciudad de Buenos Aires comunicará eso "mañana temprano", Quirós advirtió que Sinopharm "no está incluida porque los datos no son concluyentes" y la semana próxima la Capital Federal "tendrá los resultados finales".

El jefe de Gabinete, Santiago Cafiero, encabezó una reunión con los ministros, en su despacho de Casa Rosada, donde evaluaron previamente los resultados del estudio coordinado por el Ministerio de Salud y el Consejo Nacional de Investigaciones Científicas y Técnicas (Conicet), dependiente del Ministerio de Ciencia, Tecnología e Innovación.