---
category: La Ciudad
date: 2021-05-07T08:32:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/ines-carlitos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ines Larriera
resumen: "“El intendente no previó cómo garantizar el funcionamiento del Sistema Municipal
  de Educación”"
title: "“El intendente no previó cómo garantizar el funcionamiento del Sistema Municipal
  de Educación”"
entradilla: "Así lo afirmaron los concejales Inés Larriera y Carlos Pereira del bloque
  UCR-Juntos por el Cambio. \n"

---
Así lo afirmaron los concejales Inés Larriera y Carlos Pereira del bloque UCR-Juntos por el Cambio. “Hoy los Jardines Municipales están cerrados porque no se gestionó la inclusión de las docentes en el Plan de Vacunación de provincia, ni se previeron las dificultades que iba a enfrentar la educación en esta segunda ola”, indicaron los ediles. 

El Concejo Municipal aprobó un pedido de informe de los concejales del bloque UCR-Juntos por el Cambio, Inés Larriera y Carlos Pereira, que solicita conocer las gestiones realizadas por el intendente para garantizar el funcionamiento del Sistema Municipal de Educación. “Concretamente queremos saber qué acciones y diligencias realizó el mandatario municipal en los últimos 90 días para garantizar la incorporación al Plan de Vacunación de provincia a los trabajadores y trabajadores de los Jardines Municipales y el Liceo Municipal, como así también los jardines de gestión privada”, detallaron los ediles. 

Los concejales recordaron que los Jardines Municipales nacieron con el objetivo de brindarles igualdad de oportunidades en el acceso a la educación a los niños y niñas de los barrios más vulnerables de la ciudad. “Ese noble objetivo hoy no se cumple, porque los chicos que asisten a jardines de gestión privada siguen asistiendo a clases; mientras que los niños de los jardines municipales no pueden hacerlo, por culpa de la falta de previsión del intendente”, sostuvieron los legisladores. 

En la misma línea, Inés Larriera y Carlos Pereira señalaron; “Desde Juntos por el Cambio venimos defendiendo la educación presencial, porque estamos convencidos de que en la educación está futuro. Por eso, nos preocupa la falta de previsión y garantías en este tema por parte del intendente, más que nada teniendo en cuenta de quienes más necesitan de este vínculo con los Jardines Municipales son niños y niñas que viven en condiciones de vulnerabilidad”.