---
category: La Ciudad
date: 2021-12-24T06:15:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/callerecreativa1jpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Las costaneras se convertirán en peatonal este 24 por la noche
title: Las costaneras se convertirán en peatonal este 24 por la noche
entradilla: La Este en su totalidad y la Oeste desde las letras al faro serán vedadas
  para el tránsito desde las 22 horas.

---

Desde la Secretaría de Control y Convivencia Ciudadana informaron como será el operativo de tránsito para este 24 y 25 de diciembre en la capital santafesina.

 “El sector de Costanera Este, de paradores, va a estar vedado para el tránsito a partir de las 22 horas del día 24. El Puente Colgante será peatonal. La Costanera Oeste, estará cortado desde las letras hasta el faro” señaló Fernando Peverengo, Secretario de Control de la Municipalidad.

 Cabe destacar, que más de 60 trabajadores municipales estarán abocados a los diferentes operativos, y que contará también con la colaboración de la policía.

 Además, se recuerda que habrá controles de alcoholemia dinámicos en los lugares de mayor concentración de personas que comenzarán a la 1 de la madrugada y finalizarán a las 8.

 **Pirotecnia 0**

 Por otra parte, Peverengo se refirió a la campaña para evitar la pirotecnia en la ciudad “Empezamos con los operativos, se hace en conjunto con la brigada de explosivos de la URI y también vamos a hacerlo también para fin de año. Vamos a trabajarlo preventivamente, concientizando a la gente”.