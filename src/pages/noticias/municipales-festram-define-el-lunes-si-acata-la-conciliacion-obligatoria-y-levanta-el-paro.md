---
category: La Ciudad
date: 2021-08-22T06:00:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/FESTRAM2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Municipales: Festram define el lunes si acata la conciliación obligatoria
  y levanta el paro'
title: 'Municipales: Festram define el lunes si acata la conciliación obligatoria
  y levanta el paro'
entradilla: El Ministerio de Trabajo convocó a las partes para el próximo martes a
  las 10 mediante videoconferencia. El sindicato había resuelto un paro de 48 hs para
  martes y miércoles. Qué pasará en Santa Fe.

---
El Ministerio de Trabajo de la provincia dictó la conciliación obligatoria para desactivar el paro de 48 horas para el martes y miércoles de la semana que viene por la Federación Santafesina de Trabajadores Municipales (Festram). El sindicato pretende abrir la mesa de discusión salarial ante el avance de la inflación en el poder adquisitivo de los trabajadores.

La Festram había ratificado la huelga anunciada la semana pasada ante la falta de respuestas de los intendentes y presidente comunales de la provincia. En rigor, los municipales de toda la provincia solicitan adelantar la última cuota del 35% acordado en la última negociación paritaria de abril.

Claudio Leoni, secretario General de la Festram, le dijo a UNO Santa Fe que el lunes se reunirán (en forma virtual) los secretarios generales de los sindicatos municipales de la provincia para definir si acatan o no el llamado a conciliación.

Cabe recordar que, en esta capital, el gremio Asoem no se sumó a la protesta aduciendo que no negocia más incrementos salariales en la mesa paritaria provincial. "Lo hacemos a través de las Mesas Paritarias Locales", dijo una voz gremial a UNO.

Con respecto al llamado de conciliación obligatoria, desde la cartera laboral provincial indicaron que "la decisión tiene por fundamento la necesidad de retomar con normalidad los servicios municipales que fueron suspendidos durante el lapso de implementación de la medida de fuerza, los cuales han sido declarados esenciales en un contexto de pandemia mundial".

De esa manera aclararon que los municipios y comunas "están cumpliendo con el acuerdo salarial actualmente vigente hasta el mes de noviembre del presente año".

Asimismo, el Ministerio convocó a las partes involucradas a una audiencia conciliatoria a realizarse el martes próximo, a las 10, que se llevará adelante mediante sistema de videoconferencia por razones vinculadas a la pandemia de coronavirus.