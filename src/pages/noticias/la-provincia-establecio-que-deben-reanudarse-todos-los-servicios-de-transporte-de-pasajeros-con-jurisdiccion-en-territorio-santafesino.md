---
category: Estado Real
date: 2021-02-09T08:31:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/unnamed.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia estableció que deben reanudarse todos los servicios de transporte
  de pasajeros con jurisdicción en territorio santafesino
title: La provincia estableció que deben reanudarse todos los servicios de transporte
  de pasajeros con jurisdicción en territorio santafesino
entradilla: El objetivo de esta medida es garantizar que ninguna localidad quede sin
  conectividad.

---
La secretaría de Transporte, dependiente del Ministerio de Producción, Ciencia y Tecnología, a partir de la Resolución 03/2021, determinó que a partir de este lunes todos los servicios de transporte automotor de pasajeros de jurisdicción provincial deben reanudarse y estableció una serie de pautas a cumplir para volver a prestar el servicio.

Al respecto, el director Provincial de Transporte de Pasajeros, Joaquín Sánchez destacó: ”Si bien será con horarios reducidos para garantizar que se cumplan las medidas de seguridad sanitaria en el marco de la pandemia, el objetivo de esta medida es garantizar que ninguna localidad que contaba con servicio de transporte de pasajeros previo a la emergencia sanitaria quede sin conectividad”.

**Las pautas que establece la Resolución son los siguientes:**

>>Los horarios a cumplir serán esquemas equivalentes a frecuencias y programaciones de días sábados o medios festivos aprobados oportunamente previo a la pandemia. Los mismos podrán ajustarse de acuerdo a la demanda de los servicios, no pudiendo en ningún caso superar ni modificar aquellos actualmente autorizados para cada línea.

>>Los Concesionarios podrán solicitar la reducción parcial del servicio, cuando la demanda lo justifique y conforme a lo establecido en el artículo 37º de la Ley Nº 2499.

>>Los horarios de los servicios se programarán tomando en cuenta las tasas de ocupación de las distintas terminales de origen y destino, procurando no generar aglomeraciones de personas al ascender y descender de las unidades y en la evacuación de las terminales. De manera previa a cada modificación, las empresas deberán presentar dichos esquemas en la Secretaría de Transporte del Ministerio de Producción, Ciencia y Tecnología.

>>Los vehículos de transporte automotor interurbano de pasajeros deberán limitar su capacidad de ocupación de acuerdo con el protocolo aprobado por la Secretaría de Transporte del Ministerio de Producción, Ciencia y Tecnología.

La mencionada Resolución también indica que, tal como lo establece la normativa vigente, las empresas permisionarias deberán informar a dicha Secretaría, los esquemas de horarios a prestar teniendo en cuenta las pautas establecidas y toda modificación a los mismos con la debida antelación.

Asimismo, cuando por razones especiales y/o debidamente fundadas soliciten reducciones parciales de los servicios, deberán realizarlo conforme lo establece el artículo 37º de la Ley Nº 2499, acompañando información estadística que avale la solicitud, para su análisis y aprobación. No podrán dejar de prestar los servicios en ningún caso, sin la debida aprobación por parte de esta Secretaría. Además, deberán remitir semanalmente, por línea y por horario a través de una planilla.