---
category: La Ciudad
date: 2021-07-31T06:15:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/REDONDA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El operativo de vacunación en la ciudad de Santa Fe se realizará solo en
  dos vacunatorios
title: El operativo de vacunación en la ciudad de Santa Fe se realizará solo en dos
  vacunatorios
entradilla: En total son más de 2.000 los turnos otorgados este viernes en la ciudad
  de Santa Fe para vacunar en las instalaciones del CEF Nº29. También se inoculará
  en el Cemafe y ex Iturraspe.

---
Este viernes 30 de julio y según se informó desde el ministerio de Salud de la provincia, se otorgaron 2.165 turnos para vacunación en la ciudad de Santa Fe, según el siguiente detalle:

\-CEF Nº 29: 2.165 turnos para primeras dosis Sinopharm 8 a 16.30

Además, se sigue con la inoculación en el Cemafe, en el exIturraspe y los centros de salud barriales.

**Centros de testeos**

En la ciudad de Santa Fe hay seis los puestos fijos de testeo masivo contra los coronavirus ubicados en distintos barrios y que atienden de lunes a lunes de 9 a 13. Se puede asistir sin turno previo y presentando uno o más síntomas compatibles con coronavirus o habiendo sido contacto de casos confirmados.

Están ubicados en:

\-Estación Belgrano

\-Estación Mitre

\-El Alero de Coronel Dorrego

\-Playón Municipal de Villa Hipódromo (Cassanello 4200)

\-Cemafe (de 8.30 a 12.30)

\- ExIturraspe

**Dónde anotarse o reprogramar un turno**

Por último, recordar que para inscripciones como para el control de la asignación de turnos se puede realizar en el sitio web de la provincia Santa Fe Vacuna. Asimismo, quienes no puedan hacerlo allí por diversas razones, pueden canalizarlo consultando al 0800 555 6549 o en el centro de salud más próximo a su domicilio.

**Qué pasa si perdí el carné de vacunación**

También si llegó el momento de la segunda dosis y se extravió el carné de vacunación, se le pide a la persona, que, en un horario de la tarde, se acerque al centro de vacunación donde le aplicaron la dosis y pida que le rehagan el carné, ya que en ese vacunatorio tienen los datos registrados.