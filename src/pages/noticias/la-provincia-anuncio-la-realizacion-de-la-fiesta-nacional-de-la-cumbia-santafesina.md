---
category: Cultura
date: 2021-11-06T06:15:24-03:00
layout: Noticia con imagen
author: UNO Santa Fe
resumen: La provincia anunció la realización de la Fiesta Nacional de la Cumbia Santafesina
title: La provincia anunció la realización de la Fiesta Nacional de la Cumbia Santafesina
entradilla: 'Se trata del festival que el año pasado fue postergado por la pandemia.
  Anticipan la presencia de más de 20 grupos y artistas. Las fechas del esperado evento.

'
thumbnail: https://assets.3dnoticias.com.ar/CUMBIA.jpg
link_del_video: ''

---
El gobierno de la provincia de Santa Fe anunció la realización de la 5ta. Fiesta Nacional de la Cumbia Santafesina . El mega evento está previsto para los días 3, 4 y 5 de diciembre.

Cabe recordar que el festival fue postergado por la pandemia en la edición 2.020 y este año reunirá a más de 20 grupos y artistas destacados del género musical que identifica a la provincia de Santa Fe.

La confirmación se dio a conocer este viernes, en el marco del Día de la Cumbia, y se presentará oficialmente la próxima semana.

De esta manera, el festival musical que reúne a los grandes exponentes del género musical que caracteriza a la provincia, volverá a concretarse este año, tras un año 2.020 donde tuvo que suspenderse la edición prevista, por las medidas sanitarias vigentes por la pandemia del coronavirus .

Cabe destacar que serán tres noches a pura fiesta, con más de 20 grupos y artistas destacados en vivo, desplegando un evento lleno de emoción y alegría, con la mejor música santafesina para disfrutar en familia.

Desde el 2016, los amantes de la cumbia santafesina dicen presente en la Fiesta Nacional de la Cumbia Santafesina, un evento que reúne a los artistas más representativos del género en un marco espectacular para este ritmo, que tiene por ley su Día de la Cumbia Santafesina todos los 5 de noviembre, fecha en recuerdo de la muerte de Martín Robustiano «Chani» Gutiérrez, uno de los impulsores iniciales y permanentes de la música santafesina desde su disquería y también en la promoción de los jóvenes talentos que hoy son la realidad del movimiento musical cumbiero santafesino.

En las ediciones anteriores, asistieron entre 50 y 60 mil personas en los tres días del festival, y se espera que este año se supere esa cifra, teniendo en cuenta la presencia de cerca de treinta artistas, con una grilla de entre siete y ocho grupos por cada jornada.