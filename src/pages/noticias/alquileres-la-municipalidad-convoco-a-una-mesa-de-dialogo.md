---
category: La Ciudad
date: 2021-10-16T06:00:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALQUILERES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Alquileres: la Municipalidad convocó a una mesa de diálogo'
title: 'Alquileres: la Municipalidad convocó a una mesa de diálogo'
entradilla: El objetivo fue intercambiar opiniones y buscar herramientas que mejoren
  la relación locativa en el marco de la nueva ley nacional de alquileres.

---
La Municipalidad de Santa Fe llevó a cabo la primera mesa de diálogo con los actores involucrados en la temática de alquileres de la ciudad. En la reunión, que tuvo lugar este miércoles en el nuevo espacio de la Dirección de Derechos y Vinculación Ciudadana (ex Predio Ferial, Las Heras 2883), participaron miembros de distintas instituciones y organismos públicos. Uno de los ejes del encuentro giró en torno a las consecuencias que la nueva ley nacional de Alquileres implicó en las relaciones locativas.

Estuvieron presentes el director de Derechos y Vinculación Ciudadana de la Municipalidad, Franco Ponce de León, junto a los responsables del área municipal de inquilinos Martín Benitez y María Fernández; Adriana Garrido, por la Defensoría del Pueblo de la provincia; Pedro Peralta, por el Frente de Inquilinos Nacional; Soledad Ureta Cortés, por la Cámara Inmobiliaria de Santa Fe (Cisfe); Jorge Pighin, en representación del Colegio de Corredores Inmobiliarios; Walter Govone, por la Cámara de Empresas y Corredores Inmobiliarios de la Provincia de Santa Fe (CECI); y Vanina Sturla, por la Facultad de Derecho de la Universidad Católica de Santa Fe.

En la ocasión, se dialogó sobre la aplicación del Índice ICL y los ajustes del precio, renovaciones y prórrogas contractuales, problemática de la vivienda y su disponibilidad, cauciones locativas, impuestos y sellados contractuales, entre otros. Asimismo, de la reunión surgieron propuestas para seguir avanzando en la búsqueda conjunta de soluciones y la realización de una jornada de capacitación sobre “locaciones” que será abierta a la comunidad, entre otras acciones a realizar.

Ponce de León calificó a esta primera reunión como “auspiciosa” y explicó que “el objetivo fue abordar la incidencia de lo que son las locaciones en la vida diaria de los santafesinos”. Destacó la participación de todos los involucrados en la temática de alquileres y dijo que “la idea es continuar con reuniones periódicas para el abordaje colaborativo con los actores de la sociedad civil y del sector privado a fin de avanzar en políticas públicas de promoción y protección de las relaciones locativas”.

El funcionario municipal mencionó que la Oficina Municipal del Inquilino brinda atención diaria a los ciudadanos que así lo requieran y también en cada uno de los barrios que requirieron la presencia de la oficina. Hasta la fecha las consultas ascienden a unas 3 mil.

**Informes**

Los sitios donde dirigirse para realizar consultas son:

– Las Heras 2883 (ex Predio Ferial, ala sur oeste), de lunes a viernes, de 7.15 a 13 hs

– Whatsapp: +54 342-5315450

– Teléfono: 4574119

– Correo: derechosyvinculacionciudadana@santafeciudad.gov.ar

– [Oficina Virtual municipal](https://oficinavirtual.santafeciudad.gov.ar/web/inicioWebc.do?opcion=noreg)