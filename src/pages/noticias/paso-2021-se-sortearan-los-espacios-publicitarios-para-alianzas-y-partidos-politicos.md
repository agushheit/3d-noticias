---
category: Estado Real
date: 2021-08-13T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/sorteo.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Paso 2021: se sortearán los espacios publicitarios para alianzas y partidos
  políticos'
title: 'Paso 2021: se sortearán los espacios publicitarios para alianzas y partidos
  políticos'
entradilla: Será el viernes 13 de agosto, a partir de las 8, en la sala de la Lotería
  de la Provincia ubicada en el Puerto de Santa Fe.

---
Este viernes 13 se realizará el sorteo para determinar los espacios publicitarios destinados a las diferentes alianzas y partidos políticos, cuyos precandidatos y precandidatas competirán en las próximas elecciones Primarias, Abiertas, Simultáneas y Obligatorias (PASO 2021), que tendrán lugar el próximo 12 de septiembre.

Dicho sorteo se desarrollará, a partir de las 8, en la sala de la Caja de Asistencia Social -Lotería de Santa Fe-, ubicada en Francisco Miguens 1976, en el Puerto de Santa Fe y será supervisado por el área notarial del organismo.

Además, estarán presentes autoridades del Tribunal Electoral de la Provincia de Santa Fe y apoderados de las alianzas y partidos políticos, como así también autoridades de la Lotería y de la Secretaría de Comunicación Social.