---
category: La Ciudad
date: 2021-07-22T06:00:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/xtrenesss.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Adjudican por más de 5 mil millones la obra de la circunvalación ferroviaria
  en Santa Fe
title: Adjudican por más de 5 mil millones la obra de la circunvalación ferroviaria
  en Santa Fe
entradilla: 'El proyecto implica la creación de 23 nuevos kilómetros de vías, la recuperación
  de otros 38 kilómetros y un nuevo puente sobre el Río Salado, entre otras obras. '

---
El directorio de Trenes Argentinos Infraestructura (empresa estatal) informó que se logró avanzar con la adjudicación de una obra histórica para los trenes de cargas de la Argentina: se trata del mega plan para circunvalar de manera completa la ciudad de Santa Fe, actualmente atravesada por las líneas del Belgrano Cargas en la unión de cinco provincias con los puertos de exportación. La obra en cuestión será la primera circunvalación ferroviaria en el país para el transporte de mercancías y recorrerá desde el norte de Santo Tomé, en pleno ramal F, hasta la Laguna Paiva, región del ramal C.

Se trata de una inversión de $ 5.170 millones, y la adjudicación quedó a cargo de las firmas Vial Agro S.A, a cargo del Renglón I; la unidad temporal Lemiro Pietroboni S.A – Merco Vial S.A – Sabavisa S.A, con el Renglón II; y la alianza Ferromel S.A – Herso S.A con el Renglón III.

Cada una de las empresas adjudicatarias de cada tramo de los trabajos “ha dado muestras de solvencia y de la capacidad técnica y operativa necesaria para llevar adelante una obra ambiciosa y que escribirá un capítulo nuevo para el transporte de cargas”, indicaron desde Trenes Argentinos. Así, se busca dar un nuevo paso en el plan de Modernización del Transporte Ferroviario que impulsa el Ministerio de Transporte de la Nación y su titular, Alexis Guerrera.

“Esta es una obra histórica. Vamos a generar un cambio en la seguridad, la logística y la optimización de las vías de cargas con un impacto significativo en el desarrollo productivo regional y en la vida de cientos de miles de santafesinos y santafesinas”, celebró el presidente de Trenes Argentinos Infraestructura, Martín Marinucci, luego de la adjudicación de la obra para circunvalar Santa Fe.

En la actualidad, las formaciones atraviesan la ciudad santafesina a 20 kilómetros por hora y la conexión entre el puerto de Timbúes y laguna Paiva se demora diez horas. Luego de la aprobación del directorio de Trenes Argentinos Infraestructura, se proyecta ponerle fin a la recorrida urbana del tren, así evitar su traslado por 62 pasos a nivel en el gran Santa Fe e impactar de lleno en la calidad de vida de cientos de miles de vecinas y vecinos.

En ese sentido, el ministro Guerrera, remarcó: “Estamos dando un paso muy importante en nuestro Plan de Modernización del Transporte, invirtiendo en la infraestructura de una de las zonas con mayor circulación de carga”. En esa línea, sostuvo que “esta obra nos va a permitir crecer exponencialmente en la cantidad de carga transportada, en la cantidad de trenes que operamos, en los tiempos de viaje y, sobre todo, en seguridad, porque el tren ya no va a atravesar la ciudad de Santa Fe ni va a ser una barrera física que la divide. Es un avance enorme para las y los santafesinos y son más oportunidades para las y los productores argentinos”.

**Detalles**

En definitiva, el ambicioso plan contempla desde la creación de 23 nuevos kilómetros de vías, la recuperación de otros 38 kilómetros de vías, un nuevo puente sobre el Río Salado, además de una serie de puentes vehiculares para cruces a distintos niveles, más de medio centenar de alcantarillas y cerramientos para la zona rural y urbana. De esa manera, se busca generar un cambio de paradigma en el transporte de cargas y su impacto en Santa Fe.

Así, con la circunvalación de Santa Fe, se garantizará la seguridad tanto para vehículos y peatones como para los trenes al tiempo que se logrará la integración urbana al poner fin a la interrupción de las formaciones en la traza urbana. Las obras permitirán incrementar el paso de dos a diez trenes de cargas por día e impactar de lleno en la reconstrucción argentina con una mirada federal y de desarrollo regional. Además de incrementar la velocidad de traslado a 65 kilómetros por hora y reducir la demora a 2.30 horas.