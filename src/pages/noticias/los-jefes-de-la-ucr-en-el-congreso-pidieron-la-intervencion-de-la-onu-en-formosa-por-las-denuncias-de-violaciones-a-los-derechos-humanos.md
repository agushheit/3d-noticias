---
category: Agenda Ciudadana
date: 2021-01-27T10:20:56Z
thumbnail: https://assets.3dnoticias.com.ar/formosa.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Infobae
resumen: Los jefes de la UCR en el Congreso pidieron la intervención de la ONU en
  Formosa por las denuncias de violaciones a los derechos humanos
title: Los jefes de la UCR en el Congreso pidieron la intervención de la ONU en Formosa
  por las denuncias de violaciones a los derechos humanos
entradilla: Mario Negri y Luis Naidenoff elevaron una notificación a la Alta Comisionada
  Michele Bachelet para que constate si se vulneraron las garantías básicas en los
  centros de aislamiento

---
Los presidentes de los bloques de senadores y diputados de la UCR, **Luis Naidenoff y Mario Negri,** respectivamente, elevaron este martes un pedido de intervención urgente a la Alta Comisionada de las Naciones Unidas para los Derechos Humanos, Michelle Bachelet, para que **constate si se vulneraron los derechos humanos de las personas en los centros de aislamiento de la provincia de Formosa.**

Ambos legisladores ya habían realizado la semana pasada un pedido de medida cautelar ante la Comisión Interamericana de Derechos Humanos (CIDH) con el objetivo de revertir la situación de las personas que permanecen aisladas en los centros dispuestos por el gobierno de Gildo Insfrán.

Ahora, elevaron el mismo reclamo ante la ex Presidenta chilena, quien **es la máxima autoridad de Naciones Unidas en la defensa de los Derechos Humanos**. La presentación consta de siete páginas en las que se hace un detallado informe de las denuncias de los atropellos que padecen los formoseños a manos del gobierno provincial.

“Estamos ante **medidas de gobierno que no superan el test de razonabilidad exigido por la Constitución argentina y el derecho internaciona**l, ya que impiden que las personas con síntomas leves de COVID-19 o asintomáticas, como así también los contactos estrechos de personas contagiadas, realicen el aislamiento en sus domicilios particulares, desoyendo las recomendaciones de las autoridades sanitarias nacionales e internacionales”, explican en el escrito Naidenoff y Negri.

![Dos concejalas que denunciaron la situación fueron detenidas en la vía pública](https://www.infobae.com/new-resizer/bC8t84c2bhQnAczWH7H8Etg2hLc=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/6LWH7OP3UBBAXGJVVL5X5FQ3JE.jpg)

Según el informe, los centros de aislamiento no cumplen con las “condiciones mínimas de salubridad, higiene y confort”. Además, el estudio asegura que no realizan un efectivo aislamiento entre pacientes contagiados y contactos estrechos que podrían no estar contagiados. Incluso aseguran que muchas personas son obligadas a “permanecer en los CAS sin brindar su consentimiento” y “son trasladados allí compulsivamente por las fuerzas de seguridad”.

En esa línea, señalaron que esta situación “genera temor y angustia” en los habitantes de la provincia “que se resisten a ser llevados a centros de aislamiento que se asemejan, más que a un lugar de contención y cuidado, a un régimen carcelario, con condiciones de hacinamiento evidentes que generan innegables daños a la salud física y psíquica de los aislados, siendo una grave afrenta a la dignidad humana”.

El senador y el diputado también le solicitaron a Bachelet “encaminar las medidas de protección que estuvieren a su alcance para que cese la situación descrita, en particular teniendo en cuenta la posible existencia de violencia institucional por parte del gobierno provincial”.

Esta mañana, el presidente de la UCR de Formosa, Martín Hernández, denunció que en los centros de aislamiento hay menores de edad encerrados tras rejas y difundió un video que también será presentado ante la Justicia.

La primera denuncia por tratos inhumanos y hacinamiento en los centro de aislamiento había sido presentada la semana pasada por la abogada y concejala de Formosa capital Gabriela Neme.

Según su testimonio, en los centros no se alojaba sólo a pacientes con COVID-19, sino también personas que aún no habían recibido el resultado de su hisopado. La letrada, que se comunicó con personas que se encontraban en el lugar, denunció que casi no había distanciamiento entre sanos y contagiados y que hasta compartían el mismo baño.

Luego de esta denuncia, Neme y Celeste Ruiz Díaz fueron detenidas y alojadas durante varias horas en una dependencia policial, algo que fue repudiado tanto por la oposición como por organismos internacionales.