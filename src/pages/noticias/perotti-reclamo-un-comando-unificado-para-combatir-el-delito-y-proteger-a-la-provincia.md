---
category: Agenda Ciudadana
date: 2021-09-21T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/perotti.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Perotti reclamó un "comando unificado" para combatir el delito y proteger
  a la provincia
title: Perotti reclamó un "comando unificado" para combatir el delito y proteger a
  la provincia
entradilla: Lo dijo antes de reunirse con el nuevo ministro de Seguridad, Aníbal Fernández.
  Pidió un comando unificado "para garantizar una operatividad que le dé a los vecinos
  el acompañamiento necesario".

---
El gobernador, Omar Perotti, se manifestó en favor de un comando unificado de Seguridad en la provincia, luego de que en la tarde de este lunes se haya dirigido a Capital Federal para presenciar la jura de los nuevos ministros en Casa Rosada. Durante la tarde de este lunes está prevista una reunión con el recién nombrado ministro de Seguridad de la Nación, Aníbal Fernández.

El gobernador advirtió que los distintos niveles del Estado "no pueden permitir impunidad" ante el narcotráfico y reclamó un "comando unificado" para garantizar medidas de protección en Santa Fe. Lo dijo al reunirse con Aníbal Fernández, para coordinar acciones conjuntas.

"Necesitamos que se entienda que estas situaciones no se dan en cualquier otra provincia", dijo Perotti en declaraciones a la prensa en la Casa Rosada, al alertar sobre el "sicariato" que rige en Santa Fe.

Para el mandatario provincial, "la presencia de las fuerzas federales tiene que ayudar", mientras la administración provincial se dedica a "fortalecer con más uniformados la propia institución" policial.

Perotti abogó por "garantizar la seguridad de todos los que llevan adelante las acciones de desarrollo de un juicio" y remarcó la necesidad de que los distintos sectores de la Justicia tengan la "garantía de estar protegidos".

"No podemos permitir impunidad y que en nuestra propia institución haya habido jefes que se hayan apartado de sus servicios y hayan colaborado con el narcotráfico", planteó Perotti. En esta sintonía, Perotti planteó que la situación en Rosario debe enfrentarse con el "acompañamiento de las fuerzas federales".

Perotti propuso un "comando unificado fuerte" que pueda "garantizar una operatividad que le dé a los vecinos el acompañamiento necesario". Actualmente en Santa Fe operan la Prefectura, Gendarmería y la Policía Federal, según detalló el mandatario provincial.