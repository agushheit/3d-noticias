---
category: Agenda Ciudadana
date: 2021-10-25T06:15:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/PUSINIERI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Nueva reunión paritaria para buscar acuerdos con docentes
title: Nueva reunión paritaria para buscar acuerdos con docentes
entradilla: Será este miércoles y luego de tres días de paro de los maestros agrupados
  en Amsafé tras rechazar el último ofrecimiento del 17% a cobrar en tres tramos.

---
Tras las tres jornadas de paro docente en rechazo a la oferta salarial en las últimas dos semanas, el próximo miércoles el gobierno de Santa Fe mantendrá una nueva reunión paritaria con los representantes de los docentes de escuelas públicas, aunque ya adelantó que no pagará el aumento correspondiente a este mes a los gremios que no aceptaron su propuesta.

Los maestros agrupados en la Asociación del Magisterio de Santa Fe (Amsafé) rechazaron el ofrecimiento del 17% en tres tramos, que sumado al 35% otorgado en marzo, elevó la suba al 52% en el año, y ya realizaron tres jornadas de paro.

En cambio, sí aceptaron la proposición los docentes del Sindicato Argentino de Docentes Particulares (Sadop) y los agentes de la administración central representados por la Asociación Trabajadores del Estado (ATE) y por la Unión del Personal Civil de la Nación (Upcn).

El ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, indicó que el Gobierno ratifica “lo que se vino sosteniendo en forma invariable, en el sentido de que los mismos (aumentos) no alcanzan a aquellos gremios que no aceptaron la oferta salarial acordada en la última reunión paritaria”.

“Esto es por una sencilla razón, y es que la negociación requiere acuerdo que en este caso no existe”, añadió el funcionario.

Pusineri dijo que hay una norma general que “permite proceder a liquidar los incrementos para quienes sí estuvieron de acuerdo (con la propuesta)”, pero aclaró que “a todo el sector pasivo docente sí se le va a abonar el aumento como anunciamos el día viernes”.

El ministro indicó que “los gremios que rechazaron la oferta salarial serán convocados el próximo miércoles 27 a las 10 para seguir dialogando”, en referencia a la Amsafé y al Sindicato de Profesionales Universitarios de la Sanidad (Siprus).