---
category: La Ciudad
date: 2021-06-05T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/SUSTENTABLE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad convoca a un festival virtual de Cultura Sustentable
title: La Municipalidad convoca a un festival virtual de Cultura Sustentable
entradilla: Será este fin de semana y se realizará íntegramente a través de la web.
  Junto al Club de Cultura Sustentable, se dictarán talleres, conversatorios y clínicas,
  y habrá una feria de emprendedores.

---
La Municipalidad invita a participar, este fin de semana, del Festival de Cultura Sustentable: Exposición de Iniciativas Sostenibles. La actividad, que se organiza por primera vez en la capital de la provincia, se concretará este sábado 5 y domingo 6 de junio, bajo la modalidad virtual.

Enmarcado en la Semana del Ambiente, la propuesta consiste en propuestas audiovisuales, talleres, conversatorios y clínicas, que se completan con una exposición de Eco Emprendedores.

Se trata de una iniciativa del Club de Cultura Sustentable, una asociación civil a nivel nacional con la cual el municipio estableció contactos a través de su espacio Capital Activa, dependiente de la secretaría de Producción y Desarrollo Económico. La misma fue presentada al Ministerio de Cultura de Nación, a través de la convocatoria Festivales Argentinos, y se recibió financiamiento para el armado.

Entre los objetivos del festival, se cuentan, aumentar la concientización de la importancia de la cultura sustentable, elevar la calidad de las propuestas locales y brindar herramientas de apoyo. Al mismo tiempo, se promueve la posibilidad de formar redes y alianzas entre los participantes, así como el turismo cultural para el desarrollo del entorno y la economía local. En el mismo sentido, se busca garantizar el acceso a propuestas sustentables al público en general e instalar la necesidad de una producción y un consumo responsable.

La actividad es abierta y gratuita, y está dirigida a emprendedores, gestores culturales y profesionales, pero también a todo el público general interesado en la temática de la sustentabilidad. Para inscripciones y más información, ingresar a [https://clubdeculturasustentable.org.ar/festival/.](https://clubdeculturasustentable.org.ar/festival/. "https://clubdeculturasustentable.org.ar/festival/.")

**Grilla de actividades**

**Sábado 5 de junio:**

– 11 horas: Nuevos paradigmas de empleos verdes

– 13 horas: Experiencia del algodón sin agrotóxicos

– 14 horas: Diseñar desde el Litoral

– 15 horas: Separación diferenciada y reutilización de plásticos a baja escala

– 16 horas: Diseño y comunicación para emprendedores sustentables

– 17 horas: Taller de diseño sustentable

– 18 horas: Tutoriales de reutilización textil

– 19 horas: Experiencia de trabajo con la comunidad de artesanas QOM de Santa Fe

– 20 horas: Negocios con impacto social

**Domingo 6 de junio:**

– 11 horas: Clase abierta Taller de Eco Movimiento

– 12 horas: Producir moda sostenible y consumo responsable

– 14 horas: Fermentos y probióticos

– 16 horas: Cosmética natural

– 17 horas: Cerámica artesanal

– 18 horas: Comunicar con empatía. Tips para la comunicación en redes

– 20 horas: Suprareciclaje como identidad cultural

– 21 horas: Presentación del recital audiovisual: Trabajando, sí. Música corporal