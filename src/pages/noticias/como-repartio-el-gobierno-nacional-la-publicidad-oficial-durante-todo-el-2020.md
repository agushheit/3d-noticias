---
category: Agenda Ciudadana
date: 2021-01-28T10:08:16Z
thumbnail: https://assets.3dnoticias.com.ar/distribucion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Infobae
resumen: Cómo repartió el gobierno nacional la publicidad oficial durante todo el
  2020
title: Cómo repartió el gobierno nacional la publicidad oficial durante todo el 2020
entradilla: Los canales de TV abierta y cable se ubicaron en los primeros lugares,
  detrás las radios y los diarios

---
Durante el año pasado, el gobierno nacional destinó la suma de $4.704 millones en publicidad oficial repartida entre un total de más de 2.000 medios de todo el país, según datos oficiales de la Secretaría de Medios y Comunicación Pública actualizados a diciembre de 2020.

De acuerdo con el detalle de la información, de este total, los canales de televisión abierta recibieron $843 millones y las señales de televisión por cable, $898 millones. Entre las dos se llevan casi un 40 por ciento del total.

Entre los canales de TV abierta, Telefé fue el que recibió el mayor monto con un total de 227,5 millones de pesos. En la lista, lo siguen Canal 13, con $215,8 millones, y América TV, con $194 millones. Finalmente, la lista se completa con Canal 9 (con $129 millones), la TV Pública (con $63 millones) y NetTV (del grupo de medios Perfil) con 14,3 millones de pesos.

En este caso, la pauta está alineada con el rating, ya que según los datos Kantar Ibope de diciembre para los canales abiertos, Telefé también se ubicó en el tope de la tabla con una mediación de 7,8 puntos (para la franja de lunes a domingos de 12 a 24 Hs). Los siguieron Canal Trece, con 5,5 puntos; Canal 9, con 2,1; América, con 1,9; TV Pública, con 0,4; y Net TV con 0,3 unidades.

![](https://assets.3dnoticias.com.ar/pauta.jpg)

En tanto, en el segmento de la TV por cable, el monto principal fue para la señal C5N, con 120 millones de pesos. Luego se ubicaron Crónica TV ($107,2 millones), TN ($98,2 millones), Canal 26 ($57,2 millones), A24 ($51,8 millones), La Nación+ ($18,9 millones), Canal KZO ($10,6 millones), TNT Sports (casi $10 millones) y Fox Premium ($9,5 millones).

En términos del rating del cable –siempre considerando los datos de diciembre– hay cambios con respecto a la lista de publicidad oficial. Lideró la señal TN con 1,87. El podio lo completaron C5N (1,60) y A24 (1,28). Hasta la posición 10, estos son los canales de cable más vistos: ESPN 2 (1,20), Canal 26 (0,98), Crónica TV (0,95), Fox Channel (0,69), Cartoon Network (0,62), TyC Sports (0,47) y ESPN (0,42).

En el caso de los diarios, el primer lugar fue para Clarín, que recibió $131,8 millones. Luego, el segundo lugar con $120,9 millones, fue para La Nación. Y el tercer puesto, con $93,1 millones, fue para Crónica.

La lista se completó con Página/12 ($93 millones), Diario Popular ($89,8 millones); Ámbito Financiero ($66,4 millones); Infobae ($63,6 millones); El Cronista Comercial ($58 millones); Perfil (45,7 millones); Minuto Uno ($15 millones); Tiempo Argentino ($13,6 millones); Big Bang News ($7,1 millones); La Política Online ($6,2 millones) y El Cohete a la Luna ($5,2 millones).

![infobae-image](https://www.infobae.com/new-resizer/UFa8AigeNAqA_zeQr3tfpFeVChY=/420x747/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/U6SIQKV5VBFNTHQOFN6EKIXM3E.jpg)

Según Comscore, Infobae registró en el mes de noviembre (el último informado hasta el momento) un total de usuarios únicos de 22.411.141. De esa manera es el líder en audiencia del segmento. Clarín sumó en el período 21.976.321 y La Nación, 18.431.603. Las primeras cinco posiciones del rubro “Diarios” se completan con Perfil, con 9.398.848 de visitantes únicos mensuales, y Página 12 con 9.148.253.

**Radios**

En radios AM de alcance nacional, el monto de publicidad oficial más alto fue para Radio Mitre, con $53,2 millones, seguida por Radio 10, con $39,4 millones; AM 750, con $38 millones; La Red, con $34 millones; y Radio del Plata, con $18,8 millones.

![
](https://www.infobae.com/new-resizer/-ZGmpoWXpvVE0BXKkDKgB0wPJzI=/420x630/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/4DAXMVXUZVDIZKH6A5PINH2TEI.jpg)

Cuando se analiza el promedio de encendido (o share) de los últimos tres meses del año, Radio Mitre también ganó en audiencia con un encendido de 38,9 por ciento. La lista se completa con La Red (12,5); Radio 10 (12,1); AM 750 (8,3); Rivadavia (7,2); Continental (5,5); Nacional (1,7) y La 990 (1,0)

Mientras que en el segmento de radio FM de alcance nacional, el primer lugar fue para FM 100, con un total de $35,7 millones durante 2020. La lista, en orden según el monto recibido, se completó con Radio Pop ($28,3 millones), FM Aspen ($26,9 millones), Radio con vos ($25,2 millones) y Radio Mega ($23,6 millones).

![infobae-image](https://www.infobae.com/new-resizer/hpzEdmf7kxmEpNlNSbogJVg68AM=/420x630/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/ID2ZSJDT3JC4TDJ53GYSYLBSAM.jpg)

A la hora de analizar la audiencia de la FM, también La 100, la radio del Grupo Clarín, encabezó la tabla con un promedio de encendido en octubre, noviembre y diciembre de 16,7 por ciento. El podio lo completan Aspen, 14,3% y Disney, con 9,5.

Según la Resolución 247/2016 de la Secretaría de Comunicación Pública de la Jefatura de Gabinete de Ministros, se considera “publicidad oficial” a toda forma de comunicación, anuncio o campaña institucional para difundir informaciones de interés público. El Gobierno difunde dos veces al año la información sobre la distribución de esa publicidad, con el detalle de qué medios resultaron destinatarios de la pauta.