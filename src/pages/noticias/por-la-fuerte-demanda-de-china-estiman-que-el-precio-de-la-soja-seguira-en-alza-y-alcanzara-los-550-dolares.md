---
category: El Campo
date: 2021-02-17T07:01:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/soja.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Clarin
resumen: Por la fuerte demanda de China, estiman que el precio de la soja seguirá
  en alza y alcanzará los 550 dólares
title: Por la fuerte demanda de China, estiman que el precio de la soja seguirá en
  alza y alcanzará los 550 dólares
entradilla: Lo aseguró el referente Enrique Erize en una jornada realizada por la
  empresa Tecnomyl.

---
Como un leopardo esperando por su presa, las plagas están siempre agazapadas aguardando su oportunidad para hacer daño en el cultivo. La oportunidad la ofrece el clima, que genera condiciones predisponentes para su crecimiento y multiplicación, pero también desatenciones en los monitoreos. Caminar los lotes para anticiparse.

En este sentido, Augusto Casmuz ingeniero de la EEA Obispo Colombres del INTA, repasó algunas estrategias de manejo de las principales plagas en cultivos de soja y maíz, en una charla virtual organizada por la empresa Tecnomyl.

“Hoy hay ataques de spodoptera en maíz, también hay orugas en soja y probablemente en algunas zonas haya problemas con arañuelas y trips, típicas de años secos. La condicione no tan favorable para el desarrollo de los cultivos genera que las plagas tengan más impacto en los cultivos”, dijo como resumen y anticipación el ingeniero del INTA.

Cuando se refirió a oruga bolillera (Helicoverpa Gelotopoeon), que en épocas de sequía como se dio esta campaña tienen vía libre para crecer. “La plaga prosiguió su desarrollo y se manifestó en etapas reproductivas de la soja generando daño en vainas afectando directamente los rendimientos”, dijo Casmuz.

En cuanto a las estrategias, ponderó el tratamiento de semillas, “sobre todo en campañas que vienen de tendencia seca”, para favorecer la puesta en marcha de la soja. “Si tenemos mucha presión de helicoverpa probablemente el periodo de protección de tratamiento de semilla va a limitarse, pero siempre va a ser mejor que nada, y después se puede recurrir a tratamientos foliares que a los tres días ya logran controles superiores al 70%”, dijo. Aunque advirtió que si las condiciones son propicias para el desarrollo de la plaga el lote se re infecta y hay que estar atentos a ello.

También se refirió al complejo de defoliadoras y medidoras, entre las que destacó anticarcia gemmatalis, chrysodeixis includens (“falsa medidora”) y rachiplusia nu (“medidora”).

“Anticarsia se ubica en tercio superior del cultivo, consume hojas sin respetar nervaduras y su aparición es explosiva, en tanto que las medidoras comen la hoja respetando nervaduras y se ubican en el tercio medio del cultivo, muchas veces en el envés de la hoja, por lo que desde la camionera por ahí se ve el cultivo sanito pero si te bajás y abrís el follaje ves el daño”, dijo Casmuz. En cuanto al control, la falsa medidora, al estar más escondida, es más complicada para controlar.

Como estrategia, se probaron aplicaciones tempranas, antes que se produzca el cierre del entre surco, comparado con aplicaciones más tardías cuando se llega al umbral de aplicación. “En el caso de anticarsia, tempranas y tardías los productos tienen un control muy contundente, en medidora, con una aplicación temprana los niveles de control son de 95% de control versus aplicación tardía con más follaje y más larvas que llega a 75% de efectividad”, advirtió Casmuz. El momento de aplicación también hace a la estrategia, principalmente en falsa medidora.

El referente de la EEA Obispo Colombres también se refirió a las plagas que no están bajo el paraguas de la soja Intacta. Por ejemplo, Spodoptera, que está cobrando importancia las últimas campañas. “Las larvas tienen gran capacidad de consumo y comen vainas, son especies que pueden desarrollarse en numerosos vegetales, tienen marcada preferencia en gramíneas y los problemas en soja se dan porque se pasan de la gramínea a la oleaginosa”, explicó Casmuz.

En chinches, el ingeniero identificó varias especies pero marcó la chinche de la alfalfa y la chinche verde como las más dañinas para el cultivo de soja. “Las pasturas o, ahora cada vez más, los cultivos de servicio pueden servir de puente, de hospedantes para llegar a la soja”, contó Casmuz. Y agregó: “En invierno no se alimentan ni multiplican, pero cuando llegan los primeros calores arrancan y en soja generan abortos de vainas en las primeras etapas con el consiguiente impacto en rindes, luego en llenado de granos que si son chicos también aborta y si son grandes generan problemas de calidad afectando poder germinativo y la sobrevivencia del grano como semilla”.

**Plagas en maíz**

Para el cultivo de maíz, Casmuz identificó dos problemas de plagas principales: gusano cogollero y oruga de la espiga. “Cogollero puede desarrollarse todo el año en zonas del norte del país”, advirtió, aunque, claro, la etapa más cruda es de enero a marzo. “Aparece a lo largo de todo el ciclo del maíz, desde etapas vegetativas actuando como cortadora, aunque el daño clásico es de defoliadora y cogollera, afectando el normal crecimiento de la planta”, contó.

También apuntó que pueden llegar a consumir granos afectando no sólo el rendimiento sino también generando una puerta de entrada para enfermedades que terminan afectando también la calidad.

Si bien Casmuz identificó la eficacia de control de la tecnología Bt, también recomendó el uso de 10% de refugio y el monitoreo frecuente. “En cuanto a las estrategias de manejo, el tratamiento de semillas es importante para que la planta arranque bien y, llegado el caso, cuando se tiene que hacer la aplicación foliar el cultivo está mejor parado”, dijo.

Recomendó que la aplicación foliar se haga con 20% de pequeños daños, “raspado” de orugas chicas, porque es ahí donde va a ser más efectiva. “Hago hincapié en el monitoreo porque es una plaga que en el norte tiene una evolución explosiva de 4-5 días”, dijo.

Finalmente, se refirió a la oruga de la espiga, que básicamente se alimenta del grano generando pérdida de rendimientos y, al igual que cogollero, causa un daño en la planta que es la ventana para la aparición de patógenos que afectan la calidad del grano. “Hay que estar atentos con el monitoreo, porque cuando las larvas pasan a la espiga ya se hace incontrolable, lo recomendado son productos con poder de volteo en el contacto pero también persistencia”, resumió Casmúz como cierre.

**Alza de precios y voracidad china**

Como coequiper de charla de Casmuz participó Enrique Erize que le puso el cascabel a la suba en la cotización de los granos.

“Para algunos es una crisis de oferta, para nosotros, la explicación de gran parte de todo este aluvión de precios es China”, dijo, como postura inicial, el analista de mercados de Nóvitas, Enrique Erize. Y agregó: “El mercado granario para China es un tema geopolítico, fíjense que para 2024, el Banco Mundial y el Fondo Monetario Internacional pronostican que China va a ser potencia mundial, pero además, entre los 5 primeros habrá 4 asiáticos, es un golpe de efecto fuerte”.

Otro tema de trascendencia, antes de meterse en los números de lleno, es que China, junto a 14 países asiáticos como Tailandia, Malasia, Filipinas, Vietnam, Indonesia, Singapur, Nueva Zelanda, Australia, están por firmar la conformación de un bloque regional que sería el más grande del mundo con el 30% del PBI mundial. “Y un detalle, salvo Australia y Nueva Zelanda, que son parcialmente autosuficientes y excedentarios en producción de alimentos, el resto son todos demandantes de alimentos”, destacó Erize.

Ahora bien, ¿Qué pasó con los mercados en los últimos meses y por qué? “No es normal que suban los precios en Chicago en plena cosecha, menos habiendo tenido una cosecha histórica de maíz y una muy buena de soja, para algunos el tema es el clima, que para mí sólo puede representar un 10%, para otros influye el dólar debilitado, que sí, puede explicar otro 20% de las subas, pero el 70% de las subas se deben a la voracidad china”, explicó el analista.

“En el caso del maíz EE.UU. dice que va a exportar 60 millones de toneladas entre septiembre de 2020 y septiembre de 2021 ¡y en 5 meses ya tiene vendido casi el 90% del total del maíz que dicen que tienen para exportar en 12 meses! Esto es noticia”, se exasperó Erize. En cuando a la soja, un panorama similar: “Primero se llevaron toda la soja de Brasil en 2020, y en 5 meses EE.UU ya tiene vendido el 96% de la soja que estimó iba a vender hasta septiembre de ese año, es otro shock de demanda”.

Consultado sobre cómo sigue la cosa, Erize es tajante: “Veo mucha firmeza, la soja creo que va incluso a seguir subiendo”. Y explicó: “La pregunta del millón es si los picos de precios que se están viendo en Chicago para racionar la demanda externa, van a alcanzar, yo creo que los 500 dólares no van a amedrentar la demanda, por eso veo la soja en 550 para mayo-junio”.