---
category: La Ciudad
date: 2021-11-08T06:00:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/POLZE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: En Santa Fe los policías que fallecieron por Covid tendrán un ascenso extraordinario
  post-mortem
title: En Santa Fe los policías que fallecieron por Covid tendrán un ascenso extraordinario
  post-mortem
entradilla: "Se trata de un reconocimiento al servicio de los agentes fallecidos en
  pandemia durante el cumplimiento de sus funciones. También recibirá ascenso extraordinario
  un grupo de Policías por actos de valentía.\n\n"

---
La Subsecretaría de Recursos Humanos del Ministerio de Seguridad provincial convocó a un Jurado para el tratamiento de ascensos post-mortem y extraordinarios para el personal policial.

 La Subsecretaría de Recursos Humanos del Ministerio de Seguridad provincial convocó a un Jurado, designado mediante resolución N.º 1236/2021, para el tratamiento de ascensos post-mortem y extraordinarios para el personal policial. En ese marco, el cuerpo resolvió otorgar el ascenso post-mortem a 11 agentes fallecidos como consecuencia de complicaciones derivadas de haber contraído Covid-19 en el cumplimento efectivo de sus funciones.

  En virtud del carácter esencial del trabajo realizado por la Policía de la provincia de Santa Fe en el transcurso de la emergencia sanitaria, el personal que se encontraba prestando servicio presencial efectivo, resultó expuesto a un mayor riesgo a contraer el virus que aquellos que debían estar cumpliendo el aislamiento social, preventivo y obligatorio. Por esta razón, el gobierno provincial, a través del Ministerio de Seguridad, hace efectivos estos ascensos con la premisa de otorgar un reconocimiento a los agentes fallecidos.

 En el mismo sentido, el Jurado estableció el ascenso para tres agentes de la Policía de la provincia por actos de valentía en cumplimiento de sus funciones. En este sentido, este tipo de ascensos se otorgan a los efectivos que, en circunstancias de estar cumpliendo funciones policiales, realicen acciones con grave y real riesgo para su vida, en defensa de la vida, los bienes y derechos de las personas, mostrando en su actuar condiciones excepcionales de valor, coraje y responsabilidad, por lo que se los distingue por su accionar.