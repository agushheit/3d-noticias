---
category: Estado Real
date: 2021-06-19T06:00:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/OFICIOSpng.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Últimos días de inscripción para la escuela de oficios artísticos
title: Últimos días de inscripción para la escuela de oficios artísticos
entradilla: Hasta el domingo 20 se mantendrá abierta la recepción de postulantes para
  los cursos gratuitos de Iluminación, Artes visuales y Carnaval, murgas y comparsas.

---
El Ministerio de Cultura de la Provincia de Santa Fe en conjunto con la Universidad Tecnológica Nacional (UTN) convocan a la tercera etapa de la Escuela de Oficios Artísticos en su edición virtual, dirigida a toda la comunidad mayor de 18 años que reside en el territorio provincial, la convocatoria se mantendrá abierta hasta el domingo 20, con cupos limitados.

De carácter gratuito, los cursos de esta nueva instancia son Iluminación, Artes Visuales y Carnaval, murgas y comparsas. La inscripción puede realizarse ingresando a www.industriascreativas.gob.ar/escuela-de-oficios-artisticos.

La Escuela de Oficios Artísticos es un programa de capacitaciones públicas y gratuitas con cursos de formación en Producción, Técnica, Comunicación, Arte y Diseño, para proyectos culturales y artísticos de la Provincia de Santa Fe. Consta de cursos de 8 módulos, que se desarrollan en un aula virtual de 50 estudiantes por departamento de la provincia, alcanzado una capacidad máxima de 950 estudiantes por curso.

Tiene por finalidad contribuir a la profesionalización de trabajadores en la producción de bienes culturales. A través de esta propuesta, busca originar la infraestructura necesaria para la formación en oficios culturales con un sentido federal e inclusivo, intentando llegar a la mayor cantidad de habitantes de la provincia de Santa Fe.

El programa aborda contenidos multidisciplinarios con gran diversidad de lenguajes que se articulan para crear productos y experiencias más fructíferas. De esta manera, busca generar trabajadores competentes en saberes técnicos específicos e implicados en el proceso creativo general. Su principal objetivo es conjugar el oficio y el arte para enriquecer el acervo cultural en todo el territorio provincial.