---
category: La Ciudad
date: 2021-01-29T10:00:27Z
thumbnail: https://assets.3dnoticias.com.ar/jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: 'Camino Viejo: una transformación que beneficiará a 14 mil vecinos'
title: 'Camino Viejo: una transformación que beneficiará a 14 mil vecinos'
entradilla: 'Son cinco las empresas interesadas.Tendrá doble calzada, cantero central
  con ciclovía, senda peatonal, cinta verde, desagües pluviales y cloacales y una
  nueva iluminación. '

---
Más de 14.000 vecinos y vecinas de cinco barrios del noroeste se beneficiarán con la obra para transformar de manera integral Camino Viejo que licitó la Municipalidad de Santa Fe hoy. El intendente Emilio Jatón encabezó, junto al gobernador Omar Perotti, el acto de apertura de sobres con las ofertas económicas para llevar a cabo la intervención, entre Gorriti y Larrea, que consiste en la pavimentación del sector con una doble calzada de hormigón de 8 metros de ancho separadas por un cantero central parquizado.

“Es una obra ambiciosa pero sobre todo es una deuda histórica con una parte de la sociedad santafesina”, remarcó Jatón; y también destacó el trabajo mancomunado entre Nación, Provincia y Municipio para que este tipo de intervenciones se puedan concretar. La obra implicará una inversión de más de 204 millones de pesos.

El intendente se mostró orgulloso de poder cambiar falsas promesas por hechos concretos. “En los últimos años pasaron muchos políticos que dijeron que iban a hacer el Camino Viejo, pero nunca se cumplió, nosotros nos lo propusimos y hoy ya es una realidad”, dijo contento. Comparó esta calle como “una frontera que divide la ciudad” y por eso “tenemos que voltearla y ya dimos el primer paso”.

Se estima que en unos 60 días, luego de estudiar las ofertas y adjudicar la obra, los trabajos comenzarán; y, si bien la intervención es integral, lo que más destacó el intendente es que “el zanjón que tiene décadas a cielo abierto va a estar tapado” y al respecto expresó: “Cuando puedo cumplir con los objetivos que me propongo me pongo muy feliz”.

En otra línea, destacó el trabajo conjunto porque “no se puede hacer de otra manera si no es mancomunadamente”, dijo y al mismo tiempo agregó: “El dinero para la obra viene de Nación, nosotros presentamos el proyecto y, en el medio, la provincia también colaboró para que eso suceda”.

Antes de finalizar, el mandatario local, manifestó: “Esto es mucho más que una obra hídrica. Cuando hablamos de equilibrar la ciudad es esto y son intervenciones que también tranquilizan a la ciudad y le dan dignidad a muchos vecinos. Estoy contento porque es cumplir con la palabra, pasaron décadas de promesas incumplidas, nosotros ya no las anunciamos sino que abrimos sobres para que una empresa la haga”.

Verónica Luna es vecina de la zona y además promotora ambiental y se acercó al intendente para agradecerle por “haberse acordado de los barrios porque hacen falta muchas cosas; y que empiecen con el entubado para nosotros ya es mucho”, dijo y contó que llevaron a cabo una campaña de concientización sobre la deposición final de la basura.

“Trabajamos en concientizar a los vecinos para que pongan la basura en bolsas, que no la tiren en el zanjón y logramos un paso muy grande. Ahora estamos trabajando por el dengue. Estas obras tienen una gran importancia para el barrio. Hace 20 años que vivo acá y que esperamos esto y al fin llegó. Ojalá se concrete el entubado”, finalizó Luna.

**Licitación**

Los habitantes de los barrios Los Troncos, Santo Domingo, Acería, Juventud del Norte y San Ignacio de Loyola Sur son los favorecidos con esta obra que tiene un presupuesto oficial de $ 204.406.284 y se ejecuta dentro del Plan Integrar con financiamiento del programa Argentina Hace II. Serán 700 metros los que se intervendrán con un cantero central de siete metros que tendrá una ciclovía y una vereda peatonal de tres metros de ancho total, y dos cintas verdes.

En este marco, fueron cinco oferentes -cuatro empresas y una UTE- las que se presentaron a la licitación. La primera propuesta fue de Coemyc SA, quien ofertó $ 267.920.258; la segunda fue de Mundo Construcciones SA con una propuesta de $ 206.517.505; la otra fue de Guerechet SA, con $ 218.978.390; la cuarta perteneció a la firma Cocyar SA con $ 223.816.365; y por último la Unión Transitoria de Empresas integrada por Pilatti y Winkelmann que ofertó realizar los trabajos por un monto de $ 209.546.546.

**Trabajo conjunto**

Por su parte, el gobernador Omar Perotti también se mostró contento por “dar respuesta a años y años de expectativas y de sueños de todo este sector de la ciudad”. “Vinimos hace seis meses a este lugar con Emilio y nos habíamos comprometido con los vecinos. Vimos sus caras que nos decían que querían creer porque les dijeron muchas veces eso. Hoy es otra la expresión”, dijo más adelante.

Luego agregó: “Obras de esta magnitud no se hacen porque hay mezquindades, o la provincia no se pone de acuerdo con el municipio, o el municipio no tiene los recursos, o porque no hay una mirada integral de lo importante. Pero en este caso, sentimos lo mismo y nos pusimos de acuerdo. Nación aporta los fondos, el municipio diseñó el proyecto y la provincia acompaña todo el proceso”.

Antes de finalizar, Perotti expresó: “Es importante que los vecinos vean que la Nación, la provincia y la ciudad están trabajando juntos para resolver un problema, creo en esa forma de trabajar, de cooperar”.

**Detalles de la obra**

Vale destacar también, que el nuevo trazado vial contempla tanto la futura conexión con los bulevares proyectados en la intersección con Gorriti y con Beruti, como dos cruces de calles a nivel de vereda del cantero central en Pje. Guevara y Matheu. Con respecto a la iluminación urbana, comprende la provisión y colocación de 48 columnas y 72 artefactos tipo led a ubicarse sobre el cantero central.

La obra se encuentra en las Cuencas Gorri y Flores, y el proyecto de desagüe pluvial consiste en la ejecución de dos colectores: uno secundario en Camino Viejo a Esperanza para la primera cuenta mencionada, y un desagüe pluvial en la otra. El mismo está compuesto por 76 metros de caños de hormigón armado de sección circular de diámetro de 1 metros sobre calle Camino Viejo a Esperanza entre Calle Matheu  y Pje. Roca al sur.

Y 230 metros de caños de hormigón armado de sección circular de diámetro 1,20 metros sobre calle Camino Viejo a Esperanza entre Pje. Roca y calle Larrea (Sur). Se complementará con elementos que permiten las captaciones, inspección, limpieza y mantenimiento.

El presente proyecto incluye la renovación y traslado del colector de desagües cloacales en Camino Viejo a Esperanza, de Gorriti a Larrea con el fin de readecuarlo, se prevé ejecutar una colectora subsidiaria que no genere interferencias en el desagüe pluvial proyectado. La obra comprende el tendido de 1030 metros de cañería, la ejecución de 11 bocas de registro y trabajos complementarios.

Una de las premisas que se tuvo en cuenta en la instancia de diseño es la búsqueda de una convivencia armónica de todos los tipos de movilidad existentes del sector: vehicular, ciclista y peatonal. Por lo que, además de las obras viales descriptas, se proyecta una bicisenda en el cantero central que se incorpora a la red de carriles exclusivos para ciclistas de la ciudad de Santa Fe.

Pero al mismo tiempo, habrá un espacio para peatones ejecutando un piso de hormigón con terminación tipo alisado ferrocementado llaneado de 3 metros de ancho, acompañado de áreas de permanencia con piso de doblado de ladrillos a la vista incluyendo mobiliario urbano como bebederos, cestos, bancos y bicicleteros; y solados absorbentes.

![](https://assets.3dnoticias.com.ar/camino.jpg)

![](https://assets.3dnoticias.com.ar/camino2.jpg)