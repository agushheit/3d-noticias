---
category: La Ciudad
date: 2021-05-24T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/TRENES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Volverá el tren de pasajeros de Santa Fe a Laguna Paiva
title: Volverá el tren de pasajeros de Santa Fe a Laguna Paiva
entradilla: Falta terminar de acondicionar las vías y las estaciones para ponerlo
  en funcionamiento. Partirá desde la Estación Belgrano con capacidad para 200 pasajeros
  sentados, y tendrá cuatro paradas en el norte de la ciudad.

---
Falta terminar de acondicionar las vías y las estaciones para ponerlo en funcionamiento. Partirá desde la Estación Belgrano con capacidad para 200 pasajeros sentados, y tendrá cuatro paradas en el norte de la ciudad.

Un tren de pasajeros pronto unirá la ciudad de Santa Fe con Laguna Paiva, en un recorrido de 38 kilómetros. Será una formación con tres vagones con capacidad para transportar hasta 200 personas sentadas. La locomotora y los tres coches traccionados que conforman el tren fueron restaurados a nuevo y ya están casi listos -los están pintando- en la provincia de Córdoba. Se espera el acondicionamiento final de las vías y de las estaciones para comenzar a funcionar y transportar así entre 600 y 800 pasajeros, en cuatro viajes diarios, de aproximadamente 1.10 a 1.20 horas de duración. El viaje se abonará con el sistema Sube.

La confirmación y todos estos detalles fueron brindados a El Litoral por la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana. El retorno del tren a Paiva había sido el último anuncio del ex ministro de Transporte de la Nación, Mario Meoni, en un acto llevado a cabo el pasado 23 de abril en Rosario, junto al presidente Alberto Fernández y al gobernador Omar Perotti, horas antes de protagonizar un incidente vial en su auto particular en el que el funcionario perdió la vida. Ahora el cargo al frente de Transporte es ocupado por Alexis Guerrera, quien venía desempeñándose como titular de Trenes Argentinos Infraestructura.

**Desde la Belgrano**

En la ciudad de Santa Fe el tren tendrá su primera estación junto a la ex Estación Belgrano, sobre bulevar Gálvez y Vélez Sarsfield. La primera idea era que llegue al hangar principal del edificio, pero las vías fueron cubiertas con hormigón, por lo que se está montando otra estación al lado. La misma será una estación intermodal para el tren, colectivos, taxis y bicicletas, sobre calle Vélez Sarsfield. Así fue pensada en conjunto entre la provincia y la Municipalidad local. Y a futuro también se piensa extender su trayecto hasta el Puerto de Santa Fe. Pero primero hay que sortear un escollo generado por el paso de un ducto de gas natural que por el momento lo impide.

**Cuatro paradas en la ciudad**

La primera parada dentro de la ciudad tendrá un apeadero y será en Vélez Sarsfield y Calcena, a la altura de la escuela Avellaneda; la segunda -también con apeadero- sobre Vélez Sarsfield y Lavaisse, a la altura de la Universidad Tecnológica Nacional de Santa Fe (UTN); la tercera parada será en la Estación Guadalupe (hoy a cargo de la Municipalidad, que la está recuperando), ubicada en Avellaneda y Risso; y la cuarta y última será otro apeadero sobre bulevar French y Avellaneda.

Desde allí en adelante, el tren viajará hasta la estación ubicada en Ángel Gallardo, en jurisdicción de la Comuna de Monte Vera. Luego llegará a la localidad antes mencionada para continuar hasta las estaciones de Arroyo Aguiar y Constituyentes, antes de llegar a su destino final en Laguna Paiva. "Las estaciones de A. Aguiar y Constituyentes están usurpadas, por lo que habrá que construir un apeadero adicional", mencionó la ministra Frana, en diálogo con El Litoral.

**El proyecto**

El proyecto del regreso del tren de pasajeros es llevado adelante por el Ministerio de Transporte de la Nación, junto a Belgrano Cargas, la Operadora Ferroviaria Sociedad del Estado (SOFSE) -que está restaurando la formación- y la Administradora de Infraestructura Ferroviaria Sociedad del Estado (ADIFSE). Mientras que una Unidad Especial de Gestión Ferroviaria dependiente de los ministerios de la Producción y de Infraestructura provincial está trabajando junto a los municipios y comunas involucrados en la traza del tren. Estos son las ciudades de Santa Fe, Laguna Paiva, la comuna de Monte Vera, Arroyo Aguiar y la localidad de Constituyentes.

La obra completa para el retorno del tren a Laguna Paiva requiere una inversión inicial de $ 401.972.000; de los cuales Nación aporta el 62% y la Provincia de Santa Fe, el 38%. Este monto incluye el material rodante (locomotora y 3 coches), el acondicionamiento de vías, las estaciones y los apeaderos.

**Primera etapa**

En esta primera etapa, se hará lo más rápido y fundamental para que el tren empiece a circular, según se indicó desde el Ministerio de Infraestructura provincial. Luego, mientras el tren circule, se va a intervenir en las paradas intermedias.

La traza es operada en la actualidad por Belgrano Cargas y Logística S.A. "Los operarios ya están trabajando sobre bulevar Gálvez y Vélez Sarsfield en el acondicionamiento de las vías, también están en los tramos intermedios y en Laguna Paiva", indicó Frana, quien recordó luego que todos los estudios para concretar el regreso del tren se realizaron en el año 2010 y estuvieron a cargo de la Universidad Nacional de Rosario y el Instituto del Transporte, y luego fueron ratificados por el Grupo de Estudios del Transporte de la UTN Santa Fe.

\-¿Cuándo podría comenzar a funcionar?, consultó El Litoral a la ministra Frana.

\-Cuando se pensó el proyecto, la idea era que comience a funcionar a principios del año que viene. Pero con la pandemia es aventurado decir hoy una fecha. Ya se pueden ver a los operarios trabajar sobre las vías, el tren está casi listo y resta terminar las estaciones.

**Gran expectativa**

"En Laguna Paiva, en San Cristóbal, hay muchas expectativas en la gente, porque son ciudades en las que cuando desapareció el tren se generó un problema social muy grave, del que hoy todavía vemos sus consecuencias", dijo Silvina Frana. "Por ello, el regreso del tren desde ese punto de vista social es muy importante porque es la posibilidad de generar una fuente de trabajo", agregó.

Además, con el tren "tendremos un sistema de transporte de calidad, sustentable, inclusivo, accesible y muy esperado por la comunidad, que además tiende a contribuir a la seguridad vial, porque todos sabemos que hoy el incremento del parque automotor en todas las grandes ciudades no se condice con la infraestructura urbana", dijo sobre el final de la entrevista la ministra.