---
category: Estado Real
date: 2021-08-10T06:15:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/COVICHO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid-19: la provincia comenzó el operativo de combinación de vacunas para
  completar los esquemas de inmunización'
title: 'Covid-19: la provincia comenzó el operativo de combinación de vacunas para
  completar los esquemas de inmunización'
entradilla: Así lo anunció la ministra Sonia Martorano, quien informó que la segunda
  dosis de Sputnik V será combinada con Moderna. “La buena noticia es que el 80 %
  ha confirmado su turno”, sostuvo.

---
Las autoridades del Ministerio de Salud brindaron conferencias de prensa en la ciudad de Rosario, a cargo de la ministra Sonia Martorano, y en Santa Fe, por parte del secretario de Salud, Jorge Prieto, con el objetivo brindar información sobre el operativo de combinación de vacunas para completar los esquemas que habían iniciado con la primera dosis de Sputnik V.

“Aquellas personas que superaron los 90 días de haber recibido la primera dosis de Sputnik V, hoy están completando el esquema con Moderna, que es una excelente elección porque es una combinación de ARN mensajero y, como saben, en el mundo la combinación de vacunas es una de las maneras más eficientes y eficaces”, comenzó explicando la ministra de Salud, Sonia Martorano.

La funcionaria provincial agregó que esta vacuna, Moderna, “hace que la respuesta inmunológica sea muy superior. Por eso queremos aclarar que la plataforma de Sputnik V ya comienza siendo una vacuna de combinación porque el primer componente es diferente del segundo, el laboratorio Gamaleya inició el estudio ya con diferentes adenovirus”, argumentó para especificar el desarrollo y funcionamiento del proceso de combinación.

“De esta manera, todas las personas vacunadas en el mes de abril serán alcanzadas y podrán ser inmunizadas con Moderna como hemos comenzado”, pero no descartó que más adelante “puede ser el segundo componente de las vacunas Sputnik V”.

**80% de turnos confirmados**

“Hoy comenzamos con Moderna y se otorgaron 27.600 turnos”, detalló Martorano quien aseguró que “la buena noticia es que el 80% confirmó su asistencia”.

Asimismo, el secretario de Salud, Jorge Prieto, agregó: “Esto demuestra una vez más el apoyo de santafesinos y santafesinas para aumentar la barrera epidemiológica en el menor tiempo posible frente al avance de la circulación de la variante Delta”.

Por otra parte, se continúa con el operativo de más “de 41 mil turnos con segundas de Sinopharm, segundas dosis de AstraZeneca porque recuerden que el mes de agosto es donde se da la campaña más fuerte para completar esquemas”, manifestó.

**Llegar al millón**

“Hay 750 mil personas con esquemas completos, queremos superar el millón y continuar avanzando”, sostuvo Martorano quien también anticipó la llegada de 3.000 dosis de AstraZeneca que se suman a las 117.600 de Moderna.

Los turnos son otorgados y en la notificación a las personas se les informa qué dosis van a recibir, “pueden confirmar el turno o continuar esperando otro stock de acuerdo a la disponibilidad. Lo que venimos viendo con expertos es que esta es una muy buena opción, ya que la combinación es segura y eficaz”, afirmó la ministra de Salud quien recomendó “inmunizarse lo antes posible”.

“Llevamos más de 2 millones 800 mil dosis aplicadas, estamos en el 97% de colocación de vacunas si tomamos el monitor nacional. Por este motivo estamos a la espera de mayor cantidad de vacunas para cumplir con los objetivos”, explicó el secretario de Salud, Jorge Prieto.

En el caso de los menores, ya se han colocado 11.700 dosis para mayores de 12 años con factores de riesgo, “hay 20 mil inscriptos por lo que se completará el operativo esta semana”, finalizó Prieto.