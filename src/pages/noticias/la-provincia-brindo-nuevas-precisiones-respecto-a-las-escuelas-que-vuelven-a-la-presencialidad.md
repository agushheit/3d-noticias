---
category: Agenda Ciudadana
date: 2021-06-17T08:59:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuela1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia brindó nuevas precisiones respecto a las escuelas que vuelven
  a la presencialidad
title: La provincia brindó nuevas precisiones respecto a las escuelas que vuelven
  a la presencialidad
entradilla: La medida alcanza a cerca de 30.000 niñas y niños de 404 establecimientos
  educativos de los niveles inicial y primario.

---
El Ministerio de Educación brindó nuevas precisiones respecto a la habilitación para regresar al sistema de presencialidad con alternancia educativa en 253 localidades y parajes que tienen baja densidad demográfica y que no han superado los diez contagios en los últimos 14 días. 

La medida que se dispuso alcanza a 404 establecimientos educativos de los niveles inicial y primario e involucra a 29.893 niñas y niños santafesinos.

En relación a la incorporación de nuevas localizaciones que se habilitan para la presencialidad, Cantero precisó que “son parajes y pequeñas comunidades que encuadran en el criterio que implementamos el fin de semana, es decir aquellas localidades donde los contagios de los últimos 14 días acumulados no exhiben una cifra mayor de 10 y entonces analizando con la dirección de epidemiología y los datos que tiene ministerio de salud hemos incorporarlo en 107 localidades más que encuadran precisamente en esta definición”.

En ese contexto, la ministra resaltó que “nosotros seguimos monitoreando el día a día, el semana a semana y tenemos la expectativa de recuperar lo antes posible nuestra presencialidad cuidada de modo que si las santafesinas y los santafesinos seguimos cuidándonos  lograremos de alguna manera que los indicadores sanitarios en la provincia estén mejor, y a partir de ese dato iremos recuperando esa presencialidad que todos queremos”.

En referencia a modificaciones en el calendario escolar, Adriana Cantero aclaró que “nosotros no dijimos extender el ciclo lectivo. Nosotros estamos evaluando, si la vacunación masiva nos da un mejor contexto en el segundo semestre, el fortalecimiento de los cursados fundamentalmente en los grupos prioritarios para afianzar las trayectorias escolares. Para ello estamos trabajando fuertemente con el plan Acompañar que financia Nación y con el propio Acompañar que tiene la provincia y su línea "Todos las chicas y los chicos en la escuela aprendiendo,  y lo haríamos con una propuesta innovadora tal cual hicimos con Verano Activo, donde salimos a buscar a las y los alumnos que habían fragilizado sus trayectorias en el ciclo lectivo 2020 y donde en el mes de enero estuvimos trabajando en 562 sedes en todo el territorio de nuestra provincia”.

Asimismo, la titular de Educación manifestó que en los protocolos aprobados para el ciclo 2021 en el Consejo Federal de Educación ampliado con la presencia de los gremios docentes con representación nacional se había estipulado “presencialidad con bimodalidad y alternancia pero también con la flexibilidad suficiente de poder ir cerrando los dispositivos escolares si las curvas epidemiológicas así lo exigían y del mismo modo poder recuperar rápidamente presencialidad ni bien mejor en esos indicadores”.

En eses marco, la ministra Cantero aclaró que “la intermitencia forma parte de ese formato escolar nuevo para un tiempo inédito como el que estamos viviendo. Un tiempo absolutamente diferente donde la emergencia sanitaria condiciona todas las regularidades de la vida cotidiana y también la de las escuelas”.