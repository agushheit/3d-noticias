---
category: La Ciudad
date: 2021-08-22T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/3RAOLA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Nanci Blaza. El Litoral
resumen: 'Pronóstico Covid: septiembre con tercera ola y protagonismo de la variante
  Delta'
title: 'Pronóstico Covid: septiembre con tercera ola y protagonismo de la variante
  Delta'
entradilla: Aún no hay circulación comunitaria en Santa Fe, pero se estima que será
  inevitable. Una experta de Epidemiología de la provincia recomienda sostener los
  cuidados preventivos, más allá de las nuevas aperturas.

---
"Alfa", "Beta" y "Gamma". Esas son las tres variantes de SARS-CoV-2, el virus causante de la enfermedad Covid-19, que tienen presencia en el país y a las que se sumó recientemente "Delta", aún sin circulación comunitaria en la provincia de Santa Fe. Este dato es central pero no definitivo. Se aguarda que a mediados del mes de septiembre la Argentina atraviese una tercera ola de casos de coronavirus con la variante Delta como predominante.

En ese diagnóstico coinciden expertos de todo el país y también la infectóloga Florencia Galati, integrante del equipo de Epidemiología del Ministerio de Salud de la provincia. En diálogo con El Litoral, la médica explicó cuál es la situación que se espera para las próximas semanas y por qué es fundamental llegar al próximo mes con una buena cantidad de población vacunada con las dos dosis.

\-¿Para cuándo se esperan casos significativos de la variante Delta en Santa Fe?

\-Venimos con un descenso en el número de casos y esto tiene que ver con la pausa previa a la tercera ola que va a venir. No podemos precisar el momento exacto, pero por lo menos hay que esperar unos 30 días.

\-¿Esta tercera ola va a estar vinculada con la circulación de la variante Delta?

\-En realidad la modalidad que tiene la enfermedad, como vimos en el resto del mundo, es de una cepa predominante por encima de otras en función de la circulación. Inglaterra lo vivió con un aumento de Delta y empezó a disminuir la presencia de la variante del Reino Unido.

\-¿Cuál es la cepa predominante en Santa Fe?

\-Esto va de la mano con lo que ocurre en el resto del país. Tenemos mayor porcentaje de Manaos (Gamma) que es de Brasil. Hay variantes que son de interés y variantes de preocupación. De preocupación son las del Reino Unido, Manaos que es la que tenemos mayoritariamente circulando, la de Sudáfrica y la Delta que es la de la India.

\-Y que todavía no tiene circulación comunitaria en Santa Fe.

\- No. Tenemos a la fecha ocho reportes y todos son en relación con un viaje internacional. Son repatriados.

\-¿Preocupa que en Ciudad de Buenos Aires se hayan detectado casos positivos sin nexo con viajeros?

\-Son cinco casos notificados que no tienen vínculo con alguien que haya viajado fuera del país. En su momento pasó con la variante Manaos, donde se intentó contener por todos los medios hasta que comenzó la circulación comunitaria.

\-¿Por qué tiene que preocuparnos tanto esta variante?

\- La característica es la mayor transmisibilidad respecto de las otras, con una mayor tasa de ataque secundario: esto significa que produce mayor número de contagios a partir del caso índice. Pero si lo analizamos en comparación con el porcentaje de vacunación de un Estado vemos que cuanto mayor es ese porcentaje, mejor podemos contener esta situación, porque no vamos a tener mayor número de internados y fallecidos.

\-Por eso agosto se fijó como el mes de las segundas dosis.

\-Exactamente. Porque si esperamos que esta tercera ola comienza a mediados de septiembre, cuanto mejor lleguemos con el porcentaje de vacunación, vamos a estar mejor preparados para hacer frente al ingreso de la cepa.

\-Hay que estar atentos porque los síntomas son distintos o más dispersos. ¿Cualquier síntoma asociado a un resfrío puede considerarse indicador de esta variante?

\- No son síntomas muy distintos a los que ya se conocían. Sí se agregó en el protocolo de Nación el resfrío y la congestión nasal, que no estaban dentro de los síntomas y signos del Covid. A partir de los registros, se cambió la definición y sabemos que hay que prestar más atención al resfrío acompañado de otros síntomas. Sí observamos que las personas vacunadas que tienen Covid presentan síntomas muy leves. Por eso es muy importante registrar el síntoma que tiene la persona para llegar al aislamiento.

\-Dentro del contexto internacional, ¿cómo se comporta esta variante en personas ya vacunadas?

\- Hay un alto número de contagios como se pudo ver en algunos países. España ha llegado a estar entre la cuarta y la quinta ola con más de 20 mil casos por día. Pero no impactó en el número de internaciones y muertes; la mayoría son casos leves y eso tiene que ver con este porcentaje de vacunación, en el momento en que toca enfrentar esta cepa y esta ola que, para nosotros, va a ser la tercera.

\-Mientras tanto, se mantienen las recomendaciones para prevenir los contagios.

\- Sí. Ya se está hablando de público en los estadios de fútbol, de reuniones sociales. Pero a pesar de ello las medidas siguen en pie, porque tenemos que estar preparados para la tercera ola que va a llegar y la variante Delta va a terminar siendo predominante como fue en el resto del mundo. No vamos a estar fuera de que nos ocurra.

El ritmo de vacunación es bueno; falta un poco para que cuando llegue esta tercera ola estemos mejor preparados. Hay que aprender a convivir con el virus con todos los cuidados: el distanciamiento, el barbijo, y esperar que la mayor población tenga su segunda dosis.

**LA OTRA AGENDA**

Mientras toda la atención está puesta sobre el nuevo coronavirus, variantes, contagios y porcentajes de vacunación, es importante atender la otra agenda sanitaria. La pregunta que se plantea a Florencia Galati es la misma que se les repite a referentes de todas las áreas de la cartera provincial.

\- ¿A qué otros temas de salud deberíamos prestar atención?

\-Soy infectóloga y trabajo en la Dirección de Epidemiología. Me preocupan, por un lado, los pacientes con VIH que necesitan estar más cerca del sistema de salud, aunque la pandemia los ha alejado por el tema de no salir, del miedo a contagiarse, de esperar la vacuna.

Hay dos temas fundamentales: uno es el control de la salud más allá del Covid y los chequeos anuales como la mamografía y papanicolau, o en personas con enfermedades crónicas. Y el otro tema es la educación con los chicos, que tiene que reconectarse con ese ámbito.