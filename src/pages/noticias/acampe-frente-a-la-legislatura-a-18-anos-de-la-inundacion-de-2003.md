---
category: La Ciudad
date: 2021-04-26T06:00:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/inundacionalejandrovillar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Acampe frente a la Legislatura a 18 años de la inundación de 2003
title: Acampe frente a la Legislatura a 18 años de la inundación de 2003
entradilla: Reclaman que el 29 de abril sea declarado el "Día de la Memoria del Pueblo
  Inundado". Un proyecto de ley que quedó frenado desde el año pasado, con media sanción
  de Diputados.

---
Pasaron 18 años de la inundación de abril de 2003 en la ciudad de Santa Fe y la región, que dejó un saldo de 158 muertes y más de 130 mil personas desplazadas de sus hogares por el paso del agua del río Salado. A los 23 muertos reconocidos de manera oficial por el Estado se suman las víctimas por causas colaterales que año tras año son recordadas por las organizaciones que luchan contra la impunidad, por la justicia y memoria.

A la organización Carpa Negra, gestada durante aquellos trágicos días, se le sumaron este año la organización La Poderosa y y el espacio político Ciudad Futura. Entre los tres organizaron para este un acampe frente a la Legislatura Provincial. "Las inundadas y los inundados seguimos gritando que la impunidad inunda tanto como el agua, y seguimos exigiendo justicia", señalan los organizadores, y recuerdan que "el principal responsable, el ex gobernador Carlos Alberto Reutemann, jamás fue imputado por la causa inundaciones, y sigue ocupando una banca en el Senado Nacional".

**Acampe**

En busca de evitar la impunidad y para construir memoria sobre la inundación, en abril del año pasado estas tres organizaciones impulsaron un proyecto de ley para que el 29 de Abril sea declarado el Día de la Memoria del Pueblo Inundado, y su efeméride sea incluida dentro del calendario escolar en los distintos niveles del sistema educativo provincial, "para construir en las aulas un significado colectivo de una fecha que desgarró las vidas de todos los barrios del cordón oeste de la ciudad, y contribuir a que nunca más nos inunde la desidia estatal", sostienen.

Además, desde esa ley se pretende que cada 29 de abril sea "una jornada de acción de limpieza, cuidado y preservación de los anillos de defensa", reza en su artículo segundo. Y agrega que se instituya "en la Plaza 25 de Mayo a las 18 horas un acto con motivo de la conmemoración del Día de la Memoria de la Inundación de Santa Fe".

El proyecto obtuvo media sanción en Diputados, pero aún aguarda en el Senado su sanción definitiva.