---
category: La Ciudad
date: 2020-12-15T12:22:45Z
thumbnail: https://assets.3dnoticias.com.ar/robo-bv.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Vestido como cadete de una conocida aplicación salió a robar por bulevar
title: Vestido como cadete de una conocida aplicación salió a robar por bulevar
entradilla: Se llevó 100 mil pesos que sacó de una camioneta. Las cámaras públicas
  de vigilancia lo grabaron cuando corría, perseguido por el dueño del vehículo.

---
La policía busca a un hombre de 30 años que ya está identificado, como el autor de un llamativo robo ocurrido en pleno bulevar Pellegrini al 3300 el viernes de la semana pasada. El incidente se produjo aproximadamente a las 10.50 en esa cuadra de la Recoleta santafesina.

La víctima es un hombre que estacionó en el lugar su camioneta Grand Cherokee para ingresar a un banco ubicado en las inmediaciones. Unos minutos más tarde, este sujeto salió de la entidad crediticia y se topó con el delincuente dentro de su vehículo.

Su grito puso en fuga al malviviente, que comenzó a correr por bulevar en dirección al oeste. La víctima fue tras sus pasos. Toda la secuencia fue tomada con claridad por las cámaras públicas de vigilancia.

El ladrón llevaba un pantalón gris y una camisa roja con las inscripciones de una conocida app de cadetería. Dobló en calle Saavedra, hacia el sur, y trató de abordar su moto, una Honda Twister de 125 centímetros cúbicos negra que estaba estacionada junto a una caja roja (con los logos de la misma aplicación). En ese momento fue alcanzado por el hombre que lo perseguía. Se trenzaron en lucha y logró zafar, pero tuvo que seguir huyendo a pie, dejando su rodado y la caja abandonados.

La víctima volvió a su camioneta y allí se percató de que le había llevado 100 mil pesos que tenía en el habitáculo. Estaba muy golpeado por la pelea. Entonces, le contó a policías de la Brigada Motorizada lo que le había ocurrido.

El caso fue a parar al despacho de la fiscal Carolina Parodi, quien inmediatamente dispuso las primeras medidas.

# **Robo y persecución en Bulevar**

El escape del delincuente por bulevar quedó registrado en las cámaras de vigilancia. Por la vereda lo seguía la víctima.

Los detectives de la Agencia de Investigación Criminal revisaron la documentación de la moto y luego de observar las grabaciones en el centro de monitoreo pudieron comprobar de que se trataba de la misma persona. Ya era sábado y también se enteraron entonces de que el propietario había denunciado el robo de su birrodado en una comisaría y en la compañía de seguro.

Con los elementos reunidos, la fiscal solicitó el allanamiento del domicilio del sospechoso, que se concretó en horas de la mañana del domingo. El prófugo no estaba, pero sí había una mujer que terminó siendo aprehendida por "encubrimiento". Luego se pudo constatar que ella se encontraba cumpliendo una condena condicional por distintos delitos. En el lugar se secuestraron más de 60 mil pesos y dos cheques al portador por 130 mil pesos cada uno. Estos documentos tenían sendos pedidos de secuestro emitidos por el Banco Central de la República Argentina.

![](https://assets.3dnoticias.com.ar/robo-bv1.jpg)

En el inmueble, ubicado en inmediaciones del cruce de calle Misiones y Pasaje Sin Nombre, los uniformados también encontraron la vestimenta que habría utilizado el hombre para cometer el hecho.

Ahora, la AIC sigue buscando al sospechoso, que enfrenta cargos por hurto calificado y lesiones leves dolosas, entre otros.

![](https://assets.3dnoticias.com.ar/robo-bv2.jpg)