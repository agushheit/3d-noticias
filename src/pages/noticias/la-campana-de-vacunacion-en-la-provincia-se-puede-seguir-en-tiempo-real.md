---
category: Agenda Ciudadana
date: 2021-02-25T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La campaña de vacunación en la provincia se puede seguir en tiempo real
title: La campaña de vacunación en la provincia se puede seguir en tiempo real
entradilla: Un registro al que se puede acceder online y que muestra en tiempo real
  la información sobre el operativo nacional de vacunación contra el coronavirus

---
De acuerdo a la última actualización a las 6.08 de este miércoles, se distribuyeron 1.703.615 dosis de vacunas, de las cuales se aplicaron 780.455. Hasta ahora, 513.394 personas recibieron solo la primera dosis y 267.061 ya completaron la segunda aplicación. En Santa Fe, la distribución total fue de 135.100 vacunas y ya se aplicaron 65.851 dosis.

El dispositivo fue puesto en marcha por la ministra de Salud, Carla Vizzotti, y puede ser consultado por cualquier ciudadano y cuenta con información pública para "garantizar más transparencia en cada una de las etapas de la implementación del plan de inmunización", señaló.

En el sitio web del monitoreo se puede verificar, provincia por provincia, las dosis distribuidas en todo el país, así como la cantidad de aplicaciones por condición (personal de salud, personas de más de 60 años, personal estratégico y personas con factores de riesgo de 18 a 59 años).

Los datos sobre la estrategia de inmunización pandémica, provienen del Registro Federal de Vacunación Nominalizado (Nomivac), que forma parte del Sistema Integrado de Información Sanitaria Argentino (Sisa).