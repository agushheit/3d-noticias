---
category: Estado Real
date: 2022-02-18T09:57:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/Juntos_por_el_Cambio.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: 'Cuatro fundaciones para un acuerdo programático en Juntos por el Cambio '
title: 'Cuatro fundaciones para un acuerdo programático en Juntos por el Cambio '
entradilla: Se trata de las fundaciones de las cuatro fuerzas políticas que componen
  la coalición.

---
Respondiendo al mandato recibido por la Mesa Nacional de Juntos por el Cambio se reunieron en el día de ayer los representantes de las fundaciones de las cuatro fuerzas políticas que compone la Mesa Nacional: por la UCR la Fundación Alem, por el PRO la Fundación Pensar, por la Coalición Cívica el Instituto Hannah Arendt y por el Peronismo Republicano el Grupo Encuentro. La reunión tuvo por objeto establecer el programa de trabajo dirigido a la elaboración de los contenidos programáticos comunes de la coalición. A tal fin se definieron los diez temas prioritarios y se estableció un cronograma para la planificación de las tareas y la organización de los equipos de trabajo para cada área. Los diez temas prioritarios definidos por las fundaciones son: Trabajo productivo para todos los argentinos, Un cambio de fondo en la educación, Crecimiento económico, Salud: de la emergencia al desarrollo humano, Nuestro paradigma de seguridad, Justicia independiente, eficaz y creíble, Ambiente, Política exterior: volver a ser un país respetado, Pobreza y crisis social, y Estado y bienes públicos de calidad. Por la Fundación Alem participaron Fabio Quetglas, Lilia Puig de Stubrin y Agustín Campero Por la Fundación Pensar participó Franco Moccia Por el Instituto Hannah Arendt participaron Paula Oliveto y Fernando Sánchez Por el Grupo Encuentro participaron Eduardo Mondino y Juan Carlos Sánchez Arnau.