---
category: La Ciudad
date: 2021-05-09T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/CODE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe tendrá un Observatorio y un Planetario
title: Santa Fe tendrá un Observatorio y un Planetario
entradilla: El proyecto será completado luego de cinco años de idas y vueltas. En
  2022 Santa Fe será la única provincia con dos planetarios y un simulador de cielo.

---
El proyecto que comenzó durante el gobierno de Miguel Lifschitz verá la luz el año que viene: no solamente se acondicionará el Observatorio Astronómico y Museo del Espacio (Code), también se construirá el segundo planetario de la provincia de Santa Fe.

La convocatoria a licitación está prevista para el 31 de mayo, un plazo de obra de 180 días y un presupuesto de 60 millones de pesos “a actualizar”. Así lo informó Silvina Frana, ministra de Infraestructura, Servicios Públicos y Hábitat de la provincia, quien recorrió el Observatorio abandonado desde 2018 junto a su director, Jorge Coghlan y el subsecretario de Planificación de Obras, Omar Romero.

“Esto tiene una historia, un aporte de la provincia de Santa Fe, una obra que en su momento llevaba adelante la Municipalidad, con quienes para llegar a esta instancia tuvimos un esquema de diálogo”, dijo Silvina Frana. “La buena noticia para la ciudad de Santa Fe y la región es que esto se puede completar y terminar".

La ministra de Infraestructura precisó que las obras consisten en “completar aquello que se planteó” en 2017: observatorio, las salas de exposición del museo, el simulador planetario y mejorar las condiciones en general del edificio, como renovar los sanitarios.

“Para la historia del Code y para la historia de la astronomía santafesina es un paso muy importante porque finalmente se va a lograr un viejo sueño”, dijo Jorge Coghlan, director del Observatorio, que el año que viene cumple 60 años.

“Ya en los años 70 teníamos previsto hacer un observatorio y un planetario en el Parque Sur”, contó Coghlan. Con el golpe cívico militar en marzo del 76 quedó todo cancelado, cuando ya se había empezado a hacer los cimientos, que se pueden ver en un triángulo que está cerca de los monoblocks del parque. “El arquitecto Santiago Toretta lo había diseñado y había regalado todo su trabajo para el Code. Ahí nos frustramos porque perdimos la posibilidad de tener el observatorio y después de un planetario que iba a ser muy parecido al de Rosario”.

En 1992 el entonces intendente Jorge Obeid les propuso adaptar el comedor “Costa de Sol”, ubicado en Almirante Brown y Calcena, para hacer el observatorio. “Cuando Obeid era gobernador en 1999 se hizo la cúpula del lado sur, que tiene el telescopio más grande de la historia de la provincia de Santa Fe, más grande y moderno que el de Rosario”, continuó el relato Coghlan.

En 2016 el gobernador Lifschitz hizo un primer llamado a licitación para remodelar el Observatorio y construir el tan ansiado Planetario, que no resultó y se volvió a convocar en octubre de 2017. Según el director del Code las obras empezaron recién en abril de 2018 con una empresa que “tenía problemas de papeles” y luego de problemas en el pago a sus trabajadores, abandonaron todo a principios de 2019. “Dejaron a la gente tirada, con juicio y demás. Yo iba transmitiendo todo esto a la prensa y funcionarios, ministros, senadores y diputados, a todo el mismo grito”, dijo Coghlan.

“En diciembre de 2019 la única persona que me llamó y me sorprendió una mañana fue el gobernador Perotti. Dijo «yo soy electo, no puedo hacer nada por ahora, pero te prometo que ni bien podamos te vamos a dar una mano». Pero en 2020 la situación de pandemia y demás retrasó todo este proyecto a pesar de que durante 2020 trabajamos mucho con el Ministerio de Obras Públicas en todo el replanteo del proyecto original”, concluyó el director del Code.

La ministra Silvina Frana expresó: “Quienes somos de la ciudad no podemos dejar de valorar la resolución de este tema. Mirando las imágenes, el museo, un televisor del año 69 que fue cuando el hombre llegó a la Luna, no podemos no poner en condiciones este lugar para compartir toda esta información y toda la riqueza que hay en este lugar con el resto”.

Por último, aseguró que la renovación forma parte del esquema turístico que le quiere dar el gobernador Perotti a toda la provincia y “generar las condiciones para cuando, si Dios quiere sea pronto, salgamos de esta pandemia, también el turismo sea una fuente de trabajo y una fuente de conocimiento y cultura. Creo que este lugar reúne todas las condiciones para eso”, dijo Frana.

Sobre el planetario, el profesor Coghlan explicó que es una simulación del cielo real de manera virtual donde uno queda imbuido dentro de una cúpula que simula la esfera celeste, funciona todo el año y se va actualizando al cielo del día. “Se puede explicar mucho el movimiento de la esfera celeste, de los astros, de los planetas, las fases de la Luna. Hay una serie de documentales que se pueden proyectar en un domo de este tipo que son de una realidad extraordinaria”.

“Estamos muy contentos y pensamos que el año que viene vamos a tener en funcionamiento el observatorio restaurado y el planetario”, finalizó el director del Code.