---
category: Agenda Ciudadana
date: 2021-11-09T06:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/eleccionesnov.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los 14 puntos a tener en cuenta para votar en las elecciones del domingo
title: Los 14 puntos a tener en cuenta para votar en las elecciones del domingo
entradilla: 'Desde el Tribunal Electoral de la provincia dictaron una serie de pautas
  a cumplir para poder ejercer el voto en las elecciones generales del domingo 14
  de noviembre.

'

---
A días de las elecciones generales del domingo 14 de noviembre, el Tribunal Electoral de la provincia de Santa Fe dictó una serie de condiciones en los que se expresa la normativa sanitaria y de procedimiento para emitir el voto. Precisamente son 14 los puntos a tener en cuenta por los votantes al acercarse a las escuelas, buscando beneficiar la organización y acelerar el proceso.

**Los 14 puntos**

\- Sanitice sus manos al ingreso y utilice barbijo durante todo el tiempo que permanezca en el local de votación, cubriendo nariz, boca y mentón.

\- Mantenga la distancia con otras personas y colóquese detrás de la línea demarcatoria, en los casos que la hubiera.

\- Preséntese al/la presidente de mesa por orden de llegada, con el barbijo correctamente colocado, dando su nombre y apellido exhibiendo su documento para que se pueda verificar su identidad, luego deposítelo sobre la mesa, sin contacto con el/la Presidente de mesa.

\- El secreto del voto es un deber durante todo el acto electoral. Ningún elector o electora puede formular manifestaciones que signifiquen violar el secreto del voto.

\- Queda prohibido la portación de armas y el uso de banderas, divisas u otros distintivos durante el día de la elección, 12 horas antes y 3 horas después del comicio.

\- El/La presidente de mesa verificará si el/la ciudadano/a al que pertenece el Documento Nacional de Identidad figura en el Padrón Único de Electores, a cuyo fin cotejará si coinciden los datos personales consignados en tal registro con las mismas indicaciones contenidas en el DNI.

\- Cuando por error de impresión, algunas de las menciones del Padrón Único de Electores no coincidan exactamente con el DNI del/la elector/a el/la presidente de mesa no podrá impedir el voto siempre que coincidan las demás constancias.

\- Quien figure en el padrón tiene el derecho de votar y nadie podrá cuestionar ese derecho. Por consiguiente, no se admitirán impugnaciones fundadas en la inhabilidad del/la ciudadano/a para figurar en el registro.

\- Quien se encuentre registrado/a en el padrón como "Observado" remarcado con color verde (Art. 2 Ley 11.627), podrá votar solo en las categorías nacionales.

\- Los/las electores menores de 18 años no podrán votar en las elecciones y se encontrarán resaltados de color amarillo en el padrón.

\- Si la identidad del/la elector/a no es impugnada el/la presidente de mesa le entregará un bolígrafo sanitizado y una boleta única de cada categoría de cargo electivo, el que estará identificado con la letra y el color correspondiente.

\- Las autoridades de mesa, los/las fiscales de mesa y generales podrán votar únicamente en la mesa en que se encuentren empadronados/as.