---
category: Agenda Ciudadana
date: 2021-10-06T06:00:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/alberto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Fernández: "Pensemos en generar trabajo y no en lo que cuesta una indemnización"'
title: 'Fernández: "Pensemos en generar trabajo y no en lo que cuesta una indemnización"'
entradilla: '"Le digo que sí al trabajo y no al desempleo. Pido que unamos fuerzas.
  Decirle sí al que invierte y no al que especula. Sí a la Argentina que nos merecemos
  y no a la que tanto dolores nos trajo", indicó.'

---
El presidente Alberto Fernández convocó a construir puentes de diálogo hacia el futuro de la Argentina, en la que se priorice la inversión y la creación de puestos de trabajo y no la especulación, al tiempo que pidió no pensar "en lo que cuesta una indemnización sino en que el trabajo se genere".  
  
Al participar del acto de cierre de la Convención Anual de la Cámara Argentina de la Construcción (Camarco), el jefe del Estado remarcó la necesidad de generar trabajo "por sobre todas las cosas" para "salir de una economía de especulación a otra que produce" a partir "del diálogo y el consenso" que permita "construir un mundo hacia adelante".  
  
"La Argentina del futuro se construye con el diálogo, escuchándonos, de terminar con los gritos altisonantes", afirmó Fernández, al encabezar el acto de cierre de la la Convención que tuvo lugar en La Rural.  
  
Al respecto, sostuvo que sigue "reivindicando el diálogo como el mejor camino para alcanzar la Argentina que soñamos", un proceso que -aseguró- se está logrando gracias al trabajo del Consejo Económico y Social, donde se abordaron proyectos de ley como la promoción de producción de hidrocarburos y de desarrollo agro-bioindustrial, además del aumento del Salario Mínimo Vital y Móvil.  
Durante su exposición el Presidente hizo foco en la necesidad de que la construcción sea uno de los motores de la generación de trabajo a la vez que anticipó que el Gobierno está pensando en un esquema que permita a personas que hoy reciben una asistencia del Estado tener un trabajo formal sin perder esa ayuda.  
  
"No nos hace feliz que haya tantos hombres y mujeres en la Argentina que perdieron su empleo y hoy viven de la asistencia del Estado. Lo que debemos hacer es ver cómo convertimos planes sociales en empleo, devolverle dignidad y trabajo a quienes lo perdieron y reconstruir la cultura del empleo", aseguró el Presidente.  
  
Al respecto, dijo que espera que "pronto" pueda reglamentarse un decreto que permita a un empleador "recurrir a alguien que tiene un plan social y pueda dar trabajo porque lo que nos importa es recuperar el trabajo no sostener el plan social".  
  
Si bien negó que con esto se terminen los planes sociales, ya que "mientras los argentinos los necesiten seguirán teniéndolos", afirmó que sí se debe "recuperar la cultura de trabajar".  
  
"El dilema no es discutir una economía formal y una economía popular, sino ver si salimos de la economía de la especulación y pasar a la que produce, porque si esta triunfa el empleo va a existir", aseguró Fernández.  
  
Por eso, aseguró: "No pensemos en lo que cuesta una indemnización en el día de un despido, pensemos en que el trabajo se genere".  
  
"Le digo que sí al trabajo y no al desempleo. Pido que unamos fuerzas. Decirle sí al que invierte y no al que especula. Sí a la Argentina que nos merecemos y no a la que tanto dolores nos trajo", concluyó.

Por su parte, el presidente de Camarco, Iván Szczech, agradeció la participación del Presidente y le pidió hacer eje en tres políticas para el sector de la construcción: profundizar la inversión pública en infraestructura, dar incentivos a la construcción de viviendas y para una ley de créditos hipotecarios; y mejorar la transparencia en la ejecución de la obra pública.  
  
"Debemos brindar mayores oportunidades a la inversión privada, a quienes inviertan en proyectos estratégicos de Energía y Minería entre otros de manera tal que la Argentina pueda desarrollar sus potencialidades a la par de las mejores naciones del mundo", agregó Szczech. ​  
  
Al mismo tiempo, el Gobierno anunció hoy el programa "A Construir", que implica avanzar en la transformación de planes sociales en trabajo a través de la formación, capacitación profesional e inserción laboral formal en obras públicas nacionales, provinciales y municipales para personas beneficiarias de planes de asistencia social.  
  
"A Construir" fue lanzado en la sede central de la Uocra, con la presencia del jefe de Gabinete, Juan Manzur; ministros del Ejecutivo nacional; el titular de Camarco, y el secretario general del gremio, Gerardo Martínez.

"Estaremos abocados a brindar las capacitaciones, certificaciones de competencia y oportunidades de empleo formal en nuestras empresas a quienes hoy no tiene esa posibilidad y dependen exclusivamente de un plan social para vivir", afirmó Szczech, sobre la aplicación del programa.  
  
En ese sentido, señaló que ya se recuperaron los 70.000 puestos de trabajo perdidos entre marzo y julio del ano pasado y "estamos por sobre los niveles pre pandemia creciendo desde hace doce meses de manera sostenida".  
  
"Vamos en búsqueda de nuestro mejor registro histórico de 435.000 trabajadores", dijo el titular de Camarco.

En agosto pasado, el Presidente presentó en Concordia, Entre Ríos, el plan para la promoción del trabajo registrado y la ampliación de la protección social para 250.000 trabajadores rurales que realizan actividades temporales y estacionales, lo que constituyó el primer paso concreto del Gobierno para convertir los planes sociales en trabajo formal.