---
category: El Campo
date: 2021-06-09T08:30:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/produccion.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia dispuso más de 40.000 millones de pesos en recursos para sostener
  el aparato productivo santafesino
title: La provincia dispuso más de 40.000 millones de pesos en recursos para sostener
  el aparato productivo santafesino
entradilla: El monto surge de un informe presentado por el Ministerio de Producción,
  Ciencia y Tecnología ante la Cámara de Diputados de la provincia.

---
El ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, visitó este martes la Legislatura Provincial con el fin de brindar un informe a la Cámara de Diputados sobre las medidas adoptadas por el gobierno provincial para sostener el aparato productivo santafesino, en el marco de la emergencia sanitaria provocada por la pandemia de Covid-19.

El informe que presentó el funcionario provincial realiza un recorrido por todas la asistencias y programas que la gestión del gobernador Omar Perotti ha puesto en marcha desde iniciada la pandemia y, según explicó Costamagna, “buscan apuntalar la economía provincial cuidando la salud de todos las santafesinas y santafesinos. Trabajamos para que cuando salgamos de esta crisis podamos tener la mayor cantidad de empresas abiertas y de pie”.

“Desde mayo del año pasado a la fecha, sumando las asistencias directas, los fondos de financiamiento, lo inyectado para reactivar el consumo a través de Billetera Santa Fe, los beneficios impositivos y las medidas impulsadas desde EPE y ASSA, el gobierno provincial ha puesto a disposición del aparato productivo más de 40.000 millones de pesos en recursos. Esto es posible gracias a la decisión política de administrar los recursos de las y los santafesinos de manera eficaz, con prioridades claras y sin endeudarnos", agregó.

Según el informe, a la fecha, a través del Programa de Asistencia Económica de Emergencia, se invirtieron 1.240 millones de pesos que llegaron a más de 4.500 beneficiarios en más de 23.000 aportes. “Hablamos de gimnasios, salones privados de eventos, jardines de infantes, transportes escolares, peloteros, academias de danza, predios de fútbol 5, academia de artes marciales, centros culturales, escuelas de conducción, predios de paddle, boliches, centros de yoga y pilates, peluquerías, manicuría, podología, comestología y todos lo que tenga que ver con servicios conexos vinculados a la belleza; rubros que fueron los más golpeados por las consecuencias de la pandemia”, precisó el titular de la cartera productiva.

Asimismo, la presentación enuncia los 462 millones de pesos dispuestos para entregar créditos a través de agencias y asociaciones para el desarrollo de los 19 departamento y la red de mutuales de todo el territorio provincial. Desde esta herramienta ya se otorgaron más de 2.300 créditos a comercios, mipymes industriales y prestadores de servicios.

En cuanto al financiamiento impulsado por el gobierno provincial, se destacan las líneas Santa Fe de Pie, a través del Banco Nación, con un fondo de 26.500 millones de pesos destinada a agregar valor en el sector productivo y las instrumentadas en el Nuevo Banco de Santa Fe con más de 500 millones de pesos de fondo disponible con el fin de sostener la actividad productiva.

El ministro de Producción también destacó la inversión que realizará la provincia para atenuar el impacto de la segunda ola de coronavirus. En relación a ello resaltó que “ya comenzó a instrumentarse una ayuda que prevé invertir 3.000 millones de pesos, en asistencia directa a los rubros comerciales, a los trabajadores del sector hotelero y a los trabajadores de la gastronomía, también a transporte relacionado al turismo y a agencias de viajes y servicios anexos. A dicha asistencia le debemos sumar los beneficios impositivos y las medidas dispuestas en EPE y ASSA”.

“Este informe da cuenta de un gobierno que trabajó codo a codo con el sector productivo, diseñando políticas de apoyo en articulación con el sector privado y acompañando de cerca a un aparato productivo que es admirable. Que durante 2020, aun con todos los inconvenientes que presentó la pandemia, ha logrado poner a la provincia como epicentro de inversiones”, concluyó el ministro.