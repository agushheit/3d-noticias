---
category: Agenda Ciudadana
date: 2021-07-17T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/APORTESHIJES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Son 15.000 las santafesinas que accederán al reconocimiento de aportes por
  hijos
title: Son 15.000 las santafesinas que accederán al reconocimiento de aportes por
  hijos
entradilla: Esta medida, que regirá desde el 1 de agosto, alcanzará en el país a 155.000
  mujeres. Cómo acceder al beneficio de aportes del Ansés.

---
Unas 15.000 santafesinas están en condiciones de jubilarse tras la decisión del gobierno nacional de reconocer aportes por las tareas de cuidado, un hecho inédito en el país, que forma parte de una demanda histórica del movimiento feminista que se convirtió en política de Estado. En la Argentina, en total, serían unas 155.000 mujeres de entre 59 y 64 años las alcanzadas por el beneficio.

El Programa de Reconocimiento de Aportes por Tareas de Cuidado, que comprende a mujeres que estén en edad de jubilarse, hayan tenido hijos y no sumen 30 años de trabajo formal, comenzará a regir el próximo 1 de agosto, informó ayer la Administración Nacional de la Seguridad Social (Ansés). A todas las argentinas que hayan sido madres y a pesar de tener 60 o más años no estén jubiladas, el gobierno nacional le reconocerá un año de aportes por hijo, dos años en el caso de que fuera adoptado o que tuviera una discapacidad y tres años en caso de que haya accedido a una Asignación Universal por Hijo (AUH) por, al menos, 12 meses.

También se reconocerán los plazos de licencia por maternidad y de licencia por excedencia de maternidad a las mujeres que hayan hecho uso de estos períodos al momento del nacimiento de sus hijos.

Hasta que entre en vigencia el programa, la Ansés invitó a las mujeres que forman parte de este grupo a que ingresen con su CUIL y clave de la seguridad social a la solapa de “Mi Ansés” en su web y verifiquen si los vínculos familiares están actualizados en la solapa de “información personal” y, luego, en “relaciones familiares”. En el caso de que no figuren los datos deberán solicitar un turno para actualizarlos.

Todas las preguntas, todas las respuestas

**¿Qué es el beneficio?**

Reconocimiento de aportes por tareas de cuidado: es para mujeres que tengan la edad requerida para jubilarse, no cuenten con los años de aportes necesarios y tengan hijos.

**¿Qué aportes se reconocen?**

El reconocimiento de aportes por tareas de cuidado computará: 1 año de aportes por hijo. 2 años de aportes por hijo adoptado. Asimismo, reconocerá de forma adicional 1 año por hijo con discapacidad y 2 años en caso de que haya sido beneficiario de la Asignación Universal por Hijo por al menos 12 meses. Además, se reconocerán los plazos de licencia por maternidad y de excedencia de maternidad a las mujeres que hayan hecho uso de estos períodos al momento del nacimiento de sus hijos.

**¿A quiénes les corresponde?**

Mujeres con hijos, en edad de jubilarse (60 años o más) que no cuenten con los años de aportes necesarios. Podrán acceder a esta medida aquellas mujeres que no cuenten con una jubilación ya otorgada o en trámite.

**¿Cómo se gestiona?**

Los trámites para el beneficio deberán gestionarse por turno a través de la web de Anses (www.anses.gob.ar) a partir del 1 de agosto en la solapa de “Mi Ansés”.

**¿Cuándo se puede hacer el trámite?**

Los turnos estarán disponibles a partir del 1 de agosto.

**¿Qué se necesita para realizar el trámite?**

La atención será únicamente con turno previo, DNI y las partidas de nacimiento de los hijos.

**¿Qué se puede hacer antes de solicitar el turno?**

Revisar si los vínculos familiares están actualizados en Ansés. Es muy importante que los hijos se encuentren registrados para que se pueda realizar este trámite.

**¿Dónde consultar si los hijos están registrados?**

Ingresar a Mi Ansés con el Cuil y clave de la seguridad social. Seleccionar la opción “Información Personal” y luego “Relaciones Familiares”.

**¿Qué hacer si algún hijo no figura?**

Actualizar tus vínculos familiares presentando el DNI y la partida de nacimiento de cada uno de los hijos. Solicitar un turno para ser atendida en la oficina de Ansés más cercana al domicilio.