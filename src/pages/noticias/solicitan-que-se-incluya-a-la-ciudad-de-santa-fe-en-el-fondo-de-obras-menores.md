---
layout: Noticia con imagen
author: Nota de la Redacción de 3DNoticias
resumen: " Fondo de Obras Menores"
category: La Ciudad
title: Solicitan que se incluya a la ciudad de Santa Fe en el Fondo de Obras Menores
entradilla: "El pedido fue realizado por el concejal de Juntos por el Cambio,
  Carlos Suárez, acompañado por sus pares de bloque Sebastián Mastropaolo y
  Luciana Ceresola, mediante un proyecto de comunicación. "
date: 2020-11-24T11:52:59.520Z
thumbnail: https://assets.3dnoticias.com.ar/suarez-ceresola-mastropaolo.jpg
---
Legisladores nacionales, provinciales y locales de Juntos por el Cambio realizaron una conferencia de prensa días atrás, donde solicitaron -entre otras cuestiones-, que la Legislatura incluya a las ciudades de Santa Fe y Rosario en el Fondo de Obras Menores.

“La incorporación Santa Fe en este fondo representaría unos 400 millones de pesos para la ciudad”, afirmaron.

En este sentido, el concejal Carlos Suárez presentó un proyecto de comunicación para formalizar ese pedido ante las Cámaras de Diputados y Senadores de la provincia. La iniciativa fue acompañada por sus pares de bloque Sebastián Mastropaolo y Luciana Cerersola.

“Esta solicitud no solamente tiene que ver con pelear por los recursos que nos corresponden, sino con los valores de justicia y equidad, que para Juntos para el Cambio son centrales. Incorporar a nuestra ciudad al Fondo de Obras Menores saldaría una deuda histórica que tiene la provincia y, en definitiva, mejoraría la calidad de vida de todos los vecinos”, afirmó Suárez.

El concejal de Juntos por el Cambio explicó que estos recursos, que implican alrededor de 400 millones de pesos, pueden ser destinados a desarrollar lotes con servicios. “Es decir, a buscar una solución a las usurpaciones y ocupaciones ilegales que hoy son un problema en muchos barrios de la ciudad y afectan tanto a vecinos, como a quienes tienen la necesidad real de una vivienda”, agregó.