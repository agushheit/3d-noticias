---
category: La Ciudad
date: 2020-12-31T11:04:25Z
thumbnail: https://assets.3dnoticias.com.ar/3112-lucas_simoniello.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Simoniello: «Trabajamos por equilibrar lo urgente con lo importante para
  la ciudad»'
title: 'Simoniello: «Trabajamos por equilibrar lo urgente con lo importante para la
  ciudad»'
entradilla: 'El concejal realizó un balance del trabajo durante el año 2020, al que
  calificó de «difícil y cargado de incertidumbre». '

---
Con cerca de 60 proyectos presentados destacó: «nos concentramos en aportar a las respuestas que la pandemia exigía y exige, pero no abandonamos los proyectos que articulan la idea de una ciudad más equilibrada».

«Para realizar balances es necesario revisar objetivos y en ese sentido, cuando llegamos al Concejo, destacamos que con la herramienta del diálogo comenzábamos un camino de la mejora y el sostenimiento de lo que se había hecho bien en la ciudad en los últimos años».

«Principalmente, nos encomendamos al trabajo en ejes que aporten a la idea de una ciudad más justa y equilibrada, como el acceso a la primera vivienda y al suelo para más familias de la ciudad, la movilidad, el transporte, el espacio público -desde los parques y plazas hasta las veredas y calles-, la gestión de riesgos y la construcción y apertura de información para gestionar políticas públicas».

Sin embargo, la pandemia de COVID-19 obligó a reconsiderar las agendas de trabajo. «Este año se pusieron a prueba las capacidades de gestión de las administraciones y gobiernos en todos los niveles del Estado; y desde el Concejo, y particularmente desde la gestión encabezada por su presidente Leandro González, transitamos un año con mucho debate, pero con la construcción de acuerdos importantes».

«Impulsamos la ayuda a distintos sectores de la economía local desde el gobierno municipal, particularmente a instituciones educativas, clubes, espacios de arte y sectores comerciales. En el caso de los Jardines maternales y de infantes de gestión particular, consideramos fundamental protegerlos y acompañarlos porque educan a más de 2000 niños y niñas, además de crear casi 300 empleos de manera directa y ser un pilar fundamental del Sistema de Educación Inicial».

«Sabemos que falta mucho por hacer y que hace falta un mayor acompañamiento del gobierno provincial en el avance y finalización de obras en la ciudad, así como desde el gobierno nacional con la llegada de más oportunidades en cuanto al acceso a la vivienda para miles de santafesinos que están esperando cumplir ese sueño en su ciudad».

<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 800">**Vivienda, movilidad y espacios públicos**</span>

En total fueron casi 60 proyectos presentados entre los que se destacan: la instalación de una red de Estaciones de Transferencia Multimodal (Tramu) distribuidas en al menos nueve espacios públicos en lugares estratégicos de la ciudad para articular y combinar los diferentes medios de movilidad, como colectivos, bicicletas, taxis y remises; que va de la mano con la creación de un Sistema Público de Transporte por Bicicletas.

Vale resaltar también la aprobación de la Ordenanza Nº 12715, a través de la cual se crea la Infraestructura de Datos Espaciales de la Ciudad de Santa Fe, una herramienta para generar, almacenar, analizar y mapear todo tipo de información disponible tanto para el Municipio y para los vecinos y vecinas en general.

Desde el anuncio del relanzamiento del ProCreAr, Simoniello presentó una serie de iniciativas para «aprovechar al máximo posible esta oportunidad que tienen las familias santafesinas que aún esperan cumplir el sueño de la casa propia», al mismo tiempo que destacó «el impacto positivo que pueden tener en la economía, reactivando las industrias relacionadas con la construcción, generando mano de obra y compra de materiales».

Algunas de ellas son: la actualización del Banco de Tierras Municipal; beneficios tributarios cuando los permisos sean para obras de construcción, refacción o ampliación de viviendas únicas familiares; agilización de los trámites e incorporación de nuevos sistemas de construcción y materiales a la normativa urbana. Entre ellas se destaca autorizar la subdivisión de lotes: «facilitar su división, para que donde hoy se puede construir solo una vivienda se puedan realizar dos o más casas individuales. También fomentar que puedan construirse viviendas arriba o atrás de otras ya existentes».

En el mismo sentido, se aprobó un proyecto para declarar de interés prioritario a ciertas áreas de la ciudad en cuanto a la aplicación de políticas de vivienda.

«Una política de hábitat adecuada a la realidad de Santa Fe necesita de una eficiente gestión del suelo. Por eso, buscamos garantizar que el Municipio pueda localizar planes de viviendas a construirse o promoverse en forma pública, desarrollos urbanísticos y la promoción del acceso de las familias a suelo urbanizado y a su lugar en la ciudad».