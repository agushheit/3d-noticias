---
category: Agenda Ciudadana
date: 2021-11-19T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/OFICIALISMO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: En un clima tenso, el Frente de Todos avaló en el Congreso 116 decretos y
  Juntos por el Cambio se retiró de la Bicameral
title: En un clima tenso, el Frente de Todos avaló en el Congreso 116 decretos y Juntos
  por el Cambio se retiró de la Bicameral
entradilla: 'La idea del oficialismo es aprobar los decretos en el recinto antes del
  10 de diciembre, cuando se produzca el recambio legislativo y el Frente de Todos
  pierda la mayoría en el Senado. '

---
Antes de consumarse el recambio legislativo, el oficialismo avaló hoy en la comisión Bicameral de Trámite Legislativo un paquete de 116 decretos que el presidente Alberto Fernández había dictado desde que asumió el cargo y la oposición de Juntos por el Cambio, que estuvo presente en el principio de la reunión, se retiró con duros reproches.

Con esta paso, el paquete de decretos, de los cuales 70 son de "necesidad y urgencia", quedó en condiciones de ser tratado por una de las dos cámaras, y se presupone que será el Senado el cuerpo que lo vote, ya que allí el oficialismo tiene mayoría hasta el próximo 10 de diciembre.

En el comienzo de la reunión, el diputado del PRO Pablo Tonelli explicó la postura de rechazo de Juntos por el Cambio: "Todos los plazos están vencidos y esta metodología no se ajusta ni a la Constitución ni a la reglamentación. Rechazamos esta convocatoria y los decretos que pretenden tratar en Comisión".

Según pronosticó, "el Gobierno va a lograr la validez de los decretos, pero de una manera irregular", y recordó que "lo que los constituyentes han querido es que el tratamiento que la comisión y el Congreso deban hacer de esos decretos sea inmediato, sea rápido".

Tonelli precisó que la Ley 26.122 dispone que cuando “la comisión no se expida dentro del plazo de 10 días, las Cámaras quedan habilitadas y deben considerar de manera inmediata y expresa los decretos".

"Este tratamiento demorado, y para peor conjunto, está muy lejos de respetar las normas de la Constitución y de la ley", concluyó.

Luego el chubutense Gustavo Menna (UCR) completó los argumentos por los cuales Juntos por el Cambio no estaba dispuesto a acompañar la aprobación de los decretos.

Para el diputado radical, lo que está en debate es "si la atribución de dictar leyes sigue perteneciendo al Congreso o lo vamos a trasladar al Ejecutivo".

"El hecho de que sean 116 los decretos de contenido legislativo en consideración es una desmesura, de una exorbitancia tal, que habla a las claras que en estos casi dos años de Alberto Fernández se ha legislado por decreto. El Poder Legislativo se ha trasladado a la Casa Rosada", criticó.

"Además de la cuestión formal, lo que es un tema de fondo es si vamos a sostener un sistema de división de poderes en donde la competencia de dictar leyes sigue perteneciendo al Congreso o la vamos a trasladar al Poder Ejecutivo".

Menna remarcó que "el artículo 99 de Constitución, inciso 3, no es una habilitación a dictar DNU festivamente", sino que únicamente habilita a hacerlo cuando "no es posible por circunstancias excepcionales seguir el trámite ordinario para la sanción de las leyes".

"Estamos asistiendo a una suerte de operación de blanqueo. Como ustedes han perdido a partir del 10 de diciembre la mayoría en el Senado, lo que se pretende es apurar la convalidación, la ratificación de estos decretos, que no les importó tratarlos en todo este tiempo en esta comisión", acusó.

Luego de este discurso, tanto Menna como Tonelli, Luis Petri (UCR) y Leonor Martínez Villada (Coalición Cívica), quienes participaban de la Bicameral, se levantaron de sus sillas para abandonar el Salón Eva Perón, donde se llevó a cabo la reunión.

En ese instante, el presidente de la Bicameral, el diputado Marcos Cleri (Frente de Todos) les pidió "respeto" y los invitó a que se quedaran a escuchar la postura del oficialismo en boca de la vicejefa del bloque de senadores del Frente de Todos, Anabel Fernández Sagasti.

Al tomar la palabra, la mendocina destacó que los 116 decretos "son muy importantes para la institucionalidad de la Argentina", y recordó que la mayoría de ellos fueron dictados durante la pandemia, en “un contexto excepcional, de necesidad y urgencia, que nos obligaba a abocarnos a los decretos".

La legisladora oficialista acusó a la oposición de "tener una memoria selectiva” y recordó que el DNU 27 firmado por el ex presidente Mauricio Macri durante el verano del 2018 derogó 19 leyes y se modificaron alrededor de 150 normas, "con la excusa de desburocratizar el Estado".

En este sentido, Fernández Sagasti les reprochó a los opositores "venir a hacerse los desmemoriados y no reconocer que esta situación que está viviendo la Argentina es excepcional".

"En pandemia, como nunca antes han estado los DNU más justificados en cuanto a su necesidad y urgencia", insistió y remató: "No vamos a permitir que una vez más se nos señale desde la oposición al oficialismo como que nosotros somos los que cotidianamente estamos afrentando a la institucionalidad".

Por su parte, el diputado y senador electo Pablo Yedlin (Frente de Todos) defendió la herramienta de los DNU utilizada por el presidente Alberto Fernández para tomar decisiones y al respecto consideró que "este instrumento constitucional fue muy útil para gestionar la pandemia".

"Si en algún momento un profesor de derecho constitucional tuviera que elegir un período más justificado para dictar decretos de necesidad y urgencia, no tengo dudas que tomará la pandemia como un ejemplo", manifestó el tucumano.

"La oposición y algunos medios quieren confundir intentado plantear que los DNU son actuales, y que el gobierno esta evitando el debate parlamentario. La única verdad es la realidad", resaltó.

Antes de que comenzara la reunión de la bicameral, los representantes de Juntos por el Cambio de la bicameral, acompañados por el presidente del bloque del Senado, Luis Naidenoff (UCR), realizaron una conferencia de prensa en la cual ratificaron su rechazo al procedimiento que había anunciado el oficialismo para aprobar los decretos.

"Rechazamos la pretensión del oficialismo de aprobar con un trámite express dos años de gobierno por DNU, apurados por el resultado electoral". "Es un grave atropello institucional que demuestra el desprecio por el Congreso, la Constitución y por las reglas de la democracia", lanzó el formoseño.