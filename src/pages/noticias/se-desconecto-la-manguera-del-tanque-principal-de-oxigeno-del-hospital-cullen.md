---
category: La Ciudad
date: 2021-06-05T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/OXIGENOUPS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Se desconectó la manguera del tanque principal de oxígeno del Hospital Cullen
title: Se desconectó la manguera del tanque principal de oxígeno del Hospital Cullen
entradilla: El hecho se produjo durante la recarga. La desprovisión temporal “no afectará
  la atención de pacientes Covid y No Covid” que utilizan camas respiradas, aseguró
  el director Juan P. Poletti.

---
Ocurrió este viernes, en horas de la tarde.En un sector del Hospital José M. Cullen, se empezó a a ver una enorme nube de “humo” blanco despedido a borbotones. ¿Qué fue eso? Ocurrió que cuando se estaba realizando la recarga del tanque de oxígeno, se produjo la rotura de la válvula conectora de un acople. Esto generó la desconexión de ese tanque principal y la pérdida del hoy vital elemento para los pacientes internados que lo requieren.

Así lo explicó el director del hospital, Dr. Juan Pablo Poletti. “Se sintió el latigazo de la manguera, que llevo a perder el oxígeno que ya se había cargado en el tanque durante la jornada, algo que se está haciendo todos los días por la alta demanda”, confió el médico

Esa manguera “descontrolada” pegó en la cara y en la cabeza del camionero encargado de realizar esa esa función de recarga del tanque. “Fue atendido en la Guardia y no sufrió lesiones: “Esta persona pudo cortar las válvulas de seguridad para que se deje de perder (oxígeno). “Ahora estamos sin pérdida”, añadió Poletti.

“Ya nos encontramos armando toda la logística de la empresa encargada para que se realice de forma inmediata la recarga del tanque grande necesaria para la provisión de oxígeno”, afirmó el director, al tiempo que aseguró que este infortunio “no va a afectar la atención con los pacientes (Covid y no Covid)” 