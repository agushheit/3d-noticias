---
category: Agenda Ciudadana
date: 2021-11-12T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Presidente pidió que el domingo lo "ayuden a construir el sueño de vivir
  la Argentina que nos merecemos"
title: El Presidente pidió que el domingo lo "ayuden a construir el sueño de vivir
  la Argentina que nos merecemos"
entradilla: "\"Les pido a todos ustedes que vayan y hablen con sus vecinos, que les
  expliquen dónde estábamos, dónde estamos y a dónde queremos ir\", enfatizó el jefe
  de Estado. \n"

---
El presidente Alberto Fernández afirmó hoy que el Gobierno nacional fue "escuchando" las demandas de los ciudadanos tras el resultado de las PASO, y pidió que el próximo domingo lo "ayuden a construir el sueño de vivir la Argentina que nos merecemos".

"Les pido a todos ustedes que vayan y hablen con sus vecinos, que les expliquen dónde estábamos, dónde estamos y a dónde queremos ir. El domingo lo que les pido es que me ayuden a construir el sueño de vivir la Argentina que nos merecemos", resaltó Fernández.

Durante el acto de cierre de campaña del Frente de Todos en el partido bonaerense de Merlo, el Presidente prometió que el Ejecutivo nacional seguirá dando "pasos firmes para que el crecimiento llegué a cada argentino y cada argentina".

"No nos basta de nada que el PBI crezca más de nueve puntos si la vida de los argentinos no mejora. Lo único que me preocupa es verles la sonrisa de decir 'estoy viviendo mejor'", enfatizó.

En presencia de la vicepresidenta Cristina Kirchner, el jefe de Estado recordó que al asumir en el cargo el 10 de diciembre de 2019 se encontró con un país "sumido en el endeudamiento", con "una deuda impagable" y "sin ministerio de Salud, de Trabajo ni de Ciencia y Tecnología".

Al referirse a la renegociación de la deuda con el Fondo Monetario Internacional (FMI), el Presidente advirtió que se tomará "el tiempo que haga falta", y aclaró: "No lo voy a resolver en 5 minutos, porque quien resuelve ese problema en 5 minutos es porque le da da la razón al Fondo en todo lo que pide".

"La razón yo se la doy a ustedes, no al Fondo. Me tomaré todo el tiempo que haga falta para el mejor alcanzar el mejor acuerdo para las argentinas y argentinos", subrayó.

En ese marco, el mandatario argentino recordó: "En este año difícil que nos tocó pasar, no nos detuvimos. Seguimos por sobre todas las cosas pensando en los que menos tienen, en los que más padecen, en los que más sufren"..

"El día que asumí les hice una promesa: les dije primero los últimos. Y será por los últimos que vamos a seguir trabajando, más que por nadie", expresó.

En ese sentido, el Presidente continuó: "No necesito dar muestras de que es eso lo que queremos hacer, porque es lo que hicimos permanentemente. Somos hijos de (Juan Domingo) Perón y de Eva (Perón). Somos la continuidad de Néstor (Kirchner), y soy también la continuidad de Cristina (Kirchner)".

Además, Alberto Fernández enumeró las políticas que se llevaron adelante en materia sanitaria desde el comienzo de la pandemia de coronavirus, y destacó las políticas que se impulsaron en los últimos meses para controlar el precio de los alimentos y los medicamentos.

Desde el Parque municipal Presidente Néstor Kirchner, a tres días de las elecciones legislativas, el Presidente puntualizó: "La Argentina está avanzando. Está avanzando gracias a Dios del modo que nosotros queremos: con producción y con trabajo".

"Siento que hemos hecho mucho y soy optimista. Creo que nosotros debemos seguir en esta senda. La primera condición que debemos cuidar siempre, porque permanentemente tratan de que no ocurra, es estar unidos. Lo esencial es la unidad de nuestro espacio", ponderó.

Asimismo, el jefe de Estado expresó: "Fuimos a la elección de septiembre convencidos de haber hecho muchas cosas en favor del cambio de la Argentina. Cuando un número importante de argentinos no nos había acompañado ese día, me dispuse individualmente a salir a escuchar".

"Fui escuchando y dándome cuenta de que muchos argentinos conocían ese crecimiento, pero el crecimiento a ellos todavía no les había llegado. Los escuché decir 'la inflación me preocupa' y 'los ingresos no me alcanzan'", describió.

Al respecto, Alberto Fernández indicó: "Nosotros escuchamos e hicimos: mejoramos asignaciones familiares, mejoramos mes a mes la tarjeta Alimentar y la AUH".

"Los dos años que quedan voy a dejar todo de mí para poder seguir mirando a los ojos a cada argentino. Seguiré trabajando incansablemente para que toda la Argentina se ponga de pie, para que cada chico vaya al colegio, para que los científicos no se vayan, para que los que tienen planes los puedan reemplazar por empleo seguro", concluyó.