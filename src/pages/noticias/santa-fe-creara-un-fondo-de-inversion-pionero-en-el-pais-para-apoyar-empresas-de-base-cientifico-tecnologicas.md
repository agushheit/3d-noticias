---
category: Estado Real
date: 2021-08-05T06:00:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/BIOCERES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe creará un fondo de inversión pionero en el país para apoyar empresas
  de base científico tecnológicas
title: Santa Fe creará un fondo de inversión pionero en el país para apoyar empresas
  de base científico tecnológicas
entradilla: Será una herramienta público-privada que incentivará el nacimiento de
  500 startups en la provincia. Se espera aportar U$D 300 millones durante la próxima
  década.

---
El gobernador Omar Perotti y el CEO de Bioceres, Federico Trucco, firmarán este jueves en Rosario el Convenio Marco de Colaboración para la creación de SF500, un fondo de inversión pionero en el país para apoyar empresas de base científico tecnológicas. Además, el titular de la Casa Gris rubricará el decreto que crea el Consejo de Economía de Conocimiento.

SF500 será un fondo de inversión con el que se espera aportar U$D 300 millones durante la próxima década para la promoción de startups con “altura inventiva”, integrará fondos públicos y privados, y apunta a retroalimentar la ciencia del futuro. Una iniciativa pionera en el país que busca facilitar el nacimiento de las próximas 500 startups de base científica-tecnológica de Argentina en los próximos 10 años, e impulsar una transformación cultural que favorezca el diálogo entre la ciencia, las empresas y el Estado.

Del evento participarán, además del CEO de Bioceres, empresa santafesina líder en biotecnología y principal socio del gobierno en esta integración, el director y socio fundador de Terragene, Esteban Lombardía; el cofundador y presidente de Agrofy, Alejandro Larosa; y el representante de la Comisión de Innovación en Bolsa de Comercio de Rosario, Diego Viruega.

También estarán referentes del ecosistema de innovación, aceleradoras, incubadoras, centros y polos tecnológicos, municipios e instituciones académicas públicas y privadas de la provincia de Santa Fe como la Universidad Nacional de Rosario, la Universidad Nacional del Litoral, la Universidad Nacional de Rafaela y Universidad Austral.

En ese marco, el ecosistema científico narrará en primera persona el marco en que se producen las firmas, que resultan la institucionalización de lo trabajado en este año y medio, con la creación del Consejo de Economía del Conocimiento, y el nacimiento de una nueva herramienta de inversión que surge de las propias necesidades del ecosistema.