---
category: Agenda Ciudadana
date: 2021-01-18T09:00:37Z
thumbnail: https://assets.3dnoticias.com.ar/siprus.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: 'Nuevo paro de los trabajadores de la salud: será por 72 horas'
title: 'Nuevo paro de los trabajadores de la salud: será por 72 horas'
entradilla: Trabajadores nucleados en Siprus retomarán las medidas de fuerza este
  martes 19, miércoles 20 y jueves 21, en reclamo de un aumento salarial por parte
  del gobierno provincial

---
Los trabajadores de la salud nucleados en Siprus continuarán con las medidas de fuerza reclamándole al gobierno provincial una mejora salarial. El mismo será por 72 horas, desde el martes 19 al jueves 21 de enero. El mismo será sin asistencia a los lugares de trabajo.

A través de un comunicado dieron a conocer los motivos. " Luego del importante acatamiento de las medidas de fuerza de la semana pasada y ante la falta de convocatoria al sindicato mayoritario de los profesionales, se ratifica el paro de 72 horas para martes 19, miércoles 20 y jueves 21.

En la reunión de conciliación obligatoria, al gremio minoritario le habrían ofrecido una propuesta muy similar a la anterior, poniendo como bonificable un 1,5% (560 pesos para el cargo de 24 horas) y 10 días de licencia extraordinaria ( no están autorizadas para usar ahora). Pretenden así cerrar un acuerdo con Amra (gremio que siempre firma propuestas a la baja) sabiendo que la inmensa mayoría de los profesionales de la salud, con la representación de Siprus, nos negamos a garantizar que los trabajadores pierdan sus derechos".

Además, rechazaron que “el 70% de los trabajadores de la salud tenga aumentos por debajo de la inflación, perjudicando más fuertemente a terapistas e intesivistas, a las guardias y a APS y a los que tienen más antigüedad, siendo que estos grupos pierden hasta 10% frente a la inflación”.