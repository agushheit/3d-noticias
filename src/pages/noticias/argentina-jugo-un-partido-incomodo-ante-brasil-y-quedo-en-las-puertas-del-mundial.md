---
category: Deportes
date: 2021-11-17T06:15:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/empate.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Argentina jugó un partido incómodo ante Brasil y quedó en las puertas del
  Mundial
title: Argentina jugó un partido incómodo ante Brasil y quedó en las puertas del Mundial
entradilla: 'En el estadio Bicentenario de San Juan, Argentina igualó 0-0 Brasil por
  la 14ª fecha de las Eliminatorias y quedó en las puertas de clasificar al Mundial

'

---
Argentina, segundo en las Eliminatorias Sudamericanas, se medía este martes al líder y ya clasificado Brasil en San Juan en busca del pasaporte al Mundial Qatar 2022, por la 14ª fecha de las Eliminatorias Sudamericanas, sin embargo, la tensión pudo más y la historia terminó 0-0. El partido se disputó en el estadio Bicentenario de San Juan, con arbitraje del uruguayo Andrés Cunha. Con este resultado, no pudo sellar su clasificación.

**Formaciones**

Argentina: Emiliano Martínez; Nahuel Molina, Cristian Romero, Nicolás Otamendi, Marcos Acuña; Rodrigo De Paul, Leandro paredes, Giovani Lo Celso; Lionel Messi, Lautaro Martínez y Ángel Di María. DT: Lionel Scaloni.

Brasil: Alisson; Danilo, Marquinhos, Éder Militao, Alex Sandro; Fabinho, Fred, Lucas Paquetá; Raphinha, Matheus Cunha y Vinicius Juniors. DT: Tite.

Árbitro: Andrés Cunha (Uruguay).

Estadio: Estadio Bicentenario (San Juan).