---
category: La Ciudad
date: 2021-11-16T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/juanjosaul.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Perman vs. Piedrabuena: la competencia entre las sorpresas, seccional por
  seccional en la ciudad'
title: 'Perman vs. Piedrabuena: la competencia entre las sorpresas, seccional por
  seccional en la ciudad'
entradilla: 'El hombre del megáfono se impuso en 11 seccionales de la ciudad, mientras
  que el cantante de cumbia lo hizo en siete

'

---
En las elecciones generales de este domingo dos santafesinos que no vienen de la militancia política se ganaron una banca en el Concejo Municipal y fueron las grandes sorpresas. Se trata de Saúl Perman y Juan José Piedrabuena. Perman se hizo conocido por recorrer durante años la ciudad en bicicleta y con un megáfono en la mano para cantar sus consignas de alimentación consciente. El otro, por brillar cada vez que se sube a un escenario.

En el mano a mano, el activista de la bicicleta ganó en 11 seccionales. Mientras que el cantante de cumbia lo hizo en las siete restantes y, particularmente en la seccional de la Subcomisaría Primera, Piedrabuena les ganó a todos los candidatos.

Al momento de ir al recuento total de votos en la ciudad Perman aventajó a Piedrabuena por 1.588 sufragios, según el escrutinio provisorio. El activista consiguió 19.542 contra los 17.954 del cantante. Esos guarismos les permiten a ambos lograr una banca en el Concejo Municipal. Solo resta que se realice el escrutinio definitivo que comenzará este miércoles y que se espera que finalice el próximo sábado.

El candidato a concejal por Unión Federal llamó a votar por los candidatos a cargos nacionales de la lista de Omar Perotti.

En los últimos días de campaña se viralizó un video de Piedrabuena pidiendo el apoyo a las listas nacionales del Frente de Todos. La aparición del cantante sosteniendo las boletas de Marcelo Lewandowski y María de los Ángeles Sacnun para el Senado y de Roberto Mirabella para diputados despertó una serie de conjeturas. Finalmente el Frente de Todos solo consiguió una banca y Alejandro Rossi, segundo en la lista de Jorgelina Mudallel, se quedó fuera del Concejo. Esa banca quedó en manos de Piedrabuena. Mientras que la que consiguió Perman fue la tercera banca que puso en juego el Frente Progresista que este domingo solo llegó a lograr dos lugares.