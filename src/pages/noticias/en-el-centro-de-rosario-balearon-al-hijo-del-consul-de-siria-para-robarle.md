---
category: Agenda Ciudadana
date: 2020-12-12T13:16:44Z
thumbnail: https://assets.3dnoticias.com.ar/robo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: En el centro de Rosario balearon al hijo del Cónsul de Siria para robarle
title: En el centro de Rosario balearon al hijo del Cónsul de Siria para robarle
entradilla: Fue en San Juan y Oroño, cerca de las 15 de este viernes. Los ladrones
  llegaron en moto y escaparon con una suma no determinada de dinero. La víctima está
  fuera de peligro.

---
Este viernes, cerca de las 15, el hijo del Cónsul de Siria fue asaltado en pleno centro de la ciudad de Rosario. **El hijo del diplomático resultó baleado, aunque su vida no corre peligro**. 

El atraco se produjo en San Juan y Oroño, cuando dos personas que iban a bordo de una motocicleta se pusieron a la par del auto en el que viajaba la víctima, le dispararon y se llevaron una bolsa con una indeterminada cantidad de dinero.

Según las primeras informaciones, el hombre fue baleado en una pierna y su vida estaría fuera de peligro. El episodio ocurrió en San Juan al 2100. 

Allí, el conductor de una Renault Capture, que conducía Damián Abdelmalek (42 años, hijo del cónsul Jorge Fanos Abdelmalek), quien fue interceptado por dos ladrones a bordo de una moto roja, según indicaron testigos que presenciaron el asalto.

"Sí, es el cónsul de Siria", confirmó un allegado a la víctima, quien fue trasladada con un disparo en el muslo derecho, aunque se encontraría fuera de peligro. Al tiempo que el hermano de la víctima confirmó que "lo siguieron para robarle y le dispararon".

<br/>

![](https://assets.3dnoticias.com.ar/robo1.jpg)

***

Uno de los testigos confirmó que "hubo tres disparos, uno de ellos impactó en el muslo del joven, pero aparentemente no afectó estructuras óseas ni vasculares". Y detalló que "el otro disparo, el más llamativo, cruzó todo el habitáculo y tuvo orificio de salida en la ventana delantera del acompañante".

"Escuché estampidos y ví una moto con dos jóvenes, pero estaba muy lejos", agregó. Y señaló que "se llevaron dos bultos, que aparentemente sería dinero".

“Estábamos almorzando cuando se escucharon los disparos; me asomo al balcón y el delincuente estaba al lado de la Capture”, contó un vecino que alcanzó a ver parte de la secuencia de lo que, en principio, se trataría de una salidera bancaria.

En ese sentido, el testigo señaló a Radio 2 que el conductor arrojó “una bolsa negra al pavimento y otro (un cómplice) la agarró”. Tras ello, los hampones escaparon a bordo de la moto por San Juan hacia el oeste y se perdieron entre los autos.

Mientras tanto, una ambulancia privada y un móvil del Sies llegaron al lugar para asistir a la víctima que fue trasladada hacia el Sanatorio de la Mujer.

“Tiene una herida en la pierna derecha en la parte del muslo, donde le impactó una bala, lo llevaron a una ambulancia del Sies. Está consciente”, comentó.

**Inmediatamente, se desplegó un importante operativo policial en la zona**.

<br/>

![](https://assets.3dnoticias.com.ar/robo2.jpg)

***