---
category: La Ciudad
date: 2021-09-22T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARBIJO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Médicos de Santa Fe pidieron cautela ante las flexibilizaciones y defendieron
  el uso del barbijo
title: Médicos de Santa Fe pidieron cautela ante las flexibilizaciones y defendieron
  el uso del barbijo
entradilla: Desde Amra y el Colegio de Médicos de Santa Fe, los profesionales se pronunciaron
  sobre las nuevas medidas de convivencia. "No van más allá de lo que se ve en la
  calle", aseguran.

---
Luego de las medidas dispuestas por Nación, a días del revés electoral en las PASO, dando lugar a nuevas flexibilizaciones en el marco de la pandemia, los médicos de Santa Fe se pronunciaron al respecto.

Los profesionales de la Salud señalaron que están de acuerdo en que se flexibilicen nuevas actividades, aunque llamaron a la coherencia y a la cautela para realizarlo.

Al respecto, el presidente del Colegio de Médicos de Santa Fe (primera circunscripción), Dr. Daniel Rafel, destacó: "Son medidas que en algún momento tienen que ir dándose y la sociedad debe retomar una vida con mayor normalidad. Clínicamente, la situación en los hospitales da lugar para que se esperen estas medidas".

Además, el especialista advirtió que las nuevas flexibilizaciones obedecen a una lógica que "no va mucho más allá de lo que ya está en vigencia y que es mayor el ruido que se está haciendo sobre estas decisiones que su contenido". En este sentido, hizo hincapié en el avance de la vacunación que "a duras penas" está pudiendo cumplir con su objetivo, con el efecto que esto tiene en la desocupación de camas.

Otro de los profesionales médicos con los que dialogó UNO fue Néstor Rossi, miembro de comisión directiva de la Asociación de Médicos de la República Argentina (Amra), seccional Santa Fe. En este punto, Rossi manifestó: "está bien y hay que flexibilizar, pero hay que ser cautelosos, coherentes y respetuosos".

Según la opinión del especialista, "las actividades deben liberarse forma gradual con prioridad en la educación, como corresponde y en un tiempo programado. No porque haya una nueva política de estado debemos salir todos a las apuradas, debe haber una libertad controlada porque sino será un caos".

"Estoy absolutamente de acuerdo del uso de barbijo al aire libre. El viento es el mayor vector de comunicación del virus, aunque en espacios abiertos estando solo no sería acertado manteniendo cierto distanciamiento y haciendo las cosas como corresponde", afirmó Rossi sobre el correcto uso de barbijo al aire libre luego de que el gobierno nacional decrete su uso opcional en este ámbito.

Como recomendación, el directivo de Amra agregó: "Corresponde que se use barbijo en lugares con conglomerados de gente como pueden ser plazas, filas, mercados, etc".

La ministra de Salud de la provincia, Sonia Martorano, hizo referencia a la batería de anuncios que realizó el gobierno nacional vinculado a mayores flexibilizaciones en el marco de las normas preventivas contra el Covid-19. La titular de la cartera sanitaria santafesina señaló que “estas definiciones que se anunciaron a nivel nacional requieren de un decreto (nacional) que las ratifique. Esto aún no está"

Además, Martorano recordó que una vez que Nación publique las nuevas normas de convivencia en el boletín oficial, "cada provincia determinará si adhiere o no en los conceptos que crea pertinente.