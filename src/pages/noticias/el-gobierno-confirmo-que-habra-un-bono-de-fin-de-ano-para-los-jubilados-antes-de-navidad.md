---
category: Agenda Ciudadana
date: 2021-12-10T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/JUBILACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: El Gobierno confirmó que habrá un bono de fin de año para los jubilados antes
  de Navidad
title: El Gobierno confirmó que habrá un bono de fin de año para los jubilados antes
  de Navidad
entradilla: 'Tras haberlo desmentido la semana pasada, la portavoz presidencial confirmó
  la medida en conferencia de prensa.

'

---
De cara a fin de año, finalmente el Gobierno resolvió impulsar un bono destinado a jubilados a ser percibido antes de las Fiestas. A pesar de haber manifestado la semana pasada que la disposición no estaba en agenda, la portavoz presidencial Gabriela Cerruti terminó por confirmarlo este jueves en conferencia de prensa.

Cabe destacar que horas después de la negativa de Cerruti el jueves pasado, la titular de la ANSES Fernanda Raverta manifestó que el Gobierno "evaluaba" la medida, inscribiendo un nuevo capítulo de contradicciones en la gestión de Fernández, atribuidas al "dinamismo" de la realidad, tal y como señaló la portavoz.

"Como lo venimos haciendo desde que asumimos como gobierno, seguimos pensando en cómo llegar, atender y asistir mejor a nuestros jubilados y, es por eso, que estamos evaluando la posibilidad de un bono de fin de año", detalló Raverta.

"El presidente le pidió a Fernanda Raverta, y al ministro (Martín) Guzmán que implemente un bono para los jubilados antes de la navidad", argumentó la portavoz, y declaró que "aún se está trabajando en el tema". " Los detalles de cuándo y de qué monto los sabremos cuando el ministro termine de definirlos", aseguró.

Por su parte, PAMI anunció el pago de un bono festivo y un bono extraordinario para el mes de diciembre destinado a los 700.000 jubilados afiliados a la entidad. Los montos que percibirán los afiliados parten de $5000, y se determinaran en base a las ubicaciones geográficas y al tipo de bolsón que percibe cada jubilado.

“PAMI puso en marcha esta medida desde el inicio de la emergencia sanitaria en reemplazo de los bolsones de alimentos que se distribuían a través de centros de jubilados y pensionados que durante la pandemia debieron permanecer cerrados”, señaló la titular de la entidad Luana Volnovich.