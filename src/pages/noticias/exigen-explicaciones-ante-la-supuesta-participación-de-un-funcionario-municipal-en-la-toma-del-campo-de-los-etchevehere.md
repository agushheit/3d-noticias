---
layout: Noticia con imagen
author: "-"
resumen: Exigencia al municipio
category: Estado Real
title: Exigen explicaciones ante la supuesta participación de un funcionario
  municipal en la toma del campo de los Etchevehere
entradilla: El interbloque de concejales de Juntos por el Cambio de la ciudad de
  Santa Fe presentó este martes un pedido de informes al municipio.
date: 2020-10-27T17:54:45.476Z
thumbnail: https://assets.3dnoticias.com.ar/pereira-carlos.jpg
---
La denuncia nace del propio Luis Etchevehere, ex ministro de Agricultura, Ganadería y Pesca de la Nación, quien asegura que funcionarios del gobierno nacional acompañaron la toma del campo perteneciente a su familia y pasaron la noche dentro de “Casa Nueva”, junto al resto de los usurpadores.

En diálogo con 3D Noticias, Carlos Pereira, concejal de Santa Fe, detalló los motivos del pedido realizado al municipio: “Al principio se pensó que era un funcionario nacional, pero más tarde se precisó que pertenecía a la ciudad. El propio Etchevehere twiteó un video esa misma noche de la toma, donde se lo veía a este funcionario municipal. Se trata de Sebastián Correa, que está al frente del área de Economía Social de la Municipalidad de Santa Fe. Fue designado el 10 de diciembre por Emilio Jatón.”

Asimismo, sostuvo que “lo más grave de la situación es que este funcionario está convalidando y siendo protagonista de un hecho delictivo, lo cual es inadmisible para un funcionario público”.

El informe pide saber si el intendente estaba al tanto de la situación y si se tomó o se piensa tomar alguna medida. “Este hecho reviste gravedad. De ninguna manera el Estado, ni el Estado de Santa Fe, puede convalidar que alguien que lo representa esté cometiendo un delito como es la usurpación de tierras” finalizó.

[Leer pedido de informes](/uploads/pedido-informes-pereira.pdf)