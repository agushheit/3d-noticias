---
category: La Ciudad
date: 2021-09-02T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/insegurodad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'De luto: vecinalistas piden a las autoridades que "se hagan cargo frente
  a la inseguridad"'
title: 'De luto: vecinalistas piden a las autoridades que "se hagan cargo frente a
  la inseguridad"'
entradilla: 'La jornada de visibilización de la problemática se realizó frente al
  Monumento del Brigadier. Hubo reclamos por más patrullajes, cámaras de seguridad
  e incluso que se reactive la Policía Comunitaria. '

---
Al cartel de verde y blanco de "Santa Fe" que está en la Costanera Oeste se le colocó una tela negra; al lado, en una luminaria, se colgó otra del mismo color. Era la simbología del "luto", una movida que empezó la Red de Vecinales por Seguridad, que este miércoles realizó una concentración para advertir sobre los altos índices de inseguridad que registra la ciudad capital, y reclamar medidas para frenar el delito. Las actividades de visibilización de la problemática continuarán los próximos días.

Enfrente, el Monumento del Brigadier Estanislao López lucía tristemente un cartel donde estaba todo dicho: "Basta de inseguridad". En la concentración había vecinos de varios barrios que llegaron para sumarse a la movida y se aguantaron el repentino chubasco que cayó mientras comenzaba la conferencia de prensa. El mensaje era claro y contundente: Que las autoridades políticas, en todos sus estamentos, se hagan cargo: que hagan lo que deben hacer para frenar la escalada del delito.

La vocera fue Susana Spizzamiglio, una de las referentes de la Red. "Esta movida seguirá con cartelería sobre los bulevares, con volantes por todos los barrios. La problemática de la inseguridad es una bola de nieve que se viene acrecentando, no por las causas sino por las consecuencias; y las consecuencias son todo lo que debieran hacer todos los gobernantes y no hacen en materia de lucha contra el delito", fustigó, visiblemente ofuscada.

Según el seguimiento que desde la Red se hace sobre las estadísticas del Ministerio de Seguridad, el MPA y el Observatorio de Seguridad, en los últimos meses los actos delictivos recrudecieron en una dimensión que es mucho más grande de lo que se pensaba, advirtió la vecinalista de Fomento 9 de Julio. "Se tienen que dar políticas de Estado y estrategias que determinen que algo está pasando para que cambien justamente esas estrategias, que evidentemente no están dando resultado".

Y dio un dato: desde diciembre de 2019, se jubilaron 1.019 policías. "Recién ahora se va a poder cubrir esas cifras. ¿Faltan hoy policías? Todo indica que sí. Y hay que poner cámaras de vigilancia. En Alto Verde los vecinos no tienen cámaras, algo que están pidiendo, y estamos hablando de un barrio de 20 mil habitantes. Entonces, les decimos a las autoridades: 'Zapatero a sus zapatos': a gobernar y gestionar en materia de seguridad, y nosotros como entidad civil acompañaremos", fue su mensaje.

\-En materia de las competencias del municipio en lo que puede aportar para frenar el delito (patrullajes de la GSI, más iluminación, desmalezamiento, etcétera), ¿cuáles son las falencias que notan?, consultó El Litoral a Spizzamiglio.

\-Por ejemplo, no sabemos sobre la incompatibilidad de las cámaras del Centro de Monitoreo municipal con las de la provincia. Yo quisiera saber si ya están trabajando sobre eso, si esa compatibilidad está operativa en términos técnicos, sobre todo en las arterias principales de la ciudad.

El municipio hace tareas de prevención, pero podría hacer mucho más, como entregar más botones de pánico; y no hay un equipo interdisciplinario que atienda a las víctimas del delito. Porque el delito fractura no sólo al que lo padece, sino a su grupo familiar e incluso al barrial, a los vecinos más cercanos. También la Municipalidad podría extender las líneas de seguridad en cámaras en lugares no hay.

El gobernador Omar Perotti anunció una partida para arreglar comisarías. Hace mucho tiempo que están pidiendo esto las vecinales Nueva Santa Fe y Favaloro, para que arreglen esa "pocilga" que hoy es la comisaría de ese sector donde viven más de 20 mil habitantes.

\-La Policía Comunitaria (parejas de dos efectivos patrullando a pie, en una suerte de policía de cercanía con el vecino) se había implementado hace unos años y dio buenos resultados, pero hoy se desactivó. ¿Considera que debiera volver?

\-Nosotros estamos pidiendo efectivos caminantes en Aristóbulo al 4100. A la Comunitaria la reclama barrio Centenario, porque daba resultado. Lo mismo Barranquitas. Hablamos del policía de cercanía, estrategia que se tomó del modelo español con buen criterio, que dio muy buenos resultados en siete barrios de la ciudad. Hay falencias en el 911 donde notamos que un patrullero demora entre 45 minutos y una hora en llegar a un lugar donde se produjo un delito.