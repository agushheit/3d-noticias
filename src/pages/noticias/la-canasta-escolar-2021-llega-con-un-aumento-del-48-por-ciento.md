---
category: Agenda Ciudadana
date: 2021-02-02T06:59:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/carrito-de-compras-con-utiles-escolares-dentro.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3D Noticias '
resumen: La Canasta Escolar 2021 llega con un aumento del 48 por ciento
title: La Canasta Escolar 2021 llega con un aumento del 48 por ciento
entradilla: 'A la espera del regreso a las escuelas, las familias deberán elegir ahora
  entre las compras controladas y el ingenio para reinventar útiles usados. '

---
En medio de la incertidumbre por el regreso a las clases presenciales, una de las grandes incógnitas es la compra o no de la canasta escolar y los uniformes.

Lo cierto es que si decidís pasar por una librería para adquirir una nueva canasta escolar, te vas a encontrar con aumentos . **“Los productos importados tuvieron más aumento que los nacionales, abarcan entre el 35 y el 40 por ciento. Lo más importante es que los productos de librería son baratos y tenés mucho para elegir”**, expresó Daniel Iglesias López, Presidente de la Cámara de Papelerías, Librerías y Afines (CAPLA). 

Los precios son variados: “Los lápices negros, de primera marca, están a 20 pesos; sacapuntas a 18; cuadernos de 50 hojas, tapa blanda, 75 pesos; 12 lápices largos, 125, un café en Palermo cuesta lo mismo y te dura cinco minutos, los lápices todo el año. Se gastan mil pesos para poner un chico en la escuela“, detalló López. 

Los clientes anticipan que las compras serán limitadas este 2021. El objetivo es paliar los aumentos generalizados a la espera de una mayor incertidumbre económica: “Los precios en general están triplicados de lo que eran hace tres meses. Da la impresión que se quieren poner al día en este tiempo de lo que no se pudo vender. Las cosas están dolarizadas y los sueldos no”, declaró Marcela, clienta. 

La canasta escolar 2021 llega con aumentos de hasta el 48 por ciento, muy por encima de la inflación interanual. A la espera del regreso a las escuelas, las familias deberán elegir ahora entre las compras controladas y el ingenio para reinventar útiles usados. 