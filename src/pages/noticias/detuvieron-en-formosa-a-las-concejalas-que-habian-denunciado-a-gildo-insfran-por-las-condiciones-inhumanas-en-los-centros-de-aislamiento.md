---
category: Agenda Ciudadana
date: 2021-01-22T04:14:46Z
thumbnail: https://assets.3dnoticias.com.ar/detencion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Infobae
resumen: Detuvieron en Formosa a las concejalas que habían denunciado a Gildo Insfrán
  por las condiciones inhumanas en los centros de aislamiento
title: Detuvieron en Formosa a las concejalas que habían denunciado a Gildo Insfrán
  por las condiciones inhumanas en los centros de aislamiento
entradilla: Gabriela Neme y Celeste Ruiz Díaz fueron alojadas en una dependencia policial
  luego de haber sido apresadas mientras se manifestaban por la situación de las personas
  que realizan el confinamiento por COVID

---
Las concejalas **Gabriela Neme y Celeste Ruíz Díaz**, que el fin de semana pasado [**habían denunciado al gobernador Gildo Insfrán por las “condiciones inhumanas” en un centro de aislamiento de Formosa**](https://www.infobae.com/politica/2021/01/19/denunciaron-hacinamiento-y-condiciones-inhumanas-en-un-centro-de-atencion-de-coronavirus-en-formosa-que-explicaron-desde-el-gobierno-provincial/), **fueron detenidas este jueves por la tarde mientras se manifestaban en la puerta de una escuela** de la ciudad capital de la provincia.

El momento de la liberación de NemeTras pasar detenidas alrededor de una hora, **Neme y Ruíz Díaz dejaron la comisaría.**

Neme, enrolada en el peronismo disidente, y Ruiz Díaz, de la UCR, **estaban manifestándose de manera pacífica en la puerta de la Escuela N° 18 de Formosa junto a otros vecinos cuando fueron abordadas por varios efectivos policiales, quienes las esposaron y las llevaron a la Comisaría 1ª.** El reclamo estaba motivado por la situación de 19 personas, que son obligadas a realizar el aislamiento en ese colegio a pesar de haber dado negativo en sus tests de coronavirus, según la letrada.

Desde los primeros días del año, cuando la ciudad de Formosa –además de otras localidades de la provincia– volvió a Fase 1, **las fuerzas de seguridad siguen un estricto protocolo por el que no se permite ningún tipo de aglomeración de personas o manifestación en la vía pública.** En ese marco, oficiales de la Policía arrestaron a las concejalas, mientras eran grabados por los teléfonos celulares de los vecinos que también se encontraban en la puerta de la escuela.

Varias figuras de la política se manifestaron sobre estos hechos apenas trascendieron. Uno de ellos fue el diputado nacional por Mendoza **Alfredo Cornejo**, quien publicó en su cuenta oficial en Twitter: “**Exigimos la inmediata liberación de las concejalas @CelesteRuizDiaz y @gabrielaneme**, detenidas en la ciudad de Formosa por la violación de los derechos humanos que lleva adelante el gobernador Gildo Insfrán”.

La diputada nacional por la Ciudad de Buenos Aires **Paula Oliveto** se manifestó en el mismo sentido: “**Ahora detienen a concejales de Formosa que reclamaban al gobierno de Insfrán.** Los chorros que se robaron el país gozando de libertad y los que pelean por justicia presos. Se ve que ese es el modelo que defiende el kirchnerismo. Orgullosa de ser oposición a este modelo autoritario”.

La **Juventud Radical** utilizó su cuenta en la red social para dirigirse al presidente, **Alberto Fernández**: “El gobierno de @insfran_gildo detuvo a las concejalas @CelesteRuizDiaz y @gabrielaneme por acompañar a las víctimas de violaciones de derechos humanos en Formosa. Exigimos su liberación inmediata y la intervención del Estado Nacional como garante de derechos. **@alferdez, es tu responsabilidad**”.

El legislador **Álvaro de Lamadrid** escribió: “Acaban de detener en Formosa a las concejalas @CelesteRuizDiaz y @gabrielaneme, quienes habían denunciado la violación de los derechos humanos de Gildo Insfrán. **Exigimos la inmediata liberación de los detenidos. Estos sí son presos políticos**”.

**Alejandra Lordén**, diputada de la provincia de Buenos Aires y vicepresidenta del Comité Nacional de la Unión Cívica Radical, publicó: “Desde la @UCRNacional solicitamos la inmediata liberación de @gabrielaneme y @CelesteRuizDiaz, quienes fueron detenidas por el gobierno autoritario de Gildo Insfrán. **Solicitamos al gobierno nacional su intervención urgente**”.

![](https://assets.3dnoticias.com.ar/cornejo.jpg)

![](https://assets.3dnoticias.com.ar/oliveto.jpg)

![](https://assets.3dnoticias.com.ar/jr.jpg)

Algunas de las manifestaciones públicas sobre el arresto de las concejalas (Twitter)

Neme había denunciado formalmente el sábado pasado a Insfrán y al ministro de Gobierno, Justicia, Seguridad y Trabajo provincial, **Jorge Abel González**, **por propagar la pandemia del coronavirus e incumplir con sus deberes de funcionarios públicos,** después de que se dieran a conocer algunos videos desde el interior del Estadio Cincuentenario de la capital provincial, que funciona como un centro de atención sanitaria por la pandemia del coronavirus.

Según señaló Neme a **Infobae**, en el centro de atención sanitaria no solo se encuentran pacientes con COVID-19, sino también personas que **aún no recibieron el resultado de su hisopado**. La letrada, que se comunicó con personas que se encuentran en el lugar, asegura que casi no hay distanciamiento entre sanos y contagiados y que hasta comparten el mismo baño.

El centro de atención sanitaria montado en el Estadio Cincuentenario de Formosa. Las concejalas denunciaron hacinamiento y "condiciones inhumanas" en el lugar

“Hago la denuncia por incumplimiento del artículo 205 del Código Penal porque están propagando la pandemia. En el Cincuentenario están todos juntos y tienen el patio vallado. No los dejan salir y comparten los baños, **entonces lo que hay son centros de contagio**”, explicó Neme.

En su denuncia también habla de supuesto incumplimiento de las tareas de los funcionarios públicos. En ese sentido, indicó: “La denuncia es en concurrencia con la violación al artículo 248 del Código Penal, debido a las irregularidades colaterales de esta situación: los que hacen los hisopados muchas veces son voluntarios y a las personas no les hacen firmar un consentimiento como indica la ley. **No les dan asistencia psicológica a todas estas personas que están en un estado de incertidumbre, pánico, angustia y depresión terrible**”.

Ante esta situación, el senador formoseño **Luis Naidenoff**, presidente del Interbloque de Juntos por el Cambio, [**presentó un hábeas corpus colectivo y correctivo en el Juzgado Federal de la provincia**](https://www.infobae.com/sociedad/2021/01/20/luis-naidenoff-en-formosa-nadie-va-a-querer-hisoparse-por-temor-a-quedar-privado-de-su-libertad/) **para pedir que el Estado se abstenga de alojar de manera compulsiva a las personas con COVID-19 positivo o que hayan tenido contacto estrecho con un caso confirmado, y se disponga el aislamiento en domicilios particulares.** También solicitó que se corrijan las condiciones en las que se cumple el confinamiento en los centros de aislamiento, particularmente en el montado en el Estadio Cincuentenario.

“Alrededor de 250 personas fueron alojadas (en el Estadio Cincuentenario) por ser casos positivos leves o de contacto estrecho. **No se respetan las mínimas condiciones de higiene y salubridad ni las recomendaciones sobre el aislamiento efectuadas por el ministerio de Salud de la Nación**”, se precisa en el hábeas corpus.

Al enterarse de la detención de las concejalas, Naidenoff utilizó su cuenta en Twitter y publicó: “[**Violencia, brutalidad y silenciamiento**.](https://www.infobae.com/politica/2021/01/19/formosa-denunciaron-violencia-contra-miembros-de-comunidades-wichis-que-intentaban-cobrar-subsidios-provinciales/) La fórmula de la represión del gobierno formoseño contra quienes denunciás las violaciones a los derechos humanos en la provincia. Exigimos la liberación urgente de @CelesteRuizDiaz y @gabrielaneme”.

![](https://assets.3dnoticias.com.ar/naidenoff.jpg)