---
category: Estado Real
date: 2021-11-03T06:00:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/vetes.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia participó de la apertura de una ronda de negocios para el sector
  de nutrición animal y productos veterinarios
title: La provincia participó de la apertura de una ronda de negocios para el sector
  de nutrición animal y productos veterinarios
entradilla: El encuentro virtual vinculó a empresas de Chile y Colombia con 16 pymes
  santafesinas potenciales proveedoras.

---
El ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, participó este martes de la apertura de una ronda de negocios virtual que vinculó a doce empresas de Chile y Colombia del sector de nutrición animal y productos veterinarios, con dieciséis pymes santafesinas, potenciales proveedoras del sector.

Dicha actividad fue organizada por la Secretaría de Comercio Exterior del Ministerio de Producción, Ciencia y Tecnología, en conjunto con la Agencia Argentina de Inversiones y Comercio Internacional (AAICI), el Consejo Federal de Inversiones (CFI), la Agencia Santa Fe Global y la Agencia de Inversiones y Comercio Exterior de La Pampa (I COMEX).

Del encuentro también participaron el secretario de Comercio Exterior, Germán Burcher; el titular del Área de Relaciones Internacionales del CFI, Norberto Mosca; la gerenta de Comercio Exterior de la Agencia I COMEX, Eugenia Paturlanne; y el representante de la empresa Garay SRL, Anibal Blandon.

Durante la apertura, Costamagna indicó: “Es una gran alegría poder estar en la apertura de este evento a sabiendas de que tenemos en la provincia de Santa Fe un banco de pruebas permanente para este tipo de empresas, que es muy exigente con empresas vinculadas a la producción láctea, porcina, de carnes y por supuesto de alimentos para mascotas, lo que sin lugar a dudas genera un marco de demandas y de selectividad en la alimentación, de profesionalismo e incluso de fórmulas personalizadas, ya que tenemos un grado de genética en los distintos sistemas productivos que exigen la mayor implementación de calidad a los fines de que puedan expresar todo su potencial genético”.

“Tenemos la convicción de que las empresas de nuestra provincia conocen a fondo la demanda de los distintos empresarios en cuanto a las calidades de los alimentos requeridos y por lo tanto están en condiciones de abastecer gran parte de los mercados nacionales e internacionales; cuando se exporta un producto a otro país, se está exportando el conocimiento lógico que se aplica para que estos alimentos puedan maximizar y adaptarse a las realidades territoriales de países como Colombia y Chile en algunos aspectos como el ambiente o el tipo de nutrición, por lo que implica un gran desafío”, finalizó el ministro.

Por su parte, Burcher destacó: “Es la ronda de negocios número 20 que organizamos en lo que va del año; desde la Agencia Santa Fe Global hemos realizado 15 rondas internacionales y cinco nacionales de desarrollo de proveedores a través de la plataforma web”. Asimismo continuó: “Tenemos la expectativa de poder volver a participar de ferias, misiones y rondas de negocios presenciales, mientras tanto ha sido una experiencia muy positiva, ya que pudimos cerrar las distintas operaciones realizadas”.

A su turno, Paturlanne se mostró agradecida por el trabajo conjunto con la provincia de Santa Fe y luego expresó: “Nuestro desafío es que las empresas participen activamente, que se adueñen de esta ronda de negocios y puedan establecer contactos importantes con potenciales clientes del exterior”.

Para finalizar, Blandon valoró la importancia del evento para las empresas santafesinas destacando que “este tipo de rondas son vitales para poder mostrar el potencial productivo que tiene la provincia de Santa Fe en particular, ya que contamos con una amplia gama de productos de excelente calidad, materias primas y servicios de investigación y desarrollo”.

Las firmas provinciales vendedoras que participaron de la actividad son oriundas de las localidades de Rosario, Santa Fe, Venado Tuerto, Arteaga, Recreo, Funes, Pilar, Esperanza y Calchaquí.

Las mismas, ofrecen comederos de lona para animales, alimentos balanceados para bovinos, cerdos, aves, suplementos alimenticios para perros, premezclas y sustitutos lácteos, sistemas de gestión y soluciones móviles, suplementos dietarios, prótesis dentales para bovinos, detector de celo bovino, medicamentos veterinarios, alimentos balanceados y concentrados proteicos destinados a la alimentación de rumiantes y suplementos minerales.