---
category: Agenda Ciudadana
date: 2020-12-16T12:30:55Z
thumbnail: https://assets.3dnoticias.com.ar/clandestinas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: LT10'
resumen: 'Fiestas clandestinas: para evitarlas piden que los municipios organicen
  eventos'
title: 'Fiestas clandestinas: para evitarlas piden que los municipios organicen eventos'
entradilla: Se acercan las fiestas de fin de año y, con la llegada del verano, los
  encuentros clandestinos van en aumento. La solución es darle el poder a intendentes
  y presidentes comunales para organizar y controlar.

---
El senador provincial Lisando Enrico presentó un proyecto ante las autoridades provinciales para que **se le permita a municipios y comunas organizar encuentros nocturnos para los jóvenes, y así evitar las fiestas clandestinas o ilegales**.

El mismo fue presentado el jueves de la semana pasada en consecuencia de que los jóvenes se reúnen, “aumentó el nivel de los encuentros, y eso se hace de forma clandestina en quintas, campos e incluso en caminos rurales”. Por ese motivo, creen que es de vital importancia hacerlo de una “manera más ordenada y controlada, y creemos que hay que ir por lo segundo, hay que ir por delante de los problemas y no por detrás”.

Según Enrico, “**están habilitados los salones de fiestas para eventos familiares, están habilitados los casinos, los teatros para encuentros musicales y artísticos, y también hay que ver la manera de habilitar el espacio controlado de diversión nocturna**”.

Además, insiste en que la solución no tiene que ser especialmente un boliche, ya que “**hay comunas que nos pidieron hacerlo al aire libre, en clubes**. Pero evitar que sigan reuniéndose en lugares donde no hay ningún tipo de control ni medidas de seguridad”.

“A mí el pedido me lo hacen los intendentes y los presidentes comunales, que están corriendo fiestas clandestinas todos los fines de semana” concluyó el senador sobre una posible solución a una problemática de creciente preocupación.

Habrá que establecer protocolos y criterios, boliches con capacidad limitada, eventos en patios. “Creemos que el Gobernador va a firmar un decreto antes de las fiestas” declaró esperanzado.