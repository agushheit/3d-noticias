---
category: Agenda Ciudadana
date: 2022-01-05T06:00:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/Reunion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Acuerdo con el FMI: Guzmán, Kulfas, Pesce y Feletti definen los precios
  con empresarios'
title: 'Acuerdo con el FMI: Guzmán, Kulfas, Pesce y Feletti definen los precios con
  empresarios'
entradilla: 'El equipo económico recibió hoy a un grupo de empresarios de la alimentación,
  limpieza e higiene para avanzar en el pacto.

'

---
El equipo económico encabezado por, Martín Guzmán, recibió hoy a un grupo de empresarios de la alimentación, limpieza e higiene para avanzar en un acuerdo de precios, luego de obtener el visto bueno del FMI para incluir esta herramienta como complemento para contener la inflación.

"Hubo una actitud cooperativa y constructiva por parte de las empresas, quienes valoraron la importancia de los acuerdos de precios para ayudar a coordinar expectativas que permitan atacar el elemento de persistencia en el proceso inflacionario de una forma que sea consistente con el resto de las políticas macroeconómicas", señaló un comunicado del Palacio de Hacienda.

Durante la conversación "hubo un intercambio de comentarios entre los funcionarios y las empresarias y empresarios sobre el esquema de acuerdos de precios al que se apunta llegar en los próximos días".

En un cambio de paradigma, en su último documento el organismo multilateral consideró que la suba de precios en la Argentina es “multicausal” y abrió la puerta para que un pacto entre Gobierno, empresarios y sindicatos forme parte del programa de Facilidades Extendidas que se está negociando.

Con esa premisa, el jefe del Palacio de Hacienda almorzó hoy con Laura Barnator (Gerente General de Unilever para Argentina y Uruguay), Gabriela Bardin (Gerente General Argentina de P&G), Gonzalo Fagioli (Vicepresidente de Asuntos Corporativos, Legales y Compliance) de Quilmes, Abelardo Gudiño (Gerente General para Argentina y Uruguay de Coca Cola), Adrián Kaufman Brea (Gerente General de Comunicación Institucional y Asuntos Públicos) de Grupo Arcor, Maximiliano Lapidus (Gerente de Area Interior de La Serenísima) y Agustín Llanos (CEO de Molinos Río de la Plata).

Acompañaron a Guzmán el ministro de Desarrollo Productivo, Matías Kulfas; el Secretario de Comercio de Comercio Interior, Roberto Feletti, y el presidente del Banco Central (BCRA), Miguel Pesce.

La conformación de la comitiva oficial muestra la intención del Gobierno nacional de dar una señal de que la política macroeconómica estará alineada en todas sus variables para, desde ese compromiso, lograr la confianza del empresariado.

En el Palacio de Hacienda apuestan a cohesionar el frente interno para contrarrestar el escepticismo que existe sobre la Argentina en el plano internacional. Cerca de Guzmán insisten que mostrar una posición sólida como sociedad es fundamental para poder cerrar el programa con el organismo multilateral.

La estrategia oficial apunta a, en los próximos días, mantener reuniones con el sindicalismo para lograr su apoyo y luego reunir a todas las partes para sellar un pacto que permita darle a precios y salarios un sendero de previsibilidad. Contener la inflación es uno de los pilares del plan que se discute con el staff del FMI.

Lejos del 29% que inicialmente figuraba en el Presupuesto 2021, la inflación del año pasado que se conocerá oficialmente la semana próxima, se ubicará en torno del 50% interanual.

El Gobierno pretende que en ese camino las remuneraciones se ubiquen por encima de la inflación y de este modo recomponer el poder adquisitivo de los trabajadores para estimular la demanda.

En este esquema resulta esencial la política de emisión monetaria que implementará el Banco Central, de allí la presencia de Pesce.