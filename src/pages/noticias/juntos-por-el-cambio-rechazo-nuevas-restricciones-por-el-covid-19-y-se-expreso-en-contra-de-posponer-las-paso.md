---
category: Agenda Ciudadana
date: 2021-04-07T08:11:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/juntosxcambio.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Juntos por el Cambio rechazó nuevas restricciones por el Covid-19 y se expresó
  en contra de posponer las PASO
title: Juntos por el Cambio rechazó nuevas restricciones por el Covid-19 y se expresó
  en contra de posponer las PASO
entradilla: Los líderes del espacio se reunieron en Palermo. Estuvieron Macri y Vidal.
  "Debemos defender la mayor normalidad posible", plantearon ante la segunda ola.

---
Ante la inminente definición de nuevas medidas para frenar el crecimiento de los contagios de Covid-19, los principales referentes de Juntos por el Cambio criticaron hoy que el Gobierno busque "insistir con restricciones excesivas y mal calibradas" y se expresaron "en contra del cambio de las reglas de juego" en materia electoral, durante una reunión de la mesa nacional del espacio.

El encuentro presencial se llevó a cabo en el barrio de Palermo, donde analizaron no solo la idea de posponer las elecciones primarias (PASO) impulsada por el Gobierno sino también las medidas ante el crecimiento de casos de Covid-19 que son objeto de negociación entre el Gobierno nacional y la administración porteña de Horacio Rodríguez Larreta.

"Nos parece particularmente preocupante que frente al fracaso de la estrategia sanitaria del gobierno nacional la respuesta repetida sea insistir con restricciones excesivas y mal calibradas", señalaron los principales dirigentes y referentes parlamentarios del espacio a través de un comunicado.

Del encuentro participaron el ex presidente Mauricio Macri; la ex gobernadora bonaerense María Eugenia Vidal; Rodríguez Larreta; los titulares del PRO, Patricia Bullrich; de la UCR, Alfredo Cornejo, y de la Coalición Cívica, Maximiliano Ferraro. También los gobernadores Gerardo Morales (Jujuy), Rodolfo Suárez (Mendoza) y Gustavo Valdéz (Corrientes); el ex candidato a vicepresidente Miguel Pichetto y los referentes del Congreso Mario Negri, Juan Manuel López, Cristian Ritondo, Maricel Etchecoin, Martín Lousteau, Humberto Schiavoni y Luis Naidenoff.

En el documento, los dirigentes expresaron: "Estamos convencidos de que debemos defender la mayor normalidad posible, que implica garantizar el derecho a la educación, el trabajo y el ejercicio de las libertades fundamentales".

"Frente a la situación que estamos atravesando nos parece inoportuno estar discutiendo sobre cuestiones electorales", señalaron los referentes de la coalición opositora ante la propuesta del Gobierno de realizar las PASO en septiembre en lugar de agosto.

La oposición buscó así unificar una posición antes de la reunión que mantendrá el ministro del Interior, Eduardo de Pedro, con las autoridades del Congreso para negociar cambios en el cronograma electoral, y afirmó que están "en contra del cambio de las reglas de juego".

"Las reglas electorales son sagradas en una democracia. Hasta hoy no hemos recibido ninguna propuesta concreta por parte del poder ejecutivo nacional. Y de haberla, tiene que significar una mejora estructural y no solo para esta elección", advirtieron.

Además, apuntaron contra el Gobierno al considerar que la discusión no debe pasar por lo electoral sino por "la evolución de la situación sanitaria, la falta de vacunas en la escala necesaria, la inflación, el aumento de la pobreza, la inseguridad y los problemas cotidianos de los argentinos por la falta de empleo, el cierre de negocios y la caída de la actividad económica".

"Juntos por el Cambio está comprometido a trabajar junto a todos los ciudadanos que en este momento están en una situación de vulnerabilidad, en su educación, sus empleos, su vida, su salud mental y su bienestar", concluyeron los representantes de la alianza opositora en un escueto comunicado.

Los dirigentes mantuvieron bajo secreto el lugar en el cual llevaron a cabo el encuentro a partir de un acuerdo para evitar las filtraciones, tras los pases de factura internos que se dieron semanas atrás, cuando se publicó la imagen de una reunión por Zoom en la que se veía a Macri en la habitación y detrás de él a su esposa, Juliana Awada, en la cama.

Además, la reunión buscó bajar el ruido interno que generó la reunión a la que asistieron la semana pasada Ritondo y el presidente del PRO bonaerense, Jorge Macri, en la Casa Rosada, donde el jefe del bloque de diputados del oficialismo, Máximo Kirchner, sugirió posponer las PASO.

La foto generó malestar entre los principales dirigentes de la coalición y obligó a Jorge Macri y a Ritondo a aclarar que no había existido ningún principio de acuerdo sobre el tema, luego de que circulara una versión al respecto que alteró el clima interno.