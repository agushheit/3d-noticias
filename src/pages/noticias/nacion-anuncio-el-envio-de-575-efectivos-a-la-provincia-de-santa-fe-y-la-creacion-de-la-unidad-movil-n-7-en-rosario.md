---
category: Estado Real
date: 2021-09-29T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/ANIBALSEGURIDAD.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Nación anunció el envío de 575 efectivos a la provincia de Santa Fe y la
  creación de la unidad móvil n°7 en Rosario
title: Nación anunció el envío de 575 efectivos a la provincia de Santa Fe y la creación
  de la unidad móvil n°7 en Rosario
entradilla: 'Así lo confirmó este martes el ministro de Seguridad de la Nación, Aníbal
  Fernández, junto al gobernador Omar Perotti, el jefe de Gabinete Juan Manzur y el
  ministro Eduardo de Pedro. '

---
El gobernador de Santa Fe, Omar Perotti, participó este martes del anuncio del gobierno nacional del envío de 575 efectivos policiales a la provincia y la creación de la Unidad Móvil N° 7 en Rosario. El anuncio fue encabezado por el ministro de Seguridad, Aníbal Fernández; quien estuvo acompañado por el jefe de Gabinete de la Nación, Juan Manzur; y el titular de Interior, Eduardo de Pedro.

En la oportunidad, Perotti destacó que esta "noticia es importante para la provincia. Es una señal concreta de una decisión del gobierno nacional de tomar este tema como propio, con la responsabilidad y la definición del presidente Alberto Fernández. Esta ayuda, además, se necesita porque estamos en la reconversión de nuestra institución policial. Eso requiere tiempo para la formación de cada uno de sus nuevos agentes y el acompañamiento para esos tiempos de formación", aseguró el gobernador.

Y agregó: "Estos hechos concretos definen la presencia de una unidad móvil creada para abordar este tema. Eso es un diferencial, no es una instancia más. Para el territorio santafesino es una muy buena noticia porque sentimos que este tema es tomado de acuerdo a la magnitud que tiene, con la seriedad que corresponde y sin dudas abre una expectativa importante en todos nuestros ciudadanos", sostuvo Perotti.

"La mejor forma de empezar a resolver un problema es ponerlo sobre la mesa y enfrentarlo. Eso es lo que se está haciendo, que la Nación tome esta decisión habla a las claras de la magnitud de lo que hay que resolver y que nos requiere a todos: la decisión nacional tomada, la provincia con su responsabilidad en su institución policial y también requiere el esfuerzo de todos los legisladores nacionales, porque necesitamos que la estructura federal en territorio provincial también se fortalezca. Necesitamos que se cubran cargos y la creación de nuevos. Todos tienen su granito de arena para aportar a la solución de este problema", finalizó el gobernador de la provincia de Santa Fe.

Por su parte, el ministro Aníbal Fernández sostuvo que "es un problema que le compete a todos los argentinos: o resolvemos una situación de estas características, o vamos a ver crecer más las complicaciones de lo que vimos hasta este momento. Para esa razón decidimos sumar 575 efectivos que estarán en Santa Fe en 15 o 20 días. El traslado es inmediato. La ubicación es no solamente en Rosario y Santa Fe Capital, sino también las vías que confluyen desde una hacia otra y lo que viene desde el norte", precisó Fernández.

Además, el ministro de Seguridad de la Nación afirmó que "Gendarmería tiene el desarrollo de sus centros conocidos como Unidades Móviles. Estamos creando la Unidad N° 7, que es la que va a vivir en Rosario y será destinada específicamente a la lucha contra el narcotráfico y el delito complejo en Rosario y la ciudad de Santa Fe. Y las vías de acceso a estas ciudades desde cualquiera de sus puntos cardinales, con lo que estamos atacando toda una problemática que nos preocupa mucho".

"Estamos llevando 1.575 efectivos entre hoy y marzo, con respuesta inmediata y con conclusiones que tienen que ver con la participación adicional de lo que puede estar ordenándonos la Justicia del lugar. Tenemos entonces los dos movimientos, los 575 efectivos que estamos llevando en poquitos días, más la Unidad Móvil Nº 7, más el resto de las fuerzas puestas al servicio de la solución de un problema bastante complicado y que nosotros queremos darle un punto final", finalizó el titular de la cartera de Seguridad nacional.

Por último, el jefe de Gabinete, Juan Manzur, afirmó que "la instrucción y la decisión política que me dio el presidente de la Nación, Alberto Fernández, es que todo el gobierno de la Nación, en sus diferentes áreas, esté a la par del gobernador Perotti y de todo el pueblo de Santa Fe. Vamos a redoblar los esfuerzos, vamos a coordinar todas las acciones. En este caso, puntualizar más la colaboración en el área de seguridad y en las áreas con mayor dificultad en Rosario. Pero acciones como éstas se van a seguir repitiendo de aquí en adelante".