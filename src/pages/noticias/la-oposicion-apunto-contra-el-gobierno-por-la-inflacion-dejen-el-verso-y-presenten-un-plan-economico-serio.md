---
category: Agenda Ciudadana
date: 2022-01-14T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIDAL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'La oposición apuntó contra el Gobierno por la inflación: "Dejen el verso
  y presenten un plan económico serio"'
title: 'La oposición apuntó contra el Gobierno por la inflación: "Dejen el verso y
  presenten un plan económico serio"'
entradilla: 'Patricia Bullrich, Vidal, Negri y López Murphy criticaron a la administración
  de Alberto Fernández por la suba de precios de 50,9% en 2021. Advirtieron sobre
  la "inercia" de ese índice para este año.

  '

---
Dirigentes de Juntos por el Cambio criticaron al Gobierno por la inflación y el endeudamiento luego de que el INDEC diera a conocer el índice de 50,9% registrado en 2021 y le reclamaron que "presente al Congreso un plan económico serio".

Los diputados nacionales de Juntos por el Cambio María Eugenia Vidal, Ricardo López Murphy y Mario Negri, además de la presidenta del PRO, Patricia Bullrich, fueron algunos de los que remarcaron sus críticas a la política económica luego de que se conociera que la inflación de diciembre pasado fue de 3,8% y de un 50,9% acumulado para todo el año.

"Argentina está dentro de los cinco países con la mayor inflación del mundo.​ Esto sigue siendo un problema crónico de nuestro país: desde la vuelta a la democracia, hemos tenido un 48% de inflación promedio (sin contar la hiper)", subrayó Vidal​ a través de Twitter.

En este sentido, la diputada sostuvo que "este Gobierno no sólo tiene una inflación esperada por arriba de un 50% para este año, sino que también se endeuda a un ritmo más acelerado que el del gobierno de Cristina Kirchner y el de Mauricio Macri".

"Ya es hora de que ​dejen ​el verso de lado y presente​n​ al Congreso un plan económico serio", disparó la referente del PRO en el mensaje que publicó en su cuenta de la red social.

En la misma línea, Negri subrayó que la Argentina está tercera en el ranking de inflación mundial, "detrás de Venezuela y Sudán", y advirtió: "El 2022 será con más inflación, más pobreza, más deuda, más emisión, más presión tributaria. Es hora que dejen el relato y muestren un plan. Argentina no da más".

A su turno, Bullrich, remarcó: "Un Gobierno aplicando recetas que fracasaron, a bordo de un barco sin rumbo y sin plan, asfixiando con impuestos a los argentinos, con millones de personas que caen día tras día bajo la línea de pobreza".

"¿Hasta cuándo, presidente?", interpeló la titular del PRO en referencia a Alberto Fernández, a través de su cuenta de Twitter, tras remarcar el dato de 50,9% de inflación correspondiente a 2021.

Por su parte, López Murphy sostuvo que ese índice "dejará una gran inercia para este año", al tiempo que señaló que "el presupuesto de Guzmán dice que será 33%" y que "no hay indicios sobre el acuerdo con el FMI ni con los demás organismos multilaterales". En este sentido, el diputado nacional y economista consideró que "Argentina necesita menos sarasa, menos populismo y decir más la verdad".

Los dirigentes de la principal coalición opositora se expresaron así luego de que el INDEC difundiera el acumulado del aumento del costo de vida en 2021, que mostró un resultado de casi 22 puntos por encima de lo que había proyectado el Ministerio de Economía, al fijar inicialmente una meta de inflación del 29% para ese período.

La inflación de 50,9% para 2021 es 14,8 puntos superior al 36,1% que se produjo en 2020, cuando la economía cayó cerca de 10% producto de la recesión que provocó la pandemia de coronavirus.