---
category: Agenda Ciudadana
date: 2020-12-06T11:43:46Z
thumbnail: https://assets.3dnoticias.com.ar/escuelas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia invirtió 345 millones de pesos en infraestructura escolar relacionada
  con los protocolos COVID-19
title: La provincia invirtió 345 millones de pesos en infraestructura escolar relacionada
  con los protocolos COVID-19
entradilla: Escuela Segura consistió en ir al territorio para ver las escuelas y elaborar
  la mejor evaluación posible de manera que se constituya en una fuente concreta de
  información para el diseño de políticas públicas de gestión.

---
El Ministerio de Educación de la provincia desarrolló un Plan Integral para hacer efectiva la idea inicial de la gestión provincial de garantizar el derecho a la educación en el marco de la pandemia por Covid-19.

En este sentido, organizar el regreso a las aulas significó el armado de una línea de trabajo denominada Escuela Segura, que consistió en ir al territorio para ver las escuelas y elaborar la mejor evaluación posible de manera que se constituya en una fuente concreta de información para el diseño de políticas públicas de gestión que significaron una inversión posterior de $345.300.000 con partidas provenientes del Fondo de Asistencia para Necesidades Inmediatas (FANI).

Al respecto, la secretaria de Gestión Territorial Educativa, Rosario Cristiani, indicó que “salimos a todo el territorio, escuela por escuela, para observar cómo estaban los establecimientos, lo que significó un operativo muy importante que generó una cantidad enorme de información que nos permitió actualizar la base de datos ministerial respecto a la situación de cada edificio”.

En ese marco, desde julio se realizó un relevamiento inédito, con el trabajo de 70 estudiantes de Arquitectura e Ingeniería, que sirvieron de apoyo a los profesionales de las áreas de Infraestructura de cada una de las delegaciones regionales para obtener un diagnóstico de los edificios de cada una de las escuelas provinciales.

Más adelante, la funcionaria provincial aclaró que “la Provincia ha ido girando los fondos de manera progresiva, sostenida y sistemática desde la sede central del Ministerio hacia las regionales, y desde ahí a las escuelas”.

La inversión realizada por el gobierno provincial en el periodo julio-noviembre para afrontar los protocolos Covid, dispuestos por las autoridades sanitarias y educativas nacionales, alcanzaron a $ 201.100.000 que significaron $ 70.000 para cada uno de los 3000 edificios escolares, y otros $ 144.200.000 de FANI para 1068 escuelas santafesinas, lo que determina un monto total superior a los 345 millones de pesos.

## **UNA NUEVA GESTIÓN**

En relación con la importancia de la cercanía con los actores territoriales, Cristiani precisó que “nosotros visitamos los establecimientos antes y después de las reparaciones, escuchamos las demandas de las comunidades educativas y analizamos de manera conjunta cuáles son las mejores alternativas, haciendo hincapié en las prioridades de cada establecimiento”.

La visita a diferentes establecimientos educativos provinciales permitió detectar, en no pocos casos, una ausencia de contacto prolongada en el tiempo con las comunidades educativas. “Muchas autoridades escolares nunca habían solicitado ayuda porque la idea generalizada era que los fondos nunca llegaban o iban siempre a otras escuelas”, puntualizó la secretaria.

Al respecto, la profesora Cristiani indicó que “esta situación descrita por directoras y directores de escuelas provinciales fue un elemento simbólico a vencer porque los recursos estaban disponibles y desde el Ministerio tuvimos que insistir para que se realicen las gestiones por los canales correspondientes e iniciar las respuestas a cada solicitud”.

La inversión concretada por un Estado provincial presente es “muy importante porque ha habido situaciones de escuelas que venían esperando los fondos de FANI con reclamos de larga data para arreglos de tanques de agua, baños, aberturas deterioradas, cañerías, redes de electricidad y gas, filtraciones, pintura, mampostería y cercos perimetrales”, afirmó la secretaria de Gestión Territorial Educativa.