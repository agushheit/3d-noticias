---
category: Agenda Ciudadana
date: 2021-02-23T06:56:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/obras2.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Riacho Santa Fe: intiman a tres personas por las obras'
title: 'Riacho Santa Fe: intiman a tres personas por las obras'
entradilla: Los responsables de la obra deberán restituir, a las condiciones anteriores
  a la intervención, los lotes donde se efectuaron los movimientos de suelo. Tienen
  48 horas para comenzar los trabajos.

---
Como había anunciado el gobierno santafesino, intimaron a los responsables de la obra sin autorización que estos particulares realizaron sobre el Riacho Santa Fe.

Los tres presuntos ejecutores deberán restituir la zona a las condiciones anteriores a la intervención los lotes donde se efectuaron los movimientos de suelo sin autorización.

Además, el acta de intimación labrada por el Ministerio de Infraestructura, Servicios Públicos y Hábitat les otorga un plazo de 48 horas de haber sido notificados para que comiencen a ejecutar los trabajos de construcción requeridos.

Los trabajos que se realizaron implicaron movimiento de suelo,  tala de árboles con más de 20 años de antigüedad y construcción de un terraplén al costado de la ruta nacional 168, cerca del acceso a Alto Verde.

Ya el 6 de febrero el municipio había clausurado las obras ilegales que la constructora estaba llevando adelante a la vera del riacho.

**Los responsables**

El secretario de Recursos Hídricos, Roberto Gioria indicó: “Hemos intimado a los tres presuntos responsables de las obras en la zona del riacho Santa Fe que, una vez notificados, deberán proceder a restituir el terreno a su estado original en el menor tiempo posible con la finalidad de que el impacto antrópico sea el menor y ante la posibilidad de que se produjeran crecidas en el Sistema Río Paraná”.

El funcionario precisó que “por estas horas estamos esperando que los imputados por estas obras, Daniel Questa, Ignacio Benuzzi y la empresa Coivalsa S. A., den el visto al expediente y en el término de 48 horas, procedan a comenzar los trabajos para restituir la zona intervenida”.