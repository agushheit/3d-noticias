---
category: Agenda Ciudadana
date: 2021-10-03T06:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/AMSAFE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Amsafé definirá el miércoles si acepta la propuesta de incremento salarial
  del gobierno
title: Amsafé definirá el miércoles si acepta la propuesta de incremento salarial
  del gobierno
entradilla: El lunes se conocerán las mociones que luego votarán los docentes. El
  gobierno ofreció un aumento salarial del 17 por ciento en tres partes.

---
En la reunión paritaria del viernes, el gobierno provincial propuso al gremio de docentes un aumento salarial del 17 por ciento en tres partes, a abonar de la siguiente forma: 10 por ciento en octubre; 5 por ciento en diciembre y dos por ciento en enero.

La oferta será analizada por los sindicatos docentes. En ese sentido, fuentes de Amsafé indicaron que el mecanismo de votación será el mismo que el de ocasiones anteriores.

En ese sentido, explicaron a este medio que el lunes a las 8:30 se realizará el plenario en la delegación La Capital de Amsafé, donde se conocerán las mociones que se votarán en las escuelas. En tanto, el miércoles a las 14 se realizará el plenario provincial, que reúne a todas las delegaciones, y que definirá la aceptación o rechazo de la oferta.

Por su parte, el Sindicato de Docentes Privados (Sadop) prevé que entre martes y miércoles de la semana que viene culminar el proceso de consultas.

**La propuesta**

El 17 por ciento propuesto es sobre los valores salariales de febrero de este año. De esa manera, con el 35 por ciento que ya se les había otorgado a los docentes en el primer acuerdo paritario se estaría llegando a un incremento del 52 por ciento anual.

Además, los porcentajes de octubre y de diciembre entrarían en el cálculo de la segunda parte del salario anual complementario que se abona en el último mes del año.

En cuanto a los jubilados, el gobierno comunicó que se trasladará la política salarial definida al sector pasivo del escalafón docente en las mismas formas y condiciones establecidas para los activos.

Además, la propuesta establece que el blanqueo para activos y pasivos tendrá un cronograma de seis cuotas durante el transcurso de 2022 iniciandose con los sueldos de febrero de ese año. En ningún caso el cronograma de blanqueo significará una rebaja salarial.

De esa manera, un maestro de grado que recién se inicia cobraría en octubre 59.759 pesos. Mientras que en diciembre pasaría a 61.688 pesos y, en enero, a 62.460 pesos.

Por su parte, los docentes de secundaria con 44 horas cátedra y con antigüedad mínima cobrarían 87.682 pesos en octubre, 90.441 pesos en diciembre y 91.545 pesos en enero. Mientras que los directores que cuentan con el puntaje más bajo pasarían a cobrar 106.284 pesos en octubre, 109.817 pesos en diciembre y 111.230 pesos en enero.

Ahora los gremios docentes están analizando la propuesta y Amsafé definirá en las próximas horas cuándo se harán las asambleas departamentales para definir las mociones y luego votar.