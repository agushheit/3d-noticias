---
category: Agenda Ciudadana
date: 2021-06-10T07:53:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/gimnasios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Promotores de la actividad física cortan la ruta 168 y piden que los dejen
  trabajar
title: Promotores de la actividad física cortan la ruta 168 y piden que los dejen
  trabajar
entradilla: Se trata de un grupo de dueños de gimnasios, fútbol 5, clubes, náutica
  y profesares de educación física, entre otros. Quieren ser escuchados por las autoridades.
  Permanecerán en el lugar hasta las 9

---
Cerca de las 7 de la mañana un grupo autoconvocado promotores de la actividad física, realizan una manifestación sobre la ruta 168, mano a Santa Fe. Cortan de manera total la calzada, mano a Santa Fe, la cual liberan para el paso de vehículos por algunos minutos.

Se trata de un grupo integrado por profesores de gimnasia, de boxeo, de danza, de telas, actividades náuticas y representantes de distintos deportes que reclaman que el gobierno provincial les permitan trabajar con protocolos porque entienden que son esenciales para la salud de sus clientes.

Según manifestaron el corte se prolongará hasta las 10 de la mañana y la modalidad adoptada es permitir el paso de algunos vehículos cada 20 minutos.