---
category: La Ciudad
date: 2021-05-04T08:25:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuelas.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: La Capital tiene indicadores para estar en zona de "alerta" y se deberían
  suspender las clases
title: La Capital tiene indicadores para estar en zona de "alerta" y se deberían suspender
  las clases
entradilla: Las medidas restrictivas podrían ser las mismas que en Rosario y San Lorenzo.
  Contrareloj, la provincia quiere que Nación modifique los parámetros que se utilizan
  para definir la situación de cada departamento.

---
Las nuevas variables definidas por Nación y que determinan el tipo de restricciones en cada aglomerado para frenar la escalada de contagios por Coronavirus colocan al departamento La Capital en zona "de alerta epidemiológica y sanitaria".

De confirmarse ese escenario, se deberían profundizar las medidas restrictivas tal como ocurrió en Rosario y San Lorenzo el domingo, departamentos en los cuales se suspendieron (por al menos siete días) el dictado de clases presenciales.

Dos indicadores claves son los que comprometen a esta capital: incidencia y ocupación de camas.

El decreto nacional 287/2021, sobre medidas generales de prevención, establece en su artículo 3° que los "departamentos de más de 300.000 habitantes, serán considerados en SITUACIÓN DE ALARMA EPIDEMIOLÓGICA Y SANITARIA cuando la incidencia (casos acumulados en los últimos 14 días por cien mil habitantes) sea igual o superior a 500 y el porcentaje de ocupación de camas de terapia intensiva sea mayor al 80 por ciento".

Datos a los que pudo acceder UNO Santa Fe, actualizados a este lunes (03/05/21), revelan que la ciudad registra un nivel de incidencia de 496, apenas cuatro menos que el límite. En tanto, fuentes del Ministerio de Salud informaron a este medio que a nivel departamental se superó dicha barrera y se registró un nivel de incidencia de 504.

En cuanto a la ocupación de camas, este lunes, el director del hospital Cullen, Dr. Poletti, informó que durante el fin de semana las terapias de los tres hospitales de esta ciudad estuvieron ocupadas en un 95 por ciento.

Con respecto a la situación en sala general, el director del efector destacó que "pese a tener más cantidad de internados que el año pasado, estamos con disponibilidad de camas; y próximo a tener como soporte el hospital militar para los pacientes no covid, para poder hacer una sala covid en el interior del hospital".

Si esta Capital pasa a ser considerada como zona de "alerta", se deberían adoptar medidas similares a las anunciadas el domingo para el sur provincial. Las mismas, incluyen la suspensión de clases presenciales y la limitación aún más de la actividad gastronómica; establece que solo tengan sus puertas abiertas hasta las 19 para luego continuar bajo las modalidades delivery y take away.

Los nuevos criterios establecidos por el gobierno nacional el pasado viernes definen el riesgo en el que se encuentra un aglomerado, distrito o departamento: bajo, medio, alto riesgo epidemiológico y sanitario (donde hoy se encuentra La Capital), y de alarma epidemiológica y sanitaria (Rosario y San Lorenzo).

Hasta este lunes a la tarde, Nación no había realizado ninguna modificación sobre el esquema de riesgos y mantenía en situación de "alerta epidemiológica y sanitaria" solamente a Rosario y San Lorenzo. Como "medio riesgo epidemiológico sanitario" a los departamentos de General Obligado y Vera. Y como Alto riesgo epidemiológico sanitario al resto: Belgrano, Castellanos, General López, Iriondo, Las Colonias, San Cristóbal, San Jerónimo, San Justo, San Martín, La Capital, Caseros y Constitución.

Este domingo, en ocasión de anunciar las nuevas medidas restrictivas para Santa Fe, el gobernador Omar Perotti informó que le solicitará al gobierno nacional una revisión de las nuevas categorías y parámetros definidos para evaluar el riesgo epidemiológico en el que se encuentra cada aglomerado o departamento.

"Esos indicadores hay que reanalizarlos en vista de las particularidades de nuestra provincia y de nuestro sistema sanitario", afirmó Perotti.

El mandatario pidió analizar la variable de ocupación de camas, y profundizó: "Tenemos una Santa Fe con 19 departamentos que concentran en algunos lugares los centros de internación; es allí en donde están las principales unidades de terapia intensiva. Con lo cual, una medición de camas en un lugar, donde todos derivan, sin duda que va a dar muy alto, aunque los pacientes no le pertenezcan (a esa ciudad)".

En la misma línea, el intendente de Rosario Pablo Javkin pidió este lunes que Nación revise el esquema definido. "Rosario está orgullosa de ser núcleo sanitario y atender a personas de otros lugares, pero no es lógico establecer desde el Amba los parámetros de corredores sanitarios", definió.

El mandatario sostuvo que en esa ciudad se atiende a pacientes de varias localidades de la región que fueron dejados fuera de la zona de alarma sanitaria y pidió "ser premiados por ser núcleo sanitario en lugar ser condicionados".

Anticipó que este reclamo es el que llevará al gobernador Omar Perotti, con el que dialogará además sobre la suspensión de clases presenciales.

**Los cuatro nuevos parámetros**

Se utilizan tres variables para definir las nuevas categorías.

> **1** - "Razón de casos": surge del cociente (resultado de dividir) el número de casos confirmados acumulados en los últimos 14 días y el número de casos confirmados acumulados en los 14 días previos. Ejemplo: si arroja como resultado 1,20, significa que los casos aumentaron un 20% de una quincena a la otra".

> **2** - "Incidencia": Es el número de casos confirmados acumulados de los últimos 14 días por 100 mil habitantes

> **3** - Ocupación de camas UTI (Unidad de Terapia Intensiva)

Nación determinó que serán considerados partidos y departamentos de “riesgo bajo”, los que verifiquen los siguientes parámetros en forma positiva: cuando la razón de casos sea inferior a 0,8 y cuando el número de casos confirmados acumulados de los últimos días por 100.000 habitantes, sea inferior a 50.

Con relación a los departamentos o Partidos de “riesgo medio", describe: Cuando "la razón de casos se encuentre entre 0,8 y 1,2 y la incidencia se encuentre entre 50 y 150.

Estarán en zona de "alto riesgo" quienes presentan una razón de casos superior a 1,20 y la incidencia sea superior a 150

Con relación a la situación de "alerta sanitaria", el decreto manifiesta: "Los grandes aglomerados urbanos, departamentos o partidos de más de 300.000 habitantes, serán considerados en SITUACIÓN DE ALARMA EPIDEMIOLÓGICA Y SANITARIA cuando la incidencia definida como el número de casos confirmados acumulados de los últimos CATORCE (14) días por CIEN MIL (100.000) habitantes sea igual o superior a QUINIENTOS (500) y el porcentaje de ocupación de camas de terapia intensiva sea mayor al OCHENTA POR CIENTO (80 %).