---
layout: Noticia con imagen
author: "Fuente: NA / Ambito"
resumen: "Plazo fijo: aumento de tasas"
category: Agenda Ciudadana
title: Aumentan las tasas de los plazos fijos
entradilla: Según informó el Banco Central, se incrementarán 37% para sumas
  menores a $1 millón y del 34% para el resto de los depósitos.
date: 2020-11-13T17:13:19.101Z
thumbnail: https://assets.3dnoticias.com.ar/plazos-fijos.jpg
---
El Banco Central elevará la tasa de interés para incentivar las colocaciones en pesos ante la suba del dólar blue.

La autoridad monetaria subirá la tasa mínima garantizada de los plazos fijos, con un rendimiento de 37% TNA (44% TEA) a 30 días para imposiciones de personas humanas menores a $1 millón y para el resto de 34% TNA (39,8% TEA).

En el primer caso se trata de un ajuste de tres puntos porcentuales, desde el 34%, mientras que en el segundo caso, representa un incremento de dos puntos porcentuales, desde el 32%.

El BCRA también dispuso un incremento de la tasa de interés de la política monetaria "para continuar con la estrategia de armonizar los rendimientos en pesos". En ese sentido, elevó dos puntos la tasa de pases pasivos a 7 días, que queda en 36,5% TNA y en un punto los pases a 1 día, que se ubica en 32%.