---
layout: Noticia con video
author: "Fuente: Diario El Litoral"
resumen: "Covid19: comunicado oficial"
category: Agenda Ciudadana
title: Sigue el aislamiento en departamentos de 10 provincias
entradilla: Aún no se especificaron los distritos alcanzados por las medidas que
  regirán hasta el 29 de noviembre. El presidente Alberto Fernández anunció este
  viernes cómo continuará la estrategia epidemiológica ante la pandemia.
date: 2020-11-07T10:58:00.000Z
thumbnail: https://assets.3dnoticias.com.ar/2020-10-07.jpg
url: https://youtu.be/neLHDw5B1gg
---
Tal como se esperaba, el jefe de Estado comunicó que el Área Metropolitana de Buenos Aires (AMBA) pasará al distanciamiento social en lugar del aislamiento. “Básicamente es circular sin autorización”, detalló el mandatario nacional.

En un mensaje grabado desde la Quinta de Olivos, el jefe de Estado aclaró que por el momento quedarán en etapa de “aislamiento” departamentos de diez provincias, donde aún se mantiene constante el nivel de casos. “Lamentablemente (el paso al DISPO) no alcanza a todo el país porque hay provincias que siguen necesitando el aislamiento”, afirmó el jefe de Estado.

Tras anunciar que la zona metropolitana entrará en etapa de distanciamiento social -tras ser la última que quedaba en ASPO-, advirtió que el problema con el coronavirus está “lejos” de resolverse e hizo un llamado a mantener las medidas de cuidado.

“Es un distanciamiento sanitario, lejos está de que el problema se haya resuelto y aquí acudo a la responsabilidad colectiva”, subrayó Fernández.