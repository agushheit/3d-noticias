---
category: Estado Real
date: 2021-02-09T08:23:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/tarjeta.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia extiende hasta el 31 de marzo el reempadronamiento de titulares
  de la Tarjeta Única de Ciudadanía
title: La provincia extiende hasta el 31 de marzo el reempadronamiento de titulares
  de la Tarjeta Única de Ciudadanía
entradilla: El trámite es obligatorio para mantener el beneficio.

---
El Ministerio de Desarrollo Social, a través de la Dirección Provincial del Programa Alimentario, extendió el plazo para realizar el reempadronamiento de titulares de la Tarjeta Única de Ciudadanía, hasta el 31 de marzo.

El objetivo es obtener los datos personales actualizados de los beneficiarios para instrumentar un nuevo sistema de cobro, donde se podrá utilizar la aplicación móvil "Plus Pago". El reempadronamiento es obligatorio para mantener el beneficio y sólo podrán realizarlo los actuales titulares de la tarjeta.

El ministro de Desarrollo Social, Danilo Capitani, explicó que “esta nueva herramienta que se incorpora al pago de programas sociales, es una apuesta de la provincia para facilitar el proceso de inclusión financiera y digital, y la posibilidad de que los titulares de la tarjeta puedan acceder a importantes descuentos a través de la Billetera Santa Fe”.

**BILLETERA ELECTRÓNICA**  
Este nuevo formato permite el cambio de la tarjeta precargable a la Billetera Electrónica, lo que posibilitará que los beneficiarios puedan accedan al programa Billetera Santa Fe, recientemente lanzado por el gobierno provincial para impulsar el consumo, y que otorga un reintegro de hasta el 30 % de las compras en alimentos realizadas en los comercios adheridos.

**PASOS PARA EL REEMPADRONAMIENTO**  
Con el objeto de facilitar dicho trámite, la provincia ha dispuesto el sitio web [https://mds.pluspagos.com/ministeriodedesarrollosocialSF](https://mds.pluspagos.com/ministeriodedesarrollosocialSF "https://mds.pluspagos.com/ministeriodedesarrollosocialSF") para realizarlo de manera virtual, sin necesidad de trasladarse físicamente.

Se necesita contar con DNI y correo electrónico; y completar el formulario ingresando a la página web del Banco de Santa Fe: [https://www.bancosantafe.com.ar/ministeriodedesarrollosocialSF](https://www.bancosantafe.com.ar/ministeriodedesarrollosocialSF "https://www.bancosantafe.com.ar/ministeriodedesarrollosocialSF")

Luego recibirá un mensaje de texto, en el celular informado previamente para completar y finalizar el trámite.

El titular debe descargar la aplicación "Plus Pago" en su teléfono móvil, para acceder al beneficio.

**INFORMACIÓN**  
En caso de dudas se podrán canalizar consultas a través de WhatsApp al 341 – 6170678 y al correo electrónico: [info@pluspagos.com](http://info@pluspagos.com/)