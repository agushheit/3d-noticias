---
category: La Ciudad
date: 2022-01-04T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/perirtus.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Recomendaciones para evitar los golpes de calor en animales de compañía
title: Recomendaciones para evitar los golpes de calor en animales de compañía
entradilla: "La Municipalidad informa cómo prevenir los trastornos que pueden ocasionar
  las altas temperaturas. Además comunica cómo continúa la campaña de castración gratuita.\n\n"

---
Con las altas temperaturas es indispensable el cuidado de los animales de compañía porque también se pueden ver afectados por los golpes de calor al igual que los humanos. Para ello, desde el área de Salud Animal municipal se brinda una serie de pautas a tener en cuenta para conocer las reacciones que pueden presentar y así tomar las medidas que prevengan cualquier tipo de problema.

En primer lugar, se deben conocer los síntomas para poder actuar al respecto. El decaimiento y la debilidad es uno de ellos, pero también pueden aparecer temblores musculares, coloración azulada en la piel; dificultad para moverse y respirar; se babea y tambalea; y el ritmo cardíaco es alto.

Para prevenir todo esto, los animales deben tener agua limpia y fresca siempre disponible; estar en un espacio amplio y ventilado o con sombra; dales de comer por la noche; realizar los paseos en horarios de menor calor; evitar las caminatas prolongadas y que haga ejercicio intenso; no dejarlos en el interior del auto: y chequear su estado periódicamente.

Vale recordar que los más propensos a los golpes de calor son los animales más viejos y los cachorros; los que tienen sobrepeso; las razas con nariz chata; y aquellos que poseen problemas del corazón o respiratorios.

**Castraciones y vacunación**

Por otro lado, la Municipalidad de Santa Fe recuerda que está vigente un cronograma de castración móvil hasta el 14 de enero, destinada a los animales de compañía. Se debe tener presente que el servicio es gratuito y se ofrece a todos los vecinos y vecinas de la ciudad con la intención de controlar la población felina y canina y garantizar el cuidado de su salud.

Como ya es habitual, en las sedes del Instituto Municipal de Salud Animal (Imusa) del Parque Garay (Obispo Gelabert 3691) y del Jardín Botánico (San José 8400) se atiende de mañana y de tarde en los horarios de 8 a 17. Aquí también se brinda atención veterinaria.

Los puntos móviles definidos para esta semana y la próxima son Riobamba 8144 y en la sede del Distrito Noroeste en La Tablada, Teniente Loza 6970, que funcionarán hasta el 7 y el 14 de enero, respectivamente. Igualmente todos los días se realizan vacunaciones y castraciones en la Protectora de Animales, los turnos se solicitan al 3425496736.

En el caso de castraciones, la atención se brinda con turnos programados en las sedes fijas del Instituto Municipal de Salud Animal (Imusa) y pueden obtenerse en la web oficial www.santafeciudad.gov.ar/turnos. Allí se debe buscar el apartado denominado “Castraciones”, se elige la opción “Solicitar un nuevo turno” y se completa el formulario con los datos pedidos.

Posteriormente se escoge el día, el horario y la sede a la cual concurrir. En tanto, para el caso de puestos móviles descentralizados, se puede concurrir sin turno previo en el horario de 8 a 12 y el cupo es hasta 20 animales por día.