---
category: Agenda Ciudadana
date: 2021-11-24T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Unión Tranviarios Automotor
resumen: Paro de colectivos de corta y media distancia para este viernes
title: Paro de colectivos de corta y media distancia para este viernes.
entradilla: 'Así lo confirmaron desde la Unión Tranviarios del Automotor del Interior. '

---
Ponemos en conocimiento de los trabajadores, las autoridades y de la opinión publica en general, que no hemos tenido respuesta alguna al pedido salarial formulado en la audiencia del día de la fecha en el Ministerio de Trabajo de la Nación, sin contar con ningún tipo de certezas respecto de los aumentos salariales reclamados. 

Fuimos claros en nuestro pedido, pretendemos para los trabajadores del interior, el mismo incremento salarial que se ha acordado hace ya mas de un mes con nuestros compañeros del AMBA. Los tiempos de los trabajadores se han agotado, sin alcanzar un acuerdo que contemple nuestros legítimos intereses. Queremos dejar expresado que responsabilizamos de todas las consecuencias de cualquier medida de acción gremial, a la total negativa a acordar los sueldos de los trabajadores representados.

 No vamos a permitir salarios por debajo de la inflación. Por eso hemos resuelto un paro actividades por 24 horas para todos los trabajadores del transporte de pasajeros de corta y media distancia del interior del país, desde las 00 horas del viernes 26 del corriente.