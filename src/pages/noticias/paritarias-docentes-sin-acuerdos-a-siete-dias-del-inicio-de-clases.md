---
category: Agenda Ciudadana
date: 2022-02-23T11:16:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/pusineri.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3d noticias
resumen: 'Paritarias docentes: sin acuerdos a siete días del inicio de clases.'
title: 'Paritarias docentes: sin acuerdos a siete días del inicio de clases.'
entradilla: A una semana del comienzo de clases, el gobierno provincial a través de
  su ministro de trabajo, presento la oferta salarial para el sector docente.

---
“La oferta salarial prevé una suba de 17,5% a partir de marzo; 8,08 % a partir de junio (lo que impactará en el sueldo anual complementario correspondiente); y la misma cifra en agosto y en septiembre. Estos incrementos, sumados a los aumentos nacionales, totalizan un promedio de 42, 8%, e incluyen la posibilidad de realizar una instancia de revisión en el mes de septiembre”, comentó Pusineri.

Por último, el ministro de Trabajo reiteró que “es voluntad del gobierno que durante 2022 el salario docente se ubique por sobre la inflación, tal como sucedió el año pasado. Si tomamos el cargo testigo de maestro inicial, tenemos que este docente va a comenzar el año con un sueldo de $75.158, un número muy superior a la cifra nacional”, concluyó.S

La representación de AMSAFE manifestó que la propuesta está lejos de lo esperado por la docencia santafesina, entendiendo que en la provincia existen condiciones económicas para mejorar incluso el porcentaje de la propuesta nacional.

A partir de hoy  miércoles la propuesta será puesta a consideración en las asambleas departamentales, votando en todas las escuelas los días jueves y viernes y realizando la asamblea provincial el sábado 26 del corriente mes.

En dialogo con LT 9 Pedro Bayugar, de SADOP dijo "No es mejor la oferta provincial que la nacional, es menor, y menor inclusive que lo que nos ofrecieron el año pasado" |

"El viernes por la tarde el congreso de delegados provincial resolverá si acepta o no y qué pasos se darán"

A una semana del inicio de clases. Así estamos.