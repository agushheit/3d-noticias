---
category: Agenda Ciudadana
date: 2021-04-11T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/PASO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Continúa el diálogo para modificar el calendario electoral por la pandemia
title: Continúa el diálogo para modificar el calendario electoral por la pandemia
entradilla: El oficialismo conversará con miembros del Interbloque Federal, Unidad
  Federal para el Desarrollo y el Frente de Izquierda.

---
El Gobierno nacional continuará analizando la semana próxima con los referentes de varias fuerzas de la oposición el borrador con la propuesta impulsada por el oficialismo para aplazar las primarias abiertas, simultáneas y obligatorias (PASO) al 12 septiembre y las elecciones legislativas al 14 noviembre, debido a la pandemia por coronavirus.

"Es un tema que se debe discutir en el Congreso y cualquier tipo de modificación electoral tiene que tener acuerdo y consenso político", sostuvo hoy el jefe de Gabinete, Santiago Cafiero, en diálogo con FM Millenium, y afirmó que el Gobierno nacional "adaptará el calendario según lo que decida el Congreso".

**Ampliarán el arco de consultas**

Luego de reunirse el jueves último en el Congreso con los referentes de Juntos por el Cambio, el ministro del Interior, Eduardo de Pedro, buscará reunirse la semana próxima con los referentes del Interbloque Federal, Unidad Federal para el Desarrollo y el Frente de Izquierda para avanzar en la posibilidad de postergar un mes las PASO y las elecciones legislativas.

La realización de la reunión con esos espacios opositores fue adelantada el jueves por el propio titular de la Cámara de Diputados, Sergio Massa. El presidente de la cámara baja confirmó que se acordó "hacer una segunda reunión con los bloques de los partidos que no son de Juntos por el Cambio: la Izquierda y los partidos provinciales. Lo que pretendemos es buscar un consenso general con garantías de participación democrática", explicó.

**Algunas posturas ya anticipadas**

Desde el interbloque Unidad Federal para el Desarrollo, José Luis Ramón, adelantó que ese espacio "apoya la suspensión o la prórroga, en la medida que exista un informe previo del ministerio de Salud de la Nación que así lo impusiere en razón del cuidado de la vida y de la salud de miles de argentinos".

"De la reunión vamos a participar y vamos a llegar a la conclusión que se arribe en un acuerdo con todas las fuerzas políticas, pero siempre teniendo en cuenta la existencia de la pandemia y la necesidad de cuidar la salud y la vida de miles de personas. Vamos a ser propositivos en el sentido de que se llegue a una decisión de tener el consenso de todos los que participemos".

Desde Consenso Federal, Alejandro "Topo" Rodríguez, sostuvo que "son cambios electorales que ya se cocinaron en dos reuniones entre kirchneristas y macristas" e ironizó: "Tal vez nos llamen para hacernos el favor de informarnos lo que ya todos sabemos".

**Frente de Izquierda: “Tenemos cuestionamientos históricos a las PASO”**

Desde la Izquierda, en tanto, Nicolás del Caño, afirmó que si los convocan evaluarán "la participación en función de la convocatoria" y agregó: "No obstante recordamos que desde el Frente de Izquierda tenemos cuestionamientos históricos al sistema de las PASO, como el piso prescriptivo del 1,5 por ciento".

Las PASO están previstas inicialmente para el 8 de agosto y las generales para octubre, pero a raíz de la pandemia del coronavirus y la llegada de la segunda ola, el Gobierno analiza postergar esa fecha para un mes más tarde para evitar mayores contagios.

**Necesario consenso partidario**

"Teniendo en cuenta la segunda ola y lo dispuesto por el calendario electoral vigente, lo que discutimos es que sería óptimo comenzar a debatir el corrimiento de la elección por lo menos un mes. Entendemos que es un mes más de vacunación, lo que es importante", sostuvo De Pedro esta semana.

Fuentes gubernamentales señalaron que, desde el comienzo de esta discusión, el presidente Alberto Fernández había manifestado que cualquier modificación debería salir del Poder Legislativo y para ello, debía conseguir consenso multipartidario.

Esa comisión será clave para debatir la suspensión de las elecciones PASO, así como otros proyectos pendientes de tratamiento como la reforma judicial y de reforma de la Ley del Ministerio Público, para que se pueda elegir al Procurador General de la Nación con mayoría calificada en lugar de dos tercios como establece la ley vigente.

**Evitar aglomeraciones**

Desde el Poder Ejecutivo indicaron además que el debate no es sobre el sistema electoral y que no se busca discutir otros temas como el de la Boleta Única Papel.

"Nosotros creemos que hay consenso para avanzar en la postergación con el respeto de lo indicado por la Justicia Electoral sobre los plazos que debe haber entre las Paso y las generales", añadieron las fuentes.

Con estos encuentros, el Gobierno busca un acuerdo para posponer un mes las PASO en busca de evitar la aglomeración de gente en los meses de invierno, como para dar tiempo a que el plan de vacunación alcance a cubrir a toda la población de riesgo.

**Rol clave de la comisión de Asuntos Constitucionales**

Para ello, se requiere un acuerdo político, ya que las leyes electorales necesitan mayorías calificadas de 129 votos en Diputados y de 37 en el Senado (la mitad más uno de los miembros de cada Cámara), por lo que hace falta un consenso entre el Frente de Todos y Juntos por el Cambio.

Para avanzar en esa propuesta, el oficialismo de la Cámara de Diputados ya designó al legislador pampeano del Frente de Todos Hernán Pérez Araujo, al frente de la comisión de Asuntos Constitucionales, en reemplazo de Pablo González, quien renunció en febrero para asumir como director de Yacimientos Petrolíferos Fiscales (YPF).