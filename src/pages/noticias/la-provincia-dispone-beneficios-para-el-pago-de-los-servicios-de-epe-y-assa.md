---
category: Estado Real
date: 2021-05-19T08:39:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/epe.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia dispone beneficios para el pago de los servicios de EPE y ASSA
title: La provincia dispone beneficios para el pago de los servicios de EPE y ASSA
entradilla: Se trata de beneficios en la facturación y pago de las liquidaciones,
  entre mayo y julio de 2021, que incluirá a diferentes sectores comerciales y productivos,
  afectados en su actividad por la pandemia Covid-19.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Unidad de Coordinación, creada por Resolución N° 210/2020, implementó nuevas medidas para sectores comerciales y productivos, que se vieron afectados en el desarrollo de su actividad por la pandemia.

En este sentido, el gobierno provincial especificó que los beneficios en la facturación y el pago de las liquidaciones, sean desde mayo y hasta el 31 de julio.

Para los gimnasios, asociaciones vecinales, escuela de danzas o artes marciales, pilates, jardines maternales, peloteros, fútbol 5, paddle, transportes escolares, salones privados de eventos, escuelas de conducción, talleres y salas culturales; clubes, hoteles, complejos turísticos (Ej: Cabañas de alquiler), gastronómicos y empresas con permiso para realizar servicios de transporte automotor de pasajeros para el turismo de jurisdicción nacional radicadas en la provincia de Santa Fe (las cuales se establecerán a partir de un listado que confeccionará la Secretaria de Turismo y Transporte); pagarán a valor histórico sin intereses de actualización o mora, las liquidaciones de servicios con vencimiento a partir del 17 de mayo 2021 y hasta el 31 de julio 2021; sean éstos de facturación mensual o bimestral (Pequeñas Demandas o Grandes Demandas).

Con respecto a la facturación para grandes demandas, usuarios con actividad: hotelera, gastronómicos, salones de eventos y clubes; se le facturará sólo la potencia registrada en los “períodos de consumo” de los meses de mayo, junio y julio del año 2021.

Para las cadenas de franquicias gastronómicas (multisuministros) el beneficio tendrá un tope máximo en pesos de $100.000, agrupando todos los suministros. Quedando exceptuados de estos beneficios, los Casinos y Salas de Juego.

Para los suministros de consorcios de galerías comerciales y locales que la componen, sector no amparado por medidas de contención adoptadas previamente, se los incluirá en los beneficios otorgados a los usuarios alcanzados por el Decreto 283-2020 del gobierno provincial: pago a valor histórico de las facturas con vencimiento a partir de abril 2020 hasta junio 2020 y además, para los grandes usuarios, se dispondrá sólo la facturación de la potencia registrada en los meses de abril a junio del año 2020.

Finalmente, la EPE dispondrá en su oficina virtual, un link para acceder a los beneficios de pago a valor histórico: el usuario deberá ingresar a www.epe.santafe.gov.ar/oficinavirtual, ir al apartado reclamos comerciales y tildar en tipo de reclamo, “pago a valor histórico”.

En tanto, en el caso de Aguas Santafesinas se dispondrán para los períodos/bimestres 3 y 4/2021, de planes especiales con descuentos de recargos e interés de financiación, para los inmuebles habilitados para las siguientes actividades: gimnasios, asociaciones vecinales, escuela de danzas o artes marciales, jardines maternales, peloteros, fútbol 5, paddle, talleres y salas culturales; clubes, hoteles, hoteles alojamiento, centros de convenciones (sin salas de juego en sus instalaciones), bares, restaurantes y empresas con permiso para realizar servicios de transporte automotor de pasajeros para el turismo de jurisdicción nacional radicadas en la Provincia de Santa Fe (a partir de un listado que confeccionarán las Secretarías de Turismo y Transporte). Asimismo, para aquellos usuarios de estos rubros que no cuenten con consumo medido, se bonificará hasta un 70% del monto a pagar por los mismos períodos bimestrales indicados. Para ello, la empresa dispondrá en su oficina virtual de formularios para que los usuarios alcanzados por estas medidas puedan tramitarlas exclusivamente en línea a través de este link: [http://www.aguassantafesinas.com.ar/gestiones](http://www.aguassantafesinas.com.ar/gestiones "http://www.aguassantafesinas.com.ar/gestiones")

![](https://assets.3dnoticias.com.ar/epe1.jpeg)

![](https://assets.3dnoticias.com.ar/epe2.jpeg)

![](https://assets.3dnoticias.com.ar/epe3.jpeg)

![](https://assets.3dnoticias.com.ar/epe4.jpeg)