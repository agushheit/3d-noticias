---
category: Agenda Ciudadana
date: 2021-04-04T08:10:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERNANDEZ.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Coronavirus: Alberto Fernández y Horacio Rodríguez Larreta acordaron esperar
  para tomar nuevas medidas'
title: 'Coronavirus: Alberto Fernández y Horacio Rodríguez Larreta acordaron esperar
  para tomar nuevas medidas'
entradilla: 'El presidente de Nación y el Jefe de Gobierno porteño mantuvieron este
  sábado por la tarde una breve reunión virtual. '

---
Fernandez y Horacio Rodríguez Larreta, se reunieron virtualmente este sábado para discutir la suba de casos de COVID-19. El encuentro duró menos de media hora, durante la cual coincidieron en la preocupación por la situación sanitaria ante la llegada de la segunda ola, pero decidieron esperar para tomar medidas. Además, el jefe de gobierno porteño pidió modificar el criterio de distribución de las vacunas de la Nación a las provincias.

El primer mandatario y el dirigente porteño “compartieron la preocupación por la evolución de los contagios de los últimos días en todo el país”, según se informó oficialmente. Larreta se interesó por la salud del Presidente, y luego intercambiaron miradas sobre la situación epidemiológica en la Ciudad. Hubo sintonía, y en especial coincidieron en un punto sobre el que hubo diferencias el año pasado: la educación presencial. Los mandatarios decidieron que mantendrán las escuelas abiertas y que esa será su “máxima prioridad”.

El jefe de gobierno le dijo a Alberto Fernández que, en los últimos días, en la Ciudad se aceleró el proceso de vacunación gracias el ingreso de nuevas dosis, y le planteó “analizar el criterio de distribución de las vacunas, para alinearlo a la estrategia de vacunación nacional que prioriza los grupos de riesgo (trabajadores de la salud y adultos mayores de 60 años)”.

La administración local viene planteando hace semanas públicamente la necesidad de modificar el criterio de distribución de las vacunas de la Nación a las provincias, para adaptarlo al tipo de población, y no exclusivamente en relación a la cantidad de habitantes. Si la Nación diera el visto bueno al pedido, la Ciudad recibiría mayor cantidad de dosis.

La respuesta de Alberto Fernández se conoció a través de un hilo en su cuenta de Twitter: “Larreta me expresó su preocupación por acelerar la vacunación de los adultos mayores en CABA, teniendo en cuenta que en la Ciudad reside un mayor porcentaje de ellos. Ante la inquietud, propuse analizar la posibilidad de involucrar al PAMI en la aplicación de las vacunas”, dijo el Presidente. Las miradas sobre cómo enfrentar la falta de vacunas en el ámbito porteño, por ahora, aparecen con diferencias.