---
layout: Noticia con imagen
author: .
resumen: Operativo Verano Seguro
category: La Ciudad
title: 'Policía de Santa Fe: comenzó el operativo "Verano Seguro"'
entradilla: Esta mañana se produjo el lanzamiento del Plan Operativo “Verano
  Seguro”, cuya implementación fue ordenada por la Jefatura de Policía de la
  Provincia de Santa Fe.
date: 2020-11-08T18:49:48.551Z
thumbnail: https://assets.3dnoticias.com.ar/costanera.jpg
---
Este plan a gran escala tiene como objetivo brindar seguridad a quienes residen a lo largo y a lo ancho de nuestra provincia y a todos aquellos que nos visiten, incrementando la vigilancia policial en balnearios públicos, barrios de casas de fines de semana, zonas de costas, playas, espacios recreativos, etc.

Todo el operativo fue desarrollado mediante una labor mancomunada con el gobierno provincial, municipios y comunas, buscando lograr mayor eficiencia y eficacia del accionar policial y disminuir riesgos de contagios de Covid-19 en espacios públicos.