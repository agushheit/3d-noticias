---
category: La Ciudad
date: 2021-12-08T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/COLONIASMUNICIPALES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Abrieron las inscripciones para las colonias de vacaciones municipales
title: Abrieron las inscripciones para las colonias de vacaciones municipales
entradilla: "La propuesta de verano se desarrollará en ocho sedes desde el 10 de enero
  al 18 de febrero de 2022. Las inscripciones se realizan en las Estaciones municipales
  hasta el 31 de diciembre. \n\n"

---
La Municipalidad de Santa Fe informa que se encuentran abiertas las inscripciones para participar de las Colonias de vacaciones, que funcionará desde el 10 de enero al 18 de febrero de 2022 en ocho sedes en el horario de 8 a 12.

Las sedes para la temporada de verano 2022 son: Amsafé y Amsap, que recibirán tanto a grupos de adultos mayores como a infancias y juventudes; Stia, que recibirá únicamente a personas adultas mayores, al igual que Pierdas Blancas; y Alto Verde, destinada a infancias y juventudes, al igual que Fraternidad deportiva, Estacionero y Canillitas.

El grupo de infancias está destinado a personas de entre 6 y 13 años que concurrirán a las colonias de martes a jueves; en tanto que el grupo de juventudes (14 a 35 años) participará de la propuesta los viernes, y las personas adultas mayores podrán asistir de lunes a viernes.

Se informa además que las personas con discapacidad que deseen participar deberán inscribirse en los grupos de acuerdo a la edad, siendo el rango etario en el caso de juventudes de 13 a 40 años.

**Lugares y requisitos de inscripción**

Las inscripciones se realizan hasta el 31 de diciembre en cualquiera de las 17 Estaciones con las que cuenta el municipio: Colastiné, La Guardia, Alto Verde, La Boca, San Lorenzo (Entre Ríos 4080), Barranquitas (Iturraspe y Estrada), Mediateca (Pje. Mitre y Tucumán), CIC Facundo (Facundo Zuviría 8040), Loyola (Furlong 8400, esquina Pedroni), San Agustín (Fray Francisco de Aroca 9549), Abasto (Gdor. Menchaca y Pedro de Espinoza), Villa Teresa (La Pampa 6200/6300), Dorrego (Larrea 1735), Varadero Sarsotti (Tacuaritas S/N), Las Lomas (Viñas 6800 o Padre Viera entre E. Zeballos y Espora), Liceo Norte (Callejón Aguirre 5197, esquina Pje. Larguía) y Villa Hipódromo (Roque Sáenz Peña 6148).

Al momento de la inscripción se deberá completar una ficha y presentar certificado médico, fotocopia de DNI, certificado buco dental, fotocopia de carné de vacunación y último electrocardiograma en el caso de las personas adultas mayores.