---
category: Agenda Ciudadana
date: 2021-11-07T06:00:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/sinopharm.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: 'Polémica por Sinopharm: ministros de Salud ratificaron la vacunación pediátrica'
title: 'Polémica por Sinopharm: ministros de Salud ratificaron la vacunación pediátrica'
entradilla: 'Además, se remarcó la prioridad de iniciar y completar esquemas en mayores
  de 18 años, adolescentes, niños y niñas, y se consensuó avanzar con la vacunación
  de refuerzo contra la Covid-19 en personal de salud.

'

---
En el marco de la reunión del Consejo Federal de Salud (COFESA) que se realizó este viernes, las autoridades sanitarias de las 24 jurisdicciones y del Ministerio de Salud de la Nación destacaron el avance del plan de vacunación contra la COVID-19 en la población pediátrica, que hasta el momento ya lleva 2.655.559 dosis aplicadas y supera el 35% de esquemas iniciados en esta población objetivo de niños y niñas de entre 3 y 11 años.

 En ese sentido, los ministros y ministras reafirmaron el proceso de toma de decisión conjunta que llevó a la utilización de la vacuna Sinopharm en la población infantil, en base a la recomendación de la ANMAT. Además, volvieron a expresar su total confianza en la autoridad regulatoria nacional, quien recomendó la ampliación en la edad luego de evaluar los ensayos clínicos fase 1 y 2 publicados en The Lancet y analizar la información del avance del estudio puente de fase 3 que se realiza en Emiratos Árabes.

 A su vez, se subrayó el adecuado perfil de seguridad que evidencia en nuestro país la vacuna Sinopharm, que cuenta con una tecnología de virus inactivado, una plataforma ampliamente utilizada en otras vacunas incluidas en el Calendario de Vacunación.

 Por último, se resaltó la importancia de la oportunidad en la toma de decisión, ya que, en base a la situación epidemiológica y la circulación predominante de la variante delta, se observó un aumento de la proporción de casos en personas menores de 18 años de 8 a 25% en las semanas previas al inicio de la vacunación dado que se trataba de la población susceptible. El beneficio individual y el rol en la disminución de la transmisión son otros de los fundamentos para ampliar a más franjas etarias la vacunación. A medida que las coberturas de vacunación fueron aumentando, se aceleró en todo el país la disminución de casos, internaciones y fallecidos.

 Durante el encuentro, las autoridades sanitarias coincidieron en destacar el importante avance de la vacunación en todas las franjas etarias, sosteniendo la prioridad de iniciar, completar esquemas de vacunación y aplicar las terceras dosis a las personas inmunocomprometidas de todas las edades y mayores de 50 años que hayan recibido la vacuna Sinopharm.

 Teniendo en cuenta el aumento de la circulación de la variante delta y la disponibilidad de vacunas en el país, se decidió avanzar, entre lunes y martes, en la presentación y generación de consenso con la Comisión Nacional de Inmunizaciones (Conain) y el Comité de Expertos para impulsar la estrategia de aplicación de dosis de refuerzo, comenzando por el personal de Salud.

 En simultáneo, se continuará trabajando con la Administración Nacional de Medicamentos, Alimentos y Tecnología Médica (ANMAT), como vienen haciéndolo organismos internacionales como el Centers for Disease Control and Prevention (CDC)  y la Food and Drug Administration (FDA) para la implementación de las recomendaciones consensuadas.

 Con respecto a la importancia de avanzar con esta estrategia de dosis de refuerzo, se remarcó que está relacionada de manera directa con el aumento de la circulación de la variante delta y el objetivo de proteger lo antes posible al personal de salud que haya completado su esquema hace más de seis meses, para luego avanzar con el resto de la población en forma escalonada en función de cada plan provincial.

 También se destacó que en la actualidad Argentina cuenta con disponibilidad de vacunas para dar respuesta a los planes provinciales teniendo en cuenta la diversidad geográfica, cultural, climática, los diferentes avances en las coberturas de vacunación así como también las estrategias de vacunación futuras.