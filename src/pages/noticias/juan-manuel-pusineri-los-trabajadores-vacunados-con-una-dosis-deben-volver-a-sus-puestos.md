---
category: Agenda Ciudadana
date: 2021-08-13T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/teletrabajojpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Juan Manuel Pusineri: los trabajadores vacunados con una dosis "deben volver
  a sus puestos'
title: 'Juan Manuel Pusineri: los trabajadores vacunados con una dosis "deben volver
  a sus puestos'
entradilla: "\"El ministro de Trabajo de la provincia Juan Manuel Pusineri se refirió
  al pedido que hizo la UIA de no pagar sueldos a empleados que no se vacunen y no
  regresen."

---
El ministro de Trabajo de la provincia, Juan Manuel Pusineri, se pronunció hoy sobre el requerimiento de la Unión Industrial Argentina (UIA) para dejar de pagar sueldos a los empleados que no quieran vacunarse contra el coronavirus y que no regresen al trabajo presencial.

En declaraciones al programa “El primero de la mañana” de LT8, el funcionario provincial remarcó en primera instancia: “Hay un marco normativo. La resolución 4 de los ministerios de Trabajo y de Salud de la Nación indica que quienes estén vacunados con una dosis deben volver al trabajo o están en condiciones de ser convocados por los empleadores”.

Pero en ese sentido el responsable del área Trabajo aclaró que “la vacunación no es obligatoria, y entonces es muy difícil dictar una norma general. Esto es lo que dice el Ministerio de Trabajo de la Nación para resolver todas las cuestiones”.

Pusineri sostuvo que cada caso se deberá resolver de acuerdo a sus particularidades. “En base a los principios de buena fe y de colaboración de la ley de Contrato de Trabajo, en cada caso cada empresa deberá resolver la situación de las personas que no fueron vacunadas: si pueden o no seguir en teletrabajo o si se le puede asignar alguna tarea que no ponga en riesgo al resto del personal”, precisó.

“La empresa siempre podrá evaluar si la actitud del empleado responde o no a los parámetros de buena fe. Si el trabajador actúa de mala fe, existe la posibilidad de sanciones que siempre están en manos del empleador. No hay otra forma de resolver esto que no sea caso por caso”, señaló Pusineri.

No obstante, el ministro de Trabajo aclaró que hasta este momento “no hay casos” de esta naturaleza que se hayan planteado ante su cartera. “Hay muchos trabajadores que volvieron a la presencialidad y se está instruyendo a los ministerios para que convoquen a los trabajadores de grupo de riesgo vacunados, conforme a la resolución 4 mencionada. Hoy, la vacunación es la salida y todas las opciones tienen en la mira esto. Vamos camino a una combinación de actividades presenciales y a distancia. Es lo que está sucediendo en las empresas”, aseguró

**Programa Empleo Joven**

Pusineri también se refirió al plan Empleo Joven, que será presentado hoy por el gobernador Omar Perotti. “Se trata de un programa que se pone en marcha para la población de entre 18 y 30 años, y que consiste en aportar, durante seis meses, 20 mil pesos por trabajador contratado por una empresa, la cual deberá completar el salario que corresponda por convenio colectivo de trabajo”.

“Previo a ello habrá capacitación y prácticas también a cargo del Estado. Todo, con la finalidad de apuntalar el proceso de paulatino crecimiento del empleo y de expectativas más favorables para que las empresas contraten personal”, precisó Pusineri.