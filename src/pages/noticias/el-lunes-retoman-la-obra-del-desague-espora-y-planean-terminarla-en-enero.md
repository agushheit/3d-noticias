---
category: La Ciudad
date: 2021-08-26T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESPORAK.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Lia Masjoan. El Litoral
resumen: El lunes retoman la obra del desagüe Espora y planean terminarla en enero
title: El lunes retoman la obra del desagüe Espora y planean terminarla en enero
entradilla: La empresa Mundo Construcciones SA aseguró que la semana que viene se
  verán mayores trabajos.

---
El 20% de los trabajos que faltan para terminar la construcción del desagüe Espora se reactivará desde el lunes próximo. En dos reuniones por zoom sucedidas este martes, la empresa Mundo Construcciones llegó a un acuerdo con el Ministerio de Obras Públicas de Nación, en gestiones con la Municipalidad, por el atraso en los pagos de las redeterminaciones de precios que había llevado a paralizar la obra.

“El lunes reiniciamos”, aseguró Renato Franzoni, titular de la firma. “Vamos a empezar a llevar todo el equipamiento para retomar con el ritmo normal porque se comprometieron a cumplir lo que dice el pliego (de adjudicación de obra)”, amplió.

El acuerdo comprende una actualización de los pagos de las redeterminaciones de precios y el compromiso de que las obras que se certifiquen en agosto 2021 se van a cobrar con precios de noviembre 2020. Hasta ahora estaban cobrando a valores de febrero del año pasado, y la inflación había provocado un desfasaje financiero que debió absorber la constructora hasta que se dificultó la continuidad. Pero, además, el mes próximo habrá una nueva actualización y todas las certificaciones se harán con precios de abril de 2021.

Durante la negociación, se acordó una reprogramación de las tareas pendientes, que rondan el 20%. Con este nuevo cronograma, la empresa estimó que podría terminar la obra a fines de enero de 2021. Y valoró la buena predisposición de las partes para resolver favorablemente el conflicto.

Mundo Construcciones tomó intervención en agosto del año pasado, luego de que en octubre de 2019 el municipio rescindiera el contrato inicial a Automat.