---
category: Estado Real
date: 2021-09-24T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/PUSINIERI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia inició una nueva ronda de reuniones paritarias con sindicatos
title: La provincia inició una nueva ronda de reuniones paritarias con sindicatos
entradilla: Funcionarios del gobierno provincial mantuvieron encuentros con distintas
  organizaciones gremiales en el marco de la instancia de revisión salarial acordada
  para el último trimestre.

---
Autoridades de los ministerios de Trabajo, Economía y Educación utilizaron la modalidad virtual para escuchar la situación del sector docente a través de los representantes de UDA, AMEP, AMSAFE, SADOP Santa Fe y SADOP Rosario, mientras que dialogaron presencialmente con los sindicatos que nuclean a los trabajadores de la administración central: ATE y UPCN.

Al término del encuentro el ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, indicó: “Hoy escuchamos los planteos referidos a la situación salarial por parte de los gremios, el pedido de recomposición que formularon y las distintas variables que ellos manejan y vamos a seguir manteniendo reuniones a lo largo de los próximos días con otros sindicatos”.

Asimismo, destacó que ante un esquema inflacionario distinto al planteado a principios de año y tal cual lo convenido, “queremos llevar tranquilidad: nuestra voluntad es que el salario no pierda con la inflación. Vamos a hacer los esfuerzos correspondientes para brindar una propuesta en ese sentido, que va a tener que ser considerada por cada una de los gremios”.

Consultado acerca de los indicadores que se están teniendo en cuenta para realizar la revisión, Pusineri detalló algunos acuerdos que ya se cerraron a nivel nacional y que estarían operando como marcos de referencia: “Reunidos estos elementos y con datos que provienen de instancia nacionales que se fueron dando, el gobierno va a formular una propuesta durante el transcurso de la semana que viene”.

El lunes 27 se realizará el encuentro del sector salud, mientras que se estableció una segunda reunión con el sector docente el día viernes 1 de octubre.