---
category: La Ciudad
date: 2022-01-11T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/7JEFES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Vecinos de 7 Jefes: entre la inseguridad y los ruidos molestos'
title: 'Vecinos de 7 Jefes: entre la inseguridad y los ruidos molestos'
entradilla: 'Desde la vecinal indicaron que crecieron los robos a viviendas y arrebatos
  en las calles. Respecto a los ruidos molestos, aseguraron que los vecinos "están
  hartos" y temen por situaciones de violencia.

'

---
Los vecinos de barrio 7 jefes vienen sufriendo diversos episodios de inseguridad en las últimas semanas, entre arrebatos en las calles e ingresos a viviendas, sobre todo los fines de semanas. A esa situación de malestar, se le suman los ruidos molestos y el gran movimiento diurno y nocturno de jóvenes en la zona con música a todo volumen desde los paradores y desde los autos con equipos de sonido que superan los decibeles permitidos.

Lo cierto es que este fin de semana, el hartazgo de los vecinos de barrio 7 Jefes, llegó a desbordarse a raíz de los hechos de inseguridad y de la música a todo volumen. "Llegamos al límite de temer por situaciones de violencia que se puedan llegar a dar por la propia intervención de los vecinos con la gente que no respeta los derechos de los demás; no queremos que pase lo mismo que ocurrió en barrio Candioti por la música a todo volumen en un bar", manifestaron desde la vecinal.

Este domingo por la noche, en pleno corazón de barrio 7 Jefes, delincuentes ingresaron a un departamento y se llevaron una importante suma de dinero en efectivo entre pesos, dólares y euros. Así lo denunció la víctima del cuantioso robo que se registró en la esquina de Pasaje Fraga y Laprida, a metros de bulevar Muttis.

Por otro lado, una vecina de la misma zona del barrio, manifestó que la semana pasada también robaron en su domicilio, y personal de la Comisaría 3era, al momento de recepcionarle la denuncia, le contestó: "Señora, la próxima debería comprarse un perro para que la alerte de los robos".

Respecto a los ruidos molestos que sufren a diario los vecinos de barrio 7 Jefes, sobre todo los que viven cerca de la costanera santafesina, el referente de la vecinal, Gabriel Crespo, fue contundente en sus declaraciones: "Hay mucha pasividad por parte de la municipalidad con los controles que tiene que ejercer en la vía pública y en los paradores con la inspección de los decibeles".

"Los ruidos molestos y la música a todo volumen es permanente, tanto de día como de noche. Los paradores en la costanera este santafesina arrancan a las 10 de la mañana y con un uso de la música totalmente indiscriminado. No nos queremos parar desde el lado de la prohibición, sino que desde el lado del control y el cumplimiento de las ordenanzas", indicó el referente de la vecinal 7 Jefes.

"Tenemos el problema de los autos con música a todo volumen en la zona del faro y los paradores en la costanera este. Son mini fiestas en distintos puntos del barrio", sentenció Crespo y finalizó: "Con la municipalidad comenzamos a trabajar en conjunto pero no estamos viendo resultados positivos. Los vecinos están realmente saturados de la situación. Nos parece fantástico que la gente use el barrio para el ocio y la diversión, pero necesitamos urgente controles y respecto de las normas para la buena convivencia cuidadana".