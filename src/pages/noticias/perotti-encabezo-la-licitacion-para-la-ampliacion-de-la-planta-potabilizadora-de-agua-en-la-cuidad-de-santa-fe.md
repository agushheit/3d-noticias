---
category: Estado Real
date: 2021-10-20T06:00:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/licitacion.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti encabezó la licitación para la ampliación de la planta potabilizadora
  de agua en la cuidad de Santa Fe
title: Perotti encabezó la licitación para la ampliación de la planta potabilizadora
  de agua en la cuidad de Santa Fe
entradilla: "“Se trata de una de las obras más importantes de la historia de la capital
  de la provincia, en cuanto a saneamiento y provisión de agua potable”, aseguró el
  gobernador."

---
El gobernador Omar Perotti encabezó este martes, en Santa Fe, el acto de apertura de las ofertas económicas en el marco de la licitación pública para la ampliación de la planta potabilizadora de agua de la ciudad capital. La actividad se llevó a cabo en la sede de Aguas Santafesinas SA (ASSA).

En la oportunidad, Perotti destacó que esta obra “nos va a permitir llevarle agua al 100% de los barrios de la capital”, y agregó que “también nos deja una proyección hacia el año 2050 en el crecimiento de la ciudad”.

En ese sentido, remarcó que “24 meses es el plazo final para la culminación de los trabajos, lo cual no quiere decir que haya que esperar en algunos barrios que no tienen conexiones ni la red domiciliaria, para empezar a tender la red de cañerías”, aclaró.

Asimismo, el gobernador anunció “la pronta licitación de la estación Sur, del orden de los 300 millones de pesos, que se suman al presupuesto de 4 mil millones de la planta potabilizadora. Esto significa que barrios como Loyola, Acería, Santo Domingo, Las Lomas, Juventud del Norte, Yapeyú y San Agustín, entre otros, son los que, en el corto plazo, van a poder empezar a recibir y a notar una clara mejora”, aseguró.

El mandatario provincial abundó: “Esto es proyectar la posibilidad de crecimiento, es pensar en un esquema de futuro y, también, reparar daños. Porque cuando las obras toman esta magnitud no hay municipio que pueda responder y, una provincia por sí sola puede que no alcance, sobre todo si la dimensión del retraso abarca la ciudad capital y a la ciudad más poblada de la provincia”, añadió.

“Tanto Santa Fe como Rosario, tienen el mismo problema y es una herencia que hay que reparar. Porque hablamos mucho de salud, pero sin agua potable ni cloacas, no hay salud”, afirmó el gobernador Perotti.

**AMBICIOSO PROGRAMA DE SANEAMIENTO**

Más adelante, Perotti sostuvo que “en toda la provincia, ASSA lleva adelante el mayor programa de inversión de saneamiento en la historia de Santa Fe”, y destacó “el compromiso del gobierno nacional, cumpliendo para realizar estas obras que nosotros priorizamos y que acompañamos con todo el personal técnico de ASSA y con cada uno de los municipios, mediante nuestra decisión de que esos recursos tengan esta prioridad”, expresó.

En esa línea, el gobernador indicó que “las obras emblemáticas son las que le hacen bien a la gente; y las que le resuelven el día a día de miles y miles de personas que ganan en salud y calidad de vida porque mejora la prestación de su servicio”.

Finalmente, el gobernador santafesino calificó la obra como una “de las más importantes de la historia de la capital de la provincia, en lo que hace a su saneamiento y a la provisión de agua potable”.

A su turno, la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, destacó que “esta es una obra soñada y solicitada hace muchos años”, la cual “se resolvió en medio de una pandemia, en un momento donde ASSA y muchas localidades le pusieron el hombro a la bajante del río Paraná y, aún así, la agenda en favor de la gente siguió existiendo”.

Por su parte, el intendente de Santa Fe, Emilio Jatón, recordó que “con más de 100 años, la planta estaba al límite y no aguantaba más”, al tiempo que valoró que “hoy es un día muy importante porque Nación, provincia y municipio están a la altura de las circunstancias, realizando un trabajo colectivo y planificando una ciudad de aquí a 50 años”.

Previamente, el presidente del directorio de ASSA, Hugo Morzán, manifestó que “desde las áreas técnicas de la empresa se trabajó mucho a partir de la premisa que nos planteó el gobernador Perotti, desde el inicio de su gestión, de hacer realidad estas obras que transforman la ciudad y que permiten expandir, en un ciento por ciento, la capacidad de abastecimiento de agua potable a la capital provincial”.

**DETALLES DE LA OBRA**

Los trabajos permitirán incrementar un 75 por ciento la capacidad de potabilización, mejorando el suministro en varias zonas de la capital provincial y acompañando el crecimiento de la ciudad hasta el año 2050.

Asimismo, incluyen la construcción de una nueva toma de captación de agua cruda; nuevos módulos de potabilización de alta tasa; diferentes reformas y mejoras operativas en sectores de producción; sistema de tratamiento de lodos y descarga de efluentes; y un nuevo laboratorio de calidad.

“Contará con un nuevo muelle de toma sobre el río, que permitirá captar hasta 14.500 metros cúbicos/hora (m3/h); un acueducto de 850 metros hasta la planta; y la incorporación de un módulo de alta tasa para potabilizar 6.250 m3/h y ponerlos a disposición de la red de agua potable”, explicó Morzán.

El proyecto, elaborado por el gobierno provincial a través de ASSA, cuenta con un presupuesto de 4.420 millones de pesos, con fondos provenientes del Estado Nacional por medio del Ente de Obras Hídricas de Saneamiento (Enohsa) y tendrá un plazo de ejecución de 24 meses.

**LAS OFERTAS**

En la oportunidad se presentaron dos ofertas: la UTE Obring SA-Supercemento-Basaa SA, que cotizó los trabajos en $4.309.577.452; y la UTE Proyección Electroluz-Riva-Bauza, en $4.457.520.661.

**PRESENTES**

Del acto participaron también la vicepresidenta de ASSA, Marisa Gallina; el senador nacional, Roberto Mirabella; el senador departamental, Marcos Castelló; el diputado nacional, Marcos Cleri; las concejalas locales, Jorgelina Mudallel y Laura Mondino, junto con representantes del sindicato de Trabajadores de Obras Sanitarias (Santa Fe) y demás autoridades provinciales y locales.