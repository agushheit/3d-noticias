---
category: La Ciudad
date: 2021-09-24T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/FERIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: En la ciudad ya funcionan 25 ferias del Mercado Santafesino
title: En la ciudad ya funcionan 25 ferias del Mercado Santafesino
entradilla: Se inició un nuevo espacio para emprendedores alimentarios en el Parque
  Garay, conformado por 20 puestos en los que se ofrecerán verduras, frutas, panificados,
  congelados y diversos productos complementarios

---
La Feria Cordial del Parque Garay comenzó a ocupar un espacio dentro de este importante pulmón verde de la capital provincial. De este modo, todos los viernes de 8.30 a 13 horas, cerca de 20 emprendedores ubicarán sus puestos en la intersección de avenida Presidente Perón y Salvador Caputto.

Se trata de un nuevo lugar de referencia para que santafesinos y santafesinas ofrezcan productos elaborados de manera artesanal, que se suma a las 24 ferias del Mercado Santafesino impulsadas por la Municipalidad. Algunas de ellas se desarrollan de manera semanal, mientras que otras se hacen cada 15 días o una vez al mes. A estas se suman las eventuales, convocadas esporádicamente ante un acontecimiento especial.

No obstante, hay varios procesos en marcha para nuevas aperturas, en los que se trabaja desde la Secretaría de Integración y Economía Social municipal. En ese marco, Emilio Scotta, coordinador de Promoción de la Economía Social y Popular, destacó la importancia de generar más espacios genuinos y propicios paras las ferias en la ciudad con el fin de que “trabajadores y trabajadoras de la economía social y popular, que es autogestiva y familiar, encuentren en los espacios públicos de feria un lugar para integrar la sociabilidad, la cultura y las expresiones artísticas que tiene cada barrio de la ciudad; y para volver a encontrarse y poner en valor los espacios públicos santafesinos”.

En consonancia, Scotta añadió que “la economía social y el emprendedurismo son parte de una nueva dinámica laboral que se viene desarrollando en América Latina. Santa Fe no está exenta, sino al contrario, los números indican que hay más trabajadores de ese sector y, por lo tanto, desde el municipio debemos acompañarlos con políticas públicas que les brinden la posibilidad de desarrollar su trabajo de la mejor manera posible”.

**Trabajo colectivo**

Rocío Aquino es una de las emprendedoras que desde este viernes se instalará cada semana en el parque Garay, para formar parte de la Feria Cordial. Ella trabaja con ecocuero y elabora sus productos de manera artesanal.

Según relató, la idea de ocupar un espacio en el parque se inició “a través de un grupo de gente que buscaba una forma de salir adelante, en el marco de los problemas económicos que estamos atravesando. Hoy podemos decir que somos compañeros y volcamos todas las ideas para mejorar el espacio y ayudarnos entre todos”, contó.

En referencia al espacio en que se instalará la feria, mencionó que surgió la posibilidad de ocuparlo, teniendo en cuenta las restricciones actuales por la pandemia de Covid-19. “Decidimos hacerlo al aire libre, para que todos los que vengan sepan que nos seguimos cuidando y respetando todos los protocolos vigentes”, dijo.

Por último, enumeró los rubros que se podrán encontrar: quesos y salamines, sales, miel, panificación y cosas dulces, en el rubro alimentario; y se suman accesorios para mujeres y hombres, sublimados, herrería, almohadones, plantas y tejidos entre otros. “Esperamos a todos los vecinos y vecinas que pueden acercarse, visitarnos y conocer nuestro espacio”, concluyó.

**Ferias, días, lugares y horarios**

**Feria Agroecológica de Candioti Sur**: miércoles de 9 a 13 horas, en la Plaza Emilio Zola (Caseros y Marcial Candioti)

**Feria de las 4 Vías:** miércoles y sábados de 8 a 13 horas, en la Plazoleta Laureano Maradona (Mariano Comas y Facundo Zuviría)

**Feria Popular de Berutti:** miércoles y sábados de 8 a 13 horas, en el Parque Juan B. Justo (Blas Parera y Berutti)

**Feria Cordial del Parque Garay**: viernes de 8:30 a 13 horas, en el Parque Garay (Presidente Perón y Caputto)

**Feria Alimentaria de Igualdad**: sábados de 8 a 13 horas, en la Plaza Constituyentes (Junín 3050)

**Feria de La Verdecita**: sábados de 8 a 13 horas, en la Plaza Pueyrredón (Balcarce 1650)

**Feria Paseo Capital:** jueves de 16 a 19 horas, en Nicolás Rodríguez Peña e Independencia

**Feria de Igualdad de Emprendedores**: se realiza mensualmente, los viernes de 16 a 19 horas, en la Plaza Escalante (9 de Julio y Martín Zapata)

**Feria Mujeres Emprendedoras CAMEES**: sábados de 10 a 19 horas, en la Plaza Fragata Sarmiento (San Martín y Juan de Garay)

**Feria La Roma**: se realiza mensualmente, los sábados de 16 a 20 horas, en el Parque Garay (Presidente Perón y Salvador Caputto)

**Feria Dignidad Artesanal**: sábados de 16 a 20 horas, en Peñaloza y Regimiento 12 de Infantería

**Feria Emprendedores Unidos**: sábados de 16 a 20 horas, en el Paseo Escalante (Sarmiento y Gorostiaga)

**Feria Boulevard**: sábados de 16 a 20 horas, en Pedro Vittori y bulevar Gálvez

**Feria Sueños del Norte:** sábados de 16 a 20 horas, en la Plaza Islas Baleares (Baigorrita 9300)

**Feria Sembrar Comunidad, Cosechar Futuro:** se realiza cada 15 días, los sábados de 16 a 21 horas, en la Plaza central de barrio El Pozo (Leloir y Busaniche)

**Feria de la Cámara de Emprendedores**: sábados de 15 a 19 horas, en el Paseo Escalante (9 de Julio y J.M. Zuviría)

**Feria del Sol y de la Luna**: sábados y domingos de 16 a 21 horas, en la Plaza Pueyrredón (Boulevard Gálvez y Sarmiento)

**Feria Sembrar Comunidad, Cosechar Futuro:** se realiza cada 15 días, los sábados de 13 a 19 horas, en la Plaza Estanislao López de La Guardia (Alissio y Estanislao López)

**Feria Murat**: domingos de 16 a 20 horas, en la Plaza Guadalupe (Javier de la Rosa y Patricio Cullen)

**Paseo Costanera Oeste:** domingos de 16 a 21 horas, en Almirante Brown 5700

**Feria Cultural Parque Federal**: domingos de 16 a 20 horas, en el Parque Federal (Quintana y las vías)

**Feria Incuba:** domingos de 16 a 20 horas, en la Plaza Constituyentes (4 de Enero y Junín)

**Feria del Botánico**: se realiza cada 15 días, los domingos de 16 a 19 horas en el Jardín Botánico Municipal (Gorriti 3900)

**Feria de Pie**: se realiza cada 15 días, los domingos de 12 a 18 horas, en la Plaza Fournier (Castelli y Gobernador Freyre)

**Feria de Emprendedores de Casanello**: domingos de 14 a 18 horas en la Costanera Oeste (Almirante Brown y Ángel Casanello).