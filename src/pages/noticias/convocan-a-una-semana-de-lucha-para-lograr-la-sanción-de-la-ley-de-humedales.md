---
layout: Noticia con imagen
author: "Fuente: La Capital"
resumen: Ley de humedales
category: Agenda Ciudadana
title: Convocan a una semana de lucha para lograr la sanción de la ley de humedales
entradilla: Unas 615 mil personas firmaron las peticiones online, aseguraron
  miembros de la Red Nacional de Humedales (ReNaHu).
date: 2020-11-15T14:54:10.023Z
thumbnail: https://assets.3dnoticias.com.ar/arboles-quemados.jpg
---
Ambientalistas de todo el país convocaron a una semana de lucha para lograr el urgente avance en el Congreso de la Nación del proyecto de Ley de Humedales, que apoyan 315 organizaciones de la sociedad civil y 190 profesionales de institutos y universidades argentinas.

Integrantes de la Red Nacional de Humedales (ReNaHu) señalaron que la jornada de lucha y visibilización “es para pedir por la urgente sanción de una ley de presupuestos mínimos para la protección de los humedales”.

ReNaHu señaló que el proyecto de Ley cuenta con el apoyo explícito y solicitud de “315 organizaciones de la sociedad civil, presentada a los diputados de la Comisión ad hoc el 30 de septiembre, por iniciativa del Programa Humedales sin Fronteras”.

también, con el apoyo “de 190 profesionales de distintos institutos y universidades de todo el país” y “más de 615 mil personas que hasta el momento han firmado las peticiones online que reclaman una Ley de Humedales Ya”.

“El apoyo a la sanción de la Ley de Humedales es tan abrumador como urgente”, subraya ReNaHu.

La Multisectorial por los Humedales criticó la actuación de la Justicia Federal en los incendios en las islas del Paraná y dijo que después de nueve meses “más de 300 mil hectáreas devastadas, daños incalculables a flora y fauna, no hay un solo detenido”.

“Después de nueve meses y 300 mil hectáreas devastadas, daños incalculables al humedal, a su flora y fauna, lo que se observa es inoperancia y complicidad de los tres poderes del Estado”, dijo a Télam Ivo Peruggino, de la Multisectorial rosarina. Añadió que al cabo de todo ese tiempo “igual siguen quemando y están sucediendo cosas sobre la tierra quemada que todos esperábamos”

“Se están viendo maquinarias para hacer terraplenes con el fin de evitar que esos terrenos se inunden y sean más productivos. Se ven máquinas de siembra y si sembramos, significa que luego vendrán los agrotóxicos”, advirtió el ecologista. Y tras apuntar que “al fuego y al humo, lo vemos y sentimos, pero los agrotóxicos y las fumigaciones no la vamos a ver”, Peruggino, reveló: “Ya estamos viendo además barcazas llenas de vacas que entran y salen del humedal como si nada”.

Los humedales del país “sufren fuertes presiones por el avance del sector minero, forestal, la ganadería industrial, la agricultura intensiva, las urbanizaciones y el turismo, que se llevan adelante sin respetar los humedales y sus pobladores”, agrega.

La Ley de humedales es una iniciativa ciudadana con ocho años de recorrido en el Congreso Nacional “que ya obtuvo dos medias sanciones por parte del Senado, (2013 y 2016), pero no se logró que fuera aprobada por la Cámara de Diputados”.

“Este año al calor de los incendios que arrasaron cientos de miles de hectáreas de humedales y bosques en todo el país, pero con mayor intensidad en las provincias del litoral, proliferaron los proyectos de ley de humedales”, puntualiza.

“Son 15 los proyectos que tienen estado parlamentario en las Cámaras de Diputados y Senadores. ¡Ley de Humedales Ya!: 15 proyectos y ninguna ley”, finaliza diciendo ReNaHu.