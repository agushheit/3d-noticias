---
category: Agenda Ciudadana
date: 2021-02-22T06:30:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIOLENCIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Frederic: "Los femicidios deben ser una prioridad en la agenda del Gobierno
  nacional"'
title: 'Frederic: "Los femicidios deben ser una prioridad en la agenda del Gobierno
  nacional"'
entradilla: La ministra Sabina Frederic expresó su preocupación por el aumento de
  los casos de violencia de género en el último año e indicó que está trabajando con
  los ministerios de Mujeres, Género y Diversidad.

---
La ministra de Seguridad de la Nación, Sabina Frederic, aseguró este domingo que el tratamiento de los casos de femicidios “debe ser una de las prioridades de la agenda del Gobierno nacional” y destacó la decisión del presidente Alberto Fernández de crear un Consejo Federal para la Prevención y Abordaje de Femicidios, Travesticidios y Transfemicidios.

La ministra Sabina Frederic expresó su preocupación por el aumento de los casos de violencia de género en el último año e indicó que trabaja con los ministerios de Mujeres, Género y Diversidad y de Justicia para abordar la problemática.

“Nosotros hemos venido trabajando todo el año pasado para tratar de abordar las violencias extremas por razones de género. Esa tarea nos ha llevado a diseñar una serie de instrumentos para mejorar la intervención, para anticiparnos a estos hechos trágicos, repudiables, lamentables, que azotan a la Argentina, y en algunas zonas del país más que en otras”, expresó en diálogo con CNN Radio.

Frederic destacó la decisión del presidente Alberto Fernández de crear el Consejo para la Prevención y Abordaje de Femicidios, Travesticidios y Transfemicidios.

La ministra señaló que “este Consejo está pensado para tratar de fortalecer y sobre todo comprometer a las provincias, a todos los equipos y operadores que trabajan en este tema, porque hay mucha gente tratando de resolver este problema, y hay muchos casos que son exitosos", pero hay que "evitar la mayor cantidad de agresiones de este tipo porque son una herida a todos”, enfatizó.

“Sabemos que no vamos a impedir todos los femicidios de acá a un mes, pero tenemos la responsabilidad de evitar todos los casos que podamos, también considerando que las herramientas para la intervención las tienen los gobiernos provinciales”, precisó.

La titular de la cartera de Seguridad aseguró que la tarea del Gobierno nacional será “articular las intervenciones, apoyar, poner el tema en agenda todo el tiempo, que el tema esté entre las prioridades de la agenda de gobierno”.

Por otra parte, se refirió al femicidio de Úrsula Bahillo, la joven de 18 años asesinada el 8 de febrero de varias puñaladas por su expareja en la localidad bonaerense de Rojas.

“En este caso hubo varias omisiones, Los policías que estaban en esa localidad mostraron cierta inoperancia, sobre todo en lo que fue la contención de la población y de la familia”, señaló.

La funcionaria dijo también que existieron “omisiones de los operadores judiciales”, al reseñalr que Úrsula "había hecho la denuncia, el asesino tenía muchas denuncias previas, y había como una parálisis en cuanto a la reacción fuerte y decidida de la Justicia que es la que tiene que actuar”.

“Hay que repartir las responsabilidades por que llega un momento en que la policía no puede tomar decisiones, las decisiones las toma el Poder Judicial”

La ministra aseguró que “muchas veces las medidas que se toman, se toman revictimizando a la víctima, cercando a la víctima en lugar de cercar al victimario”.

“Me parece que ahí hay que repensar y todos tenemos que repensar esas intervenciones para que la víctima no termine acorralada como termina pasando en la mayor parte de los casos”, concluyó.