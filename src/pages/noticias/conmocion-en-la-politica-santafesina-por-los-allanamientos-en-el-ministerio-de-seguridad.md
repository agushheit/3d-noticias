---
category: La Ciudad
date: 2021-11-27T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/MSAIN.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Conmoción en la política santafesina por los allanamientos en el Ministerio
  de Seguridad
title: Conmoción en la política santafesina por los allanamientos en el Ministerio
  de Seguridad
entradilla: " Fue en busca de evidencias sobre espionaje ilegal ordenado por el ex
  ministro Marcelo Sain. Habría archivos sobre legisladores, empresarios, gremialistas,
  funcionarios judiciales y periodistas.\n"

---
Los allanamientos a las oficinas del Ministerio de Seguridad, tanto en Rosario como en Santa Fe, en busca de evidencias sobre espionaje ilegal ordenado por el ex ministro Marcelo Sain a dirigentes políticos, empresarios y periodistas, entre otros, generó un verdadero tembladeral político en la provincia y, todo indica, precipitará las renuncias de los funcionarios porteños que llegaron con el ahora asesor de Aníbal Fernández y seguían trabajando en la cartera que ahora conduce Jorge Lagna.

"Hay carpetas pero no escuchas", fue la frase de una fuente del gobierno provincial, que buscó así diferenciar el caso de otras situaciones de espionaje, como las que sufrieron los familiares de los muertos del ARA San Juan por las cuales ahora está investigado el expresidente Mauricio Macri.

El matiz incluye una admisión: Sain realmente acumulaba información sensible sobre distintas personalidades de la provincia, presumiblemente para realizar eventuales "carpetazos". El ex ministro "hacía una ficha de cada persona con la que trataba o tenía algún cruce", agregó otra fuente que transitó los pasillos de la cartera de Seguridad.

Muchos de esos archivos se nutrían de información que fue comprada en bases de datos a través de la utilización de fondos reservados asignados al propio Ministerio de Seguridad.

"Es una barbaridad", admitieron desde el gobierno provincial. Fuentes de la investigación explicaron que por esta razón uno de los delitos atribuibles a los funcionarios involucrados sería malversación de fondos públicos.

Uno de los celulares secuestrados hoy en el allanamiento es el del viceministro de Seguridad, Germán Montenegro, hombre de estrecha confianza de Sain. A al menos otros dos funcionarios se les quitó también sus aparatos. Todos ellos protagonizaban una feroz interna con el ministro Lagna, por lo cual se especulaba con una próxima renuncia que ahora es casi un hecho consumado.

"Razonablemente esto apura los tiempos", dijo una fuente de la Casa Gris.

El gobernador Omar Perotti convocó para la semana que viene a la oposición a debatir medidas para combatir la crisis de la seguridad pública. Seguramente querrá llegar al miércoles, día de ese encuentro, con el camino algo más despejado.