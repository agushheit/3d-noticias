---
category: Agenda Ciudadana
date: 2021-03-23T06:25:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/festram.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: 'Acuerdo en la paritaria municipal: aceptaron la propuesta salarial'
title: 'Acuerdo en la paritaria municipal: aceptaron la propuesta salarial'
entradilla: El incremento será del 35%, similar a lo ofrecido al resto de los gremios.
  El aumento será escalonado y el pago en tres tramos.

---
La Festram aceptó este lunes la propuesta salarial elevada por intendentes y presidentes comunales luego de un nuevo encuentro paritario, y tras el paro de 48 horas que realizaron miércoles y jueves de la semana pasada los trabajadores municipales tras la falta de oferta.

Los gobiernos locales ofrecieron un 35% de incremento salarial, a pagar en tres tramos y con una suba escalonada. En detalle, consiste en un 18% retroactivo al 1º de marzo; 8% en junio y 9% en septiembre.

Además, en el acta paritaria se dejó asentado que habrá una cláusula de revisión en el mes de noviembre más la actualización de las asignaciones familiares que reciben los trabajadores de municipios y comunas.

La propuesta estuvo en sintonía con el ofrecimiento del gobierno provincia a los gremios estatales, docentes y de salud.