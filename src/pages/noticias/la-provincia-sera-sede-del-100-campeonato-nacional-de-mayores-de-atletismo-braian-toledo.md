---
category: Deportes
date: 2020-12-15T12:55:44Z
thumbnail: https://assets.3dnoticias.com.ar/atletismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia será sede del 100° Campeonato Nacional de Mayores de Atletismo
  “Braian Toledo”
title: La provincia será sede del 100° Campeonato Nacional de Mayores de Atletismo
  “Braian Toledo”
entradilla: Atletas de todo el país se reunirán en Rosario para lograr marcas para
  clasificar a los Juegos Olímpicos de Tokio 2021.

---
El gobierno de la provincia, a través de la Secretaría de Deportes del Ministerio de Desarrollo Social, la Confederación Argentina de Atletismo, y la Organización de la Federación Argentina de Atletismo y la Asociación local, organizará el **100° Campeonato Nacional de Mayores “Braian Toledo”**.

Del **18 al 20 de diciembre**, atletas de todo el país se reunirán en la ciudad de **Rosario** para lograr marcas que les permitan clasificar a los Juegos Olímpicos de Tokio 2021. Se trata del **primer certamen nacional que se realizará en contexto de pandemia**, por lo que se extremarán los cuidados según los protocolos vigentes.

Al respecto, el ministro de Desarrollo Social, Danilo Capitani, expresó que “es un orgullo para la provincia de Santa Fe ser sede del certamen número 100 del Campeonato Nacional de Mayores y recibir a atletas de primer nivel. El certamen estará condicionado por la nueva normalidad, cuidando y respetando los protocolos, al igual que se hace en otros países del mundo, con esquemas que permitan retomar las actividades, salvaguardando siempre la integridad de los deportistas”.

Por su parte, la secretaria de Deportes, Claudia Giaccone, resaltó que “venimos trabajando coordinadamente con el equipo del Ministerio de Salud de la provincia y con todas las instituciones deportivas desde que comenzó la pandemia y los resultados fueron muy buenos. Tempranamente pudimos habilitar los entrenamientos individuales, después realizar competencias virtuales y con el tiempo también de manera presencial, cerrando el año con este certamen a nivel nacional, que con todos los cuidados a los que estamos acostumbrados, reunirá por primera vez en el 2020 a las y los mejores atletas del país”.

La Pista Luis Brunetto del Estadio Municipal Jorge Newbery, que será el escenario del primer y único torneo nacional de estas características, recibirá atletas como Germán Chiaraviglio, Belén Casetta, Jennifer Dahlgren, Carlos Layol, los maratonistas ya clasificados a Tokio 2020 Joaquín Arbe, Marcela Gómez y Eulalio Muñoz, los santafesinos que participaron en los Juegos Olímpicos de la Juventud Buenos Aires 2018 Julio Nobile y Lazaro Bonora y el histórico Adrián Marzo, en otros.

***

## **TELEVISACIÓN**

La **señal de TyC Sports** será la encargada de emitir en directo entre las 9 y las 11 horas los días viernes 18 y sábado 19 y, en diferido, entre las 13 y las 15 horas del domingo 20.

***

## **HONOR A BRAIAN TOLEDO**

El certamen lleva el nombre del atleta olímpico Braian Toledo, de 26 años, quien murió producto de un accidente de tránsito en el mes de febrero, en Marcos Paz, su localidad natal. Medalla de oro en los Juegos Olímpicos de la Juventud Singapur 2010, fue finalista olímpico en Río 2016 y se estaba preparando para participar en Tokio 2020.