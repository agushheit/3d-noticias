---
category: La Ciudad
date: 2021-02-10T18:12:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/feria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Santa Fe Capital se prepara para disfrutar el primer fin de semana largo
  del año
title: Santa Fe Capital se prepara para disfrutar el primer fin de semana largo del
  año
entradilla: La ciudad se prepara para recibir visitantes de la región y el país. Por
  tal motivo, la Municipalidad, junto al sector privado, preparó una agenda turística
  muy variada.

---
Entre el sábado 13 y el martes 16, en lo que será el primer fin de semana largo del año, la Municipalidad ofrecerá paseos turísticos, visitas guiadas, actividades culturales y recreativas que se complementan con excelentes servicios turísticos para hacer de Santa Fe Capital una gran experiencia.

Es importante remarcar que la ciudad de Santa Fe fue distinguida por el Consejo Mundial de Viajes como “Destino Seguro”, debido a la elaboración de diversos protocolos para cada rubro vinculado al desarrollo de la actividad turística. Estos protocolos fueron producidos conjuntamente por los sectores público y privado, y están disponibles en la [web](https://lt.dptagent.com/9c46e9a20985cf11bbd49ed7be3dc7d3-919442e3fb9157317d319f740d4c9753) de la Municipalidad.

**Propuestas gratuitas**

Desde comienzos de la temporada de verano, el municipio ofrece una variada agenda turística, cultural y recreativa para santafesinos y visitantes. “Verano Capital” es la propuesta que engloba todas las actividades entre las que se cuentan los paseos guiados de “Mi ciudad como turista”, el programa destinado a promover y poner en valor el acervo cultural, patrimonial e histórico de la capital provincial. El mismo ofrece la posibilidad de realizar visitas guiadas gratuitas por diferentes espacios emblemáticos como el Puerto, el Casco Histórico, el Bulevar y la Peatonal, entre otros.

Durante el fin de semana de Carnaval, “Mi ciudad como turista” estrenará un nuevo paseo: el Camino de la Constitución. Se trata de un recorrido por diversos puntos de la ciudad en el que se puede apreciar el legado constitucionalista en Santa Fe. A lo largo del mismo se visitarán lugares que tuvieron un papel fundamental en la firma de la Constitución de 1853 y sucesivas reformas. Será el sábado 13 a las 18 horas. El punto de encuentro será la Plaza 25 de Mayo (San Martín y 3 de Febrero) y, como ya es costumbre, el personal encargado de realizar la visita guiada estará identificado con sombrillas verdes.

Otra de las propuestas de recorrido para el fin de semana es la Manzana Jesuítica (San Martín 1540) que abrirá sus puertas el sábado 13, de 9 a 12 y de 16.30 a 19.30 con visitas guiadas a las 10 y a las 18 horas. También se podrá recorrer ese histórico solar el lunes 15, en el horario especial de las 18. El lunes 15, también, habrá una edición especial de los paseos guiados de “Mi ciudad como turista” con Paseo Costanera a las 19 horas y el punto de encuentro será el Faro de avenida Costanera.

Vale destacar que estos paseos cuentan con audioguías especialmente preparadas para quienes deseen realizarlos sin depender de los horarios y días establecidos por la Municipalidad. Las mismas se pueden descargar o escuchar online desde la [web](https://lt.dptagent.com/5d30073938b9025c52f9996abb72c0a3-919442e3fb9157317d319f740d4c9753) del Municipio o desde el perfil de Santa Fe Capital en [Spotify](https://lt.dptagent.com/e2ddb8c5b576b776d7240c66d433953f-919442e3fb9157317d319f740d4c9753).

**Paseos náuticos y urbanos**

Nuestra ciudad, rodeada de ríos, riachos y lagunas, es una oportunidad única para conocer y disfrutar de la naturaleza típica del litoral. En ese marco, hay varias formas de disfrutar el entorno costero, entre las que se destacan los paseos náuticos. Durante el fin de semana de Carnaval se podrá disfrutar de los recorridos a bordo de La Ribereña y del Catamarán Costa Litoral, así como de otros especialmente elaborados por Agencias de Viajes y Turismo locales.

El Catamarán Costa Litoral zarpará desde el Dique 1 del Puerto los días sábado 13, domingo 14 y lunes 15 a las 17 horas, para recorrer la zona de islas aledañas a la ciudad. Quienes deseen más información pueden consultar al teléfono 0342 456 4381.

Por otra parte, la empresa de Viajes y Turismo Costanera 241 ofrecerá durante el fin de semana de Carnaval tres paseos náuticos para vivir el litoral al máximo. Las propuestas son pesca embarcada, “safari” fotográfico y paseo en yate por el Río Paraná, con gastronomía incluida.

Además, durante el fin de semana, el sector privado de la ciudad comercializará diversos paseos para conocer costumbres y pasiones santafesinas. Habrá City Tours, excursiones gastronómicas y cerveceras y visitas al estadio del Club Atlético Colón. Estas propuestas son ofrecidas y comercializadas por las empresas de Viajes y Turismo de la ciudad Expertur, STL y Costanera 241.

También durante el fin de semana se podrá disfrutar de “Paradiso Puerto”, un espacio de cine al aire libre y música en vivo. En esta oportunidad se presentará “Koino Yokan”, con entradas a la venta en [https://www.eventbrite.com.ar/e/koino-yokan-en-vivo-tickets-134342793913](https://www.eventbrite.com.ar/e/koino-yokan-en-vivo-tickets-134342793913 "https://www.eventbrite.com.ar/e/koino-yokan-en-vivo-tickets-134342793913")[ -](https://lt.dptagent.com/d2f7bee020c144c645c79c26252c845a-919442e3fb9157317d319f740d4c9753).

**Entretenimiento y compras**

La ciudad ofrece diversas propuestas de entretenimiento y compras. Vale destacar que es fundamental respetar las medidas de cuidado personal de cara a la pandemia de Coronavirus: uso obligatorio de tapabocas, distanciamiento social e higiene de manos. Los shoppings mantienen sus puertas abiertas, como así también los centros comerciales a cielo abierto y el casino, en el complejo del Puerto.

Finalmente, las ferias artesanales y de antigüedades de la ciudad funcionarán en diversos espacios. Entre ellas se destaca la tradicional Feria del Sol y de la Luna en Plaza Pueyrredón, en bulevar Gálvez al 1600.

**Información turística**

Durante el fin de semana de Carnaval, los Centros de informes de la ciudad funcionarán en los siguientes horarios:

– Terminal de Ómnibus (Belgrano 2910): todos los días de 8 a 20 horas

– Centro de informes del Puerto (cabecera del Dique 1): el sábado 13, de 10 a 18; el domingo 14, de 9 a 19 y el lunes 15 y martes 16, de 12 a 18 horas.

– Peatonal (San Martín 2020): el sábado 13, lunes 15 y martes 16, de 9 a 13 y de 16 a 20 horas.

También se podrá conocer más sobre la oferta diaria de propuestas en las redes de la Municipalidad ([Facebook](https://lt.dptagent.com/43e2d38d0a50ebe6913e9e94b5826413-919442e3fb9157317d319f740d4c9753), [Twitter](https://lt.dptagent.com/05890e8c614c686d96c86693932b5271-919442e3fb9157317d319f740d4c9753) e [Instagram](https://lt.dptagent.com/b697476dd00adab9e02678acf5a74490-919442e3fb9157317d319f740d4c9753)) y en las web oficiales [santafeciudad.gov.ar](http://santafeciudad.gov.ar/) y [santafenoticias.gob.ar](http://santafenoticias.gob.ar/).

En tanto, se recuerda que los operadores receptivos de la ciudad son:

**Catamarán Costa Litoral: Dique I – Puerto de la Ciudad de Santa Fe**

Teléfono: (0342) 4564381

Facebook: @CostaLitoral

Instagram: /Catamaran_costa_Litoral

E-mail: [reservas@costalitoral.info](mailto:reservas@costalitoral.info)

Web: [www.costalitoral.info](http://www.costalitoral.info/)

  
**Costanera 241 Leg. 8113 – Santa Fe**

Teléfono: (0342) 5950508

E-Mail: [claudia@costanera241.tur.ar](mailto:claudia@costanera241.tur.ar)

Web: [www.mirasantafe.costanera241.tur.ar](http://www.mirasantafe.costanera241.tur.ar/)

  
**STL Turismo Leg. 14411 – Santa Fe**

Teléfono: (0342) 4283096 – 4283330

E-mail: [comercial@stloperador.com.ar](mailto:comercial@stloperador.com.ar)

  
**Expertur Leg. – 5070**

Teléfono: (0342) 4521495

E-mail: [expertur@expertur.tur.ar](mailto:expertur@expertur.tur.ar)