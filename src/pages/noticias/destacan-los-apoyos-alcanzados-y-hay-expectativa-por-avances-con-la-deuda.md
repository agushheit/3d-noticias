---
category: Agenda Ciudadana
date: 2021-05-14T06:00:25-03:00
thumbnail: https://assets.3dnoticias.com.ar/DEUDA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Destacan los apoyos alcanzados y hay expectativa por avances con la deuda
title: Destacan los apoyos alcanzados y hay expectativa por avances con la deuda
entradilla: El jefe de Estado volverá en las próximas horas de Europa con espaldarazos
  públicos y explícitos de los mandatarios visitados a los que se suma la declaración
  de la titular del Fondo Monetario, Kristalina Georgieva.

---
La gira oficial del presidente Alberto Fernández por Europa generó un clima de entusiasmo en la Casa Rosada por los apoyos cosechados a la posición argentina en la renegociación de la deuda con los organismos multilaterales de crédito, un balance que despierta expectativas sobre posibles próximos anuncios en la materia.

El jefe de Estado volverá en las próximas horas de Europa con espaldarazos públicos y explícitos de los mandatarios de Portugal, Francia, Italia y España; a los que se suma la declaración de la titular del Fondo Monetario, Kristalina Georgieva, diciendo haber "tomado nota" del pedido argentino para "reformar la política de sobrecargos" del organismo.

"El Presidente fue con el ministro (de Economía) Martín Guzmán con la idea de comenzar a cerrar un problema estructural del país. Un problema heredado. Y los apoyos fueron muy significativos", señalaron fuentes del Gobierno nacional.

Las fuentes se referían a las negociaciones que mantiene Argentina, a través de Guzmán, con el organismo multilateral de crédito para refinanciar los vencimientos de deuda, que de no hacerlo comenzarían a pagarse este mes y concluirían en 2024, sobre la base de una deuda de 44.000 millones de dólares tomada por la gestión de Juntos por el Cambio.

Pero, además, Argentina también tiene deuda tomada con el Club de París por 2.400 millones de dólares, de la que en mayo entró a correr el vencimiento.

**Negociaciones por deudas con el Club de Paris y el FMI**

Respecto a las negociaciones -tanto con el Fondo Monetario Internacional como con el Club de París-, por estas horas en los pasillos de la Casa Rosada no niegan la existencia de "expectativas" por un posible acuerdo, pero dicen que se deben respetar tiempos y que, en el caso de haber novedades, serán comunicadas por el titular del Palacio de Hacienda oportunamente.

"El año pasado, cuando se trabajaba con los acreedores privados, había economistas como Daniel Marx, Toto Caputo, y Prat Gay que desde meses antes hablaban y decían cómo y cuándo se debía cerrar. Ninguno de esos proponía que los argentinos ahorraran más de 30 mil millones de dólares", agregaron las fuentes en referencia a lo que el Gobierno de Alberto Fernández logró el año pasado al renegociar la deuda que el país mantenía con bonistas privados.

En referencia a un despacho que esta tarde publicó la Agencia Bloomberg, en el que se informaba un aplazamiento acordado entre Argentina y el Club de París, insistieron: "Esperemos los tiempos de Guzmán".

Incluso el presidente Alberto Fernández se expresó en el mismo sentido al ser consultado en Roma por la prensa que viajó para cubrir su gira, al rechazar esa especulación con un rotundo "no".

Respecto de la negociación, el discurso de los voceros gubernamentales no se aparta de lo dicho públicamente por el propio Presidente: "Todo acuerdo debe permitir el desarrollo económico argentino".

"Nosotros buscamos un acuerdo se pueda sostener en el tiempo y que no postergue más a los argentinos. Los argentinos ya han sido postergados por el enorme crédito que le ha dado el FMI", dijo el mandatario argentino en la primera escala de su gira, en Portugal, en una declaración conjunta con el primer ministro Antonio Costa.

"Le pido al Fondo comprensión sobre la naturaleza de ese acuerdo que fue nocivo para la Argentina y comprensión por el tiempo que vive el mundo por la pandemia. Ojalá podamos llegar a ese cuerdo rápido”, completó en esa primera oportunidad.

Esa idea fue replicada en cada uno de los encuentros con los líderes europeos y con el papa Francisco.

**La vacuna como bien público global**

Según la visión oficial, durante la gira, Fernández consiguió además la misma calidad de apoyos a su idea de convertir a la vacuna contra el coronavirus como "un bien público" global, y que no existan las diferencias de hoy en su acceso entre los diferentes países ante la escasez en la oferta.

El Presidente había sostenido esa posición -casi en soledad- durante las reuniones del G-20 y que ahora, en cambio, consiguió el visto bueno de todos sus anfitriones, destacaron las fuentes

**Encuentro con el Papa**

Finalmente, en Balcarce 50 se refirieron al encuentro con el Papa Francisco y señalaron: "La reunión fue muy buena. La idea del Gobierno es evitar involucrar al Papa en cuestiones domésticas y, en cambio, buscar su apoyo en temas de discusión global, como el acceso a las vacunas", indicaron.

En Portugal, Alberto Fernández celebró que AstraZeneca haría efectivo "para fines de mayo" el envío de 4 millones de dosis, lo que constituiría el primer lote mexicano de vacunas de ese laboratorio que llegan a la Argentina, donde se produce su principio activo, remitido a México para la elaboración final.

Esta dotación de vacunas se sumaría a las que el Gobierno argentino viene trayendo desde Rusia, China e India, y a las conversaciones que continúa manteniendo para conseguir más dosis que se sumen a las casi13 millones ya arribadas en vuelos de la aerolínea de bandera.