---
category: Estado Real
date: 2021-04-19T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/PROTOSANITARIOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia despliega acciones de control de protocolos sanitarios en bares
  de Santa Fe y Rosario
title: La Provincia despliega acciones de control de protocolos sanitarios en bares
  de Santa Fe y Rosario
entradilla: Lo hace a través del ministerio de Trabajo, Empleo y Seguridad Social
  de forma conjunta con equipos municipales. Se verifica el cumplimiento estricto
  de los protocolos vigentes en el marco del Covid19.

---
El Ministerio de Trabajo, Empleo y Seguridad Social, de forma conjunta con equipos municipales, realizaron durante el fin de semana más de 30 inspecciones en simultáneo en las ciudades de Santa Fe y Rosario en el marco de las acciones de control que está llevando adelante el gobierno provincial en lo que respecta a la implementación de los protocolos establecidos para la actividad de bares y restaurantes.

El Ministerio de Trabajo verificó el cumplimiento de los protocolos sanitarios respecto de los trabajadores en relación de dependencia y de las normas de distanciamiento social y cantidad máxima de comensales permitidos al mismo tiempo dentro de un local, denominada técnicamente “aforo”.

Al respecto, el ministro de la cartera laboral, Juan Manuel Pusineri, destacó la capacidad de los distintos niveles del estado para actuar articuladamente cuando se tiene un objetivo claro: “Nos hemos puesto como meta cuidar la salud de los santafesinos protegiendo la actividad económica, es por ello que los operativos continuarán durante todo el fin de semana de manera sistemática. Vamos a ser muy estrictos y no descartamos apelar a la clausura de locales en aquellos casos en que consideremos necesario.”

En Santa Fe las inspecciones se organizaron en conjunto con la Municipalidad. En Rosario las fiscalizaciones se realizaron por separado, pero en cooperación y colaboración con la Municipalidad, dando aviso en los casos de incumplimiento, a fin de que los agentes municipales pudiesen verificar la situación y hacer cumplir las ordenanzas pertinentes.

Por parte del Ministerio de Trabajo el operativo estuvo a cargo del Subsecretario de Fiscalización Facundo Osia quien destacó que “en general el operativo fue positivo ya que en ambas ciudades vimos un alto nivel de cumplimento de las normas de distanciamiento social en el interior de los locales gastronómicos.”

En lo relacionado al control de la situación de los trabajadores en relación de dependencia, el funcionario informó que “las inspecciones permitieron comprobar que la gran mayoría cuenta con los elementos de protección necesarios, ha recibido capacitación respecto de las normas establecidas y la está cumpliendo”

Asimismo, agregó que “lo que en muchos casos ha faltado es la figura del “veedor” o encargado de hacer cumplir los protocolos covid19 dentro del establecimiento, lo cual hemos solucionado nombrando a una persona en el mismo momento de la visita.”

Respecto de la situación en el exterior de los bares, también se verificó un alto grado de cumplimiento de los protocolos sanitarios por parte de los comensales y en los casos en que se encontraron grupos de personas reunidos que excedían el máximo permitido se los intimó a los propietarios a reubicar las mesas, lo cual fue cumplimentado de forma inmediata y sin que se genere ningún inconveniente.