---
category: Cultura
date: 2021-08-10T06:00:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/DISEÑA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Convocatoria abierta para La Diseña 2021-2022
title: Convocatoria abierta para La Diseña 2021-2022
entradilla: La Municipalidad de Santa Fe abre la convocatoria para marcas y proyectos
  de diseño de la ciudad, su área metropolitana, y Paraná.

---
La Municipalidad convoca a diseñadoras y diseñadores de la ciudad de Santa Fe, Paraná y del Área Metropolitana (Ordenanza N° 12.326), a participar de La Diseña 2021/2022. La convocatoria es para sumarse a las ediciones de esta muestra, consolidada en la capital santafesina como el espacio de encuentro y comercialización del diseño local, desde la feria navideña en diciembre 2021 en la Estación Belgrano hasta los eventos que se realicen en otros espacios municipales, como el Mercado Progreso, hasta noviembre de 2022 inclusive.

El llamado está abierto hasta el 20 de agosto y se seleccionarán hasta 240 proyectos, que a los efectos de ser admitidos para participar de la selección del jurado deberán cumplir con las instancias de clínicas de formación para la revisión y mejoramiento de las producciones, entre septiembre y octubre. El jurado estará integrado por tres representantes elegidos por la Mesa de Diseño, según la Ordenanza N° 12.656/19.

**Cómo inscribirse**

Se debe completar un formulario online disponible en la sección de Convocatorias vigentes de la Secretaría de Educación y Cultura en la página oficial del municipio, donde también pueden consultarse las bases completas de la convocatoria: ([https://santafeciudad.gov.ar/secretaria-de-educacion-y-cultura/convocatorias-vigentes/](https://santafeciudad.gov.ar/secretaria-de-educacion-y-cultura/convocatorias-vigentes/ "https://santafeciudad.gov.ar/secretaria-de-educacion-y-cultura/convocatorias-vigentes/")).

El formulario solicita datos formales de la marca y su titular e información sobre las características del emprendimiento, y como archivo adjunto, una carpeta en formato PDF (hasta 10Mb) que describa las particularidades formales estéticas de la propuesta y sus procesos de producción. También se deberá presentar una carta de intención que exprese por qué considera importante la participación de su proyecto.

**Rubros**

Todas las propuestas deberán contar con diseños originales, y en caso de que sean intervenciones sobre prendas/objetos/soportes industrializados, deberán estar intervenidos en un 80%.

Los rubros contemplados en esta edición son 1) “Accesorios y complementos”: Joyería, sombreros, calzado, accesorios y portables, etc.; 2) “Almacén natural”: Cuidado corporal, perfumería, esencias, sahumos, velas, envasados, etc.; 3) “Comunicación multisoporte”: Diseño gráfico, productoras de contenidos, realidad aumentada, realidad virtual, videojuegos, etc.; 4) “Indumentaria y Textil”: Confección, intervención, estampación, y/ó tejido de prendas para uso femenino, masculino, de niñas, niños y mascotas, etc. ; y 5) “Objetos y mobiliario”: Juguetes y lúdicos, editoriales, decoración y equipamiento para el hogar y mascotas, paisajismo, etc.

**Clínicas**

Los contenidos de las clínicas de formación serán propuestas por la Mesa de Diseño y se desarrollarán entre septiembre y octubre de 2021. Serán obligatorias para las marcas seleccionadas y otorgarán puntaje, pudiéndose ofrecer capacitaciones optativas que se suman a las tres obligatorias. El cursado será de modalidad presencial o virtual según el caso.