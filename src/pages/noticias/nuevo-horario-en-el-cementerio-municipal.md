---
category: La Ciudad
date: 2021-04-01T07:14:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/cementerio.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Nuevo horario en el Cementerio Municipal
title: Nuevo horario en el Cementerio Municipal
entradilla: Desde hoy -jueves 1° de abril- comienza a regir el horario de invierno
  en la necrópolis local.

---
A partir de este jueves 1° de abril comienza a regir el horario invernal en el Cementerio Municipal. Por tal motivo, la Municipalidad comunica que las visitas se podrán hacer de lunes a sábados, de 9 a 16 horas. En tanto, los domingos y feriados el horario será de 8 a 12.