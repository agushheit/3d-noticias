---
category: La Ciudad
date: 2021-11-02T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/setybal.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Definieron las zonas de privilegio para navegar en la Setúbal y alrededores
title: Definieron las zonas de privilegio para navegar en la Setúbal y alrededores
entradilla: "La Prefectura Naval de Santa Fe recibió a referentes de clubes y actividades
  náuticas de la ciudad. Las áreas se establecieron para la convivencia y seguridad
  entre cada deporte acuático. \n\n"

---
Ante la llegada de la temporada estival y la creciente presencia de embarcaciones en los distintos sectores de la laguna Setúbal y espejos de agua que circundan a Santa Fe y localidades aledañas, en la sede de la Prefectura Naval Argentina (Seccional Santa Fe) se realizó una reunión entre el Prefecto Principal, Marcelo Hidalgo, y los presidentes de clubes náuticos de la ciudad capital, Santo Tomé y Coronda, para definir las "zonas de privilegio" para cada una de las disciplinas náuticas. 

 Kuki Angelucci, presidente del Yacht Club Santa Fe (YCSF) indicó sus sensaciones tras el encuentro y las áreas que quedaron delineadas por Prefectura: "Todavía no hemos tomado una posición, lo vamos a evaluar en la Comisión Directiva", y agregó: "Vemos que es un poco restringido, pero si esas son las reglas veremos cómo lo podemos hacer cumplir". 

 En este sentido, Angelucci destacó que todo está sujeto a la altura del río, "si te mandan a navegar a la laguna y no hay agua, es un problema". 

 En el encuentro, además del YCSF, participaron los referentes del Club Náutico Azopardo; el Club Marinas Puerto Santa Fe; Club Regatas; Náutico Sur; Regatas Coronda; Náutico Santo Tomé; y también hubo representantes de la Escuela de Kitesurf.

 **Disciplinas: una por una**

 Para lograr una mejor convivencia y garantizar la seguridad de las personas que practican deportes en los espejos de agua de Santa Fe y alrededores, así quedaron definidas las zonas de privilegio: 

 -Kitesurf: en el espejo de agua de la laguna Setúbal, desde la línea imaginaria perpendicular al mirador del Espigón II, hacia el norte sin límites y a un mínimo de 50 metros del boyado delimitante de la zona de balnearios en temporada estival; y al este hasta la costa contraria, sin límite.

 -Windsurf: desde los pilares del ex puente ferroviario hacia el norte, supeditada esta actividad a condiciones hidrometeorológicas favorables, debiendo arbitrar los medios necesarios para que el traslado hacia la zona asignada, no obstaculice la realización de otra actividad náutica.

 -Stand Up Paddle (SUP): zona comprendida desde el balneario Los Alisos (B° El Pozo) hacia el norte, por el lado este de la Setúbal; y la costa este comprendida entre los pilotes del ex puente ferroviario hasta el Puente Colgante.

 -Natación: en la Setúbal desde los pilotes del ex puente ferroviario hacia el este en la zona del balneario Los Alisos.

 -Remo/Canotaje: canal de Acceso Norte, sobre la margen derecha desde el Puente Colgante hasta el ex muelle YPF; en la Setúbal: sobre la margen izquierda desde los pilares del ex puente ferroviario hasta las inmediaciones Los Alisos a no menos de 50 metros de la costa; y desde los pilares del ex puente ferroviario sobre la margen derecha hasta el Puente Colgante, a no más de 50 metros del paredón de la Costanera Oeste.

 -Optimist, Laser, 29er y embarcaciones similares: en la Setúbal, desde la línea imaginaria perpendicular al mirador del Espigón II, hacia el norte sin límites y a un mínimo de 50 metros del boyado delimitante de la zona de balnearios (en temporada estival), y al este hasta la costa contraria, sin límite.

 -Moto de agua, Esquí acuático, Wakeboard, gomones de arrastre remolcados y veleros: en río Paraná Viejo, desde el km. 584 progresiva al río Paraná hacia el sur; río Colastiné, desde el puente homónimo sobre la Ruta Nacional 168 (km. 590 progresiva al río Paraná), hacia el norte hasta la localidad de Rincón.

 -Zona de fondeo para embarcaciones deportivas: comprendidas entre la margen izquierda de la Setúbal, entre los pilotes del ex puente ferroviario hasta Puente Colgante, teniendo en cuenta un margen de seguridad a una distancia no menor a los 50 metros desde el boyado que delimite la zona balnearia y desde el Paso La Paciencia (km. 585 progresiva del río Paraná) en ambas márgenes hacia el extremo norte de la isla La Paciencia.

 -Prohibición para la práctica de deportes náuticos: las zonas comprendidas entre: el canal de acceso al Puerto de Santa Fe, desde el km 584 hasta la zona de maniobras; aguas del Dique I; y canal de derivación norte desde la zona de maniobras hasta el ex muelle de YPF (lindante al norte del muelle de la empresa Shell-Raizen-).

 