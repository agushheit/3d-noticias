---
category: Estado Real
date: 2020-11-30T15:55:42Z
thumbnail: https://assets.3dnoticias.com.ar/CASAS-RECREO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia informa que se encuentra abierta la inscripción al sorteo de
  19 viviendas en Recreo
title: La provincia informa que se encuentra abierta la inscripción al sorteo de 19
  viviendas en Recreo
entradilla: Hay tiempo para anotarse hasta el 17 de diciembre a las 13 hs. El trámite
  se realiza exclusivamente a través de internet sin intermediarios.

---
El gobierno de la provincia de Santa Fe informa que se encuentra abierta la inscripción para el sorteo de 19 viviendas que se están ejecutando en la ciudad de Recreo. Hay tiempo hasta el 17 de diciembre a las 13 hs.

Las familias interesadas en participar deberán anotarse exclusivamente a través de internet en el Registro Digital de Acceso a la Vivienda. En tanto, quienes ya estén registrados en este sistema deberán volver a ingresar para actualizar sus datos.

Una vez cumplido el plazo de inscripción, la dirección provincial de Vivienda y Urbanismo (DPVyU) conformará y publicará el padrón de familias aptas para participar del sorteo que se realizará el 10 de febrero del 2021.

Para más información, consultar www.santafe.gob.ar o contactarse vía e-mail: registrovivienda@santafe.gob..

  
 

## **REQUISITOS**

Para poder participar del sorteo de viviendas es necesario ser ciudadano argentino o extranjero naturalizado, no ser propietarios de inmuebles y residir o trabajar desde hace al menos dos años en la ciudad de Recreo.

Es obligatorio conformar un grupo familiar en el que al menos dos personas estén unidas en matrimonio o concubinato, ser padre o madre con hijos, hermanos o abuelos con nietos.

Por último, los miembros en su conjunto, deben reunir un mínimo de ingresos mensuales de $47.128 y un máximo de $94.500. Los mismos deben ser demostrables, por lo que se considerarán como válidas salario, monotributo, autónomos, jubilación, pensión y Asignación Universal por Hijo, entre otros subsidios.

##   
   
**LAS VIVIENDAS**

Las nuevas unidades habitacionales, que se encuentran en un estado de construcción avanzado, son de dos dormitorios de las cuales 18 corresponden a la tipología dúplex y la restante estará adaptada para el cupo de personas con discapacidad motriz.