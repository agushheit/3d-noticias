---
category: Agenda Ciudadana
date: 2021-04-26T06:15:22-03:00
thumbnail: https://assets.3dnoticias.com.ar/aviones.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Llegó de China primera partida como parte de un millón de dosis de vacunas
  Sinopharm
title: Llegó de China primera partida como parte de un millón de dosis de vacunas
  Sinopharm
entradilla: Con estas dosis, la Argentina completará un total de 10 millones de vacunas
  recibidas desde el inicio de la pandemia.

---
La primera partida de más de 380 mil dosis de vacunas Sinopharm, de un total de un millón que forman parte de la denominada "Operación Beijing", llegó este domingo al país procedente de China, en tanto se aguarda para este lunes el arribo de otro vuelo de Aerolíneas Argentinas con más 370 mil dosis y el martes la empresa Lufthansa traerá más de 240 mil.

El avión fue recibido por la viceministra de Salud, Sandra Tirado, quien dijo a la prensa que está "muy contenta y satisfecha" con la llegada de esta nueva partida y explicó que "serán distribuidas de inmediato para que se coloque la segunda dosis a los que ya han sido vacunados con la primera".

Destacó que esta partida de 384 mil vacunas "forma parte de un cargamento total que va a estar llegando durante toda la semana hasta lograr el millón de dosis y estamos muy orgullosos en el Ministerio de Salud de la Nación por seguir trabajando para que sigan llegando vacunas. Sabemos que cada una de estas dosis que llegan es esperanza, es vida".

Confirmó que "entre mañana y pasado van a estar llegando las dosis que faltan para completar el millón, mañana en un vuelo de Aerolíneas y pasado mañana a la noche en un avión de Lufthansa".

El vuelo de este domingo integra con los otros dos la denominada "Operación Beijing", con la que el Gobierno trae un millón de dosis del laboratorio Sinopharm y alcanzar así el total de 10 millones de unidades desde el inicio de los operativos.

Así, este lunes otro vuelo de Aerolíneas Argentinas llegará con 371.200 dosis y el martes la empresa Lufthansa traerá 244.800 dosis.

Según la viceministra Tirado, la totalidad de estas vacunas estarán destinadas a completar los esquemas de vacunación ya iniciados.

El Monitor Público de Vacunación, que permite seguir en tiempo real el proceso de vacunación, muestra que hasta este domingo por la mañana se habían distribuido en todo el país 8.701.508 vacunas, de las cuales ya fueron aplicadas 7.134.949: 6.263.849 de ellas corresponden a la primera dosis y 871.100, a la segunda.

Días atrás, el presidente de Aerolíneas Argentinas, Pablo Ceriani, señaló que "Beijing es la tercera ciudad de China que volamos en operaciones de carga".

Hasta este momento Aerolíneas había traído desde China 2.288.000 dosis de la vacuna Sinopharm.

Con el vuelo que llegó este domingo y el que arribará este lunes, Aerolíneas Argentinas estará concretando 16 servicios destinados al transporte de vacunas.

Fueron 12 operaciones a la Federación Rusa, desde donde se trajo la vacuna Sputnik V, del Instituto Gamaleya, y ahora se completarán cuatro viajes a Beijing para transportar la Sinopharm.

También arribaron 1.082.200 dosis de la vacuna AstraZeneca, mediante el mecanismo del Fondo Global de Acceso a las Vacunas contra la Covid-19 (Covax) en dos vuelos regulares de la compañía de los Países Bajos, KLM, y 580.000 dosis de la vacuna Covishield, desarrollada por la India con tecnología de AstraZeneca y la Universidad de Oxford, que llegaron en un vuelo de Qatar Airways.