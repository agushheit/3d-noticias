---
category: Agenda Ciudadana
date: 2021-12-29T06:15:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/penitenciario.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Es ley la emergencia en seguridad pública y en el Servicio Penitenciario
  de la provincia
title: Es ley la emergencia en seguridad pública y en el Servicio Penitenciario de
  la provincia
entradilla: 'La cámara de Diputados dio sanción definitiva a la ley que establece
  herramientas excepcionales en Seguridad por un año y en el Servicio Penitenciario
  por 180 días

'

---
La Cámara de Diputadas y Diputados de Santa Fe dio media sanción este martes al proyecto de ley de la diputada del bloque Socialista Lionella Cattalini sobre “Emergencia en Seguridad Pública”, iniciativa que otorga al Poder Ejecutivo herramientas administrativas para gestionar con mayor operatividad gestiones tales como la incorporación de tecnología y móviles policiales, y el otorgamiento de suplementos salariales para el personal policial y del sistema penitenciario, entre otras. La norma, que tendrá vigencia por un año y se sanciona luego del vencimiento de la prórroga de la anterior el pasado 1º de octubre, prevé también la autorización de partidas para los fines previstos.

El proyecto aprobado también establece que el Servicio Penitenciario vuelve a la órbita del Ministerio de Seguridad.

La legisladora puso especial énfasis en destacar el gesto de confianza de la oposición: "La seguridad es una cuestión de Estado y nos tiene que encontrar a todos comprometidos, velando y trabajando para garantizarla al conjunto de santafesinos y santafesinas”, sostuvo, al tiempo que indicó que la iniciativa aprobada toma en cuenta a otras presentadas por los senadores Alcides Calvo y Rubén Pirola.

“A pesar de las promesas de paz y orden, se perdieron dos años fundamentales en materia de seguridad. Entendemos que el gobierno de Omar Perotti quiere dar vuelta la página y comenzar un nuevo tiempo en el Ministerio de Seguridad; confiamos en este cambio de actitud y vamos a seguirlo muy de cerca", añadió.

Al respecto, Cattalini especificó que “el proyecto aprobado otorga la suma de 3 mil millones de pesos y será el ministro de Seguridad (Jorge Lagna) quien tendrá que presentarse cada seis meses ante la Legislatura para rendir cuentas”. No obstante, advirtió: “Esta ley no alcanza si no hay un plan de seguridad a largo plazo que sea consensuado entre todos los poderes del Estado que, además, vaya acompañado de políticas sociales para los sectores más vulnerables”.

La diputada remarcó que “pese a ser críticos del gobierno por no usar los 3.000 millones de pesos que la Nación envió hace más de un año para atender la seguridad y a que, además, tiene dinero en plazos fijos sin ejecutar, entendemos que es un acto de responsabilidad de nuestra parte construir acuerdos. Desde el socialismo vamos a seguir muy de cerca lo que ocurra con esta Ley de Emergencia; queremos ser parte de la Comisión Bicameral para controlar su aplicación y llevar respuestas a la ciudadanía más rápidamente”.

La media sanción de esta ley contempla más recursos para adquirir nuevos vehículos policiales así como mejorar la situación edilicia de los inmuebles destinados a dependencias de las fuerzas de seguridad, adquirir equipamiento y tecnología para fortalecer el sistema de seguridad pública así como la construcción, refacción y/o modernización de complejos penitenciarios con nuevos sistemas de videovigilancia e inhibidores de celulares. También prevé mejoras en el esquema vigente de los suplementos salariales para atender situaciones excepcionales o de riesgo especial, ampliando las partidas necesarias para hacer frente a los recursos presentados por el personal policial y penitenciario.