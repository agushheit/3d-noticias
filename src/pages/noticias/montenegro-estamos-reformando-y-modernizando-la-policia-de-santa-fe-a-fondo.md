---
category: Estado Real
date: 2020-12-27T12:01:50Z
thumbnail: https://assets.3dnoticias.com.ar/2712-seguridad.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Montenegro: «Estamos reformando y modernizando la policía de Santa Fe a
  fondo»'
title: 'Montenegro: «Estamos reformando y modernizando la policía de Santa Fe a fondo»'
entradilla: El secretario de Seguridad Pública de Santa Fe, realizó un balance del
  primer año de gestión del Ministerio de Seguridad que conduce Marcelo Saín.

---
A un año de gestión en el Ministerio de Seguridad, el Secretario de Seguridad Pública Germán Montenegro realizó un balance de lo realizado y un relevamiento de las políticas necesarias para modernizar y mejorar las estructuras de la Policía de Santa Fe. Al respecto, el funcionario provincial expresó: «Cuando asumimos nos encontramos con una policía que debía ser reformada a fondo y, como consecuencia, a partir de una iniciativa del Ministerio de Seguridad, el gobierno provincial presentó ante el poder Legislativo un paquete de leyes, precisamente tres, con el fin de modernizar institucionalmente el Sistema de Seguridad pública de la provincia de Santa Fe».

Con relación a estas propuestas de modernización, se encuentran la reforma de la Ley del Sistema Policial de la Provincia de Santa Fe y los proyectos de Ley de Seguridad Pública y la Ley de Control del Sistema Policial, que fueron elaborados con la participación activa de las distintas áreas del Ministerio, y un intercambio continuo con la sociedad y sus diferentes actores, a través de foros abiertos a la comunidad. Estos proyectos fueron enviados a ambas cámaras el pasado mes de octubre.

<br/>

**<span style="font-family: helvetica; font-size: 1.25em; font-weight: 600;"> CAPACITACIÓN Y BIENESTAR POLICIAL</span>**

«Estamos ensayando una política orientada a capacitar y perfeccionar profesionalmente al personal, y a darles la mayor cantidad de bienestar posible en términos de sus condiciones laborales. Los proyectos de ley, prevén entre otras cosas, el establecimiento de un sueldo básico para el personal policial, equivalente a dos sueldos básicos y medio, para el inicio de la carrera, con lo cual, subimos la base salarial que hoy tiene el personal, y la idea es ir consolidando un salario digno. Les planteamos una expectativa de carrera que creemos mucho más novedosa, atractiva para que puedan desarrollar su profesión y su vocación, que es lo que finalmente propugnamos», agregó Germán Montenegro.

«Los relevamientos que hicimos a principio de año no solo sirvieron a la transparencia de la gestión, ya que los mismos se encuentran publicados y son de fácil acceso a la ciudadanía, sino que también se realizaron con el fin de generar y propiciar el debate social y político en torno a la modernización institucional, y los cambios del sistema de seguridad e institucional en todos sus aspectos, debate que lleva una deuda de más de 50 años en la provincia de Santa Fe», continuó el funcionario.

<br/>

**<span style="font-family: helvetica; font-size: 1.25em; font-weight: 600;"> CENSO POLICIAL</span>**

A partir de los primeros diagnósticos, el Observatorio de Seguridad Pública, que depende del Ministerio de Seguridad, y la Universidad del Litoral diagramaron el Censo Policial 2020, con el objetivo de establecer precisiones de la cantidad de personal con la que cuenta la estructura policial. Este proceso fue obligatorio para todos los agentes policiales, y por la pandemia por el covid 19, el mismo se realizó de forma online.

Entre sus cometidos, el Censo fue preparado para abordar y profundizar sobre la calidad institucional, además de aportar información válida para la formulación de estrategias con el fin de mejorar las políticas de seguridad pública de la provincia y, conocer en profundidad los aspectos de la vida, situación laboral, profesional, opinión personal de las y los policías sobre la institución.

«Cuando asumimos, nos encontramos con una situación bastante compleja, con una policía de un tamaño muy reducido, con un nivel de capacitación bastante bajo, con problemas de infraestructura y logísticos serios. Una Policía sobredimensionada en su estructura, pero muy limitada en su capacidad operativa y logística y, a partir de ahí pusimos en marcha un proceso tendiente a recuperar esas capacidades logísticas. El gobierno de la provincia financió un plan de recuperación logística para la compra de más de 200 patrulleros, más de 300 motos, la puesta en marcha de procesos licitatorios para adquirir armamento, chalecos antibalas; todo el equipamiento básico que tiene que tener la policía para desempeñar sus tareas», culminó Germán Montenegro.