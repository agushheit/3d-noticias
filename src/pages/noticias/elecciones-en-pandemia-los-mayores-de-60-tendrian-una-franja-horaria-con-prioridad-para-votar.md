---
category: Agenda Ciudadana
date: 2021-07-16T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/SECRELECTORAL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Elecciones en pandemia: los mayores de 60 tendrían una franja horaria con
  prioridad para votar'
title: 'Elecciones en pandemia: los mayores de 60 tendrían una franja horaria con
  prioridad para votar'
entradilla: El secretario Electoral de Santa Fe señaló que la nación evalúa esa posibilidad.
  Advirtió "que ese día se apelará mucho a la responsabilidad ciudadana".

---
En Santa Fe, son casi 1500 para los cargos municipales y comunales que se ponen en juego en septiembre y noviembre. Como lo anticipó UNO, en esta capital se anotaron 43 listas para competir por una de las ocho bancas que se pondrán en juego en el Concejo local.

Pablo Ayala, secretario Electoral de Santa Fe, precisó que son 1466 los expedientes; casi 350 más que el año 2017. Serán unas elecciones particulares, marcadas por los cuidados y protocolos por la pandemia de Coronavirus.

Si bien aclaró que "el horario (de los comicios) no se modificará y será de 8 a 18hs", confirmó que Nación evalúa "disponer de una franja horaria donde los mayores de 60 años tengan prioridad para votar".

En declaraciones a la emisora LT8 de Rosario, comentó que analizan que ese sector de la población tenga la preferencia de ir a votar en las primeras horas de la mañana y sobre el cierre del acto electoral

Indicó que "ese día se apelará mucho a la responsabilidad ciudadana. La persona que tenga algún síntoma ese día o días anteriores, que por favor no asista a votar”, pidió.

El funcionario dio precisiones sobre como continúan los procesos administrativos de cara a las primarias: “A partir del día lunes, el tribunal empezó a cargar toda la información, se procesa y se notifica a los apoderados de los errores como domicilio, edad, cupo, ley de paridad; se van mecanizando esos cambios, y el próximo vencimiento importante lo tendremos el sábado a la noche cuando los apoderados de listas presentan el pedido de oficialización acompañado de los adherentes”, explicó Ayala. La lista que no presenta los adherentes, se cae".

Precisó que "ya recibimos el padrón de electores definitivos, y ahora esperamos que Nación haga la reubicación del padrón en las mesas electorales; cuando tengamos esa información, ya Nación tendrá el sistema de autoconsulta que tendremos alojado en nuestra página; seguramente antes que termine Julio”.