---
category: Agenda Ciudadana
date: 2021-11-02T06:15:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/turistas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Ya rige la nueva normativa para el ingreso de pasajeros desde el exterior
title: Ya rige la nueva normativa para el ingreso de pasajeros desde el exterior
entradilla: Entró en vigencia desde este lunes y establece que los turistas extranjeros
  podrán ingresar al país si poseen el esquema completo de vacunación y la documentación
  que lo acredite.

---
El Ministerio de Transporte comenzó a aplicar desde este lunes la nueva normativa para la llegada de turistas extranjeros, que establece que podrán ingresar al país si poseen el esquema completo de vacunación y la documentación que lo acredite, por lo que los pasajeros que llegaron al aeropuerto de Ezeiza pudieron realizar sus trámites migratorios y aduaneros a un ritmo similar al de la prepandemia de coronavirus.  
  
En tanto, desde la Jefatura de Gabinete anunciaron que "en las próximas horas" entraría en vigencia la normativa que habilita una medida similar para todos los puntos habilitados de ingreso al país.  
  
De acuerdo con un comunicado difundido este lunes, “a partir de ahora se eliminará la realización de la prueba de antígenos en los únicos puntos habilitados de ingreso al país”.  
  
Se advirtió que, “previo a la realización del viaje, los operadores aéreos/fluviales/marítimos deberán verificar la declaración jurada que contempla los requisitos de ingreso que deben cumplir las y los pasajeros”.  
  
En tanto, unos cinco mil pasajeros que llegaron este lunes al aeropuerto Ministro Pistarini volvieron a circular hacia la salida con un régimen similar a los tiempos previos a la aparición de la Covid-19, al haberse levantado las restricciones para el ingreso y ya no tener que realizarse el test de antígeno que se exigía hasta ayer, según indicaron a Télam fuentes aeroportuarias.  
  
"El flujo hoy fue normal y si bien no tenemos aún un registro de la cantidad de pasajeros, hasta el mediodía habían arribado 19 vuelos internacionales, por lo que se estima que entre 4.500 y 5.000 pasajeros pasaron hasta ese momento por los controles migratorios", explicó la fuente.  
  
Los visitantes "no tendrán la necesidad de realizarse un análisis de antígeno al llegar, pero deberán contar con un PCR negativo 72 horas antes en origen", indica la norma difundida por el Ministerio de Transporte.  
  
"Cumpliendo con lo establecido en la decisión administrativa 951 /21 publicada en el Boletín Oficial, desde este lunes 1 de noviembre podrán ingresar al país pasajeros de cualquier parte del mundo", se había informado oficialmente.  
  
De esta manera, "los turistas y/o residentes que cuenten con el esquema completo de vacunación -por lo menos 14 días antes de su ingreso al país- y cumplan las exigencias referidas estarán exceptuados del testeo de antígeno y del aislamiento".  
  
Transporte aclaró también que quienes ingresen al país deberán presentar una declaración jurada electrónica 48 horas antes, incorporando un test de PCR negativo en origen con toma de muestra hasta 72 horas previas, o un certificado médico de alta si hubieren contraído el virus SARS-CoV-2 en los últimos 90 días, y luego se les solicitará un análisis de PCR entre el quinto y séptimo día desde su llegada.  
  
Por otra parte, quienes no cuenten con el esquema completo de vacunación al arribar al país deberán realizarse un test de antígenos y luego un test de PCR negativo para dar por finalizado el aislamiento obligatorio de siete días.  
  
Además, para los turistas menores de 18 años sin esquema completo, el Ministerio de Salud ofrecerá dosis en los puntos de fronteras.  
  
Por otro lado, "los turistas o extranjeros no residentes deberán contar con un seguro con cobertura Covid-19, mientras que los menores de seis años que ingresen al país acompañados de adultos estarán exceptuados de realizar los testeos de antígenos y PCR, pero deberán cumplir obligatoriamente con la cuarentena de siete días".  
  
  
Respecto de las nuevas reglamentaciones, personal del Ministerio de Transporte será el encargado de verificar la información que acredite los esquemas de vacunación y la Policía de Seguridad Aeroportuaria asistirá dichos controles.  
  
En el caso de quienes obtengan resultados positivos de Covid-19, "la norma indica que deberán permanecer aislados por diez días en los dispositivos dispuestos por el Estado nacional en casos de ingreso por el Aeropuerto Internacional de Ezeiza, Aeroparque o San Fernando, y en los demás casos en los dispositivos definidos por las provincias según sus protocolos".  
  
Este lunes por la mañana ya comenzaron a llegar al aeropuerto internacional de Ezeiza los primeros turistas extranjeros que cumplen con esta normativa.  
  
En las casillas de control de migración no se registraron demoras y el flujo fue constante durante la mañana, con un movimiento que se asemejó mucho a la pre pandemia, con la salvedad de que las compañías aéreas recién van a llevar sus frecuencias a un punto bastante más cercano a lo que era antes de las restricciones, para, entre mediados de este mes y diciembre.  
  
En cuanto al sector Sanidad, aún permanece vigente la zona de control de Antígenos, pero en este caso solamente para aquellos pasajeros que no llegan con el esquema completo de vacunación, mientras que también, personal del Ministerio de Transporte está a cargo de los controles antes de salir del aeropuerto para verificar los datos consignados en la Declaración Jurada y la validez del certificado vacunatorio.  
  
Más allá de estos controles, este lunes la salida de los pasajeros, una vez aterrizado el avión, fue fluida y sin mayores demoras en cuanto al retiro de equipajes y el paso por Aduana.  
  
Días atrás, los pasajeros podían demorar entre dos y tres horas desde el momento en que la aeronave tocaba tierra.  
  
Además, se sostenía entre cada vuelo un tiempo de espera para el desembarque y podía llevarles a los pasajeros unas dos horas hasta concretar los tests en el aeropuerto, donde además se tomaban los recaudos de distanciamiento social.  
  
Este lunes arribaron hasta el mediodía, 19 vuelos, uno de Copa Airlines, desde Panamá; dos de Avianca desde Bogotá; uno de Aerolíneas Argentinas desde Miami; otro de Qatar Airlines desde Doha; tres de American Airlines, uno de Nueva York, otro de Dallas y un tercero de Miami.  
  
También arribaron vuelos de Lufthansa desde Frankfurt; de United desde Houston; de Iberia desde Madrid y de Air Europa desde el mismo destino; de Air France desde París; dos de Latam, desde Santiago de Chile y de San Pablo; de Level desde Barcelona y de Boliviana de Aviación procedente de Santa Cruz de la Sierra.