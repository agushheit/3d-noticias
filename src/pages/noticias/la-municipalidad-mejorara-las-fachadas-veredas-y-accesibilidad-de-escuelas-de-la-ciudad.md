---
category: La Ciudad
date: 2021-06-26T06:00:21-03:00
thumbnail: https://assets.3dnoticias.com.ar/escuelas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La Municipalidad mejorará las fachadas, veredas y accesibilidad de escuelas
  de la ciudad
title: La Municipalidad mejorará las fachadas, veredas y accesibilidad de escuelas
  de la ciudad
entradilla: Fue en el marco de una reunión virtual con directivos de escuelas que
  se verán beneficiadas por esta iniciativa. La misma es impulsada por la Municipalidad
  desde el Fondo de Asistencia Educativa.

---
La Municipalidad presentó la iniciativa “Escuelas de mi ciudad”, ante directivos y cooperadores de las primeras instituciones educativas que serán beneficiadas con una serie de mejoras edilicias y de los entornos. El objetivo es realizar la reparación de fachadas y veredas, así como la mejora en los accesos, de alrededor de 40 establecimientos educativos, en una primera etapa.

“Escuelas de mi ciudad” se desarrolla con presupuesto del Fondo de Asistencia Educativa (FAE). En este sentido, es importante destacar que desde el FAE y en conjunto con la Dirección de Obras de Asistencia Educativa (Doae), se viene trabajando en un plan de mantenimiento del césped, limpieza de tanques en 184 escuelas, entre otros trabajos que constituyen un acompañamiento constante a la tarea educativa de los jardines, el Liceo y las Estaciones municipales, que son dispositivos de integración social, cuidado, educación y cultura.

Las primeras intervenciones de este programa, que acompañan las inquietudes de sus directivos, tendrán una inversión de más de $ 23 millones y se realizarán en los predios escolares que comprenden al Jardín Nº 173, CEPA Nº 49, Cecla Nº 20, la Escuela Nº 809, CEF 52, ESSO Nº 512; los mismos están localizados en Mendoza 4369 y Estrada 2290, del barrio Santa Rosa de Lima. En estos espacios habrá también dos nuevas “Veredas para jugar”.

La subsecretaria de Gestión Cultural y Educativa, Huaira Basaber, explicó que, en el marco del FAE, el municipio viene impulsando la realización de un mapeo educativo para verificar las demandas y sistematizar los requerimientos que se hacen desde las instituciones educativas con el objetivo de ordenar y hacer más efectivo el alcance de los pedidos de las escuelas, así como más operativa y ágil la dinámica para resolver las problemáticas.

Asimismo, Basaber aludió a la propuesta que realizó el municipio para transformar el Pasaje Estrada, que actualmente tenía una acumulación importante de residuos, en un espacio para las infancias.

La funcionaria sostuvo que “esta iniciativa es una propuesta más de integración social y un trabajo con las redes institucionales de Santa Rosa de Lima”; y remarcó que “todas las instituciones educativas tienen un proyecto relacionado a la escuela y al barrio”. Y destacó que “siempre fue una indicación del intendente que acompañemos, sostengamos y vehiculicemos los pedidos de las escuelas”.

Este viernes, los encuentros continuarán con la comunidad educativa de la escuela N° 42, de barrio La Esmeralda.