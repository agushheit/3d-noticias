---
category: La Ciudad
date: 2020-12-24T10:54:19Z
thumbnail: https://assets.3dnoticias.com.ar/2412disena0.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'La Diseña: una edición especial junto a los emprendedores y el público'
title: 'La Diseña: una edición especial junto a los emprendedores y el público'
entradilla: El trabajo de las y los diseñadores junto a la Municipalidad, hizo posible
  la esperada edición de Navidad de La Diseña. El público acompañó la propuesta cumpliendo
  los protocolos sanitarios.

---
La Municipalidad, junto a las y los diseñadores, encontró este año la manera de dar continuidad a La Diseña, con una producción cuidadosa de todas las personas involucradas, que implicó adaptar el montaje de la feria y sus dinámicas a las nuevas normas de convivencia que rigen por la pandemia de Covid-19. El encuentro finalizó el 22 de diciembre, tras sus siete jornadas en la Estación Belgrano.

En una valoración del evento, la subsecretaria de Gestión Cultural y Educativa, Huaira Basaber, destacó el cumplimiento del protocolo que elaboró la Municipalidad y presentó ante el Ministerio de Trabajo de la provincia, que garantizó las condiciones de higiene y el distanciamiento físico, necesarios en este contexto. En el acceso por bulevar Gálvez se midió la temperatura a cada persona, se colocó alcohol en gel en las manos, se verificó el uso de tapabocas y el personal controló el ingreso, en función de la afluencia de público.

Los stands de las 156 marcas que participaron se distribuyeron en la nave central de la Estación Belgrano. Dos amplios pasillos, donde los rubros se identificaban claramente, invitaban a recorrer, observar y acercarse a los stands en el momento de consultar un precio o realizar la compra. La circulación se ordenaba en esos corredores; y en la salida por calle Avellaneda se ubicaron los puestos de cerveza artesanal y emprendimientos gastronómicos vinculados a Capital Activa, que de esa manera pudieron sumarse también a la propuesta.

Para los que querían organizar previamente su visita a la feria y anticipar las compras, consultando precios y productos disponibles, la Municipalidad desarrolló una galería virtual a la que se accedía por la plataforma Capital Cultural, con enlaces a las redes sociales y web de cada marca.

***

![](https://assets.3dnoticias.com.ar/2412disena1.webp)

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;"> **Consensos**</span>

La subsecretaria destacó también «el compromiso de las personas que participaron con sus productos para que **La Diseña** se pudiera realizar». Y en particular, la conformación de la Mesa de Diseño Santafesino, donde se consensuaron las características de esta edición tan particular para que resultara beneficiosa dentro de la coyuntura de emergencia; así como también la participación de Capital Activa, de la Secretaría de Educación y Cultura y otras áreas que se involucraron para poder realizar la edición de navidad.

En sintonía con esa manera de llevar adelante la gestión, Basaber valoró la decisión de eximir a las marcas del pago del canon por participar de la feria, como una manera que encontró la Municipalidad de colaborar con el sector en un momento crítico. Desde el inicio de la gestión, recordó: «cuando decidimos denominar a La Diseña de esa manera, porque es como todos la conocemos, fuimos llevando adelante todas las decisiones junto al intendente para que la feria pudiera suceder, para que todos podamos disfrutar, comprar regalos hechos con creatividad, innovación, con mucho amor y atendidos por cada productor y cada productora de estos objetos maravillosos».

***

![](https://assets.3dnoticias.com.ar/2412disena.webp)

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;"> **Experiencias**</span>

María Alejandra Romero Niklison participó por segundo año consecutivo con «Tramando sueños», su emprendimiento de productos realizados en telar y tejidos con las manos. «Pensé que no se iba a dar la posibilidad de estar en la feria este año, pero por suerte se hizo con todos los cuidados. Los clientes me dijeron que el espacio amplio y los controles que se hacían a todos los que ingresaban, les generaron confianza para venir y se sintieron cómodos», contó.

Sin dejar de reconocer que la pandemia afectó mucho la economía del sector, valoró los momentos «en que empezamos a permitirnos pensar otras cosas, otras formas de desarrollar los productos que hacemos, pensando también en los demás». A partir de su compromiso con transmitir lo que sabe y mantener las técnicas de fieltro y telar, desarrolló un kit básico «para niñas, niños o adultos que quieran empezar con las primeras tramas, con un pequeño instructivo, un minitelar para pintar, lanas, naveta y aguja».

Cuando supo que la feria se hacía, Karina Mendoza puso manos a la obra con las distintas líneas de cerámica que trabaja usando «agua de lluvia y barro costero», como cuenta en las redes de Aguaraka. En un año que la llevó a implementar otras estrategias de difusión y modalidades de venta, dijo que «necesitábamos estar en La Diseña porque para muchos este año no hubo otras ferias, así que realmente la esperábamos y superó nuestras expectativas. Estamos felices porque la gente apoyó, tuvo los cuidados que se pedían y respondió bárbaro».

***

![](https://assets.3dnoticias.com.ar/2412disena2.webp)

<br/>

<span style="font-family: helvetica; font-size: 1.35em; font-weight: 800;"> **Llegada y cercanía**</span>

Clara Bertona dijo que en los momentos difíciles de este 2020 fue cuando más se aferró a su emprendimiento Mudra Magia. Implementó como otros la difusión por redes y cuando fue posible hizo entregas a domicilio. A mitad de año, con un grupo de diseñadoras se organizaron en un proyecto colectivo que llamaron «Gérminas Comunidad de Diseño» y hoy cuenta con un local en el Mercado Norte, donde comercializan también productos de unas 25 marcas locales, además de los de Mudra Magia, Ayni, Wakanda Lencería, Libiak Art y Flor Kannemann ilustraciones. «Ahora estamos en **La Diseña** porque nos ayuda mucho estar en este entorno. Es un momento de venta fuerte en el año que se necesita también para mantener nuevos proyectos y encarar lo que viene».

Con un marco de público diferente a la masividad de otras ediciones, previas a la pandemia, Clara valoró que «todos se adaptaron a los protocolos y hemos podido trabajar, tener una llegada y una cercanía con el público».