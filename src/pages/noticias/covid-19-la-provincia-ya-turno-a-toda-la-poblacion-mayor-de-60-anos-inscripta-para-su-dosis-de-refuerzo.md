---
category: Estado Real
date: 2022-01-15T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/mayores60.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Covid-19: La provincia ya turnó a toda la población mayor de 60 años inscripta
  para su dosis de refuerzo'
title: 'Covid-19: La provincia ya turnó a toda la población mayor de 60 años inscripta
  para su dosis de refuerzo'
entradilla: Asimismo se suman nuevos vacunatorios para ampliar la capacidad.

---
Luego de una recorrida por el Sindicato Luz y Fuerza (Paraguay 1135) de la ciudad de Rosario, la ministra de Salud, Sonia Martorano, junto a su par de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, informó que toda la población santafesina mayor de 60 años, inscripta en la web provincial, fue turnada para recibir su vacuna de refuerzo contra el Covid-19.

“Estamos con un alto número de contagios y de circulación del virus. Esto ha hecho que cambiemos los criterios de hisopados y que fuertemente estemos trabajando en la vacunación. En ese contexto podemos decir que todos los mayores de 60 están turnados, de manera tal que este grupo con mayor vulnerabilidad ya se encuentra vacunado con su dosis de refuerzo”, señaló Martorano.

“Hoy tenemos el 83% de la población general con dos dosis y alcanzamos ya el 20% de terceras dosis. Queremos recordar que terceras dosis se están aplicando con turno previo y deberán esperar que les llegue la cita a cualquiera de los vacunatorios”, agregó.

Asimismo, la titular de la cartera sanitaria mencionó que “estamos trabajando en la expansión y la ampliación de la vacuna, convocando a los gremios, a los vacunatorios a que se sumen. Para ello se necesita una logística, por ejemplo para las vacunas Pfizer y Moderna que tienen una logística de frío bastante particular y se necesita una experiencia y un trabajo de vacunatorios capacitados”.

**Nuevos vacunatorios en la provincia**

Desde el Ministerio de Salud notificaron que funcionará un nuevo vacunatorio en la ciudad de Rosario. El mismo estará ubicado en la sede del Sindicato Luz y Fuerza (Paraguay 1135) y estará destinado para la inoculación de población en general.

“Hoy estamos vacunando a todo el personal considerado estratégico, especialmente en estos momentos, como son los empleados de la EPE, de aguas y de seguridad, con terceras dosis y para poder continuar con la comunidad y priorizar estos grupos es que necesitamos lo que estamos haciendo hoy, abrir nuevos vacunatorios”, indicó Martorano.

“Es por ello que agradecemos a Luz y Fuerza, que tiene una amplia trayectoria trabajando en conjunto con la provincia en todas las campañas de inoculación, y de esta manera se suma tanto para sus afiliados como para toda la comunidad la vacunación para terceras dosis”, continuó.

Asimismo, la titular de la cartera sanitaria informó que “en la ciudad de Santa Fe, el próximo lunes 17 de enero se abrirá un vacunatorio con las mismas características en las instalaciones de la Obra Social de los Empleados de Comercio y Actividades Civiles (OSECAC), mientras continuamos con tratativas para abrir otros más”.

**Testeos**  
Al ser consultada por las nuevas estrategias de testeos en la población, la ministra de Salud explicó que “hoy aquella persona que tiene que hacer un aislamiento, lo puede cargar a través de la App Cuidar, que inmediatamente se bloquea si es un paciente sintomático, si es positivo o si tiene un hisopado. Además, las obras sociales lo están cubriendo cuando tienen las indicaciones pertinentes, es decir aquellas personas mayores de 60 con factores de riesgo, personas gestantes, personal de salud”.

Además, la titular de la cartera sanitaria agregó que “las personas asintomáticas no hace falta hisoparse, si tuviste Covid en los últimos 90 días tampoco hay que hisoparse. Insistimos en que los contactos estrechos asintomáticos con vacunación completa o con dos dosis y la segunda colocada en menos de 5 meses o con dos dosis y Covid en los últimos 90 días, no se indica aislamiento. Si se les indica extremar los cuidados”, continuó.

“Seamos muy claros, quien esté cursando un Covid o tenga el alta de Covid debe esperar 90 días para el refuerzo. Porque se ha demostrado que deja anticuerpos y con la vacuna se genera, por decir, una competencia entre los anticuerpos de la vacuna y los que generó el Covid. De manera tal que si cuentan con dos dosis, funciona como si fuese un refuerzo. Si me llegó el turno lo guardo porque cuando pasen los 90 días me presento y me vacuno”, enfatizó la ministra.

Por último, Martorano hizo mención a la inversión en ambulancias que se realizó durante su gestión: “Hemos incorporado en la provincia más de 100 ambulancias, hay otra compra encaminada pero puntualmente en Rosario y zona de influencia tenemos muchas ambulancias nuevas”.

“Ayer nos comunicamos con el Sies para evaluar la problemática y hoy, desde la provincia se están colocando los vehículos que hacen falta. Esto tiene que ver con la articulación que tenemos entre el sistema público-privado como entre provincia y municipios por lo que hoy se está solucionando”, concluyó la ministra de Salud.

**Restricciones**  
Finalmente, Pusineri explicó que “las medidas que adoptamos tienen que ver con lo que señalaba la ministra colocado centralmente en la vacunación, que es lo que nos permitirá salir de esta situación, por eso también volvemos a marcar el agradecimiento a la gente que ha montado un vacunatorio abierto a la comunidad tanto en Rosario como en Santa Fe”.

“El resto de las medidas son las que se conocieron a fin de año. Aforos al 70%, pase sanitario para las actividades que ya conocen y hoy desaconsejar la realización de eventos masivos. Las instancias de contagio se dan en la socialización, está comprobado y todas las autoridades sanitarias y la ministra de salud lo viene remarcando en los encuentros, entonces vamos a tener una política por la cual se desaconseje la realización de eventos masivos. Sobre las medidas anunciadas a fin de año no habrá modificaciones en lo inmediato”, agregó.

Por otra parte, Pusineri resaltó que “los datos del ausentismo laboral derivado de contactos estrechos vienen siendo datos dinámicos, lógicamente tenían un impacto hace 15 días y hoy es otro, estamos estimando que en la totalidad de las actividades, me refiero a industria, construcción, comercio o servicio, el ausentismo por contagios y por contactos estrechos ronda el 15 y el 20 % por eso, para no afectar la producción pero, centralmente no afectar servicios esenciales como Salud, fuerzas de seguridad, servicios penitenciarios, provisión de agua y energía, es que se dispusieron las normas relativas al aislamiento”

“Es decir, lo que marcaba recién la ministra de salud, normas para que no se aíslen contactos estrechos asintomáticos con el esquema de vacunación completo que van a trabajar con ciertas garantías respecto de seguridad y con una evaluación diaria de su situación”, concluyó el funcionario.

De la actividad participaron además, la jefa de la Policía de Santa Fe, Emilce Chimenti, el presidente de Aguas Santafesinas, Hugo Morzán y el secretario general de Luz y Fuerza, Alberto Botto.