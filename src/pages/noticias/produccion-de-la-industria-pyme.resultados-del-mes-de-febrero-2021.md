---
category: Agenda Ciudadana
date: 2021-03-23T07:07:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/industria.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa CAME
resumen: Producción de la industria pyme. Resultados del mes de febrero 2021
title: Producción de la industria pyme. Resultados del mes de febrero 2021
entradilla: La producción de la industria pyme creció por tercer mes consecutivo en
  febrero al ubicarse 0,4% arriba del mismo mes de 2020.

---
\- La producción de la industria pyme creció por tercer mes consecutivo en febrero al ubicarse 0,4% arriba del mismo mes de 2020. La recuperación manufacturera en ese segmento de empresas sigue siendo muy lenta y traccionada por un tercio de las industrias, que son las que están progresando.

\- Con esos datos, el **Índice de Producción Industrial Pymes** alcanzó un valor de 76,3 puntos en febrero frente a un valor de 76 en el mismo mes del año pasado.

\- Los datos surgen del promedio de las tasas de variación reportadas por 300 industrias pymes del país en la **Encuesta Mensual Industrial** de **CAME**.

\- De los 11 grandes sectores relevados, 5 tuvieron aumentos anuales y 6 finalizaron con bajas.

\- Un fabricante de artículos electrónicos de Mar del Plata que registró una caída cercana al 50% anual en su producción, expresaba: “**_estamos teniendo demoras de 3 meses en la entrega de insumos y nos complica cumplir con lo pautado con nuestros clientes”._**

\- El rubro de mayor ascenso anual fue **Productos químicos,** con un aumento de 13,8%. La intensificación de los pedidos en algunas cadenas de valor, como la construcción, que son abastecidas por esta industria explica buena parte de ese incremento. También las ventas externas de ese ramo vienen escalando, luego de haber bajado en todo 2020, y eso contribuyó a la recuperación. Especialmente la mayor demanda externa se sintió en aceites esenciales y resinoides; productos de perfumería, de tocador o cosmética y productos farmacéuticos.

\- Diferente fue el caso para los sectores **Maderas y muebles** y **Papel, cartón, edición e Impresión,** donde la producción descendió 5,4% y 3,9% anual respectivamente. Donde más se sintió el declive en la demanda fue en **Edición** y **muebles**. En cambio, **papel** y **cartón** tuvieron un mejor mes, especialmente para aquellas empresas vinculadas al ramo agropecuario, y lo mismo en **Maderas** y productos de maderas sin incluir **muebles.**

![](https://www.redcame.org.ar/advf/imagenes/2021/03/6057497805f89_706x243.png)

\- En el rubro **Alimentos y bebidas** la producción subió 2,5% anual, y operó con mayor normalidad que otros, sin los problemas de abastecimiento que mostraron algunos rubros. Las mayores dificultades de las firmas de ese sector fueron financieros. La vuelta de clases favoreció algunos ramos como los vinculado a panificados.

\- En el bloque **Minerales no metálicos** (+2,5% anual), el rubro que más creció fue el relacionado a materiales para la construcción y donde más complicaciones se encontraron fue en la producción de artículos de vidrios, aunque de todos modos la fabricación de estos se mantuvo sin cambio frente al año pasado. Según los industriales consultados, la actividad se hubiera movido con más fuerza si no fuera por los contratiempos para abastecerse de insumos, especialmente de los importados.

\- El sector **Indumentaria y Textil** es uno de los que todavía no puede consolidar su recuperación. Solo el 29% de las empresas relevadas de este sector tuvieron mejoras en la comparación anual. Un conflicto que vienen teniendo los fabricantes de indumentaria, es el surgimiento de una gran cantidad de mini emprendedores que comercializan por redes, generando más jugadores en un mercado que todavía percibe los dilemas de ingresos familiares.

![](https://www.redcame.org.ar/advf/imagenes/2021/03/6057498422e69_706x316.png)

\- Las industrias trabajaron en febrero con una capacidad instalada de 67,2%, 1 punto arriba de febrero 2020.

![](https://www.redcame.org.ar/advf/imagenes/2021/03/6057498425411_706x245.png)

**Encuesta cualitativa: mejora la rentabilidad**

\- En febrero, el 47,9% de las industrias pymes consultadas tuvieron rentabilidad positiva, levemente por encima de enero (47%). El 31,5% de las firmas tuvo rentabilidad nula y 11,9%, negativa (en enero el 17% de las pymes relevadas tuvieron rentabilidad negativa).

![](https://www.redcame.org.ar/advf/imagenes/2021/03/6057498426bab_706x242.png)

**Notas metodológicas**

Consideraciones generales:

El Índice de Producción Industrial PYME (IPIP) mide el desempeño mensual en la producción manufacturera de las pequeñas y medianas industrias (PYMIS) argentinas. La información se obtiene durante los primeros 20 días del mes en base a encuestas directas realizadas entre 300 pymes industriales del país. Releva todos los meses un equipo de 30 encuestadores localizados en las ciudades capitales, Ciudad de Buenos Aires y Gran Buenos Aires (Zona sur, zona norte y zona oeste). A su vez, un equipo de 6 supervisores desde CAME controlan la calidad de la información recolectada y coordinan el equipo de encuestadores.

Las empresas relevadas han sido seleccionadas en función de tres variables:

**1.** Tipo de producto elaborado por la empresa: se determinaron productos que reflejen de manera más fehaciente el nivel de actividad del sector, ya sea en forma directa como indicador representativo de la producción, o en forma indirecta por constituir el insumo principal de otro sector.

**2.** Localización geográfica de la empresa: se seleccionaron firmas localizadas en regiones con predominancia en la elaboración de los productos preseleccionados.

**3.** Calidad de la información: se relevan empresas donde pudo comprobarse la calidad y precisión de los datos aportados.

**Consideraciones particulares:**

El IPIP está dividido en 11 sub ramas industriales. Para determinar el valor del IPIP se elaboran números índices de cada una de esos rubros y del nivel general que reflejan la evolución de la producción industrial en términos de volumen físico. Los índices resultan útiles para homogeneizar a las variables, facilitando su comparación a lo largo del tiempo. El año base de la serie se estableció en diciembre de 2008.

La ponderación de cada una de las ramas industriales se realizó en base al Censo Económico 2004 y al Mapa PYME elaborado por la Sepyme. La consideración asignada a cada sector se puede leer en la siguiente tabla:

![](https://www.redcame.org.ar/advf/imagenes/2021/03/60574984282d1_706x461.png)