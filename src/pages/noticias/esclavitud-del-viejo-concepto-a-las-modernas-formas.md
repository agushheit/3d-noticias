---
category: Agenda Ciudadana
date: 2022-12-02T16:49:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/freedom-2048417_1920.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Ricardo Miguel Fessia
resumen: 'ESCLAVITUD: DEL VIEJO CONCEPTO A LAS MODERNAS FORMAS '
title: 'ESCLAVITUD: DEL VIEJO CONCEPTO A LAS MODERNAS FORMAS '
entradilla: Hoy 2 de diciembre se conmemora a nivel mundial el Día Internacional para
  la Abolición de la Esclavitud.

---
I – Existen muchas formas de definir la esclavitud, pero a estos efectos diremos que es la idea de someter a una persona –humana- a la condición de bien o propiedad de otra. De esta forma, ese otro ser, que sin dudas siempre lo fue, no se les reconocen ni derechos y mucho menos garantías que desde siempre han existido. En definitiva, es un status social garantizado por el ordenamiento jurídico por el cual se podía disponer: utilizar, vender o ceder y hasta matar.

Debieron pasar centurias para que la eliminación de esta situación de esclavitud se incorpore en los estatutos normativos de los diversos países en distintos momentos y fue prolíficamente abordado a nivel global durante los siglos XIX y XX.

En nuestro país desde la Asamblea General Constituyente de 1813 se decretó primero la denominada “libertad de vientres” y unos pocos días después la libertad de todo/as los/as esclavos/as que ingresaran a lo que entonces se llamaba Provincias Unidas del Río de la Plata. Esto fue plasmado posteriormente en la Constitución Nacional de 1853, a través de un artículo que se conserva hasta el día de hoy: _“En la Nación Argentina no hay esclavos: los pocos que hoy existen quedan libres desde la jura de esta Constitución; y una ley especial reglará las indemnizaciones a que dé lugar esta declaración. Todo contrato de compra y venta de personas es un crimen de que serán responsables los que lo celebrasen, y el escribano o funcionario que lo autorice. Y los esclavos que de cualquier modo se introduzcan quedan libres por el solo hecho de pisar el territorio de la República”_ (artículo 15 de la CN).

 La esclavitud es considerada una vulneración a los Derechos Humanos porque supone la negación de todos los atributos que se asocian a una persona. Es por ello que el derecho internacional lo ha tipificado con la categoría de crimen contra la humanidad. En definitiva, se enmarca en una relación de profunda asimetría de poder, en la que una de las partes es propietaria de la otra, afectando profundamente su libertad, voluntad y dignidad.

II - La Asamblea General de Naciones Unidas aprobó el “Convenio para la Represión de la Trata de Personas y de la Explotación de la Prostitución Ajena” (resolución A/RES/317IV) el día 2 de diciembre de 1949. Atendiendo a ello es que ese día se conmemora a nivel mundial el Día Internacional para la Abolición de la Esclavitud. Se trata de una fecha que nos interpela a la reflexión por la realidad, por las condiciones actuales que conservan rasgos de sometimiento y de vulneración de libertades esenciales y derechos y que, por lo tanto, constituyen un delito que el Estado debe erradicar.

La esclavitud moderna se utiliza como un término general que abarca prácticas como el trabajo forzoso y el matrimonio forzado. Con ella, se hace referencia a situaciones de explotación en las que una persona no puede rechazar o abandonar debido a amenazas, violencia, coerción, engaño o abuso de poder.

La Organización Internacional del Trabajo (OIT), realizó una investigación titulada “Estimación mundial sobre el trabajo infantil: Resultados y tendencias, 2012-2016”, en donde revela que 152 millones de niños, entre 5 y 17 años, se encuentran en situación de trabajo infantil, de los cuales, 73 millones realizan trabajos peligrosos.

El mayor número de niños entre 5 y 17 años víctimas del trabajo infantil se encuentra en África (72,1 millones), seguida por Asia y el Pacífico (62 millones), las Américas (10,7 millones), Europa y Asia Central (5,5 millones) y los Estados Árabes (1,2 millones).

De igual forma la Organización Internacional del Trabajo (OIT), y la Walk Free Foundation, en colaboración con la Organización Mundial para las Migraciones (OIM), elaboraron un informe llamado “Estimación mundial sobre la esclavitud moderna: Trabajo forzoso y matrimonio forzado”, y revela que 40,3 millones de personas fueron víctimas de esclavitud moderna en 2016. De ese total, 24,9 millones de personas estaban sometidas a trabajo forzoso y 15,4 millones de personas vivían en un matrimonio forzoso al cual no habían prestado su consentimiento.

De acuerdo a esto, los Objetivos de Desarrollo Sostenible, y en particular el objetivo 8, el cual promueve el crecimiento económico sostenido, inclusivo y sostenible, el empleo pleno y productivo y el trabajo decente para todos, no podrán ser alcanzados a menos que se intensifiquen en forma drástica los esfuerzos para eliminar la esclavitud.

En específico con la meta 8.7 _“…Adoptar medidas inmediatas y eficaces para erradicar el trabajo forzoso, poner fin a las formas contemporáneas de esclavitud y la trata de personas y asegurar la prohibición y eliminación de las peores formas de trabajo infantil, incluidos el reclutamiento y la utilización de niños soldados, y, de aquí a 2025, poner fin al trabajo infantil en todas sus formas.”_

III - No obstante que esta jurídicamente penalizada en la mayoría de los países del mundo, en la actualidad podríamos decir que la esclavitud lamentablemente se encuentra vigente de formas variadas, con más o menos disimulo, tal como lo son la trata y la explotación de personas en sus diferentes modalidades.

La trata de personas, el trabajo forzoso y el matrimonio forzado son delitos que pueden ser considerados como formas modernas de esclavitud, que implican graves violaciones a los derechos fundamentales: vulnera la libertad, la dignidad, la integridad (física y psíquica) y la autodeterminación de las personas, es decir, la posibilidad de construir un proyecto de vida.

El delito de trata comprende distintas acciones y fines. Las acciones que lo componen son la captación, el traslado y la acogida, y su fin es la explotación, cuyas modalidades más frecuentes son la explotación sexual y la laboral. Si bien es una problemática que puede afectar a todas las personas, lo hace especialmente a mujeres, niñas y personas LGBTI+. En la trata de personas con fines de explotación sexual, el 96% de las víctimas son mujeres, niñas y LGBTI+, y en la trata laboral, al menos un 30%. Como puede observarse, la trata de personas tiene la finalidad de obtener un rédito económico a través de la cosificación de seres humanos, que son considerados como simple mercancía, afectando gravemente su libertad y dignidad.

Las formas modernas de esclavizar sólo pueden comprenderse desde las prácticas sociales imperantes en la segunda mitad del siglo XXI. De esta manera, cuando decimos que la trata y explotación de personas son delitos esencialmente contra la libertad, no nos referimos meramente a su dimensión ambulatoria (como movilidad del cuerpo), sino a todas las dimensiones que hacen a la capacidad de construcción de un proyecto de vida. Es por eso que en la actualidad el sometimiento se produce y reproduce a través de mecanismos más sutiles (aunque no por ello menos violentos), tales como sujeción económica a través de sistemas de endeudamiento, la retención de documentación personal, el ejercicio de violencia o amenazas, entre otras. No se limita al ejercicio de relaciones de propiedad en sentido estricto y clásico, pero sí se enmarca en un sometimiento y control que vulnera profundamente su autonomía y dignidad.

El Estado, como garante de los derechos de todas las personas, debe abordar esta problemática de manera integral, en pos de prevenir el delito, sancionar a quienes lo cometen y asistir a las víctimas. Para ello, debe tener en cuenta las múltiples vulnerabilidades de las personas que podrían verse afectadas —género, edad, condición migratoria, situación de pobreza, entre otras— y cómo estas afectan a cada persona. Debe intervenir también trabajando coordinadamente desde los distintos poderes y sectores del Estado, en todo el territorio respetando las características de cada región, y en pos de garantizar la restitución de los derechos de las víctimas sin discriminación.

IV - La legislación de nuestro país, en consonancia con lo antedicho, entiende a la trata de personas y otras formas de esclavitud moderna como formas de violencia contra las mujeres, según se establece en la Ley 26.485, de Protección Integral para Prevenir, Sancionar y Erradicar la Violencia contra las Mujeres en los Ámbitos en que Desarrollen sus Relaciones Interpersonales. Además, la Ley 26.842, de “Prevención y Sanción de la Trata de Personas y Asistencia a sus Víctimas”, establece la obligación de prevenir esta compleja problemática, asistencia para las personas damnificadas y penas para los responsables. Se ha dado forma por este texto legal al “Comité Ejecutivo para la Lucha contra la Trata y Explotación de Personas y para la Protección y Asistencia a las Víctimas” articula las acciones de prevención, sanción y asistencia en materia de Trata de personas”.

El marco normativo y la estructura estatal no garantiza que esos hechos no ocurran. Requieren de un compromiso de los hombres y mujeres para estar siempre alertas y optar por acciones positivas para desarrolladas por el Ministerio de Justicia y Derechos Humanos, el Ministerio de Seguridad, el Ministerio de Desarrollo Social, el Ministerio de Trabajo, Empleo y Seguridad Social, el Ministerio de Seguridad, el Ministerio de Educación y el Ministerio de Salud.

V - De igual forma esta celebración, debe hacernos reflexionar en estos días del campeonato de fútbol en un país en donde hay serias denuncia de la cantidad de trabajadores en condiciones similares a la esclavitud y en donde las mujeres por su condición de tales se les limitan derechos fundamentales, existen diferencias de género e incluso toda la población tiene limitados sus derechos absolutamente esenciales.

Santa Fe, 2 de diciembre de 2022.