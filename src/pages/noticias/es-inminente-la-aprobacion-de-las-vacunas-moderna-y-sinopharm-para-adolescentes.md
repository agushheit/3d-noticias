---
category: Agenda Ciudadana
date: 2021-07-15T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/GOLLAN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Es "inminente" la aprobación de las vacunas Moderna y Sinopharm para adolescentes
title: Es "inminente" la aprobación de las vacunas Moderna y Sinopharm para adolescentes
entradilla: El ministro de Salud de la provincia de Buenos Aires precisó que podrán
  utilizarse esas dosis, junto a Pfizer y Sputnik, para inmunizar a la población de
  3 a 18 años una vez que la Anmat haya dado su autorización.

---
El ministro de Salud de la provincia de Buenos Aires, Daniel Gollan, dijo que es "inminente" la aprobación en el país de las vacunas Moderna y Sinopharm para inmunizar contra el coronavirus a menores de 18 años; en tanto Rusia anunció que en agosto comenzará a inocular a chicos entre 12 y 17 años con la Sputnik V, fármaco que se utiliza y produce en la Argentina.

"La FDA por estos días estaría aprobando (el uso de Moderna en menores) y después la aprobación de la Anmat es automática", dijo Gollan sobre la autorización que analiza la agencia de alimentos y medicamentos de los Estados Unidos a esa vacuna para ser aplicada en niños y adolescentes.

Por otro lado, apuntó que "es muy, muy inminente la aprobación de Sinopharm (para niños y adolescentes) de 3 a 18 años, con lo cual va a haber disponibilidad" de vacunas para este grupo poblacional tras la autorización de la Anmat.

El 11 de junio pasado China aprobó el uso de emergencia de las vacunas contra la Covid-19 de Sinopharm y de Sinovac Biotech (ambas utilizan la plataforma de virus inactivado) en personas de 3 a 17 años.

En tanto, en la Unión Europea (UE), en Estados Unidos y Canadá ya fue autorizado el uso de Pfizer en menores.

"Vacunas para los chicos con comorbilidades va a haber muchas", aseguró Gollan en Radio La Red, y precisó que estima que podrán utilizarse en el país las de Sputnik, Sinopharm, Moderna y Pfizer para ser aplicadas en menores cuando la Anmat haya dado su autorización y en la medida que avancen las negociaciones con esos laboratorios.

El ministro aseguró que se calcula que en la provincia de Buenos Aires hay entre "300 y 400 mil chicos" con comorbilidades y que, una vez que las vacunas estén aprobadas, su aplicación podría concretarse en "uno o dos días".

**Rusia vacuna adolescentes**

Por otro lado, el director del Centro Nacional Gamaleya, Alexander Gintsburg, anunció que el mes que viene Rusia comenzará a vacunar contra el coronavirus a chicos de entre 12 y 17 años con la Sputnik V.

En declaraciones a la agencia de noticias rusa Ria Novosti, explicó que la vacunación empezará cuando terminen las pruebas que ya se realizan sobre voluntarios de ese rango etario.

De los 100 participantes del estudio, 21 chicos y chicas recibieron el primer componente de la fórmula y sólo dos "levantaron una fiebre de más de 37 grados", dijo Gintsburg.

"Esta es una reacción normal, lo que significa que no se inyecta solución salina, sino la vacuna. Si no la hubieran medido, probablemente no se habrían dado cuenta", dijo Gintsburg sobre el progreso de las pruebas.

"La vacunación contra el coronavirus de los adolescentes debe comenzar antes del 20 de septiembre", estimó.

El ensayo comenzó el 5 de julio en el hospital infantil ZA Bashlyaeva y el hospital infantil Morozov de Moscú, explicó.

Los voluntarios se sometieron a pruebas de PCR para confirmar que no estuvieran cursando la enfermedad y luego se les aplicó una dosis menor a la Sputnik V que se utiliza para adultos.

**A nivel mundial**

La vacunación contra el coronavirus en niñas, niños y adolescentes avanza a nivel mundial como estrategia para mejorar la inmunidad de rebaño o para proteger a aquellos menores de 18 años con comorbilidades, pero actualmente sólo se encuentra aprobado el inmunizador de Pfizer para adolescentes y los de Sinovac y Sinopharm a partir de los 3 años, aunque sólo en China.

"La mayoría de los niños desarrolla cuadros leves de Covid-19, pero hay una muy baja proporción de población pediátrica que puede complicarse (un 1 o 2%), y en su mayoría se trata de niñas, niños o adolescentes que tienen alguna comorbilidad", dijo a Télam la infectóloga pediátrica Analía De Cristófano.

De Cristófano, integrante de la Sociedad Argentina de Infectología (SADI), explicó que "estas comorbilidades son parecidas a las que constituyen factores de riesgo en los adultos", tales como "inmunosupresión, obesidad, cuadros respiratorios crónicos o trastornos neurológicos que hacen que no puedan seguir las medidas de cuidado general".

"Con lo cual, si se pueden vacunar, es una población que va a mejorar su pronóstico y disminuir la posibilidad de tener casos severos. Por otro lado, está el tema de la inmunidad de rebaño, y en este sentido vacunar a los niños ayudaría a disminuir la circulación viral", indicó.

Por su parte, el médico pediatra e infectólogo y coordinador de la Comisión de Vacunas de la SADI, Ricardo Rüttimann, explicó que las vacunas pueden tener diferentes efectos adversos en los niños, por lo que "no podemos traspolar la información de la seguridad en adultos a la población pediátrica".

En el mismo sentido, De Cristófano sostuvo que "los niños son población sensible, por lo que es muy importante que los estudios estén hechos y aprobados", pero recordó que "las vacunas de virus inactivado (como Sinovac o Sinopharm) están muy probadas y con las ARN (Pfizer o Moderna) se está acumulando mucha experiencia este último tiempo".

**Las vacunas autorizadas**

La vacuna de Pfizer fue autorizada para uso de emergencia (AUE) a partir de los 16 años en diferentes países, pero el 5 de mayo las autoridades sanitarias de Canadá ampliaron esa autorización para la población de 12 a 15 años, convirtiéndose en el primer país en el mundo en aprobar la inoculación a adolescentes.

Cinco días después hizo lo mismo Estados Unidos tras recibir la aprobación de parte de la Administración de Medicamentos y Alimentos (FDA); y dos semanas después hizo lo propio la Agencia Europea de Medicamentos (EMA) para la franja etaria entre 12 y 17, lo que es válido para sus 27 estados miembro.

Por su parte, el 11 de junio pasado China aprobó el uso de emergencia de las vacunas contra la Covid-19 de Sinopharm y de Sinovac Biotech (ambas utilizan la plataforma de virus inactivado) en personas de 3 a 17 años.

Actualmente, Pfizer lleva adelante un estudio que evalúa la seguridad y eficacia de su vacuna en niños de seis meses a 11 años; también la vacuna de la farmacéutica estadounidense Moderna realiza un ensayo clínico fase 2/3 en niños de la misma edad, mientras completa otro en adolescentes.

Por su parte, Janssen está desarrollando un estudio de su vacuna en adolescentes (entre 12 y 17 años) y AstraZeneca también hace pruebas para la franja de 6 a 17 años, el mismo grupo etario del ensayo que está realizado CanSino Biologics (China).