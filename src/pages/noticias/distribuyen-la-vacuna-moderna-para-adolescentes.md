---
category: Agenda Ciudadana
date: 2021-07-29T06:15:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/MODERNA.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Distribuyen la vacuna Moderna para adolescentes
title: Distribuyen la vacuna Moderna para adolescentes
entradilla: Las 901.040 vacunas destinadas a adolescentes con factores de riesgo están
  siendo distribuidas entre las provincias de acuerdo con la población de cada distrito.

---
El Gobierno comenzó a distribuir en todas las jurisdicciones del país más de 900 mil dosis de la vacuna Moderna, destinadas a adolescentes con factores de riesgo, del total de 3.500.000 que fueron donadas a la Argentina por Estados Unidos, informaron fuentes oficiales.

De esta manera se inicia el esquema de inoculaciones contra el coronavirus de la franja poblacional de adolescentes 12 a 17 años con condiciones priorizadas, en el marco del Plan Estratégico de Vacunación.

La distribución

Las 901.040 vacunas serán distribuidas desde este miércoles entre las provincias de acuerdo con la población de cada distrito, en sintonía con los establecido en el programa nacional de inmunización.

Del total, a la provincia de Buenos Aires le corresponderán 347.760; a la ciudad de Buenos Aires, 60.480; a Catamarca, 8.400; a Chaco, 23.520; a Chubut, 12.180; a Córdoba, 74.760; a Corrientes, 21.840; a Entre Ríos, 27.300; a Formosa, 11.760, y a Jujuy, 15.120.

En tanto, se destinarán a La Pampa, 7.140 dosis; a La Rioja, 8.400; a Mendoza, 39.480; a Misiones, 25.200; a Neuquén, 13.440; a Río Negro, 15.120; a Salta, 28.560; a San Juan, 15.540; a San Luis, 10.080; a Santa Cruz, 7.140; a Santa Fe, 70.280; a Santiago del Estero, 20.160; a Tierra del Fuego, 3.640, y a Tucumán, 33.740.

La ministra de Salud, Carla Vizzotti, destacó que "hay que valorar el logro de la Argentina de estar demorando el ingreso de la variante Delta" de coronavirus y remarcó que el país se encuentra "dentro de los 20 que más vacunas recibieron".

"Los países que tienen circulación predominante de variante Delta con cobertura de vacunación alta y con esquemas completos, tienen un aumento del número de casos, pero ello no se traduce en internaciones ni en muertes. El Reino Unido nunca superó los 100 muertos por día cuando antes tenían 2.000 y eso es por la vacuna", explicó la ministra.

Además, al referirse a la vacunación contra la Covid-19 para adolescentes de entre 12 y 17 años, señaló que "estamos planificando que las jurisdicciones arranquen con la mayoría de los registros de adolescentes priorizados y el lunes que viene, en el Consejo Federal de Salud, hacer el lanzamiento", explicó.

Posteriormente, contó que se trabaja junto al Ministerio del Interior y las provincias de cara a las elecciones PASO del 12 de septiembre, mediante la elaboración de protocolos y en base a experiencias de otros países.

En tanto, Pfizer anunció que una tercera dosis de su vacuna contra el coronavirus administrada al menos 6 meses después de la segunda dosis "provoca títulos neutralizantes contra la variante Delta (B.1.617.2), que es más de cinco veces mayor en personas más jóvenes y más de 11 veces mayor en personas mayores que después de dos dosis".

En un comunicado, Pfizer y BioNTech brindaron una actualización sobre la prueba de refuerzo de fase 1/2/3 en curso de una tercera dosis de la vacuna BNT162b2 actual.

En el comunicado dado a conocer hoy, las empresas aseguran que "esperan publicar datos más definitivos" en las próximas semanas y que toda la información se compartirá con la FDA, EMA y otras autoridades reguladoras.

Este mes Pfizer y BioNTech iniciaron la parte global de la Fase 3 del estudio clínico para evaluar la eficacia, seguridad y tolerabilidad de una tercera dosis de BNT162b2 en comparación con placebo, incluida la evaluación de la eficacia del refuerzo para continuar previniendo la infección por Covid-19.

Las empresas también señalaron que en junio iniciaron un estudio de Fase 2/3 para evaluar más a fondo la seguridad, tolerabilidad e inmunogenicidad de BNT162b2 en la prevención de Covid-19 en niños sanos entre las edades de 6 meses a 11 años y que esperan tener los datos que respaldarían una autorización de uso de emergencia a finales de septiembre.

El martes pasado, Vizzotti, anunció que firmó un "acuerdo vinculante" con el laboratorio Pfizer para la provisión de 20 millones de dosis de vacunas contra el coronavirus, que llegarían al país durante este año.

BioNTech y Pfizer prevén producir 3.000 millones de dosis hasta fin de año, según el reporte difundido este miércoles.

¿A quiénes estarán destinadas?

Las vacunas tendrán como fin "iniciar el esquema de inoculaciones de la franja poblacional de 12 a 17 años con condiciones priorizadas", se informó.

Entre las condiciones incluidas en el grupo objetivo se encuentran:

• Diabetes tipo 1 o 2.

• Obesidad grado 2 (IMC > 35) y grado 3 (IMC > 40).

• Enfermedad cardiovascular crónica: Insuficiencia cardíaca, enfermedad coronaria, valvulopatías, miocardiopatías, hipertensión pulmonar, cardiopatías congénitas.

• Enfermedad renal crónica (incluidos pacientes en diálisis crónica y trasplantes) y síndrome nefrótico.

• Enfermedad respiratoria crónica: Enfermedad pulmonar obstructiva crónica (EPOC), fibrosis quística, enfermedad intersticial pulmonar, asma grave; requerimiento de oxígeno terapia; enfermedad grave de la vía aérea; hospitalizaciones por asma.

• Enfermedad hepática: Cirrosis, hepatitis autoinmune.

• Personas que viven con VIH independientemente del CD4 y CV.

• Pacientes en lista de espera para trasplantes de órganos sólidos y trasplantes de células hematopoyéticas.

• Pacientes oncológicos y oncohematológicos con diagnóstico reciente o “activa”.

• Personas con tuberculosis activa.

• Personas con discapacidad intelectual y del desarrollo.

• Síndrome de Down.

• Personas con enfermedades autoinmunes y/o tratamientos inmunosupresores, inmunomoduladores o biológicos.

• Adolescentes que viven en lugares de larga estancia.

• Personas gestantes de 12 a 17 años con indicación individual.

• Personas con carnet único de discapacidad (CUD) vigente.

• Personas con pensión de ANSES por invalidez, aunque no tengan CUD.

• Personas con pensión de ANSES por trasplantes, aunque no tengan CUD.