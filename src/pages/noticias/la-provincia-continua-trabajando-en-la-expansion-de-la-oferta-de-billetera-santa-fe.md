---
category: Estado Real
date: 2021-09-07T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/EXPANSION.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia continúa trabajando en la expansión de la oferta de Billetera
  Santa Fe
title: La provincia continúa trabajando en la expansión de la oferta de Billetera
  Santa Fe
entradilla: "“Los temas abordados en el día de hoy, forman parte de la agenda de trabajo
  que nos encomendó el gobernador Omar Perotti”, aseguró Aviano."

---
El secretario de Comercio Interior y Servicios, Juan Marcos Aviano, mantuvo este lunes una jornada de trabajo junto a la Sociedad de Carniceros y la Asociación de Industriales Panaderos de Rosario, con el objetivo de ejecutar articuladamente acciones que continúen expandiendo los beneficios del programa Billetera Santa Fe a nuevas bocas de venta minorista del rubro alimentos.

Además, el funcionario trabajó con estas entidades en el desarrollo de dos nuevas iniciativas destinadas a favorecer la actividad comercial por un lado el Observatorio de Precios y por otro lado, un sello de marca Hecho en Santa Fe.

En primer término, Aviano visitó la Sociedad de Carniceros de Rosario donde fue recibido por su presidente, José García, y otras autoridades de la institución. Sobre este encuentro, el secretario destacó: “Los temas abordados están vinculados con el conjunto de medidas que el Gobierno Provincial a través del Ministerio de Producción, Ciencia y Tecnología viene llevando adelante, los cuales tienen que ver con la cadena de producción, industrialización y comercio de artículos de primera necesidad”.

“En el caso de la Sociedad de Carniceros abordamos la utilización de Billetera Santa Fe en el sector, con el objetivo de democratizar la oferta lo mejor posible. Tomamos la iniciativa de llevar adelante una acción puntual de promoción para que, aquellos carniceros que aún no utilicen esta herramienta, puedan sumarse. Por otro lado, ahondamos sobre el Observatorio de Precios que queremos organizar desde el Ministerio para conocer los distintos eslabones de la cadena, sus costos y los productos que se elaboran en la provincia”, agregó el funcionario.

Para concluir la agenda de trabajo, el secretario de Comercio Interior y Servicios, visitó la Asociación de Industriales Panaderos de Rosario. Luego de la visita señaló: “Trabajamos en conjunto en una propuesta que tiene como objetivo desarrollar proveedores para los panaderos asociados, a fin de diseñar una ronda de negocios que incluya a los proveedores de insumos. El trabajo consiste en abordar varios tópicos con la asociación industrial de panaderos”.

“Vamos a acompañarlos en la escuela de panadería que están desarrollando en su nuevo local, porque entendemos que hacen a la producción, al comercio y también al empleo en un sector que genera puestos de trabajo y esas son las políticas que buscamos desarrollar”, finalizó Aviano.