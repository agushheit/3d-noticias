---
category: Estado Real
date: 2021-01-26T10:05:27Z
thumbnail: https://assets.3dnoticias.com.ar/fibra.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La Provincia licitó la renovación de la fibra óptica para mejorar la conectividad
  en la Autopista Santa Fe - Rosario
title: La Provincia licitó la renovación de la fibra óptica para mejorar la conectividad
  en la Autopista Santa Fe - Rosario
entradilla: Se optimizará la red existente y el Sistema de Tránsito Inteligente instalado
  sobre el corredor. El presupuesto oficial supera los 4 millones de pesos.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Dirección Provincial de Vialidad, licitó la adquisición de equipamientos y materiales para ampliar y desarrollar mejoras tecnológicas sobre la traza que conecta la capital provincial con Rosario.

Desde la administración de la Autopista explicaron que para realizar la obra de cierre de anillo de fibra óptica, la Provincia adquirirá tableros, fuentes, módulos, antenas y protectores de tensión, entre otros elementos, con un presupuesto oficial de $ 4.378.218,81.

En este sentido, se podrá lograr una mejora sustancial en la conectividad del Data Center y se optimizará el Sistema de Tráfico Inteligente que opera sobre el corredor vial Brigadier General Estanislao López.

**LAS OFERTAS**

En la apertura de sobres se presentaron dos ofertas: CONEXIS S.R.L. entregó una propuesta base por $ 4.359.048,46 y una alternativa de $ 3.647.905,65; mientras que ATSA AMERICAN TRAFFIC S.A. cotizó la suma de $ 3.842.822 y $ 3.689.109,12 respectivamente.

El acto, que se realizó en las oficinas administrativas ubicadas en el kilómetro 22 de la traza, fue presidido por el director General de Asuntos Jurídicos del organismo, Daniel Cicerchia en presencia del coordinador entre la Dirección General de Concesiones y la Unidad Ejecutora AP 01, Facundo Ayuso; y representantes de las empresas oferentes.