---
category: Estado Real
date: 2021-11-07T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/cuello.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti recorrió localidades del departamento Vera y anunció obras para varias
  localidades
title: Perotti recorrió localidades del departamento Vera y anunció obras para varias
  localidades
entradilla: El gobernador visitó Calchaquí y Margarita, donde dialogó con los vecinos
  sobre las obras en marcha y afirmó que “tenemos que mejorar cada pueblo y cada ciudad
  de la provincia”.

---
El gobernador Omar Perotti recorrió este sábado las localidades de Calchaquí y Margarita, del departamento Vera. En primera instancia, Perotti visitó el barrio San Martín de Calchaquí, donde presentó a los vecinos las obras de cloacas que la provincia va a realizar. Allí el gobernador sostuvo que “nos gusta estar con los vecinos para ver las obras que van avanzando. Me parece que es la mejor forma y lo bueno es que nos podamos volver a reunir. Igual en pandemia se pudo estar trabajando, gestionando y haciendo obras, muchas de esas se están viendo”.

“Esto que pasa en Calchaquí lo vemos en cada una de las ciudades de la provincia, donde hay obras con recursos propios, con recursos que hemos gestionado con Nación, obras que hacemos con los municipios y las comunas. La idea es que sigamos avanzando de esta manera, tenemos que mejorar cada pueblo y cada ciudad de la provincia y, particularmente, las ciudades que crecen. Ésta es una ciudad que está creciendo y hay que ayudarla a que crezca ordenada, con agua, con cloacas, que vaya creciendo con el ripio y en pavimento, con viviendas. Esto arraiga a la gente”.

En Calchaquí estuvo acompañado por su intendente, Rubén Cuello, quien agradeció la presencia del gobernador y describió que las obras “que tienen un presupuesto de $ 79 millones de pesos, incluyen una estación elevadora y seis mil metros de cañería con sus conexiones, abarca desde la ruta hasta la calle San Martín. Esto es muy importante”.

“También vamos a realizar las obras complementarias que nos faltan para completar un 95 o 100% de cloacas de los barrios más carenciados del otro lado de la ruta. En este momento estamos haciendo 44 cuadras de asfalto, ya llevamos 16, los planes que tenemos son de Argentina Hace de Obras Públicas de la Nación, el Plan Incluir de la provincia, y Obras Menores”, finalizó Cuello.

En tanto, el vecino Alcides Mussin afirmó que “estas obras cambian la calidad de vida de muchísimos vecinos. Son obras que suman para toda la población. En el último tiempo se destinaron muchos recursos a Calchaquí para embellecer a nuestra ciudad y eso es motivo para agradecer”.

Por último, las autoridades recorrieron las obras de la terminal. Los trabajos cuentan con un presupuesto de $119 millones y se realizan en el predio delimitado por Juan de Garay, Mitre, Estanislao López y Tucumán, sobre el margen oeste de la Ruta Nacional N°11. La materialización es a través de construcción en seco, estructura metálica, cubierta de liviana, un gran frente vidriado cubierto para el resguardo de la espera de los pasajeros, junto con el espacio semicubierto como puerta de entrada a la ciudad de Calchaquí.

**EN MARGARITA**

En Margarita, el gobernador recorrió las instalaciones del Club Sarmiento, que el próximo lunes firmará un convenio para hacer obras en el marco del Plan Incluir, e iniciarán las gestiones para hacer lo propio con el Club Central. Allí dialogó con los vecinos sobre proyectos para la localidad, “con muchas expectativas, porque vemos una región que empuja, con mucha valoración por el trabajo y por la producción. Hay mucho por hacer en cada uno de nuestros pueblos y ciudades. Eso es lo que ya empezamos a hacer”, describió Perotti.

“Tengo por Margarita un cariño muy grande, yo venía hasta aquí cuando era muy chico. Desde ese momento en adelante siempre hay un vínculo que nos recuerda una etapa muy linda”, finalizó el gobernador.