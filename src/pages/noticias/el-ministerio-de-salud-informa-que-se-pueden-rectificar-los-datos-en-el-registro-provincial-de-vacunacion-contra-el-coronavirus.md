---
category: Estado Real
date: 2021-06-08T07:53:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: El Ministerio de Salud informa que se pueden rectificar los datos en el Registro
  Provincial de Vacunación contra el Coronavirus
title: El Ministerio de Salud informa que se pueden rectificar los datos en el Registro
  Provincial de Vacunación contra el Coronavirus
entradilla: Se recuerda que los mismos deben estar avaladas por un documento médico
  y se deberá firmar una declaración jurada.

---
El Ministerio de Salud recuerda que se desarrolla el operativo para inocular y prevenir el Covid-19 a nivel nacional y provincial. Mediante la resolución 2883/2020, aquellas personas que formen parte de la población de 18 a 59 años con comorbilidades y se inscriban en el Registro Provincial de Vacunación podrán realizar modificaciones, ingresando a [https://www.santafe.gob.ar/santafevacunacovid.](https://www.santafe.gob.ar/santafevacunacovid. "https://www.santafe.gob.ar/santafevacunacovid.")

“Nos encontramos con algunos casos en los cuales las personas habían obviado tildar la patología, o bien las mismas no estaban disponibles, es por ello que se ha determinado modificar el sistema para que puedan realizar estos cambios en la inscripción”, informaron desde la cartera sanitaria.

Asimismo se recuerda que los y las ciudadanas que se inscriban en este grupo “con comorbilidades” deberán llevar un documento que respalde la situación declarada. De no presentarlo, podrán completar una declaración jurada antes de ser inoculados.

**LA IMPORTANCIA DE LA DECLARACIÓN JURADA**

El secretario de Salud, Jorge Prieto, explicó que “la declaración jurada como tal tiene un poder, estamos observando que algunas comorbilidades no se conducen con las patologías pero el turno fue otorgado y es por ello que se realiza una doble declaración jurada. Tenemos la posibilidad, gracias al sistema provincial de salud que es el SICAP, de poder corroborar con la identidad de la persona si efectivamente padece esta comorbilidad, por el diagnóstico o por la dispensa de medicamentos. Es por esto que recomendamos a las personas que se presenten con el certificado que esté justificando la patología que ha inscripto como comorbilidad”, sostuvo Prieto.

En todos los vacunatorios de la provincia de Santa Fe se instará a firmar las declaraciones juradas a aquellas personas que no presenten la documentación respaldatoria.

Vale recordar que de acuerdo al artículo 293 del Código Penal, aquellas personas que presenten una declaración que no sea real o esté falseada pueden tener consecuencias penales.