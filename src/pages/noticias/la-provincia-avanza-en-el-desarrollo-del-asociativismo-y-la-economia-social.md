---
category: Estado Real
date: 2021-09-16T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASOCIATIVISMO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia avanza en el desarrollo del asociativismo y la economía social
title: La Provincia avanza en el desarrollo del asociativismo y la economía social
entradilla: Se reunió el Consejo Provincial del sector y, en ese marco, se presentaron
  la Comisión de Género y el Programa "Educación en Economía Social".

---
Este miércoles se llevó adelante un nuevo encuentro del Consejo Provincial de Asociativismo y Economía Social; en ese marco se presentaron la Comisión de Género de dicho ámbito y el programa “Educación en la Economía Social”. El encuentro, realizado en la Sala de Conferencias del Hotel UNL- ATE, estuvo encabezado por el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna; la ministra de Igualdad, Género y Diversidad, Celia Arena; el presidente del Instituto Nacional de Asociativismo y Economía Social (INAES), Alexandre Roig y el secretario de Educación, Víctor Debloc.

Además participaron de la actividad el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina, el director provincial de Economía Social, Agricultura Familiar y Emprendedorismo, Guillermo Tavernier; la coordinadora del Equipo de Mutualismo y Cooperativismo Escolar del Ministerio de Educación, Gisela Carrizo y el director del Instituto Cooperativo de Enseñanza Superior (ICES) de Sunchales, Román Frutero, entre otras autoridades.

Durante la actividad, Costamagna, valoró: “Este encuentro es muy importante en función de lo que implica todo el esquema cooperativo de nuestra provincia, que se destaca por su diversidad, estructura productiva y por los esquemas asociativos y participativos; lo que marca un fuerte liderazgo. Las mutuales y cooperativas nos han acompañado en todo este tiempo de pandemia, con ellas articulamos fuertemente en cuanto a producción y financiamiento; también en la organización de productores y empresarios en las cooperativas de trabajo, educación, salud y de servicios públicos que nos brindan servicios eficientes”.

“Conceptualmente, la organización en este marco cooperativo es una forma que propugnamos y que nos posibilita gestionar y articular muy fuerte con todos los actores en el territorio de la provincia de Santa Fe. Hoy es el inicio de un nuevo desafío, más abarcativo, de un trabajo articulado que va a contribuir a esta recuperación que el país y la provincia necesitan y que se gesta desde lo productivo”, aseguró el ministro.

Por su parte, el secretario de Desarrollo Territorial y Arraigo, Fabricio Medina, indicó: “Nos reunimos en esta oportunidad para trabajar y abordar en conjunto temas que tienen que ver con género, con la economía social y la educación; con la red provincial de asociativismo y sus oficinas, con la economía social, abarcando un panorama del cooperativismo en la Región Centro”.

“Es la primera reunión luego de más de un año de pandemia, donde volvemos a vernos las caras, lo que nos permitirá planificar acciones y ejecutar las ya previstas, que tienden al fortalecimiento de la economía social y a la recuperación económica que necesitamos por medio de los valores del cooperativismo y asociativismo, que conducen a ese camino a través de este mundo del ecosistema asociativo que tanto beneficio brinda en nuestra provincia”, añadió Medina.

Para concluir, Alexandre Roig agregó: “Desde el inicio de la gestión nos planteamos poner el foco sobre el cooperativismo y el mutualismo, es decir, darle el lugar que se merece. Para ello nos propusimos un proceso de planificación estratégica junto a cada provincia. Esta es nuestra manera de trabajar, estamos convencidos que el proyecto nacional se construye desde las provincias y junto a ellas. En esta labor estamos tomando iniciativas sobre tres pilares, en primer lugar, la reorganización del trabajo, aquí el asociativismo es fundamental, el segundo pilar es reorganizar las finanzas; y en tercer lugar, contribuir a fin de reorganizar los territorios”.

**Perspectiva de género**

En la reunión del Consejo, las carteras productivas y de género abordaron distintas consideraciones respecto a la Ley provincial N° 14.002 de paridad de género firmaron un convenio de capacitación en el marco de la Ley N° 27.499, denominada Ley Micaela.

Al respecto, la ministra de Igualdad, Género y Diversidad, Celia Arena, manifestó: “Llevamos adelante esta jornada que tiene que ver con fortalecer los espacios de cooperativismo en cuanto a la perspectiva de género, estamos trabajando con las Federaciones, capacitando en Ley Micaela, en perspectiva de género y diversidad a las cooperativas”.

“Además, trabajando fuertemente en línea con la legislación nacional con el INAES en la paridad dentro de las cooperativas. Muchos de los espacios cooperativos están conformados mayoritariamente por mujeres, hubo un avance en este sentido, en la integración de las Comisiones Directivas pero, claramente, hay una decisión política de jerarquizar cada vez más estos espacios y hay muchas mujeres que tienen un compromiso muy grande y una trayectoria en el cooperativismo. Su llegada a los espacios de decisión va a enriquecer seguramente a un movimiento tan fuerte en nuestro país y, especialmente, en la provincia de Santa Fe”, culminó.

**Educación en economía social**

El encuentro previó también la presentación del programa "Educación en Economía Social" y la firma de un Acta Acuerdo entre los Ministerios de Producción, Ciencia y Tecnología y el de Educación.

Sobre el tema, el secretario de Educación, Víctor Debloc, manifestó: “Desde el área de educación tenemos una coordinación provincial de cooperativismo y mutualismo escolar dirigida por Gisela Carrizo, quien está trabajando particularmente en la propuesta de desarrollo. La formación siempre es un bien que cotiza muy positivamente en el trabajo de cooperativas y mutuales, por lo que hay una inversión y un esfuerzo importante respecto a la calidad formativa. Cuando la práctica solidaria y asociativa se convierte en un objetivo pedagógico es un logro muy importante”.

“La solidaridad es un bien que se hace, es acción, primero hay que poner en acción la solidaridad y después reflexionar para que mediante el planeamiento se puedan decidir las acciones futuras que darán logros tanto en el plano educativo como en el productivo”, finalizó Debloc.

**Consejo provincial de asociativismo y economía social**

El Consejo Provincial de Asociativismo y Economía Social tiene como objetivo actuar como órgano de consulta y asesoramiento en materia cooperativa y mutual. Está integrado por las entidades de segundo y tercer grado que nuclean a las cooperativas y mutuales radicadas en la provincia de Santa Fe.