---
category: El Campo
date: 2021-01-28T10:21:15Z
thumbnail: https://assets.3dnoticias.com.ar/campo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Gobierno de Santa Fe
resumen: Santa Fe a la vanguardia en la producción de carne bovina a nivel nacional
title: Santa Fe a la vanguardia en la producción de carne bovina a nivel nacional
entradilla: La provincia exportó un 44% del total nacional, lo que la ubica como la
  principal exportadora de productos del sector de Argentina.

---
La provincia es una de las primeras productoras de carne bovina, condición que se ve reflejada en índices informados desde el Ministerio de Producción, Ciencia y Tecnología, cartera desde la cual se destacó que Santa Fe cuenta con el 10,5% del stock bovino nacional, un 17% de participación en la faena argentina y que exportó, sobre un estimado de 917 mil toneladas (equivalente a res carcasa) en 2020, el 44% de productos provenientes de esta actividad.  
  
Al respecto, el secretario de Agroalimentos, Jorge Torelli indicó: “En nuestro territorio el porcentaje de faena es muy importante dado que cuenta con una gran concentración de plantas faenadoras, 33 en total, de las cuales 10 son plantas dedicadas a la exportación. Esto ubica a la provincia en segundo lugar a nivel país por detrás de Buenos Aires, que tiene un alto índice de faena por componente destinado al mercado interno, sobre todo en el Conurbano y al área metropolitana de Capital Federal”.  
  
“En el caso de las exportaciones provenientes de este sector, el año pasado se estima que se colocó en el mercado externo un total de 400 mil toneladas (equivalente a res carcasa), lo que representa un 44% de las exportaciones nacionales de carne, siendo el principal exportador de la República Argentina”, agregó el funcionario.  
  
Asimismo, el secretario manifestó: “Esta es una muy buena noticia que da cuenta que las principales plantas faenadoras han tenido actividad muy intensa en un año difícil en el marco de la pandemia. Este escenario es producto de la capacidad de la industria santafesina, que estuvo a la altura de las circunstancias, superando los problemas sanitarios trabajando fuertemente, en conjunto con el Estado provincial, poniendo en marcha los protocolos de seguridad y planes que no hicieron mermar su capacidad de procesamiento”.  
  
De cara a futuro, Torelli auguró tiempos auspiciosos, destacando que desde el Gobierno Provincial se trabaja para “responder antes las necesidades productivas, aumentar los índices productivos y mantener la cantidad vientres. En ese contexto estamos accionando fuertemente, iniciando este año un ambicioso Plan Ganadero Provincial que buscará darle un gran impulso a las áreas de rodeos de crías donde somos una de las provincias que tiene mayor incidencia en la producción nacional”.