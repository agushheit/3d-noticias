---
category: Agenda Ciudadana
date: 2022-01-26T06:00:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/jxc.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Juntos por el Cambio buscará una foto para mostrar una "posición unificada"
  sobre el proyecto de la Magistratura
title: Juntos por el Cambio buscará una foto para mostrar una "posición unificada"
  sobre el proyecto de la Magistratura
entradilla: 'Buscan "marcarle la cancha al Frente de Todos", y si no pueden imponer
  un proyecto propio, al menos intentarán "hacer fuerza para que el oficialismo tenga
  que modificar algunas cosas" de su propia iniciativa.

  '

---
Pese a la dispersión de proyectos de reforma del Consejo de la Magistratura que impera en la oposición, Juntos por el Cambio (JxC) aceleró los contactos en los últimos días para generar un documento que establezca los criterios básicos que debe contener la iniciativa y de esa manera "mostrar una posición unificada" en el interbloque que sirva para "marcarle la cancha al Frente de Todos".

Según supo NA de fuentes parlamentarias de la oposición, no se apuntará a una unificación de los proyectos de JxC que ya tomaron estado público (hay dos en Diputados y otros dos en el Senado) sino a generar "una especie de punteo sobre lo que debería contener un proyecto de reforma".

En las últimas horas, se intensificaron los mensajes entre los referentes en materia jurídica de las distintas bancadas de JxC y la idea es que una vez que haya una base de acuerdo discutida y avalada por las partes se convoque a una reunión presencial para comunicar la postura del interbloque opositor.

Con esa foto de unidad, JxC busca "marcarle la cancha al Frente de Todos", según confesaron las fuentes consultadas por Noticias Argentinas, ya que el oficialismo impulsa desde el Senado un proyecto que no conforma a la principal alianza opositora.

"Es muy difícil que JxC logre imponer un proyecto propio. Pero sí se puede hacer fuerza para que el oficialismo tenga que modificar algunas cosas del de ellos", reconocieron de todas formas.

Sin embargo, en la oposición no hay una única mirada y prueba de ello son las diferencias entre el proyecto presentado por el jefe del bloque de la UCR en la Cámara baja, Mario Negri, y el que impulsa el diputado del PRO Pablo Tonelli. También hay proyectos presentados en el Senado: por un lado el de los radicales Alfredo Cornejo y Mariana Juri, y por el otro el de la jujeña de la UCR Silvia Giacoppo.

Negri propone restaurar la antigua composición del Consejo de la Magistratura de 20 miembros con el presidente de la Corte Suprema a la cabeza (actualmente le tocaría asumir el cargo a Horacio Rosatti).

Además en esa integración aparece un representante del Poder Ejecutivo; cuatro senadores nacionales (dos por el bloque con más integrantes, uno para la segunda bancada y uno para la tercera) y cuatro diputados (misma distribución).

A ellos se les suman cuatro representantes de los abogados de la matrícula federal (dos por la Capital Federal y dos por el interior); cuatro jueces (dos por la Capital Federal y dos por el interior) y dos representantes del sector académico, en todos los casos respetando la equidad de género.

Por otra parte, el proyecto del radical introduce una novedad que tiene que ver con reducir de tres años a uno el plazo máximo para tramitar una denuncia contra un juez.

En tanto, el proyecto de Tonelli propone llevar a 14 el número de integrantes del Consejo, apenas uno más que en el formato actual, al sumar a un representante de la Corte Suprema de Justicia.

Así las cosas, la composición del organismo quedaría de la siguiente forma: un representante de la Corte Suprema, tres representantes de los jueces del Poder Judicial de la Nación, cuatro representantes del Congreso de la Nación, cuatro representantes de los abogados de la matrícula federal, un representante del Poder Ejecutivo y un representante del ámbito académico y científico.

El proyecto de Tonelli incorpora además un sistema novedoso por el cual cada estamento puede elegir como su representante a una persona que no forme parte del mismo. De esta manera, el representante electo por el estamento desempeñará exclusivamente ese cargo público, sin superponerse con otra función.

Además, el diputado nacional de Juntos por el Cambio busca que los miembros del Consejo de la Magistratura no puedan tener otra profesión que la de abogados, condición que no se exige de acuerdo a la normativa actual.

En otro orden, Tonelli propone que la presidencia del Consejo sea ejercida por el representante de la Corte Suprema de Justicia de la Nación, que no necesariamente debe ser el presidente del máximo tribunal sino cualquier representante elegido por este organismo judicial.

La reforma del Consejo de la Magistratura es un asunto de máxima prioridad para el Gobierno, como también para la oposición, ya que un fallo de la Corte Suprema de mediados de diciembre declaró inconstitucional la ley sancionada en 2006 que regula el funcionamiento del organismo encargado de seleccionar a los jueces, y otorgó 120 días corridos para realizar cambios en la composición del organismo a través de una nueva normativa.

La Corte exhortó al Congreso a sancionar una nueva ley por considerar que hasta ahora no se respetó el equilibrio entre los estamentos de la política, los jueces y los abogados que dan forma al organismo.

De no cumplir con este plazo perentorio, que vence el 15 de abril, el cuerpo quedará inhabilitado para firmar resoluciones tras esa fecha y a su vez, será obligado a retomar la integración que tenía antes de la ley de 2006, cuando eran 20 miembros (y no 13 como en la actualidad).

El proyecto del oficialismo, que reunirá la mayoría en el Senado si el Frente de Todos tiene asistencia perfecta y suma dos aliados a la causa (no así en Diputados donde la cuenta es mucho más compleja) apunta a elevar de 13 a 17 los integrantes del Consejo, incorporando dos abogados, un juez y dos representantes del ámbito académico y científico.

De prosperar la iniciativa del Gobierno, el Consejo quedaría integrado por cuatro jueces del Poder Judicial (ahora son dos), seis miembros del Poder Legislativo (tres diputados y tres senadores nacionales, siendo dos por el bloque mayoritario de cada cámara, y uno por la primera minoría), cuatro representantes de los abogados (ahora son tres), un representante del Poder Ejecutivo, y por último dos correspondientes a los ámbitos académico y/o científico (hasta ahora había uno solo).

Siguiendo la perspectiva de género presente en el proyecto oficialista, tanto en el caso de los legisladores nacionales como en el del estamento de académicos y científicos, y el de los abogados, al menos la mitad de los cargos deberán ser ocupados por mujeres.