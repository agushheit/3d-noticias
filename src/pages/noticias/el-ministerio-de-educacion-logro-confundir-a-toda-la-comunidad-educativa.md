---
category: Agenda Ciudadana
date: 2021-05-23T06:00:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/ROSSI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: '"El Ministerio de Educación logró confundir a toda la comunidad educativa"'
title: '"El Ministerio de Educación logró confundir a toda la comunidad educativa"'
entradilla: Lo dijo la Secretaria Adjunta de la Seccional Santa Fe de la Unión Docentes
  Argentinos, Mariela Rossi.

---
La Secretaria Adjunta de la Seccional Santa Fe de la Unión Docentes Argentinos, Mariela Rossi, salió al cruce de las circulares emanadas del Ministerio de Educación de Santa Fe por lo que “ponderamos un despropósito la comunicación que conlleva confusas directivas emanadas de esa cartera”.

“Padres, madres, alumnos y alumnas se encuentran en vilo por las permanentes contradicciones que rozan el absurdo” y agregó: “Le decimos a la ministra que por momentos más que aclarar, oscurece”.

“Son momentos que requieren precisiones y tareas conjuntas coordinadas con las trabajadoras y trabajadores de la educación”, finalizó.