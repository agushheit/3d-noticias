---
layout: Noticia con imagen
author: "Fuente: La Nación "
resumen: "Vacuna rusa Sputnik V "
category: Agenda Ciudadana
title: No creo que haya sido tan larga la cuarentena
entradilla: El ministro de Salud de la Nación, Ginés González García, se refirió
  este lunes a la compra de 10 millones de dosis de la vacuna rusa Sputnik V
  contra la Covid-19 que anunció el presidente Alberto Fernández.
date: 2020-11-04T19:20:53.828Z
thumbnail: https://assets.3dnoticias.com.ar/ggg.png
---
El ministro de Salud de la Nación, Ginés González García, se refirió este lunes a la compra de 10 millones de dosis de la vacuna rusa Sputnik V conta la Covid-19 que anunció el presidente Alberto Fernández. Y, además, habló sobre las medidas adoptadas por el Gobierno para combatir la pandemia.

"No creo que haya sido tan larga la cuarentena, la empezamos a abrir rápidamente con actividades industriales y comerciales", dijo el funcionario en diálogo con Animales Sueltos, y continuó: "Hasta mediados de abril la cuarentena fue bárbara; después, bastante bien. Ahora no se puede aflojar, no creamos que esto ya pasó".

Consultado por el temor de una segunda ola de contagios, como sucede en la actualidad en varios países de Europa, el ministro opinó que "sin dudarlo" volvería a recomendar un duro confinamiento. "El aislamiento ha sido siempre la mejor alternativa", dijo, aunque lamentó que en estos momentos "hay muy poco acatamiento".

"Esto de ASPO (Aislamiento Social, Preventivo y Obligatorio) tiene poco", señaló y dio como ejemplo el nivel de circulación que hay hoy en la ciudad de Buenos Aires.

**Vacuna rusa**

"Hoy \[refiriéndose al lunes] el Presidente anunció la posibilidad casi segura" de obtener millones de dosis de la vacuna rusa, comentó González García en Radio con Vos. "Aunque todavía no está terminado el acuerdo comercial, no hay ninguna duda de que lo vamos a hacer", sumó.

**Coronavirus en la Argentina: se consolida el descenso de casos en el AMBA y se estaría reduciendo el aumento en el interior**

Según explicó el ministro, "la ventaja es que Rusia ofrece cantidades mayores, y tienen en todo el esquema la llegada de vacunas en diciembre, pero en pequeñas cantidades". "Todas las vacunas son de dos dosis", dijo. En tanto, si se adquieren 25 millones, "sería para 12,5 millones de personas".

El responsable de la cartera de Salud agregó que, en la negociación del gobierno nacional con las farmacéuticas para la adquisición de la vacuna, tiene en cuenta "cuándo va a estar disponible, cuántas y el precio, tres cosas que están conversando con todos". Dado que el objetivo es "vacunar a más de la mitad de la población".

"El Presidente quiere que haya la mayor de gente vacunada; no sabemos cómo será la indicación final, pero queremos que sea mucha la población inmune para detener el virus", indicó, y añadió que "va a ser una cifra muy superior a la que se dijo al principio".

![Crédito: Marcelo Manera](ggg1.png "Crédito: Marcelo Manera")

Por otra parte, dijo que el Gobierno empezó a coordinarlo con todas las provincias."Tenemos que trabajar en la logística, porque son millones de vacunas y dos a cada una. Queremos trabajar con varias, puede ser que alguna no sirva, pero yo creo que no va a pasar. Ahora hay que probar que son efectivas, que previenen la enfermedad", dijo.

Más tarde, en una entrevista con Telenoche, el ministro agregó: "Tengo miedo de que pase lo mismo que en Europa. Estamos en un escenario donde no hay certezas. Yo miro todo lo que pasa en el mundo y la verdad que estoy perplejo. Acá habrá una gran cantidad de argentinos vacunados".

Respecto del número de fallecidos, dijo: "Estamos mal, me atormenta el número de muertos que tenemos, no me gusta como estamos pero esto no terminó".

**Así funcionan las vacunas en carrera contra el coronavirus**

"Lo que ha pasado es una buena noticia, que contrario a lo que nos venía pasando con los otros proveedores, los rusos dicen tener la posibilidad de entregar vacunas a fin de año o a principios del año que viene. Queremos atenuar el impacto de todo esto lo más rápido posible", concluyó.

Este lunes, Fernández anunció que comprarán 10 millones de dosis de la vacuna rusa, que llegarán en diciembre al país. El mandatario confirmó que la cantidad podría subir a 25 millones para enero. "Estamos muy esperanzados. Para diciembre podríamos tener vacuna y comenzar la vacunación", dijo el jefe del Estado.