---
category: Agenda Ciudadana
date: 2022-01-19T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/mobu.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Tras los chispazos públicos, Bullrich y Morales limaron diferencias en una
  reunión cara a cara
title: Tras los chispazos públicos, Bullrich y Morales limaron diferencias en una
  reunión cara a cara
entradilla: 'En el ala dura del macrismo había caído muy mal que el gobernador jujeño
  y presidente de la UCR dijera que el Gobierno de Cambiemos tenía responsabilidad
  en el endeudamiento con el FMI.

  '

---
Luego de los chispazos públicos por las diferencias de mirada en torno a la responsabilidad del Gobierno de Mauricio Macri en el endeudamiento con el Fondo Monetario Internacional, la presidenta del PRO, Patricia Bullrich, y su par de la UCR, Gerardo Morales, limaron asperezas en un encuentro cara a cara, "dando por concluida la interna".

"Hacía falta después de los chisporroteos públicos para mostrar unidos a los presidentes del PRO y de la UCR para dar por concluida la interna", señalaron fuentes cercanas a Bullrich sobre la charla virtual que mantuvieron en la tarde de este martes durante una hora y media.

De esta manera, los jefes partidarios de los dos principales partidos de Juntos por el Cambio pasaron de página luego de que dirigentes del ala dura del macrismo abrieran fuego contra el gobernador de Jujuy, quien había sugerido que el Gobierno de Cambiemos tenía responsabilidad en la crisis de deuda que arrastra el Estado con el Fondo Monetario Internacional (FMI) y que en consecuencia "lo menos" que podían hacer sus dirigentes era aceptar el diálogo propuesto por el Gobierno de Alberto Fernández.

"Esta deuda la contrajimos nosotros y lo menos que tenemos que hacer es ir y escuchar a Martín Guzmán", había reconocido el gobernador de Jujuy, un comentario que cayó mal no solamente en el PRO sino también en sectores del radicalismo que tienen una lectura diferente sobre los desembolsos por 45.000 millones de dólares que hizo el FMI a pedido de Macri entre 2018 y 2019.

"Me parece que ya está, no hay que jugar a las escondidas con esto. Porque van a salir los halcones, que por qué mando al vicegobernador", completó el radical.

El comentario de Morales, que tuvo lugar en la previa a la reunión en el Museo del Bicentenario con Guzmán y Fernández, desató una fuerte polémica al interior de Juntos por el Cambio, que se dividió entre aquellos que pensaban que los gobernadores de la oposición debían asistir a esa instancia de diálogo, y quienes creían que se trataba de una puesta en escena del oficialismo para vender una falsa imagen de concertación, en lugar de proponer un camino de salida concreto a la crisis con el FMI.

Enseguida, empezaron a llover las críticas a Morales, y entre los principales francotiradores se destacaron dos diputados nacionales que justamente responden a la figura de Bullrich, como Fernando Iglesias y Waldo Wolff.

"(La deuda) es hija del déficit que siempre es récord en populismo. Dialogar no es ser funcional al relato", señalaba Wolff.

Iglesias fue aún más duro contra Morales, a quien llamó "ignorante" y lo mandó a "estudiar".

"No 'contrajimos la deuda', ignorante. Tuvimos que pagarla junto con el déficit fiscal que dejó el peronismo. Estudiá", lanzó, y luego realizó otro posteo diciendo que lo tenía "harto el vice de (Roberto) Lavagna", en alusión a la fórmula presidencial del 2007.

Durante la reunión del 6 de enero pasado, Morales le recriminó a Bullrich por el nivel de agresividad en las críticas de Wolff e Iglesias, pero la presidenta del PRO defendió a los diputados del ala dura del PRO.

Las aguas se aquietaron en la última semana por mérito del propio Gobierno, que sorpresivamente dejó sin efecto la convocatoria al diálogo con la oposición que había anunciado públicamente para este martes.

Ese error no forzado del Poder Ejecutivo unificó a todo el arco opositor en los cuestionamientos al Gobierno y permitió descomprimir la interna, dejando atrás las rencillas.

La reunión entre Bullrich y Morales coronó el alto al fuego y la pacificación en la oposición.

Según supo NA, hablaron sobre temas concretos de producción y empleo en la Argentina y también se adelantaron a la discusión del 2023 coincidiendo en la necesidad de ampliar el despliegue territorial para que Juntos por el Cambio tenga candidaturas en todo el país.

"Hablaron también sobre buscar mecanismos para resolver candidaturas en provincias que eventualmente pudieran adelantar elecciones", especificaron las fuentes consultadas.

También hubo una mención explícita a los desafíos que tiene la alianza opositora por delante respecto a "cómo cumplir con el contrato electoral que le hizo ganar la última elección".