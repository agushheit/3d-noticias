---
category: La Ciudad
date: 2021-12-18T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/MECENTERIO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad suma la propuesta turística “La noche de los templos”
title: La Municipalidad suma la propuesta turística “La noche de los templos”
entradilla: "Se trata de una iniciativa diseñada en conjunto con la Mesa de Diálogo
  Interreligioso y Cultura de la Paz. La invitación es a descubrir la historia y el
  presente de ocho edificios religiosos.\n\n"

---
Para este viernes 17 de diciembre, la Municipalidad organizó la primera edición de “La noche de los templos”. Se trata de un recorrido gratuito por ocho edificios de la capital provincial, pertenecientes a diferentes religiones, que abrirán sus puertas de 19 a 22 horas para visitas guiadas. La actividad fue diseñada por el municipio, en conjunto con la Mesa de Diálogo Interreligioso y Cultura de la Paz.

La invitación es a descubrir la historia y el presente de los espacios sagrados, con un alto valor histórico, patrimonial y cultural en la ciudad de Santa Fe. De este modo, se contribuye a la ampliación y la diversificación de la oferta turística de la Municipalidad de Santa Fe que hoy cuenta con nueve paseos guiados y gratuitos, en el marco de la propuesta Mi ciudad como turista, y con audioguías bilingües español-inglés.

Según explicó el director de Turismo municipal, Franco Arone, “el objetivo es ir ampliando la oferta con la expectativa de que la ciudad de Santa Fe consolide su perfil turístico, que los hoteles puedan ofrecer servicios regulares y que, al mismo tiempo, las agencias de viajes y turismo receptivo puedan generar paquetes comercializables que permitan que cada vez más argentinos y argentinas puedan venir a disfrutar de la ciudad de Santa Fe”.

Por su parte, Mónica Levrino de la Iglesia de Jesucristo de los Santos de los Últimos Días e integrante de la Mesa Interreligiosa, destacó la importancia de esta propuesta para la ciudad y aseguró que “todas las organizaciones prepararon distintas actividades como puestas en escena para sacarse fotos, árboles de los deseos; y sobre todo explicarles a los visitantes en qué creemos, cuáles son los ritos puertas adentro”.

**Los edificios**

Para los recorridos, se ponen a disposición los guías de turismo del registro municipal. En tanto, los encargados de cada uno de los templos, conducirán la visita dentro de los inmuebles. En esta primera edición, podrán visitarse los siguientes edificios:

* La Iglesia de Jesucristo de los Santos de los Últimos Días (Jujuy 3160 y Estrada 7450). Allí se proyectará el cortometraje “El niño Jesús”. Además, se montará una escena para fotografías alusivas bajo el título “Sé parte de la historia”
* Templo Comunidad Isrraelita de Santa Fe (1° de Mayo 2152). A las 19 horas comenzará el recorrido por el museo y el shilito, y las acciones de reshet. Además se ofrecerá la escritura de los nombres en hebreo. Desde las 20 horas se celebrará el Kabalt shabat, la ceremonia del viernes por la noche. Se solicita como requisito, presentar DNI
* Cementerio Municipal: Capilla Nuestra Señora del Carmen y Oratorio. Habrá charla con guías y recorridos autoguiados con audioguías de la Municipalidad
* Escuela Científica Basilio N° 35 (Catamarca 3314). Se ofrecerá una charla sobre la historia de los fundadores y un recorrido por los salones y el museo institucional
* Presencia de Dios (Saavedra 6150). En la oportunidad se compartirá el armado del Árbol de Navidad de los Buenos Deseos
* Centro Cristiano Koinonya (República de Siria 7782). La propuesta es una charla sobre las actividades y posterior recorrida por el templo
* Convento de San Francisco (Amenábar 2557). El recorrido por las instalaciones puede hacerse con guías de la Municipalidad pero también a través de audioguías
* Iglesia Evangélica Brazos Abiertos (Corrientes 3621). Se visitará el auditorio para culminar con una charla sobre el estilo de hacer y ser Iglesia

**Trabajo conjunto**

El director de Derechos Humanos del municipio, Publio Molinas, explicó que “La noche de los templos” será un gran cierre para el “intenso trabajo que desarrollamos durante todo el año junto a la Mesa de Diálogo Interreligioso y Cultura de la Paz”.

En ese sentido, informó que entre la Municipalidad y ese espacio “se generaron distintas iniciativas que ya son parte de la agenda ciudadana y que permiten empezar a mostrar un trabajo conjunto”. A modo de ejemplo, recordó que a lo largo de 2021 se puso en marcha la recuperación del oratorio del Cementerio Municipal, se organizaron los Encuentros de la Oración por la Paz, se llevaron adelante campañas de colecta de sangre y se organizaron jornada de voluntariado, entre otras.

“Venimos trabajando con la Mesa desde hace tiempo, en diferentes actividades que la posicionen en la agenda diaria de la ciudad. Ahora llegó el momento de que puedan mostrar sus templos, sus culturas, sus ideas y sumar más participantes a este grupo de diálogo”, insistió Molinas.