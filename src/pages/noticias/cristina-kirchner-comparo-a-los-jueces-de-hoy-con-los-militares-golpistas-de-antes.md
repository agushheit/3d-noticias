---
category: Agenda Ciudadana
date: 2022-01-27T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/cristinahonduras.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Cristina Kirchner comparó a los jueces de hoy con los militares golpistas
  de antes
title: Cristina Kirchner comparó a los jueces de hoy con los militares golpistas de
  antes
entradilla: "\"De la misma manera que se financiaban los golpes militares, se comienzan
  a financiar los golpes judiciales\", sostuvo la vicepresidenta durante una charla
  que brindó en Honduras. \n"

---
A escasos días de la marcha convocada por sectores del kirchnerismo duro, la vicepresidenta Cristina Kirchner comparó el rol que desempeñan algunos jueces de la actualidad en la persecución política y desestabilización de gobiernos populares respecto del que cumplían los militares del siglo pasado que producían golpes de Estado.

"En esta década y media también surgen retrocesos en los pueblos. No ya por golpes militares. Ahora ya no es necesario llevar a militares a educarse a Panamá, ahora se necesitan jueces educados en comisiones y foros. De la misma manera que se financiaban los golpes militares, se comienzan a financiar los golpes judiciales. De las mismas maneras y con los mismos financiadores", sostuvo.

Así se expresó al disertar en el aula máter de la Universidad Nacional Autónoma de Honduras, país al que viajó para concurrir este jueves en el acto de asunción de la presidenta electa, Xiomara Castro de Zelaya.

Luego de recordar el golpe de Estado que militares le propinaron en 2009 al ex presidente Manuel Zelaya (marido de Xiomara Castro), la ex mandataria apuntó contra la Organización de Estados Americanos: "Eran otros tiempos. La OEA se ponía al frente para defender las democracias, no para voltearlas".

La clase magistral de Cristina Kirchner -titulada "Los pueblos siempre vuelven"- también generó un fuerte impacto por sus definiciones sobre el Fondo Monetario Internacional en medio de las duras negociaciones que el Gobierno mantiene con este organismo multilateral, al que cuestionó duramente por las "políticas de ajuste" que pretende imponerle a Argentina a cambio de la reestructuración de la deuda que había contraído el Estado en la época de Mauricio Macri.

Luego de fustigar al neoliberalismo y a los libertarios por su idea de "reducir al Estado a la mínima expresión", señaló que cuando ese sucede "no queda un vacío" sino que el lugar es ocupado por el narcotráfico.

"En una reunión que tuvimos con otro presidente de la región, con Álvaro Colom (ex presidente de Guatemala), contaba que el narco construía escuelas que él no podía porque tenía que aplicar las políticas de ajuste que dictan los fondos", apuntó, aludiendo al FMI, pese a no nombrarlo.

Al respecto, criticó a los "bancos que están un poquito más arriba de México", en referencia a Estados Unidos, por "lavar las formidables cantidades de dinero que hacen los narcos".