---
layout: Noticia con imagen
author: "Fuente: Diario Uno"
resumen: Colegio de Médicos Santa Fe
category: La Ciudad
title: 'Fuerte advertencia del Colegio de Médicos de Santa Fe: hay quienes
  "creen que la situación ya está superada"'
entradilla: 'Carlos Alico, titular de la entidad, reconoció el impacto
  psicológico que dejan ocho meses de pandemia, pero advirtió: "Sería una pena
  tirar por la borda todo el esfuerzo anterior".'
date: 2020-11-10T14:11:15.929Z
thumbnail: https://assets.3dnoticias.com.ar/vacunas.jpg
---
"La realidad es que la situación no está superada" afirmó a UNO Santa Fe el presidente del Colegio de Médicos de Santa Fe, Carlos Alico. "Ni aquí, ni en ninguna parte del mundo", añadió.

Reconoció que la pandemia es "una situación inédita y absolutamente inesperada", y aclaró que de ninguna manera se puede "culpabilizar o demonizar a nadie".

En esa línea, reconoció las consecuencias que provocan siete meses de restricciones. Comentó que si bien hay muchas personas que están "ante situaciones emocionales muy comprometidas", hay otros que "imaginan o creen que es una situación que ya está superada".

"Si no lo tomamos seriamente, es altamente probable que lo terminemos pagando", afirmó. Lo ocurrido en Rosario el último fin de semana, con aglomeraciones de personas en distintos puntos de la ciudad, enciende las alarmas no solo de esa ciudad (donde se observa una baja en los casos diarios) sino en esta capital donde -por el contrario- la cantidad de contagios no descienden y se observa cierto relajamiento en la población.

"Sería una pena y una verdadera lástima tirar por la borda todo el esfuerzo anterior", apuntó.

Angela Prigione, titular del Colegio de Médicos de Rosario, dijo que “lo que se vio este fin de semana no se puede repetir. Pasó lo que se venía viendo en Europa. Hay una falsa sensación de que terminó la pandemia”. Y señaló: “Lo de este fin de semana ya se vio en Europa. Hay una falsa sensación de que la pandemia terminó. Por favor, eso no puede quedar así. Está la cuestión del cansancio, del hartazgo, lo que llamamos una fatiga pandémica de toda la comunidad. Lo de este fin de semana es responsabilidad individual, pero también de los que gestionan esta pandemia”.

El titular del Colegio en Santa Fe pidió tener algo en claro, y enfatizó: "Debemos decir que la emergencia sanitaria no cedió. Hay muchas aperturas que son necesarias, desde el punto de vista social, político y económico, pero no cedió; claramente no cedió".

Graficó que "hoy, el centro de la provincia de Santa Fe es la más comprometida. Miremos los números porcentuales, no totales, que tiene una ciudad como Rafaela, Esperanza o Santa Fe".

El referente del Colegio de Médicos en Santa Fe reconoció el impacto psicológico que deja tantos meses "encuarentenados" y destacó el gran compromiso que hubo en toda la población: "Los adultos, los que no pudieron trabajar, los que sí pudieron salir; los niños, adolescentes; todos estuvieron comprometidos. No olvidemos que en el 2020 no hubo escuela, un lugar de sociabilización indispensable y que incide en la conducta de los niños".

"La pandemia afectó la salud de las personas y es un golpe emocional y económico que no se puede soslayar. Es una situación complicada, donde los gobiernos deberán estar a la altura para generar medidas equitativas", comentó.

Alico habló de la necesidad de "autocuidarnos" para seguir conviviendo con el virus en esta etapa de pandemia. "Debemos tener una mirada inteligente y observar lo que le pasó a países europeos, con una segunda oleada más agresiva que la anterior".

Consultado sobre cómo se encuentran los médicos de Santa Fe tras casi ocho meses de pandemia, el presidente del Colegio prefirió hablar de "equipos de salud", y apuntó: "Es indudable que se nota pero con absoluta claridad una situación no solamente de cansancio, sino también de irritabilidad".

Destacó que los profesionales de la salud no están exentos a las consecuencias de la pandemia y, como el resto de la sociedad, deben soportar el aislamiento y ajustarse a los protocolos cuando se registran casos positivos. "El equipo de salud ha estado siempre presente, en todo momento", afirmó.