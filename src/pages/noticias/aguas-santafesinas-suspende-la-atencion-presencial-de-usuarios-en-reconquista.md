---
category: Estado Real
date: 2021-01-11T10:04:02Z
thumbnail: https://assets.3dnoticias.com.ar/110121-aguas-santafesinas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Reconquista: Aguas Santafesinas suspende la atención presencial'
title: Aguas Santafesinas suspende la atención presencial de usuarios en Reconquista
entradilla: Será desde este lunes hasta nuevo aviso. La medida se fundamenta en la
  situación que presenta la pandemia Covid-19. Los trámites y consultas podrán gestionarse
  a través de WhatsApp o desde el portal de la empresa.

---
**Desde este lunes 11 de enero, Aguas Santafesinas suspende hasta nuevo aviso la atención presencial** a sus usuarios en la oficina de la ciudad de Reconquista, ubicada en Bv. Hipólito Yrigoyen 1180.

La medida se fundamenta en la situación que presenta la pandemia Covid-19 en los últimos días, con un crecimiento de la curva de contagios.

Hasta el momento, la aplicación de protocolos especiales permitió la atención al público en Reconquista, pero el aumento de la circulación del virus exige actuar con mayor prudencia a fines de evitar aglomeraciones.

La decisión apunta particularmente a preservar el personal de la empresa, con el objetivo de garantizar la producción y prestación de un servicio público aún más esencial en las actuales circunstancias: agua potable y saneamiento.

<br/>

### **Atención**

Cabe destacar que Aguas tiene a disposición de sus usuarios el portal web [www.aguassantafesinas.com.ar](http://www.aguassantafesinas.com.ar "http://www.aguassantafesinas.com.ar") y un nuevo canal de auto consulta de fácil acceso, sin costo alguno a través del WhatsApp 341 695 0008.

En ambos casos los usuarios pueden efectuar, de manera sencilla trámites o consultas de diverso orden, reclamos técnicos y pago, descargas e impresión de facturas.

Además de estos canales ya disponibles, la empresa contará, de  lunes a viernes, con atención telefónica al usuario de 8:00 a 14:00 horas, a través del número local 03482-418258.