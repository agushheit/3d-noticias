---
category: Agenda Ciudadana
date: 2021-10-24T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/amsafecap.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los gremios docentes que aceptaron la oferta del gobierno cobrarán el aumento
title: Los gremios docentes que aceptaron la oferta del gobierno cobrarán el aumento
entradilla: Así lo afirmó el ministro de Trabajo de la provincia, Juan Manuel Pusineri,
  quien ratificó la convocatoria para este miércoles, a las 10, a los gremios que
  no aceptaron.

---
El ministro de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri, se refirió este sábado a la liquidación de los aumentos salariales en el marco de la paritaria docente. "Ratifico lo que se vino sosteniendo en forma invariable, en el sentido de que los mismos no alcanzan a aquellos gremios que no aceptaron la oferta salarial"

"Esto es por una sencilla razón, y es que la negociación requiere acuerdo que en este caso no existe", agregó Pusineri.

"La existencia de una norma general permite proceder a liquidar los incrementos para quienes sí estuvieron de acuerdo, lo que se encuentra normativamente dispuesto", informó el titular de la cartera laboral.

"A todo el sector pasivo docente sí se le va a abonar el aumento como anunciamos el día viernes", detalló Pusineri más adelante.

Para finalizar, Pusineri informó que "los gremios que rechazaron la oferta salarial serán convocados el próximo miércoles 27 a las 10 horas para seguir dialogando, algo a lo que siempre estamos abiertos y predispuestos".