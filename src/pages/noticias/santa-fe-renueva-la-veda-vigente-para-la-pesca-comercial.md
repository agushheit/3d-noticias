---
category: Agenda Ciudadana
date: 2021-06-02T08:59:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/pesca-deportiva.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Santa Fe renueva la veda vigente para la pesca comercial
title: Santa Fe renueva la veda vigente para la pesca comercial
entradilla: Continuarán las restricciones establecidas con anterioridad. La decisión
  surgió luego de una Reunión del Consejo Provincial Pesquero en la que participaron
  todos los actores vinculados a la temática.

---
El Gobierno de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, resolvió la continuidad de la metodología vinculada a la pesca comercial y tareas de acopio de productos de pesca en aguas del Río Paraná. De esta forma, durante el mes de junio y hasta tanto una nueva norma la sustituya, continúa totalmente prohibida la pesca comercial durante los días viernes, sábados, domingos y feriados.

En el caso de la pesca deportiva, se estableció que la actividad puede practicarse exclusivamente con devolución obligatoria de toda especie, con sujeción a lo dispuesto por la normativa jerárquicamente superior dictada como consecuencia de la pandemia por el COVID-19.

La resolución surge luego de una reunión del Consejo Provincial Pesquero, encabezada por la ministra de Ambiente y Cambio Climático, Erika Gonnet, que tuvo lugar el jueves 27 de mayo de manera virtual, quien estuvo acompañada por el secretario de Políticas Ambientales, Oreste Blangini, el asesor técnico Gaspar Borra y el equipo ministerial del área.

En la misma, participaron todos los actores vinculados a la temática: representantes de pescadores comerciales o artesanales, de cooperativas de pescadores, de clubes de pescadores deportivos, de frigoríficos y acopiadores de pescado y de organizaciones de la sociedad civil tales como El Paraná No Se Toca y Taller Ecologista. También hubo representantes del sector turístico, de universidades e institutos técnicos y científicos, de Municipios y Comunas, de la Administración de Parques Nacionales, del Poder Legislativo provincial, y del Ministerio de la Producción, Ciencia y Tecnología de la provincia.

En esta oportunidad también asistieron representantes de la Procuración de la Corte Suprema de Justicia de la provincia y de los amparistas que promovieron la veda que dispuso la Justicia a fines de 2020.

Entre los considerandos de la nueva Resolución que entra en vigencia se destaca la evaluación de la situación actual del recurso ictícola de la cuenca del río Paraná en general y de la sometida a la jurisdicción de la Provincia de Santa Fe en particular, sumado a los aspectos sociales, culturales y económicos involucrados en la cuestión.

En especial, se tuvieron en cuenta los resultados de la última campaña del “Proyecto de Evaluación Biológica y Pesquera de Especies de Interés Deportivo y Comercial en el Río Paraná” (EBIPES), realizada a fines de abril.

Lograr una pesca sostenible en el tiempo requiere tener en cuenta el manejo de los peces y el componente social y económico, asegurando la sostenibilidad biológica del recurso. En ese sentido, es importante resaltar que las restricciones pesqueras establecidas tienen como objetivo principal disminuir el esfuerzo de pesca (número de días que pueden capturarse peces), mejorando las chances de sobrevida para un mayor número de ejemplares y posibilitando el eventual éxito reproductivo cuando las condiciones hidrológicas y climáticas resulten propicias.