---
category: Agenda Ciudadana
date: 2021-08-06T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/BIGOTENJEFE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Presidente retornó a Casa Rosada y define nuevos ministros y DNU por Covid
title: El Presidente retornó a Casa Rosada y define nuevos ministros y DNU por Covid
entradilla: Alberto Fernández viajó para asistir a la asunción del presidente Pedro
  Castillo y tras su regreso decidió permanecer aislado en la Quinta de Olivos, según
  lo indica la normativa.

---
El presidente Alberto Fernández retornó a la Casa de Gobierno luego de su período de aislamiento tras su viaje oficial a Perú, con dos temas a resolver en el horizonte, como son la prórroga del Decreto de Necesidad y Urgencia (DNU) que establece restricciones para frenar los casos de coronavirus, en especial para la variante Delta, que vence el viernes, y la definición de los reemplazos de los ministros de Desarrollo Social Daniel Arroyo; y de Defensa Agustín Rossi; precandidatos a ocupar cargos legislativos en las próximas elecciones.

La decisión de mover de sus cargos a esos funcionarios fue del propio mandatario, quien el 28 del mes pasado dijo en declaraciones televisivas que "todos" los funcionarios que son precandidatos para las elecciones primarias abiertas, simultáneas y obligatorias (PASO) del 12 de septiembre próximo "deben dejar sus cargos" para hacer frente a la campaña electoral y, en caso de ganar, asumir en sus bancas legislativas, porque se trata de "una regla ética".

Arroyo es el decimosegundo precandidato a diputado nacional en la lista del Frente de Todos (FdT) por la provincia de Buenos Aires y Rossi el primer precandidato a senador nacional por Santa Fe, secundado por la vicegobernadora Alejandra Rodenas, por la agrupación "La Santa Fe que queremos".

Esa fórmula confrontará con la lista lista "Celeste y Blanca" del FdT, promovida por el gobernador Omar Perotti y que lleva como candidatos al periodista deportivo Marcelo Lewandowski y a la actual senadora María de los Ángeles Sacnun; quienes tienen también el aval del Presidente y que hoy fueron recibidos por la vicepresidenta Cristina Fernández de Kirchner, en la sede del Instituto Patria, junto a los candidatos a diputados nacionales por ese distrito Roberto Mirabella y Magalí Mastaler.

A nivel provincial ya renunció Daniel Gollan como ministro de Salud bonaerense, para dedicarse a la campaña para posicionar a su lista, de la que también es parte Arroyo, aunque en su caso desde el segundo lugar, detrás de la platense Victoria Tolosa Paz, quien también dejará su cargo como titular del Consejo de Políticas Sociales.

**Las medidas sanitarias**

Además, el mandatario deberá definir si prorroga este viernes el DNU 287/21 que establece medidas sanitarias para mitigar el impacto del coronavirus en el país, y en ese sentido recibió en la tarde del jueves en su despacho a la ministra de Salud, Carla Vizzotti; a la asesora presidencial Cecilia Nicolini; y a la secretaria Legal y Técnica, Vilma Ibarra; para delinearlo y analizar eventuales cambios.

A su vez, el jefe de Gabinete, Santiago Cafiero, emitirá una nueva Decisión Administrativa (DA) para determinar el cupo de pasajeros argentinos o residentes diarios que podrán ingresar al país por vía aérea.

En esa línea, la titular de la Dirección Nacional de Migraciones, Florencia Carignano, adelantó que ese organismo volverá a habilitar desde el sábado próximo el trámite de reunificación familiar, que permitirá el ingreso al territorio nacional de extranjeros que sean familiares directos de argentinos nativos o extranjero residente y que puedan acreditar tal circunstancia, informaron fuentes del Ministerio del Interior, cartera que tiene esa oficina a su cargo.

Esos viajantes deberán ingresar por el Aeroparque Jorge Newbery, Aeropuerto de San Fernando, Aeropuerto Internacional de Ezeiza o la Terminal de Buquebús del Puerto de Buenos Aires.

También Carignano dijo en Radio La Red que se estudia autorizar el ingreso al país de turistas extranjeros que "se hayan aplicado las dos dosis" de las vacunas contra el coronavirus y luego de haber cumplido "una cuarentena", con el objetivo de "mover" la actividad, una de las más afectadas por la pandemia, y mencionó que esa mecánica se utiliza en Uruguay y en los países de la Unión Europea.

El Presidente volvió esta tarde a las actividades presenciales luego de resultar negativo un nuevo test PCR contra el coronavirus y tras haber cumplido el aislamiento preventivo desde su llegada de Perú, el jueves pasado, adonde asistió a la asunción del presidente de ese país Pedro Castillo.

El mandatario arribó a las 13.10 en automóvil desde la Quinta Presidencial de Olivos, en lugar de su traslado habitual en helicóptero, debido a una intensa niebla sobre Buenos Aires.

"Habiéndose realizado el test de PCR y atento al resultado de dicho estudio, siendo este negativo, el Primer Mandatario retoma sus actividades habituales de manera presencial", se indicó en un comunicado de la Unidad Médica Presidencial, que lleva la firma de su titular, Federico Saavedra.

Tras retornar el jueves pasado desde Perú, Fernández se efectuó un testeo que dio negativo y desde ese momento cumplía el aislamiento en Olivos.

Si bien está exceptuado de la exigencia del aislamiento por ser personal esencial, el jefe de Estado decidió cumplir el aislamiento obligatorio, de acuerdo con las normas establecidas por el Ministerio de Salud.