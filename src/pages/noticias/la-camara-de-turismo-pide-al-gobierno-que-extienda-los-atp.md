---
category: Agenda Ciudadana
date: 2021-01-26T09:27:18Z
thumbnail: https://assets.3dnoticias.com.ar/turismo.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: N/A
resumen: La Cámara de Turismo pide al Gobierno que extienda los ATP
title: La Cámara de Turismo pide al Gobierno que extienda los ATP
entradilla: La Cámara de Turismo pide al Gobierno que extienda los ATP

---
El presidente de la Cámara Argentina de Turismo, Aldo Elías, reveló hoy que le pidió al Gobierno que extienda el programa de Asistencia al Trabajo y la Producción (ATP) al menos por seis meses más porque de lo contrario el sector "puede explotar" ante la baja actividad.

En declaraciones radiales, el dirigente empresarial señaló que "por suerte" la Argentina pudo abrir el turismo en el verano después de que se perdiera completamente la temporada de invierno 2020, aunque señaló que están rigiendo las restricciones horarias en casi todos los destinos turísticos de la Argentina, lo que consideró como "algo fundamental para un sector fuerte del sector como es la gastronomía".

"Por ahora se viene tolerando la avanzada sanitaria. Seguimos teniendo una temporada que nos permite salir de la facturación cero que sufrimos la mayor parte del año", indicó el representante de los agentes del turismo a nivel nacional. No obstante, indicó que "el nivel de ocupación es bajo" y que "se definió tarde la temporada", dado que recién a fines de noviembre hubo mayor claridad sobre cómo iba a ser el verano para quienes quisieran vacacionar.

"Mucha gente salió a alquilar casas y departamentos. Eso afectó la hotelería y la gastronomía, porque esa gente cocina en las casas", dijo Elías y agregó: "El escenario por delante es incierto". También comentó que el potencial número de turistas se achicó mucho en esta temporada porque hubo gran cantidad de personas que pertenecen a grupos de riesgo que optaron por no salir de vacaciones a ningún destino.

"Hay que entender que la actividad, que emplea a 1,3 millones de personas, está en una situación difícil. Ya se perdieron al menos 100.000 puestos de trabajo y la situación puede explotar este verano", afirmó.

Finalmente, dijo que este es "un momento en el que el Gobierno tiene que seguir con los ATP. Hemos pedido que los prorrogue por seis meses. La situación requiere esta medida".