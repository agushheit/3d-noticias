---
category: La Ciudad
date: 2021-03-28T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/BRASIL.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santafesinos varados en la frontera con Brasil
title: Santafesinos varados en la frontera con Brasil
entradilla: 'Hay unas 150 personas a las que no se les permiten regresar al territorio
  argentino. Entre ellos hay tres guardavidas de Santa Fe que fueron a trabajar por
  la temporada de verano al vecino país.  '

---
En el paso fronterizo terrestre entre Uruguayana (Brasil) y Paso de los Libres (Corrientes) hay al menos unos 150 argentinos que no pueden regresar al país, ya que les bloquearon el ingreso. Según la página oficial del gobierno de la Nación en la que se detallan las aperturas o cierres de fronteras, el paso donde están varados los argentinos figura como “abierto”.

“Somos unas 150 personas sin poder regresar a nuestro país. Esperamos que nos den una mano. El Consulado nos dijo que pasemos nuestros datos para ver si nos dan la autorización de ingresar a Argentina”, dijo Nicolás Orlando en diálogo con El Litoral, y agregó que “decidimos volver por la cantidad de casos que hay en Brasil y por la nueva cepa. Nos pidieron un PCR negativo de 72 horas de anticipación y lo hicimos. Además, teníamos presente un decreto que decía que comercio internacional, residentes y ciudadanos podían pasar con el PCR. Cuando llegamos a la frontera nos dijeron: ´No van a pasar\`”, detalló el joven.

Nicolás es uno de los tres santafesinos guardavidas que se fueron hace cinco meses a trabajar en la temporada de verano, “nos fuimos porque no teníamos trabajo, ninguno de los que quiere volver estaba de vacaciones, son todos trabajadores”, sostuvo. Además, su compañera Lucía también santafesina advirtió que “hay un colectivo entero y gente en autos particulares. Hay gente que está desde las 5 de la mañana, somos todos argentinos residentes no hay turistas”.