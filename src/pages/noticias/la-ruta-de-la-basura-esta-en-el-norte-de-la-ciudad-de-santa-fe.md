---
category: La Ciudad
date: 2021-09-07T06:00:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/BASURA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: La "ruta de la basura" está en el norte de la ciudad de Santa Fe
title: La "ruta de la basura" está en el norte de la ciudad de Santa Fe
entradilla: En el costado de la calle Monseñor Rodríguez hay desperdicios de todo
  tipo. Son unos 3 kilómetros, desde Avenida Blas Parera hasta la Circunvalación.

---
La falta de recolección de la basura, lamentablemente, es un problema de larga data. Pero sin dudas hay zonas urbanas en las que el abandono es mucho más evidente. En esta oportunidad, un vecino que transita a diario por el norte de la ciudad se comunicó con El Litoral para reflejar “la ruta de la basura”.

La calle en cuestión es Monseñor Rodríguez, entre Avenida Blas Parera y la Circunvalación, en pleno barrio Nuevo Horizonte. Es el área donde justamente está el Centro de Salud de Nuevo Horizonte y el del Abasto: se trata de la parte de atrás del Mercado de Productores, del Parque Industrial Los Polígonos y unos de los caminos para acceder al barrio privado Los Molinos.

El aspecto en la banquina de Monseñor Rodríguez es lamentable. Hay basura de todo tipo: desde animales muertos y televisores rotos hasta los desperdicios “habituales” que puede contener una bolsa.

Permanentemente se ven familias enteras buscando “algo que sirva” entre la basura, con el riesgo sanitario que eso puede traer aparejado. Esa misma gente es la que ni se acuerda de la última vez que vio pasar un camión recolector.