---
category: La Ciudad
date: 2021-11-05T06:15:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESTAFAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Nueva modalidad de estafa a través de WhatsApp
title: Nueva modalidad de estafa a través de WhatsApp
entradilla: 'Desde la Defensoría del Pueblo informan que los ciberdelincuentes con
  esta nueva estafa suplantan la identidad de un conocido y ofrecen la venta de dólares

'

---
Ante la aparición en las últimas horas de numerosos casos, la Defensoría del Pueblo de la provincia de Santa Fe alerta sobre una nueva modalidad de estafa, llevada a cabo a través de la suplantación de identidad en WhatsApp.

De acuerdo al relato de uno de los casos recibidos en la institución, un hombre recibió un mensaje con el texto “Buenas tardes Juan, acá Ana. Guardá mi nuevo contacto, saludos” (los nombres usados son ficticios). El mensaje provenía supuestamente de Ana, una mujer con quien la víctima había trabajado hacía muchos años.

Luego de este mensaje, Juan y Ana intercambiaron varios textos, hasta que finalmente la mujer le envió: “Te iba a preguntar si sabés de alguien que me pueda comprar unos dólares”, junto con un video donde se veía una mesa llena de billetes. Esta situación alertó a Juan, quien decidió verificar y corroborar que en realidad no estaba hablando con su amiga, sino con ciberdelincuentes.

Luego, Juan tomó conocimiento de que otro amigo en común no logró advertir el ardid y fue estafado por 650 dólares. El intercambio sucedió en los mismos términos que el planteado entre Juan y Ana y luego de realizar la transferencia para la supuesta compra, Ana le envió un mensaje diciendo “en un rato te van a transferir”. La transferencia nunca llegó. Al no recibirla, intentó contactar a Ana por el mismo número, pero ya no tenía foto de perfil ni recibía los mensajes. Ya sus victimarios habían cortado el contacto. Preocupado por su dinero, entró en su perfil de Facebook para comunicarse con su amiga. En su muro había una alerta: “No he cambiado de número, alguien se está haciendo pasar por mí. Estén atentos, no soy yo”.

Las modalidades de estafas se renuevan constantemente, y es por esto que la Defensoría del Pueblo alerta a la población con el objetivo de que esté atenta a situaciones como la descripta y da una serie de consejos para poner en práctica.

**¿Qué se recomienda para disminuir riesgos?**

Al recibir un mensaje de un amigo o familiar notificando que cambió su número de teléfono, escribir al número que ya se tiene registrado o alguien de su entorno para verificar la información.

_• Cuando un contacto notifique que realizó un cambio de número, pedir un mensaje de voz de comprobación o hacer preguntas determinadas que solo ambos conozcan la respuesta._

_• Evitar realizar compras o ventas a través de mensajería de WhatsApp o redes sociales, sin tener contacto directo con la otra persona involucrada._

_• No agregar a personas desconocidas a redes sociales y mantener privada la información de contacto._

_• Cambiar periódicamente las contraseñas en redes sociales._

_• Implementar pasos de seguridad de verificación para ingresar en tu correo electrónico y redes sociales._

_• No abrir correos electrónicos que tengan enlaces de remitentes que no conoce ni proporcione datos a través de formularios o en respuestas del correo._

_• En caso de tener un correo de Gmail, habilitar pasos de seguridad de confirmación al ingresar en su cuenta, mientras que en Hotmail se pueden enviar códigos a los números de teléfono._

_• Facebook, desde 2018, activó una opción te permite elegir entre tres y cinco contactos de confianza para poder recuperar la cuenta en caso de ser hackeada. Esa persona recibe un código que le permite ingresar._

_• Evitar abrir correos con remitentes desconocidos, donde exijan claves de ingresos a sus cuentas de correo y redes sociales._

Por último, quienes tengan alguna duda o hayan sido víctimas de estafas pueden comunicarse con la Defensoría del Pueblo en busca de asesoramiento, completando la plantilla disponible en [www.defensoriasantafe.gob.ar/tramites,](https://www.defensoriasantafe.gob.ar/tramites) llamando a los teléfonos de contacto disponibles en la web, o personal tras sacar turno a través del portal. También se recuerda a la población que deben realizar la correspondiente denuncia ante el Ministerio Público de la Acusación.