---
category: Agenda Ciudadana
date: 2020-12-24T11:12:08Z
thumbnail: https://assets.3dnoticias.com.ar/2412garcia.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Los biocombustibles también son parte de la agenda verde
title: Los biocombustibles también son parte de la agenda verde
entradilla: La Diputada Nacional Ximena García visitó  la empresa DOBLE L BIOENERGIAS
  S.A. en un claro gesto de apoyo al sector de BIOCOMBUSTIBLES, que reclama que el
  gobierno nacional fije el precio que haga viable su producción.

---
Tras el recorrido por la planta, en la que estuvo acompañada por Leandro Luqui (titular de la empresa) y Pablo Caligaris (encargado de producción), la legisladora puntualizó: «Hoy el precio de la tonelada de biodiesel establecido por el Gobierno es $ 48.533 (este año se autorizó un único aumento del 10%), monto muy por debajo del costo de producción que llevó a que muchas pymes elaboradoras de biodiesel estén paralizadas con riesgo de cerrar si el gobierno no cumple con la Ley vigente».

Cabe destacar que el principal insumo para la elaboración de biodiesel es el aceite de soja, cuyo precio, en constante aumento, no es considerado como una variable relevante por las autoridades nacionales. Ante esta situación, el corte con biocombustibles en Diesel (10% biodiesel) y en Nafta (12% etanol) que fija la ley no se está cumpliendo por parte de las petroleras.

«Más allá de las condiciones fluctuantes de mercado, que hacen conveniente o no incorporar biocombustibles al diésel o a las naftas, existe una variable estrictamente ambiental que debemos considerar. Es de suma importancia que comencemos a **reducir cada vez más la utilización de los combustibles fósiles**, debiendo paulatinamente reemplazarlos por energías renovables, contribuyendo a construir los pilares de una economía circular y sustentable», señaló García, y agregó «este es un compromiso internacional que nuestra nación ha asumido al adherir a distintos protocolos».

Producto de la Ley de Biocombustibles, proliferaron en el territorio nacional, y puntualmente en territorio santafesino, múltiples empresas, principalmente PyMEs, que a partir de importantes inversiones localizadas en el interior de la provincia, como es el caso de Doble L BIOENERGIAS SA que se encuentra en SA PEREIRA, generan empleo genuino y desarrollo económico.

Finalmente la Diputada concluyó: «Si bien la producción de biodisel siempre ha sido volátil en nuestro país, de acuerdo con las condiciones macroeconómicas locales e internacionales,  este año registra los niveles más bajos de producción de los últimos diez años. Por esto me comprometo a impulsar, en la próxima sesión de la Cámara de Diputados, la tan **necesaria prórroga a la Ley de biocombustibles**, que ya cuenta con media sanción del Senado, a fin de dar previsibilidad e impulsar la actividad del sector».