---
category: Agenda Ciudadana
date: 2021-11-15T06:15:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/losada.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Losada se impuso sobre Lewandowski en el Senado y JxC también ganó en Diputados
title: Losada se impuso sobre Lewandowski en el Senado y JxC también ganó en Diputados
entradilla: 'Con el 72 por ciento de los votos Juntos por el Cambio le sacaba ocho
  puntos de diferencias al PJ en Santa Fe para la categoría de senadores nacionales

'

---
Con el 72 por ciento de las mesas escrutadas la lista a senadores nacionales de Juntos por el Cambio (JxC), que integran Carolina Losada y Dionisio Scarpín, se impone con claridad a la nómina de Marcelo Lewandowski y María de los Ángeles Sacnún. Además, en la categoría de diputados nacionales JxC obtuvo un triunfo que le permite llevarse cinco de las nueve bancas en juego. El Frente de Todos conseguiría tres bancas y la restante quedaría para el Frente Amplio Progresista.

Minutos después de las 21 se difundieron los primeros datos oficiales con una carga que supera el 70 por ciento y marca tendencias definitivas. Carolina Losada (40,4 por ciento contra 32,1 por ciento con el 72 por ciento de los sufragios escrutados) ganaba la elección a senador y Juntos por el Cambio se quedará con dos bancas a la Cámara alta contra una del Frente de Todos. Sin embargo, la periodista radicada a Buenos Aires no asoma a priori como candidata a gobernadora. Cómo se resolverá esa cuestión es clave para que la elección de hoy se convierta en un trampolín a la Gobernación.

La elección del Frente de Todos era floja, con el 32,1 por ciento de los votos, y parece difícil que el gobernador Omar Perotti, que iba en la lista como senador suplente, pueda imponer condiciones para su sucesión. Sí quedaría mejor parado Marcelo Lewandoski, con una gran elección en Rosario, su ciudad, aunque no tanto en el resto de la provincia. Al menos sacaba casi un punto más que el candidato a diputado Roberto Mirabella, el delfín de Perotti.

Mientras tanto, el Frente Progresista conseguiría que Mónica Fein se convierta en diputada nacional, pero el crecimiento con respecto a las paso no es tan significativos –12,3% de los votos– y la posibilidad de volver a gobernar Santa Fe parece lejana, una consecuencia directa de la muerte de Miguel Lifschitz.

Las nueve bancas se repartirían de la siguiente manera: cinco para Juntos por el Cambio con Mario Barletta, Victoria Tejeda, Luciano Laspina, Germana Figueroa Casas, Gabriel Chumpitaz; tres para el Frente de Todos con Roberto Mirabella, Magalí Mastaler y Eduardo Toniolli. La restante es para el Frente Amplio Progresista con Mónica Fein.