---
category: La Ciudad
date: 2021-03-04T00:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Jatón prepara su discurso para inaugurar las sesiones del Concejo
title: Jatón prepara su discurso para inaugurar las sesiones del Concejo
entradilla: El intendente de Santa Fe, Emilio Jatón, inaugurará este jueves el período
  de sesiones ordinarias 2021 del Concejo Municipal. La cita es a las 10, en el recinto
  de calle Salta al 2900.

---
El intendente de la ciudad de Santa Fe, Emilio Jatón, expondrá, ante los ediles e invitados especiales, su segundo discurso en estas circunstancias, con el condimento de que transcurre un año electoral en el que se renuevan ocho bancas en el Concejo, tres de su espacio político. Con el primer año de gestión cumplido en diciembre y a casi un año de las medidas derivadas por la pandemia, aprovechará la oportunidad para repasar lo actuado y mencionar la ayuda brindada por el municipio a los sectores más golpeados. 

Por los estrictos protocolos, esta vez no habrá lugar en el recinto para los referentes barriales, de instituciones y Ong's de la ciudad como ocurrió el año pasado, pero se los invitó a seguir el acto por meet. Sí están invitados el gobernador Omar Perotti y la vice, Alejandra Rodenas; el presidente de la Corte Suprema de Justicia, Roberto Falistocco, y su antecesor, Rafael Gutiérrez; y los secretarios de su gabinete, entre otras autoridades.

 

**Algunos ejes**

Con algunas obras en marcha o en procesos de licitación, casi todas con fondos nacionales y provinciales, el intendente renovará la promesa de "integrar Santa Fe" que hizo el año pasado en su primer discurso. Destacará los trabajos que ya se desarrollan en Camino Viejo a Esperanza y para otros barrios del noroeste, como Cabal y Las Lomas; y el plan para mejorar la iluminación en Scarafía, Siete Jefes, Guadalupe NE, Liceo Norte y Santa Marta. Además, destacará el programa de bacheo que inició a fines de enero y medidas para revalorizar la maquinaria municipal. 

Entre las obras no faltará la ya anunciada para llevar agua potable a Colastiné Sur, cuya licitación se realizará el 10 de marzo próximo en la vecinal del barrio. Pondrá énfasis en el incremento de más de un 40% del presupuesto en políticas de género que aplicó para este año. E insistirá con un proyecto de transporte multimodal del que aún no ha dado detalles.

No abundará en anuncios. Solo se explayará en un proyecto personal para poner en marcha una nueva planta de residuos de aparatos eléctricos y electrónicos de la que carece la ciudad capital, y otras medidas relacionadas a la separación de basura. Y es de suponer que rendirá cuentas. Al menos es lo que dijo en 2020: "El año que viene, cuando nos volvamos a encontrar en este recinto, queremos rendir cuentas de todo lo que compartimos hoy".