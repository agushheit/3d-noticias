---
category: Agenda Ciudadana
date: 2021-01-07T10:16:41Z
thumbnail: https://assets.3dnoticias.com.ar/07-01-21-veda-pesquera.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Proponen que se habilite la pesca comercial tres días a la semana
title: Proponen que se habilite la pesca comercial tres días a la semana
entradilla: El gobierno provincial apeló la medida judicial de veda total y elevó
  una propuesta para que los pescadores puedan trabajar y que se libere también la
  pesca deportiva, siempre con devolución en todas las especies.

---
El gobierno de Santa Fe concluyó hoy con las presentaciones ante la justicia en el marco de la medida cautelar que establece veda total para la pesca en aguas del río Paraná. 

A través de Fiscalía de Estado, **la provincia elevó una propuesta de veda parcial para la pesca comercial y habilitación de la pesca deportiva con devolución**.

Cabe recordar que este martes el gobierno había realizado una primera presentación solicitando apertura de feria judicial, recurso de apelación y la suspensión del fallo hasta tanto se expida la Cámara. Luego de esa instancia, **la Justicia habilitó la feria dando lugar además a la apelación que tiene su consecuencia en la presentación de la propuesta provincial**.

«La propuesta elaborada propone habilitar la pesca comercial durante 3 días por semana, ampliando así los días de prohibición a los ya establecidos por la Ley Nº 12.703», explicó la ministra de Ambiente y Cambio Climático, Erika Gonnet. Además, detalló que se busca «permitir la pesca deportiva exclusivamente con devolución obligatoria de toda especie», tal lo establece la Ley Nº 12.212 en su artículo 28.

«Venimos sosteniendo que coincidimos con el cuidado del recurso ictícola, y en ese marco a través de las direcciones técnicas se elaboró esta propuesta, con la participación del Consejo Provincial Pesquero, que va de la mano con una mirada regional que tiene al cuidado del recurso como eje e incorpora una perspectiva social, económica y productiva», continuó la ministra. 

También agregó que **durante el año se realizaron diversas acciones orientadas al cuidado del recurso** destacando «la reducción en un 50 por ciento del cupo de exportación de especies de río, gestionado por la provincia ante el Comité de Pesca Continental».