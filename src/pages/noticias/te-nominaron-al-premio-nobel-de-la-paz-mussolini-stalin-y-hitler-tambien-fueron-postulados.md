---
category: Agenda Ciudadana
date: 2021-02-11T06:50:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/06xp-nobel-medal-jumbo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: The New York Times
resumen: "¿Te nominaron al Premio Nobel de la Paz? Mussolini, Stalin y Hitler también
  fueron postulados"
title: "¿Te nominaron al Premio Nobel de la Paz? Mussolini, Stalin y Hitler también
  fueron postulados"
entradilla: Miles de personas, incluidos profesores universitarios, pueden presentar
  candidaturas. Cada año se envían cientos de postulaciones.

---
A diferencia de las renombradas ceremonias de premiación de Hollywood, en las que el simple hecho de estar nominado es un honor, el Premio Nobel de la Paz acepta postulaciones de miles de posibles candidatos.

El Comité Noruego del Nobel, encargado de seleccionar al ganador del premio, no revela los nominados ni quiénes los nominaron sino hasta 50 años después, lo cual deja a criterio de las personas si quieren anunciarlo por su cuenta.

Tras el cierre de la convocatoria para las nominaciones de este año, Alexéi Navalni, el líder disidente ruso; Greta Thunberg, la activista adolescente contra el cambio climático, y la Organización Mundial de la Salud estuvieron entre los nominados, según informó Reuters.

También se mencionó a Stacey Abrams, la exdirigente de Georgia a quien se le atribuyó el aumento de participación electoral el año pasado, y a Jared Kushner, el yerno y asesor del expresidente Donald Trump. (Trump mismo fue nominado al galardón en los últimos dos años de su presidencia, sin contar las dos nominaciones que fueron falsificadas en 2018).

Reuters realizó una encuesta entre los legisladores noruegos que “han elegido al ganador en ocasiones anteriores”.

La lista de personas que pueden postular nominados es larga e incluye a miembros de gobiernos nacionales; funcionarios al frente de organizaciones internacionales por la paz; profesores universitarios de historia, ciencias sociales, derecho, filosofía, teología y religión, y ganadores de ediciones pasadas.

El comité del Nobel afirma que la gran cantidad de personas o entes que pueden hacer las postulaciones garantiza una “gran variedad de candidatos”, pero el grupo es hermético sobre el proceso y no respondió a una solicitud para aclarar los criterios de elegibilidad de los proponentes.

En 1967, el año más reciente disponible en los registros del comité del Nobel, se presentaron 95 nominaciones (se pueden nominar grupos o individuos más de una vez en el mismo año). El comité afirmó que el año pasado hubo 318 postulaciones, por lo que el récord sigue siendo de 376 en 2016.

Hay pocos criterios para la nominación de candidatos y, en ocasiones, algunos se han aprovechado del proceso por motivos abiertamente políticos.

Uno de los casos más famosos fue el de un legislador antifascista de Suecia que nominó a Adolf Hitler en 1939 como un acto satírico. Jamás tuvo la intención de que “su postulación se tomara en serio”, se lee en una nota adjunta a su nominación archivada.

Iósif Stalin, el líder de la Unión Soviética, fue nominado en dos ocasiones, en 1945 y 1948. Benito Mussolini, el dictador italiano, fue nominado dos veces en 1935.

El proceso de selección para determinar al ganador es mucho más riguroso. El comité, nombrado por el Parlamento de Noruega, delibera en secreto a partir de febrero. El grupo reduce la lista de nominados a 20 o 30 candidatos antes de entrar a un periodo de meses de consideración. El ganador se anuncia en octubre.

El comité del Nobel ha hecho énfasis en que las nominaciones no representan un respaldo de parte del grupo y “no pueden utilizarse para insinuar una afiliación con el Premio Nobel de la Paz”.

Sin embargo, Trump es un ejemplo de cómo las nominaciones mismas pueden usarse para proyectar influencia.

En 2019, Trump les dijo a sus partidarios que había sido nominado por el entonces primer ministro de Japón, Shinzo Abe, una aseveración que Abe se rehusó a confirmar. (El premio de ese año fue concedido a Abiy Ahmed, el primer ministro de Etiopía).

El año pasado, después de que dos políticos europeos dijeron haber nominado a Trump, la secretaria de prensa de la Casa Blanca, Kayleigh McEnany, se refirió a la postulación como “un honor bien merecido y ganado a pulso por el presidente”.

Finalmente, el premio de 2020 fue otorgado al Programa Mundial de Alimentos.

De hecho, Trump había sido nominado por dos miembros escandinavos de derecha del Parlamento. No obstante, para sus seguidores, las posturas políticas personales de quienes lo nominaron, o sus escasas posibilidades de ganar el premio, eran menos importantes que la imagen que daba la nominación.

“A Donald Trump lo nominan a otro Premio Nobel todos los días”, anunció con alegría la presentadora de Fox News, Laura Ingraham, en su programa. “Es obvio que Trump debería obtener el premio”.

En un mitin de campaña en octubre, Trump se quejó de que su nominación había sido menos comentada por los medios noticiosos que la de su predecesor. (De hecho, el expresidente Barack Obama ganó el premio en 2009).

“Me acaban de nominar al Premio Nobel”, declaró. “Luego prendí el televisor y solo había noticias falsas, una tras otra. Hablan del clima en Florida y de otras cosas. Pasa una noticia y otra, y no dicen nada de esto. ¿Recuerdan cuando Obama lo recibió en sus primeros meses y ni siquiera sabía por qué se lo habían dado?”.

El premio otorgado a Obama, a solo nueve meses del inicio de su primer mandato, fue recibido con asombro y desconcierto, incluso por el ganador.

“Para ser honesto”, dijo Obama después del reconocimiento. “En realidad, siento que no merezco estar junto a tantas figuras transformadoras que han recibido el honor de este premio, hombres y mujeres que me han inspirado a mí y al mundo entero mediante su valerosa búsqueda de la paz”.

Daniel Victor es un reportero radicado en Londres que cubre una amplia variedad de historias con un enfoque en las últimas noticias. Se unió al Times en 2012, procedente de ProPublica. @bydanielvictor