---
category: Agenda Ciudadana
date: 2020-12-05T12:00:15Z
thumbnail: https://assets.3dnoticias.com.ar/suspension-paso.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: NA'
resumen: Gobernadores llegaron a un acuerdo para pedir la suspensión de las PASO
title: Gobernadores llegaron a un acuerdo para pedir la suspensión de las PASO
entradilla: Se trata de 21 mandatarios provinciales. Quedaron afuera Rodríguez Larreta,
  Rodríguez Saá y el mendocino Suárez.

---
Los 21 gobernadores que compartieron en el día de ayer un almuerzo en la Quinta de Olivos con el presidente Alberto Fernández acordaron avanzar en un pedido oficial para que se suspendan las Primarias Abiertas, Simultáneas y Obligatorias (PASO) del próximo año.

La movida de los mandatarios provinciales ya se venía armando desde semanas atrás, cuando algunos gobernadores comenzaron a plantear públicamente la necesidad de cancelar las primarias.

Este viernes, la firma del Consenso Fiscal 2020 sirvió para reunir a la mayoría de los gobernadores en Buenos Aires, donde cerraron el acuerdo para avanzar en la suspensión de las PASO.

Según coinciden, las primarias del 2021 se deben cancelar para ahorrar recursos en medio de la crisis y la pandemia de coronavirus.