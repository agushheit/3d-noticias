---
category: La Ciudad
date: 2021-12-31T06:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/añonuevo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Servicios municipales por la celebración de Año Nuevo
title: Servicios municipales por la celebración de Año Nuevo
entradilla: "Este viernes 31 de diciembre habrá asueto en el ámbito municipal, por
  lo que el horario de algunos servicios se verá modificado. Lo mismo sucederá el
  sábado 1 de enero, día feriado por la llegada del nuevo año.\n\n"

---
La Municipalidad informa los horarios de los servicios que se prestarán durante este 31 de diciembre y 1 de enero, en el marco de la celebración de Año Nuevo. Se recuerda que, debido al asueto administrativo, el viernes no estarán abiertas las cajas municipales y sólo habrá guardias mínimas.

**Recolección de residuos y barrido**

En cuanto a la recolección de residuos, las empresas Cliba y Urbafe completarán este viernes la recolección matutina y vespertina, hasta las 19 horas, pero no prestarán servicio nocturno. El sábado 1 de enero habrá recolección nocturna, desde las 20 horas, y el domingo 2, el servicio será completamente normal.

Respecto de los Eco Puntos, permanecerán cerrados el viernes y el sábado, pero estarán abiertos el domingo, en los horarios habituales.

Por otra parte, este viernes se prestará normalmente el servicio de barrido y levante de montones por parte de las empresas Cliba y Urbafe, tanto en el horario matutino como en el vespertino. Para el sábado, desde las 7 horas, la Municipalidad diagramó un operativo especial de barrido manual en las costaneras Este y Oeste; la Ruta 168, a la altura de los boliches; los bulevares Pellegrini y Gálvez; la Recoleta; y la Peatonal San Martín. Además, habrá barrido mecánico en Costanera Oeste, bulevar Muttis, bulevar Gálvez, Avenida Galicia, Avenida. Alem, Rivadavia y la zona de la Recoleta, Avenida Freyre, bulevar Pellegrini, Aristóbulo del Valle, Facundo Zuviria, y López y Planes

Del mismo modo, se brindará un servicio especial de placeros, el sábado 1 de enero, entre las 8 y las 12 horas. La intención es reforzar la limpieza de algunos espacios muy concurridos, tal es el caso de la Plaza Pueyrredón, la Plaza Constituyentes, la Plaza Colón y Plaza del Soldado.

A esto, se sumarán ocho contenedores para depósito de residuos, distribuidos estratégicamente sobre la Costanera Oeste, para que quienes pasen por el lugar durante la noche del 31 y la madrugada del 1 tengan dónde dejar sus residuos.

**Controles de tránsito y alcoholemia**

Al igual que en los festejos navideños, desde la Secretaría de Control y Convivencia Ciudadana se informó que se interrumpirá el tránsito en la Costanera Oeste, desde las letras corpóreas hasta el Faro, entre las 22 del viernes y las 8 del sábado. También en la Costanera Este y el Puente Colgante quedará vedada la circulación vehicular.

Del mismo modo, tal como se hizo en la madrugada de Navidad, volverá a concretarse el Sendero Seguro, con personal de la Guardia de Seguridad Institucional (GSI). El mismo consiste en acompañar la llegada y la salida de los jóvenes desde bulevar Gálvez y Dorrego hasta la zona de boliches, ubicada en la Ruta 168. El mismo atravesará el Puente Colgante y continuará por el camino de acceso a los locales bailables. Este dispositivo se realizará desde las 2 y hasta las 7 horas.

Además, la dirección de Control verificará el funcionamiento de la actividad nocturna y el horario de finalización de boliches y eventos. Se recuerda que el cierre podrá extenderse hasta las 6. En tanto, los paradores de la Costanera Este permanecerán cerrados.

Por otra parte, se articulará con Policía de la provincia todo lo referente a la presencia de efectivos en las zonas más concurridas como la costanera, parques, plazas y espacios públicos.

También habrá retenes de tránsito y de alcoholemia dinámicos, en los lugares de mayor concentración de personas. A esto se sumará un operativo especial para evitar el expendio de bebidas alcohólicas y de pirotecnia.

**Transporte y estacionamiento**

En cuanto al transporte público de pasajeros, el viernes 31 tendrá una frecuencia habitual y, a las 20.30, saldrán de las paradas los últimos colectivos hasta finalizar el recorrido. El sábado 1 de enero, en tanto, la frecuencia será la de un día feriado.

Finalmente, el sistema de estacionamiento medido (SEOM) no operará el viernes 31 ni el sábado 1 de enero.

**Cementerio**

El horario de visita será el habitual, de 7.30 a 12.30 horas. Asimismo, las inhumaciones serán de 7.15 a 11.30.

**Mercado Norte**

El Mercado Norte, ubicado en la intersección de Santiago del Estero y Urquiza, abrirá el viernes de 8 a 16. El sábado 1 permanecerá cerrado.

**Juzgado de Faltas**

En este caso, las oficinas ubicadas en avenida Presidente Perón y bulevar, sólo funcionarán el viernes 31, de 9 a 12 horas. Luego habrá guardias mínimas.

**Guardavidas**

Según se informó, el servicio se prestará en los horarios habituales, es decir, de 9 a 20 horas, tanto el viernes 31 como el sábado 1, en playas y parques de la capital provincial.