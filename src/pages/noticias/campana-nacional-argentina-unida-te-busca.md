---
category: Agenda Ciudadana
date: 2021-11-05T06:15:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/NIETES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Campaña nacional “Argentina Unida te busca”
title: Campaña nacional “Argentina Unida te busca”
entradilla: En colaboración con el trabajo que llevan adelante las Abuelas de Plaza
  de Mayo, la Secretaría de DDHH de la Nación busca resolver la mayor cantidad posible
  de casos de nietas y nietos apropiados.

---
La Secretaría de Derechos Humanos de la Nación impulsa la campaña “Argentina Unida te busca”, que forma parte del plan federal para colaborar con el trabajo que llevan adelante las Abuelas de Plaza de Mayo con el objetivo de ampliar la búsqueda en todo el territorio que esclarecer casos de nietas y nietos apropiados, además de difundir la lucha de Abuelas entre las nuevas generaciones y el concepto de derecho a la identidad.  
  
La campaña, lanzada en el marco de la conmemoración del Día Nacional por el Derecho a la Identidad, tiene como objetivo expandir la búsqueda en todo el territorio del país, pero también trabajar en la promoción del derecho a la identidad en términos generales, no solo en lo referido a la apropiación de niños y niñas durante el terrorismo de Estado.  
  
Gracias al trabajo que Abuelas vienen sosteniendo desde hace más de cuatro décadas, se pudieron resolver 130 casos de apropiaciones de bebés ocurridas durante la última dictadura cívico-militar. Sin embargo, y a pesar del tiempo transcurrido, aún falta encontrar a casi 300 nietos y nietas.  
  
A través de la apelación a la sensibilidad y el recuerdo, mediante distintas piezas audiovisuales, se busca generar empatía para franquear el circuito habitual de la comunicación de los derechos humanos y extender a todas las áreas del Estado -sumando al proyecto la colaboración de ministerios, dependencias gubernamentales, municipios -, así como organizaciones de la sociedad civil, el compromiso del derecho a la identidad como política pública.  
  
\#ArgentinaUnidateBusca está dirigida a mujeres y hombres nacidos entre 1975 y 1983, que duden sobre su identidad, y a la población en general que pueda aportar información para la búsqueda de los nietos y nietas que faltan.  
  
**Juicios por los crímenes de lesa humanidad**

Por otra parte, en torno al Plan Estratégico para el avance de la justicia en materia de Derechos Humanos, la Secretaría informó a través de un comunicado las distintas iniciativas que ya fueron puestas en práctica en lo que va del año para marcar el camino del Proceso de Memoria, Verdad y Justicia.  
  
Se han realizado, con el objetivo de fortalecer los aportes de la Secretaría en el proceso de justicia en todo el país, decenas de acciones y denuncias frente a demoras para garantizar celeridad en los procesos judiciales, la presentación de nueva querellas institucionales, incorporación de un cuerpo de peritos para su participación en las juntas médicas, impulso a los procesos judiciales vinculados con la responsabilidad empresarial en las causas, promover que se escuche la voz de las víctimas en las audiencias y lograr una mayor difusión de los juicios.  
  
Según señala la Secretaría, los siguientes pasos “priorizarán una mayor articulación y trabajo colectivo con los organismos de derechos humanos, los/as abogados/as querellantes y el Ministerio Público Fiscal para trabajar en reformas y cambios más profundos y estructurales”.