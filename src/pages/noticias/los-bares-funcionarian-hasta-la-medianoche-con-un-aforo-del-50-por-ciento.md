---
category: La Ciudad
date: 2021-07-23T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARES.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Los bares funcionarían hasta la medianoche con un aforo del 50 por ciento
title: Los bares funcionarían hasta la medianoche con un aforo del 50 por ciento
entradilla: Vence el decreto provincial y habría flexibilizaciones, con Santa Fe saliendo
  de zona de alarma y pasando a ser de alto riesgo. Vuelven los deportes de contacto
  y reuniones sociales hasta 10 personas.

---
Aunque falta la decisión oficial del gobierno de la provincia, altas fuentes oficiales dejaron entrever que las flexibilizaciones se ampliarán a partir de este viernes en Santa Fe, cuando venza el último decreto de la Casa Gris. En ese contexto, de no mediar contramarchas de último momento, volverían las reuniones sociales hasta diez personas en lugares cerrados, ampliarían el aforo al 50 por ciento en los bares, clubes, gimnasios, iglesias y casinos; se podría volver a practicar deportes de contacto en lugares cerrados con una capacidad del 50 por ciento.

Este viernes vence el decreto que firmó el gobernador Omar Perotti el 9 de julio con las restricciones propias de las zonas en "alarma epidemiológica" por la cantidad de contagios y los niveles de ocupación de las camas críticas. El miércoles el gobierno nacional declaró que tanto Santa Fe como Rosario ya no entran en esta clasificación. A partir de ahora se trata de departamentos con "alto riesgo epidemiológico". Según esta clasificación, se ampliarían las restricciones según indica el Boletín Oficial.

En tal sentido, la provincia definirá este viernes algunas flexibilizaciones según estos parámetros, luego de las consultas pertinentes al Ministerio de Salud y a los intendentes. Si se aprueban estos parámetros, en Santa Fe se volverían a permitir las reuniones sociales en domicilios particulares de hasta 10 personas, y en espacios públicos de hasta 50 personas.

En cuanto a la práctica recreativa en establecimiento cerrados, se ampliaría el aforo al 50 por ciento, al igual que en los casinos, bingos y en los eventos culturales y religiosos en lugares cerrados.

En los cines, teatros, clubes, gimnasios, centros culturales y otros establecimientos afines también se ampliaría la capacidad al 50%, adoptando los protocolos de cuidados respectivos.

Respecto a la gastronomía, si Santa Fe adhiere a lo que indica el Boletín Oficial, los restaurantes y bares podrían abrir hasta la medianoche, con un aforo del 50%.

También se autorizan los viajes en grupos de hasta 10 personas para actividades recreativas, sociales y comerciales, en tanto se verifiquen las siguientes condiciones:

a) Que tengan como ciudad de origen y destino localidades que no sean consideradas en situación de alarma epidemiológica y sanitaria.

b) Que se originen y finalicen en terminales de ómnibus debidamente habilitadas.

c) Que no se trate de viajes de egresados y egresadas y jubilados y jubiladas.

d) Que no incluyan pasajeros o pasajeras que hayan estado en el exterior durante los últimos 14 días o sean convivientes de quien revista tal condición.

e) Que los pasajeros o las pasajeras no hayan sido contacto estrecho de un caso confirmado de Covid-19 durante los 14 días previos al viaje, ni hayan tenido un diagnóstico positivo de Covid-19 en el mismo período.