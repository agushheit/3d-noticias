---
category: La Ciudad
date: 2021-11-16T06:00:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/ILUMINACION.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Trabajos de iluminación y bacheo previstos para este martes
title: Trabajos de iluminación y bacheo previstos para este martes
entradilla: La Municipalidad informa el avance del cronograma de tareas previstas
  en los planes de Bacheo y Santa Fe se Ilumina. A medida que se desarrollan los trabajos,
  puede haber cortes de circulación y desvíos de colectivos.

---
En el marco del plan Santa Fe se Ilumina, la Municipalidad concreta tareas tendientes a recuperar el alumbrado existente en toda la ciudad, para llegar a fin de año al 40% de la ciudad con luces led. Cuadrillas municipales trabajan en la colocación de nuevas columnas de iluminación y artefactos led en distintos barrios y avenidas, como así también en el mantenimiento de la red actual de la ciudad.

En ese sentido, se trabajará en la colocación de nuevas luminarias en:

* Los barrios: 7 Jefes, Guadalupe, Yapeyú y Ceferino Namuncurá
* “Avenidas de tu barrio”: recambio de luminarias vapor de sodio a tecnología led en calle Berutti entre avenida Blas Parera y el reservorio

También se realizará el mantenimiento del sistema eléctrico, entre otros puntos, en:

* El acceso a Alto Verde, por calle Demetrio Gomez
* Avenida Alem
* Espacios comunes en Las Flores II

**Bacheo**

Por otra parte, el municipio avanza con el plan de bacheo diseñado para reparar más de 12.000 metros cuadrados de calles de la capital provincial. El programa se desarrolla por fases, priorizando las zonas de la ciudad que evidencian mayor circulación.

Actualmente se trabaja en:

* Aristóbulo del Valle, entre J.M. Zuviría y Milenio de Polonia
* Urquiza, entre Lisandro de la Torre y General López
* Europa y Boneo
* Ecuador y La Paz
* 25 de Mayo y Santiago del Estero
* Marcial Candioti y Gobernador Candioti

Según se informa, todas las tareas se realizan siempre en sentido del tránsito. Puede haber cortes de circulación y desvíos de colectivos a medida que avanzan las obras. Además, se detalla que los trabajos están sujetos a las condiciones climáticas.