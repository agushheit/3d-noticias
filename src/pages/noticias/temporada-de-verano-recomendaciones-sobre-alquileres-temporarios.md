---
category: La Ciudad
date: 2021-11-29T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/AQUIERESVEERANO.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Temporada de verano: recomendaciones sobre alquileres temporarios'
title: 'Temporada de verano: recomendaciones sobre alquileres temporarios'
entradilla: La Municipalidad brinda algunos consejos acerca de los vínculos contractuales
  entre locador y locatario, de cara al verano. Además, se recuerdan las vías de comunicación
  ante consultas o reclamos.

---
La Dirección de Derechos y Vinculación Ciudadana del municipio recuerda que, con el inicio del período estival, se multiplican las relaciones contractuales en torno al alquiler de quintas, casas y/o departamentos de manera temporaria. En ese marco, y debido al incremento registrado en las consultas de este tipo, se extienden algunos consejos y recomendaciones a tener en cuenta para evitar malas experiencias.

El director de Derechos y Vinculación Ciudadana municipal, Franco Ponce De León, recordó que es habitual recibir consultas respecto de estas prácticas, tanto de personas que buscan propiedades, como de dueños de que desean alquilar sus inmuebles en este período. “Frente a ello, brindamos información pertinente para que todos conozcan los alcances de los contratos temporales y puedan disfrutar de su descanso sin inconvenientes”, explicó.

**Alquiler temporario**

Según el Código Civil y Comercial de la Nación, un contrato de alquiler temporario con fines turísticos es un acuerdo para brindar alojamiento en viviendas amuebladas por un período no mayor a los tres meses. La duración de ese vínculo, dependerá de la voluntad de las partes, siempre que no se exceda del máximo establecido legalmente.

Aún pese a que el período de tiempo es acotado, se recomienda elaborar por escrito y firmar el contrato correspondiente, teniendo en cuenta que se debe incluir la mayor determinación de derechos y obligaciones de las partes y evitar aquellas cuestiones pautadas verbalmente, sin consignarlas en el contrato. Del mismo modo, es importante incluir un inventario del inmueble en dos ejemplares, a fin de evitar reclamaciones futuras por el deterioro, no funcionamiento o la falta de objetos.

En ese sentido, se recuerda que el locatario tiene la obligación de pagar el precio convenido y mantener la integridad del inmueble, conservando la propiedad en las mismas condiciones en que se recibió. En tanto, el locador debe cobrar el monto pautado y garantizar el pleno uso del mismo de manera pacífica y sin alteración del destino para el que se contrató.

Los días y horarios de entrega también deben consignarse por escrito. Para que todo quede claro, es importante la lectura previa del instrumento contractual, antes de la firma de las partes, de modo que lo rubricado responda a todo lo acordado.

Ponce de León mencionó la importancia de concretar la búsqueda de posibles inmuebles mediante páginas web conocidas, que gozan de buena reputación y alta credibilidad. A ello sumó el contacto con corredores inmobiliarios, inmobiliarias o directamente las oficinas de turismo de las distintas municipalidades.

De igual modo, desaconsejó alquilar a través de redes sociales y solicitó a quienes lo hagan, “extremar las precauciones, redoblar la atención y pedir la mayor cantidad de información posible: teléfono fijo o celular, número de CUIT para conocer los datos de la persona y la ubicación exacta del inmueble para ver la dirección en el mapa, a través de internet”.

Por último, el funcionario acotó que “si no existen posibilidades de firmar un contrato, es importante que todo lo acordado quede registrado en las conversaciones por correo electrónico o WhatsApp”.

**Vías de comunicación**

Ante cualquier duda, inconveniente o reclamo, vecinos y vecinas de la ciudad pueden contactarse con la dirección de Derechos y Vinculación Ciudadana a través de las siguientes vías: mediante la[ Oficina Virtual](https://oficinavirtual.santafeciudad.gov.ar/web/inicioWebc.do?opcion=noreg), al WhatsApp 3425135450, al teléfono 0800 444 0442, a la casilla de correo electrónico derechosyvinculacionciudadana@santafeciudad.gov.ar o personalmente en Las Heras 2883 (ala suroeste del ex Predio Ferial) de lunes a viernes, de 7.15 a 13 horas.