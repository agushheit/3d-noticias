---
category: Estado Real
date: 2021-12-05T06:00:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/cumbianna.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Con una existosa noche inicial, arrancó la 5ta Fiesta Nacional de la Cumbia
title: Con una existosa noche inicial, arrancó la 5ta Fiesta Nacional de la Cumbia
entradilla: Santa Fe vibró en la primera de las tres noches del evento con la presencia
  de miles de personas que disfrutaron de los grupos elegidos por la gente en las
  redes sociales.

---
Este viernes comenzó en el Hipódromo de Las Flores, la esperada quinta edición de la Fiesta Nacional de la Cumbia Santafesina, que cuenta con la participación de más de 20 grupos referentes de la movida tropical, y se desarrolla hasta el 5 de diciembre.

La jornada arrancó con los grupos elegidos por la gente a través de la votación en las redes sociales como La Banda de Leonel, Grupo Malta y Candela Vargas Tropical. Posteriormente, continuaron Gerardo y La Sonora, Grupo Trinidad y La Contra. Para el cierre, Juanjo Piedrabuena y Coty Hernández, hicieron vibrar al público santafesino con una espectacular puesta en escena.

Miles de personas acompañaron la movida tropical más esperada del año y la programación continuará este sábado con la presencia de Mario Pereyra, Grupo Cali, Grupo Alegría, Hernán Narváez, Diana Ríos, Los Tekilas, Wendy Zanco y Adelina Me Gusta.

El domingo 5, día del cierre del evento, tendrá como anfitriones a Sergio Torres, Uriel Lozano, Marcos Castelló, Chanchi y Los Auténticos, Grupo Teorema, Ayelén Beker, Los Libres y La Tole Tole.

Para hoy está previsto que las puertas del predio se abran a las 19.30. El comienzo de la programación musical será a partir de las 20.30. Las entradas anticipadas se venden en los puntos oficiales: Chopería Santa Fe, 1980 Bv., Tribus Bar, Costa Litoral, La Feria Argentina o a través de la web Ticketway.com.ar. El valor de las entradas en el lugar tiene un costo de 700 pesos, y se espera un importante número de asistentes para esta segunda noche de la Fiesta.

**Vacunación contra el Covid-19**

Cabe destacar que el coordinador de dispositivos territoriales del Ministerio de Salud provincial, Sebastián Torres, adelantó que este fin de semana habrá un puesto de vacunación en el predio donde se desarrollará la Fiesta de la Cumbia. Al lugar se podrán acercar todos aquellos que aún no recibieron ninguna dosis contra el coronavirus. No deben solicitar turno y el servicio es gratuito.

**Movida solidaria**

Este año, el Gobierno de Santa Fe y ADER organizaron una movida solidaria que involucra a 20 instituciones y que consiste en cambiar un juguete o alimento no perecedero por una entrada para la Fiesta Nacional de la Cumbia. Nicolás Cabó, presidente de ADER, dijo que es una “oportunidad para el santafesino que no tiene la posibilidad de comprar una entrada, de hacer este intercambio solidario”.

Además, Cabó expresó que “los eventos son motor para la economía de la ciudad y la región. La fiesta tiene un condimento extra que es la cumbia, que es nuestra identidad cultural. Por eso, nos pareció importante ser parte de ella”.

Por último, señaló que “se eligieron más de 20 instituciones que intercambiaron entradas por alimentos no perecederos, juguetes, y distintas cosas que nos pidieron las instituciones que trabajan todo el año”.