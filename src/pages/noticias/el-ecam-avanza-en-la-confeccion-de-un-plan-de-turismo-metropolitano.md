---
category: Agenda Ciudadana
date: 2020-12-19T11:00:10Z
thumbnail: https://assets.3dnoticias.com.ar/1912-ecam.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: El ECAM avanza en la confección de un plan de turismo metropolitano
title: El ECAM avanza en la confección de un plan de turismo metropolitano
entradilla: Funcionarios de municipios y comunas participaron de una conferencia sobre
  turismo y desarrollo, durante la cual se debatieron los desafíos en el nuevo contexto
  internacional.

---
Luego de un año en el que la actividad turística sufrió las consecuencias de la pandemia, y ante las medidas sanitarias que potencian los destinos de cercanía para este verano, el sector inicia un proceso de reactivación que, se espera, comenzará esta temporada estival y crecerá durante el 2021. 

En ese marco, funcionarios de municipios y comunas que integran el Ente de Coordinación del Área Metropolitana (ECAM) participaron este jueves de una conferencia dictada por Camilo Makón y dirigida a actores públicos y privados, vinculados a la actividad turística de la región.

La charla fue el puntapié para que los integrantes del ECAM comiencen a debatir la confección de un plan estratégico de turismo metropolitano. La coordinadora Ejecutiva del Ente, Alejandra Chena, indicó que «el turismo favorece el desarrollo de la economía regional y hay muchas localidades que aún no perciben su potencial en este sentido. En el marco de la pandemia, los consumos turísticos se modificaron, por lo cual debemos repensarnos como región».

En este sentido, resaltó la necesidad de «trabajar en redes que interconecten los servicios turísticos y construyan una oferta más atractiva, integral y regional. Hay una nueva tendencia de la demanda que cambiaría los patrones de consumo y se abre la posibilidad a algunos destinos emergentes que atenderán esta nueva demanda», mencionó.

Cabe recordar que el desarrollo turístico regional apunta a reconocer las múltiples oportunidades de cada zona, a partir de las características singulares de los territorios y las identidades de las comunidades que integran el área metropolitana. Así también se reconocen los aspectos del patrimonio natural, cultural e histórico de cada comunidad.

Chena sostuvo que para conseguir los objetivos «es necesario realizar una planificación estratégica y operativa, tomando en cuenta los principales elementos que hacen a la articulación del desarrollo territorial local, la vinculación, la gobernanza y el trabajo con los distintos actores. Se debe trabajar de manera planificada y ordenada, pero también ser dinámicos y adaptativos para garantizar la sostenibilidad de la actividad».

La secretaria detalló: «en 2021 continuaremos construyendo el plan estratégico de turismo. El mismo incluirá las tres microrregiones del área metropolitana: centro-oeste, centro-norte y la costa, para interrelacionar los destinos históricos y urbanos con los rurales y costeros. Debemos resaltar la belleza de esta región».

Makón es licenciado en Demografía y Turismo, y director del estudio Singerman y Makón. Durante su exposición, expresó que la mirada regional permite potenciar las capacidades de desarrollo del conjunto. En ese sentido, citó una encuesta de consumo y turismo realizada pospandemia, en la cual se refleja que el 75% de la población elige un destino interno, ante la posibilidad de viajar. Asimismo, **los consultados prefirieron el auto como medio de movilidad, el verano como período para vacacionar y los destinos que ofrecen mayor contacto con la naturaleza**.