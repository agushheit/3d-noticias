---
category: El Campo
date: 2020-12-17T10:50:06Z
thumbnail: https://assets.3dnoticias.com.ar/1712-agro.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La Provincia organiza una Jornada para fomentar el uso de tecnología en el
  agro
title: La Provincia organiza una Jornada para fomentar el uso de tecnología en el
  agro
entradilla: Los ministros Costamagna y Basterra participarán de la apertura.

---
Bajo el lema “Soluciones Tecnológicas para el agro”, se realizará este lunes la **Jornada AgTech Santa Fe 2020**, actividad impulsada por el Gobierno Provincial, a través del Ministerio de Producción, Ciencia y Tecnología en conjunto con el Ministerio de Agricultura, Ganadería y Pesca de la Nación e INTA y destinada a productores que busquen mejorar la toma de decisiones en los distintos sistemas productivos y la producción de alimentos de forma sostenible basándose en la ciencia, tecnología e innovación.

La apertura de la jornada será el **lunes 21 de diciembre a las 18 horas** y contará con la presencia del ministro de Agricultura, Ganadería y Pesca de la Nación, Luis Basterra y el ministro de Producción, Ciencia y Tecnología de Santa Fe, Daniel Costamagna.

El evento será transmitido en vivo desde el [canal de Jornadas Agtech](https://youtu.be/fmhoXURXTAM "Youtube"), no tiene costo y no requiere inscripción previa.

<br/>

***

## **PROGRAMA**

<br/>

<span style="color: #cf0000;">**Primer bloque**</span>

Agronomía digital, herramientas que simplifican la toma de decisiones para la producción integral. A cargo del Ing. Agr Fernando Salvagiotti (INTA Oliveros).

**Startup 1:** **Okaratech**. Desarrollo de una plataforma web y aplicaciones para dispositivos móviles para proveer soluciones integrales a las empresas agropecuarias.

**Startup 2:** **Sima**. Desarrollo de un sistema de agricultura inteligente para monitorear lotes, geolocalizar datos, analizar información y tomar decisiones de manejo.

**Startup 3: Inteliagro**. Gestión comercial inteligente.

Lanzamiento y promoción de proyectos de innovación tecnológica. A cargo del presidente de la Agencia Nacional de Promoción de la Investigación, el Desarrollo Tecnológico y la Innovación, Fernando Peirano.

<br/>

<span style="color: #cf0000;">**Segundo bloque**</span>

Agricultura de precisión como herramienta para recuperar la fertilidad de los suelos. A cargo de Sebastián Gambaudo (Docente Jubilado de la Facultad de Ciencias Agrarias-UNL). Investigador Jubilado de INTA

**Startup 1: Geoagro**. Criterios para reconocer, validar y caracterizar ambientes para una producción sustentable.

**Startup 2: Rastros**. Herramientas para incrementar la eficiencia en los procesos y optimización de recursos.

<br/>

<span style="color: #cf0000;">**Cierre de la jornada**</span>

Políticas públicas y tecnologías para el agro santafesino. 

A cargo del subsecretario de Agricultura. Ministerio de Agricultura, Ganadería y Pesca de la Nación, Delfo Buchaillot; el secretario de Agroalimentos, Ministerio de Producción, Ciencia y Tecnología de Santa Fe, Jorge Torelli; y la secretaría de Ciencia, Tecnología e Innovación. Ministerio de Producción, Ciencia y Tecnología de Santa Fe, Marina Baima.

***