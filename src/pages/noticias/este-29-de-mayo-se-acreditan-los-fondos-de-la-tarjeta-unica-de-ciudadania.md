---
category: Estado Real
date: 2021-05-29T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/targeta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Este 29 de mayo se acreditan los fondos de la Tarjeta Única de Ciudadanía
title: Este 29 de mayo se acreditan los fondos de la Tarjeta Única de Ciudadanía
entradilla: El Ministerio de Desarrollo Social, informa que, a partir del sábado 29
  de mayo, estarán acreditados los fondos de la Tarjeta Única de Ciudadanía, en todas
  las localidades de la provincia de Santa Fe.

---
Además, se acreditará un refuerzo de $700 para los beneficiarios que convivan en su grupo familiar con niños/as de hasta 12 años, y/o personas mayores a partir de los 65 años; llegando a $1.000.

**Componente Celiacos**

El 29 de mayo estarán acreditados los fondos de la Tarjeta Única de Ciudadanía, Componente Celíacos, correspondientes al mes de mayo, en todas las localidades de la provincia de Santa Fe.

Todos los beneficiarios con celiaquía, que no cuentan con cobertura de obra social, percibirán una acreditación de $3.000.

**Acreditación Virtual**

En tanto el lunes 31 de mayo, beneficiarios de las Tarjeta de Ciudadanía, de las localidades de Reconquista, Sunchales, Santa Fe y Venado Tuerto (excepto Componente Celíacos), que cuenten con la aplicación Plus Pagos, percibirán la acreditación del beneficio por este medio.

Es muy importante destacar, que contarán con un 30% de reintegro en la compra de los alimentos.

**REEMPADRONAMIENTO**

Cabe recordar que los titulares de la tarjeta que aún no realizaron el reempadronamiento, deben realizar el trámite como requisito para continuar percibiendo el beneficio, ingresando a: [https://mds.pluspagos.com/ministeriodedesarrollosocialSF](https://mds.pluspagos.com/ministeriodedesarrollosocialSF "https://mds.pluspagos.com/ministeriodedesarrollosocialSF")

También pueden comunicarse al 0800-345-5661, donde recibirán orientación para poder completar la gestión.

Asimismo, recordamos los derechos y obligaciones a las y los beneficiarios de la tarjeta:

<< Presentar DNI

<< No existe la obligación de gastar todo el monto en una sola compra y un solo comercio

<< Nadie puede retener la tarjeta

<< Debe exigirse el comprobarte de la compra con el saldo de la tarjeta

<< Puede realizarse la compra en cualquier comercio adherido a la red

<< El comerciante no puede recargar el precio por el uso de la tarjeta

Ante cualquier irregularidad, debe realizarse la denuncia en la comuna o municipio correspondiente y, también, en el Ministerio de Desarrollo Social de Santa Fe, comunicándose al teléfono (0342) 4579269 o a través de [www.santafe.gov.ar](http://www.santafe.gov.ar).