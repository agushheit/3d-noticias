---
category: Agenda Ciudadana
date: 2021-08-12T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/MONOTRIBUTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Créditos por hasta $150.000 para más de 1 millón de monotributistas de todo
  el país
title: Créditos por hasta $150.000 para más de 1 millón de monotributistas de todo
  el país
entradilla: El Gobierno nacional anunció la puesta en marcha de una nueva línea de
  créditos a tasa cero, que estarán garantizados por el Ministerio de Desarrollo Productivo
  y se tramitarán a través de la AFIP.

---
El Gobierno nacional anunció la puesta en marcha de una nueva línea de créditos a tasa cero por hasta $150.000 para más de un millón de monotributistas de todo el país, los que estarán garantizados por el Ministerio de Desarrollo Productivo y se tramitarán a través de la Administración Federal de Ingresos Públicos (AFIP).  
   
 El anuncio fue realizado por la vicejefa de Gabinete, Cecilia Todesca Bocco, y la titular de la AFIP, Mercedes Marco del Pont, en una conferencia de prensa que ofrecieron en Casa de Gobierno, tras la reunión de gabinete económico que esta tarde encabezó el presidente Alberto Fernández.  
   
 Todesca Bocco explicó que relanzar los créditos a tasa cero para monotributistas permitirá "fortalecer el respaldo a esos trabajadores tras la afectación de la pandemia", en un momento en que se observa una "recuperación de la economía, aunque con disparidades" de acuerdo a los sectores.  
   
 "Estamos repitiendo una experiencia exitosa, con un universo potencial de un millón de monotributistas; con un abordaje bastante progresivo de los montos", afirmó Marcó del Pont respecto al anuncio.  
   
 Los préstamos contarán con 6 meses de gracia y la devolución se realizará en 12 cuotas sin intereses. La posibilidad de solicitar un crédito a tasa cero estará habilitada a partir de fines de agosto a través de la web de la AFIP.  
   
 En esa instancia, explicó Del Pont, el sistema informará el monto máximo del crédito que depende de categoría de monotributo al 30 de junio, es decir antes de la recategorización, pero se podrá acceder a montos desde $90.000 hasta $150.000  
   
 En otro tramo de la conferencia, Todesca Bocco, afirmó que "la inflación se viene reduciendo lentamente, aunque más lento de lo que nos gustaría", por lo que la meta inicialmente propuestas para el corriente año "no es realizable".  
   
 "La gente que menos tiene es la que más sufre y por eso las políticas vienen reflejando incrementos en beneficiarios como en montos" de asistencia a los distintos sectores, reseñó la funcionaria al destacar el contexto luego de "tres años de caída consecutiva".  
   
 Por su parte, Marcó del Pont, aseguró que la Argentina está en "mejores condiciones de crecer a tasas mucho más altas, porque hay capacidades productivas que se preservaron y empleo se preservó"  
   
 "Estamos convencidos que hay un horizonte muy esperanzador para la economía argentina", aseguró la titular de la AFIP.

 **La reunión del Gabinete Económico**

Más temprano, el presidente Alberto Fernández encabezó en Casa de Gobierno la reunión semanal del gabinete económico, informaron fuentes oficiales.  
 Junto al Presidente participaron el jefe de Gabinete, Santiago Cafiero; la vicejefa de Gabinete, Cecilia Todesca; y los ministros de Economía, Martín Guzmán; de Desarrollo Productivo, Matías Kulfas; y de Trabajo, Claudio Moroni.  
 Además, estuvieron presentes el presidente del Banco Central, Miguel Pesce; y la directora de la Administración Federal de Ingresos Públicos (AFIP), Mercedes Marcó del Pont.