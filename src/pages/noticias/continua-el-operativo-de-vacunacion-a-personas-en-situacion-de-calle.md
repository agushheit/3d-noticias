---
category: La Ciudad
date: 2021-08-28T06:00:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNASCALLE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Continúa el operativo de vacunación a personas en situación de calle
title: Continúa el operativo de vacunación a personas en situación de calle
entradilla: 'El intendente Emilio Jatón recorrió algunos puntos de la ciudad donde
  se está inoculando a esa población contra el Covid.19. '

---
La Municipalidad, junto al Ministerio de Salud, garantiza este derecho, en el marco de su política de atención integral a personas en situación de calle.

La Municipalidad de Santa Fe continúa con el operativo de vacunación contra el Covid-19 a personas en situación de calle, en conjunto con el Ministerio de Salud para la colocación de las segundas dosis. En ese marco, el intendente Emilio Jatón recorrió anoche algunos puntos de la ciudad y conversó con las personas, mientras recibían las vacunas. Además, saludó a los trabajadores que tienen a cargo la tarea, en el marco del día del vacunador.

Hasta el momento se llevan inoculadas 46 personas en dos días, tanto en el CIC Roca -donde hay un refugio del municipio-, como en la Esquina Encendida, donde se los acompañó a vacunarse, o en los diferentes puntos de la ciudad donde pernoctan. Con la primera dosis, en tanto, habían sido vacunadas 86 personas en situación de calle de la capital provincial en el mes de julio.

En la recorrida de anoche, el director de Acción Social municipal, Gabriel Maurer, indicó que el objetivo es alcanzar a la totalidad de las personas inoculadas con la primera dosis y realizar el seguimiento de todas ellas y acompañar el proceso. Según dijo, “en el trabajo social y el acompañamiento que hace el equipo del municipio, vamos relevando el estado de salud y la forma en que las personas en situación de calle van circulando por el territorio. Con esa información vamos construyendo su trayectoria y eso permite saber si recibieron la vacuna”.

En ese sentido, agregó que la colocación de dosis contra el Covid-19 se enmarca “en las políticas de cuidado que llevamos adelante desde la Municipalidad. Nos parece fundamental acompañar a todos los vecinos y vecinas, especialmente a la población vulnerable, como una forma de estar más cerca de quienes más sufren”, concluyó.

Virginia Insaurralde, de la Dirección de Salud municipal, es una de las personas que acompañan el dispositivo. Según reseñó, “la idea, además de garantizar el derecho a la vacunación de todas las personas, es estar atento a cualquier posible efecto adverso que pueda surgir. Por ello les recordamos las medidas básicas de cuidado que venimos trabajando desde que inició la pandemia y, por supuesto, las recomendaciones básicas relacionadas con la aplicación de la vacuna y los posibles síntomas que puedan aparecer”.

Cabe recordar que la vacuna previene el contagio y los riesgos de infección y es fundamental que la población más vulnerable la reciba, por lo que el Estado está presente para garantizar ese derecho.