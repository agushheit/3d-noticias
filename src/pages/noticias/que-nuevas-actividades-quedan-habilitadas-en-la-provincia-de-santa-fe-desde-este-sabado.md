---
category: La Ciudad
date: 2021-08-01T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/SALONES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Qué nuevas actividades quedan habilitadas en la provincia de Santa Fe desde
  este sábado
title: Qué nuevas actividades quedan habilitadas en la provincia de Santa Fe desde
  este sábado
entradilla: Desde este sábado 31 de julio y hasta el 6 de agosto, nuevas actividades
  se suman a las ya habilitadas para desarrollarse en todo el territorio provincial
  en el marco de la pandemia por coronavirus.

---
La provincia, a través del Decreto N.º 1220 firmado por el gobernador Omar Perotti, dispuso medidas en el marco de la convivencia en pandemia, ante la segunda ola de coronavirus en todo el territorio provincial. Las mismas están vigentes desde el sábado 24 de julio y hasta el 6 de agosto de 2021 inclusive., pero desde este sábado 31 de julio se suman nuevas actividades a las ya vigentes: la apertura de salones de eventos y competencias deportivas.

La actividad de los salones de eventos, fiestas y similares, para la realización de eventos sociales, con un factor de ocupación de la superficie disponible para el público que no supere el 50% y que asegure el distanciamiento entre personas y un espacio de una persona cada dos metros cuadrados de espacio circulable; sin actividades que signifiquen baile u otras que impliquen circulación de los asistentes entre las mesas o ubicaciones dispuesta s.

También se definió que desde este sábado se podrá ampliar hasta el 50% el uso de la superficie de los cines, teatros, salas culturales y espacios religiosos, siempre garantizando el distanciamiento de una persona cada 2 metros cuadrados.

En lo que refiere a competencias deportivas:

Por otra parte, el gobierno de la provincia autoriza en todo el territorio provincial, a partir de las 0 del día 31 de julio de 2021 y hasta el día 6 de agosto de 2021 inclusive, las competencias deportivas provinciales, zonales o locales del carácter profesional o amateur, incluidas las automovilísticas y motociclistas, que se desarrollen en espacios abiertos al aire libre o cubiertos con suficiente ventilación, sin concurrencia de espectadores, en ningún caso.

La autorización dispuesta queda sujeta al estricto cumplimiento de las reglas generales de conducta, los protocolos específicos aprobados para cada actividad deportiva y a las siguientes determinaciones:

Realización de las actividades en espacios habilitados por las autoridades locales y, en su caso, por las entidades que fiscalizan la actividad deportiva correspondiente.

Disponiendo de distintos lugares para el ingreso y egreso de los participantes, evitando la congestión de personas.

Manteniendo constante la provisión en los sanitarios de dispensadores de alcohol en gel o solución de alcohol al 70 %, jabón líquido y papel desechable; los que deberán ser higienizados y sanitizados adecuadamente antes y durante la realización de la actividad.

Sin la realización de reuniones de personas antes o después de las actividades deportivas.