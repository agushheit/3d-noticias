---
category: Agenda Ciudadana
date: 2021-10-31T06:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/previaje.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Previaje: las ventas se acercaron a niveles prepandemia'
title: 'Previaje: las ventas se acercaron a niveles prepandemia'
entradilla: Este domingo finaliza el plazo para cargar los comprobantes de quienes
  viajan en enero. Por la cantidad de trámites, la página de Afip registró inconvenientes.

---
Este domingo finalizó el plazo para cargar comprobantes del programa "Previaje" para quienes optaron por vacacionar en enero. En este marco, las consultas y las ventas, que venían creciendo en el último tiempo, explotaron sobre el cierre de esta semana.

La primera parte de la segunda edición del programa (quienes quieran viajar a partir de febrero tienen tiempo hasta fin de año) dejó satisfechos a los operadores de turismo de Santa Fe, quienes hablan de ventas que se acercan a los niveles prepandemia.

Fabricio Meglio, Vicepresidente de la Asociación Santafesina y Entrerriana de Agencias de Viajes y Turismo comentó que "algunos prestadores de servicios mandaron notificaciones advirtiendo de que el viernes iba a ser el último día de facturación porque ya no iba a ser posible facturar los servicios".

En esa línea, indicó que el jueves colapso la página de Afip a través de la cual se realiza la facturación online. "Ha sido una semanita bastante demandada por los clientes", graficó.

Consultado sobre las sensaciones que está dejando en el sector, Meglio destacó: "Cualquier movimiento generado iba a ser positivo. Esta herramienta dio el empuje; ha generado muy buenos resultados y va a seguir generando durante todo el año. A diferencia de la edición anterior, se pueden planificar viajes durante todo el 2.022".

"Estamos sorprendidos gratamente por el volumen de demanda", deslizó. Comentó que se dieron "muchas variables" para alcanzar este nivel de ventas: "Primero el deseo y la necesidad de viajar, también las perspectivas más claras de que se podrá viajar, hay más servicios, hay cierta normalidad en la conectividad; con aforos que han ampliado. Entonces, poco a poco, se va regenerando ese volumen turístico".

Con respecto a si las ventas se acercan a los niveles prepandemia, el referente de las agencias de viaje dijo: "Si se analizan los servicios vigentes y la demanda actual, son porcentajes superiores; no en volúmenes, sino en porcentaje de ocupación y contratación de servicios. De hecho, ayer surgió que una empresa lowcost, compañía aérea, superó los índices prepandemia de venta. O sea, en su historia nunca había tenido los niveles de venta que hoy tiene proyectado durante toda esta temporada".

Insistió que a diferencia de la primera edición del Previaje, hoy existe "una mayor previsibilidad; el año pasado la previsibilidad no superaba los siete días; no había vacunas"

Con relación a los destinos más elegidos, describió: "Se ha ido ampliando cada vez más. Obviamente el sur, con Bariloche, principalmente; estos últimos meses Puerto Madryn también tuvo una demanda muy fuerte. Y la regularidad de lo que es Ushuaia y Calafate. Ya en servicio terrestre, se destacan Salta, Jujuy; Cataratas; gente que lo hace con sus vehículos o salidas grupales. Hay una movilización muy grande de turismo en forma autónoma, con sus propios vehículos". Al mismo tiempo destacó Córdoba como uno de los destinos más elegidos en la región.