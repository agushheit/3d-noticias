---
category: La Ciudad
date: 2021-09-29T06:00:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/construccion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Concejo aprobó el código que fija las nuevas pautas constructivas en Santa
  Fe
title: El Concejo aprobó el código que fija las nuevas pautas constructivas en Santa
  Fe
entradilla: 'La norma regirá para cada intervención edilicia (individual o colectiva)
  que se realice en esta capital. '

---
En una sesión que "quedará en la historia", según la calificación de dos ediles oficialistas, el Concejo de Santa Fe sancionó por unanimidad el nuevo Código de Habitabilidad, que era el Mensaje Nº 3 remitido por el intendente Emilio Jatón a fines de mayo. La norma reemplazará al Reglamento de Edificaciones, vigente desde hace 45 años.

¿En qué consiste esta ordenanza, que tiene 290 artículos? En términos simples: regula cada paso administrativo que se da desde que un particular toma contacto con un arquitecto para construir o realizar modificaciones edilicias (o bien un desarrollador que decide iniciar un emprendimiento inmobiliario), con los plazos en los permisos de obra, los certificados a presentar, el contacto con el área de Edificaciones Privadas, hasta las autorizaciones y requisitos a presentar, entre muchos otros puntos.

Es un plexo normativo que establece pautas para edificar (obra privadas y públicas) en el ejido urbano de esta capital de aquí a tres décadas: desde las dimensiones permitidas para espacios en viviendas de uso residencial, sus escaleras, patios y vías de accesibilidad; los balcones permitidos con salida a fachadas, las terrazas y jardines verdes, y los requerimientos a certificar para la eficiencia energética en edificios.

En la ordenanza trabajaron funcionarios del área de Planeamiento Urbano del municipio, los propios concejales y sus equipos técnicos; el Colegio de Arquitectos, profesionales desarrolladores independientes, expertos de las universidades locales e incluso la Cámara de la Construcción, además de organizaciones sociales involucradas.

**Obras por escala**

La nueva norma clasifica las obras según su escala, punto que no estaba taxativamente aclarado en el viejo Reglamento. Así, éstas se dividirían en "Obra Menor" (obras nuevas o de demolición de hasta 200 m2 de superficie cubierta y semicubierta de Planta Baja y un piso, o modificaciones y/o ampliaciones sobre preexistencias (…); "Obra Media" (obras nuevas o de demolición de entre 200 m2 y 1.500 m2 de superficie cubierta y semicubierta o hasta 12 pisos, o modificaciones y/o ampliaciones sobre preexistencias (…)".

Se define también a la "Obra Mayor" como las "obras nuevas o de demolición de entre 1.500 m2 y 3.500 m2 de superficie cubierta y semicubierta o hasta 12 pisos, modificaciones y/o ampliaciones sobre preexistencias"; y la "Obra de Gran Escala" (obras nuevas o demolición de entre 3.500 m2 y 10.000 m2 de superficie cubierta y semicubierta o hasta 24 pisos, reformas o ampliaciones sobre preexistencias (...)".

Se añade además otra tipología: la "Obra de Magnitud Especial", o mega construcción (obras nuevas o de demolición superiores a 10.000 m2 de superficie cubierta y semicubierta o de más de 24 pisos, y/o ampliaciones sobre preexistencias en las que resulte una superficie cubierta y semicubierta total de más de 10.000 m2 o más de 24 pisos". Habrá requisitos constructivos especiales según cada escala de obra y sus particularidades.

**Plan a futuro**

"Esta norma se integra al plan urbano de la Santa Fe del futuro, porque sienta las bases de la ciudad que elegimos habitar", valoró Lucas Simoniello (FPCyS), uno de los concejales que más trabajó este tema en todo el proceso. Sin esta planificación, "nos quedaríamos viviendo en una ciudad que sólo priorice el desarrollo inmobiliario y homogéneo, por sobre lo ambiental y lo heterogéneo".

El código abarcará todas y cada una de las intervenciones edilicias que se realicen tanto en las parcelas privadas como en el espacio público, recordó el edil, y citó algunos ejes clave -según su criterio-, que son "el andamiaje" de la ordenanza: "Primero, la generación de reglas claras. Esto es, que una familia que accede a un crédito para su primera vivienda y necesita en 150 días tener aprobados los planos, que los pueda tener; pero también para darle previsibilidad a las empresas que deciden construir a gran escala y traerán inversiones a la ciudad, motorizando la economía local".

También citó la fijación de estándares constructivos "mucho más seguros y habitables desde el punto de vista estructural (la calidad de los materiales constructivos)"; la promoción de espacios accesibles en ascensores y escaleras para el desplazamiento autónomo de las personas; la movilidad sustentable, con más espacios para guardas para bicicletas, y planes de contingencias en edificios. Y finalmente la dimensión mediambiental, con el mantenimiento de la infraestructura verde en veredas, la eficiencia energética, entre otros puntos.

**"Paradigma"**

"Creo que esto implica un cambio de paradigma en la manera de concebir la ciudad a futuro. Es cierto que habrá cuestiones no previstas a subsanar, pero éste es un gran paso, pero la norma trata de dar celeridad en las tramitaciones", declaró Carlos Suárez (UCR-Juntos por el Cambio). "Se avanzó en cubiertas verdes y jardines verticales, por ejemplo, que mejoran la calidad medioambiental en la ciudad. Habría que buscar incentivos para que esto se aplique de hecho, sugirió. "Es la mejor norma que podemos tener en este momento".

La edila oficialista Laura Mondino celebró la unanimidad en la aprobación de la norma. "La ciudad demandaba estas nuevas pautas de edificabilidad, porque las que hoy existían quedaron totalmente desactualizadas. Y con los cambios actuales, logramos una norma ágil y moderna, pensada para la ciudad que se construirá de aquí a los próximos 30 años, y que incorpora el paradigma del cuidado de las personas, del medioambiente y la sustentabilidad".

**Eficiencia y terrazas verdes**

La norma promueve la utilización de la Certificación de Etiquetado de Eficiencia Energética como "un instrumento que brinda información acerca de las prestaciones energéticas de una vivienda". Las personas que acrediten derecho a solicitar permiso de obra "podrán presentar dicha Certificación conforme a la normativa provincial, y la documentación técnica correspondiente al momento de solicitar un permiso de obra, ampliación y/o reforma".

Asimismo, la determinación de los parámetros de eficiencia energética y confort higrotérmico en este tipo de obras será establecido por el Ejecutivo Municipal. A tales efectos, la Comisión Municipal de Habitabilidad (que se crea para controlar el cumplimiento del Código) propondrá criterios en base a las normas IRAM correspondientes, con relación "a innovaciones sobre el comportamiento higrotérmico y eficiencia energética en las construcciones".

En toda obra nueva o ampliación será obligatoria la incorporación de retardadores pluviales (esto se mantiene, ya que rige en la actualidad). Y para obras nuevas o ampliaciones se podrán incorporar cubiertas verdes. Las "Terrazas Verdes o Cubiertas Naturadas" son "sistemas constructivos que posibilitan el crecimiento de vegetación en la parte superior de los techos o azoteas de los edificios (…), capturando el agua de lluvia, mejorando la aislación térmica de los edificios, produciendo ahorro en el consumo de energía, mejorando la calidad del hábitat", agrega el texto sancionado.