---
category: Estado Real
date: 2021-04-25T06:00:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/BIOGAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia presentó líneas de financiamiento en energías renovables para
  el sector productivo
title: La Provincia presentó líneas de financiamiento en energías renovables para
  el sector productivo
entradilla: La iniciativa forma parte del plan de transición energética santafesina
  con el objetivo de incluir a todos los sectores interesados.

---
El Gobierno de Santa Fe, a través de los ministerios de Ambiente y Cambio Climático y de Producción, Ciencia y Tecnología, presentó líneas de financiamiento para la incorporación de energías renovables para la producción ganadera e industrias asociadas. Además, el lanzamiento dio lugar a un ciclo de charlas donde se desarrollan experiencias exitosas al respecto.

La apertura virtual estuvo a cargo de la ministra de Ambiente y Cambio Climático, Erika Gonnet; el ministro de Producción, Ciencia y Tecnología, Daniel Costamagna; el director de la Empresa Provincial de la Energía, Mauricio Caussi; y el gerente de la Asociación Unión de Tamberos, Luis Zurverra.

“Este esquema que tenemos planteado por una iniciativa del gobernador Omar Perotti es avanzar en la transición energética de la provincia incorporando a su vez criterios de economía circular”, resaltó la ministra de Ambiente y Cambio Climático, Erika Gonnet ante los más de 150 participantes. “Ya estamos trabajando con el Programa ERA (Energía Renovable para el Ambiente), la línea de fomento del uso de energías renovables distribuida con el objetivo de llegar a distintos sectores interesados”, continuó.

Y explicó que por ejemplo en el sector tambo “la provincia tiene más de 3500 tambos y el 75 % de ellos no hacen tratamiento de efluentes. Son cosas que podemos cambiar en conjunto, brindando herramientas, acercándonos, asesorando. No hay otra forma de discutir cuestiones ambientales que no sea todos es una misma mesa”.

A su turno, el ministro Costamagna afirmó: “Tenemos desafíos muy importantes para la lechería santafesina, un sector que tiene que ver con el arraigo, con el territorio y con nuestra historia”.

“En la sensibilidad y pertenencia de este Gobierno está el compromiso de trabajar para mejorar la calidad de vida de los que viven de esta actividad. Detrás de toda empresa lechera hay familias que hacen mucho sacrificio a diario. La lechería es parte de las políticas centrales que tiene este Gobierno; por eso avanzamos en programas como caminos de la ruralidad, la mejora en tendidos eléctricos, la conectividad, con la infraestructura, la forestación y el cuidado de los recursos naturales, nuestro suelo es un recurso invaluable que debemos cuidar. Estas jornadas van en ese sentido, capacitación para crecer. Un punto que se abordará es el tratamiento de efluentes, es un tema central, no sólo por el cuidado del medio ambiente sino por la generación de economía circular”, agregó.

Finalmente, el ministro manifestó: “Si pensamos en la lechería de antes y visualizamos hoy al sector, vemos tecnología, innovación y avances. Lo que queda por hacer es generar una ecuación ganar-ganar, a la cual llegaremos si sumamos un mejoramiento en la calidad de la leche, transparencia en el sistema comercial y mejoramiento en la logística”.

Además, participaron el subsecretario de Tecnologías para la Sostenibilidad, Franco Blatter; el director de Producción Láctea y Apícola, Abel Zenklusen; Oscar Fernández en representación del Consejo Federal de Inversiones; y Estefanía Varaldo del Cluster Lechero Regional.

“El CFI (Consejo Federal de Inversiones) financia proyectos, no inversiones puntuales, proyectos nuevos o existentes con buena tasa de interés y plazos de devolución. Va a las pequeñas empresas productivas y de servicios a la producción o de servicios que desarrollen actividad económicamente rentable”, explicó Oscar Fernández.

**Fomento a la energía solar, biogás y gestión de efluentes**

Más adelante, Franco Blatter, subsecretario de Tecnologías para la Sostenibilidad, destacó que “para ser protagonistas del mapa energético, Santa Fe debe propiciar los esquemas necesarios para producir energía aquí, con nuestros recursos, nuestro sol, nuestra biomasa, transformando efluentes en energía. Por ejemplo, el 80 % del biodiesel de la argentina, se produce en Santa Fe, generando empleo y valor agregado, debemos seguir trabajando en esta transición”.

Abel Zenklusen, por su parte, celebró el trabajo interministerial y la posibilidad de dirigir temas de energías a actividades lácteas y porcinas. “Santa Fe es una de las provincias con más tambos chicos en el país”, focalizó.

“Invertir en energía renovables requiere de financiamiento. La actividad tambera se encuentra en una transición, el tambo se convierte y permite el trabajo a mayor escala, por ende, la energía se convierte en un costo mayor. Es un buen momento para que el tambo sea más sustentable y se mantenga”, finalizó.