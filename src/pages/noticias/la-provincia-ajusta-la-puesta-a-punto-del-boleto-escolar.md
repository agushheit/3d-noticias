---
category: Agenda Ciudadana
date: 2021-02-15T06:30:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/boletoescolar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia ajusta la puesta a punto del Boleto escolar
title: La Provincia ajusta la puesta a punto del Boleto escolar
entradilla: Se trata de una serie de bonificaciones en el costo del transporte para
  estudiantes, personal docente, administrativo y de servicio. Se debe reunir una
  serie de condiciones y la solicitud se hace de manera online.

---
El Gobierno de la Provincia ajusta los detalles para relanzar el boleto educativo. La herramienta permitirá viajar desde el domicilio del beneficiario hasta el establecimiento educativo en el cuál trabaja o asiste a clases, pagando una fracción del costo del boleto. Se puede consultar las características de la ayuda en la web oficial www.santafe.gov.ar/boletoeducativo

Según las condiciones del solicitante, podrá tener un descuento del 50% en todos los pasajes o una bonificación del 100% de su valor para dos pasajes mensuales (distancias mayores de 60 km) o dos diarios (distancias menores de 60 km), cuando se encuadre en el Boleto Educativo Gratuito.

Serán beneficiarios del Medio Boleto Estudiantil (50% de descuento) los estudiantes regulares de los niveles secundarios, terciarios y universitarios que viven en una localidad diferente a la que estudian y que utilicen el transporte interurbano de pasajeros para trasladarse a los establecimientos educativos dentro del territorio de la Provincia de Santa Fe.

Accederán a su vez ese 50% de descuento el personal docente, administrativo y de servicio de los establecimientos educativos públicos y privados reconocidos, en los niveles Primarios, pre-primarios, Especiales, Medios, Técnicos y Superior, para el traslado entre el lugar de residencia y el establecimiento educativo donde preste servicios. También se benefician los alumnos primarios, de 5 a 12 años de edad.

Por otra parte, gozarán del Boleto Educativo Gratuito (100% de descuento) los alumnos de todos los niveles educativos de las escuelas provinciales públicas y privadas, a los alumnos universitarios, al personal docente, administrativo y de servicio de los establecimientos educativos públicos y privados reconocidos, en los niveles Primarios, pre-primarios, Especiales, Medios, Técnicos y Superior, para el traslado entre el lugar de residencia y el establecimiento educativo donde asistan.

**¿Cómo se solicita el beneficio?**

Todos los datos que se brinden al momento de solicitar el beneficio tienen carácter de Declaración Jurada, por lo que cuando los beneficiarios sean menores de edad, el trámite lo deberá hacer un mayor de edad a cargo.

**Para generar la solicitud, deberán seguirse los siguientes pasos**

Crear un usuario para ingresar a la aplicación o al trámite web www.santafe.gov.ar/boletoeducativo

Completar en el formulario todos los datos personales y datos de la institución educativa (la aprobación dependerá de poder verificar la regularidad / relación laboral)

Una vez que se corroboren los datos, se aprueben los recorridos y el establecimiento educativo informe la regularidad / relación laboral, se puede empezar a gozar del beneficio.

**Boleto Educativo Rural**

Por otro lado, también se lanzará el Boleto educativo rural. Este beneficio permitirá viajar al alumno/a, docente o asistente escolar hasta el establecimiento educativo rural en el cual trabaja o asiste a clases diariamente en un medio de transporte acordado con el Municipio o Comuna.

**¿Quiénes podrán acceder al beneficio?**

Alumnos/as regulares, Docentes o Asistentes Escolares de un Establecimiento Educativo de los niveles inicial, primario, secundario o superior en cualquiera de sus modalidades o de Educación Especial, que esté situado en Ámbito Rural. Pertenecer a un grupo familiar radicado en la provincia cuyos ingresos económicos totales sean menores a dos (2) canastas básicas totales para un grupo familiar de cuatro integrantes. Tener domicilio real a más de 5 km del establecimiento al cual asiste.

**¿Cómo se solicitará el beneficio?**

Completando el formulario disponible desde: www.santafe.gov.ar/boletoeducativorural

En el mismo deberán consignar todos los datos personales y datos de la institución educativa (la aprobación dependerá de poder verificar la regularidad / relación laboral).

La información será corroborada por el establecimiento educativo y posteriormente el trayecto diario a ser realizado será coordinado con el Municipio o Comuna donde esté ubicado el mencionado establecimiento con el medio de transporte adecuado para el recorrido aprobado.

**¿Dónde puedo consultar por cualquier inconveniente con la solicitud?**

Por cualquier dificultad para realizar la solicitud pueden enviarla por correo electrónico a: boletoeducativorural@santafe.gov.ar