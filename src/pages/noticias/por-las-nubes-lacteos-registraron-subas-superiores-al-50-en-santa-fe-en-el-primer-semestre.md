---
category: Agenda Ciudadana
date: 2021-07-26T06:15:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/LACTEOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Por las nubes: lácteos registraron subas superiores al 50% en Santa Fe en
  el primer semestre'
title: 'Por las nubes: lácteos registraron subas superiores al 50% en Santa Fe en
  el primer semestre'
entradilla: Lo encabezó la manteca, con un aumento de casi el 55%. El litro de leche
  subió más de un 48 por ciento. Los tamberos insisten con que el gobierno regule
  la cadena productiva.

---
Datos recabados por UNO Santa Fe revelan que los lácteos registraron un incremento promedio superior al 45 por ciento durante el primer semestre.

Quien encabeza el podio es la manteca con una suba del 54,59 por ciento en los primeros seis meses del año. En diciembre del año pasado, 150g de manteca costaba $97,54; seis meses después el mismo producto cuesta $150,79. Le siguió el queso sardo con un aumento del 54,1 por ciento.

El tercer lugar fue para el litro de "leche fresca entera sachet" con un crecimiento del 48,51 por ciento. Hace seis meses costaba 67,06 y ahora sale $99,59. Más atrás se ubicaron el dulce de leche y el queso crema, ambos productos con aumentos del 47,51 por ciento.

El informe fue realizado en base al Índice de Precios al Consumidor (IPC) que elabora y publica el Instituto Provincial de Estadísticas y Censos de la provincia (Ipec)

A nivel general, la inflación en lo que va del año alcanzó en Santa Fe el 26,5 por ciento y estuvo condicionada por los aumentos en el rubro "alimentos y bebidas", que tuvo una suba del 29 por ciento. En este último segmento es que los lácteos se impusieron sobre el resto con fuertes incrementos.

"¿Es caro el litro de Leche?", le preguntó UNO Santa Fe a Marcelo Aimaro, titular de la Mesa de Productores Lácteos de Santa Fe; a lo que respondió: "Para el productor es el más barato del mundo pero para el asalariado es caro".

Reconoció que todos los años el consumo de lácteos viene cayendo en el país, y graficó: "De 215 litros que tuvimos por el 2.010 y 2.011, hoy estemos en 190 litros".

El referente de los productores informó que el aumento en el precio de la leche al Productor, durante los primeros seis meses del año, no superó el 35 por ciento.

Comentó con relación a la suba de los productos en góndola que el problema es de fondo y que "los productores no son justamente los que salen ganando". En esa línea, continuó: "Venimos reclamando a los sucesivos gobiernos que nos sentemos en una mesa con todos los papeles".

Y se preguntó: "¿Por qué un queso en la góndola vale 800 mangos y a mí me se me paga 30 pesos el litro de leche?". En ese sentido, explicó: "Para hacer un kilo de queso se necesita, en promedio (según el queso) 10 litros de leche. Entonces, no puede ser que un queso valga 800 o mil pesos en la góndola y al productor le diste 300".

Advirtió que el productor es quien "pone más en juego, porque tienen que poner el campo, la vaca, la fuerza de trabajo; es el que más arriesga y el que menos se lleva".

Destacó que el tambero no se puede guardar la leche y la tiene que vender: "Todos ponen sus ganancias en la cadena, menos nosotros. La industria pone sus costos y sus ganancias, el comercio también; y los únicos que no lo podemos hacer somos nosotros".

Propuso revisar el aumento que tuvieron los lácteos durante el 2.020, y comentó: "El gobierno, con el afán que no no haya aumentos, mantuvo a los precios en góndola quietos (durante el 2.020). Cuando se permitió el incremento, pegaron un sacudón en las góndolas".

Insistió con la necesidad de que el gobierno regule el sector. "No pedimos una intervención, lo que decimos es que el estado sea el árbitro y que nos sentemos todos en una mesa a discutir estos temas; algo que jamás se logró. No hay voluntad política del gobierno de regular la cadena, de sentarnos a todos en una mesa".

Reconoció que la industria láctea es un jugador importante, pero pidió no olvidar a las grandes comercializadoras, las cuales "son formadoras de precios". Al mismo tiempo, dijo que el gobierno podría hacer un esfuerzo, ya que también es otro participante de la cadena, y disparó: "Los lácteos no tienen menos de un 40 por ciento de impuestos".

Según comentó, muchos productores -a pesar de la suba del 35 por ciento por litro de leche en lo que va del año- "no alcanzan a cubrir el costo o apenas llegan". Y precisó: "Se dispararon insumos que son básicos en el campo, como maíz y soja. Hay otros costos que estan dolarizados y subieron terriblemente; una goma de tractor que valía 150 mil pesos, hoy vale 350 mil pesos. Los monopolios se llevan la renta nuestra".