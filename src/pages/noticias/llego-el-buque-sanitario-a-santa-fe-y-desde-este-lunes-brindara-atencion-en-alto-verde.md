---
category: La Ciudad
date: 2021-07-19T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/BARCO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Llegó el buque sanitario a Santa Fe y desde este lunes brindará atención
  en Alto Verde
title: Llegó el buque sanitario a Santa Fe y desde este lunes brindará atención en
  Alto Verde
entradilla: El operativo forma parte de una Campaña Fluvial Socio Sanitaria nacional.
  Se quedará hasta el jueves 22 y además de salud, brindará otros servicios.

---
La Campaña Fluvial Socio Sanitaria 2021 llegará a la ciudad de Santa Fe este lunes y permanecerá hasta el jueves 22 para desplegarse en el barrio de Alto Verde.

El operativo es conducido por el Ministerio de Defensa de la Nación, Este domingo, cerca del medidía, amarró en el Dique 1 del Puerto de Santa Fe (zona del shopping La Ribera) el buque multipropósito ARA “Ciudad de Rosario” y el patrullero ARA “King” de la Armada Argentina.

El objetivo primordial de la campaña socio sanitaria es el de brindar atención y prevención primaria de la salud a los y las habitantes de las localidades que por su ubicación geográfica presentan dificultades de acceder por vía terrestre a los centros médicos más cercanos.

Es organizada a través de la Secretaría de Coordinación Militar en Emergencias. Se centrará en aquellas poblaciones vulnerables para brindar atención territorial con el fin de mejorar su calidad de vida. El recorrido que realizará la campaña recorre las provincias de Entre Ríos y Santa Fe a lo largo del Río Paraná.

En ese sentido, permanecerá hasta el 22 de julio en la zona, para luego continuar con el siguiente itinerario

\- Isla del Espinillo, frente a la ciudad de Rosario (25 y 26 de julio)

\- Isla Ibicuy, Entre Ríos (29 y 30 de julio)

\- Villa Paranacito, Entre Ríos (2 y 3 de agosto)

**Trabajo interagencial del Estado**

La campaña que cuenta con el trabajo conjunto de la Secretaría de Coordinación Militar en Emergencias, la Armada Argentina, la Dirección de Asistencia en Emergencias (DIMAE) y el área de Sanidad Militar, contará con la prestación de otros servicios, dependientes de las siguientes agencias del Estado:

· Ministerio de Salud de la Nación, a través de la Secretaría de Acceso a la Salud.

· Ministerio de Desarrollo Social de la Nación, a través de la Secretaría de Articulación de Política Social.

· Ministerio del Interior de la Nación, a través de la Secretaría de Municipios.

· Instituto Nacional de Servicios Sociales para Jubilados y Pensionados (PAMI).

· Agencia Nacional de Discapacidad (ANDIS).

· Registro Nacional de las Personas (RENAPER).

· Administración Nacional de la Seguridad Social (ANSES).