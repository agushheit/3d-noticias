---
category: Estado Real
date: 2021-05-02T06:15:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTI.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti: "Voy a tomar las decisiones que haya que tomar, buscando el diálogo,
  el consenso y la unidad de todos los santafesinos"'
title: 'Perotti: "Voy a tomar las decisiones que haya que tomar, buscando el diálogo,
  el consenso y la unidad de todos los santafesinos"'
entradilla: Lo aseguró el gobernador en la inauguración del 139° período de sesiones
  ordinarias, donde saludó a las y los trabajadores en su día y deseó “una pronta
  recuperación” a Miguel Lifschitz.

---
En el marco de la inauguración del 139° período de sesiones ordinarias de la Legislatura provincial, el gobernador Omar Perotti repasó los ejes de su gestión, saludó a las y los trabajadores en su día y deseó “una pronta recuperación” al presidente de la Cámara de Diputados, Miguel Lifschitz. Y remarcó: “Voy a tomar las decisiones que haya que tomar, buscando el diálogo, el consenso y la unidad de todos los santafesinos”.

“Es un orgullo enorme repasar lo actuado en el año 2020, rendirles cuentas y mirar hacia adelante, porque tenemos mucho para decir, mucho por hacer, y seguir avanzando en la consolidación de una provincia cada vez más fuerte”, comenzó el titular de la Casa Gris, al tiempo que saludó “especialmente a las trabajadoras y trabajadores en su día; es mi intención dejarles un mensaje de aliento y esperanza a aquellos que hoy no lo tienen”.

A continuación, Perotti dedicó un párrafo a su antecesor: “Al ingeniero Miguel Lifschitz, un luchador que le está dando pelea al Covid, quiero desearle, fervientemente, una pronta mejoría y recuperación”.

Y agregó: “No hay día en que este gobernador no le dedique todas las fuerzas para que, cuidando de la vida de las personas, podamos seguir avanzando en un modelo productivo que genere oportunidades para todo aquel que quiera trabajar, producir e invertir en nuestra provincia. Las crisis, muchas veces, son oportunidades para acelerar cambios necesarios y postergados. Así como la pandemia desnudó falencias en materia de salud e infraestructura, no cabe duda de que también lo hizo en materia de conectividad”.

En esa línea, el mandatario solicitó a los legisladores “que nos acompañen; que la Cámara de Diputados acompañe al Senado y, cuanto antes, apruebe el Proyecto de Conectividad para toda la provincia. Tenemos que reducir la brecha tecnológica existente entre alumnos conectados y no conectados; tenemos que llegar con fibra óptica a los 365 pueblos y ciudades. No hay justicia si no hay igualdad de oportunidades para todos”.

Por otro lado, recordó que “hoy tenemos una franca recuperación de todos los sectores productivos. Los indicadores del último febrero superan largamente a los de febrero del 2020. Se está viendo reflejado en los procesos de inversión y en el crecimiento de los puestos de trabajo registrados”.

“Nuestro reto -argumentó- es concretar acciones de fondo que permanezcan en el tiempo; sentar los mojones para trazar políticas públicas de largo plazo, la Santa Fe del futuro: una provincia que no deje de crecer, una Santa Fe que será el corazón productivo de la Argentina”.

**Propuestas de gestión**

En otro tramo de su discurso, el gobernador remarcó que “propusimos leyes para modernizar el obsoleto sistema de seguridad pública provincial y marcar una línea infranqueable que divida a las instituciones estatales y la legalidad, de un lado, y el mundo delictivo, del otro”. Y puntualizó: “Seguir con la tarea de cortar los vínculos con el delito”.

Además, dijo que “lanzamos el Boleto Educativo, porque no hay progreso si no se invierte en el presente, en los jóvenes. Y para aumentar el poder de compra de la familia, lanzamos el programa Billetera Santa Fe”.

“Todas estas iniciativas -explicó- buscan trascender un período de gestión para convertirse en políticas de Estado y derechos permanentes para los santafesinos y santafesinas. Es una tarea para todos los santafesinos, no importa cómo piensen, ni su condición política o social: busquemos las coincidencias y corramos las diferencias”.

Y sentenció: “Voy a tomar las decisiones que haya que tomar, por más difíciles que sean, siempre buscando el diálogo, el consenso y la unidad de todos los santafesinos”.

**Ejes de gestión**

A continuación, Perotti repasó los ejes de su gestión, entre ellos, en materia de Salud; Producción, trabajo y economía; Fortalecimiento de las instancias locales para el arraigo; Conectividad, Educación y Conocimiento; Seguridad; Género; Cultura; y Ambiente.

“Debemos asegurarnos qué las posibilidades estén en cada rincón de la provincia, que cada santafesina y santafesino tengan oportunidades sin importar donde hayan nacido”, indicó el gobernador, y puntualizó: “Ustedes saben que toda mi vida peleé para poner a la producción, la educación y el trabajo como los grandes motores del crecimiento y el desarrollo”.

En ese sentido, afirmó que “estoy trabajando fuertemente porque sé que la sociedad espera que sea el gobernador del trabajo, la producción, la educación y la unión de los santafesinos”.

“Espero que seamos recordados como aquellos a los que les tocó legislar, administrar justicia y, en mi caso, gobernar, luchando contra el Covid. Que esta nueva conciencia sobre nuestra fragilidad nos ayude a humanizar la política y a encontrar los acuerdos que nuestro pueblo merece y espera”, agregó.

Y concluyó: “Como dijo Adolfo Pérez Esquivel, ‘los puños cerrados no siembran’. No tengo dudas que, entre todos, con el esfuerzo de cada uno de los santafesinos y santafesinas, vamos a demostrar, una vez más, por qué somos la provincia invencible de Santa Fe”.