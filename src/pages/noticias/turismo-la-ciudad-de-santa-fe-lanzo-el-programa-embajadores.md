---
category: La Ciudad
date: 2021-09-28T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/EMBAJADORES1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Turismo: la ciudad de Santa Fe lanzó el Programa Embajadores'
title: 'Turismo: la ciudad de Santa Fe lanzó el Programa Embajadores'
entradilla: En el marco del plan de reactivación de la actividad turística la Municipalidad
  y el Bureau de Eventos de Santa Fe de la Vera Cruz pusieron en marcha el Programa
  Embajadores.

---
La Municipalidad de Santa Fe apuesta a la reactivación del sector turístico pospandemia a través de acciones con diferentes grados de alcance. En esta oportunidad se presentó el Programa Embajadores “Turismo de Reuniones”, una iniciativa coordinada en conjunto con el Bureau de Eventos de Santa Fe de la Vera Cruz, y que apunta a reactivar y fortalecer la industria de eventos en la ciudad de Santa Fe. Fue esta mañana en el Auditorio Federal del Museo de la Constitución con una jornada que contó con la participación de profesionales especializados en el segmento.

Matías Schmüth, secretario de Producción y Desarrollo Económico de la Municipalidad, destacó que el programa de Embajadores lo que se busca es “potenciar el sector de eventos y reuniones, apuntando a fortalecer el sector que se dedica a los eventos en la ciudad, que se vio muy golpeado por la situación que atravesamos”.

En esa línea, el funcionario destacó que esta iniciativa es un disparador que “marcará un antes y un después en la forma de articular los eventos en la ciudad. Buscamos una forma de facilitar los eventos, que los organizadores se sientan acompañados con la articulación público-privada que venimos haciendo desde el comienzo de la gestión”.

Por su parte, Matías Tomati, presidente del Bureau de Eventos de Santa Fe, destacó que el turismo de reuniones “es una industria que mueve la economía y lo importante es que a través de este programa al que nos convoca la Municipalidad, los actores que tienen la oportunidad de dar su opinión sobre si Santa Fe es una ciudad amigable para hacer un evento, lo puedan hacer. Traccionamos desde ser proactivos junto con el municipio, y el objetivo final es formar un muy buen equipo de turismo-ciudad”.

En el lanzamiento del programa se contó, de forma virtual, con el asesoramiento técnico de Arnaldo Nardone, máximo referente del turismo de reuniones a nivel mundial reconocido por la Organización Mundial de Turismo; y Pablo Sismanian, director de Productos Turísticos del Instituto Nacional de Promoción Turística del Ministerio de Turismo y Deportes de la Nación (INPROTUR).

Nardone y Sismanian fueron los encargados de presentar el programa a los miembros del Safetur -donde se nuclean entidades e instituciones vinculadas al turismo-, a autoridades de universidades nacionales, asociaciones y colegios profesionales, asociaciones deportivas federadas, representantes del Bureau de Eventos de Santa Fe de la Vera Cruz y medios de prensa que se encontraban presentes. Entre los funcionarios municipales, también asistieron el secretario de Educación y Cultural, Paulo Ricci; y el director de Turismo, Franco Arone.

**¿Qué es el programa Embajadores?**

El Turismo de Reuniones es uno de los segmentos más importantes del rubro. Esto se debe por un lado al movimiento económico que genera ya que es una de las ramas del turismo que produce mayor gasto y, por el otro lado, es un sector que propicia el quiebre de la estacionalidad en los destinos ya que se puede practicar todo el año. Teniendo en cuenta esta característica es que desde la Municipalidad se comenzó a delinear el Programa Embajadores ya que representa una oportunidad para la reactivación pospandemia del sector turístico en general.

Básicamente se busca brindar la mayor cantidad de herramientas posibles a los representantes de instituciones y entidades sobre las ventajas competitivas de nuestra ciudad como destino sede de eventos para que realicen las diferentes postulaciones ya sea a eventos académicos, ferias, exposiciones, convenciones, etc. Cabe recordar que para mayoría de los eventos se suelen elegir sedes, dependiendo del tipo, con dos o más años de anticipación.

Para conseguir esto, primero se realizarán capacitaciones a través de talleres que estarán a cargo de Arnaldo Nardone, presidente Honorario de ICCA (Asociación Internacional de Congresos y Convenciones) y director de la Consultora Mice Consulting, una de las principales consultoras del sector en el América Latina. De estos talleres, que se focalizarán en cuatro segmentos -universitario/educativo, asociativo/profesional, deportivo y gobierno- surgirán las personas (profesionales y personalidades locales) que serán nombradas “embajadoras” de la ciudad de Santa Fe y tendrán como tarea presentar a la capital provincial como destino sede de los diferentes eventos.

Para ello contarán con una batería de materiales diseñados específicamente con ese fin; como por ejemplo Bidding Book bilingüe español-inglés, la carpeta de postulación, un registro de calendario de eventos específico de Turismo de Reuniones y el trabajo coordinado con el Observatorio Económico de Turismo de Reuniones de la República Argentina, primer sistema estadístico de monitoreo de Turismo de Reuniones del país.

De esta manera, el trabajo de los “embajadores” ayudará a seguir posicionando a la ciudad como destino, generar recursos económicos, mayor grado de profesionalización del sector y mayor empleo.

**Acompañamiento**

El Programa Embajadores se suma a un cúmulo de políticas que se llevan adelante desde la Municipalidad de Santa Fe impulsadas por el intendente Emilio Jatón, tanto para acompañar al sector turístico en los momentos de cierre total de la actividad durante las primeras etapas de la pandemia como en la actualidad donde las políticas públicas deben acompañar el proceso de reapertura del sector.

Entre las políticas destacadas se encuentran el desarrollo de protocolos específicos para las diferentes actividades relacionadas al turismo. Esto permitió que la ciudad sea una de las primeras en obtener el sello internacional “destino seguro” y también se desarrollaron las audioguías bilingües inglés-español para ampliar el espectro de servicios turísticos que ofrece la capital provincial. Vale destacar que estas audioguías surgen del Programa Mi Ciudad como Turista, una serie de nueve paseos por lugares emblemáticos de Santa Fe guiados y gratuitos. Paralelamente a estas acciones se comenzó a trabajar en el Plan de Desarrollo Estratégico de Turismo “Santa Fe Capital 2025”, a través de talleres participativos con diferentes actores del sector para establecer los lineamientos de desarrollo en el mediano y corto plazo. Y, por primera vez, se establecieron dos registros en la ciudad que apuntan a la profesionalización de la actividad y regularización del sector; por un lado el Registro de Profesionales Guías de Turismo y Técnicos en Turismo y por el otro el Registro de Alquileres Temporarios Turísticos.

Todas estas acciones, sumadas a otras de índole económico, sirvieron de acompañamiento al sector durante las etapas más álgidas de la pandemia y desde un tiempo a esta parte el desafío es crear las mejores condiciones para la reactivación de la actividad.