---
category: Agenda Ciudadana
date: 2021-03-15T06:00:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/adriana-canero.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Cantero lamentó el paro de Amsafe: "Es una de las mejores propuestas del
  país"'
title: 'Cantero lamentó el paro de Amsafe: "Es una de las mejores propuestas del país"'
entradilla: 'Dijo que fue "sorpresiva" la medida tomada por el sindicato de docentes
  públicos, ya que se mejoró lo que fue aprobado en la paritaria nacional. '

---
Aseguró que "la inmensa mayoría de las escuelas" están en condiciones de abrir y rechazó modificaciones en los protocolos escolares.

La ministra de Educación, Adriana Cantero, dijo que tomó "con mucha sorpresa" el rechazo por una diferencia de 1.080 votos de Amsafe a la propuesta del Gobierno provincial conformada por un aumento del 35% dividido en tres cuotas –18% a cobrar en marzo, 8% en julio, 9% en septiembre y una revisión en octubre. Adujo que la oferta está "entre las mejores del país" y que se elaboró en consonancia con lo que fuera aprobado en la paritaria nacional. Y considero que la decisión debería haber tenido "otra razonabilidad en el análisis", debido a la "gran expectativa puesta en el regreso a las escuelas".

\- ¿Cómo tomó la decisión de Amsafe?

\-Con mucha sorpresa. Cerramos la oferta con un enorme esfuerzo financiero de la provincia y un montón de contenidos pedagógicos, después de un largo tiempo de trabajo atendiendo a todos los requerimientos de parte de los gremios docentes.

Estamos convencidos de que, a nivel salarial, es la mejor propuesta de todo el país. Y también los dirigentes sindicales manifestaron, al término de la última reunión, que era una buena oferta.

Es sorpresivo que se tome esa decisión en un momento crucial, donde hay una gran expectativa puesta en el regreso a las escuelas, y que encima haya sido por tan pocos votos. En la provincia, tenemos más de 57 mil docentes, y solo 12 mil decidieron rechazar la propuesta para todos e ir a medida de fuerza. Es una de las votaciones más pobres en cantidad de votos que he visto.

\-En el rechazo a la propuesta, hubo reclamos sobre el incremento, los tramos en los que se divide el aumento y sobre las condiciones edilicias de escuelas que no están en condiciones para retomar la presencialidad. ¿Cómo seguirá la negociación? ¿Hay margen para presentar algo distinto?

\-La propuesta salarial va a ser ratificada. No solo porque consideramos que está entre las mejores del país, sino porque respetamos, e incluso mejoramos, lo que dispuso la paritaria federal, aprobada por todos los sindicatos con representación nacional, incluidos los nuestros. En ese marco, respetamos el cronograma marzo, julio, septiembre, pero además incrementamos el porcentaje de inversión del primer tramo del año y algunos cargos también se aumentó el valor.

Con respecto a lo que se solicita de infraestructura, quiero aclarar lo siguiente: ni bien se pudo retomar la obra pública, tras las medidas de aislamiento social, comenzamos rápidamente con relevamientos de condiciones y asistencia a los edificios escolares; es decir, ocho meses de trabajo en los cuales llevamos invertidos más de mil quinientos millones de pesos.

Esto habla a las claras del estado en que recibimos las escuelas. Son más de tres mil doscientos edificios en toda la provincia, entendemos que no es una tarea sencilla intervenirlos a todos en simultaneidad. A lo mejor hay algunos a los que todavía le restan arreglos, sobre los que nos comprometemos a seguir invirtiendo para cuidarlos, pero la inmensa mayoría ha recibido los fondos necesarios para estar en condiciones este quince de marzo en el inicio del ciclo lectivo.

\-¿Cómo entiende que se haya aprobado la paritaria a nivel nacional y se haya rechazado casi el mismo acuerdo, con algunas mejoras, en la provincia?

\-Por eso hablo de sorpresa, no se entiende que se haya aprobado en nación lo que se rechazó en la provincia, incluso con mejoras, porque en el largo diálogo que hemos tenido durante estos meses siempre intentamos mejorar lo que se nos pedía en el contexto de negociación, hasta en componentes particulares sobre los que se hizo foco.

Además, considero que la decisión debería haber tenido otra razonabilidad en el análisis. A las propuestas es necesario analizarlas en contexto. Todavía estamos atravesando la pandemia, por lo que trabajamos fuertemente para recuperar la máxima presencialidad posible en nuestras escuelas, con todas las acciones preparatorias que también fueron parte de los reclamos de los sectores sindicales, como la creación de cargos adeudados, la titularización en plena pandemia, el suministro de insumos Covid gratuitos, la prioridad en la vacunación, y la puesta en marcha del boleto educativo gratuito.

\-¿Desde el gobierno evalúan alguna medida ante el rechazo a la propuesta, que podría repetir una doble jornada de paro la próxima semana?

\-Los diálogos exigen conversación, y la conversación exige la posibilidad de que las dos partes puedan entrar en análisis de cómo salimos de esta situación. Por el momento no nos enfocamos en ninguna medida, pero no descartamos ninguna posibilidad. Por supuesto, ratificando la propuesta que demandó casi dos meses para presentar la mejor formulación posible.

\-En los últimos días, un grupo de investigadores envió una carta con sugerencias en cuanto a la aplicación de los protocolos. ¿Hay algunos aspectos que sirvan para mejorarlos?

\-Al contrario, todo lo que viene ratificando las distintas experiencias -apertura de escuelas en el centro-norte santafesino el año pasado, los primeros encuentros presenciales en diciembre y el trabajo con los grupos prioritarios en febrero- ratifican la razonabilidad de los protocolos, que están adecuados a los grandes acuerdos federales, que fueron largamente discutidos por los 24 ministros de Salud y Educación del país, para combinar la mirada sanitaria y pedagógica.

Con respecto al grupo de investigadores del Conicet, pertenecientes en su mayoría a las facultades de Farmacia y Bioquímica de la Universidad de Rosario, y entre los que se encuentran también algunos exfuncionarios de la gestión anterior, sorprenden algunas cuestiones de lo que plantean. Por un lado, niego que el protocolo federal no haya analizado que los contagios de coronavirus se den por vía aérea, precisamente por eso, las inspecciones realizadas observaron la disposición de ventanas y puertas para mantener una correcta ventilación y recomendamos tanto la utilización de espacios al aire libre como la condición de uso de barbijo y la distancia de metro y medio entre las personas.

Además, las disposiciones del ministerio contemplan la organización de la actividad escolar con niños y adolescentes. Entonces, si tenemos que poner rutinas de higiene cada vez que entramos y salimos del aula, sería difícil llevar adelante las actividades pedagógicas. Si cada vez que entramos al aula tenemos que hacerlo de forma distanciada, higienizar las manos y acomodarnos, se van veinte minutos de clase en eso, por lo que queda muy poco tiempo para estructurar la propuesta de enseñanza. Por eso, son muchas las provincias que aplicamos este criterio de 90 minutos de clase, ya que destinan un tiempo lógico para poder enseñar y aprender.

Y por otra parte, si estos investigadores están tan preocupados por los contagios en la comunidad, ¿los cuarenta minutos de actividad se los sugieren también a bares, shoppings, fábricas, clubes, cines? Es decir, quiero saber si según este criterio, tenemos que salir veinte minutos para higienizar en todas las actividades, ¿o esta investigación está dirigido solamente al sistema educativo provincial?

**Comedores, todavía no**

Sin recuperar, por el momento, el funcionamiento de los comedores dentro de las escuelas, la ministra aseguró que se mantendrá la entrega de módulos alimentarios por familias y se agregará la distribución de refrigerios a los alumnos:

"El año pasado tuvimos que hacer variaciones en el servicio debido al incremento de la necesidad y la necesidad de mantener el distanciamiento social. En ese sentido, casi triplicamos la inversión, incrementando la asistencia alimentaria de 170 mil raciones individuales a la distribución de más de 130 mil módulos por familia.

Este sistema lo vamos a mantener porque todavía no estamos en condiciones sanitarias de poder cocinar en las escuelas ni de juntar a 200 niños y niñas en un mismo comedor. En reemplazo de la copa de leche, estamos diagramando la entrega individual de un refrigerio para garantizar la ingesta en la escuela, intentando reducir el riesgo de contagio"

**Los protocolos no se tocan**

\-El ciclo lectivo 2021 comenzará igual, aunque no sea de forma unánime, a partir de este 15 de marzo. ¿Cuáles son las expectativas?

\-El ciclo lectivo comienza el 15 de marzo, tal como está fijado por normas de calendario escolar, para garantizar como mínimo los 180 días de clase que indica la ley. Iniciarán los más de 840 colegios privados que tenemos en la provincia. Y creo que muchas de las escuelas de gestión estatal también comenzarán, porque nos han manifestado de forma completa su voluntad de volver a clases.

En esta recorrida que venimos haciendo desde el ministerio, hemos visto comunidades educativas que, con mucho esmero y afectividad, han preparado sus espacios y actividades para sostener el vínculo pedagógico en la presencialidad. Así que esperamos que se mantenga vivo el entusiasmo por volver a las escuelas.

\- ¿Se registraron contagios en las clases para concluir el ciclo anterior? ¿Cómo actuarán en caso de que sí se den en este comienzo masivo?

\-No tuvimos contagios dentro de las escuelas. Los casos que hubo se produjeron fuera de la escuela, y sí motivaron el despliegue de los protocolos de alerta dentro de la institución. Ahora, en este nuevo ciclo lectivo, vamos a estar muy atentos a cómo se despliegan todas las experiencias.

El dispositivo provincial indica que en caso de alguna señal hay que comunicarse inmediatamente al 0800 y notificar a la dirección regional, donde habrá alguien que recibirá esos mensajes y señale qué es lo que se debe hacer en cada caso.

Además, todos los agentes del sistema educativo podrán hacer uso de todos los artículos de las licencias Covid. En caso de los reemplazos, serán activados en lugares estratégicos e insustituibles, como tramos directivos, de asistentes escolares y docentes frente a grupos.