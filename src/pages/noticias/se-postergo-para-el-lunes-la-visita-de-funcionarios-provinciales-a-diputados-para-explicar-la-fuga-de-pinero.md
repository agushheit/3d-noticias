---
category: Agenda Ciudadana
date: 2021-06-30T08:47:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/fuga.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Se postergó para el lunes la visita de funcionarios provinciales a Diputados
  para explicar la fuga de Piñero
title: Se postergó para el lunes la visita de funcionarios provinciales a Diputados
  para explicar la fuga de Piñero
entradilla: Walter Gálvez y Jorge Bortolozzi estaban citados para este miércoles en
  la Cámara baja. Por cuestiones de agenda se reprogramó el encuentro para el lunes
  5

---
La convocatoria a los funcionarios provinciales que debían brindar detalles sobre la fuga de Piñero en la Cámara de Diputados se postergó para el próximo lunes 5 de julio con horario a confirmar, aunque sería por la mañana. El encuentro estaba previsto, en principio, para este miércoles a las 13. Sin embargo, por cuestiones de agenda, el encuentro se realizará la semana que viene.

Luego de la fuga del domingo de ocho presos del penal más importante del sur provincial, la comisión de Seguridad y la de Derechos y Garantías de la Cámara de Diputados decidieron citar al secretario y subsecretario de Asuntos Penales y Penitenciarios de la provincia, Walter Gálvez y Jorge Bortolozzi, para conocer de primera mano la información acerca del escape de los reclusos y la actuación de los agentes que estaban cumpliendo su trabajo en esa cárcel en ese momento.

La postergación de la reunión fue confirmada por el presidente de la comisión de Seguridad de la Cámara baja, Juan Cruz Cándido, quien el lunes se había manifestado sobre lo ocurrido en Piñero. “La situación que se generó con el ataque a balazos y la posterior fuga de ocho presos es de una gravedad extrema, es la segunda fuga en un mes por eso queremos trabajar en conjunto con el secretario Gálvez y el subsecretario Bortolozzi”, sostuvo.

Según pudo saber UNO Santa Fe, el encuentro iba a tener de manera presencial a los presidentes de las comisiones, Juan Cruz Cándido y Lionella Cattalini, mientras que el resto de los participantes, tanto los diputados y diputadas como los funcionarios provinciales, se iban a conectar de manera remota. Ahora que hay más tiempo para organizar el encuentro se analizará si hay modificaciones a ese esquema.

“La situación de los presos de alto perfil en pabellones comunes de Piñero –agregó– la advertimos en febrero del 2020, temiendo que al estar en contacto con otros reclusos, en lugares donde es más difícil realizar un seguimiento más personalizado, podría llegar a ocurrir hechos graves”.

“Es una situación inédita que hace pensar que hubo un trabajo de inteligencia previa que no fue advertido por las autoridades” remarcó Cándido al finalizar el lunes la reunión de la comisión de Seguridad de Diputados y agregó que también harían consultas respecto de los pedidos de informe que aún no fueron contestados.

Por su parte, Lionella Cattalini, integrante de la Comisión de Seguridad de la Cámara de Diputados de Santa Fe, manifestó su preocupación tras las declaraciones de Perotti sobre la segunda fuga de presos de Piñero en dos meses. “El gobernador no se hace cargo de nada de lo que ocurre en la provincia, le echa la culpa a la oposición o a gobiernos anteriores, mientras lleva casi dos años al frente del Ejecutivo, tiene el presupuesto aprobado por la Legislatura y no lo usa”, cuestionó.

“Durante el año 2020 el gobierno provincial ejecutó sólo el 39% de los más de 600 millones que tenía para destinar a la inversión pública en construcción o equipamiento de las cárceles. Y durante el primer trimestre de este año 2021 sólo el 4,5% de los 1.700 millones que desde la Legislatura aprobamos en el presupuesto para el mismo fin. Los recursos están a disposición pero por falta de gestión o desinterés no los han utilizado”, sostuvo.