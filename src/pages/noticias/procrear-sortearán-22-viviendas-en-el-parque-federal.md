---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: "PROCREAR: sorteo de viviendas"
category: La Ciudad
title: "Procrear: sortearán 22 viviendas en el Parque Federal"
entradilla: Los que pueden inscribirse son aquellas personas que tengan entre 3
  y 8 salarios mínimo vital y móvil por grupo familiar o convivientes. Los otros
  requisitos y cómo anotarse.
date: 2020-11-17T14:12:00.784Z
thumbnail: https://assets.3dnoticias.com.ar/procrear.jpeg
---
El Gobierno lanzó la inscripción para más de 1.300 viviendas a sortear en distintos puntos del país.

"Hoy se abre la inscripción para 1.362 viviendas en 8 predios de la línea Desarrollos urbanísticos de Procrear en Buenos Aires, Córdoba, Mendoza, Misiones, San Luis y Santa Fe", comunicó el Gobierno a través de sus redes sociales.

Por LT10, Luciano Scatolini, subsecretario de Política de Suelo y Urbanismo, y miembro del Comité Ejecutivo del Procrear, precisó que las viviendas de Santa Fe son las correspondientes "a la denominada Estación Cambio, urbanizada desde 2014". Respecto a las mismas indicó que hay todavía dos edificios que tienen viviendas para entregar, y con este sorteo se completará la deuda que tiene el Estado que es terminar el otro edificio que fue una obra que quedó paralizada", indicó el funcionario.

![Procrear](procrear1.jpg "Procrear")

Procrear: [+ info](https://www.argentina.gob.ar/habitat/procrear/desarrollosurbanisticos)

En cuanto a los requisitos dijo que el formulario es único para todos los Desarrollos Urbanísticos que se encuentren abiertos para la inscripción. Al completar el Código Postal, se indicará, en caso de corresponder, el o los predios disponibles para el domicilio declarado.

Podrán hacerlo además aquellos que tengan entre 18 y 64 años al momento de completar la inscripción.

Además, no se debe haber resultado beneficiado con planes de vivienda en los últimos diez años para poder acceder a este sorteo. Del mismo modo, se debe demostrar ingresos netos mensuales del grupo familiar conviviente entre 3 y 8 salarios mínimo vital y móvil.

¿Cómo inscribirse? ingresando AQUÍ que es el enlace que subió el gobierno para que accedan aquellos que quieran anotarse.

El plan se ajusta exclusivamente a vivienda única familiar y de ocupación permanente. Scatolini también manifestó que el trámite es online y que el sorteo se hará antes de fin de año, pero no dio una fecha precisa.