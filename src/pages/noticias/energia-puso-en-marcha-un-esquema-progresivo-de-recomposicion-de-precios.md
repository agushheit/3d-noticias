---
category: Agenda Ciudadana
date: 2021-01-05T10:32:07Z
thumbnail: https://assets.3dnoticias.com.ar/050121-biocombustibles.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Télam'
resumen: Energía puso en marcha un esquema progresivo de recomposición de precios
title: Energía puso en marcha un esquema progresivo de recomposición de precios
entradilla: 'Se fijaron nuevos precios de adquisición del biodiésel destinado a su
  mezcla obligatoria y se impuso un plazo máximo de 30 días para que las petroleras
  abonen las facturas correspondientes, entre otras cuestiones. '

---
La Secretaría de Energía implementó un esquema paulatino y progresivo de recomposición de precios de los biocombustibles, que se aplicará de manera gradual a lo largo de cinco etapas hasta mayo próximo e involucra también la reducción en los porcentajes de corte obligatorio de gasoil, de manera de no impactar en el bolsillo de los consumidores.

A través de las resoluciones 1/2021 y 2/2021, publicadas este lunes en el Boletín Oficial, se fijaron nuevos precios de adquisición del biodiésel destinado a su mezcla obligatoria y se impuso un plazo máximo de treinta días corridos para que las petroleras abonen las facturas correspondientes, entre otras cuestiones.

La medida fue el resultado de distintos encuentros y rondas de negociación que mantuvo la Secretaria de Energía y las cámaras del sector, en las que acordaron un sendero de recomposición de los precios.

Las resoluciones buscan dar una respuesta al reclamo que vienen realizando desde hace varios meses las principales entidades que representan a las empresas elaboradoras de biodiésel, entre las que se encuentran la Cámara Argentina de Empresas Regionales Elaboradoras de Biocombustible (Cepreb), la Cámara Santafesina de Energías Renovables (Casfer) y la Cámara Panamericana de Biocombustibles Avanzados (Capba), entre otras.

La Resolución 1/2021 estableció la suspensión, hasta el 31 de mayo, del procedimiento para la determinación del precio de adquisición del biodiésel fijado en marzo de 2018, así como las modificaciones introducidas en abril de 2019.

Además, determinó los nuevos precios de adquisición del biodiésel destinado a su mezcla obligatoria con gasoil, con un valor inicial de $ 77.300 por tonelada para las operaciones a llevarse a cabo durante enero.

En febrero próximo, el precio pasará a ser de $ 86.875 la tonelada; en marzo $ 89.975; en abril $ 90.300 y en mayo $ 92.558 la tonelada de biodiésel.

Al mismo tiempo, la normativa decidió sustituir transitoriamente, para enero, febrero y marzo, la proporción obligatoria de biodiésel en su mezcla con el total del volumen del combustible fósil gasoil que fue establecida en las resoluciones 1283/2006 y 660/2015, ambas de la ex Secretaría de Energía del ex Ministerio de Planificación Federal.

De esta forma, para enero, el corte quedó establecido en el 5%; en febrero 6,7% y en marzo 8,4%.

Bajo este esquema, a partir de abril próximo retornará el porcentaje de corte del 10% de mezcla obligatoria establecido por las normas sustituidas de manera transitoria.

La nueva norma también suspende transitoriamente la asignación mensual de biodiésel en el período comprendido entre enero y mayo de 2021, reduciendo a prorrata las cantidades que correspondan a las empresas del sector.

Para el caso del bioetanol, la Resolución 2/2021 suspende hasta el 31 de mayo próximo el procedimiento para la determinación del precio de adquisición del bioetanol que se utiliza para el corte de naftas en un 12% y fija los nuevos precios en un sendero también de incremento progresivo.

Así, se acordó fijar un valor de $ 43.600 por tonelada para las operaciones a llevarse a cabo durante enero.

En febrero, el nuevo precio fijado será de $ 47.800 la tonelada; en marzo $ 48.700; en abril $ 49.600 y en mayo $ 51.132 la tonelada de biodiésel, lo que significará una recomposición del 55,9%