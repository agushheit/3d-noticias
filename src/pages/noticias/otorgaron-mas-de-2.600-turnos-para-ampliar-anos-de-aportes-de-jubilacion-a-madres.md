---
category: Agenda Ciudadana
date: 2021-08-05T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/MADRESJUBILADAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Otorgaron más de 2.600 turnos para ampliar años de aportes de jubilación
  a madres
title: Otorgaron más de 2.600 turnos para ampliar años de aportes de jubilación a
  madres
entradilla: Lo informaron desde la delegación Litoral de Ansés. En total son 11.000
  las madres santafesinas que van a poder acceder al beneficio de sumar años de aportes.

---
La Ansés comenzó con la atención para el reconocimiento de períodos de aportes por tareas de cuidado. El primer día se brindaron en todo el país 5.298 turnos de asesoramiento sobre jubilaciones para madres. En la región litoral del organismo, ya se encuentran otorgados 3.545 turnos: 2.676 en la provincia de Santa Fe y 869 en Entre Ríos, para que las mujeres que tienen la edad para jubilarse, pero no cuentan con los aportes necesarios, puedan recibir asesoramiento y de estar en condiciones, iniciar sus trámites jubilatorios.

En el marco de sus visitas por las delegaciones para conocer cómo dio comienzo la atención, el Jefe Regional Litoral de Ansés, Diego Mansilla, mantuvo charlas con las primeras madres citadas para jubilarse a partir de esta ampliación de la cobertura de la Seguridad Social.

Al respecto destacó: “Desde hoy miles de mujeres en Argentina están pudiendo tramitar su jubilación mediante esta nueva política pública implementada por el Gobierno Nacional. En este momento estamos acompañando a Nélida, una madre santafesina de 61 años, quien hoy está pudiendo dar inicio a su jubilación gracias al reconocimiento de años de aporte por el cuidado de sus dos hijos”.

Asimismo, expresó: “Estamos siendo parte de un momento histórico, en el que el Estado nuevamente está presente cuidando y reconociendo la enorme tarea de muchas madres en nuestro país, a través de su inclusión previsional”.

Cabe recordar que el reconocimiento es de un año de aportes por cada hijo; dos años por hijo adoptado siendo menor de edad; un año adicional por cada hijo con discapacidad y dos años adicionales en caso de que por el hijo haya accedido a la Asignación Universal por Hijo (AUH) por al menos 12 meses.

También se reconocen los plazos de licencia por maternidad y de licencia por excedencia de maternidad a las mujeres que hayan hecho uso de estos períodos al momento del nacimiento de sus hijos. Esta medida de inclusión es compatible y, de ser necesario, puede complementarse con las moratorias vigentes.

Pueden acceder a este beneficio las mujeres en edad de jubilarse –con 60 años o más–, que sean madres y no cuenten con los 30 años de aportes necesarios para acceder a una jubilación, solicitando un turno en www.anses.gob.ar/reconocimiento-de-aportes-por-tareas-de-cuidado.