---
category: La Ciudad
date: 2021-10-30T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/residencias.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón participó de la inauguración de residencias estudiantiles
title: Jatón participó de la inauguración de residencias estudiantiles
entradilla: Se trata de unidades de vivienda para 240 alumnos de la Universidad Nacional
  del Litoral, que se ubican a la vera de la Ruta 168. “Es un gran sueño que se está
  cumpliendo”, dijo el intendente.

---
El intendente Emilio Jatón participó este viernes de la inauguración de las residencias estudiantiles que construyeron en conjunto la Universidad Nacional del Litoral (UNL) y la Asociación Trabajadores del Estado (ATE). El complejo se encuentra ubicado a la vera de la Ruta Nacional 168 y cuenta con 60 departamentos equipados y amoblados, que tienen capacidad para 240 alumnos.

En la oportunidad, Jatón recorrió el espacio junto al rector de la UNL, Enrique Mammarella; y el secretario General de ATE, Jorge Hoffman, a los que se sumaron autoridades provinciales, municipales y los propios alumnos que ocuparán algunas de las unidades. El intendente rememoró que las obras comenzaron en 2002, con la firma del convenio entre las partes y, en ese momento, “veíamos la importancia de que un gremio y la universidad empiecen a trabajar juntas en este predio”.

“Hoy pasaron los años y llegamos a estas residencias estudiantiles que, junto a la Ciudad Universitaria, conforman un verdadero campus”, aseguró.

En el mismo sentido, afirmó que “cuando uno escucha hablar de residencias estudiantiles recuerda que posiblemente, en algún momento, quiso estudiar afuera y no le daba la economía, porque alquilar era todo un problema. Ahora, quienes cursan en la universidad, tengan buenos promedios y no pueden pagar un alojamiento, pueden venir acá”.

Jatón se refirió a la importancia de “dar oportunidades” que otorga el flamante complejo, “porque estudiar en la universidad abre puertas y nos abre al conocimiento. Y no tener dinero o no tener dónde vivir es un impedimento”.

“Es un gran sueño de la universidad y de la ciudad de Santa Fe que se está cumpliendo”, finalizó.

**Ciudad del conocimiento**

Durante al acto inaugural, Mammarella afirmó que “estas residencias tienen que ver con el valor y la defensa de la Educación Pública, el compromiso social de igualar oportunidades, el virtuosismo del trabajo en equipo entre UNL y ATE, y el trabajo cotidiano que desarrollamos para que Santa Fe se consolide como la Ciudad del conocimiento”.

Hoffman, por su parte, se mostró “muy orgulloso de lo que realizamos durante estos 30 años. Esto demuestra que en Argentina hay una enorme potencialidad en la sociedad y ojalá esto sea un modelo de construcción social”.

En la construcción de los departamentos se combinaron dos propuestas tipológicas: los de planta baja son unidades simples que incluyen viviendas adaptadas para estudiantes con capacidades diferentes, mientras que el resto son dúplex. Todos cuentan con capacidad para cuatro personas, sala de estudios, espacios de recreación, cocina y baño.