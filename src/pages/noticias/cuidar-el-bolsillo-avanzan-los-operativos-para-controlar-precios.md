---
category: Agenda Ciudadana
date: 2021-10-23T06:15:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/feletti.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Cuidar el bolsillo: avanzan los operativos para controlar precios'
title: 'Cuidar el bolsillo: avanzan los operativos para controlar precios'
entradilla: El Gobierno afinó los detalles de los operativos que se pondrán en marcha
  para garantizar el cumplimiento de la resolución que congeló por 90 días los precios
  de más 1.400 productos de consumo masivo.

---
El Gobierno afinó los detalles de los operativos que se pondrán en marcha para garantizar el cumplimiento de la resolución que congeló por 90 días los precios de más 1.400 productos de consumo masivo, a la vez que volvió a reclamar la solidaridad de los empresarios para "cuidar el bolsillo de los argentinos".  
  
El asunto fue uno de los ejes de la reunión que encabezó el presidente Alberto Fernández en la quinta de Olivos con funcionarios nacionales, intendentes bonaerenses y dirigentes de la coalición gobernante, como los diputados Sergio Massa y Máximo Kirchner.  
  
El gobernador bonaerense, Axel Kicillof participó de manera virtual.  
  
En ese encuentro, el Presidente llamó a desempeñar un "fuerte trabajo en defensa del bolsillo de los argentinos, garantizando la estabilidad de los precios y cuidando a las familias", dijeron fuentes oficiales.  
  
El Gobierno emitió esta semana una resolución por la que se congelan los precios de 1.432 productos de consumo masivo hasta el próximo 7 de enero.  
  
El ministro de Desarrollo Social, Juan Zabaleta, afirmó tras la reunión en Olivos que la canasta de productos de consumo masivo a precios convenientes puesta en marcha por el secretario de Comercio Interior, Roberto Feletti, apunta a "cuidar el bolsillo de argentinos", y pidió "solidaridad" a los empresarios.  
  
"Tenemos que cuidar el bolsillo de los argentinos, es una demanda, es una necesidad y a partir de mañana cada intendente, cada funcionario, va a ir a recorrer" el territorio para controlar "esa medida que se tomó de precios máximos", dijo Zabaleta.  
  
El ministro recordó que "a muchos empresarios, durante la pandemia, el Gobierno los asistió con el programa ATP (Asistencia al Trabajo y la Producción) para que los trabajadores pudieran cobrar sus salarios, y eso estuvo bien".  
  
"Ahora necesitamos que los empresarios nos ayuden para cuidar el bolsillo de los argentinos", subrayó Zabaleta.  
  
Durante el encuentro se acordó también que intendentes y funcionarios del Gobierno de la provincia de Buenos Aires realicen controles para ayudar a lograr el fiel cumplimiento de la resolución oficial.  
  
Al respecto, el intendente de Berisso, Fabián Cagliardi, confirmó que mañana comenzarán a "controlar los precios en los municipios" y dijo que los jefes comunales se comprometieron a "a estar muy encima de eso".  
  
A su turno, el intendente de Ensenada, Mario Secco, dijo que "se trató de una reunión de gestión", en donde "se habló de economía" preparándose "para los próximos dos años de Gobierno", porque "la política es la única que soluciona todos los temas".  
  
En la reunión, el Presidente estuvo acompañado por el jefe de Gabinete, Juan Manzur; el canciller Santiago Cafiero; los ministros Zabaleta; de Obras Públicas, Gabriel Katopodis; de Agricultura, Ganadería y Pesca, Julián Domínguez; del Interior, Eduardo De Pedro; de Seguridad, Aníbal Fernández; de Desarrollo Territorial y Hábitat, Jorge Ferraresi; y de Transporte, Alexis Guerrera.  
  
También participaron el secretario general, Julio Vitobello; el secretario de Hábitat, Santiago Maggiotti; la titular de Aysa, Malena Galmarini; el administrador general de Vialidad Nacional, Gustavo Arrieta; y el presidente de la Cámara de Diputados de la Nación, Sergio Massa.  
  
El gobernador bonaerense, Axel Kicillof, participó mediante videoconferencia, debido a que esta tarde estuvo en el municipio de Mar Chiquita, donde inauguró obras.  
  
Allí, durante un contacto que mantuvo con la prensa, Kicillof criticó a las empresas por no adherir de manera voluntaria al acuerdo de precios impulsado por el Gobierno nacional y dijo que "ser oposición no significa oponerse a todo" y que lo que se debe privilegiar es el cuidado del "bolsillo de la gente".  
  
"Es realmente una situación insólita porque la propuesta del Gobierno nacional es perfectamente razonable, no atenta contra la ganancias de las empresas simplemente buscamos darle un respiro al bolsillo de la gente que esta tratando de recuperarse de la pandemia y recuperar la vida previo a la pandemia", dijo el gobernador.  
  
Respecto a la oposición, Kicillof sostuvo que "si quieren hacer un uso político y tirarse en contra de lo que beneficia a la gente, que directamente digan: como hay elecciones, preferimos que haya inflación y así hay descontento" en la población, sostuvo el mandatario.  
  
"La verdad es que uno espera que por lo menos colaboren y no se tiren en contra de la gente, del bolsillo de la gente y del acceso a la alimentación de los sectores populares y medios que tanto lo necesitan", agregó el gobernador bonaerense.  
  
En lo que respecta a la canasta de productos de consumo masivo dispuesta por la Secretaría de Comercio Interior, hoy la Asociación de Supermercados Unidos (ASU) y las cadenas que la integran afirmaron que están trabajando para que "el cambio de precios" resultante de la decisión oficial de congelar los valores de 1.432 artículos "se vea reflejado en las góndolas a la mayor brevedad posible".  
  
"En la última semana, las cadenas de supermercados hemos trabajado para la implementación de la Resolución 1050/2021, dispuesta por el Gobierno nacional", afirmó ASU en un comunicado de prensa.  
  
Más allá de ratificar su predisposición a cumplir con la medida impulsada por el Gobierno, los supermercadistas dijeron que requieren del "compromiso" de las empresas productoras-proveedoras "para cumplir y mantener sus niveles de servicio".