---
category: El Campo
date: 2021-02-12T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/leche.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobierno presentó los lineamientos y programas para fortalecer la actividad
  lechera
title: El Gobierno presentó los lineamientos y programas para fortalecer la actividad
  lechera
entradilla: Entre los aspectos más relevantes se encuentran la inclusión y el fortalecimiento
  de los operadores lácteos, la competitividad y la promoción de las exportaciones.

---
El titular de la Dirección Nacional de Lechería, Arturo Videla, mantuvo un encuentro con representantes tamberos de todo el país, donde se analizó la coyuntura productiva del sector y se presentó un conjunto de lineamientos de trabajo y programas a futuro para el desarrollo y fortalecimiento de la actividad lechera nacional.

Según informaron desde la cartera agropecuaria, entre los principales ejes de trabajo se encuentran la inclusión y el fortalecimiento de los operadores lácteos, la competitividad, la promoción de las exportaciones, la consolidación de las estadísticas sectoriales, un sistema de pago por calidad de la leche y por último, el mejoramiento de los caminos rurales.

El encuentro se llevó adelante desde la dirección Nacional de Lechería con el objetivo de consolidar una agenda de trabajo en común para el crecimiento y desarrollo de la actividad lechera.