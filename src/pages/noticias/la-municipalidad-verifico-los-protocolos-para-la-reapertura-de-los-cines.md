---
category: La Ciudad
date: 2021-03-09T06:57:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/cine1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: La Municipalidad verificó los protocolos para la reapertura de los cines
title: La Municipalidad verificó los protocolos para la reapertura de los cines
entradilla: El miércoles se reinicia la actividad y funcionarios municipales constataron
  el correcto funcionamiento de las medidas tendientes a evitar contagios.

---
Esta mañana se ultimaron los detalles para el regreso de los espectadores a las salas de cines de esta capital. En ese marco, funcionarios municipales realizaron una recorrida por el complejo Cinemark Santa Fe, a los fines de constatar la preparación y señalética de los espacios y las medidas de distanciamiento a adoptar de acuerdo a lo que indican los protocolos vigentes.

Matías Schmüth, secretario de Producción y Desarrollo Económico de la Municipalidad, junto a Cesar Pauloni, director de Salud municipal, participaron de la recorrida junto a Julio Benítez, gerente del complejo. Al finalizar la misma, brindaron detalles sobre cómo serán los protocolos por Covid-19.

El secretario de Producción adelantó que esta semana será la reapertura de los cines: “Nos pone muy contentos que una nueva actividad pueda volver a abrir sus puertas, esto implica cuidar las fuentes laborales de los trabajadores y también que las vecinas y vecinos tengan una opción más de esparcimiento”.

“Por un pedido del intendente Emilio Jatón, desde el municipio siempre hemos apoyado a los diferentes sectores productivos para que puedan volver a funcionar, obviamente, con los recaudos necesarios”, destacó el funcionario. Con respecto a  la recorrida por el complejo de cine, Schmüth indicó que “observamos cómo serán los protocolos y cómo se están preparando para que la gente pueda disfrutar”.

**Se encienden las pantallas**

En tanto, Pauloni anticipó que el lugar “está preparado para estas nuevas modalidades de apertura”. En esa línea, el director de Salud de la Municipalidad indicó que “se cumplen los protocolos, los espacios están pensados para que la circulación de personas no sea cruzada y está pensada la recirculación del aire”. En relación a la cantidad de espectadores, el funcionario consignó: “la capacidad será a un 50 por ciento de cada sala, sosteniendo las distancias correspondientes”.

Asimismo, los espacios comunes y de ingreso y egreso de personas también fueron revisados. “Ya se acordó la separación que debe haber el hall de ingreso, entendemos que el espacio está apto y que los protocolos cumplen los requerimientos en pos de evitar los contagios. No obstante, siempre apelamos a la responsabilidad social de los espectadores. Una pandemia se transita y en ese tránsito la ciudadanía va adoptando medidas que se tienen que sostener en el tiempo. Es fundamental que las personas sostengan las medidas necesarias para controlar  los contagios”, explicó el director de Salud.

**Retomar el trabajo**

Por su parte, Benítez, destacó que “venimos trabajando desde mayo del año pasado en los protocolos para que se concrete esta reapertura. Este miércoles podemos abrir las puertas, así que los esperamos a todos”.

Luego de un año de puertas cerradas “estuvimos adaptándonos a esta nueva normalidad, preparándonos en cada detalle para que el virus no se propague y tener todas las precauciones necesarias tanto para el cliente como para los empleados”, indicó Benítez.

Cabe remarcar, que con la vuelta al cine, un total de 16 trabajadores volverán a la actividad el miércoles, “si bien en total somos 31 los trabajadores, volvemos en grupos de burbujas, así que este primer equipo arranca el miércoles”, aclaró Benítez.

![](https://assets.3dnoticias.com.ar/cine.jpg)

![](https://assets.3dnoticias.com.ar/cine2.jpg)![](https://assets.3dnoticias.com.ar/cine3.jpg)