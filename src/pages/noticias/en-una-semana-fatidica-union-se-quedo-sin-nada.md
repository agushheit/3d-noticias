---
category: Deportes
date: 2020-12-06T12:11:09Z
thumbnail: https://assets.3dnoticias.com.ar/union.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: En una semana fatídica, Unión se quedó sin nada
title: En una semana fatídica, Unión se quedó sin nada
entradilla: Unión perdió ante Atlético Tucumán 5-3 y no logró clasificar a la Fase
  Campeonato. El martes había quedado eliminado de la Copa Sudamericana.

---
En apenas cuatro días, Unión se quedó con las manos vacías, el martes fue eliminado de la Copa Sudamericana empatando como local ante el Bahía. Y este sábado como local perdió frente a Atlético Tucumán 5-3 y no logró clasificar a la Fase Campeonato.

Unión tenía dos resultados a favor, triunfo y empate, pero perdió y con la victoria de Arsenal ante Racing, quedó en la tercera posición. Con el agravante de que el Decano puso en cancha una formación alternativa. El Tate se desmoronó por completo en lo futbolístico y en lo anímico.

Fue un horror en el aspecto defensivo y en ataque si bien anotó tres goles, también falló mucho. El equipo estuvo totalmente desbalanceado, generando una enorme preocupación de cara al futuro. Unión competía en los dos frentes y cuatro días después no pelea por nada, jugando la Fase Complementación.

Sin dudas que un golpe demasiado fuerte y que puede presagiar una tormenta en lo inmediato. Tuvo todo a favor, definiendo las dos instancias como local, pero no supo hacerlo. Pasó de la ilusión a una enorme decepción y a un futuro plagado de interrogantes.

Unión con la necesidad de sumar al menos un punto salió a jugar frente a Atlético Tucumán con el esquema de siempre, 4-3-3 tratando de mirar el arco rival. Enfrente un equipo alternativo del Decano, ya que está clasificado como primero en su grupo.

En ese contexto es que Unión arrancó urgido buscando el triunfo aún sabiendo que el empate lo beneficiaba. Y en esos primeros minutos se observó al elenco local bastante impreciso y apurado, frente al visitante que jugaba sin presiones y con mayor soltura en el juego.

Así las cosas, en el primer cuarto de hora se lo observó mejor al Decano, y por eso no extrañó que se ponga en ventaja. Una imprudencia de Brian Blasi quien derribó a Ramiro Ruiz Ríaz y el árbitro Fernando Espinoza decretó la pena máxima. Y a los 17' el ex Unión Augusto Lotti cambió penal por gol para establecer el 1-0.

Unión lo pudo empatar rápidamente, pero un cierre providencial de Guillermo Ortiz evitó el gol. El Pocho Franco Troyansky quedó mano luego de un pase de Fernando Elizari y su remate fue interceptado por el ex Colón que fue al piso y mandó el balón al córner. Fue la primera llegada clara de un equipo que no jugaba bien.

Por ello, a los 24' una vez más el Decano aprovechó los errores del fondo rojiblanco, pelotazo a las espaldas de Vera y Blasi falló en el despeje. Matías Alustiza habilitó a Lotti quien con una muy buena volea de derecha decretó el 2-0. Atlético Tucumán era pura efectividad y ganaba por dos goles.

El equipo local estaba muy nervioso y tomaba malas decisiones. Pero a los 32' Cabrera habilitó a Vera y el lateral derecho ante la salida de Lucchetti cedió a Troyansky quien yendo al piso alcanzó a conectar y con un toque suave alcanzó el descuento.

El partido era realmente entretenido, de ida y vuelta, ya que la mitad de la cancha era una zona de paso. Ambos equipos arriesgaban y en defensa no se mostraban seguros. De allí las muchas situaciones de peligro y los goles que se dieron en esa primera etapa.

En el segundo tiempo el Rojiblanco salió decidido a empatarlo. Pero era una cosa cuando atacaba y otra cuando defendía. Cada contra del Decano era sensación de gol, y por eso a los 7' Lucas Melano puso el 3-1. Una vez más le ganaron las espaldas a Vera y Blasi por izquierda y Lotti habilitó a Melano quien definió por el centro.

Unión pudo descontar con una media vuelta de Troyansky, pero Lucchetti en una gran intervención envió el balón al córner. Y antes cuando el partido estaba 2-1, Fernando Elizari remató y el arquero visitante dio rebote hacia un costado, el Tate llegaba pero no concretaba.

A los 25' un pase exacto de Elizari lo dejó a Juan Manuel García mano a mano con Lucchetti, pero su remate se fue por encima del horizontal. Y en la jugada siguiente Leonardo Heredia pudo anotar el cuarto, pero su remate fue tapado por Moyano y en el rebote definió mal.

El Pocho Troyansky logró descontar a los 32' con un remate cruzado como para darle vida al Tate. El delantero de Unión fue lo más rescatable en el conjunto dirigido por Juan Manuel Azconzábal. Y en un rebote dentro del área aprovechó para poner el cotejo 3-2.

Pero la ilusión duró poco, ya que el recientemente ingresado Kevin Isa Luna a los 36' anotó el cuarto gol del Decano con un remate cruzado que se metió en el segundo palo del arco defendido por Moyano. Otra vez la acción se generó por el costado derecho de la defensa rojiblanca.

Unión siguió desperdiciando opciones de gol, primero Blasi luego Gastón González debajo del arco, pero en el tercer minuto de descuento, Juan Manuel García de cabeza puso el tercero para Unión y para nuevamente ilusionarse con la hazaña.

Pero rápidamente se rompió, ya que en la acción siguiente y con Unión jugado en ataque, el Bebe Guillermo Acosta probó de media distancia y Moyano falló, ya que el balón le pasó por encima del cuerpo y decretó el definitivo 5-3 para dar por finalizado un partidazo plagado de goles y de jugadas de gol.

## **Síntesis**

**🔴⚪ Unión:** 25-Sebastián Moyano; 36-Federico Vera, 17-Brian Blasi, 27-Jonathan Galván, 3-Claudio Corvalán; 14-Nery Leyes, 30-Juan Ignacio Nardoni, 18-Fernando Elizari; 7-Javier Cabrera, 9-Fernando Márquez y 22-Franco Troyansky. DT: Juan Manuel Azconzábal.

**🔵⚪ Atlético Tucumán:** Cristian Lucchetti; 4-Gustavo Toledo, 24-Guillermo Ortiz, 6-Mauro Osores, 14-Agustín Lagos; 26-Lucas Melano, 7-Franco Mussis, 19-Nicolás Aguirre, 18-Ramiro Ruiz Rodríguez; 9-Augusto Lotti y 11-Matías Alustiza. DT: Ricardo Zielinski.

**Goles:** 17' Augusto Lotti (AT), 24' Augusto Lotti (AT), 32' Franco Troyansky (U), ST 7' Lucas Melano (AT), 32' Franco Troyansky (U), 36' Kevin Isa Luna (AT), 48' Juan Manuel García (U), 49' Guillermo Acosta (AT).

**Cambios:** ST 0' Gastón Comas x Nardoni (U), 12' Gastón González x Vera (U), Juan Manuel García x Márquez (U), 12' Marcelo Ortiz x Lagos (AT), Leonardo Heredia x Ruiz Rodríguez (AT), 27' Kevin Isa Luna x Lotti (AT), Guillermo Acosta x Aguirre (AT), 36' Jonas Romero x Alustiza (AT), 41' Kevin Zenón x Elizari (U).

**Amonestados:** Vera y Troyansky (U), Lucchetti, Melano y Mussis (AT).

**Estadio:** 15 de Abril.

**Árbitro:** Fernando Espinoza.