---
category: Estado Real
date: 2021-01-21T09:07:53Z
thumbnail: https://assets.3dnoticias.com.ar/ley_micaela.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de la Provincia de Santa Fe
resumen: 'Ley Micaela: El Gobierno de Santa Fe y la Asociación de Prensa llevan adelante
  una capacitación por una comunicación más inclusiva'
title: 'Ley Micaela: El Gobierno de Santa Fe y la Asociación de Prensa llevan adelante
  una capacitación por una comunicación más inclusiva'
entradilla: Se llevó a cabo el tercer y último encuentro de esta primera instancia
  con trabajadores y trabajadoras de la ciudad capital.

---
El Gobierno de Santa Fe, a través de la Secretaría de Estado de Igualdad y Género, sigue adelante con la implementación de la Ley Micaela. Esta semana tuvo lugar la tercera jornada virtual de capacitación para trabajadores y trabajadoras de prensa de la Asociación de Prensa de Santa Fe. Durante el encuentro se trabajaron los temas violencia simbólica, lenguaje inclusivo y abordajes de la diversidad sexual en los medios de comunicación.

“Este trabajo en conjunto que comenzamos junto a los sindicatos de prensa, en este caso particular con la Asociación de Prensa de Santa Fe, es estratégico por muchas razones. En principio porque los sindicatos representan el mundo del trabajo; en segundo lugar porque la palabra y lo simbólico ocupan un lugar central en la construcción cotidiana de mensajes”, sostuvo Celia Arena, secretaria de Estado de Igualdad y Género. “Desde la gestión tenemos el compromiso de no reproducir las desigualdades y avanzar en políticas públicas que aporten a una sociedad más igualitaria y sin violencias. Ley Micaela es una herramienta crucial para ello y es un compromiso del gobernador Omar Perotti y el Gobierno de Santa Fe que su aplicación llegue a todos los rincones de la provincia”, afirmó Arena.

"Somos conscientes de que en cuanto a la perspectiva de género, las y los trabajadores de prensa tenemos un doble desafío: hacia adentro, con nuestros compañeros, compañeras y jefes y jefas, y hacia afuera, porque como comunicadoras y comunicadores tenemos una gran responsabilidad social. Apuntamos a transformarnos en replicadores porque necesitamos comenzar a transitar desde nuestros lugares de trabajo, un camino hacia una comunicación más inclusiva, libre de violencias y estereotipos", sostuvo por su parte Cintia Mignone, secretaria de Prensa y Derechos Humanos de la Asociación de Prensa de Santa Fe (APSF).

Este curso, destinado a la conformación de un equipo de formadores y formadoras que llevarán adelante la capacitación en otras instituciones, se dio en el marco del acuerdo de colaboración recíproca suscrito entre la Secretaría de Estado de Igualdad y Género, y los Sindicatos de Prensa de la provincia durante el año 2020.

Dentro de la agenda de trabajo se realizaron tres encuentros de intercambio y reflexión sobre la perspectiva de género y diversidad sexual en los medios. La jornada de este lunes fue la última.

**LEY MICAELA EN LA PRENSA**

El acuerdo de colaboración, suscrito el 22 de octubre con la Asociación de Prensa de Santa Fe y con el Sindicato de Prensa de Rosario, tiene por objetivo establecer un marco general de cooperación a partir del cual ambas partes “se comprometen a coordinar acciones tendientes a desarrollar en forma conjunta o en cooperación programas o proyectos en materia de políticas de género, igualdad, diversidad, y prevención y atención de situaciones de violencia por razones de género, en función de las capacidades y competencias propias, sin perjuicio de otro tipo de actividades que puedan llevarse a cabo en áreas de mutuo interés”.

Cabe recordar que dicha Secretaría de Estado es el organismo de aplicación de la Ley Micaela a nivel provincial. El espíritu de la normativa contribuye a promover una sociedad más justa e igualitaria, libre de violencias y discriminaciones.