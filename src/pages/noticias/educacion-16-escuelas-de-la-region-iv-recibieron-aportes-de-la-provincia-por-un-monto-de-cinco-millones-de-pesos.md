---
category: Estado Real
date: 2021-01-04T09:41:11Z
thumbnail: https://assets.3dnoticias.com.ar/040121-aportes-escuela.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Educación: 16 escuelas de la Región IV recibieron aportes de la provincia
  por un monto de cinco millones de pesos'
title: 'Educación: 16 escuelas de la Región IV recibieron aportes de la provincia
  por un monto de cinco millones de pesos'
entradilla: Pertenecen a las localidades de Santa Fe, Progreso, Ángel Gallardo, Gálvez,
  Campo El Martillo, San Carlos, Providencia, Recreo, Santa María Norte y Campo la
  Vigilancia.

---
La Ministra de Educación, Adriana Cantero, la secretaria de Gestión Territorial Educativa Rosario Cristiani, el secretario de Educación, Víctor Debloc, y la Delegada Regional de Educación IV, Mónica Henny, **visitaron la escuela N° 618 «Ingeniero Julio Bello» de la ciudad de Santa Fe, en donde se entregaron aportes para 16 escuelas** de Santa Fe, Progreso, Ángel Gallardo, Gálvez, Campo El Martillo, San Carlos, Providencia, Recreo, Santa María Norte y Campo la Vigilancia **por un monto total de $5.389.744,46**.

**Las partidas provenientes del FANI (Fondo Asistencia Necesidades Inmediatas) serán destinadas a refacciones generales**, instalación de sanitarios con conexión a la red cloacal, instalaciones eléctricas y adquisición de equipamiento para aulas, cambio de cubiertas, fabricación y colocación de rejas, portones, ventanas y bomba de agua, adquisición de cocina industrial para comedor escolar, instalación de gas natural, modificación de cañería de calefactores, colocación de rejillas de ventilación, pruebas de hermeticidad, colocación de tanque cisterna, mantenimiento de bombas, colocación de llaves automáticas, reparaciones de cubierta de chapas, restauración de casa habitación para uso de espacios pedagógicos, colocación de losetas y mosaicos en pisos de galería, sanitarios de sum, colocación de bomba presurizadora y recambio de cubierta en galería, cambio de tanque y adquisición de una heladera.

En el acto de entrega de fondos, Cantero extendió el agradecimiento «a todas, todos y cada uno de los docentes de la provincia y a la totalidad de los asistentes escolares por todo lo que se ha desarrollado con presencia activa de las escuelas en las comunidades con la asistencia y el compromiso. Las escuelas han sostenido la alimentación en tiempos de crisis de más de 130 mil familias a partir de una inversión muy grande del Ministerio de Educación para garantizar la alimentación de nuestras niñas, niños, adolescentes y también adultos y a su vez sostener con metodologías y reinventándonos para mantener el vínculo pedagógico».

Por otra parte, la titular de la cartera educativa destacó: «**nunca soñamos con una escuela deshabitada**, pero logramos que siga viva en cada casa y en todos los rincones de nuestra provincia. Lo que hoy nos convoca tiene que ver con **preparar los edificios escolares para mejorar las condiciones sanitarias**, por eso toda la entrega de los recursos a través del FANI, tienen que ver con una mirada en el marco de esta pandemia, y es por ello que **el destino de estos fondos está relacionado principalmente con la provisión de agua segura, tanques, sanitarios, lavamanos y espacios ventilados**».

Por su parte, Debloc indicó: «hemos estado asistiendo a muchas escuelas durante este año, superando los 360 millones de pesos de aportes de Fani en toda la provincia. Probablemente no hemos llegado a todos los edificios, pero **hemos logrado una asistencia cercana del 70 % de los edificios que estaban en dificultades, siempre con la perspectiva de volver a la presencialidad**. Por eso en estos meses vamos a poner el eje en las actividades recreativas y formativas de verano para fortalecer la revinculación de las chicas y los chicos y eso es posible por el compromiso de todas y todos los docentes santafesinos».

Por último, Cristiani manifestó: «estamos cerca de las escuelas, cerca de los docentes y los directivos que son los que hacen que las escuelas funcionen, porque para esta gestión las escuelas son el corazón del sistema educativo, porque nosotros apostamos a las escuelas y sabemos que se producen cosas maravillosas, ojalá el próximo año podamos volver a la presencialidad y eso dependerá de nuestra propia responsabilidad».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">DETALLE DE LOS APORTES</span>**

◾ **Jardín N° 131** «Martín de la Peña» de Progreso entrega de $766.178,00.

◾ **Escuela Secundaria Orientada N° 506** «Ángela Peralta Pino» de Santa Fe entrega de $415.918,86.

◾ **Escuela Primaria N° 1111** «Luis Borruat» de Santa Fe entrega de $340.131,64.

◾ **Escuela Primaria N° 1065** «Manuel Belgrano» de Ángel Gallardo entrega de $45.000.

◾ **Escuela Especial N° 2041** «Dr. José Gálvez» de Gálvez entrega de $101.730.

◾ **Escuela Primaria N° 9** «Juan José Paso» de Santa Fe entrega de $83.600.

◾ **Centro de Educación Rural N° 545** de Campo El Martillo entrega de $103.400.

◾ **Escuela Primaria N° 618** «Ingeniero julio Bello» de Santa Fe entrega de $734.608.

◾ **Escuela Primaria N° 365** «Domingo Faustino Sarmiento» de San Carlos entrega de $546.680.

◾ **Escuela Primaria N° 943** «Nicolás Rodríguez Peña» de Providencia entrega de $463.140.

◾ **Taller N° 110** «11 de Septiembre» de Santa Fe entrega de $260.250,00.

◾ **Escuela Secundaria Orientada N° 253** «Mariano Moreno» de Progreso entrega de $758.684,00.

◾ **Jardín N° 140** «Sol Rosa Acuña» de Recreo entrega de $345.852.

◾ **Escuela Primaria N° 366** «Enrique de Vedia» de Santa María Norte entrega de $88.310,03.

◾ **Escuela Primaria N° 1321** «Dr. Agustín Zapata Gollán» de Santa Fe entrega de $306.750.

◾ **Escuela Primaria N° 334** «Brigadier Gral. Estanislao López» de Campo la Vigilancia entrega de $29.502,93.