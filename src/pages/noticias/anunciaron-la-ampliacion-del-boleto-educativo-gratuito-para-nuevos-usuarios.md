---
category: Agenda Ciudadana
date: 2021-08-26T18:15:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/BOLETOEDUCATIVO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Anunciaron la ampliación del Boleto Educativo Gratuito para nuevos usuarios
title: Anunciaron la ampliación del Boleto Educativo Gratuito para nuevos usuarios
entradilla: El beneficio se extenderá a quienes concurran a Centros de Educación Física
  (CEF), talleres de educación manual, bibliotecas pedagógicas y cocinas centralizadas.
  ¿Cómo gestionar el beneficio?

---
El Gobierno de la Provincia, a través de la secretaría de Transporte del Ministerio de Producción, Ciencia y Tecnología, anunció que el beneficio del Boleto Educativo Gratuito se extenderá también a quienes concurran a Centros de Educación Física (CEF), talleres de educación manual, bibliotecas pedagógicas y cocinas centralizadas.

El objetivo del programa es garantizar la llegada de estudiantes de todos los niveles, docentes y asistentes escolares de los establecimientos educativos de toda la provincia y representa una inversión del gobierno provincial en pos de una educación inclusiva, sin que el valor del transporte sea un impedimento para lograrlo.

Al respecto, el director Provincial de Boleto Educativo, Rober Benegui, destacó: "Cuando lanzamos el Programa sabíamos que una vez en marcha, íbamos a tener que optimizar su funcionamiento, contemplar situaciones especiales, incluir realidades no consideradas en la primera etapa de ejecución y la incorporación de estos establecimientos era una de nuestras prioridades”.

**¿Cómo gestionar el beneficio?**

Se debe descargar e ingresar a la APP “Boleto Educativo” a través del celular, o ingresando en www.santafe.gov.ar 

Para realizar el trámite, se deberá estar previamente registrado en ID Ciudadana, ingresando en www.santafe.gov.ar/idciudadana

Los usuarios tendrán esta nueva opción en el campo "Establecimiento al que asiste" seleccionando la opción “Organismos Auxiliares / Otros”. Allí están incluidos los Centros de Educación Física (CEF), los Talleres de Educación Manual, las Cocinas Centralizadas y también las Bibliotecas Pedagógicas.

Para consultas o reclamos, se puede enviar un correo electrónico a consultasbeg@santafe.gov.ar