---
category: Agenda Ciudadana
date: 2021-01-27T10:14:01Z
thumbnail: https://assets.3dnoticias.com.ar/35porciento.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Infobae
resumen: Paso a paso, cómo pedir a la AFIP la devolución del 35% por la compra de
  dólar ahorro o gastos con tarjeta
title: Paso a paso, cómo pedir a la AFIP la devolución del 35% por la compra de dólar
  ahorro o gastos con tarjeta
entradilla: El organismo habilitó el procedimiento en su página web para los que están
  autorizados a pedir el reintegro

---
La Administración Federal de Ingresos Públicos (AFIP)  habilitó la posibilidad de gestionar la devolución del 35% pagado a cuenta por las compras del cupo mensual de USD 200 para atesoramiento, conocido como “dólar ahorro”, y las compras en dólares con tarjeta de crédito.

Esta opción está disponible para las personas que no paguen el Impuesto a las Ganancias o el impuesto sobre los Bienes Personales. Para solicitar la devolución, deberán acceder con clave fiscal al servicio “Mis aplicaciones web”

Asimismo, durante la última semana de diciembre la AFIP habilitó un servicio web dentro de su página que permite consultar e informar percepciones practicadas sobre las operaciones alcanzadas por el Impuesto Para una Argentina Inclusiva y Solidaria (PAIS), que son los afectados por la norma establecida a mediados de septiembre que tenía como objetivo frenar las compras de dólar “solidario”.

Por lo tanto, monotributistas, trabajadores en relación de dependencia y jubilados que realizaron compras en moneda extranjera pero que no están alcanzados por Ganancias y Bienes Personales podrán solicitar el reintegro de la percepción.

La AFIP aún no informó qué plazo demorará la devolución. La acreditación se realizará en la cuenta bancaria del contribuyente, que debe estar consignada en la sección “Declaración de CBU” del sitio de la AFIP.

 

**Cómo tramitar la devolución**

Ingresar a la página de AFIP (www.afip.gob.ar) con clave fiscal. El sistema desplegará en pantalla la lista de servicios AFIP habilitados. Se debe seleccionar “Mis Aplicaciones Web”. Si no se encuentra este servicio, se tiene que habilitar utilizando la opción “Administrador de Relaciones de Clave Fiscal”.

  

**Requisitos**

\- Contar con Clave Única de Identificación Tributaria (CUIT) y con clave fiscal.

\- Informar a la AFIP la Clave Bancaria Uniforme (CBU) de la cuenta bancaria donde se quiere recibir la devolución, a través del servicio “Declaración de CBU para cobros de origen tributario, aduanero y de la seguridad social”.

\- Contar, previamente, con la cancelación de deudas o regularización de las inconsistencias, para después poder acreditarse lo percibido.

\- Contar con Domicilio Fiscal Electrónico.

 

**Pasos**

**1)** Ingresar con “Clave Fiscal” al servicio “Mis Aplicaciones Web”

**2)** Seleccionar la opción “Nuevo” que se encuentra en el margen superior izquierdo. Como consecuencia se abrirá una nueva pantalla donde se tiene que seleccionar nombre y los siguientes campos:

     * Organismo: AFIP

     * Formulario: F746/A - Devoluciones y transferencias

     * Período Fiscal: indicar el período por el cual se solicita la devolución, con el formato AAAAMM.

     * Finalmente, seleccionar “Aceptar”

**3)** A continuación el sistema mostrará el detalle de percepciones registradas en “Mis Retenciones” para ese período. Se debe tildar cuáles son las que se quiere tramitar en devolución.

**4)** En caso de que existan percepciones no registradas en el sistema, se podrán ingresar seleccionando: “AGREGAR PERCEPCIÓN”.

**5)** Para realizar la carga se tiene que informar:

     * Origen

     * Tarjeta

     * Fecha de Pago del resumen y/o liquidación/ Fecha de débito

     * Monto de la percepción.

**6)** Una vez que se haya realizado la carga completa de datos, seleccionar el botón GRABAR, que se encuentra en el margen superior de la pantalla. Para finalizar la carga, presionar el botón PRESENTAR. El sistema preguntará si se confirma la presentación, y luego informará que la presentación ya fue realizada. Aceptando este mensaje se puede visualizar el acuse de recibo de la presentación.

**7)** Para realizar el seguimiento de la solicitud de devolución, hay que ingresar con “Clave Fiscal” al Servicio “Mis Aplicaciones Web”.

**8)** A continuación, se visualizará la pantalla de ingreso al servicio. Se tiene que seleccionar la opción BUSCAR que se encuentra en el margen superior izquierdo. Allí, hacer clic en ACEPTAR para visualizar las solicitudes realizadas.

**9)** De esta forma, se visualizará las solicitudes realizadas y, si se hace clic en PRESENTADO, se podrá ver el estado de la solicitud seleccionada y el sistema desplegará el estado del trámite.

 