---
category: Agenda Ciudadana
date: 2020-12-06T11:57:38Z
thumbnail: https://assets.3dnoticias.com.ar/jubilados.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: Pami anunció un bono navideño de $1.500 para 550.000 afiliados
title: Pami anunció un bono navideño de $1.500 para 550.000 afiliados
entradilla: Se abonará entre el 17 y 23 de diciembre, según el calendario diagramado
  junto a la ANSES, según terminación de DNI.

---
El PAMI abonará un bono navideño de 1.500 pesos para las 550.000 personas afiliadas destinatarias del Programa Alimentario, “que garantiza la seguridad alimentaria de quienes más lo necesitan”, informó hoy la institución.

El bono, de 1.500 pesos, se abonará entre el 17 y 23 de diciembre, según el calendario diagramado junto a la ANSES, por terminación del DNI de las personas afiliadas.

Además, el PAMI informó que entregará “un bono navideño de $10.000 a los centros de jubilados y pensionados, lugares clave para la socialización y recreación de las personas mayores”.

La medida, que se efectivizará también en diciembre, es un refuerzo fijo y extraordinario de PAMI para el fortalecimiento de los 4.200 centros de jubilados que por la pandemia deben permanecer cerrados.

“La obra social trabaja sostenidamente en las políticas de inclusión social para alivianar la situación compleja que se vive durante la emergencia sanitaria. 

Con el bono navideño estamos cumpliendo, una vez más, los compromisos que asumimos con las jubiladas y jubilados de la obra social más grande de América Latina”, dijo la directora ejecutiva, Luana Volnovich.