---
category: Agenda Ciudadana
date: 2021-09-24T06:00:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/DESOCUPACION2.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El índice de desocupación bajó al 9,6% en el segundo trimestre del año
title: El índice de desocupación bajó al 9,6% en el segundo trimestre del año
entradilla: Según informó el Instituto Nacional de Estadística y Censos (Indec), la
  sub ocupación demandante, que comprende a la gente que trabaja menos de 35 horas
  semanales, aumentó al 8,5%.

---
El índice de desempleo se ubicó en el 9,6% al cierre del segundo trimestre, por debajo del 13,1% de igual período del 2020, y del 10,2% del primer trimestre del corriente año, informó el Instituto Nacional de Estadística y Censos (Indec).

En tanto, la sub ocupación demandante, que comprende a la gente que trabaja menos de 35 horas semanales, aumentó al 8,5% contra el 5% del segundo trimestre del año pasado, mientras que los “no demandantes” bajó al 3,9%, contra el 4,6% de igual lapso anterior.

Entre el segundo trimestre del 2021, e igual período del año pasado, el Producto Bruto Interno (PBI) creció 17,9%, si bien retrocedió 1,4% en relación al nivel de actividad registrado en el primer trimestre del corriente año, según datos del Indec.

Al observar la composición de la población ocupada por rama de actividad, se destaca que, en la comparación interanual, el rubro Construcción aumentó su participación en 1,6 puntos porcentuales (de 6,9% a 8,5%); Transporte, almacenamiento y comunicaciones, en 0,7 puntos (de 7,3% a 8,0%), Otros servicios comunitarios, sociales y personales en 0,7 puntos (de 4,8% a 5,5%) y Hoteles y restaurantes, en 0,6 puntos (de 2,8% a 3,4%).

En base a estos números, el Indec calculó que la cantidad de desocupados fue de alrededor de 1.273.000 personas en el segundo trimestre, frente a una población económicamente activa de 13.254.000 en condiciones de trabajar en los principales centros urbanos.

Además, dio cuenta también que se redujo fuertemente el ausentismo, producto del Covid-19 entre ambas mediciones. Cuando operaban a pleno las restricciones para combatir la pandemia alcanzaba al 21,1% y esa tasa bajó al 4,8% entre abril y junio de este año.

Los niveles de mayor desocupación se anotaron en Santa Rosa, la capital de La Pampa, con el 13,3%; seguido por Tucumán, y Córdoba, ambas con el 12,4%; Rosario, 11,8%; Mar del Plata 11,6%; Rawson-Trelew, 10,7%; Salta 10,4%; y el Gran Buenos Aires, donde se concentra casi un tercio de la población del país, con el 10,2%

El informe del Indec precisó también que en el segundo trimestre de 2021, la tasa de actividad (TA) -que mide la población económicamente activa (PEA) sobre el total de la población- alcanzó el 45,9%; mientras que la tasa de empleo (TE) -que mide la proporción de personas ocupadas con relación a la población total- se ubicó en 41,5%; y la tasa de desocupación (TD) -personas que no tienen ocupación, están disponibles para trabajar y buscan empleo activamente, como proporción de la PEA- se ubicó en 9,6%.

Al analizar las poblaciones específicas para la población de 14 años y más por sexo, la tasa de actividad de las mujeres fue de 48,4%, mientras que la de los varones fue de 69,1%.

A nivel geográfico, la región Cuyo presentó la mayor tasa de actividad en el segundo trimestre del año con el 47,9%, seguida por la región Pampeana (47,0%).

Por el contrario, las regiones Patagonia (42,9%) y Noreste (42,5%) presentaron las menores tasas de actividad.

Del total de trabajadores ocupados en el período, el 72,6% son asalariados, de los cuales 31,5% no tienen descuento jubilatorio, por lo que desarrollan sus tareas en de manera informal.

En tanto, el 23,2% son trabajadores que laboran por cuenta propia; el 3,7% son patrones o dueños de los emprendimientos; y el 0,5% corresponden a trabajadores familiares sin remuneración.

El informe detalló también que en el segundo trimestre del año el 17,9% de los ocupados realizó sus tareas desde sus viviendas, de manera remota, en medio de la segunda ola de coronavirus.

De este subtotal, entre los ocupados asalariados, el 9,5% utilizó sus propias maquinarias/equipos para realizar su trabajo de manera online.

En el plano educativo, el 31% de los trabajadores cuenta con secundario incompleto, el 26,4% con secundario completo, el 42,6% presenta un nivel entre superior y universitario (completo o incompleto).