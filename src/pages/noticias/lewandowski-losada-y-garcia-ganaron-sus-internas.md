---
category: Agenda Ciudadana
date: 2021-09-13T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/TRICOTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Lewandowski, Losada y García ganaron sus internas
title: Lewandowski, Losada y García ganaron sus internas
entradilla: En la interna del Frente de Todos el candidato de Perotti se impuso de
  forma clara a Rossi. El Socialismo sostiene la hegemonía en el Frente Progresista
  y Losada se impone a Pullaro en la interna amarilla

---
Con más del 77 por ciento de las mesas escrutadas las internas del Frente de Todos y la del Frente Amplio Progresista ya mostraban una tendencia irreversible. Mientras que en Juntos por el Cambio dos listas se disputaban el triunfo voto a voto.

En el oficialismo, el candidato de la Casa Rosada y la Casa Gris, Marcelo Lewandowski, se imponía de forma contundente con el 67,06% (264.661 votos) contra el 32,93% (129.983 votos) del exministro de Defensa, Agustín Rossi.

En la interna más caliente, la de Juntos por el Cambio, Carlina Losada se imponía con el 30,61% (161.358 votos), seguida muy de cerca por el exministro de Seguridad de la provincia se imponía con el 29,81% (157.161 votos). Luego sigue Federico Angelini con el 23,69% (126.858 votos) y por último José Corral que sumó 15,98% (85.620 votos).

En tanto, en el Frente Amplio Progresista Clara García, la candidata de la lista Adelante y exmujer de Miguel Lifschitz, conseguía 67,89% de la interna (94.842 votos) contra el 32,10% (44.853 votos) de Rubén Giustiniani.