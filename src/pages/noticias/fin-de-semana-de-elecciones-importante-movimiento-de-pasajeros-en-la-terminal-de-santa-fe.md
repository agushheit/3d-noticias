---
category: La Ciudad
date: 2021-09-11T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/EXODOVOTANTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Fin de semana de elecciones: importante movimiento de pasajeros en la terminal
  de Santa Fe'
title: 'Fin de semana de elecciones: importante movimiento de pasajeros en la terminal
  de Santa Fe'
entradilla: Hasta las 19 ya habían ingresado más de 8.000 personas. Muchos son estudiantes
  que regresan a sus localidades de origen para votar.

---
La terminal de ómnibus de la ciudad de Santa Fe registró durante la jornada de este viernes mayor cantidad de viajantes que lo habitual.

Se debe a que este domingo son las elecciones Primarias Abiertas, Simultáneas y Obligatorias (PASO) y muchos estudiantes viajan a sus respectivas localidades de origen para emitir su voto.

A su vez, de la estación de la capital provincial partieron algunos colectivos que forman parte de paquetes de turismo, en su mayoría de jubilados.

Según el relevamiento que realizan en la puerta, entre las 7 de la mañana y las 14 ingresaron más de 5.000 personas. En tanto, durante la tarde (hasta las 19) el número de pasajero era de 3.200 y se esperaba que aumente en horas de la noche.