---
category: La Ciudad
date: 2021-12-13T01:04:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/orgullo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Aire Digital
resumen: 'Marcha del Orgullo en Santa Fe: una multitud reclamó la Ley Integral Trans
  y la aparición de Tehuel'
title: 'Marcha del Orgullo en Santa Fe: una multitud reclamó la Ley Integral Trans
  y la aparición de Tehuel'
entradilla: Este domingo, cientos de personas volvieron a las calles de la ciudad
  para reivindicar el Orgullo como una respuesta política a la discriminación.

---
Una bandera, un pañuelo, un paraguas, una panza que tenía escrito "amemos sin pedir permiso ni perdón". Todo se tiñó multicolor en [**Santa Fe**](https://www.airedesantafe.com.ar/santa-fe-a874) este domingo desde las 16:30. Es que, después de dos años, una multitud que se extendió a lo largo de más de seis cuadras marchó con orgullo y con la voz en alto para reclamar por una **Ley Integral Trans** y la aparición de **Tehuel de la Torre**, el joven trans que desapareció el 11 de marzo cuando salió de su casa de San Vicente.

"Al calabozo no volvemos nunca más", "Donde está Tehuel", "Señor, señora, no sea indiferente, se mata a las travestis en la cara de la gente", fueron algunos de los cantos que se escucharon en la movilización que recorrió las calles 1° Junta, Urquiza y dobló por General López para llegar a la emblemática **Plaza de Mayo**, testigo de innumerables luchas en la ciudad. En la marcha, miles de personas caminaron en un espíritu alegre y festivo, llenas de brillos y disfraces coloridos. Hasta un camión acompañó la caravana con una especie de carroza atrás en la que subía quien quería y sonaba Lali a todo volumen. En el acto central, **la consigna del acceso a la salud de la población disidente fue la preocupación más recurrente.**

![En la marcha, miles de personas caminaron en un espíritu alegre y festivo, llenas de brillos y disfraces coloridos. Hasta un camión acompañó la caravana con una especie de carroza atrás en la que subía quien quería y sonaba Lali a todo volumen. ](https://media.airedesantafe.com.ar/p/cca066abc9b4ea52f82ad22d22defc69/adjuntos/268/imagenes/003/280/0003280632/marcha-2jpg.jpg?2021-12-12-21-41-40 =1012x675)

En la marcha, miles de personas caminaron en un espíritu alegre y festivo, llenas de brillos y disfraces coloridos. Hasta un camión acompañó la caravana con una especie de carroza atrás en la que subía quien quería y sonaba Lali a todo volumen.

Las consignas que se exigieron fueron la Ley Integral Trans, una iniciativa que procura se garantice la inclusión social y los derechos en igualdad de condiciones para dicha población, una nueva Ley Nacional de respuesta integral al VIH, las Hepatitis virales, la Tuberculosis e infecciones de transmisión sexual (ITS), una Ley Antidiscriminatoria, una Ley Provincial de ESI con perspectiva de diversidad, la aprobación del cupo laboral trans en la UNL, una justicia feminista y disidente, la separación de la iglesia y el estado, y el acceso integral a la salud para personas LGBTTNB+.

En conversación con AIRE, **Alejandra Ironici,** activista trans, dijo: "Hoy marchamos porque hay derechos que están todavía en las cámaras de senadores y diputados en la provincia de Santa Fe. Si bien el cupo laboral trans viene a suplir la necesidad de tener un trabajo digno de las compañeras, aún no queda claro en la sociedad santafesina cuáles son nuestros derechos como militantes y activistas trans".

![Este domingo cientos de personas volvieron a las calles para reivindicar el Orgullo como una respuesta política a la discriminación. ](https://media.airedesantafe.com.ar/p/a010c91f79cc402a03ae685f1886a8f9/adjuntos/268/imagenes/003/280/0003280671/marcha-del-orgullo-santa-fe-10jpg.jpg?2021-12-12-21-45-35 =1012x675)

Este domingo cientos de personas volvieron a las calles para reivindicar el Orgullo como una respuesta política a la discriminación.

Gonzalo Gorosito / Aire Digital

Además, afirmó que la sociedad avanzó, pero que todavía quedan muchas cosas por cambiar. "Tenemos que cambiar a una sociedad que nos vea como sujetas de derecho a poder tener un trabajo digno, formar una familia, acceder a sistemas de salud integrales y a la vivienda, que es una de las grandes problemáticas que esta comunidad tiene", denunció.

Mientras sonaba Bad Romance, de Lady Gaga, una participante de la movilización, **Ayelén Gioldi**, contó a **AIRE** que es la primera marcha que participa en Santa Fe, ya que volvió después de mucho tiempo a la provincia. "Estoy acá porque el mundo es diverso, porque nadie nace heterosexual, y porque necesitamos la lucha disidente para entender el mundo", aseguró.

Sobre las consignas que la movilizan, puso en primer lugar saber dónde está Tehuel, en segundo lugar que el cupo trans se cumpla, no solo que se reglamente. Y en tercer lugar que se respeten las niñeces trans. "Veo la sociedad santafesina tímida respecto de esta lucha y que no se anima aún. Falta mucho, se avanzó poco, lo veo como una necesidad y una urgencia", sostuvo.

![Una de las consignas más se escuchó durante la marcha fue ](https://media.airedesantafe.com.ar/p/b229c6cd1d00ddc4ebedde9bd1a9c87d/adjuntos/268/imagenes/003/280/0003280658/marcha-del-orgullo-7jpg.jpg?2021-12-12-21-43-32 =1012x675)

Una de las consignas más se escuchó durante la marcha fue "dónde está Tehuel?".

Gonzalo Gorosito / Aire Digital

Por su parte, **Cristian Moreyra** y **Fernánda Gutierrez**, asistentes de la jornada de lucha y orgullo, en conversación con AIRE fueron contundentes en que estaban presentes porque "entendemos que si la comunidad LGBTIQ+ no sale a marchar, los gobiernos que trascendieron durante toda la historia no se hacen cargo de nuestros derechos. Por el contrario, siempre hay recortes para las minorías. Por eso estamos hoy en la calle".

Para Gutiérrez, "es muy importante esta movilización, que hayamos colmado la Plaza de Mayo de Santa Fe con los reclamos que son la Ley Integral Trans, la separación de la iglesia del estado. También estamos en contra del acuerdo con el FMI, que eso va a implicar más recortes de derechos que estamos sufriendo las mujeres y disidencias del polo trabajador", dijo y consideró que en Santa Fe la juventud está siendo parte de una "revolución feminista de la nueva ola", que vino a cuestionar todo: las instituciones, la justicia, la iglesia, y cree que es fundamental la movilización y la conquista de más derechos.

![Los varones trans de Santa Fe también alzaron su voz en la Marcha del Orgullo 2021.](https://media.airedesantafe.com.ar/p/ee3d0fc6602cb5b7fa8ff1b7a881978d/adjuntos/268/imagenes/003/280/0003280697/marcha-del-orgullo-5jpg.jpg?2021-12-12-21-47-29 =1012x675)

Los varones trans de Santa Fe también alzaron su voz en la Marcha del Orgullo 2021.

Gonzalo Gorosito / Aire Digital

Por su parte **Josefina**, integrante de la **Mesa del Orgullo Santa Fe**, dijo a **AIRE** que el motivo de la marcha es "defender los derechos conquistados y por todos los que nos hacen falta que son un montón. También por la visibilidad, porque el Orgullo es una respuesta política". Además, coincidió con la opinión de Gioldi, y consideró que "la sociedad santafesina es muy conservadora, falta mucho, por eso invitamos a todos a que se sumen a estos eventos, nos acompañen y que sean parte de esta lucha por una sociedad más igualitaria".

En el acto se desarrolló una feria de emprendimientos de personas que conforman la comunidad, y hubo comida, accesorios, ropa y cuadros de arte. Además, un festival de artistas disidentes se vio frustrado por la lluvia que cayó sobre la ciudad cerca de las 20:00.

![Algunas personas acompañaro la Marcha del Orgullo 2021 desde sus balcones.](https://media.airedesantafe.com.ar/p/b684f448cbb82d884af1ebc1551715e2/adjuntos/268/imagenes/003/280/0003280624/1200x0/smart/marcha-3jpg.jpg =1200x0)

Algunas personas acompañaron la Marcha del Orgullo 2021 desde sus balcones.