---
category: La Ciudad
date: 2020-12-03T11:05:11Z
thumbnail: https://assets.3dnoticias.com.ar/seguridad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Convocan a una caravana por la inseguridad en la ciudad
title: Convocan a una caravana por la inseguridad en la ciudad
entradilla: Este jueves, a partir de las 18, habrá una concentración en el Faro reclamar
  mayor seguridad.

---
Es convocada por la Subcomisión de Seguridad de la "Vecinal B° Roma", que extendió la invitación a toda la ciudad. La Red de Vecinales ya dio su apoyo, y también participarán los chicos los deliveries, la Cámara de Kiosqueros y vecinos autoconvocados.

Jorge Tira, el Presidente de la Sub Comisión de Seguridad de la Vecinal Barrio Roma, brindó detalles a El Litoral. "El pasado 8 de octubre hicimos la primera de este tipo de protestas en modo caravana. Invitamos a toda la ciudadanía porque este es un problema de todos, no solo de un barrio determinado".

Hablando concretamente de Barrio Roma, Tira dijo: "Todos los días tenemos hechos de inseguridad. Las metodologías varían. Siempre andan en moto, sin patente. El último sábado, a las 8.30 de la noche, a un vecino de Tucumán al 3900 le quisieron robar. No pudieron porque el señor empezó a los gritos y salieron otras personas. Ahí eran 3 en dos motos. Ese mismo día, también a un auto que estaba parado le rompieron la luneta y le robaron una mochila y herramientas. Y esto es algo que ocurre en todos los barrios de la ciudad".

"La presencia policial es insuficiente. Hay móviles nuevos, pero es escasa la presencia. Hay algunos vecinos que ya están hartos de ser ignorados y no van a hacer la denuncia cuando sufren un robo. Nosotros siempre tratamos de decirle que lo hagan, pero están cansados de que siga todo siempre igual por más denuncia que se radique", finalizó Tira.

Desde el 8 de octubre, los vecinos no tuvieron ninguna respuesta. Pero van a insistir: presentarán notas en Casa de Gobierno, y otra en el Ministerio de Seguridad pidiendo una reunión con Saín.