---
category: Agenda Ciudadana
date: 2021-09-15T06:15:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/MALVINAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Identificaron los restos de seis soldados argentinos inhumados en las Islas
  Malvinas
title: Identificaron los restos de seis soldados argentinos inhumados en las Islas
  Malvinas
entradilla: Las muestras analizadas por el Equipo Argentino de Antropología Forense
  en Córdoba determinaron las identidades.

---
El Comité Internacional de la Cruz Roja (CICR) informó este martes que fueron identificados los restos de seis soldados argentinos inhumados en las Islas Malvinas, en el marco del Segundo Plan de Proyecto Humanitario (PPH 2), informó el organismo en un comunicado.

El CICR junto a autoridades nacionales comunicaron a los familiares de los excombatientes cuyos restos se encuentran en la tumba C.1.10, en el Cementerio de Darwin, los resultados alcanzados en la segunda etapa del plan, lo que "ha puesto fin a casi 40 años de incertidumbre sobre lo sucedido a sus seres queridos", destaca el documento.

Días atrás, en el marco del Segundo Plan de Proyecto Humanitario (PPH 2), se exhumaron los restos de varias personas que estaban en una sola tumba, conocida como C.1.10, en el Cementerio de Darwin, y tras la excavación de la sepultura, el equipo del CICR halló los restos de seis exsoldados argentinos.

Las muestras de ADN fueron trasladadas al laboratorio de Genética Forense que el Equipo Argentino de Antropología Forense (EAAF) tiene la ciudad de Córdoba, a finales de agosto pasado, en un vuelo privado proveniente de las Islas Malvinas.

Esas muestras fueron analizadas y cruzadas con las muestras de ADN aportadas por los familiares, alcanzando resultados positivos para la identificación, se indicó.

El resultado del análisis genético determinó 4 nuevas identidades que se encontraban inhumados en la sepultura C.1.10: Subalférez Guillermo Nasif; Cabo primero Marciano Verón, Cabo primero Carlos Misael Pereyra y Gendarme Juan Carlos Treppo.

Además, se confirmó la identidad del Primer alférez Ricardo Julio Sánchez, que había sido inhumado en la C.1.10 con nombre y se reasociaron restos óseos del Cabo Primero Víctor Samuel Guerrero, quien se encuentra inhumado en una tumba individual ya identificada.

Los 6 gendarmes murieron en combate en el mismo evento, a raíz del incendio sufrido en el helicóptero que viajaban y que fue derribado en Monte Kent en 1982.

El presidente Alberto Fernández indicó en su cuenta de Twitter que "el Equipo Argentino de Antropología Forense y la Cruz Roja identificaron los restos de cuatro excombatientes nuestros caídos en Malvinas.

El resultado del análisis genético determinó 4 nuevas identidades que se encontraban inhumados en la sepultura C.1.10: Subalférez Guillermo Nasif; Cabo primero Marciano Verón, Cabo primero Carlos Misael Pereyra y Gendarme Juan Carlos Treppo.

Mi abrazo emocionado, 39 años después, a sus familiares. Gloria a quienes murieron por la Patria".

En tanto, la Cancillería argentina celebró la identificación de seis soldados argentinos inhumados en el Cementerio de Darwin.

"Gracias al excelente trabajo desplegado por el equipo forense del CICR y el personal del Laboratorio del EAAF (Equipo Argentino de Antropología Forense) fue posible identificar los restos de 6 excombatientes argentinos caídos en las Malvinas que se encontraban en la tumba C.1.10 del Cementerio de Darwin", expresó el Palacio San Martín en un comunicado.

De acuerdo con lo previsto en el PPH2, los familiares expresaron ya su voluntad respecto del lugar de descanso de sus familiares en el Cementerio de Darwin, indicó la Cancillería y precisó que en algunos casos han preferido que los restos de sus familiares permanezcan juntos y en otros casos han optado por tumbas separadas.

"Pudimos identificar los restos de seis personas y dar respuestas a sus familiares, después de tantos años. Todas las familias merecen saber qué les sucedió a sus seres queridos; me conmueve mucho ser parte de este proceso y poner fin a la incertidumbre de las familias", remarcó Laurent Corbaz, jefe del proyecto del PPH 2.Y agregó: "Queremos agradecer a todos los que brindaron su inquebrantable apoyo para hacer posible este proyecto, tanto en las islas como en otras partes".

Por su parte, Luis Bernardo Fondebrider, jefe de la Unidad Forense del CICR,destacó que "la misión ha sido fructífera, pese a que las condiciones meteorológicas fueron a veces muy duras" y agregó que "la identificación fue posible a través de la aplicación de estándares forenses internacionales y de un enfoque multidisciplinario. De esta forma, esperamos ayudar a aliviar el sufrimiento de las familias".

El equipo del CICR también exploró una zona llamada Caleta Trullo/Teal Inlet para determinar si había restos de combatientes argentinos, pero no hubo ningún hallazgo.

**Las anteriores identificaciones**

Estas tareas fueron una continuación del Primer Plan de Proyecto Humanitario (PPH 1), que condujo a la exhumación de los restos de 122 soldados argentinos en el Cementerio de Darwin.Luego de realizar el análisis de las muestras de ADN de las familias, se logró identificar a 115 soldados.

Cabe señalar que el PPH2 fue suscripto por los gobiernos de la República Argentina y el Reino Unido de Gran Bretaña e Irlanda del Norte y el CICR en marzo de 2021.

Con la misma perspectiva humanitaria que tuvo la primera fase iniciada en 2012, estos acuerdos tienen el fin de llegar a la identificación de los restos de los soldados argentinos que lucharon por la recuperación del ejercicio de la soberanía nacional en las Malvinas y perdieron la vida en las islas, así como para dar respuestas a sus familias en cuanto al lugar donde rendir honores a sus seres queridos, destacó la Cancillería argentina.