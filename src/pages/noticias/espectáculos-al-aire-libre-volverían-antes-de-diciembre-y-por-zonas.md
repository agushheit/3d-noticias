---
layout: Noticia con imagen
author: .
resumen: Espectáculos al aire libre
category: Agenda Ciudadana
title: "Espectáculos al Aire Libre: volverían antes de diciembre y por zonas"
entradilla: "“Estamos pensando en eventos al aire libre, en parques y sin
  amontonamiento de gente”, explicó el ministro de Cultura de Santa Fe, Jorge
  Llonch. "
date: 2020-11-04T13:48:26.900Z
thumbnail: https://assets.3dnoticias.com.ar/espectaculos.jpg
---
El ministro de Cultura de Santa Fe, Jorge Llonch, se refirió al posible regreso de los espectáculos al aire libre en la provincia y explicó que desde su área se trabaja, junto al Ministerio de Salud, en el desarrollo de un protocolo que permita recuperar la actividad cultural al aire libre, tras casi ocho meses de parate obligado por la pandemia de coronavirus.

El gobernador Omar Perotti habría manifestado su intención de avanzar con la propuesta y el posible plazo para que eso ocurra es “antes de que termine noviembre, siempre que lo habilite el Ministerio de Salud”.

El ministro aclaró que no sería una medida generalizada para toda la provincia, sino que se tendrán en cuenta las condiciones sanitarias de las diferentes zonas.

“Estamos pensando en eventos al aire libre, en parques y sin amontonamiento de gente”, detalló Llonch.

**Autocine en la provincia**

Al ser consultado sobre la posibilidad de asistir a funciones de cine al aire libre, el ministro de Cultura anticipó que existe una propuesta de un grupo empresario de Funes para ofrecer espectáculos, algunos de ellos “importantes”, en un predio de esa ciudad santafesina.

La oferta es para “500 autos, con escenario giratorio y un protocolo de cuatro personas por auto”. “Con un ticket de 2500 pesos por auto, les da un número para espectáculos grandes”, concluyó.