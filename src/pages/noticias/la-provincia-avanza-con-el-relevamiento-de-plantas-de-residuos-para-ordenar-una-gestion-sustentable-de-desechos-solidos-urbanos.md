---
category: Agenda Ciudadana
date: 2020-12-05T11:48:57Z
thumbnail: https://assets.3dnoticias.com.ar/GONNET.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Relevamiento de Plantas de Residuos para ordenar una gestión sustentable
  de desechos sólidos urbanos
title: Relevamiento de Plantas de Residuos para ordenar una gestión sustentable de
  desechos sólidos urbanos
entradilla: "“Santa Fe tiene más basurales a cielo abierto que localidades, evidentemente
  la gran inversión de años anteriores no dio resultados ambientalmente positivos”,
  resaltó Gonnet."

---
El gobierno de Santa Fe, a través del Ministerio de Ambiente y Cambio Climático, se encuentra desarrollando un relevamiento de las plantas de tratamiento de residuos disponibles en la provincia. “Tenemos megaplantas de tratamiento que están paradas y, a metros, basurales y gente trabajando en ellos”, resaltó la ministra Erika Gonnet, luego de su paso por Villa Gobernador Gálvez y Venado Tuerto.

Por ello, la funcionaria se mostró partidaria de “no bajar a las localidades con acciones enlatadas” sino más bien “trabajar junto con los municipios y comunas, en esquemas que contemplen las distintas realidades de cada una de ellos”.

En ese sentido, señaló que “desde el comienzo de la gestión, los intendentes y presidentes comunales nos manifiestan esta problemática. Es llamativo, porque la provincia tenía armado un esquema de más de 30 consorcios, pero nos encontramos con que funcionan apenas tres o cuatro y no completamente”.

Y remarcó: “Nos encontramos con que la provincia tiene más basurales a cielo abierto que localidades. Nos extrañó que tengamos esta problemática, después de tantos años de inversiones millonarias”.

Cabe señalar que, pese a la pandemia de Covid 19, que demoró algunos plazos, ya se relevaron las situaciones en Reconquista, Esperanza, Emilia, Cayastá, Helvecia, Villa Gobernador Gálvez, Venado Tuerto y el consorcio De La Costa, entre otros.

## **Villa Gobernador Gálvez**

Durante su recorrida por Villa Gobernador Gálvez, Gonnet remarcó que la ciudad “tiene la planta más grande de la provincia, con una inversión muy importante. Sin embargo, hoy tiene 10 centímetros de heces de paloma en todas las instalaciones que se inauguraron en 2017 y no se pusieron en marcha nunca”. Y añadió: “A menos de 50 metros, en un mar de residuos, vemos mucha gente revolviendo basura”.

Es por eso que enfatizó: “Tiene que haber una responsabilidad que nos pare a todos en el mismo lugar y podamos abordar este tema porque tener estructuras millonarias paradas y a metros gente revolviendo la basura, es algo que amerita una solución que la construyamos entre todos”.

## **Venado Tuerto**

Seguidamente, en la localidad de Venado Tuerto, Gonnet remarcó: “Tenemos una planta que es una gran inversión, de todas maneras, sigue el basural a cielo abierto y esa planta está parcialmente en funcionamiento. Continuamos avanzando en propuestas que permitan ampliar las prestaciones de la planta e incorporar a localidades de la región”.

“Estamos trabajando en muchas localidades y abordando el tema, es un gran desafío para nosotros y los jefes territoriales que integran el consorcio, para ver cómo lo ponemos en funcionamiento y damos pasos importantes en la erradicación de basurales, en la separación de residuos en origen y en la generación de nuevos subproductos derivados”, finalizó la funcionaria provincial.

## **Plantas paradas**

La Ing. Saida Caula, miembro del equipo técnico que realiza el relevamiento, agregó por su parte que “la postal en todos los sitios es similar, plantas paradas y recuperadores informales, en su mayoría mujeres, trabajando en condiciones que no son las mejores”.

Así, las acciones consisten en “acompañarlos en el proceso de la gestión integral de los residuos sólidos urbanos, técnica, legal y normativamente”, repasó, y añadió que “el Ministerio sacó recientemente un modelo de Ordenanza para ayudar a municipios y comunas a que se introduzcan en la gestión de residuos. Estamos convencidos de trabajar no solo en la temática ambiental desde el punto de vista técnico, sino también acompañado de la situación social de la localidad”, completó Caula.

Por último, cabe señalar que, en Venado Tuerto, la ministra Gonnet recorrió la planta de tratamiento de residuos junto al intendente Leonel Chiarella, los presidentes comunales de Carmen, Walter Czelada, y de Murphy, Marcelo Camussoni, y los concejales venadenses Patricio Marenghini y Emilce Cufre. Además, por el Ministerio estuvieron presentes el secretario de Políticas Ambientales, Arq. Oreste Blangini, el subsecretario de Tecnologías para la Sostenibilidad, Ing. Franco Blatter, y la asistente técnica, Ing. Saida Caula.