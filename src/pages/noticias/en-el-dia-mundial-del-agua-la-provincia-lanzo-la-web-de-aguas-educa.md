---
category: Estado Real
date: 2021-03-23T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/aguas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: En el Día Mundial del Agua, la provincia lanzó la web de Aguas Educa
title: En el Día Mundial del Agua, la provincia lanzó la web de Aguas Educa
entradilla: La página www.aguaseduca.ar está destinada a alentar sobre el uso responsable
  y solidario de este elemento vital.

---
El Gobierno de Santa Fe, a través de Aguas Santafesinas, lanzó el sitio web www.aguaseduca.ar como parte del programa Aguas Educa, destinado a seguir formando a los chicos y chicas de toda la provincia en el uso responsable y solidario de este elemento esencial.

El acto, realizado en la planta potabilizadora Rosario, se realizó en el marco del Día Mundial del Agua y contó con la participación de las ministras de Educación, Adriana Cantero; de Ambiente y Cambio Climático, Erika Gonnet; y de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana. También se sumaron el director provincial de Desarrollo Territorial - Zona Sur del Ministerio de Desarrollo Social, Camilo Scaglia; la vicepresidenta de Aguas Santafesinas, Marisa Gallina; los directores Juan Manuel Costantini y Oscar Barrionuevo (secretario general del Sindicato de Obras Sanitarias Rosario) y gerentes y trabajadores de la empresa.

Al referirse al lanzamiento, Cantero destacó que “la problemática del agua es increíblemente actual, es uno de los recursos naturales más valiosos que tenemos y no nos damos cuenta de ese patrimonio impresionante que tenemos. Por eso, es muy importante que aprendamos a cuidarlo. El agua es un eje para trabajar desde lo educativo e interdisciplinario, nos permite incorporar todo, la política, la plástica, las ciencias naturales, la organización económica, todo está relacionado con la problemática del agua. Cuando nació el programa Aguas/educa también era ministra en ese momento y ahora estoy viendo todo el desarrollo que ha tenido el programa, tenemos que ir por algo más para que todo ese trabajo no sea un tema sólo de Aguas, sino que comprometa al sistema educativo. Entonces. esa vinculación en tramas seguramente va a hacer que trabajemos conjuntamente”.

Por su parte, Frana resaltó que “en esta gestión provincial, Aguas es una empresa comprometida con la realidad, que plasma un trabajo comprometido e inédito, para concientizar a nuestras generaciones y a las futuras sobre un recurso que no abunda. Quiero felicitar a los funcionarios y empleados de Aguas por esta iniciativa. Es la mejor forma de honrar este Día Mundial del Agua”.

Por último, Gonnet celebró luego que “este proyecto esté encarado por gente joven que se involucra en el desarrollo de actividades en donde un recurso tan importante como el agua es, sin duda, un motivo que nos une y nos invita a ser parte y trabajar juntos”.

**SOBRE LA PÁGINA WEB**

La nueva web www.aguaseduca.ar tiene como objetivo constituirse en una herramienta de aprendizaje y de recursos para las actividades que despliega el programa Aguas Educa de Aguas Santafesinas, destinado a todos los niveles educativos.

Durante el 2020 las actividades llegaron de manera virtual a más de 6.000 niños y niñas de toda la provincia.

La página es una herramienta de trabajo para los docentes y alumnos y alumnas, dado que Aguas es la única empresa de saneamiento del país que lleva realiza una comunicación presencial y directa con las escuelas. Esa comunicación se daba, antes de la pandemia, con visitas a los dispositivos educativos: los espacios Aguas Educa de Rosario y de Santa Fe; más la presencia en otras ciudades del área de servicio. La pandemia, con el cese de la presencia de docentes y estudiantes en las escuelas, planteó un gran desafío que se superó gracias a la continuidad que las mismas escuelas, los mismos docentes y los mismos niños y niñas tomaron como propio para el programa: la virtualidad.