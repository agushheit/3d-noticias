---
category: Estado Real
date: 2021-10-01T06:00:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/VIETNAM.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Omar Perotti encabezó un encuentro de vinculación comercial con el embajador
  de Vietnam
title: Omar Perotti encabezó un encuentro de vinculación comercial con el embajador
  de Vietnam
entradilla: "“Hay buena perspectiva a futuro en este intercambio y en tener a Vietnam
  como una puerta de vínculo con todos los países asiáticos”, dijo el gobernador."

---
El gobernador Omar Perotti encabezó este jueves, junto al embajador extraordinario y plenipotenciario de Vietnam en Argentina, Thanh Duong Quoc, un encuentro de vinculación comercial en el marco del programa “Santa Fe mira a Asia”, que busca el fortalecimiento de los lazos empresariales entre la provincia y ese país del sudeste asiático.

La actividad se desarrolló en la sala Rodolfo Walsh de la Sede de Gobierno en Rosario, y contó además con la presencia del ministro de Producción, Ciencia y Tecnología, Daniel Costamagna; y el secretario de Comercio Exterior, Germán Bürcher.

Durante el encuentro, el gobernador de la provincia destacó que “Argentina necesita potenciar su comercio internacional, diversificando y aprovechando en su plenitud el potencial de nuestros recursos naturales y toda la incorporación de valor agregado, de tecnología, y de hacernos conocer en esa potencialidad del talento de nuestra gente”.

Luego, Perotti se refirió a la importancia de conocer en profundidad a aquellos países que demostraron interés comercial en los productos santafesinos: “Esto requiere de un esfuerzo adicional de parte nuestra, de incorporar el conocimiento de esa cultura, de esa idiosincrasia, de esa forma de comercialización, porque ahí va a estar la posibilidad real y concreta de nuestras operaciones”, explicó.

“Cuando nos familiarizamos y conocemos la relación -continuó el mandatario-, el trato es mucho más directo y los beneficios también. Por lo cual, estrechar este vínculo es esencial para crecer en el negocio de ida y vuelta”. Y agregó: “Si conseguimos consolidar esa marca, detrás de ese conocimiento, cada uno de ustedes puede seguramente empezar a hablar con un cliente”.

Sobre el final, el mandatario santafesino dijo que “hay una perspectiva muy buena a futuro en este intercambio y en tener a Vietnam como una puerta del vínculo con todos los países asiáticos”. Y concluyó: “Nos ayuda a la inteligencia comercial y facilita estrechar los vínculos de confianza que son los que terminan generando oportunidades de negocios. Es trabajo para nuestra gente; esa es nuestra tarea fundamentalmente”.

**Mercado para productos santafesinos**

A su turno, el ministro Daniel Costamagna destacó que “el programa es un orgullo y un trabajo muy fuerte de articulación en lo que tiene que ver con los aspectos productivos y tecnológicos. En todo aquello que tiene que ver con lo que es nuestra provincia y la necesidad imperiosa del mejoramiento de la calidad de nuestros productos, sobre todo alimentos, agroindustria y laboratorios”.

Luego, dirigiéndose a la comitiva vietnamita, resaltó que “estamos deseosos que conozcan nuestra provincia, nuestro entramado institucional, a nuestros productores y a nuestros empresarios”.

Por su parte, el embajador de Vietnam en Argentina subrayó la magnitud que posee ese mercado asiático en el mundo. “Vietnam es una economía abierta en pleno desarrollo, manteniendo un crecimiento sostenido en los últimos 35 años. Somos el segundo país con más crecimiento económico en las últimas dos décadas, solo superado por China. Tenemos una fuerza laboral joven y con precios competitivos”, afirmó.

Y agregó: “Tenemos 15 acuerdos de libre comercio con varias economías. Por lo tanto, Vietnam abre la puerta a muchos mercados importantes en el mundo. El mercado de Vietnam abre muchas oportunidades a empresarios argentinos”.

Finalmente, Thanh Duong Quoc remarcó la importancia de los productos santafesinos en el mercado vietnamita: “Los productos de Santa Fe son apreciados por los importadores por su precio y calidad, como maíz, soja, carne bovina, productos lácteos, medicamentos, vacunas, insumos veterinarios y médicos, entre otros”.

**SANTA FE MIRA A ASIA**

El programa “Santa Fe Mira a Asia” es un instrumento que busca desarrollar mercados estratégicos con China, India y Vietnam, proponiendo el abordaje de estos tres países, con la complejidad que requieren desde sus aspectos culturales, sociales, políticos, científico-tecnológicos y comerciales.

Vietnam es el segundo socio comercial de Santa Fe, después de la India. Las exportaciones a este destino representan el 8% del total provincial (33% del total de las exportaciones argentinas a este país) por un monto que asciende a los U$D 710.596.159 en lo que va de este año. Entre los principales temas identificados se buscan desarrollar oportunidades para potenciar el comercio exterior, la economía del conocimiento, la cooperación en materia ambiental, de salud y a nivel cultural.

**Agenda del embajador en la provincia**

El embajador, junto a su delegación, mantienen una intensa agenda de trabajo en las ciudades de Rosario, Santa Fe y Rafaela. El miércoles, visitaron las instalaciones de la firma biotecnológica Terragene, ubicada en la localidad de Alvear. Incluye también visitas a la firma de maquinaria agrícola y siembra directa Bertini; al Ente Portuario de Rosario (Enapro); a la Bolsa de Comercio de Rosario; al Laboratorio Industrial Farmacéutico Santa Fe (LIF); a la Universidad Nacional de Rafaela (UNRaf); y a los centros regionales del Instituto de Tecnología Agropecuaria (INTA) y del Instituto Nacional de Tecnología Industrial (INTI) de Rafaela.