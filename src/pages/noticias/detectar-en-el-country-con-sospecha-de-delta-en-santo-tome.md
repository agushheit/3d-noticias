---
category: La Ciudad
date: 2021-09-15T06:15:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/DETECTARSANTOTO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: DetectAR en el country con sospecha de Delta en Santo Tomé
title: DetectAR en el country con sospecha de Delta en Santo Tomé
entradilla: En la tarde de este martes se conocerá si los casos de Covid en un country
  de Santo Tomé son de la variante Delta. Implementarán el Plan DetectAR.

---
En un country de Santo Tomé, una familia que llegó del exterior el pasado 4 de septiembre presentó tres casos positivos de coronavirus en el hisopado de rigor que se realiza a la semana de ingresar al país. Se sospecha que puede tratarse de la variante Delta.

El secretario de Salud Santo Tomé, Daniel Susmann, explicó que las muestras se enviaron al Instituto Malbrán el sábado y la confirmación demora 72 horas, por lo que en la tarde de este martes debería darse la confirmación.

“Las autoridades del Ministerio de Salud ya estaban al tanto de esta familia que había regresado de Estados Unidos; habían sido hisopados en Ezeiza y Migraciones avisó al Ministerio que se trasladaba esta familia acá y tenían que cumplir los siete días de aislamiento. Al séptimo día se les realizó el hisopado como dice el protocolo y se detectó que tres miembros daban positivo, con una gran posibilidad de que sea la variante Delta”, relató Susmann.

Los tres contagiados están en perfecto estado de salud y nunca tuvieron síntomas, pero debían cumplir con los siete días de aislamiento dispuestos por decreto nacional.

Según el secretario de Salud de la ciudad vecina, el día anterior a realizarse el hisopado un integrante de la familia concurrió a una fiesta, por lo cual hay 22 personas más aisladas. “Los hisopados les han dado negativo y están en perfecto estado de salud cumpliendo aislamiento, para volverse a hisopar dentro de siete días y estar seguros de que no han sido contagiados”.

Si bien no supo responder sobre la sanción que corresponde, Susmann dijo que la preocupación es llevar tranquilidad al barrio. “Por lo pronto, junto con el Ministerio vamos a realizar un Plan DetectAR en el barrio, por las dudas de que haya habido algún otro tipo de contacto”.

Aclaró que el Plan DetectAR se realizará este mismo martes y “tal vez mañana” en el barrio, pero no confirmó un despliegue temporal y geográfico más extenso, porque eso lo dispone el Ministerio de Salud de la Nación. “Cuando vienen nosotros hacemos todo el operativo de logística. Bienvenidos sea que se haga. Ya se hicieron varios durante la pandemia así que siempre es importante”, opinó.

Además, destacó: “Lo bueno de todo esto es que el plan de vacunación avanza en forma muy buena. Ayer cumplimos con 75.000 dosis aplicadas acá en Santo Tomé así que estamos muy contentos con eso, y se nota. Se nota porque venimos con varios días de casos cero, así que evidentemente la vacunación está dando sus buenos resultados”.

Finalmente informó que los encargados del country acompañan el operativo y se encargaron de avisar a todos los vecinos para que concurran a hisoparse.