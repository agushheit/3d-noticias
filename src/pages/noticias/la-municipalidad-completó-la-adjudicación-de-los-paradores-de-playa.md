---
layout: Noticia con imagen
author: .
resumen: Paradores de playa
category: La Ciudad
title: La Municipalidad completó la adjudicación de los paradores de playa
entradilla: Se realizó este lunes la segunda apertura de sobres. De esta manera,
  se cerró el proceso con la adjudicación de seis paradores. Abrirán a partir de
  diciembre.
date: 2020-11-18T13:18:59.545Z
thumbnail: https://assets.3dnoticias.com.ar/foto-paradores.jpg
---
Este lunes se concretó la segunda apertura de sobres correspondiente al llamado a licitación para administrar los paradores de playa de la capital provincial. En esta oportunidad, se ofrecieron tres espacios, dos de los cuales recibieron ofertas: dos firmas se presentaron por el Espigón II, mientras que una lo hizo por el Parador 7, ubicado en Costanera Este.

El parador del Espigón II se compone de equipamiento de playa temporal de estructura desmontable y mobiliario urbano permanente sobre la Costanera consistente en baños públicos y un kiosco que funcionará durante todo el año. El que se ubica en el Paseo Néstor Kirchner, en tanto, tendrá equipamiento de playa temporal de estructura desmontable.

De esta manera, se completó la adjudicación de seis paradores, por un plazo de tres años, que comenzarán a funcionar durante la temporada de verano 2020-2021. Se ubican en Espigón I, el Espigón II, el Centro de Deportes Municipal, la Costanera Este, Punta Norte en Costanera Este, y el Paseo Néstor Kirchner. Todos ellos tendrán concesionarios a su cargo que prestarán servicio de playa y desarrollarán actividades gastronómicas, deportivas y lúdicas, entre otras.

El objetivo es que los administradores generen nuevos espacios de servicios y mejoren los ya existentes, incorporando baños públicos, duchas y vestuarios para guardavidas. En esas tareas trabaja cada concesionario, a los efectos de llegar a la primera semana de diciembre próximo, cuando se inaugure oficialmente la temporada.

El secretario de Producción y Desarrollo Económico municipal, Matías Schmüth, indicó que es “una muy buena noticia contar con seis concesionarios que hayan elegido la ciudad para invertir y para poder brindar servicios”. Según explicó, en el contexto de la pandemia por COVID-19, las playas tendrán esta temporada “una ocupación especial, lo que será una oportunidad porque muchos santafesinos van a elegir Santa Fe para vacacionar”.

Schmüth subrayó que, a partir de diciembre próximo, “tendremos seis paradores que prestarán servicios y brindarán a la ciudad infraestructura para un mejor disfrute de los momentos de ocio”. El objetivo es llegar a la apertura de la temporada “con los paradores en funcionamiento, las playas preparadas y los protocolos necesarios para que la actividad se desarrolle de manera cuidada”.

El funcionario insistió en que la intención es “potenciar a la ciudad en esta temporada que, por sus características particulares, esperamos que sea muy buena para la capital de la provincia, con mucha afluencia de público en plazas, parques y playas, sobre todo. La premisa de la Municipalidad es que estos espacios se encuentren en las mejores condiciones, de modo que sean un gran atractivo para la región”, concluyó.

Estaba previsto un séptimo parador -el N° 3- pero su concesión quedó desierta por no haber ofertas.

![](https://assets.3dnoticias.com.ar/parador1.jpg)