---
category: La Ciudad
date: 2021-01-10T11:16:01Z
thumbnail: https://assets.3dnoticias.com.ar/100121-controles-playas-santafesinas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: UNO Santa Fe'
resumen: Controles y prevención en las playas y parques de la ciudad de Santa Fe
title: Controles y prevención en las playas y parques de la ciudad de Santa Fe
entradilla: Tal como  se anunció, comenzaron los operativos de control para evitar
  aglomeración de personas. Se realizan test de olfato, toma de temperatura, hay más
  patrullajes y policías caminantes

---
**Este fin de semana se lleva adelante un operativo especial en playas y parques de la ciudad, con el objetivo de evitar la aglomeración de personas**.

Tal como se había anticipado, las tareas se llevan adelante con más de 200 personas, entre las que se cuentan agentes sanitarios, integrantes del Cobem y la GSI, inspectores de tránsito, profesores de educación física, guardavidas, integrantes de la Policía de la provincia y fuerzas federales como Prefectura, Policía Federal, Gendarmería y la Policía de Seguridad Aeroportuaria.

Los operativos se montarán de forma permanente en el Parque del Sur, el Parque Garay y la Costanera Este, y de modo itinerante a lo largo de toda la Costanera Oeste de la capital, el viernes, sábado y domingo de 14 a 19 horas. 

**En todos estos espacios se fijarán postas de cuidados que realizarán test de anosmia, toma de temperatura y difusión de medidas de seguridad entre la población.**

En Costanera Este hay dos controles para el ordenamiento del tránsito: uno en el ingreso al paseo y otro a la altura de la rotonda, en el inicio de la Avenida Néstor Kirchner. Allí se realizan controles de olfato y temperatura, igual que a quienes ingresen a pie. Del mismo modo, hay recorridas permanentes por el área de playa efectuadas por efectivos policiales caminantes a los que se sumarán actores, mimos y payasos que recordarán las medidas de prevención generando conciencia a través del arte. La intención es completar tareas disuasivas entre los presentes y evitar las aglomeraciones.

También participan guías de turismo de la Agencia para el Desarrollo de Santa Fe y su Región (Ader) que se ubicarán en tres postas situadas en la Costanera Este, la Costanera Oeste y el kilómetro 0,5 de la Ruta Provincial 1. Los mismos estarán acompañados por integrantes de la Agencia Provincial de Seguridad Vial y completarán tareas de concientización entre quienes circulen por esos lugares.