---
category: Agenda Ciudadana
date: 2021-06-09T08:42:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/in1623189065674.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Cómo es el nuevo programa de precios y qué productos incluye
title: Cómo es el nuevo programa de precios y qué productos incluye
entradilla: El Gobierno lanzó un nuevo acuerdo de precios para reemplazar Precios
  Máximos. Los detalles y la lista de artículos.

---
El Gobierno lanzó este martes el programa "Súper Cerca", por el cual se anunció un acuerdo de precios fijos por seis meses e impresos en los envases, con el objetivo de cuidar la canasta familiar.

Se logró un acuerdo con 24 empresas del sector por 70 productos de alimentos, limpieza e higiene personal, y cuyo objetivo es cuidar la canasta familiar, en función de la inflación registrada en los últimos meses, sobre todo en el rubro alimentos.

    https://twitter.com/produccion_arg/status/1402362882982125575?s=20

Con el nuevo programa daría fin a otro: el de Precios Máximos, que fue diseñado en el momento de mayor incertidumbre de la pandemia del coronavirus.

El nuevo plan está compuesto por los productos más representativos de los rubros alimentos, artículos de limpieza, perfumería y bebidas. Incluirá además primeras marcas de las empresas más reconocidas.

Entre las novedades del programa está que el precio de los productos estará impreso en el packaging, lo que facilitará el control por parte de los consumidores y que no haya remarcaciones según la región del país.

Consultá [aquí ](https://www.argentina.gob.ar/produccion/supercerca)la lista completa de productos.