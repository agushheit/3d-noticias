---
category: Agenda Ciudadana
date: 2021-02-26T06:54:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/alimentar.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de El Litoral
resumen: 'Tarjeta Alimentar: ¿La plata no gastada se acumula para el siguiente mes
  o se pierde?'
title: 'Tarjeta Alimentar: ¿La plata no gastada se acumula para el siguiente mes o
  se pierde?'
entradilla: Durante el mes de febrero la Tarjeta Alimentar fue noticia ya que aumentó
  los montos mensuales que reciben los beneficiarios para la compra de alimentos. 

---
Durante el mes de febrero la Tarjeta Alimentar fue noticia ya que aumentó los montos mensuales que reciben los beneficiarios para la compra de alimentos. 

Al respecto, los usuarios realizaron consultas con respecto al dinero acreditado y la forma de utilización y, desde el Ministerio de Desarrollo Social, resaltaron que la Tarjeta Alimentar no es un instrumento de ahorro, por esta razón los saldos mensuales no son acumulativos. 

Es decir, que el monto de la tarjeta vuelve a $ 0 previo a recibir la siguiente carga.

De esta manera, por ejemplo, si un beneficiario de $ 9000 gastó solo $ 8000, los $ 1000 restantes se perderán cuando se acredite la próxima carga. Por ende, el saldo de la tarjeta, con la nueva carga, será de $ 9000 y no de $ 10.000.

 **OTROS DATOS DE LA TARJETA ALIMENTAR**

Los beneficiarios son personas que cobren la Asignación Universal por Hijo con hijas e hijos de hasta 6 años inclusive; embarazadas a partir de los 3 meses que cobren la Asignación por Embarazo para Protección Social y personas con discapacidad que cobren la Asignación Universal por Hijo, sin límite de edad.

El monto mensual de la tarjeta es de $ 6.000 si el beneficiario tiene un solo hijo menor de 6 años y de $ 9.000 si tiene dos o más hijos menores de 6 años.

La Tarjeta Alimentar es únicamente para la compra de alimentos que forman parte de la canasta básica y excluye a las bebidas alcohólicas.

No es necesario tramitar la tarjeta porque su implementación es automática, cruzando bases de datos de Anses y de la Asignación Universal por Hijo. 