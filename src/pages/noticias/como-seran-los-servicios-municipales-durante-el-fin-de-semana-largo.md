---
category: La Ciudad
date: 2021-10-08T06:15:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/SERVICIOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Cómo serán los servicios municipales durante el fin de semana largo
title: Cómo serán los servicios municipales durante el fin de semana largo
entradilla: La Municipalidad informa sobre los servicios de recolección de residuos,
  el cementerio y el estacionamiento medido

---
Con motivo de los feriados con fines turísticos de este viernes 8 de octubre y el del lunes 11 en conmemoración al Día del Respeto a la Diversidad Cultural, la Municipalidad de Santa Fe informa los horarios de los servicios de recolección de residuos, cementerio y estacionamiento medido para este fin de semana largo.

**RECOLECCIÓN DE RESIDUOS**

Este viernes y lunes el servicio de recolección no tiene cambios y será el habitual; y tampoco habrá modificaciones para sábado y domingo.

**CEMENTERIO**

El viernes y lunes, las puertas permanecerán abiertas de 7.30 a 12.30 horas, con motivo de los feriados, para las visitas del público.

**SEOM**

El Sistema de Estacionamiento Medido (SEOM) no estará operativo.

**MERCADO NORTE**

El Mercado Norte, ubicado en la intersección de Santiago del Estero y Urquiza, abrirá este viernes y lunes, en el horario de 9 a 13 horas. Mientras que serán los habituales para los días sábado, 9 a 13 hs. y de 17 a 21 hs.; y domingo, de 9 a 13 horas.