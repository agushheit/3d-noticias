---
category: Deportes
date: 2021-12-21T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/DOMINGUEZ.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Eduardo Domínguez renunció como DT de Colón
title: Eduardo Domínguez renunció como DT de Colón
entradilla: 'En las últimas horas, Eduardo Domínguez decidió dar un paso al costado
  como entrenador de Colón y volver a Buenos Aires

'

---
En las últimas horas, Eduardo Domínguez presentó la renuncia como DT de Colón. La noticia se instaló en Buenos Aires, donde algunos ya lo vinculan con otros equipos. Desde el club, por ahora no se informó nada, pero todo está ya resuelto.

Una información que genera un fuerte sismo en los hinchas, que lo tienen en el pedestal de los elogios y confiaban en que continuaría. Sin embargo, el Barba pensó que era el momento de cambiar el aire y analizar más a fondo otras cosas. Justo en medio de los rumores que lo vinculan con otros clubes de Buenos Aires. Por ejemplo, Independiente.

Vale recordar que cuando renovó su contrato, quedó establecida una cláusula de salida a fin de año, pero se trata directamente de una renuncia indeclinable. Un detalle importante está en que ya desalojó la casa donde vivía en un barrio privado cercano al predio de entrenamiento.

Por ahora, no se conocen los detalles de esta determinación. Había cosas que no le gustaban y que pidió mejorar. El diálogo con los dirigentes siempre fue bueno, aunque habrían existido algunos chispazos antes del Clásico. Quizás pudo ser el detonante.

Para muchos, es una decisión que la venía madurando hacía un tiempo y que no tomó a la ligera. Pero su comodidad en la ciudad, tener autoridad y sobre todo, potenciar a un equipo, eran cosas que lo ilusionaban con la continuidad. Sin embargo no era tan así.

De esta manera, cierra quizás uno de sus ciclos más exitosos como entrenador. En Colón, histórico por haberlo llevado a la gloria con el primer título en 116 años de vida. Si bien es cierto que el cierre de temporada no fue el mejor, cayendo en el Trofeo de Campeones, se mira el todo: se hizo un buen colchón de puntos en el promedio y, por si fuera poco, se dio el pase a la próxima Copa Libertadores.

Domínguez hizo historia en Colón, así se va por la puerta grande y se lleva el respeto de la gente.