---
category: Agenda Ciudadana
date: 2021-04-19T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/covid.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Advierten que, a más contagios, más posibilidades de que se genere una nueva
  variante
title: Advierten que, a más contagios, más posibilidades de que se genere una nueva
  variante
entradilla: 'Lo que ocurre con las variantes de Manaos, Reino Unido o Sudáfrica es
  una muestra de que lo pueden producir las mutaciones: mayor contagiosidad, mayor
  saturación del sistema de salud y aumento de fallecidos.'

---
Cuantas más personas contagiadas de coronavirus hay, más posibilidades de que el virus mute y se genere una nueva variante, advirtió Mariana Viegas, coordinadora del consorcio de investigadores Proyecto País, sobre otra de las consecuencias menos conocidas del aumento de casos.

"Cuantas más posibilidades de transmitirse le damos, más posibilidades de mutar tiene, y por eso hay que evitar que el virus se circule indiscriminadamente entre las personas, con medidas de contención más estrictas", dijo la investigadora en diálogo con Télam.

Lo que ocurre con las variantes de Manaos, Reino Unido o Sudáfrica es una muestra de lo que pueden producir las mutaciones: mayor contagiosidad que en el tiempo se traduce en mayor saturación del sistema de salud y aumento de fallecidos.

Por eso, Viegas pone el foco en que limitar la circulación es también una manera de frenar esas mutaciones, ya que "la posibilidad de que emerja una variante del coronavirus en Argentina es tan posible como en cualquier otro lugar del mundo, el virus muta cuando replica", resumió.

Viegas es bioquímica, investigadora del Conicet y trabaja en el Hospital de Niños Ricardo Gutiérrez. Pero, además, es coordinadora del Consorcio interinstitucional para la Secuenciación del genoma y estudios genómicos de SARS-CoV2, Proyecto PAIS, el grupo de científicos reunidos por el Ministerio de Ciencia, Tecnología e Innovación de la Nación para rastrear las mutaciones del virus.

En su último informe, del 12 de abril, el consorcio determinó "la presencia de cuatro variantes de interés epidemiológico mundial en nuestro país": la variante del Reino Unido, la de Manaos, la de Río de Janeiro y la de California. Esto quiere decir que, aunque todavía no pudo determinarse una circulación comunitaria "es esperable que aumenten su frecuencia en las regiones donde ya se observa circulación".

¿Por qué es importante evitar que se generen nuevas variantes? Viegas explicó que la respuesta está en lo que ya ocurrió con esas mutaciones: "aumenta su población respecto de los otros virus que están circulando, o hay una disminución en el reconocimiento de anticuerpos neutralizantes frente a esas variantes", esto es, las vacunas no responden cómo se espera.

La razón es que cuando los virus replican, "lo que hacen es copiar muchas veces su genoma y en ese proceso pueden cometer errores que quedan y marcan diferencias" con el virus que les dio origen.

De esa manera, a medida que el virus se replica genera mutaciones, "que pueden considerarse como más peligrosas cuando empiezan a aumentar su proporción en la circulación hasta volverse dominante, o si pueden reinfectar a pacientes recuperados, o si presentan características epidemiológicas que no se habían registrado", señaló Viegas y agregó que "en realidad no se trata de que una variante sea más peligrosa sino más contagiosa, lo que se ha visto en las llamadas 'variantes de preocupación' es que son más transmisibles, que una persona puede transmitir a más cantidad de personas a su alrededor".

"En el caso de la variante del Reino Unido se ha visto que las personas infectadas generan cargas virales mayores y eso genera mayor capacidad de transmitir el virus, que estas personas serían contagiosas por un tiempo más prolongado y que registra una mayor letalidad"

Durante la entrevista con Télam, la especialista puntualizó que ocurrió con estas variantes en los países donde fueron detectadas por primera vez: "En el caso de la variante del Reino Unido se ha visto que las personas infectadas generan cargas virales mayores y eso genera mayor capacidad de transmitir el virus, que estas personas serían contagiosas por un tiempo más prolongado y que registra una mayor letalidad".

El proceso en Brasil no fue diferente ya que desde que apareció la variante de Manaos, se produjo un "colapso sanitario" y se pudo comprobar que esta mutación "va desplazando a las otras variantes en el resto del país. Se registró que reinfectó a muchos pacientes, lo que apunta a que está evadiendo la respuesta inmune generada por otras variantes", repasó.

"Todas las vacunas que hay a disposición han demostrado ser efectivas contra todas las variantes del coronavirus para disminuir severidad y mortalidad"

La especialista aclaró que "todas las vacunas que hay a disposición han demostrado ser efectivas contra todas las variantes del coronavirus para disminuir severidad y mortalidad", sin embargo, advirtió que resulta vital restringir la circulación: "si no nos cuidamos y permitimos que el virus se siga replicando es posible que en futuro haya necesidad de reformular las vacunas para responder a una nueva variante".

Aunque estas variantes llevan el nombre de los lugares donde fueron detectadas por primera vez, Viegas precisó que esto "no quiere decir que hayan emergido en esos lugares. Allí fueron detectadas por primera vez pero no se sabe donde emergieron; no hay condiciones que digan que una variante tiene que emerger en un lugar determinado, lo que tenemos que saber es que cuando el virus se replica puede mutar y en función de esas mutaciones puede alcanzar alguna ventaja para que emerja una variante. Sí sabemos que cuanta más capacidad tiene el virus para transmitirse más posibilidades tiene de mutar".

Viegas mencionó que "dos de las 'variantes de preocupación', la del Reino Unido y la de Manaos, ya representan entre el 10 y el 14 por ciento de la circulación viral en el Área Metropolitana de Buenos Aires, hay que vigilarlas para que no aumenten su proporción y para eso hay que evitar toda la circulación de personas posible".

"Las variantes de preocupación' tienen mayores niveles de transmisibilidad y en el caso de la del Reino Unido también de severidad y mortalidad, pero no representan ninguna diferencia en la atención a los pacientes ni en las medidas de prevención, sí es importante identificarlas en estudios poblacionales porque a mayor transmisibilidad de la enfermedad hay mayor cantidad de pacientes que necesitan atención y más exigencia en el sistema sanitario; por eso es tan importante evitar la circulación, y extremar los cuidados en estos momentos de alta circulación viral", enfatizó.