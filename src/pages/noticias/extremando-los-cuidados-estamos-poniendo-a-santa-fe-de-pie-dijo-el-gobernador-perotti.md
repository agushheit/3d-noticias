---
category: Agenda Ciudadana
date: 2020-12-12T12:50:45Z
thumbnail: https://assets.3dnoticias.com.ar/perotti21.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: '"Extremando los cuidados, estamos poniendo a Santa Fe de pie", dijo el gobernador
  Perotti'
title: '"Extremando los cuidados, estamos poniendo a Santa Fe de pie", dijo el gobernador
  Perotti'
entradilla: El mandatario santafesino anunció su alta epidemiológica de Covid-19,
  e instó a empezar desde hoy a tomar todos los recaudos necesarios para que sean
  posibles las celebraciones familiares de fin de año.

---
El gobernador Omar Perotti anunció hoy que obtuvo el alta epidemiológica por Covid-19, al tiempo que instó a la ciudadanía a “empezar a construir desde hoy” el camino propicio para posibilitar las reuniones familiares en las Fiestas de Fin de Año, evitando encuentros de grupos de personas durante los días previos.

Perotti, en un mensaje grabado para la ciudadanía, planteó una pregunta repetida por los santafesinos en los últimos días: “¿Podemos reunirnos como mínimo 10 personas para Navidad y Año Nuevo? Es el deseo de todos”, dijo. Y agregó: “Pero ese deseo lo tenemos que empezar a construir desde hoy, siendo muy responsables los días previos”.

<hr style="border:2px solid red"> </hr>

“Seremos diez si nos cuidamos los días previos, si no cada uno va a llevar a esa mesa todos los contactos de los últimos días: con cuántas personas estuvo en las despedidas del trabajo o con los amigos; todas esas previas, cada uno va a llevar a todos ellos a la mesa”, insistió.

<hr style="border:2px solid red"> </hr>

El gobernador detalló que al cabo de su alta epidemiológica irá “volviendo poco a poco a la actividad oficial, con las precauciones necesarias después de haber pasado el Covid”. En ese marco agradeció “todas las muestras de apoyo y los deseos de recuperación”.

Perotti destacó la labor de todo el personal de salud y, en particular, del Hospital Cullen, que lo atendió. “No solo por cumplir una tarea conmigo, sino por cumplir y haber cumplido esa misma tarea de estar cuidando a todos y cada uno de los santafesinos y las santafesinas”, subrayó.

El mandatario provincial expresó también “el deseo de estar nuevamente en actividades, de estar nuevamente junto a cada uno de ustedes. De saber que, cuidándonos, extremando cada uno de los cuidados, estamos poniendo a Santa Fe de pie”.

“Se están recuperando los niveles de actividad, se están recuperando los niveles de empleo y ese es el camino que tenemos que seguir. Cuidándonos de la mejor manera, no vamos a parar, no vamos a detener la actividad, pero sin dejar de cuidarnos”, insistió el titular de la Casa Gris.

<br/>

# **El camino a seguir**

Al abundar sobre su experiencia de haber contraído Covid-19, Perotti afirmó: “No es una broma ni una circunstancia. A nadie le gusta tener que aislarse 14 días, pero haberlo hecho principalmente los primeros días fue fundamental para evitar la multiplicación de contagios”.

Perotti reconoció el deseo de los santafesinos y santafesinas de pasar las fiestas de fin de año en familia y, en ese punto, apeló a “la responsabilidad de cada uno”. Lo dijo al graficar lo siguiente: “Seremos diez si nos cuidamos los días previos, si no cada uno va a llevar a esa mesa todos los contactos de los últimos días: con cuántas personas estuvo en las despedidas del trabajo, con los amigos; todas esas previas, cada uno va a llevar a todos ellos a la mesa”.

“Y si cada uno estuvo con diez como mínimo, a esa mesa de diez vamos a ser cien ¿No entraríamos los cien, verdad? Sería una despedida de fin de año o una Navidad muy numerosa. Seamos diez cuidándonos, tomando estos recaudos previos. Son los mismos que nos permiten estar con quienes queremos, poder estar con ellos en esos días tan especiales”, dijo.

Por último, Perotti sostuvo: “Quiero agradecerles a todos los santafesinos y santafesinas el impulso que nos lleva al pleno convencimiento de que vamos a poner de pie a esta gran provincia. Todos somos parte de algo grande: eso grande es Santa Fe, eso grande es nuestra Argentina. Todos los días cuidándonos y sin dejar de trabajar”.

<br/>

[![Twitter](https://assets.3dnoticias.com.ar/tweet.jpg)](https://twitter.com/omarperotti/status/1337407637596549120?s=20 "Abrir hilo en Twitter")