---
category: Deportes
date: 2021-02-01T07:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/ciclismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La Selección Argentina de Ciclismo concentró en el Club Ciclista de la ciudad
  de Rafaela
title: La Selección Argentina de Ciclismo concentró en el Club Ciclista de la ciudad
  de Rafaela
entradilla: Más de 40 ciclistas de diferentes categorías entrenaron durante toda la
  semana en el velódromo de la ciudad del oeste santafesino.

---
La directora provincial de Clubes, Florencia Molinero, integrante de la Secretaría de Deportes perteneciente al ministerio de Desarrollo Social recorrió las instalaciones del club junto al secretario de Deportes del municipio rafaelino, Ignacio Podio. Fueron recibidos por dirigentes de la institución quienes los invitaron a presenciar el entrenamiento de los atletas seleccionados.

El ministro de Desarrollo Social, Danilo Capitani, destacó la importancia “de promover a todas las actividades deportivas como elementos de formación, contención y educación de los niños y niñas. Tener en Rafaela a estos profesionales que con su ejemplo alientan la práctica de deportes es una satisfacción y un gran honor” indicó.

Molinero aseguró que "para la provincia y la ciudad de Rafaela es un honor que la selección argentina de ciclismo elija para su entrenamiento un club santafesino, ya que reúne las condiciones óptimas para prepararse para competencias del mas alto nivel".

Los deportistas que realizaron este entrenamiento pertenecen a la Selección que participará de los juegos “Rosario 2022”, la Selección nacional juvenil masculina y femenina, la Selección nacional categoría elite masculino y femenino y la Selección para ciclismo de hombres y mujeres que se preparan para participar del Dakar 2022.

Los trabajos de preparación fueron supervisados por el entrenador Walter Pérez y los técnicos asistentes Martín Ferrari, Daniel Capella y Cristián Romero.