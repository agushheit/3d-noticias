---
category: Agenda Ciudadana
date: 2021-01-22T04:10:31Z
thumbnail: https://assets.3dnoticias.com.ar/despidos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Infobae
resumen: El Gobierno extenderá por 90 días la prohibición de despidos y por el resto
  del año la doble indemnización
title: El Gobierno extenderá por 90 días la prohibición de despidos y por el resto
  del año la doble indemnización
entradilla: Fuentes oficiales confirmaron a Infobae que la medida se oficializará
  el lunes próximo, una vez que se publique el decreto en el Boletín Oficial

---
A través de un Decreto que se firmará en las próximas horas, el Gobierno prorrogará la prohibición de despidos por 90 días y mantendrá la obligatoriedad de la doble indemnización por todo el 2021, según confirmaron a Infobae fuentes oficiales.

De esta manera, las empresas no podrán echar a sus empleados sin justa causa y por las causales de fuerza mayor o disminución de la demanda de trabajo. La medida aplica a los privados, no al sector público, y se toman días antes de que caduquen las prohibiciones vigentes.

Según pudo saber Infobae el nuevo decreto ya está escrito y se completaría las firmas el fin de semana.

Cabe recordar que para limitar ese impacto de la pandemia de coronavirus sobre el mercado de trabajo, el Poder Ejecutivo impuso la prohibición de los despidos y la penalidad del pago de doble indemnización a las empresas que dispusieran cesantías sin causa de su personal.

La prohibición de despidos fue decretada inicialmente a fines de marzo, luego de que el presidente Alberto Fernández cuestionara públicamente a los empresarios que decidieron echar a trabajadores en medio de la pandemia y del parate económico producto de la cuarentena obligatoria.

La decisión fue tomada luego del intento del Grupo Techint de despedir 1.450 trabajadores de la constructora por la parálisis de la actividad. Alberto Fernández cargó contra Paolo Rocca -titular del holding-: “Has ganado tanta plata en tu vida, tenés una fortuna que te pone entre los más millonarios del mundo; hermano, esta vez colaborá, y hacelo con los que hicieron grande a tu empresa, con los trabajadores”, aseguró el Presidente.

Los fundamentos del nuevo decreto serán los mismos que en los casos anteriores (el primer DNU rigió desde el 1° de abril, el segundo, desde el 1° de junio y el tercero desde el 1 de dicembre): se dicta en el marco de la “emergencia pública en materia económica, financiera, fiscal, administrativa, previsional, tarifaria, energética, sanitaria y social establecida por la cuarentena obligatoria para contrarrestar el coronavirus”.

La medida fue cumplida casi a rajatabla, según mostró este mes una Encuesta de Indicadores Laborales del Ministerio de Trabajo a las empresas del sector privado con más de 10 de trabajadores. Pasó de representar un 18% del total en el promedio de 2019 a un 2% en la segunda mitad de 2020, aunque en noviembre subió a 3,1% de las bajas.

El ministro de Trabajo, Claudio Moroni, admitió a fines de 2020 que el Gobierno mantendría la prohibición de despidos y la doble indemnización hasta que se haya normalizado la situación económica de la Argentina. “Frente a situaciones extraordinarias tuvimos que tomar medidas extraordinarias”, dijo el funcionario en un evento organizado por el Consejo Interamericano de Comercio y Producción (CICYP).

Y agregó que esas decisiones apuntaron, por un lado, a la “preservación de los contratos de trabajo” mediante la prohibición de los despidos y de las suspensiones sin pagos, y, por el otro, a la “protección de las empresas a través del Programa de Asistencia de Emergencia al Trabajo y la Producción (ATP)”.

Por estas horas se trata de un tema que preocupa al sector industrial pese a la leve recuperación de la actividad que se registró en los últimos meses.

Durante el encuentro del comité que se celebró ayer, el titular de la UIA, Miguel Acevedo, destacó precisamente la recuperación del nivel de actividad y planteó que hay que trabajar sobre los temas que complican la sostenibilidad del crecimiento, como la cuestión laboral. En palabras del vicepresidente de la entidad y titular de la Copal, Daniel Funes de Rioja, el “triple cepo: prohibición de suspensiones, despidos y la imposición de la doble indemnización”.

“Además, como ya no hay aislamiento sino distanciamiento, no se justifica que haya medida restrictivas desde el punto de vista de los licenciamientos sin prestación de tareas”, remarcó el dirigente.

En la central fabril sostienen que esas medidas desalientan la creación de empleo y que si bien se entienden como parte de una coyuntura de emergencia, como fue el inicio de la pandemia el año pasado, ya no tienen justificación alguna y complica los planes de las empresas de contratar personal.

Los últimos datos de la cartera laboral muestran que si bien en octubre cayó levemente el número de asalariados registrados del sector privado (-0,1%; -4.244 puestos), para la industria, el décimo mes del año fue el quinto período consecutivo de aumento del empleo registrado industrial (+0,3%; +3.672 puestos). Por lo tanto, remarcó un informe de la entidad, ya desde septiembre se encuentra por sobre el nivel pre pandemia y actualmente lo supera en 4,5 mil trabajadores (+0,4%).