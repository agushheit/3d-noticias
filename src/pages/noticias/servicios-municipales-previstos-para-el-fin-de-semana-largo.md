---
category: La Ciudad
date: 2020-12-05T12:02:00Z
thumbnail: https://assets.3dnoticias.com.ar/munifinde.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Servicios municipales previstos para el fin de semana largo
title: Servicios municipales previstos para el fin de semana largo
entradilla: La Municipalidad informa sobre los servicios que se garantizan a la población
  durante este fin de semana largo.

---
Con motivo del fin de semana largo por el feriado nacional puente del lunes 7 y martes 8 de diciembre, la Municipalidad informa los horarios de los servicios de recolección de basura, cementerio y garrafas.

Por otra parte, se recuerda a la población que ya quedó inaugurada la temporada estival Verano en Capital.

## **Servicios del fin de semana**

Los servicios municipales que se prestarán en todos los barrios de la capital santafesina, son los siguientes:

### **-Recolección de residuos:**

Los días sábados, domingo y lunes el servicio se prestará según el cronograma habitual. En tanto, el martes 8 de diciembre no habrá recolección.

### **-Cementerio:**

Las puertas permanecerán abiertas en sus horarios habituales: el sábado y el lunes, de 14 a 17 horas, el domingo, de 8 a 12 horas. Por única vez con motivo del feriado, el día martes tendrá un horario especial de 8 a 17.

### **-Garrafas:**

Tanto el lunes como el martes los puntos de venta no se encontrarán abiertos por motivo del feriado largo.

### **-Verano Capital:**

Se recuerda a la población que los Espigones I y II, playa Grande, Costanera Este, el Paseo Néstor Kirchner y Los Alisos, están habilitados únicamente como solariums. En la arena están demarcados círculos para contener hasta 6 personas. Además, se contará con la presencia de 90 guardavidas.

Habrá una agenda de actividades culturales y deportivas en distintos espacios públicos. En tanto, desde el sábado 5 de diciembre se habilitarán como balnearios los piletones de los parques del Sur y Garay y el polideportivo de Alto Verde.