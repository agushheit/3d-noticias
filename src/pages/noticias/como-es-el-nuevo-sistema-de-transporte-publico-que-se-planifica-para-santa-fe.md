---
category: La Ciudad
date: 2021-11-22T06:00:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Cómo es el nuevo sistema de transporte público que se planifica para Santa
  Fe
title: Cómo es el nuevo sistema de transporte público que se planifica para Santa
  Fe
entradilla: 'Especialistas de las 3 universidades recomiendan un cambio estructural,
  que incluya normativas flexibles. Más carriles exclusivos, coches más chicos y con
  energías limpias, ciclovías, estaciones multimodales.

'

---
No es novedad que el servicio de colectivos de la ciudad está en decadencia. Perdió un 55% de los pasajeros que trasladaba antes de la pandemia, recorre un 35% menos de los kilómetros que hacía en 2019, la flota es un 21% más chica, realiza 500 viajes menos por día y las empresas recaudan un 20% menos. Todos los indicadores están en baja y los usuarios lo sienten cuando pierden tiempo esperando en las paradas.

A esto se suma una tarifa retrasada respecto a la inflación, y a lo que se cobra en otras provincias. El último aumento se dio en enero de este año, y fue de un 46,5% respecto a septiembre de 2019, cuando entre esa fecha y abril pasado la inflación registrada superó el 80%. Los subsidios que otorga el Estado no alcanzan a cubrir la ecuación económica. Y el 67,1% de los usuarios viaja con algún tipo de descuento en la tarifa.

La actual gestión municipal lo sabe. Por eso analiza licitar un nuevo sistema de transporte público multimodal, con nuevas reglas de juego para un contexto de movilidad totalmente distinto. La intención del intendente Emilio Jatón es llegar a anunciar en marzo del año que viene el envío de los pliegos al Concejo, cuando realice la tradicional apertura de sesiones ordinarias. Pero es tan complejo lo que tiene por delante, que habrá que ver si dan los tiempos.

Esto no sucede desde 1994, durante la intendencia de Jorge Obeid. Luego hubo una crisis casi terminal del sistema que desencadenó en la caída de esa licitación, y la caducidad de las líneas, de a poco: "Se había hecho sobre la base de 60 millones de pasajeros transportados por año, y en el 2000 llevaban 28 millones. Terminó siendo una estructura de costos totalmente irreal", recordó un actor importante de aquellos años. Luego, vinieron años de concesiones precarias, con una nueva ordenanza en 2009 (la N° 11.580) que estableció exigencias a las empresas y una base de cálculo para el precio del boleto, entre otros puntos.

El primer paso para iniciar un nuevo proceso licitatorio fue dado. Y es el estudio que presentaron días atrás tres universidades de la ciudad -UTN, UNL y UCSF-, de donde se desprenden los datos mencionados en los primeros párrafos de esta nota. Allí se vuelcan los principales indicadores que muestran la decadencia del sistema y se hacen algunas recomendaciones.

Lo que queda claro es que la solución a los problemas de movilidad que tiene Santa Fe no pasará únicamente por barajar y dar nuevas reglas de juego para el sistema de colectivos. Será necesario contemplar todas las formas de movilidad que han incorporado los ciudadanos en los últimos años, pandemia mediante, y las que podrían incorporarse a futuro, pensando en el avance de energías limpias.

Para el boom de las bicicletas habrá que construir una red de ciclovías más integradora y extensa; los monopatines, también en auge por el bajo costo y la rapidez de traslado en distancias cortas, están regulados por ordenanza pero debe controlarse a los usuarios a que cumplan la norma; si se avanza con el proyecto de trenes de cercanías hacia Laguna Paiva y Santo Tomé, habrá que acercar a las estaciones otros medios de transporte para conectarlas con otros puntos de la ciudad; y deberá ser más ágil el expendio de licencias de conducir y habilitaciones para taxis y remises porque hoy se dificulta conseguir uno, en especial de noche. Fomentar estas alternativas puede ayudar a que el automóvil particular y la moto pierdan la centralidad que tienen hoy en el sistema de transporte santafesino.

**DATOS DEL ESTUDIO**

El estudio que, coordinado por UTN, realizaron las tres universidades reunidas en la Mesa de Movilidad puso sobre la mesa de análisis los siguientes datos:

* El actual sistema cuenta con 17 líneas, administradas por tres empresas.
* La red estática del sistema se vio reducida a raíz de la pandemia.


* El valor de la tarifa plana se incrementó 46,5% entre septiembre de 2019 y abril de 2021, valor inferior a la inflación registrada en el país en igual período. La tarifa plana de la ciudad es similar a la de las ciudades de Neuquén y Comodoro Rivadavia, ciudades con menos población; es inferior a las registradas en las ciudades de Rosario (que tiene el triple de población) y que la vecina ciudad de Paraná (que tiene una población levemente inferior).
* La flota activa representa en abril de 2021 el 79% de la flota activa en circulación en abril de 2019. Hay 39 coches menos.
* En relación a los km recorridos a abril de 2021 alcanza el 64,7% de km en comparación con abril del año 2019.
* Sobre los pasajeros transportados las transacciones de agosto de 2021 representan el 44% de las registradas en noviembre de 2019.
* En abril de 2021 el 67,1% de los usuarios viajó con algún tipo de descuento en la tarifa.
* En relación a los reclamos, frecuencia e incumplimiento de paradas lideran el ranking.

En definitiva, los indicadores evaluados permiten observar que el sistema aún no logró recuperarse del impacto de la Pandemia Covid-19, y la ciudad de Santa Fe no ha sido exenta de lo ocurrido a nivel mundial.

**RECOMENDACIONES**

A partir del diagnóstico realizado, la Mesa de Movilidad concluyó que no se podrá tener un transporte público de pasajeros por colectivo moderno, eficiente y sustentable sino se tienen en cuenta las siguientes cuestiones:

* Continuación de red de carriles exclusivos y su monitoreo de funcionamiento.
* Revisión Técnica Obligatoria en las unidades.
* Mantención de las unidades en buen estado general y limpias.
* Posibilidad de incorporación de unidades más pequeñas.
* Zonas 30 reales y controladas. (Se colocó cartelería en el microcentro, pero faltan radares para hacerlas cumplir)
* Mantenimiento constante y sistemático de la red vial.
* Continuidad de la construcción de ciclovías.
* Estaciones multimodales.
* Una nueva red de recorridos.
* Fiscalización del sistema. Falta de controles operativos.
* Prever la incorporación de unidades eléctricas y otras energías limpias.
* Contar con unidades accesibles para personas con discapacidades.
* Y con otras especiales que permitan viajar transportando bicicletas.

"En base al retraso en la actualización de la tarifa, la gran parte de pasajes que son subvencionados por el Estado, el número de flota activa y el número de empresas que son prestatarias del servicio, entre otros aspectos, se identifica la importancia de efectuar un cambio en el sistema estructural del transporte público", concluye el diagnóstico que las universidades entregaron al gobierno local.

La última norma que regula el servicio de colectivos data del 23 de abril de 2009 (Ordenanza N° 11.580): "Obviamente que una normativa que posee 12 años de antigüedad, que establecía en su momento recorridos, horarios, frecuencias, etc. debe ser actualizada a partir de las realidades actuales de crecimiento demográfico, nuevas urbanizaciones, crecimiento del parque automotor, etc.", recomiendan. Es por eso que el Ejecutivo ya piensa en convocar a un nuevo proceso licitatorio que apunte a una movilidad integrada.

"Para mejorar la calidad del transporte público, será necesario crear una red integrada que garantice calidad y cobertura, a través de la integración de los diferentes modos de transporte con infraestructura adecuada, servicios planeados, tarifa integrada y tecnología para los vehículos, el recaudo, el control, la información y servicio al usuario", finalizan los expertos de las universidades.