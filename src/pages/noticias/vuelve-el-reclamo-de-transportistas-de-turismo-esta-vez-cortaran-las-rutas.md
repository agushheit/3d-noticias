---
category: Agenda Ciudadana
date: 2021-05-11T09:05:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'Vuelve el reclamo de transportistas de turismo: esta vez, cortarán las rutas'
title: 'Vuelve el reclamo de transportistas de turismo: esta vez, cortarán las rutas'
entradilla: Tras casi un mes del bloqueo por tres días consecutivos a la Terminal
  de Ómnibus de Santa Fe, desde el sector no recibieron respuestas a sus reclamos.
  "Hace un año que no trabajamos; estamos desesperados", indicaron.

---
Luego de un mes del reclamo que se plasmó en el bloqueo a la Terminal de Ómnibus por parte del sector de transportistas de turismo autoconvocados, este martes volverán a manifestarse, pero esta vez será a la vera de la autopista Santa Fe-Rosario a la altura del intercambiador de la autovía 19.

Según manifestaron a UNO Santa Fe desde el sector empresarial, el reclamo sigue siendo el mismo, pero agravado por la espera de un mes y las "falsas promesas". "Hace un año que no trabajamos y estamos desesperados. Nos gustaría ver a los políticos sin trabajar 12 meses, sin cobrar sus sueldo; que nos expliquen como hacemos para vivir así no sentimos esta falta de respuesta como una tomada de pelo", sostuvo Leandro, transportista autoconvocado.

"Pasó un mes de nuestra protesta y nadie nos volvió a llamar, pero por aquel entonces todos nos brindaron falsas promesas y nosotros cedimos, levantamos la medida de reclamo", resaltó Leandro y continuó agregando: "Finalmente no tuvimos respuestas de ningún tipo y observamos que los tiempos de la política no son los mismos que el de los trabajadores".

Este martes, minutos después de las 8, los trabajadores del sector de transportistas de turismo comenzarán a concentrarse con los colectivos en la zona de la autopista a la altura del peaje del acceso a la ciudad de Santo Tomé, para "luego evaluar los distintos bloqueos en diversos sectores y por tiempo indeterminado". Según manifestaron a UNO Santa Fe, "no se descartan cortes en otros puntos de las ciudades de Santa Fe y Santo Tomé".

Este miércoles 12 de mayo se cumplirá un mes del comienzo de las medidas de fuerza que llevó adelante el sector de transportistas, bloqueando por aquel entonces todos los ingresos a la Terminal de Ómnibus santafesina y volcando el reclamo con cortes también sobre avenida Alem a la altura de calle Belgrano, generando congestionamientos vehiculares en toda la zona de micro y macrocentro.

Las medidas de fuerza que se realizaron a nivel nacional, fueron en repudio a las restricciones impuestas por el gobierno nacional, en el marco de la pandemia de coronavirus. En dicha protesta, que en la ciudad de Santa Fe generó cortes de tránsito de forma total en avenida Alem y en los accesos al micro y macrocentro, los dueños de micros reclamaron a Nación (la CNRT los regula) un trato igualitario con las líneas regulares de turismo, que en la actualidad siguen prestando el servicio.

Al mismo tiempo del cónclave nacional, autoridades de la provincia de Santa Fe dialogaron con los manifestantes que realizaron el bloqueo a la Terminal durante más de 50 horas. Tras el encuentro, los transportistas autoconvocados decidieron levantar el corte y esperar por respuestas, "las cuales nunca llegaron", sostuvieron desde el sector.

**Lo que piden los transportistas**

**Petitorio**

Desde el sector de Transporte de Pasajeros de Turismo Autoconvocados, nos reunimos con el objetivo de reclamar y proteger nuestro derecho de poder ejercer libremente nuestra actividad comercial con normalidad. Siempre hemos cumplido y colaborado con las medidas que impuso el gobierno nacional. En estos momentos no podemos seguir sin realizar nuestras tareas ya que no tenemos recursos económicos suficientes para poder afrontar nuevamente la situación que sufrimos en el año 2020 de tener 9 meses sin trabajar ni recibir ninguna ayuda económica ni del gobierno nacional ni provincial. Estos meses desde que autorizaron desarrollar nuestra actividad, muchas empresas volvieron a recurrir a préstamos, créditos para poder reactivar la fuente laboral.

Las empresas de turismo cumplieron en su totalidad con los protocolos establecidos como de transportar de 80% de los pasajeros asumiendo las pérdidas que esto implica.

Por lo expuesto anteriormente como reclamo, solicitamos de forma urgente.

1\. Solicitamos el libre desempeño de nuestra fuentes de trabajo, con la igualdad que poseen las empresa de líneas y transportes aéreos.

2\. Solicitamos la eximición del Decreto 235/2021,

3\. Declarar la emergencia económica para el sector.

4\. Habilitar terminales con testeos rápidos de los pasajeros como para nuestros choferes.

5\. Cancelación del pago de ingresos brutos y ganancias por el periodo que dure la pandemia.

6\. Implementar un subsidio por los meses de interrupción de nuestra actividad o lucro cesante de nuestras unidades.

7\. El pago de sueldos de nuestros empleados y cargas sociales.

8\. Ampliación del vencimiento de habilitación nacional de las unidades con antigüedad de 15 años, incluyendo vehículos modelos año 2007.

9\. Solicitamos gasoil subsidiado para unidades con habilitación nacional.

10\. Solicitamos con urgencia una reunión con autoridades provinciales o nacionales para dejar asentado en acta el libre desempeño de nuestra actividad siempre cumpliendo con los protocolos establecidos.