---
category: Estado Real
date: 2021-08-31T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/INDUSTRIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Omar Perotti: “hay una clara decisión política de un proyecto con industria
  nacional”'
title: 'Omar Perotti: “hay una clara decisión política de un proyecto con industria
  nacional”'
entradilla: El gobernador de la provincia participó este lunes en Armstrong del acto
  por el Día de la Industria.

---
El gobernador Omar Perotti participó este lunes, en la localidad de Armstrong, del encuentro organizado por la Federación Industrial de Santa Fe (Fisfe), con motivo del Día de la Industria.

"Hay una clara decisión política de un proyecto nacional con industria nacional", aseguró el mandatario, e hizo mención a los efectos de la pandemia y la importancia de la industria local a la hora de confeccionar y producir los elementos que se necesitaban para afrontar la crisis sanitaria.

Perotti dijo que en Argentina existe “el cimiento de una fuerte vocación industrial y las bases de un sistema científico-tecnológico que nos brinda, a los argentinos, uno de nuestros mayores activos”.

“La pasión y la decisión que ponen nuestros industriales defiende el lugar (donde se radican), y detrás de esa pasión se genera la vida de nuestros pueblos y el arraigo, por lo cual la necesidad de la estructuración de una industria con alto contenido federal y un gobierno acompañando, es prioritario para una Argentina que necesita equilibrarse poblacionalmente e integrarse territorialmente”, indicó el gobernador.

Y agregó: “La vacuna es una esperanza. Y generar oportunidades para todos es la otra esperanza. Todo nuestro sector agroalimentario y agroindustrial, potenciado con la ciencia y la tecnología, nos está dando alto valor y capacidad de competir de igual a igual. Esto contagia y entusiasma”, sostuvo Perotti.

En ese sentido, aseguró que “el Estado tiene que dar certezas, con su presencia en momentos duros, y acompañar al que produce, invierte y genera empleo, y allí estuvo la asistencia nacional en el inicio de la pandemia y sigue estando”.

Más adelante expresó que “tenemos que seguir alentando a que nuestros actores productivos, todos los días, generen más. Lo que Santa Fe recibe lo potencia y lo devuelve multiplicado, y a eso lo vemos en cada empresa y comercio como dinamizador de actividad”.

Además, Perotti mencionó las características del programa Billetera Santa Fe, que “dinamiza la economía y ayuda a recuperar la actividad”. También, añadió que “creo profundamente en este proceso, en el trabajo, en el que invierte para dar trabajo. Y allí es donde el Estado tiene que acompañar con infraestructura y el financiamiento necesario. Ya se están dando señales en ese sentido”, dijo.

“Vaya si tenemos historia y derechos adquiridos para que la maquinaria agrícola tenga su acompañamiento, para cuidar este mercado interno que no sólo tiene campo para trabajar, sino la inteligencia y la decisión de llevarla adelante. Demuestra que tiene todas las condiciones para pelearla de igual a igual, para ser un exportador de esta producción y uno de los mejores del mundo”, afirmó el gobernador de la provincia.

Finalmente, Perotti señaló que este sector, junto a la ciencia y la tecnología, hacen de “esta provincia el corazón productivo de la Argentina”. Y subrayó que “la conectividad es uno de los desafíos más importantes para dar un salto de competitividad en la provincia. Creo profundamente en una provincia que acepta las diferencias, pero no las desigualdades”.

**RECOMPOSICIÓN Y CRECIMIENTO**

Por su parte, el presidente de la Unión Industrial Argentina (UIA), Daniel Funes de Rioja, expresó que “nos vamos nutridos de una nueva fe, porque podemos construir un proyecto de Argentina con desarrollo sustentable”.

“La UIA quiere diálogo y concertación para un futuro argentino optimista y constructivo. Y lo podemos hacer desde la industria, porque la industria es trabajo formal. Queremos un país productivista”, propuso Funes de Rioja.

A su turno, el titular de Fisfe, Víctor Sarmiento, dijo que “todos los días” mantiene diálogo con cada una de las áreas del gobierno de Santa Fe. “Tenemos como premisa clara el valor de la articulación con el gobierno provincial”, remarcó.

“Santa Fe es la provincia que viene liderando la recomposición de crecimiento, tanto industrial como de mano de obra”, informó Sarmiento, quien pidió estar “orgullosos de que Santa Fe aporta el 22 por ciento del valor de trabajo recuperado; soñamos con una Argentina grande e inclusiva; y con una provincia de Santa Fe que cada vez incluya más”.

En tanto, el secretario de Industria, Economía del Conocimiento y Gestión Comercial Externa de la Nación, Ariel Schale, enumeró algunas de las acciones ejecutadas por la administración central para “seguir consolidando una Argentina industrial”.

“Vamos a trabajar defendiéndolos con la convicción de que la Argentina que viene es la Argentina de la industria nacional”, indicó Schale.

**RECORRIDA Y DIÁLOGO CON EMPRESARIOS**

Previamente, en la ciudad de Las Parejas, el gobernador Omar Perotti, junto a los funcionarios nacionales y las autoridades de Fisfe, recorrieron la fábrica de máquinas agrícolas y remolques Ombú SA, y en el Sportivo Atlético Club de Las Parejas participaron de una reunión de trabajo.

En ese marco, Perotti señaló que “el sector industrial fue de los primeros que se sumó a los esenciales y claramente aprovechó esa oportunidad en el sostenimiento de empleo y en el crecimiento posterior, virtuoso porque hay tecnología, nuevos productos y eso ha entusiasmado y ha generado, acompañado por una instancia de inversión del sector agrícola, a un movimiento realmente muy importante”.

“El sector metalmecánico claramente ha generado uno de los mayores movimientos de recuperación, que nos pone orgullosamente a la provincia al tope de la recuperación del nivel de actividad, de inversión y de empleo registrado”, repasó el gobernador de la provincia.

En el evento, que se desarrolló en los talleres metalúrgicos Crucianelli SA, también participaron el ministro de la Producción, Ciencia y Tecnología, Daniel Costamagna; el secretario de Trabajo de la Nación, Marcelo Bellotti; los intendentes de Armstrong, Pablo Verdecchia, y de Las Parejas, Horacio Compagnucci; y el presidente del Centro Comercial, Industrial y Rural de Armstrong (Ccira), Claudio Del Vechio, entre otros.