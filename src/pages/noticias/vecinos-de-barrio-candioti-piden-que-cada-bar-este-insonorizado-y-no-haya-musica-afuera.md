---
category: La Ciudad
date: 2021-12-02T06:00:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/bardoconbares.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Vecinos de barrio Candioti piden que cada bar esté insonorizado y no haya
  música afuera
title: Vecinos de barrio Candioti piden que cada bar esté insonorizado y no haya música
  afuera
entradilla: 'La Municipalidad evalúa cómo proceder ante el conflicto de la nocturnidad
  santafesina y los vecinos reclaman que la música se escuche solo dentro del bar.

'

---
El último fin de semana de noviembre se produjo un incidente muy grave en el bar Gente Que No, ubicado en pleno Bulevar Gálvez, entre Mitre y Lavalle. Mientras se desarrollaba un recital de rock los vecinos de los edificios linderos lanzaron botellas cargadas con agua congelada sobre el patio del local y contra los espectadores.

“Desde ningún punto podemos tolerar tremenda agresión, como así tampoco vamos a aceptar ningún discurso que la avale o la justifique”, escribieron en una publicación de Instagram los dueños del bar, refiriéndose al hecho como un “atentado”.

Sobre el ataque, una de las vecinas del barrio dijo: “Creo que si ponen música en vivo entiendo que los vecinos estén pasando un mal momento, sobre todo si son edificios donde la música sube. Desconozco cómo sucedió pero sí sé que no es la forma de resolver un conflicto”. Si bien ella no vive cerca del establecimiento donde los vecinos tiraron los proyectiles, entiende lo desesperante que es vivir cerca de un pub en Santa Fe.

“Son muchos los bares que tenemos en la ciudad, cada manzana tiene uno o dos y tienen música afuera del bar y termina dentro de nuestros hogares”, describió. “Lo que le pedimos a la Municipalidad es que haya un control y que no permita que haya música afuera de los bares, que sea adentro con la insonorización que corresponde”.

La vecina aclaró que no todos los establecimientos son iguales ni pretenden que cierren los bares, pero sí esperan que la Municipalidad “se haga cargo y haga cumplir las reglamentaciones” para lograr “una convivencia pacífica”.

“Que los bares hagan la inversión que tengan que hacer para que puedan funcionar ellos y nosotros poder vivir, porque en barrio Candioti se le ha tornado imposible vivir a aquel que tenga un bar al lado o enfrente”, expresó Miriam.

Además, puntualizó que la relación con los propietarios de los establecimientos es dispar. "No se puede decir que sean todos iguales pero los vecinos sí han tenido conflictos: situaciones muy agresivas verbales y en un caso ha terminado un vecino preso por una agresión verbal, y siempre es el vecino el que termina pagando las consecuencias".

“Yo no sé cuál es el motivo por el cual ellos son ciudadanos con un nivel diferente al nuestro, porque nosotros nunca tenemos la razón”, se quejó.

De todas formas, la ciudadana se mostró cansada por la inacción de las autoridades. “Lo que pasa es que uno golpea y golpea las puertas de la Municipalidad y no tiene respuestas. Hemos tenido charlas en una oportunidad con funcionarios municipales donde cada uno de los vecinos pudimos decirles lo que sentimos, lo que estamos pasando; ellos escucharon y supuestamente se lo iban a transmitir a Jatón”.

“O no lo entendió o no pudo resolver nada porque desde ese momento a esa parte no ha habido ningún cambio”, reclamó, refiriéndose a un encuentro que hubo en el Mercado Progreso.

Finalmente, dijo: “Ellos tienen el discurso en los medios de comunicación de que se viven reuniendo con nosotros, y la verdad que nosotros nos reunimos una sola vez, que fuimos todos los vecinos. Pero después otra reunión no hemos tenido. Las dos vecinales de Candioti norte y sur están pidiendo una reunión en la Municipalidad y hasta ahora no han tenido respuesta”.