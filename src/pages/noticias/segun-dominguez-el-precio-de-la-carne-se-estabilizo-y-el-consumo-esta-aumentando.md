---
category: Agenda Ciudadana
date: 2021-10-02T06:00:36-03:00
thumbnail: https://assets.3dnoticias.com.ar/dominguez.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Según Dominguez, el precio de la carne "se estabilizó" y "el consumo está
  aumentando"
title: Según Dominguez, el precio de la carne "se estabilizó" y "el consumo está aumentando"
entradilla: 'El ministro de Agricultura aseguró que "la Argentina en 2020 tuvo la
  mayor exportación" del producto "de su historia ganadera" y este año las ventas
  externas del sector serán "el segundo récord de la historia"

'

---
El ministro de Agricultura Julián Domínguez afirmó hoy que con las medidas adoptadas por el Gobierno nacional sobre la exportación de carne "se estabilizó el precio y el consumo está aumentando", y aseguró que "la Argentina en 2020 tuvo la mayor exportación" del producto "de su historia ganadera" y este año las ventas externas del sector serán "el segundo récord de la historia".

El año pasado fue el "de mayor exportación de carne en la historia ganadera de la Argentina, con un 29% en la relación producción-exportación, el nivel más alto de la historia de exportación de carne en el país", sostuvo Domínguez en conferencia de prensa durante una visita a La Pampa, al referirse a las medidas adoptadas en la materia para el sector.

"Este 2021 Argentina va a exportar tanta carne que va a quedar como el segundo récord de la historia, vamos a superar el nivel del saldo exportable, porque estamos ante una aspiradora sin límites, como lo es China", precisó.

"Argentina no cerró las exportaciones y lo cierto es que quienes hablan del cierre de las exportaciones de carne tienen dos propósitos. Uno de ellos es generar incertidumbre en los mercados de la Argentina. Nuestro país no perdió un solo mercado y no va a perder ninguno", aseveró.

Según el ministro, "el otro propósito es generar incertidumbre en el productor, quien en ese contexto sale a vender su producción a menor precio y sale perjudicado".

Tras destacar que al 31 de diciembre "tendrá que estar resuelto el problema de la vaca de conserva", el ministro opinó que "las medidas del Gobierno nacional permitieron estabilizar el precio de la carne", y remarcó que "el consumo aumentó".

"El nivel de consumo que se proyecta para éste año es de 50,6 kilos por habitantes, el año pasado fue de 50,2, mientras que entre 2015 y 2019 se perdieron 7 kilos de consumo por habitante.

Además "hay que tener en cuenta que el Producto Bruto Interno cayó el 10% por la pandemia, aún no hay una recuperación total de la economía y el empleo, y sabemos que los argentinos para poder adquirir la carne, necesitan dinero en sus bolsillos", comentó.

Domínguez ponderó que "los productores quieran ganar más, que quieran producir más, lograr mayor rentabilidad y más eficiencia" y calificó como "desafío" convencer de que eso no separa sino que "genera nuevos horizontes en la medida que se entienda el rol de cada uno".

"Vamos a poner todo el acento en el mercado chino, que éste no sea un problema, sino que aporte a la industrialización y al crecimiento", remarcó el ministro.

Además, planteó la necesidad de "poner el acento en lograr que la carne sea el principal estandarte de la inserción en el mundo y trabajar para ponerle valor agregado a las exportaciones".

Ayer, el presidente Alberto Fernández presentó el proyecto de ley de Fomento al Desarrollo Agroindustrial para la promoción de nuevas inversiones que permitan incrementar la productividad, el agregado de valor y la generación de empleo.

Durante su visita, el ministro también se reunió con representantes de las entidades del campo, de la industria y el comercio vinculado a la producción y comercialización de la carne.

En el Salón de Acuerdos, previo a su conferencia de prensa, Ziliotto marcó la importancia de que "el Estado se involucre en la economía y sea el motor de la inversión productiva", destacó la necesidad de encontrar las soluciones a través del diálogo y confió en que "la Argentina saldrá adelante, con el trabajo conjunto de todos los sectores productivos, para producir y exportar cada vez".