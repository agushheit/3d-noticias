---
category: Agenda Ciudadana
date: 2021-10-08T06:00:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/RAVERTA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: El gobierno anunció un "complemento mensual" para el salario familiar a partir
  de octubre
title: El gobierno anunció un "complemento mensual" para el salario familiar a partir
  de octubre
entradilla: 'La directora ejecutiva de ANSES, Fernanda Raverta, informó que se duplicará
  el complemento mensual de las asignaciones familiares

'

---
El presidente Alberto Fernández anunció que el Gobierno implementará un "complemento mensual" para el salario familiar a partir de octubre.

"A partir de octubre vamos a beneficiar a más de 2 millones de trabajadoras y trabajadores argentinos y más de 3 millones de niños, niñas y adolescentes", destacó el mandatario desde su cuenta de Twitter.

La directora ejecutiva de ANSES, Fernanda Raverta, informó que se duplicará el complemento mensual de las asignaciones familiares por hijo a partir de octubre, de modo tal que quien actualmente percibe 5.063 pesos de asignación familiar pasará a recibir a partir del próximo mes un monto de $10.126.

La medida alcanza a "más de 2 millones de trabajadores en relación de dependencia, monotributistas A, B, C y D y quien percibe una prestación de desempleo", detalló Raverta en conferencia de prensa desde Casa Rosada.