---
category: La Ciudad
date: 2021-12-08T06:15:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/SERVICIOS8DIC.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Servicios municipales previstos para este miércoles 8
title: Servicios municipales previstos para este miércoles 8
entradilla: "La Municipalidad informa los servicios que se garantizan a la población
  durante este día con motivo del feriado nacional. \n\n"

---
La Municipalidad comunica los horarios y servicios que se prestarán durante el feriado nacional de este miércoles 8 de diciembre.

En ese sentido, se detalla que la recolección de basura y barrido de calles se prestarán con normalidad por las empresas Cliba y Urbafe.

**Transporte y estacionamiento**

Por otra parte, para este miércoles, el transporte urbano de colectivos tendrá frecuencias similares a los domingos.

Con respecto al sistema de estacionamiento medido, por ser feriado nacional, no estará operativo.

**Cementerio**

Este miércoles, los horarios de visita serán de 7.30 a 12.30, con motivo del feriado. En tanto, los trámites para inhumaciones se podrán realizar desde las 7.15 hasta las 11.30.

**Mercado Norte**

El Mercado Norte, ubicado en la intersección de Santiago del Estero y Urquiza, abrirá durante este miércoles en el horario de 9 a 13 horas.