---
category: Agenda Ciudadana
date: 2021-05-08T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/paso.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno y la oposición acordaron postergar las Paso y las legislativas
  por un mes
title: El Gobierno y la oposición acordaron postergar las Paso y las legislativas
  por un mes
entradilla: El Gobierno y la oposición llegaron a un acuerdo para postergar hasta
  el 12 de septiembre las elecciones Primarias, Abiertas, Simultáneas y Obligatorias
  (PASO).

---
El acuerdo quedó sellado durante una reunión entre el ministro del Interior, Eduardo 'Wado' de Pedro; y el presidente de la Cámara de Diputados, Sergio Massa; con los jefes de distintos bloques de ese cuerpo legislativo.

Originalmente, las PASO estaban previstas para el 8 de agosto y las generales para el 24 de octubre, por lo que el retraso sería de cinco semanas en el primer caso y de tres en el segundo.

Tras la reunión, desarrollada en el Salón de los Escudos de Casa Rosada, Wado de Pedro informó que "con los jefes de los distintos bloques del Congreso pudimos terminar de articular y acordar realizar las elecciones de este año priorizando la salud y la vida de la gente, en el marco de la pandemia que estamos viviendo".

En ese marco, detalló que junto a los representantes parlamentarios "se consensuó un proyecto de Ley para modificar el cronograma electoral, por lo que de esta forma la celebración de las PASO quedará programada para el 12 de septiembre, y las generales el 14 de noviembre".

Al acuerdo se llegó luego de que el Gobierno aceptará que la postergación será "por única vez", una de las demandas vertidas por Juntos por el Cambio cuando se conoció el borrador del proyecto.

Por su parte, Massa ponderó que "las reglas del juego en materia electoral son producto del consenso y el acuerdo de la gran mayoría de las fuerzas políticas".

**La postura de la oposición**

A su turno Mario Negri, presidente de la bancada de Juntos por el Cambio, señaló: "Entendemos que la situación sanitaria amerita un acuerdo político entre todos los partidos con representación en el Congreso".

"No especulamos políticamente, por el contrario, creemos que mientras más personas estén inmunizadas mayor será la participación de la ciudadanía en las PASO; porque el éxito de las primarias es que la sociedad se involucre para definir qué modelo de país quiere", señaló.

En el mismo sentido, el titular de la cartera de Interior valoró y agradeció “la predisposición y buena voluntad de todos los bloques y gobernadores para alcanzar este objetivo”.

“Es poder ganar un mes en cada una de las instancias electorales para que más argentinos y argentinas puedan vacunarse. Cada día que ganamos para vacunar son más vidas que se salvan”. añadió.

Massa también destacó que “durante las últimas semanas el ministro articuló con cada una de las distintas fuerzas políticas de la Argentina".

"La gran mayoría de ellas coincidimos en que, en el marco de esta pandemia, lo mejor era encontrar un mecanismo de postergación para concretar el proceso político hacia momentos más protegidos desde el punto de vista de la salud”, completó.

Y adelantó, además, que De Pedro “informará a la ciudadanía en las próximas semanas los protocolos sanitarios para garantizar el proceso electoral desde el punto de vista de la distancia social, el momento de la votación, el traslado y la protección del personal en funciones”.

**Los participantes**

Junto a Massa y De Pedro participó desde Casa de Gobierno la secretaria de Asuntos Políticos del Ministerio del Interior, Patricia García Blanco, mientras que por videoconferencia lo hicieron los oficialistas Máximo Kirchner -presidente del bloque del Frente de Todos-, Cecilia Moreau y Cristina Álvarez Rodríguez.

Por los bloques opositores estuvieron Mario Negri (UCR), Cristian Ritondo (PRO) y Juan Manuel López (Coalición Cívica), todos ellos de Juntos por el Cambio.

Eduardo Bucca (Justicialista), Carlos Gutiérrez (Córdoba Federal), Alejandro Rodríguez (Consenso Federal), Andrés Zottos (Justicialista), Luis Contigiani (Progresista)y Enrique Estévez (Socialista), lo hicieron por el interbloque Federal.

Unidad Federal por el Desarrollo estuvo representada por José Luis Ramón (Unidad para el Desarrollo), Ricardo Wellbach (Frente de la Concordia misionero) y Luis Di Giácomo (Juntos Somos Rio Negro).

Completaron la lista, por el interbloque Acción Federal, Felipe Álvarez; por el Movimiento Popular Neuquino, Alma Sapag; por IIzquierda Socialista – Frente de Izquierda, Juan Carlos Giordano; por el PTS – Frente de Izquierda, Nicolás del Caño, y por el Partido por la Justicia Social tucumano: Beatriz Ávila.

Así como Juntos por el Cambio planteó la cláusula de garantía de que las elecciones PASO no se suspenderían y que la postergación del calendario sería por única vez, otras bancadas también formularon diferentes demandas.

Ramón pidió que el proyecto sea acompañado de un informe del Ministerio de Salud sobre la situación sanitaria, en tanto que Bucca anticipó su rechazo al pedido de Juntos por el Cambio para que no se puedan volver a postergar las elecciones.

El también médico argumentó que no se puede cerrar esa posibilidad teniendo en cuenta el pico de la segunda ola de la pandemia y sin saber cuál será la situación en septiembre.

Rodriguez y el socialismo insistieron con su pedido para que se utilice la Boleta Única Papel o que, ante la imposibilidad de poder aplicarla éste año, se garantice su obligatoriedad en las elecciones de 2023.

La izquierda, en tanto, traslado su histórico reclamo de que se elimine el piso de votos requeridos en las PASO para poder competir en las elecciones generales.