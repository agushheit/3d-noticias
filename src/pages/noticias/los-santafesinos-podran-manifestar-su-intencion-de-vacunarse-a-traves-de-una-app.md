---
category: Estado Real
date: 2021-01-21T08:19:35Z
thumbnail: https://assets.3dnoticias.com.ar/vacunacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: Los santafesinos podrán manifestar su intención de vacunarse a través de
  una app
title: Los santafesinos podrán manifestar su intención de vacunarse a través de una
  app
entradilla: Así lo confirmó el secretario de Salud de Santa Fe, Jorge Prieto. El objetivo
  es conocer la intención de vacunación de la "población objetivo" a inmunizar.

---
La provincia de Santa Fe continúa con el plan de vacunación contra el coronavirus y este miércoles se comenzó a colocar la segunda dosis de la vacuna Sputnik. La misma es suministrada en los trabajadores de la salud de áreas críticas que ya habían sido inoculados con la primera dosis en diferentes efectores provinciales. Detrás del personal sanitario, llegará el turno del los agentes de la fuerza de seguridad y docentes.

En tanto, crece en el resto de la población el interés general por ser inmunizados. Ayer se conoció la noticia de la apertura de un registro para que los santafesinos manifiesten su voluntad de ser inoculados.

Este jueves, el secretario de Salud Jorge Prieto dio más precisiones: “Hay mucha adhesión por parte de la población a inscribirse para solicitar ser vacunados de forma voluntaria y para ello, se está trabajando de manera coordinada en la implementación de una aplicación donde podrán inscribirse aquellos santafesinos que se encuadren en la población objetivo. Son aproximadamente 1.200.000 personas y alcanzaría a un 30 por ciento de la población".

Destacó con relación al registro que el mismo estará disponible en gobierno (página web) y servirá "para manifestar, en una primera instancia, la intención de vacunación".

“De ahí en más, una vez que lleguen las vacunas y se establezca la gradualidad del programa de vacunación, cada una de las personas a través de su DNI va a poder referenciarse al centro de salud más cercano a su domicilio donde podría recibir la vacuna”, concluyó Prieto.

Aclaró con relación al nuevo arribo de vacunas: "Hoy se comenzó a vacunar a aquellos que han cumplido el plazo mínimo (de 21 días) de la primera dosis".

Dijo que el objetivo es alcanzar a vacunar a los "50 mil trabajadores de salud que están faltando. En una segunda instancia será seguridad, se va a continuar con docentes. Luego personas entre 18 y 59 años con comorbilidades; y ni bien se habilite, por supuesto, una población muy sensible y vulnerable que son los mayores de 60 años para tratar de alcanzar el objetivo que se había planteado a inicios en la provincia".