---
category: La Ciudad
date: 2021-11-13T06:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONEO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Avanzan las obras integrales en la intersección de Europa y Boneo
title: Avanzan las obras integrales en la intersección de Europa y Boneo
entradilla: "La Municipalidad lleva adelante una serie de tareas tendientes a saldar
  una deuda histórica con los vecinos de barrio San Martín.\n\n"

---
En la intersección de las calles Europa y Boneo, la Municipalidad lleva adelante una obra integral, basada en un reclamo histórico de los vecinos de barrio San Martín. Desde hace al menos 30 años, los habitantes de la zona reportan una acumulación de agua constante que no se puede drenar ya que no encuentra salida.

El secretario de Obras y Espacio Público municipal, Matías Pons Estel, indicó que durante una visita del intendente Emilio Jatón a la zona, se definió la realización de las tareas tendientes a brindar una solución integral. Para ello, “se concretó un trabajo muy profundo que incluyó estudios de suelo en los cuales se detectó que los sistemas pluviales estaban descalzados y tenían pérdidas”.

De este modo, una vez completadas las evaluaciones, se determinó la apertura y debió rearmarse el sistema pluvial: se calzaron los caños, se hizo un saneamiento y se colocaron mallas geotextiles que protegen el sistema. Concluidas esas tareas, se procedió a la compactación y al relleno del suelo. Por estos momentos, se llevan adelante los trabajos con RCD para luego volcar el hormigón.

También se tuvieron en cuenta nuevas captaciones pluviales, porque uno de los problemas principales radicaba en que el agua se estancaba en el lateral este. Y esas captaciones se conectaron a la red pluvial principal.

Pons Estel subrayó la importancia de “esta obra 100% municipal: desde los estudios de suelo, los pliegos técnicos, y los proyectos técnicos acorde a la obra para sacar los niveles y tomar las determinaciones, hasta la maquinaria y el personal”. Además, comentó que en la planificación y la ejecución intervino también la Secretaria de Asuntos Hídricos y Gestión de Riesgos de la Municipalidad.

Por lo expuesto, el secretario se mostró “orgulloso de cumplir con la palabra y ejecutar una obra de calidad que beneficiará a toda la zona”.

Finalmente, mencionó que cuando concluyan los trabajos en la media calzada que está en ejecución actualmente, se comenzará a arrojar hormigón. Una vez que el hormigón fragüe, se habilitará la circulación para avanzar en la media calzada restante.

**En detalle**

El objetivo fue la captación de los excedentes pluviales que pertenecen a la zona sur de la cuenca de aporte al desagüe Espora y se concentran en la esquina de Europa y Boneo. La cuenca Espora pertenece a la cuenca Flores, ambas definidas por el Plan Director de Desagües Pluviales de la ciudad de Santa Fe. Para esto, se consideró la implementación de bocas de tormenta en calle Obispo Boneo, en la intersección con Europa, y un colector (caño de H°A° diámetro 0,60 m y una longitud aproximada de 110 m), con traza por calle Europa, entre Obispo Boneo hasta su conexión al desagüe Espora.

Cabe mencionar que estos trabajos que darán solución al problema de Boneo y Europa son posibles gracias al avance de la obra del Espora, que permaneció paralizada por largo tiempo, hasta que la gestión del intendente Emilio Jatón logró reactivar.