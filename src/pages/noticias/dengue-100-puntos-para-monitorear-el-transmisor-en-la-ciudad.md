---
layout: Noticia con imagen
author: "Fuente: MCSF"
resumen: Monitoreo del dengue
category: La Ciudad
title: "Dengue: 100 puntos para monitorear el transmisor en la ciudad"
entradilla: Se colocaron 60 ovitrampas nuevas en distintos barrios, que se suman
  a las 40 ya existentes. La idea es hacer un seguimiento del aedes aegypti y
  controlar la aparición de huevos. Los análisis son en conjunto con la UNL.
date: 2020-10-22T15:07:54.863Z
thumbnail: https://assets.3dnoticias.com.ar/dengue.jpg
---
La Municipalidad lleva adelante acciones tendientes al monitoreo entomológico del vector aedes aegypti, mosquito transmisor de la enfermedad conocida como dengue. En este sentido, se avanzó en un sistema de vigilancia que implica la colocación de ovitrampas con el fin de determinar la actividad del vector. Ya había en funcionamiento unos 40 de estos dispositivos y ahora se sumaron otros 60, llegando a un total de 100 diseminados en todos los barrios de la ciudad.

Estos recipientes están colocados en viviendas particulares y ahora también se pusieron en vecinales, clubes y otras asociaciones e instituciones barriales. La intención es analizar los resultados para planificar acciones de trabajo preventivo, cuyo objetivo es controlar la aparición de casos.

El trabajo del monitoreo por ovitrampas se realiza en conjunto con la Universidad Nacional del Litoral (UNL) y las acciones que deben realizar cada una de las partes están reguladas a través de un Convenio de Cooperación y Asistencia Técnica.

![](https://assets.3dnoticias.com.ar/dengue1.jpg)

**Cómo funcionan**

Las ovitrampas son una metodología de muestreo que consta en el preparado de dos frascos de plásticos de 375 ml cada uno, los cuales uno posee una infusión filtrada de gramilla y agua; y el otro posee la misma infusión pero diluida en 9 volúmenes de agua. Mediante un estudio exhaustivo de la ciudad se designaron en un principio 60 nuevos sitios entre domicilios particulares y distintas dependencias, más las 40 ya colocadas hace algunos años.

Estos sitios deben contar con ciertas características para ser seleccionados: ser de fácil acceso en caso de inclemencias climáticas, realizar el recambio de ovitrampa sin necesidad de ingresar al domicilio y contar con techo o galería que proteja los frascos del ingreso de agua en caso de lluvia. Los puntos activos, cada 7 días corridos, se recambian y los retirados son analizados en el laboratorio entomológico de la universidad.

En el laboratorio se observa el líquido y la banda de papel que se colocó. Para el primer caso, se determina la presencia de larvas o pupas y se analizan para su identificación. Mientras que las bandas de papel se recolectan en una bolsa de polietileno, se secan y se analizan por medio de una lupa para determinar la presencia de huevos y proceder a su recuento. Con todos los datos obtenidos y analizados se elabora semanalmente un informe cuantitativo y cualitativo, describiendo lo detectado por medio de mapas y gráficos para facilitar la lectura de toda la información relevada.

![](https://assets.3dnoticias.com.ar/dengue2.jpg)

**Descacharrado**

Vale recordar que la eliminación de cacharros o depósitos de agua que haya en las viviendas es fundamental para prevenir la enfermedad ya que permite reducir los posibles criaderos de mosquitos y terminar con los huevos. Mantener los patios limpios implica un manejo adecuado de los recipientes que van a contener agua así como eliminar aquellos en desuso.