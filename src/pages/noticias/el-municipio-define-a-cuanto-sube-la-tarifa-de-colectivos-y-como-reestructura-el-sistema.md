---
category: La Ciudad
date: 2021-01-19T09:00:47Z
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: El Municipio define a cuánto sube la tarifa de colectivos y cómo reestructura
  el sistema
title: El Municipio define a cuánto sube la tarifa de colectivos y cómo reestructura
  el sistema
entradilla: Se anunciarán varios cambios de recorridos, pero aseguran que ningún barrio
  perderá conectividad. El objetivo es equilibrar la cantidad de pasajeros transportados
  entre todas las líneas.

---
En diciembre del año pasado, los empresarios de transporte de la ciudad presentaron ante la Municipalidad un pedido de aumento del boleto de colectivo, cuyo monto no se actualiza desde octubre de 2019: de $ 29,90 estimaron que debería valer $ 50,60.

El Órgano de Control ya realizó el estudio de costos y recomendó una tarifa que, respecto al valor actual, es superior al 35%. Ahora, es el Ejecutivo, junto a la Secretaría de Transporte, quien debe definir el nuevo importe, teniendo en cuenta en la ecuación que resulte viable para el sistema y que la pueden abonar los pasajeros.

La semana que viene se anunciaría la nueva tarifa, la cual probablemente comenzará a regir en febrero con un valor cercano al que ya tienen otras ciudades del interior del país.

El sistema de transporte viene de afrontar el año más duro de este siglo, con una caída del 70% de los pasajeros que transportaba hace un año y subsidios que siguen llegando pero que ya no alcanzan para cubrir los costos reales, que han ido aumentando de la mano de la inflación.

 

**Los cambios**

Es por eso que para salir de esta crisis, no bastará con un aumento de la tarifa, que de todos modos llegaría muy retrasado. Muchos pasajeros ya han migrado a otros sistemas -se han comprado motos, optaron por la bicicleta, etc.- y será difícil recuperarlos.

La actual gestión municipal ha planificado una reestructuración gradual del sistema, en etapas. “Apuntamos a equilibrar algunas líneas de colectivos porque tenemos líneas que llevan muchas cantidad de pasajeros y otras muy poco, y necesitamos que todas funcionen con una cantidad similar”, explicó el subsecretario de Transporte, Lucas Crivelli.

El concepto general es mantener la red estática, pero hacer una reestructuración de los pasajeros que van por línea. Esto implica que “algunas cambien sus recorridos y otras se troncalicen, es decir, que algunas pasen más por los barrios y otras dejen de pasar, pero se sumen otras”.

_- ¿Y qué se lograría con esto?_

\-Que el sistema sea sostenible y los barrios no pierdan conectividad. Ya tenemos algunas líneas identificadas, que vamos a informar más adelante. 

_-¿Entonces ningún barrio quedaría sin colectivos, a pesar de los cambios?_

\- No, porque se mantiene casi igual la red estática, no implica una reducción del sistema ni de los kilómetros recorridos, apenas son 6 cuadras menos en total.

 

**Etapas siguientes**

 El plan tiene una segunda etapa, sujeta a que el gobierno provincial ponga en marcha su proyecto de Boleto Educativo, destinado a docentes, personal no docente y estudiantes.

 “Ese plan está muy avanzado y, de implementarse, creo que permitiría recuperar rápidamente los pasajeros perdidos. Si eso no prospera, sí tendremos que avanzar con nuestro proyecto de movilidad multimodal más amplio, incorporando otros medios de transporte, como los trenes (junto a Provincia), el transporte fluvial para la zona de Alto Verde, y potenciar las bicicletas”, aseguró el funcionario. Esto se trabajará dentro de un esquema más colaborativo con la sociedad, con aportes de instituciones afines “apuntando a la revisión del sistema de movilidad y a otros sistemas que acompañen a los colectivos”, agregó.