---
category: Agenda Ciudadana
date: 2021-06-27T06:00:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/DIPUTADOS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Frente de Todos aspira a sumar una decena de bancas en noviembre
title: El Frente de Todos aspira a sumar una decena de bancas en noviembre
entradilla: La Cámara baja elegirá en los comicios de renovación parlamentaria 127
  bancas, de las cuales el 53 por ciento pertenecen a Juntos por el Cambio, que arriesga
  60 escaños, y el 42 por ciento al Frente de Todos.

---
El oficialismo buscará en las elecciones del 14 de noviembre renovar sus 51 bancas y sumar una decena en la Cámara de Diputados para poder alcanzar el quórum propio y así poder sancionar leyes claves para el Gobierno nacional, dado que hoy se le dificulta avanzar en esas iniciativas sin acuerdos con la oposición.

La Cámara baja elegirá en los comicios de renovación parlamentaria 127 bancas, de las cuales el 53 por ciento pertenecen a Juntos por el Cambio, que arriesga 60 escaños, y el 42 por ciento al Frente de Todos, que pone en juego 51 lugares.

Fuentes parlamentarias señalaron que el objetivo en los comicios que se desarrollarán este año es no solo renovar las 51 de los 119 diputados que hoy detentan sino sumar una decena para alcanzar el piso de 129 diputados necesarios para abrir el quórum y aprobar aquellas iniciativas claves para el Gobierno nacional.

Debido que el oficialismo no tiene mayoría debe recurrir a los bloques provinciales para poder sancionar las principales iniciativas y algunas de esas bancadas no acompañan determinados proyectos, lo cual impide poder aprobar un conjunto de propuestas esenciales para la administración de Alberto Fernández.

Esto sucede con los proyectos de reforma judicial y del Ministerio Público, donde el oficialismo tiene dificultades para lograr el quórum para habilitar la sesión debido al rechazo a estas propuestas del interbloque Federal, que tiene 11 integrantes, y otras bancadas menores.

En cualquier caso, el objetivo del Frente de Todos es incrementar sus bancas para ya no depender de volátiles acuerdos políticos -sea con el Interbloque de Unidad Federal para el Desarrollo, sea con el Interbloque Federal- para alcanzar el quórum o la sanción de leyes claves.

Además, su principal aliado, el interbloque de Unidad Federal para el Desarrollo, pone en juego cuatro de las seis bancas ya que concluyen su mandato el mendocino José Luis Ramon, el sindicalista Pablo Ansaloni, y los misioneros Flavia Morales y Ricardo Wellbach.

Las principales espadas del oficialismo -el jefe del bloque Máximo Kirchner, el presidente de la Cámara Sergio Massa, la secretaria parlamentaria Cristina Álvarez Rodríguez, y la vicepresidente Cecilia Moreau- tienen mandato hasta el 2023.

La bancada del oficialismo cuenta actualmente con 68 diputados con mandato hasta el 2023, por lo que si logra sumar una decena de bancas a las 51 que tiene que renovar podrá contar con el piso de 129 diputados.

Para lograr ese objetivo debe conseguir repetir la elección del 2019, cuando obtuvo 19 bancas al obtener el 51 por ciento de los votos en el principal distrito del país.

En Santa Fe arriesga 3 de las 9 bancas que se disputan y allí espera poder sumar una banca más si obtiene un porcentaje de votos parecido al logrado hace dos años cuando consiguió un 42 por ciento de los votos.

En el distrito porteño, el bastión del macrismo, el Frente de Todos pone 3 de las 13 bancas en juego y si mantiene el 35 por ciento de los votos logrados en el 2019 podrá sumar una banca mas y conseguir 4 escaños como en la última elección.

En Entre Ríos, la división es muy pareja: el Frente de Todos y Juntos por el Cambio, se reparten las cinco bancas en juego

Otro distrito difícil para el oficialismo es Córdoba, pero allí solo pone una sola banca de las 9 que están en disputa, ya que Cambiemos pone en juego 5 escaños y 3 Córdoba, con lo cual si logra un 22 por ciento de los votos como hace dos años atrás puede renovar y sumar una banca mas.

En Mendoza, se plantea una situación similar: el Frente de Todos renueva un escaño de los cinco que se disputan y si repite la elección del 2019 cuando consiguió el 35 por ciento los votos podrían obtener dos bancas.

En Tucumán, en tanto, se ponen 4 en juego: 2 de ellos son del Frente de Todos, y el oficialismo aspira no solo a renovar ese lugar sino sumar una banca más.

En provincias como en Entre Ríos siempre la división es muy pareja entre el Frente de Todos y Juntos por el Cambio, donde se reparten las cinco bancas en juego, y en esta ocasión el FdT pone 2 en juego y también espera poder ganar una banca mas.

En Corrientes, concluyen su mandato 3 diputados, pero uno solo arriesga el Frente de Todos en esa provincia que es conducida por el radical Gustavo Valdes, quien en agosto volverá a competir por su reelección.

Una situación diferente ocurre en Misiones, donde el Frente de Todos no renueva ninguna banca, pero aspira a conseguir un escaño en el distrito administrado por el Frente Misionero de la Concordia, cuyos legisladores acompañan la mayoría de las iniciativas del oficialismo.

En Salta, otra provincia administrada por el dirigente opositor Gustavo Sáenz, el Frente de Todos pone en juego uno de los tres lugares que se elegirán en los comicios del 14 de noviembre.

Chaco, en tanto, renovará 4 bancas, de las cuáles dos pertenecen al FdT, donde el oficialismo confía en repetir los resultados del 2019 y no solo renovar los dos lugares sino sumar uno más para el oficialismo.

En Formosa, a su vez, se elegirán 2 bancas y es probable que el reparto se mantenga como sucedió en las últimas elecciones donde un escaño correspondió al FdT y otro a Juntos por el Cambio, mientras que en Santiago del Estero se renuevan 3 bancas que corresponden al Frente de Todos y el oficialismo confía en mantener esa cantidad de lugares.

En Jujuy, la provincia gobernada por el radical Gerardo Morales donde este domingo se eligen diputados provinciales, el Frente de Todos pone en juego uno de los tres escaños que se elegirán en noviembre.

Otra provincia donde no se aguardan cambios en la distribución de bancas es en La Rioja, donde se eligen 2 diputados, de los cuales uno corresponde al Frente de Todos y otro a Juntos por el Cambio.

Una situación similar se plantea en San Juan donde el Frente de Todos arriesga dos de las tres bancas, y en Catamarca también el FdT pone en juego dos de los tres lugares que se elegirán el 14 de Noviembre.

Algo parecido ocurre en Rio Negro, donde el Frente de Todos busca renovar una de las dos bancas que están en disputa, aunque en este caso también pesará que a diferencia del 2017 competirá por un lugar Juntos por Río Negro, cuya gobernación está en manos de ese partido provincial.

Lo mismo ocurre en Neuquén, donde el oficialismo local pertenece al histórico Movimiento Popular Neuquino y allí el Frente de Todos buscará renovar una de las tres bancas que están en juego, ya que las restantes se encuentran en manos del MPN y Juntos por el Cambio.

En la La Pampa, donde también concluyen su mandato 3 diputados, el Frente de Todos disputa dos escaños, y lo mismo en Chubut, donde el oficialismo pone una de los dos diputados que se renovarán.

En Santa Cruz, se disputan 3 bancas, de las cuales una está en manos del Frente de Todos, mientras que en Tierra del Fuego el oficialismo pone en juego uno de los dos lugares que estarán en disputa.