---
category: Agenda Ciudadana
date: 2021-01-09T11:43:49Z
thumbnail: https://assets.3dnoticias.com.ar/santa-fe-controles.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: UNO Santa Fe'
resumen: 'Coronavirus: las restricciones no serán uniformes en la provincia de Santa
  Fe'
title: 'Coronavirus: las restricciones no serán uniformes en la provincia de Santa
  Fe'
entradilla: 'Tras el decreto nacional que otorga facultades a las provincias para
  restringir las actividades nocturnas, el gobierno provincial trabaja, junto a los
  municipios, en la norma que entrará en vigencia a partir del lunes. '

---
No obstante, anticiparon que las restricciones «no serán de manera uniforme» y se contemplará la situación epidemiológica de cada región. La idea es **no perjudicar al sector productivo**, teniendo en cuenta el descontento manifestado por la Asociación Empresaria de Rosario y los representantes de bares y restaurantes.

«Vamos a adherir el día lunes, pero seguramente no será de manera uniforme; vamos a analizar cada caso en particular», le anticiparon a La Capital fuentes del gobierno provincial, mientras el gobernador Omar Perotti mantenía contactos con los intendentes y jefes comunales de la provincia para coordinar y establecer pautas que no perjudiquen al sector económico, como ya ocurrió durante la etapa más dura de la pandemia durante el año pasado.

De hecho, el secretario de Turismo de la provincia, Alejandro Grandinetti, confirmó este mediodía en conferencia de prensa que el objetivo del gobernador Omar Perotti, es «tratar de **mantener y preservar las fuentes de trabajo y toda la actividad económica, pero sin poner en riesgo la salud**».

En ese equilibrio que se planteó, Grandinetti adelantó que «no va a haber una medida uniforme para toda la provincia. Sabemos que tenemos ciudades en el centro-norte y otras, como Rosario, que están dentro de lo manejable».

El caso de Tostado, que volvió a Fase 1 por el abultado incremento de contagios en una ciudad de tan pocos habitantes, encendió las alertas en el Ministerio de Salud provincial, que también tuvo en cuenta el monitoreo de casos en Rosario, Santa Fe y otras localidades del sur santafesino. 

El relajamiento de las medidas de prevención, tales como el distanciamiento social, el uso de alcohol en gel y el cubreboca parecen ser un factor determinante a la hora de evaluar el elevado número de contagios.

Rosario reportó este jueves 458 contagios de los 1.432 que notificó la provincia, mientras que Santa Fe registró 199 contagios. Esas cifras se acercan al promedio que comenzó a registrarse en agosto del año pasado hasta llegar a un promedio de 1.000 casos diarios en Rosario. 

Por eso, la decisión del gobierno provincial es **tomar cartas en el asunto para no saturar al sistema sanitario de salud**, cuya ocupación de camas generales alcanzó el 77% con un 61% de camas críticas y un 18% de camas Covid.

Sin embargo, desde el sector gastronómico y comercial de Rosario ya avisaron que una medida de tal naturaleza los llevaría al cierre definitivo de locales y la consecuente pérdida de fuentes de trabajo.