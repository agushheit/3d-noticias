---
category: Estado Real
date: 2021-12-01T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTIENBAIRES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Perotti y Cabandié formaron un acta acuerdo para ampliar la superficie del
  parque nacional "Islas de Santa Fe"
title: Perotti y Cabandié formaron un acta acuerdo para ampliar la superficie del
  parque nacional "Islas de Santa Fe"
entradilla: El gobernador de la provincia y el ministro de Ambiente y Desarrollo Sostenible
  de la Nación, rubricaron el compromiso de aumentar a 10.000 las hectáreas del área
  protegida.

---
El gobernador Omar Perotti y el ministro de Ambiente y Desarrollo Sostenible de la Nación, Juan Cabandié, firmaron este martes un acta acuerdo para propiciar la ampliación del Parque Nacional Islas de Santa Fe. La iniciativa pretende aumentar el área protegida desde las 4.096 hectáreas actuales a un total de 10.000 hectáreas.

El acto se llevó a cabo en la sede de la cartera nacional en la ciudad de Buenos Aires, y contó con la participación de la Ministra de Ambiente y Cambio Climático, Érika Gonnet; la secretaria de Gestión Federal, Candelaria González del Pino; el presidente del directorio de la Administración de Parque Nacionales, Lautaro Erratchu; el secretario de Políticas Ambientales, Oreste Blangini; y la directora de Desarrollo Territorial, María Josefina Obeid.

Luego de la firma, Gonnet afirmó que “la ampliación del Parque Nacional Islas de Santa Fe no sólo permitirá avanzar en la conservación del área protegida, sino que también será la base para el impulso de actividades turísticas, científicas y educativas”.

“Junto al ministro Cabandié, a la par del proyecto de ampliación, ya venimos recorriendo un camino para fortalecer el Parque”, destacó la funcionaria y agregó que “gracias a este trabajo conjunto entre Nación y provincia, hoy contamos con guardaparques y con una base operativa en Puerto Gaboto. Además, actualmente estamos gestionando la infraestructura necesaria para habilitar, en una primera instancia, un espacio abierto al público”.

Por su parte, Cabandié celebró el acuerdo y destacó: “Desde que asumimos la gestión, nos comprometimos a proteger la zona del Delta. Es por eso que sumamos guardaparques, brigadistas e instalamos faros de conversación” y afirmó: “Trabajamos de manera articulada con las provincias para cuidar las áreas protegidas”.

Cabe señalar que el acta establece que se coordinarán las capacidades operativas, logísticas y técnicas para realizar el relevamiento necesario para la elaboración del proyecto de ampliación del Parque; determinando la zonificación del área protegida, definiendo las áreas de conservación y las destinadas al uso público con fines turísticos.

“**ISLAS DE SANTA FE”**  
El Parque Nacional fue creado en 2010, mediante la Ley Nacional Nº 26.648, y se encuentra emplazado en el sito Ramsar “Delta del Paraná”, con el objetivo de proteger el ecosistema de la zona. Actualmente cuenta con 4.096 hectáreas.

Santa Fe, a través de la Ley Provincial N° 12.175, cuenta con un Sistema Provincial de Áreas Naturales Protegidas que prevé las categorías de Paisaje Protegido, Parque Provincial, Reserva Natural Estricta, Reserva Natural Manejada, Reserva Hídrica Natural y Reserva Privada, entre otras, según los objetivos generales de conservación perseguidos.