---
category: Estado Real
date: 2021-07-15T06:15:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/TUNEL.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Túnel Subfluvial actualizará sus tarifas
title: El Túnel Subfluvial actualizará sus tarifas
entradilla: A partir de las 0 horas del 15 de julio se modificarán las tarifas.

---
El Túnel Subfluvial Raúl Uranga - Carlos Sylvestre Begnis informa que a partir de las 00:00 horas del 15 de julio, se modificará el actual cuadro tarifario, medida que resulta indispensable para garantizar el correcto y normal mantenimiento del viaducto, su seguridad y continuidad de las obras indispensables.

La actualización que comenzará a regir desde mañana va en consonancia con las dispuestas según Resoluciones 942, 943 y 944/21 de la Dirección Nacional de Vialidad en todos los Corredores Viales de Jurisdicción Nacional.

Asimismo, se recuerda que dicha entidad biprovincial no recibe subsidios ni subvenciones de ninguna clase, como sí ocurre con otros corredores viales.

Finalmente, se informa que se mantienen vigentes la Resoluciones que otorgan el 50% de descuento para el uso frecuente de automóvil y del 20% al transporte público de pasajeros, como así también de carga, de empresas con domicilio y radicadas en las provincias de Santa Fe y Entre Ríos.

Nuevo tarifario disponible en: [http://www.tunelsubfluvial.gov.ar/wp-content/uploads/2021/07/NUEVO-CUADRO-TARIFARIO-2.pdf](http://www.tunelsubfluvial.gov.ar/wp-content/uploads/2021/07/NUEVO-CUADRO-TARIFARIO-2.pdf "http://www.tunelsubfluvial.gov.ar/wp-content/uploads/2021/07/NUEVO-CUADRO-TARIFARIO-2.pdf")