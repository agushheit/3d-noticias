---
category: Agenda Ciudadana
date: 2021-03-27T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/afip.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Habrá plazo hasta el 16 de abril para la declaración jurada y el pago del
  Aporte Solidario
title: Habrá plazo hasta el 16 de abril para la declaración jurada y el pago del Aporte
  Solidario
entradilla: 'La Unión Industrial Argentina, la Cámara de la Construcción y la Cámara
  de Comercio habían solicitado una prórroga. '

---
La Administración Federal de Ingresos Públicos (AFIP) extenderá hasta el viernes 16 de abril inclusive, el vencimiento de las obligaciones de presentación de la declaración jurada y el pago del Aporte solidario y extraordinario.

El vencimiento previsto originalmente era el próximo 30 de marzo, precisaron este viernes fuentes de la dependencia oficial.

El organismo tomó la decisión para facilitar el ingreso del Aporte solidario y extraordinario, agregaron las mismas fuentes y estimaron que alrededor de 13 mil personas con patrimonios por más de 200 millones de pesos están alcanzadas.

**Solicitudes de prórroga desde distintas instituciones**

En las semanas previas, desde distintas entidades empresarias como la UIA, la Cámara de la Construcción y la Cámara de Comercio solicitaron una prórroga del plazo.

En las notas enviadas a la AFIP, las entidades empresarias hicieron referencia a la necesidad de un plazo más extenso para que sus representados alcanzados puedan producir la documentación e información de respaldo necesarias para concretar el pago del Aporte.

Más allá de la prórroga, lo que sigue en firme son las fechas correspondientes al plan de facilidades de pago de hasta cinco cuotas.

**Fiscalización sobre quienes no presentaron Bienes Personales en 2019**

En este marco, la AFIP también avanza con la fiscalización sobre aquellas personas donde la no presentación de sus declaraciones juradas de Bienes Personales en 2019 constituye un indicio indubitable de un ardid tendiente a eludir el pago del Aporte Extraordinario.

"El organismo inició las fiscalizaciones y realizará denuncias penales por evasión agravada cuando no realicen el pago del Aporte", concluyeron las fuentes.