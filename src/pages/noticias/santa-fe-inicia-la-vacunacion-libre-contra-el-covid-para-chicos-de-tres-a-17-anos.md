---
category: Agenda Ciudadana
date: 2022-01-06T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/frrecovid.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe inicia la vacunación libre contra el Covid para chicos de tres a
  17 años
title: Santa Fe inicia la vacunación libre contra el Covid para chicos de tres a 17
  años
entradilla: 'La provincia de Santa Fe comienza el próximo lunes vacunación libre contra
  el Covid a chicos de tres a 17 años y podrán concurrir sin turno a los vacunatorios

'

---
La provincia de Santa Fe iniciará el próximo lunes la vacunación libre contra el coronavirus para los chicos de tres a 17 años, que podrán concurrir sin turno previo a los vacunatorios para colocarse su primera dosis o completar su esquema, informaron hoy voceros sanitarios.

Ante la suba de contagios de coronavirus en las últimas semanas en la provincia de Santa Fe, al igual que la ocurrencia de las interminables colas para realizar un testeo gratuito, la ministra de Salud de la provincia de Santa Fe Sonia Martorano brindó una conferencia donde mostró números y habló de dos ejes principales ante esta tercera ola de la pandemia con la llegada de ómicron: los hisopados y la vacunación.

Los niños de tres a 11 años son inoculados con dosis de Sinopharm, en tanto que los adolescentes de 12 a 17 años reciben Pfizer.

Las autoridades apuntan a vacunar un 20% de los niños que solo se aplicaron la primera dosis debido a que en la provincia hay internaciones pediátricas a causa del coronavirus, como admitió ayer en Rosario la ministra de Salud del distrito, Sonia Martorano.

En la ciudad de Santa Fe, capital de la provincia serán habilitados para los menores los vacunatorios en La Esquina Encendida, en el norte de la ciudad, y La Redonda, en el macrocentro.

De acuerdo a los datos aportados por la ministra de Salud, Sonia Martorano, los adolescentes de entre 12 y 17 años que tienen el esquema completo llegan al 78%, pero en los niños de tres a 11 baja a un 57% y hay un 20% que posee una sola dosis.

"Es la primera vez durante toda la pandemia en que tenemos niños internados. Hay tres o cuatro en terapia y algunos más en sala general. No son casos graves, pero empezamos a notar esta tendencia que ya se vio a nivel mundial, en Francia y Reino Unido”, afirmó Martorano.