---
category: Agenda Ciudadana
date: 2021-03-14T06:00:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/vuelos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Nación redujo la frecuencia de los vuelos que llegan desde el exterior
title: Nación redujo la frecuencia de los vuelos que llegan desde el exterior
entradilla: 'El Gobierno nacional también recomendó a mayores de 60 años y personas
  que integran grupos de riesgo que posterguen sus viajes a otros países para evitar
  la propagación del virus. '

---
El Gobierno nacional oficializó este sábado la decisión de reducir la frecuencia de los vuelos que llegan al país desde el exterior y reiteró la recomendación de diferir los viajes al extranjero, en particular a los mayores de 60 años y a las personas que integren los grupos de riesgo, por la situación derivada de la pandemia de coronavirus.

Lo hizo mediante la Decisión Administrativa 219/2021 publicada en el Boletín Oficial, con la firma del jefe de Gabinete, Santiago Cafiero; el ministro del Interior, Eduardo Wado de Pedro; y la ministra de Salud, Carla Vizzotti.

A través de esta medida, se decidió mantener reducidas las frecuencias de vuelos provenientes de México y Europa al 30% de la capacidad y, desde ahora, ese mismo límite regirá para los aviones que lleguen desde Perú, Ecuador, Colombia, Panamá y Chile. Además, se incrementa en un 10% las restricciones que ya existían para los arribos de Estados Unidos y en un 20%, para los de Brasil.

La decisión publicada explicita que la Dirección de Migraciones, así como la Administración Nacional de Aviación Civil y el Ministerio de Salud, «coordinarán las acciones necesarias para determinar los cronogramas de vuelos y la cantidad de pasajeros y pasajeras que ingresarán en forma paulatina y diaria al país, especialmente a través de vuelos provenientes de México, Europa, Perú, Ecuador, Colombia, Panamá, Chile, Estados Unidos y Brasil , manteniendo reducidas las frecuencias de vuelos de pasajeros a México y Europa en un 30 %».

Explica que se hará «reduciendo las frecuencias de los vuelos de pasajeros a Perú, Ecuador, Colombia, Panamá y Chile en un 30%, incrementando en forma adicional a la reducción pautada por la Decisión Administrativa N° 155/21 las frecuencias de vuelos de pasajeros a Estados Unidos en un 10 % y a Brasil en un 20 por ciento».

En los considerandos de la medida, las autoridades entienden que «resulta necesario atender la situación de aquellos vuelos provenientes de centros de conexión de los que habitualmente son transportados pasajeros provenientes de las zonas informadas por la autoridad sanitaria nacional en su informe técnico relativo a la situación epidemiológica a nivel mundial». Hacen mención asimismo a los nuevos linajes de los virus detectados y también la situación de la región del continente americano, con números de contagiados en alza en Brasil, Chile, Paraguay y Uruguay. Según se indica, la autoridad sanitaria -de acuerdo a la situación epidemiológica existente en origen y/o destino- podrá «pautar obligaciones a los operadores de transporte, transportistas y tripulantes, incluyendo establecer su testeo para determinar que no sufren de COVID-19, previo al ingreso al país, u otras que estime pertinentes».

Por otra parte, se recomienda a «los nacionales o extranjeros residentes en el país y, en particular, a los mayores de 60 años de edad o a personas pertenecientes a los grupos en riesgo definidos por la autoridad sanitaria, diferir sus viajes al exterior, cuando no respondieran al desarrollo de actividades esenciales». «La salida y el reingreso desde y hacia el país implicará la aceptación de las condiciones sanitarias y migratorias del país de destino y de la Argentina al regreso, asumiendo las consecuencias sanitarias, legales y económicas derivadas de la misma», indica la norma.

Entre esas cuestiones, se encuentra «la imposibilidad de iniciar el viaje con síntomas compatibles con Covid-19, la necesidad de contar con un servicio de salud del viajero en el exterior para la cobertura médica y/o aislamiento, y de denunciar los lugares en donde estuvo en los últimos 14 días previos al reingreso al país» Además, «deberá darse cumplimiento a las condiciones impuestas por la autoridad sanitaria nacional y someterse al control de las Autoridades Provinciales, de la ciudad autónoma de Buenos Aires y Municipales, en sus respectivas jurisdicciones y ámbitos de su competencia».