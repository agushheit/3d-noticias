---
category: Deportes
date: 2021-03-10T06:32:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/patronato-union.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: El Tate ganó pero...
title: El Tate ganó pero...
entradilla: Unión le ganó 1-0 a Patronato. Sin jugar bien, el Tatengue se aprovechó
  de las debilidades del rival y se metió entre los primeros de la Zona 2. El Patrón
  perdió los cuatro partidos que jugó y sigue sin convertir.

---
Sin jugar bien, se aprovechó de Patronato, un rival que intentó, también con sus limitaciones, pero no pudo. Y demostró porque todavía no metió gol alguno en lo que va del torneo, carencia que, claro, decanta en que aún no cosechó puntos en el certamen.

El Patrón tuvo un par de chances claras en el primer tiempo. Una se la tapó Moyano (mano a mano a Bandiera) y la otra fue un tiro libre al travesaño de Canteros. Le cuesta muchísimo progresar en el campo y acercarse al área rival. Encima, cuando se aproxima no sabe cómo continuar y le niega el grito. Había arrancado un poco más incisivo el local, a partir de algunas facilidades que Bandiera hallaba por la izquierda de su ataque.

El juego se fue equilibrando hacia abajo y los arcos a los dos les quedaban muy lejos