---
category: Agenda Ciudadana
date: 2021-08-24T06:00:28-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROTTICONECTADO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Ley de Conectividad: Perotti calificó de "inentendible" la falta de aprobación
  en Diputados'
title: 'Ley de Conectividad: Perotti calificó de "inentendible" la falta de aprobación
  en Diputados'
entradilla: El gobernador cruzó fuerte a los legisladores y dijo que es inexplicable
  "tanta desidia". Reclamó la aprobación definitiva al endeudamiento por 100 millones
  de dólares para llevar Internet a toda la provincia.

---
El gobernador Omar Perotti destacó este lunes como uno de los objetivos principales "integrar territorialmente a esta provincia". En el marco de una recorrida por obras en el norte provincial, el mandatario puso énfasis en equilibrar Santa Fe "en conectividad".

En ese sentido, hizo referencia al permiso que todavía no aprobaron los diputados provinciales vinculado al endeudamiento por 100 millones de dólares para llevar internet a los 365 municipios y comunas de la provincia, proyecto del gobierno más conocido como ley de conectividad.

"Es una gran deuda que está teniendo la cámara de diputados, no con el gobernador Perotti, está teniendo una enorme deuda con toda la población santafesina", apuntó.

Y siguió: "En esos 50 diputados y diputadas está la demora; desde octubre tenemos media sanción del Senado y no están aprobando la posibilidad de tener internet de alta calidad en todos los pueblos y ciudades de la provincia".

Calificó de "inentendible" la demora en la aprobación del endeudamiento. E insistió: "No es un freno a Perotti, es un freno a la provincia de Santa Fe".

Definió "como algo inexplicable que se deje pasar tanto tiempo, tanta desidia frente a la necesidad manifiesta que hoy tenemos por el tema educativo, por la salud y telemedicina, por las posibilidades de radicación de empresas que están pidiendo luz, gas y conectividad. Las posibilidades enormes que da el teletrabajo, de poder trabajar desde cualquier lugar de la provincia".

Le solicitó "a cada uno de los diputados de las distintas zonas de las provincias que le den sanción" al permiso de endeudamiento. "Es un financiamiento de 100 millones de dólares a la tasa más baja que ha conseguido la provincia de Santa Fe en su historia. Es un tercio de lo que pagamos para el endeudamiento de los 500 millones que vamos a empezar a pagar", comentó.

El mandatario pidió que esos legisladores "le expliquen a los alumnos que quedaron desconectados, a los docentes que tienen que hacer malabares para comunicarse, a los que tienen un centro de salud con pocos profesionales y necesitan consulta de distintas profesionales y que lo podrían hacer si tendrían una buena imagen mediante una fibra de Internet".

"Mejoraríamos en salud, seguridad, en la competitividad de las empresas; el que no tiene buena internet queda afuera de posibilidades de venta, de las posibilidades de recibir una comunicación adecuada", finalizó.

**Endeudamiento**

El Programa Estratégico de Conectividad de Santa Fe implica un permiso para endeudarse por 100 millones de dólares con el Banco de Desarrollo de América Latina (CAF) para financiar el programa de inclusión digital de transformación educativa “Santa Fe + Conectada”. Se trata de un ambicioso plan para construir 4 mil kilómetros de fibra óptica.

El crédito que Perotti afirma necesitar para llevar internet de buena calidad a los 365 municipios y comunas de la provincia ya fue otorgado, tiene media sanción del Senado y espera la aprobación definitiva de diputados.

El préstamo es a 15 años con cinco años de gracia, y 2 por ciento de tasa anual en dólares a partir del sexto año, que según el gobierno son “condiciones inmejorables”. La obra se realizará a través de una unidad de gestión entre los ministerios de Obras Públicas, Educación y Economía.

**Inversiones en el norte**

Perotti sostuvo que "hay que equilibrar poblacionalmente esta provincia y una fuerte inversión en el norte no es solamente una buena noticia para el norte. Es una muy buena noticia para toda la provincia".

"Si la gente empieza a quedarse estamos atacando las causas de migraciones, las causas de que la gente no encuentra expectativa, no encuentra oportunidades y se va a Rosario, a Santa Fe. Y esas dos grandes ciudades son las que se ven beneficiadas de esta inversión en el norte", enfatizó.

Comentó que "mucha de la gente que se fue detrás de la creencia de que había más oportunidades, se encuentra con que no es tal. Hay que atenderlas, pero hay que poner el foco de una vez por todas y darle un aprovechamiento importante al potencial productivo que tiene toda nuestra zona".

Dijo estar "convencido que gran parte de la inversión que estamos haciendo se tiene que hacer en el norte. Porque tenemos mucho potencial desaprovechado; de San Justo hacia abajo vive el 80 por ciento de la población y nos va quedando un norte extenso y vacío; no porque su gente no tenga capacidad, voluntad de trabajo o talento. Vacío porque las distancias son enormes, la necesidad de infraestructura amerita una decisión política que vaya más allá de que como «como aquí hay poca gente o pocos votos» las obras se tienen que hacer en otros lugares porque sino cuesta pagarlas o cuesta recuperarlas.