---
category: La Ciudad
date: 2021-01-26T09:51:39Z
thumbnail: https://assets.3dnoticias.com.ar/mitre1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad Santa Fe
resumen: La Municipalidad recuperará la Estación Mitre
title: La Municipalidad recuperará la Estación Mitre
entradilla: El intendente Emilio Jatón anunció que en 60 días empiezan las obras de
  reparación del edificio histórico. Con una inversión de $ 36.000.000 será la primera
  etapa de una intervención integral en todo el predio.

---
La Estación Mitre es uno de los edificios patrimoniales más importantes de la ciudad. Durante muchos años, el inmueble estuvo abandonado y a ese deterioro se suman los daños que sufrió durante un importante incendio a mediados de 2019. Ahora la Municipalidad pondrá en marcha un plan de recuperación y mejora integral de la estación para salvar la estructura histórica y sumar un espacio público a la capital provincial.

“El intendente Emilio Jatón, desde el inicio de la gestión, asumió el compromiso de recuperar ese lugar porque sabemos el valor histórico que tiene y, también, que es un espacio muy utilizado por las y los vecinos del sector suroeste de la ciudad”, explicó el secretario de Desarrollo Urbano, Javier Mendiondo, quien detalló que, a raíz del pedido del intendente, se viene trabajando de manera articulada con todas las organizaciones sociales que trabajan hace años en el lugar -como la asociación cultural El Birri y los representantes de la feria La Baulera- y con vecinos e instituciones de la zona para definir en conjunto la prioridades de intervención.

“Queremos dejar de ver al oeste como el final de la ciudad. Emilio Jatón nos pidió que sea un nuevo frente de la ciudad para que, a través de la recuperación de estos espacios y de las políticas públicas municipales que se están implementando con el Plan Integrar, podamos mejorar la calidad de vida de las y los vecinos de esos barrios”, agregó Mendiondo.

Por su parte el presidente de la Comisión de Patrimonio, Juan Ortiz, evaluó: "La Comisión de Patrimonio ve con muy buenos ojos la recuperación de los bienes patrimoniales de la ciudad de Santa Fe en la gestión del intendente Emilio Jatón y su equipo de trabajo en las áreas correspondientes".

Y siguió: "En lo particular del proyecto se destaca la voluntad de destinar fondos a la recuperación edilicia de una obra tan representativa -de finales del siglo XIX- del acervo ferroviario local, que, revisado desde condiciones y requerimientos actuales, puede aportar un sentido de identidad. Contemplando las funciones preexistentes, la del Centro Cultural y Social El Birri por ejemplo, e incorporando nuevas para toda la ciudad, pero en particular para los barrios próximos, se inicia un interesante proceso de reconversión urbana que recualificará toda el área. Desde lo patrimonial y proyectual, la Comisión destaca los criterios de recuperación del edificio principal tendientes a revalorizar una imagen que ha estado siempre presente en el imaginario colectivo, y que no hace sino fortalecer la identidad santafesina".

La obra

El proyecto de intervención integral de recuperación del Predio Mitre contempla el inmueble, el parque, la feria y todo el entorno del edificio ferroviario. Las tareas previstas para iniciarse en los próximos 60 días implicarán una inversión de más de $ 36.000.000 y se concentrarán en la Estación, ubicada en avenida General López, entre San José y Zavalla.

En un plazo de 10 meses, se prevé la reparación y restauración de la fachada principal, ejecución de aberturas y rejas nuevas y reemplazo de parte de la cubierta metálica en el hall del edificio y en el andén contra el edificio. También se realizará la adecuación de desagües pluviales y cloacales; nuevas instalaciones eléctricas; nuevos sanitarios públicos y reparación de los baños existentes; y recuperación de los andenes para uso público.

“Los trabajos deberían terminarse antes de fin de año y el objetivo, una vez que estén listos, es continuar con las actividades que allí están pero también ampliar el alcance porque esta intervención es la primera etapa de recuperación de las más de tres hectáreas que están incluidas en ese predio”, remarcó el funcionario municipal.

Con respecto al financiamiento de los trabajos, Mendiondo señaló que se realizará con el dinero que ingresó por el pago de una multa por la demolición de un edificio patrimonial. “Durante 2020 trabajamos con la Comisión de Patrimonio de la ciudad, que está integrada por representantes de las universidades, de los colegios profesionales y del Concejo Municipal, para definir que los fondos de la sanción se destinen a la recuperación de la Estación Mitre”, dijo el funcionario y aclaró que el dinero no podía destinarse a reconstruir el inmueble demolido porque implicaría “un falso histórico”, ya que no se cuentan con las técnicas ni los materiales de construcción de la época.

![](https://assets.3dnoticias.com.ar/mitre1.jpg)

![](https://assets.3dnoticias.com.ar/mitre2.jpg)

![](https://assets.3dnoticias.com.ar/mitre3.jpg)