---
category: Agenda Ciudadana
date: 2021-02-05T06:53:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/festram.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Festram
resumen: 'Municipales: si no convocan a paritaria habrá paro y movilización'
title: 'Municipales: si no convocan a paritaria habrá paro y movilización'
entradilla: El Plenario de FESTRAM resolvió esperar hasta el jueves próximo para mantener
  una reunión con los Intendentes paritarios

---
En un marco de profundo malestar, los dirigentes del sindicalismo municipal de la Provincia de Santa Fe, sesionaron en Plenario para exigir la reunión de Paritaria, donde plantearán la recomposición de los salarios y un mecanismo automático de actualización inflacionaria.

El Plenario de Secretarios Generales de FESTRAM reiteró el Estado de Alerta y Movilización a la espera de la convocatoria de los representantes paritarios del sector para iniciar la discusión de la política salarial 2021 pretendida - que reintegre la caída del poder adquisitivo de más de un 16% durante el año pasado y un aumento que supere la inflación- para el conjunto de los trabajadores y trabajadoras de las Municipalidades y Comunas. 

En la reunión se analizaron las acciones a seguir ante la falta de respuesta al pedido formal de la convocatoria a la Comisión Paritaria por parte del Gobierno Provincial, lo que genera preocupación y malestar en el sector laboral. 

Es público que ya se inician las reuniones con todos los gremios estatales y se fijaron fechas concretas para el comienzo de las negociaciones salariales, pero se dejó sin definiciones al sector municipal, pese a que FESTRAM viene reclamando la convocatoria desde el mes de Enero del corriente año. 

Otra vez aparecen impericias que no ayudan a garantizar un clima de tranquilidad para asegurar una negociación salarial acorde a la crisis que vive el País y la Provincia.

FESTRAM reitera que, tanto el Estatuto y Escalafón para el Personal Municipal Ley 9.286, la Comisión Paritaria Municipal Ley 9.996, así como las Leyes Orgánicas de Municipios y Comunas (Leyes 2.756 y 2.439) mantienen su plena vigencia, más allá de los anuncios sobre Autonomía Municipal y las “Exhortaciones” que la Corte del Lawfare le hace al Gobernador Perotti, desacreditando la Autonomía Federal de Santa Fe y su propia Corte de Justicia provincial. Se decidió rechazar en todos los planos la introducción de la discusión de este tema al momento que los municipales deben acordar salarios en el ámbito de la Paritaria.

“A la ausencia de un cronograma de vacunación a los sectores esenciales de Municipios y Comunas, la falta de respuesta de las ART ante trabajadores del sector que contraen COVID, se agrega la tensión salarial e inflacionaria, y proyectos de leyes que pretenden el retorno a las política de Menen y Reutemann”, expresó Claudio Leoni.

Por lo tanto, el Plenario de Secretarios Generales de la Federación que agrupa a los Sindicatos de Trabajadores Municipales Santafesinos resolvió mantener el estado de alerta y movilización y la próxima semana definirán las fechas de medidas del Plan de Lucha.