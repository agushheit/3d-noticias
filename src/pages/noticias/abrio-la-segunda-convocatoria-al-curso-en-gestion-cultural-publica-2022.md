---
category: Agenda Ciudadana
date: 2022-06-23T07:31:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/2022-06-22NID_275067O_1.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: ABRIÓ LA SEGUNDA CONVOCATORIA AL CURSO EN GESTIÓN CULTURAL PÚBLICA 2022
title: ABRIÓ LA SEGUNDA CONVOCATORIA AL CURSO EN GESTIÓN CULTURAL PÚBLICA 2022
entradilla: Está destinada a trabajadoras y trabajadores de los diferentes niveles
  de la administración pública nacional, provincial, municipal y universitaria.

---
El Ministerio de Cultura de la provincia de Santa Fe acompaña la convocatoria abierta para trabajadoras y trabajadores culturales públicos de todo el país que lleva adelante la cartera nacional.

Luego de cuatro cursadas en formato virtual con participantes de todo el país, comenzará en agosto un nuevo ciclo destinado a las y los trabajadores de la cultura estatal.

El programa de Formación en Gestión Cultural Pública es una iniciativa del ministerio de Cultura de la Nación, a través de la Dirección Nacional de Formación Cultural perteneciente a la Secretaría de Gestión Cultural, desde la cual se impulsa la capacitación de las y los trabajadores de los diversos niveles de la gestión cultural pública en todo el país.

El curso en Gestión Cultural Pública aborda contenidos y herramientas de gestión cultural aplicadas al ámbito público. Además busca promover la vinculación profesional entre gestores de distintas provincias y jerarquizar la mirada regional como marco de una cultura federal.

La segunda convocatoria 2022 está destinada a trabajadoras y trabajadores de los diferentes niveles de la administración pública nacional, provincial, municipal y universitaria, cuya área de trabajo esté vinculada al ámbito cultural y que no hayan participado de ediciones anteriores del programa.

La inscripción estará abierta hasta el 5 de julio en el siguiente link: https://forms.cultura.gob.ar/index.php/survey/index/sid/722899/newtest/Y/lang/es  
De los inscriptos, se seleccionarán cincuenta participantes por cada una de las seis regiones: NOA, NEA, Cuyo, Centro, Buenos Aires y Patagonia.

El curso tiene una duración de 12 semanas y otorgará certificación del INAP. Está compuesto por cuatro módulos: "Cultura, Estado, y Políticas Públicas", "Herramientas de Gestión", "Públicos y comunidades", y "Taller de Proyectos". El programa busca contextualizar y problematizar los alcances de la gestión cultural pública desde diversas perspectivas.

La cursada incluye encuentros sincrónicos semanales con especialistas, materiales asincrónicos, espacios de debate y la elaboración de un proyecto como trabajo final.  
El anuncio de personas seleccionadas se realizará luego del 18 de julio.