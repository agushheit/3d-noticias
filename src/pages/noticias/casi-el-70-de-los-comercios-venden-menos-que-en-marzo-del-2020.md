---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: "Caida en las ventas "
category: Agenda Ciudadana
title: Casi el 70% de los comercios venden menos que en marzo del 2020
entradilla: Este dato fue relevado por una encuesta de la Cámara Argentina de
  Comercio y Servicios. “Llevará tiempo recuperar los niveles pre-pandemia”,
  remarcó Natalio Grinman.
date: 2020-11-11T21:59:57.724Z
thumbnail: https://assets.3dnoticias.com.ar/cac.jpg
---
Un relevamiento de la Cámara Argentina de Comercio y Servicios detectó que el 70% de los comercios del país aseguran que están vendiendo menos que en marzo del 2020 (antes del arranque de la pandemia).

Natalio Mario Grinman, secretario de esta entidad, sostuvo por LT10 que “el consumo interno explica casi el 70% del PBI en Argentina. Este consumo está motorizado por la clase media y este fue el sector más golpeado por la pandemia porque vio la caída de sus ingresos / trabajos. La clave del consumo es la expectativa. Hoy estamos esperando vacunas y que pase la crisis económica”.

Además, agregó que “la gente guarda el dinero, porque no sabe cómo va a ser su 2021. Este combo hace que el consumo no se reactive. Esto va a llevar mucho tiempo”.

Consultado sobre si esto puede tener que ver con que muchos sectores no pudieron reabrir o tuvieron que hacerlo en un porcentaje menor, el dirigente explicó que “salvo el sector del turismo y del espectáculo, el resto está abierto. Ahora, la apertura del comercio no significa ventas. Acá no hay que buscar culpables y tirar todos juntos para adelante. La clave es generar confianza en el país”.

Finalmente, Natalio Mario Grinman remarcó que “necesitamos recuperar a un sector privado pujante y vigoroso. Para esto es clave la inversión de los argentinos. Obviamente, necesitamos reglas de juego claras y un alivio impositivo”.