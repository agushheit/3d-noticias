---
category: Agenda Ciudadana
date: 2021-01-14T10:17:59Z
thumbnail: https://assets.3dnoticias.com.ar/SAIC.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam'
resumen: Preocupa la difusión de información falsa sobre la vacunación Covid
title: Preocupa la difusión de información falsa sobre la vacunación Covid
entradilla: La entidad señaló que las vacunas constituyen «la única herramienta eficaz
  para vencer la pandemia». Y llamó a mantener los cuidados y el distanciamiento social.

---
La Sociedad Argentina de Investigación Clínica (SAIC) expresó su preocupación por la difusión de «información falsa pseudocientífica», que parece tener como objetivo «generar temores en la población y evitar que las personas se vacunen para protegerse de la infección causada por el virus SARS-CoV-2».

En un documento fechado este martes, la entidad plantea que «llama la atención que muchas personas que manifiestan este temor se han inyectado a lo largo de su vida numerosas vacunas contra todo tipo de infecciones virales o bacterianas, y reciben todos los años las conocidas vacunas antigripales».

«Debido a esta situación -añade- nos sentimos en la obligación de transmitir a la población nuestro total y absoluto convencimiento de que todas las vacunas contra la Covid-19 que la ciencia mundial ha producido en tiempo récord y han avanzado satisfactoriamente en la fase definitiva de investigación clínica (fase 3), tienen la máxima seguridad que es posible obtener y constituyen la única herramienta eficaz para vencer la pandemia, que tanto daño está causando a la humanidad en general y a nuestro país en particular».

<br/>

## **Cómo funciona**

En este punto, la sociedad científica explica que el mecanismo básico de acción de las vacunas actuales contra la Covid-19 es «esencialmente el mismo que se viene utilizando desde que Edward Jener lo aplicó en la primera vacuna contra la viruela en 1796 y que finalmente permitió erradicar esta enfermedad en 1980. El mismo consiste en inyectar el agente patógeno inactivado o alguno de los elementos que lo constituyen para estimular el sistema inmunológico a producir defensas específicas. A partir de ese momento, estas defensas quedan preservadas y listas para atacar al patógeno en el caso que este ingrese a nuestro cuerpo».

En tal sentido, la vacunación imita un proceso que ocurre naturalmente durante toda nuestra vida, «ya que cuando el cuerpo es invadido por un microorganismo patógeno se pone en marcha una respuesta similar, denominada inmunidad natural. En otras palabras, podríamos decir que permanentemente nos estamos “vacunando”, pero a riesgo de sufrir complicaciones severas al cursar la enfermedad natural».

La vacunación ha permitido erradicar o controlar fuertemente enfermedades como difteria, tétanos, tosferina, poliomielitis, sarampión, rubéola, parotiditis y gripe, además de la ya citada viruela, entre otras, apunta la entidad. «Vale remarcar que en 2018 hemos asistido en nuestro país a un importante incremento de los casos de sarampión, debido a que muchos padres han dejado de vacunar a sus hijos».

<br/>

## **Los plazos**

Uno de los reparos que repiten quienes ponen en duda la eficacia de las vacunas contra Covid-19 es el plazo (breve) en que comenzaron a estar disponibles. La SAIC lo explica: «Grupos de investigación se pusieron a disposición de este objetivo y varios estados tomaron como responsabilidad aportar a las fases clínicas, incluyendo nuestro país que ha participado en la fase 3 de varias vacunas. Por otro lado, es importante resaltar que se han cumplido todos los pasos que se requieren para comprobar la seguridad y efectividad de las vacunas, y las mismas han sido autorizadas para su aplicación en seres humanos por las más importantes y prestigiosas agencias regulatorias de medicamentos del mundo, incluyendo la ANMAT en nuestro país».

Con respecto a la primera vacuna a la cual ha accedido nuestro país -la Sputnik V- «luego de la aplicación de unas 90.000 dosis (N. de la R. a primera hora de este miércoles ya eran más de 166 mil) se reportó menos del 4% de reacciones adversas (como fiebre, dolor de cabeza, dolores musculares o molestias en el sitio de inyección), siendo en la gran mayoría de los casos leves o moderadas». Este porcentaje se encuentra «dentro del rango esperable para otras vacunas antivirales probadas incluidas en el calendario», señala la entidad.

Por último, pone a disposición la «Declaración de la comunidad universitaria y científica argentina frente a la campaña de vacunación COVID-19» (disponible en [https://www.saic.org.ar/](https://www.saic.org.ar/ "https://www.saic.org.ar/")), donde se insta a los medios de comunicación y a las distintas expresiones de la política partidaria a «brindar un tratamiento responsable de la temática relacionada con las vacunas contra Covid-19. La eficacia de las vacunas, el resultado de estudios científicos, el rol de profesionales de la salud y organismos de regulación, no pueden ser puestos en duda en la búsqueda de rédito político coyuntural».

<br/>

## **Quiénes la integran**

La Sociedad Argentina de Investigaciones Clínicas (SAIC) está presidida por Alejandro Curino; su vicepresidente es Daniel Alonso; Alejandro Urtreger es secretario; Laura Todaro, tesorera y Stella Ranuncolo es prosecretaria.

<br/>

## **Mantener los cuidados**

«Se está empezando a avanzar en pos del objetivo de vacunar a una proporción importante de la población, que a futuro permitirá controlar la pandemia. Es absurdo tirar por la borda todo el sacrificio que la sociedad ha realizado hasta ahora», advierte la SAIC.

Al igual que señalan autoridades y organismos sanitarios de todo el país, considera que «mientras se alcanza este objetivo debemos seguir sosteniendo las medidas preventivas: usar tapabocas, respetar el distanciamiento y evitar las aglomeraciones, usar alcohol en gel y lavarse las manos con frecuencia, y ventilar los espacios cerrados».

**En síntesis, «recomendamos mantener los cuidados y vacunarnos tan pronto como sea posible. Es el único camino para controlar la pandemia, y así salvar vidas y solucionar los problemas sociales y económicos que está causando».**

Para finalizar queremos hacer nuestras las palabras vertidas en la reciente «Declaración de la comunidad universitaria y científica argentina frente a la campaña de vacunación COVID-19» cuya lectura recomendamos enfáticamente (el documento puede leerse en la página de nuestra sociedad [https://www.saic.org.ar/](https://www.saic.org.ar/ "https://www.saic.org.ar/") ): «instamos a los medios de comunicación y a las distintas expresiones de la política partidaria a brindar un tratamiento responsable de la temática relacionada con las vacunas contra COVID-19. La eficacia de las vacunas, el resultado de estudios científicos, el rol de profesionales de la salud y organismos de regulación, no pueden ser puestos en duda en la búsqueda de rédito político coyuntural».