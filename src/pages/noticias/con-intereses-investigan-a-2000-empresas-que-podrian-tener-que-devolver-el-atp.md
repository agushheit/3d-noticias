---
category: Agenda Ciudadana
date: 2021-01-19T09:00:51Z
thumbnail: https://assets.3dnoticias.com.ar/afip.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: La Nación
resumen: 'Con intereses: investigan a 2000 empresas, que podrían tener que devolver
  el ATP'
title: 'Con intereses: investigan a 2000 empresas, que podrían tener que devolver
  el ATP'
entradilla: Desde fines de la semana pasada, las compañías comenzaron a recibir notificaciones
  por parte de AFIP en las que se mencionan "posibles incumplimientos".

---
Se puso en marcha la investigación de las empresas que desobedecieron los requisitos del ATP, la ayuda estatal para pagar salarios. Desde fines de la semana pasada, las compañías comenzaron a recibir notificaciones por parte de AFIP en las que se mencionan "posibles incumplimientos" que traen aparejados la obligación de "efectuar las restituciones pertinentes" al Estado, es decir, devolver el salario complementado otorgado en alguna de las rondas del programa.

Según pudo saber LA NACION, el viernes pasado se enviaron las primeras notificaciones a 2000 compañías que habrían violado alguna de las condiciones, que incluyen la distribución de utilidades, la recompra de acciones y la compra de dólar MEP o contado con liquidación (CCL). AFIP seguirá enviando notificaciones en los próximos días.

Por el momento, las compañías que recibieron la notificación deberán hacer un descargo y, posteriormente, si se confirman las inconsistencias, deberán efectuar "las restituciones pertinentes al Estado Nacional", señala la notificación que remite la Dirección General de los Recursos de Seguridad Social a contribuyentes.

Los requisitos para acceder al ATP comenzaron a regir desde abril para las compañías de más de 800 empleados y desde mayo para todas las compañías que aplicaran al programa. Los límites temporales van desde los 12 hasta los 24 meses, según el tamaño de la empresa.

Cabe aclarar que, en un primer momento, el ATP solo implicaba el desembolso de subsidios para pagar salarios, pero con el correr de las rondas se incorporaron créditos a tasa real negativa.

Por el momento, estas notificaciones no aclaran cuál fue el incumplimiento que la empresa realizó, aclara el tributarista Ezequiel Passarelli, de SCI Consultores. "Comenzaron a llegar estas notificaciones o inducciones que básicamente avisan que AFIP tiene información de que se incumplió alguna restricción; ahora las compañías tienen que ver qué restricción incumplieron", dice.

Según dispuso AFIP, explica Passarelli, el mecanismo para reintegrar el salario complementario para los contribuyentes que se dan de baja del programa consiste en reponer el monto más los intereses acumulados al momento de la devolución.

![](https://assets.3dnoticias.com.ar/afip1.jpg)

En el mismo sentido, recuerda que algunas compañías ya realizaron la devolución del salario complementario cuando se encontraron con que las restricciones eran difíciles de cumplir, sobre todo por su longitud, y especialmente si se trata de compañías de gran tamaño que necesitan hacer operaciones con socios en el exterior, por caso. En la última ronda del ATP, según datos de Jefatura de Gabinete, hubo 175 empresas que decidieron hacer una baja voluntaria.

Luciano Cativa, abogado y socio de FB Tax Legal, explica que hubo compañías que tomaron la decisión de devolver el ATP cuando se dieron cuenta de que no iban a poder cumplir los requisitos, pero al mismo tiempo, otras decidieron esperar por incertidumbre. "Todas las firmas que recibieron el ATP van a encontrarse con esta etapa de fiscalización", señala.

**Cómo funciona el control sobre las empresas**

Según el último informe del jefe de Gabinete, Santiago Cafiero, ante el Congreso, la Comisión Nacional de Valores (CNV) fue encomendada a realizar controles sobre este requisito "dada la información bursátil que dispone el organismo".

"Para realizar esa evaluación, desde la CNV se analizaron las operaciones de compra en pesos de valores negociables (VN) registradas en mercados locales, y luego se verificaron si estuvieron asociadas con venta de VN con liquidación en moneda extranjera, sea MEP o CABLE, teniendo en cuenta que ambas se hayan concertado en el mismo día, dado que la normativa hace referencia a 'inmediatez'", explicó el funcionario.

Es que el punto en particular al que se refiere indica que las empresas que formen parte del ATP "no podrán adquirir títulos valores en pesos para su posterior e inmediata venta en moneda extranjera o su transferencia en custodia al exterior".

Según los datos que presentó Cafiero, en abril hubo dos beneficiarios del ATP que se dolarizaron; en mayo, 286; en junio, 73 y en julio, 33. En todos los casos representaron una porción minoritaria del total de beneficiarios, que fueron 141; 239.671; 210.145 y 131.289 respectivamente.