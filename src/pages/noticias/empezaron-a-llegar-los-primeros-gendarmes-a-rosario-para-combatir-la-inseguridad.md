---
category: Agenda Ciudadana
date: 2021-10-13T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/GENDARMES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Empezaron a llegar los primeros gendarmes a Rosario para combatir la inseguridad
title: Empezaron a llegar los primeros gendarmes a Rosario para combatir la inseguridad
entradilla: Se los vio este martes por la tarde en la puerta de un hotel. El ministro
  Fernández junto al gobernador Perotti encabezarán este jueves la presentación de
  esa fuerza en la provincia.

---
Tras el anuncio hecho este martes 12 por la mañana por el ministro de Seguridad, Aníbal Fernández, empezaron a arribar los primeros gendarmes a la ciudad de Rosario. A estos primeros efectivos se los pudo ver en la puerta del hotel Riviera, ubicado sobre calle San Lorenzo en la ciudad del sur provincial.

El desembarco de los primeros efectivos de esta fuerza forma parte de los 575 gendarmes que arribarán a Rosario y la provincia entre este martes y el jueves próximo cuando el ministro Fernández junto al gobernador de Santa Fe, Omar Perotti, y el director nacional de Gendarmería Nacional Comandante General, Andrés Severino, encabecen el acto de presentación de esa fuerza en la provincia. El evento tendrá lugar este 14 de octubre a las 11 en el Museo del Deporte Santafesino (Avenida Ayacucho al 4800).

En rigor, de acuerdo a lo que anunció el ministro Fernández en su cuenta de Twitter, los 575 gendarmes irán llegando entre este martes y los próximos días, puesto que llegan desde distintos puntos del país. Estos uniformados desembarcan para combatir la inseguridad y el narcotráfico tanto en la ciudad como en la capital provincial.

El nuevo esquema prevé también la creación de una nueva Unidad Móvil de Gendarmería nacional ad hoc en Rosario, dedicada a la lucha contra el narcotráfico y el delito complejo, que se integrará con 1.000 gendarmes más que desembarcarán escalonadamente hasta el mes de marzo del próximo año.

Este arribo de gendarmes a Rosario y la provincia forma parte del convenio que rubricaron el pasado 28 de setiembre el ministro Fernández con el gobernador en Casa Rosada. De dicha reunión también habían participado el jefe de Gabinete, Juan Manzur, y el titular de la cartera de Interior, Eduardo “Wado” de Pedro.

Tras varias reuniones entre autoridades provinciales y nacionales -a las que se sumó incluso el pedido del propio intendente de Rosario, Pablo Javkin-, este martes el propio Aníbal Fernández mostró imágenes y videos en su cuenta de Twitter sobre el desplazamiento de las fuerzas federales.

"Esta madrugada comenzaron a salir hacia Santa Fe los destacamentos móviles de Gendarmería para desarrollar el operativo, que consistirá en Operaciones policiales de control intensivo y reforzado y de intervención, para la ciudad de #Rosario como esfuerzo principal y localidades aledañas como esfuerzo secundario", comentó en Twitter.

"Será un dispositivo de seguridad simultáneo y progresivo mediante la combinación de controles fijos y patrullaje móvil, parte del operativo de agentes federales para combatir el narcotráfico en la provincia de Santa Fe, especialmente en la ciudad de Rosario", señaló el titular de la cartera de Seguridad.

Si bien no trascendió el número de efectivos que llegarán en las próximas horas, los mismos forman parte de un contingente de 575 que fueron anunciados hace algunas semanas. Además, en esa ocasión Aníbal Fernández anunció la creación en Rosario del Destacamento Móvil 7 de Gendarmería Nacional. Días atrás, el intendente Javkin dijo que se estaban analizando los lugares donde estaría establecido esa unidad.

"Vamos a desplegar 575 efectivos de las fuerzas federales nacionales y luego completaremos con 1.000 más con la creación del Destacamento Móvil 7 de Gendarmería que se desplegará en Rosario con el propósito de reforzar la seguridad en toda Santa Fe", anunció Fernández durante una conferencia de prensa en Casa Rosada. La incorporación de estos últimos 1.000 agentes federales se concretará desde fines de septiembre y hasta marzo de 2022.

Según indicó Fernández en sus posteos, los efectivos federales que están rumbo a Rosario pertenecen a los destacamentos móviles de Santiago del Estero (DM 5), Ezeiza (DM 6), Córdoba (DM 3) y Campo de Mayo (DM 1), además de un grupo de Fuerzas Especiales de Gendarmería.