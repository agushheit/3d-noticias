---
category: La Ciudad
date: 2021-07-22T06:00:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/DEPORTES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Municipalidad retomó las actividades deportivas y recreativas
title: La Municipalidad retomó las actividades deportivas y recreativas
entradilla: Luego de que lo autorizara la autoridad sanitaria, volvió el dictado de
  las actividades deportivas y recreativas que de manera libre y gratuita organiza
  el municipio en espacios públicos.

---
Después de la obligada pausa determinada por el Covid-19, la Municipalidad retomó las actividades deportivas y recreativas que de manera libre y gratuita se dictan en espacios públicos de la capital provincial. En ese marco, esta semana volvieron a ocuparse los espacios del Parque del Sur, la Dirección de Deportes ubicada en la Costanera Oeste, el Parque Garay, la Estación Belgrano, el Polideportivo La Tablada, el Polideportivo Municipal de Alto Verde y el Centro Gallego.

Cabe señalar que las clases se desarrollan con grupos preestablecidos, es decir, con quienes ya concurrían a cada una de ellas. Los interesados en formar parte, deben dirigirse a los espacios donde se realizan las actividades para inscribirse, de modo que cuando se verifique la existencia de vacantes en algunos de los grupos, pueda participar. Esta decisión busca garantizar el distanciamiento social en cada entrenamiento.

Además, por los protocolos establecidos, antes de cada clase se registra la temperatura de los participantes. Los lugares, días y horarios donde se realizan las actividades son los siguientes:

**En la Dirección de Deportes**

– Aerolocal: de lunes a viernes a las 8.30 horas

– Funcional: lunes, miércoles y viernes a las 9.30; martes y jueves, a las 14

– Yoga: martes y jueves a las 9.30 y las 14

– Taichi: lunes, miércoles y viernes a las 10.30

– Pilates: lunes, miércoles y viernes a las 11.30 y a las 15

– Gimnasia localizada: lunes, miércoles y viernes, de 16 a 17

– GAP: lunes, miércoles y viernes de 18 a 19

– Ritmos: lunes, miércoles y viernes a las 14 y a las 18; martes y jueves de 18 a 19

– Fitness: martes y jueves a las 16

– Ritmos infantiles: martes y jueves, de 17 a 18

– Círculo de corredores: de lunes a jueves, de 15 a 16

– Ajedrez: sábados de 10 a 13

**En la Estación Belgrano**

– Funcional: lunes a viernes a las 8; miércoles y viernes a las 9

– Pilates: lunes, miércoles y viernes a las 8; martes y jueves a las 9

– Ritmos: lunes, miércoles y viernes a las 10.30 y a las 16.30

– GAP: martes y jueves a las 10

– Taichi: lunes, miércoles y viernes a las 9

– Yoga: de lunes a viernes a las 15.30; martes y jueves a las 11.30

– Tango: martes y jueves, de 16 a 18

**En el Parque del Sur**

– Funcional: martes, miércoles y jueves de 9 a 10 y de 16 a 17

– GAP: lunes miércoles y viernes de 9 a 10

– Pilates: martes, miércoles y jueves, de 10 a 11; miércoles desde las 17

– Gimnasia para adultos: martes, jueves y sábados, de 10 a 11

– Hockey: lunes, miércoles y viernes, de 10 a 11; viernes exclusivamente a las 17

– Taekwondo: lunes y miércoles a las 9; martes y jueves a las 16

– Caminatas saludables: martes, jueves y sábados de 9 a 10

– Taichi: lunes y miércoles, de 10 a 11; martes y jueves, de 17 a 18

– Iniciación deportiva: lunes y miércoles a las 15; jueves a las 10

– Fútbol: lunes y miércoles a 15

– Newcom: lunes y miércoles a las 16

– Sóftbol: jueves de 15 a 17

– Vóley: lunes y miércoles, de 16 a 17

**En el Parque Garay**

– Aerolocal: lunes, miércoles y viernes de 9 a 10; martes y jueves de 15 a 16

– Aeromix: martes y jueves, de 9 a 10

– Ritmos: lunes, miércoles y viernes a las 16 y a las 17; martes y jueves a las 10 y a las 16

– Pilates: martes y jueves, de 11 a 12

– Circuito aeróbico: lunes, miércoles y viernes, de 10 a 11; martes y jueves, de 14 a 15

– Taekwondo: lunes y miércoles a las 14

– Newcom: lunes y miércoles, de 16 a 18

**En el Polideportivo La Tablada**

– Funcional: lunes a viernes de 8 a 9; lunes, miércoles y viernes, de 15 a 16

– Zumba: martes y jueves, de 9 a 10

– Aero box: lunes, miércoles y viernes, de 9 a 10

– Pilates: lunes, miércoles y viernes, de 10 a 11; martes y jueves a las 10:30

– Vóley: martes y jueves a las 11 y de 15 a 16

– Defensa personal: martes y jueves, de 10 a 11

– Boxeo recreativo: lunes, miércoles y viernes a las 17

– Hockey: martes a las 14

– Fútbol mixto: de lunes a viernes, de 9 a 10; martes y jueves, de 16 a 18

– Patín: martes y jueves a las 16, para niños y niñas de 6 a 13 años

– Taekwondo: de 6 a 12 años, los lunes, miércoles y viernes de 16 a 17; mientras que de 13 años en adelante, lunes, miércoles y viernes a las 17

**En el Polideportivo Municipal de Alto Verde**

– Fútbol recreativo: lunes a viernes a las 13

– Básquetbol: lunes a viernes a las 14

– Handball: lunes a viernes a las 15

– Vóley: martes y jueves, de 14 a 15

– Baile: lunes, miércoles y viernes a las 16.30

– Gimnasia localizada: martes y jueves a las 15

– Gimnasia localizada: martes y jueves a las 14

– Sipalki: lunes, miércoles y viernes a las 13

– Salsa y baile para menores: lunes, miércoles y viernes a las 17.30

**En el Centro Gallego**

– Yoga: lunes, miércoles y viernes a las 9 y a las 10; martes y jueves a las 17.30

– TaiChi: lunes y viernes, de 10.30 a 11.30

– Pilates: lunes, miércoles y viernes, de 14 a 15; martes y jueves de 9 a 10, y de 16 a 17

– Funcional: lunes, miércoles y viernes desde las 14; martes y jueves de 10.30 a 11.30; y desde las 16

– GAP: lunes a viernes de 15 a 16; martes y jueves de 9.30 a 10.30

– Newcom: lunes a jueves, de 14.30 a 15.30

– Minivoley: lunes, miércoles y viernes de 18 a 19, y de 19 a 20

– Baile recreativo para chicos y chicas: lunes y miércoles de 17 a 18 (de 4 a 8 años), y de 18 a 19 (de 8 a 12 años)