---
category: Agenda Ciudadana
date: 2022-06-21T08:54:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/colectivos.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Por DNU el gobierno recorta los subsidios al transporte de pasajeros.
title: Por DNU el gobierno recorta los subsidios al transporte de pasajeros.
entradilla: Destinará 38 mil millones, menos de los 46 mil prometidos por el Ministro
  y mucho menos que los 70 mil solicitado por diputados.

---
El Secretario de Transporte de la provincia de Santa Fe, Osvaldo Miatello en un par de twits, contó la situación actual de los subsidios destinados al transporte público del interior. 

“El presupuesto planteado por el DNU del ejecutivo nacional destina 38 mil millones de subsidios al transporte del interior, menos de los 46 mil prometidos por el Ministro y mucho menos que los 70 mil solicitado por diputados. Una catástrofe para el transporte de pasajeros del interior”.

El  Decreto de Necesidad y Urgencia que modifica el Presupuesto General de la Administración Nacional para el Ejercicio 2022, publicado en el Boletín oficial el día 16 del corrientes, en su artículo 19, expresa: “Prorrógase el Fondo de Compensación al Transporte Público de Pasajeros por Automotor Urbano y Suburbano del Interior del País, por un importe de PESOS TREINTA Y OCHO MIL MILLONES ($38.000.000.000), con el objeto de continuar brindando un marco transicional que tienda a compensar posibles desequilibrios financieros a aquellas jurisdicciones asistidas en tal sentido por parte del ESTADO NACIONAL.

El MINISTERIO DE TRANSPORTE será el encargado de establecer los criterios de asignación y distribución de dicho fondo, como asimismo toda la normativa complementaria que resulte menester. Las provincias que adhieran a dicho fondo deberán juntamente con las empresas de transporte implementar el sistema de boleto único electrónico.

El Jefe de Gabinete de Ministros realizará oportunamente las adecuaciones presupuestarias que pudiera requerir este fondo”.

Mientras tanto, en nuestra ciudad, todavía no impactó el nuevo precio del boleto, al día de hoy martes 21 de junio. Los choferes no han cobrado aun los $15.000 no remunerativo que fueron acordados en la paritaria, y se teme por un paro de 72 horas en las recargas de SUBE, porque “queremos que se distribuyan mejor lo que están ganando. De una carga, se gana 0,5%. De 100 pesos, ganamos 50 centavos sin contar los gastos", le comentó Andrea Ruiz de UKGRA, a Veronica Ensinas de LT10.

Lejos de resolverse, los problemas del transporte se incrementan. 