---
category: Agenda Ciudadana
date: 2021-05-10T06:15:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/bandera.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Duelo provincial por el fallecimiento de Miguel Lifschitz
title: Duelo provincial por el fallecimiento de Miguel Lifschitz
entradilla: La provincia dispuso mediante un decreto dos días de duelo a partir de
  la fecha.

---
El Poder Ejecutivo Provincial estima procedente disponer 48 horas de duelo en todo el territorio provincial por el término de 48 horas, como consecuencia del fallecimiento del Ing. Roberto Miguel Lifschitz.

También se dispuso que la Bandera Nacional y de la Provincia de Santa Fe, permanezcan a media asta en todos los edificios públicos provinciales durante el período de duelo.