---
category: Agenda Ciudadana
date: 2022-01-30T23:34:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/AHORA12.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Télam
resumen: Relanzarán el programa Ahora 12 con financiación en hasta 24 cuotas
title: Relanzarán el programa Ahora 12 con financiación en hasta 24 cuotas
entradilla: La Secretaría de Comercio Interior cerró las negociaciones con el sector
  bancario para establecer las condiciones que regirán a partir del martes en la extensión
  del programa, el cuál mantendrá todos los rubros actuales.

---
La Secretaría de Comercio Interior pondrá en marcha este martes una nueva etapa del programa Ahora 12, en la que habrá financiación en 3, 6, 12, 18 y hasta 24 cuotas, con modificaciones en las tasas de interés y sin cambios en los rubros incluidos.

La etapa actual del programa vence este lunes, por lo cual Comercio Interior cerró a mitad de la semana pasada las negociaciones con el sector bancario para establecer las condiciones que regirán en la extensión del programa.

Los bancos pedían un aumento de 17,5 puntos porcentuales en la tasa de interés nominal anual (TNA) del programa, hasta 42,5%, pero finalmente se acordó una tasa nominal anual de 31% para los plazos de 3, 6 y 12 cuotas, y de 36% para 18 y 24 cuotas.

En la nueva resolución, que se publicará el lunes en el Boletín Oficial, el programa mantendrá todos los rubros que tiene hasta el momento y se eliminará el financiamiento en 30 cuotas.

El programa vigente hasta el lunes se puso en marcha mediante la Resolución 753/2021 en agosto del año pasado, con la ampliación del plan de pago en hasta 24 y 30 cuotas -a los ya existentes de 3, 6, 12 y 18 cuotas- para electrodomésticos y computadoras.

De acuerdo con los datos de facturación y volumen de ventas, se detectó que la financiación en 24 cuotas tuvo un incremento sostenido desde su incorporación, mientras que la modalidad de 30 cuotas representó menos de un punto de la facturación total del programa desde su incorporación, lo que motivó su eliminación.

Por otra parte, se modificará el monto en dos rubros que tienen tope, que son anteojos y lentes de contacto que pasará de $15.000 a $20.000; y motos, cuyo precio final no podrá ser superior a $300.000 (antes $ 250.000)

En los primeros días de enero, la Cámara Argentina de Comercio (CAC) solicitó la extensión del programa Ahora 12 al señalar que valora "los planes mencionados, que desde su puesta en marcha, han resultado muy relevantes para fomentar el consumo y, por su intermedio, la producción nacional y el empleo".

En una nota enviada al ministro de Desarrollo Productivo, Matías Kulfas, la entidad pedía "reincorporar a los celulares 4G", los cuales continuarán excluidos como se dispuso en octubre de 2020.

Asimismo, la Confederación Argentina de la Mediana Empresa (CAME) elevó el pedido de prórroga en una nota en la que señaló que "como representantes del sector pyme de todo el país, consideramos que esta valiosa herramienta contribuye a alentar las ventas en medio del proceso de recuperación económica".

"En tal sentido, su continuidad resulta fundamental para fortalecer esta tendencia en la actividad a nivel federal", indicó CAME.

El programa incluye una treintena de rubros que abarcan línea blanca, televisores, monitores, pequeños electrodomésticos, computadoras, indumentaria, calzado, juguetes, marroquinería y servicios de turismo, entre otros.

Los productos y servicios adquiridos con Ahora 12 deben ser de producción nacional y se pueden comprar online y en los locales adheridos de todo el país, identificados por el cartel del programa en sus vidrieras, los siete días de la semana, con todas las tarjetas de crédito con las que el comercio se encuentre adherido.

Ahora 12 cerró 2021 con un récord histórico

De acuerdo con los últimos datos del Ministerio de Desarrollo Productivo, Ahora 12 cerró 2021 con un récord histórico: el nivel de facturación y los volúmenes de operaciones crecieron sustancialmente en términos interanuales, un 85% y un 25% respectivamente.

El nivel de facturación en todo 2021 por ventas de Ahora 12 fue de $944 millones y la cantidad de operaciones fue de 71,5 millones.

En diciembre las operaciones, potenciadas por las compras de Navidad, registraron un salto positivo del 30% respecto de noviembre, con ventas por $121 millones especialmente en los rubros indumentaria, calzado y marroquinería y turismo, que registraron niveles de facturación sumamente superiores a los prepandemia.

Del mismo modo, la festividad de la Navidad dinamizó las operaciones de los juguetes y juegos de mesa, rubro que presentó un fuerte crecimiento mensual de 149%, y se ubicó un 29% por encima de diciembre 2020.

Los rubros con mayores ventas en diciembre fueron indumentaria (29%), electrodomésticos (26%) y materiales para construcción (7%).