---
category: Agenda Ciudadana
date: 2021-06-23T09:06:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/juanchi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Juan Martín
resumen: Piden informes por fondos destinados a clubes por Nación
title: Piden informes por fondos destinados a clubes por Nación
entradilla: El diputado nacional Juan Martín presentó un pedido de informes al Poder
  Ejecutivo, para que aclare por qué miles de instituciones deportivas del país quedaron
  fuera del “Programa Clubes en Obra”.

---
El diputado nacional Juan Martín presentó un pedido de informes al Poder Ejecutivo Nacional, a través del ministro de Turismo y Deportes Matías Lammens, para que aclare por qué miles de instituciones deportivas del país quedaron relegadas del “Programa Clubes en Obra”. “La selección se basó en pilares de evaluación no considerados de antemano. Exigimos explicaciones e información detallada porque nos preocupa que se puedan destinar fondos de manera focalizada, orientados por parámetros arbitrarios, rompiendo además el debido equilibrio federal en la ejecución presupuestaria”, sostuvo el legislador. 

Mediante un pedido de informes, el diputado nacional Juan Martín pide datos pormenorizados acerca de la implementación del primer período del “Programa Clubes en Obra”, debido a que miles de instituciones de todo el país han quedado fuera de esta asistencia, pese a que cumplimentaron los requisitos solicitados. Las entidades que quedaron sin esta asistencia sólo recibieron por correo electrónico una nota de la Subsecretaría de Deportes de la Nación, a través de la cual se informaba que la solicitud presentada por su institución no había resultado adjudicataria del beneficio. Los argumentos que se exponen no habían sido fijados en el aplicativo para la asistencia.  

El proyecto cuenta con la adhesión de los diputados y diputadas del Interbloque de Juntos por el Cambio Héctor Baldassi, Niky Cantard, Claudia Najul, Gabriela Lena, Jorge Lacoste, Mario Arce, Gonzalo Del Cerro, Josefina Mendoza, Virginia Cornejo y Lidia Ascarate. 

“La selección se basó en parámetros de evaluación no considerados de antemano. Nos preocupa que se ejecuten fondos de manera focalizada. Por eso pedimos información clara, queremos conocer y tener la certeza de que no han sido orientados por parámetros arbitrarios, rompiendo además el debido equilibrio federal en la distribución de fondos”, advirtió Juan Martín. 

El legislador radical recordó el “rol fundamental que cumplen los clubes deportivos en nuestro país, principalmente en un momento de crisis económica y social como la que estamos atravesando, siendo el deporte un motor de inclusión en la comunidad, promoviendo valores como la solidaridad, el esfuerzo y el compañerismo entre niños, niñas y jóvenes, particularmente en los sectores más vulnerables”. Y por eso solicita, entre otros datos, que se informe en lo relativo a las adjudicaciones realizadas, cuáles fueron los criterios objetivos en cuanto a la localización de los clubes beneficiarios, la distribución equitativa y federal del presupuesto, las líneas de acción abarcadas, montos e impacto social de los proyectos financiados.

**Punto por punto**

El legislador santafesino le exige al ministro Matías Lammens y a los funcionarios de su área que detallen cuántas instituciones deportivas se inscribieron al programa, individualizadas por barrio, pueblo, comuna o municipios y provincias, y la cantidad de proyectos que fueron “pre - aprobados” por la unidad técnica de evaluación del programa y cuáles fueron los criterios de selección aplicados. Además se solicita conocer cuántos proyectos fueron “desaprobados”, individualizados por jurisdicción y motivos de su rechazo. 

Por otra parte, Juan Martín demanda la nómina de instituciones deportivas adjudicatarias del beneficio previsto en el Programa, indicando también nombre de la institución, línea de acción del proyecto, monto del beneficio, municipio y provincia. Asimismo se requiere conocer detalles de los apoyos económicos otorgados a autoridades provinciales, municipales o comunales para infraestructura deportiva, individualizando administración beneficiaria, línea de acción y monto.  

Asimismo, el legislador pide conocer estadísticas de los cupos de distribución territorial fijados en el Programa que aseguran el carácter federal de las políticas del Estado Nacional; el presupuesto total asignado para el Programa Clubes en Obra a instituciones deportivas, desagregado en porcentajes de participación por jurisdicciones provinciales y locales; el impacto socio-económico de la adjudicación presupuestaria, por obra y jurisdicción; y finalmente el detalle de fecha, hora y medio empleado para realizar las notificaciones “oficiales” y recepción de información a las respectivas secretarías provinciales de Deporte.