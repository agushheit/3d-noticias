---
category: La Ciudad
date: 2021-12-18T06:15:09-03:00
thumbnail: https://assets.3dnoticias.com.ar/BOLI6.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: En Navidad y Año Nuevo, boliches y fiestas habilitadas podrán funcionar hasta
  las 6
title: En Navidad y Año Nuevo, boliches y fiestas habilitadas podrán funcionar hasta
  las 6
entradilla: 'Una hora antes se interrumpirá el expendio de alcohol. En tanto, los
  bares y restaurantes tendrán autorización hasta las 5 de la madrugada.

'

---
Convocada por el municipio, esta mañana se concretó una reunión con representantes de boliches bailables, gastronómicos y el sector de eventos para definir en forma conjunta la flexibilización horaria de cara a las fiestas de fin de año. En ese sentido, se dispuso extender hasta las 6, el horario de cierre para los tres boliches habilitados, ubicados en la Ruta 168, y las fiestas autorizadas.

En tanto, el expendio de bebidas alcohólicas en esos espacios se interrumpirá a las 5. Por otra parte, los locales gastronómicos, bares y restaurantes, podrán funcionar hasta las 5 de la madrugada.

Del encuentro, que se concretó en el tercer piso de Palacio Municipal, participaron el secretario General, Mariano Granato; el secretario de Producción y Desarrollo Económico, Matías Schmüth; el secretario de Control y Convivencia Ciudadana, Fernando Peverengo; y el subsecretario de Convivencia Ciudadana, Guillermo Álvarez. También estuvieron los representantes de boliches bailables, gastronómicos y organizadores de eventos.

Luego del encuentro, Schmüth junto al presidente de la Cámara de Bares, Adrián Papaleo, brindaron detalles sobre la flexibilización horaria que adoptará la ciudad durante las fiestas. “Como es habitual durante la gestión del intendente Emilio Jatón, trabajamos en mesas sectoriales para definir las cuestiones que hacen al funcionamiento de la actividad”, indicó el funcionario municipal, y destacó que acordaron que durante Navidad y Año Nuevo “puedan trabajar hasta las 6 de la mañana y que la oferta nocturna esté bien sectorizada para lograr un ordenamiento en toda la capital”.

En la oportunidad también se acordó con los propietarios de los Paradores que dichos espacios permanezcan cerrados el 24 y el 31 para colaborar con ese ordenamiento nocturno.

Papaleo, por su parte, valoró la posibilidad de ampliar los horarios: “Todo lo que sea flexibilizar aperturas siempre es bienvenido. La idea es armar todo para que estos dos días sean realmente una fiesta. Está todo armado para que en estas dos fechas se ordene mejor la nocturnidad y de esa manera, evitar que pasen cosas indebidas”.

Por otra parte, Schmüth anticipó que la próxima semana se brindarán más detalles sobre “los operativos de tránsito, de control y los senderos seguros para mejorar la accesibilidad a la zona de boliches, que tendrán como objetivo que los festejos en la ciudad se hagan de manera responsable y segura”.