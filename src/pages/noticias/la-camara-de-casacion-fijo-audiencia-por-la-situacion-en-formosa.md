---
category: Agenda Ciudadana
date: 2021-01-27T10:26:29Z
thumbnail: https://assets.3dnoticias.com.ar/casacion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Con información de Perfil
resumen: La Cámara de Casación fijó audiencia por la situación en Formosa
title: La Cámara de Casación fijó audiencia por la situación en Formosa
entradilla: Se trata del máximo tribunal penal del país. Es la primera medida judicial
  por fuera de lo que sucede en la provincia, acusada por las denuncias entorno de
  los centros de aislamiento.

---
La Cámara de Casación Federal, con sede en Comodoro Py, fijó audiencia para mañana miércoles 27 de enero por el hábeas corpus presentado por el senador Luis Naidenoff, a raíz de las cuestionadas medidas de aislamiento dispuestas en Formosa ante la pandemia de coronavirus.

Se trata de la primera medida judicial por fuera de lo que sucede en la provincia gobernada por Gildo Insfrán, que en noviembre pasado ya había sido exhortado por la Corte Suprema de Justicia de la Nación para permitir el ingreso de varados, otro de los escándalos que sucedieron en la provincia desde el comienzo de la pandemia de Covid-19. 

La audiencia de mañana fue fijada por Mariano Borinsky, Presidente de la Sala de Feria de la Cámara Federal de Casación Penal, para las 13. En el caso está en estudio la acción de habeas corpus colectivo presentado por Naidenoff en favor de personas alojadas, entre los que hay menores de edad, en centros de aislamiento que funcionan en la provincia de Formosa. 

Se estima que en Formosa hay más de 100 de estos de centros, en los que hay alojadas más de 3000 personas en condiciones deplorables y entre las cuales no sólo hay personas con diagnóstico de COVID-19 positivo sino también contactos estrechos, casos sospechosos y otros que ni siquiera son unos u otros. 

La Sala de Feria deberá determinar si la resolución del Juzgado Federal de Primera Instancia N° 2 de Formosa que decretó la incompetencia del fuero federal, que fuera confirmada por la Cámara Federal de Apelaciones de Resistencia, provincia de Chaco, resulta ajustada a derecho y a las constancias de la causa. 

La denuncia Naidenoff detalló en su presentación que en los centros no se respetan mínimamente las condiciones de privacidad, intimidad, higiene y salubridad, ni las recomendaciones del Ministerio de Salud de la Nación. E invocó la afectación de derechos humanos tales como el derecho a la vida, a la salud y al medio ambiente. 

El senador también que sostuvo que el accionar del estado provincial infringe el art. 205 del Código Penal referido a la propagación de la pandemia y agregó que en los centros de aislamiento se hace convivir a las personas sin respetar su privacidad e intimidad compartiendo el mismo espacio quienes están diagnosticados con Covid como quienes no. 

La situación de Formosa ya ameritó la intervención de la Corte Suprema de Justicia de la Nación, que en noviembre ordenó al gobierno provincial que garantice el efectivo ingreso a ese territorio provincial, que tenían desde hacía meses vedado el ingreso. Entonces les dio un plazo de quince días para regularizar la situación, pero un mes después aún no lo estaba, lo que valió incluso un llamamiento de Amnistía Internacional. 