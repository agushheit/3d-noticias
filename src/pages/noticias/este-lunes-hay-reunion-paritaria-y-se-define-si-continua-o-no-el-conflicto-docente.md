---
category: Agenda Ciudadana
date: 2021-11-01T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/paritariasalesso.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Este lunes hay reunión paritaria y se define si continúa o no el conflicto
  docente
title: Este lunes hay reunión paritaria y se define si continúa o no el conflicto
  docente
entradilla: Desde Amsafé adelantaron que esperan una "oferta superadora". De no acordar,
  advirtieron que "habrá asambleas en toda la provincia".

---
Este lunes será la reunión [paritaria](https://www.unosantafe.com.ar/paritaria-a7979.html) entre [docentes](https://www.unosantafe.com.ar/docentes-a8183.html) y el [gobierno](https://www.unosantafe.com.ar/gobierno-a22630.html) [provincial](https://www.unosantafe.com.ar/provincial-a23965.html), como parte de una serie de encuentros técnicos para llegar a un acuerdo. Los gremios adelantaron que esperan "una oferta superadora" del Ejecutivo respecto al 17 por ciento en tres partes que se termina de pagar en enero de 2022.

La secretaria General de Amsafé provincial, Sonia Alesso aseguró que "si el lunes el gobierno no mejora la propuesta habrá asambleas en toda la provincia". Los docentes públicos tuvieron este viernes la primera de las dos reuniones técnicas que acordaron con el gobierno, pero le ponen plazos al diálogo con los funcionarios provinciales. Este viernes se discutieron temas vinculados a la obra social (Iapos) y al boleto educativo.

La relación entre Amsafé y el gobierno provincial no es la mejor. Venía tensa y se complicó aún más luego de que el Ministerio de Trabajo anunciara que iba a liquidar los salarios docentes sin el incremento del 17% a cobrar en tres tramos que sí aceptaron los otros trabajadores estatales (ATE, UPCN) y el sindicato de docentes particulares (Sadop). Además, aseguró que iba a descontar los días que los maestros habían realizado huelgas.

"Hay mucho malestar en la docencia", le aseguró Alesso a La Capital en un claro mensaje al gobierno provincial. Los maestros esperan una mejorada oferta salarial, por lo que no descartan nuevas medidas de fuerza si no se pone algo nuevo sobre la mesa.

Luego del paro 48 horas de la semana pasada (la semana anterior había sido de 24 horas), el gremio acusó a los responsables del ministerio de Trabajo y de Educación de "amenazar" a los docentes en lugar de sentarse a discutir salarios en el ámbito de la paritaria, mecanismo fijado por ley en la provincia desde hace años.

De acuerdo a un comunicado difundido por Amsafé, el gremio planteó que los decretos Nº 2.173 y Nº 2.180, referidos a salarios, "constituyen un acto arbitrario e inédito, desconociendo que el ámbito genuino de discusión es el paritario, donde la representación mayoritaria por el sector docente es el de la Asociación del Magisterio de Santa Fe".

A su vez, el gremio exigió que el gobierno provincial "cese con medidas extorsivas como son los descuentos por los días huelga". Además, en la mesa paritaria se puso de manifiesto el pliego reivindicativo surgido de la asamblea provincial.

Este lunes se espera haya novedades en la próxima cita entre las partes y allí quedará definido si hay acuerdo o vuelven medidas de fuerza, conforme a lo que se resuelva en las asambleas departamentales.

Por lo pronto, el gobierno dejó en claro que "es la mejor" propuesta salarial que tiene para ofrecerle a los maestros y que "es la última oferta".