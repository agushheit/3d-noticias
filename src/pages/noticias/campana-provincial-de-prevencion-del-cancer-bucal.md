---
category: Agenda Ciudadana
date: 2020-12-06T11:38:53Z
thumbnail: https://assets.3dnoticias.com.ar/cancer-bucal.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Campaña provincial de prevención del cáncer bucal
title: Campaña provincial de prevención del cáncer bucal
entradilla: El objetivo consiste en detectar precozmente esta enfermedad y fomentar
  hábitos saludables que permiten evitar su aparición.

---
El Gobierno de la provincia mediante el Ministerio de Salud, a través de su Agencia de Control del Cáncer y la Dirección Provincial de Odontología, recuerdan que **el 5 de diciembre se conmemora el día Nacional de la lucha contra el cáncer bucal**, por lo que organizan una jornada virtual de prevención y detección temprana

El cáncer bucal afecta a unas tres mil personas al año en todo el país, el 4 por ciento de los diagnósticos de cáncer. Su origen está vinculado en un 80 por ciento a hábitos fácilmente prevenibles como el consumo de tabaco y alcohol.

## **¿CÓMO PREVENIR EL CÁNCER BUCAL?**

Este tipo de cáncer se produce en los tejidos de la boca, que incluyen la lengua, labios, encías y mejillas. 

**La mejor manera de prevenir esta enfermedad es manteniendo una alimentación saludable y una buena higiene bucal, moderando el consumo de alcohol y evitando el tabaco**.

 Además, cuidando la exposición solar utilizando protector labial, manteniendo sexo oral seguro con preservativos y barreras de látex, ya que el virus del papiloma humano (HPV) también constituye un factor de riesgo. 

La consulta periódica al odontólogo u odontóloga es indispensable para la prevención de este tipo de cáncer y la correcta higiene bucal.

## **SÍNTOMAS**

Existen además algunos síntomas a los cuales se debe prestar atención y ameritan una consulta al dentista: llagas que no cicatrizan, bultos que antes no existían, adormecimiento de alguna parte de la boca, manchas blancas o rojas en encía, lengua y mejillas o dificultad para deglutir. La movilidad o pérdida de alguna pieza dentaria sin causa aparente o usar prótesis que lastimen o provoquen sangrado son también motivos de consulta.

Además, **adquirir simples cambios en el estilo de vida reduce el riesgo de cáncer bucal**: no fumar, beber con moderación, mantener una dieta saludable en el consumo de frutas y verduras, proteger los labios con protector solar y sostener una correcta higiene bucal.

## **CHARLA VIRTUAL SOBRE PREVENCIÓN Y DETECCIÓN TEMPRANA**

La Dirección Provincial de Odontología en conjunto con la Agencia de Control del Cáncer organizan una Jornada virtual de Prevención y Detección temprana en ocasión del Día Nacional de la prevención del Cáncer Bucal. Será el viernes 11 de diciembre a las 10 de la mañana a través de la Plataforma Zoom

Profesionales, equipos de salud y población en general podrán participar de las conferencias moderadas por el Dr. Gonzalo Pucheta, especialista en Cirugía Máxilo Facial y Jefe de la Unidad de Medicina Oral del CEMAFE, cirujano del servicio de Cirugía Estética del Hospital de Niños Dr. Orlando Alassia.

* **_Desórdenes potencialmente malignos de la mucosa bucal y cáncer bucal_**, dictado por la Dra. Patricia Masquijo Bisio, estomatóloga y Doctora en Odontología de la Universidad de Buenos Aires (UBA). Es parte de los Servicios de Odontología y el de Epidemiología y Carcinogénesis ambiental del Instituto de Oncología Angel H. Roffo.


* **_Rol del odontólogo antes, durante y después de un diagnóstico de cáncer bucal_**, a cargo de la Dra. Marisa Paz, Profesora adjunta de la Cátedra de Estomatología de la Universidad Nacional de Rosario (UNR) y titular del Servicio de Estomatología del Hospital Provincial de Rosario.