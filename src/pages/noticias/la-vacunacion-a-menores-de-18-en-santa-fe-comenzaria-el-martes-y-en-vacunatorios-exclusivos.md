---
category: Agenda Ciudadana
date: 2021-07-30T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/MENORES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La vacunación a menores de 18 en Santa Fe comenzaría el martes y en vacunatorios
  exclusivos
title: " La vacunación a menores de 18 en Santa Fe comenzaría el martes y en vacunatorios
  exclusivos"
entradilla: |2

  Hasta este jueves a la tarde había 14.624 inscriptos. Estiman que en la provincia son más de 70 mil los jóvenes de entre 12 y 17 años con factores de riesgo.

---
La vacunación a menores de 18 años con comorbilidades comenzaría el martes en la provincia y se prevé que en las grandes ciudades, Santa Fe y Rosario, se utilicen vacunatorios exclusivos. Se espera que el inicio de la campaña sea a nivel nacional, en todas las jurisdicciones del país.

El gobierno de Santa Fe aún aguarda por las 70.280 dosis de Moderna, la vacuna aprobada por organismos internacionales y por el país para que sea aplicada en menores de entre 12 y 17 años. Fuentes del Ministerio de Salud confirmaron a UNO Santa Fe que el arribo se producirá entre este viernes y lunes.

La cartera sanitaria estima que la población de menores con factores de riesgo que esperan recibir la vacuna en la provincia es de entre 70 mil y 76 mil; y que hasta este jueves había 14.624 inscriptos.

Si bien no se dieron mayores precisiones, el gobierno provincial está pensando en vacunatorios exclusivos para esta franja poblacional. Cabe recordar que los centros de vacunación en esta capital son cuatro: Cemafé, Centro de Educación Física (CEF) N°29, Esquina Encendida y La Redonda.

El gobierno nacional informó que la distribución de la vacuna Moderna ya comenzó. En total, se repartirán 900 mil dosis de un total de 3.500.000 que fueron donadas por Estados Unidos al país.

Cabe recordar que desde este miércoles se pueden inscribir en el registro provincial de vacunación quienes se encuentran en esa franja etaria y declarar el factor de riesgo que padece. Para ello, es necesario ingresar a [https://www.santafe.gob.ar/santafevacunacovid/inicio](https://www.santafe.gob.ar/santafevacunacovid/inicio "https://www.santafe.gob.ar/santafevacunacovid/inicio") y completar el formulario. Aquellos que ya estaban inscriptos podrán editarlo y agregar la comorbilidad respectiva.

Esta semana, la ministra de Salud, Sonia Martorano, indicó que "la decisión que se tomó es comenzar la semana que viene y de 12 a 17 años con condiciones priorizadas", cuya consideraciones son "la diabetes 1 y 2, obesidad (por encima del percentil 97), enfermedades crónicas renales, cardíacas y pulmonares; inmunodeficiencia y certificados únicos de discapacidad y embarazadas bajo condición de su profesional", explicó Martorano a la hora de confirmar que la provincia recibirá "70.240 para primeras dosis y después evaluarán si se necesitan enviar más dosis".

"Hoy estamos instando a la inscripción de las categorías priorizadas, las alergias no son la condición. Por ahora no vamos a avanzar con los niños sanos, es lo que se acordó con la Sociedad de Pediatría", precisó.

Por su parte, la ministra de Salud de la Nación, Carla Vizzotti, anticipó con relación a la vacunación contra la Covid-19 para adolescentes de entre 12 y 17 años, señaló que "estamos planificando que las jurisdicciones arranquen con la mayoría de los registros de adolescentes priorizados y el lunes que viene, en el Consejo Federal de Salud, hacer el lanzamiento", explicó.