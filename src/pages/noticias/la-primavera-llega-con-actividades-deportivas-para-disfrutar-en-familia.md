---
category: La Ciudad
date: 2021-09-20T06:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/ACTIVIDADESPRIMA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: La primavera llega con actividades deportivas para disfrutar en familia
title: La primavera llega con actividades deportivas para disfrutar en familia
entradilla: Durante los fines de semana y los días martes y miércoles, la Municipalidad
  invita a participar de propuestas gratuitas y al aire libre en distintos espacios
  de la ciudad.

---
La Municipalidad de Santa Fe, a través de la Dirección de Deportes, invita a participar de actividades físicas y recreativas gratuitas en distintos puntos de la capital provincial para celebrar la llegada de la primavera. Las propuestas iniciaron este sábado, desde las 16, en el Centro de Deportes de la Costanera con una clase de ritmos al aire libre y para toda la familia.

Además, un grupo de alumnos de la Escuela Municipal de Newcom participó este domingo de encuentros amistosos y recreativos en el Camping Municipal “Julio Migno” de la vecina ciudad de San José del Rincón. El objetivo era que ambos grupos de adultos mayores puedan compartir un grato momento, después de mucho tiempo sin encontrarse por el contexto de pandemia.

Las propuestas continúan el martes 21 por la mañana en el Centro Gallego, en Avenida Galicia 1357, a partir de las 10, con una exhibición de artes marciales a cargo de alumnos de todas las edades. A la tarde, continúan en el horario de 15 a 17 en el Polideportivo Parque Garay con tres actividades como funcional, newcom (voley para personas adultas mayores) y zumba, diseñadas para participar en familia.

El miércoles 22, la iniciativa sigue en el Centro Gallego por la tarde, de 17 a 19, con cuatro actividades como baile, ritmos, hockey y vóley. Participarán de la jornada alumnas y alumnos de la escuela municipal que promueve estas actividades y se invita a todos los vecinos y vecinas a que se sumen a este encuentro saludable.

**Protocolos**

Cada actividad se complementará con juegos y propuestas para compartir, y se realizarán bajo estricto protocolo sanitario en el marco de la pandemia por Covid-19. Para participar, se debe concurrir con barbijo o cubrebocas y elementos de higiene como alcohol en gel y toalla para garantizar la limpieza de manos. También, se solicita concurrir con agua para una correcta hidratación.