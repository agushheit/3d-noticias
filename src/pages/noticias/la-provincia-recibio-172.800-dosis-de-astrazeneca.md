---
category: Estado Real
date: 2021-06-05T06:15:18-03:00
thumbnail: https://assets.3dnoticias.com.ar/PROVINCIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia recibió 172.800 dosis de Astrazeneca
title: La Provincia recibió 172.800 dosis de Astrazeneca
entradilla: Se distribuyeron en 144 localidades en el marco del operativo de vacunación
  contra el Covid 19, que suma inoculaciones los domingos.

---
La provincia de Santa Fe, que el jueves aplicó la vacuna número un millón contra el Coronavirus, recibió este viernes 172.800 dosis AstraZeneca, que se distribuyeron en 144 localidades, para continuar con el histórico operativo desplegado en todo el territorio.

En este marco, el Ministerio de Salud indicó que se continuará con la vacunación los días domingos en las ciudades de Venado Tuerto, Rosario, Rafaela, Santa Fe y Reconquista.

Cabe señalar que la llegada de las vacunas permite continuar avanzando con la población objetivo priorizada en el esquema de vacunación contra Covid, y que actualmente más del 65% de este grupo ya recibió, al menos, una dosis.