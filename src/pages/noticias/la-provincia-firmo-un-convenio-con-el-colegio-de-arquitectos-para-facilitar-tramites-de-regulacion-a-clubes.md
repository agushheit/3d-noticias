---
category: Estado Real
date: 2020-12-29T10:53:14Z
thumbnail: https://assets.3dnoticias.com.ar/2912-arquitectos.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: La provincia firmó un convenio con el Colegio de Arquitectos para facilitar
  trámites de regulación a clubes
title: La provincia firmó un convenio con el Colegio de Arquitectos para facilitar
  trámites de regulación a clubes
entradilla: El acuerdo buscar llevar adelante acciones que permitan relevar, registrar
  y habilitar obras de infraestructura existentes a instituciones deportivas.

---
El Ministerio de Desarrollo Social, a través de la Secretaría de Deportes, y el Colegio de Arquitectos de Santa Fe, firmaron este lunes un convenio para acompañar y facilitar los trámites de regularización administrativa de obras en instituciones deportivas de la provincia.

El convenio busca llevar adelante acciones que permitan relevar, registrar, regularizar y habilitar obras de infraestructura existentes, declaradas o no, de instituciones deportivas de primer grado (clubes).

Del acto, llevado a cabo en la sede del Colegio de Arquitectos de la provincia de Santa Fe en la ciudad de Rosario, participaron el ministro de Desarrollo Social, Danilo Capitani; y la secretaria de Deportes, Claudia Giaccone.

«Es un convenio muy importante que se venía trabajando desde hace bastante tiempo, con la idea de regularizar las obras, tener bien armonizado bajo todos los reglamentos las ampliaciones, las instalaciones eléctricas y de agua», explicó Capitani, y dijo que las acciones «garantizan seguridad a cada una de las personas que visitan los clubes».

Por su parte, Giaccone coincidió en definirlo como un convenio «muy importante y trascendente, que ayuda y acompaña a todas las instituciones deportivas a regularizar, relevar sus obras de infraestructura y traer seguridad. Es un esfuerzo que hacen los colegios de llegar a la comunidad, nosotros acompañando, y habilitando obras que no están regularizadas».

En tanto, el presidente del Colegio de Arquitectos de la provincia, Germán Picarelli, explicó que el acuerdo permite una reducción de «los aportes en un 75 por ciento en beneficio de los clubes y de esta manera se colabora para permitir que regularicen sus instalaciones, adecuándola a los planos».

<br/>

<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">**EL CONVENIO**</span>

Entre algunos puntos, el acuerdo establece que el profesional (arquitecto) que realice el relevamiento y presentación del plano respectivo será propuesto por el club o el gobierno local. En tanto, el Colegio se compromete a liquidar honorarios al menor costo posible según cada situación, posibilitando planes de pago en cuotas y generando así una importante bonificación sobre los sellados correspondientes.

El objetivo es que todos los clubes puedan contar con la regularización de planos y obras para poder así acceder a las habilitaciones municipales, trámite importante para el correcto funcionamiento de las instituciones deportivas. Por su parte, la provincia se compromete a invitar a los municipios y comunas a sancionar una ordenanza que permita la exención de multas, recargos e intereses por la falta de declaración anterior a este convenio.