---
category: Agenda Ciudadana
date: 2021-05-20T08:39:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/kulfas.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario El Litoral
resumen: Se inicia la inscripción de las empresas al Repro II
title: Se inicia la inscripción de las empresas al Repro II
entradilla: El gabinete económico anticipó ayudas crediticias para gastronómicos.
  Kulfas denunció el “rulo” de la carne.

---
El gobierno nacional ratificó que el lunes abrirá la inscripción al Programa de Recuperación Productiva 2 (Repro II), correspondiente al pago de los salarios del mes de mayo. Las empresas podrán anotarse a partir del próximo lunes 24 de mayo y hasta el lunes 31 del mismo mes (inclusive).

El gabinete económico detalló que durante las primeras semanas del mes de mayo, a través del Repro, se pudieron atender más de 13.600 empresas, lo que implica un colectivo de trabajadoras y trabajadores cercano a los 400.000, con una inversión total superior a los 5.000 millones de pesos.

El detalle fue difundido tras una reunión encabezada por el jefe de Gabinete de Ministros de la Nación, Santiago Cafiero, se llevó a cabo esta mañana en el Salón de los Científicos de la Casa Rosada, de la que participaron los ministros de Economía, Martín Guzmán; de Desarrollo Productivo, Matías Kulfas y de Trabajo, Claudio Moroni, junto al presidente del Banco Central, Miguel Pesce; la titular de la Administración Federal de Ingresos Públicos (AFIP), Mercedes Marcó del Pont; la vicejefa de Gabinete, Cecilia Todesca Bocco; los secretarios de Política Económica, Fernando Morra, y Hacienda, Raúl Rigo y la jefa de Gabinete del Ministerio de Economía, Melina Mallamace.

Durante el encuentro se repasó la reducción de las contribuciones patronales para los sectores empresarios afectados, el Bono para el personal de la Salud, el programa especial -en el marco del Repro- para  monotributistas del sector gastronómico, el incremento de las prestaciones vinculadas con la economía social (a través del Programa Potenciar Trabajo), la ampliación del universo de niños y niñas receptores de AUH, entre otras. 

Los integrantes del Gabinete Económico remarcaron que los programas ya en marcha podrán ser ampliados, teniendo en cuenta los anuncios en materia sanitarias para controlar la pandemia.

Kulfas aseguró sobre el nuevo llamado al Repro que “las empresas podrán percibir ayuda para el pago de los salarios correspondientes al mes de mayo, y hoy a la tarde estamos anunciando con el Banco Nación y el Banco Central, algunas medidas que tienen que ver con financiamiento a sectores como la gastronomía, para colaborar con la mejora en las condiciones de atención de sus clientes que hoy enfrenta la gastronomía, producto de los protocolos para controlar la pandemia”.

 

**El “rulo de la carne”**

Con respecto a las exportaciones de carne, Kulfas apuntó: “El mercado interno de carnes continúa en funcionamiento. Estamos trabajando en diferentes reuniones con la mesa de la carne, con el consorcio de exportadores, con el sector de matarifes, con diferentes áreas de gobierno, analizando las diferentes medidas”.

 En esa línea explicó: “Encontramos funcionamientos que no son transparentes, algunos sectores están presionando la demanda y los precios haciendo un negocio de exportación que se lo ha denominado “rulo ganadero”, que tiene que ver con comprar el ganado, hacer la faena, exportar la carne y, en muchos casos, con sectores que no liquidan la exportación y lo que hacen es quedarse con las divisas en el exterior para liquidarlas en mercados paralelos que presentan una brecha cambiaria respecto del mercado oficial” y finalizó: “Se trata de toda una serie de elementos que ya han sido identificados y que queremos evitar. Lo que queremos es tener las reglas de juego claras para evitar prácticas especulativas que están afectando el mercado interno”.