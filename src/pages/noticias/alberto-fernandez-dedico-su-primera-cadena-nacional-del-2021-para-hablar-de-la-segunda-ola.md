---
category: Agenda Ciudadana
date: 2021-03-19T07:23:20-03:00
thumbnail: ''
layout: Noticia con video
link_del_video: https://youtu.be/JWJuRW-EdOc
author: con información de LT10
resumen: Alberto Fernández dedicó su primera cadena nacional del 2021 para hablar
  de la segunda ola
title: Alberto Fernández dedicó su primera cadena nacional del 2021 para hablar de
  la segunda ola
entradilla: Remarcó la escasez de vacunas a nivel global. Dio algunos consejos a la
  población y pidió no salir del país.

---
El presidente Alberto Fernández habló por cadena nacional a través de un mensaje grabado sobre la segunda ola de coronavirus en el país.

Luego de reunirse en la tarde de este jueves con gobernadores se esperaba algún tipo de anuncio que no llegó. El Presidente se limitó a aconsejar a la población sobre los cuidados y protocolos que debe seguir para evitar los contagios.

“Mi prioridad era cuidar la salud de los argentinos”, comenzó su discurso, donde agregó que también desde el Gobierno está al tanto de la escasez de vacunas “a nivel global”.

Mencionó que hasta el momento llegaron al país solo el 6 por ciento de las dosis que se contrataron, y que no se cuentan con dosis suficientes, pero aseguró que es optimista sobre la llegada de más vacunas en las próximas semanas para ampliar la campaña de inmunización.

En ese sentido, el mandatario pidió extremar los recaudos para que la pandemia "no nos vuelva a aislar". “Soy consciente de las dificultades, del compromiso y del empeño para acompañar a las niñas y a los niños, todas nuestras actividades se han visto afectadas. Sé que muchos sentimos hartazgo y cansancio. El mundo va a tener que convivir con la pandemia, también nosotros aprendimos a convivir y producir con el covid, a educar con cuidados, a cumplir los protocolos, a cuidarnos en actividades deportivas, culturales, en encuentros familiares. Mi mayor preocupación sigue siendo la salud, salvar vidas y cuidar la recuperación económica”, agregó.

**Los consejos**

Durante su breve mensaje, primera cadena nacional desde que asumió, Fernández realizó algunas recomendaciones a los ciudadanos:

Dos metros de distancia, uso adecuado del barbijo, higiene de manos, mantener los ambientes ventilados y realizar actividades al aire libre.

Está totalmente desaconsejado viajar al exterior. Quienes lo hagan, al regresar deberán aislarse y tener cuidados estrictos.

Las fronteras continuarán cerradas para turistas extranjeros, como lo están desde el 24 de diciembre.

"Tenemos que saber que cuando hayamos avanzado en la vacunación estaremos empezando a controlar la pandemia. Solo entonces podremos mirar hacia atrás y podremos recordar estos momentos desafiantes que hoy vivimos", completó sin agregar ninguna medida o anuncio.