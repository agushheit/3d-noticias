---
category: Agenda Ciudadana
date: 2021-10-15T06:15:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/precios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Suma apoyos el acuerdo de precios para bajar el valor de la canasta básica
title: Suma apoyos el acuerdo de precios para bajar el valor de la canasta básica
entradilla: Los principales referentes de ambos sectores minoristas expresaron su
  respaldo y confianza en la reciente medida impulsada por la Secretaría de Comercio.

---
Representantes del sector almacenero y del supermercadismo manifestaron este jueves su apoyo al acuerdo impulsado por el Gobierno nacional para “bajar el precio de la canasta básica” con el objetivo de que tenga menor impacto sobre “el salario promedio”.  
  
El titular de la Federación de Almaceneros de la provincia de Buenos Aires, Fernando Savore, expresó su confianza en el efecto positivo que tendrá en las y los consumidores el acuerdo de precios por 90 días alcanzado por el Gobierno nacional, empresas de consumo masivo y cadenas de supermercado, anunciado ayer por el secretario de Comercio Interior, Roberto Feletti.  
  
Por su parte, el director de Marolio, Juan Fera, señaló que la compañía alimenticia acompañará el acuerdo que estabiliza los precios de 1.247 productos de consumo masivo durante 90 días, y anticipó que se trabajará con la Secretaría de Comercio Interior en revisar la cadena de valor para analizar “de dónde vienen los incrementos”.  
  
Feletti dijo que el objetivo de la política de precios de su gestión y, en particular del nuevo acuerdo con los empresarios, será “bajar el precio de la canasta básica” para reducir su impacto sobre “el salario promedio”, y “asegurar la expansión del consumo vía salarios en el último trimestre del año”.  
  
Este miércoles, el funcionario anunció un acuerdo con las principales empresas de consumo masivo y cadenas de supermercados para mantener los precios estables de 1.247 productos de consumo masivo -que incluyen los del programa Precios Cuidados- durante 90 días, hasta el 7 de enero de 2022.  
  
En diálogo con El Destape Radio, Feletti planteó que el peso de la canasta básica sobre los salarios llevó a encarar esta política: “Vimos que la canasta básica alimentaria representaba en diciembre de 2019 el 9% del salario promedio del trabajador registrado en actividad, mientras que en agosto de este año estaba en 11%”.  
  
Como consecuencia, el funcionario aseguró que, pese a las paritarias, los trabajadores no pudieron “recomponer su salario real respecto de la canasta básica alimentaria”.

“Dijimos con el empresariado de parar la pelota y de tratar de no seguir en un escenario en el cual se ha deteriorando el salario a costa del consumo alimenticio”, manifestó, y puntualizó que la recomposición de dicha situación será el “objetivo nodal de la Secretaría de Comercio Interior”.  
  
Respecto de la reacción de los empresarios, el funcionario indicó que “en principio fue favorable en ambas reuniones”, y que la Secretaría está esperando para el transcurso de este jueves el envío de las listas de precios.  
  
“Yo creo que algunos empresarios deben ver esto como un proceso negativo pero si uno analiza las cuentas no hay perdida de rentabilidad, y si hubiera alguna se puede compensar tranquilamente por cantidad al ser un contexto expansivo”, agregó.  
  
El titular de la Federación de Almaceneros de la provincia de Buenos Aires, Fernando Savore, expresó su confianza en el efecto positivo que tendrá el acuerdo de precios por 90 días

  
En ese sentido, Fera afirmó que "lo importante es poder trabajar en conjunto para solucionar el grave problema que es la inflación, y que nos afecta a todos”; y adjudicó los aumentos de precios a “múltiples factores”.  
  
“En diciembre de 2019 nosotros pagábamos la tonelada de trigo a US$165, US$170, hoy vale US$ 240”, indicó sobre el aumento de los precios internacionales de las commodities, y agregó: “Entiendo que necesitamos exportar porque necesitamos dólares pero también son productos que se consumen acá”.  
  
Al mismo tiempo, el empresario, hijo del supermercadista dueño de Maxiconsumo, Victor Fera, consideró que es necesario “incentivar la inversión” para “generar una gran competencia de precios”.  
  
Feletti aseguró que pese a las paritarias, los trabajadores no pudieron “recomponer su salario real respecto de la canasta básica alimentaria”.

Por su parte, Savore afirmó destacó el gesto del Gobierno que los convocó a la firma del acuerdo: “Desde que comenzó esta gestión nuestro sector nunca había sido convocado, pese a que representamos 70.000 comercios en todo el país"  
  
En declaraciones a El Destape Radio, el representante de los almaceneros bonaerenses planteó sus dudas respecto al cumplimiento de este compromiso por parte de los supermercados, y manifestó su desacuerdo con que estos grandes centros de venta funcionen en grandes ciudades.  
  
"Tengo mis dudas de que el 1 de enero los precios estén al mismo precio (en los supermercados). En Europa están afuera de la ciudad y estaríamos de acuerdo con sacarlos" de los grandes centros urbanos, subrayó.  
  
En este sentido, aclaró que "la Secretaria de Comercio se va a tener que enfrentar a estos monstruos. El riesgo es que esto genere una olla a presión y luego de tres meses aumenten mucho los precios”.