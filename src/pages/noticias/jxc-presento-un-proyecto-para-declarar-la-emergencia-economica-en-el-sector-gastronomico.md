---
category: Agenda Ciudadana
date: 2021-05-26T08:22:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/jxc.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: JxC presentó un proyecto para declarar la emergencia económica en el sector
  gastronómico
title: JxC presentó un proyecto para declarar la emergencia económica en el sector
  gastronómico
entradilla: Legisladores de Juntos por el Cambio presentaron en las últimas horas
  en el Congreso de la Nación el proyecto de ley de «Emergencia Gastronómica», aplicable
  tanto a nivel provincial como a nivel nacional.

---
Una de las diputadas que compartió el proyecto a través de sus redes fue la legisladora nacional Natalia Villa, quien al momento de dar a conocer la presentación de la iniciativa opositora, escribió:

«Presentamos el proyecto de ley de #EmergenciaGastronomicaYa en Provincia y Nación, en un trabajo conjunto de legisladores de ambas cámaras. La situación del rubro es alarmante y este proyecto propone soluciones concretas», explicó.

«Juntos y en equipo es la mejor manera de salir adelante», añadió la legisladora.

Otra de las legisladoras que opinó respecto al proyecto presentado fue Verónica Barbieri, diputada de Juntos por el Cambio, que respecto al tema añadió:

«Acompañemos a los que trabajan y dan trabajo, Acompañemos las medidas sanitarias con medidas de alivio fiscal», sentenció.

En los argumentos, el proyecto afirma que alrededor de 10 mil locales gastronómicos debieron cerrar sus puertas debido a la crisis que trajo la pandemia y las posteriores restricciones.

«Miles de familias perdieron su fuente de ingresos y muchos jóvenes se quedaron sin su primer trabajo. La ayuda al sector es insuficiente y de difícil acceso», reza la iniciativa de JxC.

Con la ley, se buscará a nivel nacional crear el Certificado Único de Comercio en Emergencia Gastronómica y prohibir a las empresas prestadoras de los servicios esenciales la suspensión de los servicios en caso de mora o falta de pago.

Por otro lado, tiene como objetivo reducir en un mínimo de un 50% los aportes y las contribuciones patronales, como así también eximir del pago de anticipos del impuesto a las ganancias por el ejercicio fiscal en curso.

Otra de las medidas de carácter nacional, si se aprueba la iniciativa, será la de congelar los precios de los alquileres de las locaciones que desempeñen actividades gastronómicas.

    https://drive.google.com/file/d/1Df2hpvRmmVeGz8CMmo9PXn9cFMydJaUi/view