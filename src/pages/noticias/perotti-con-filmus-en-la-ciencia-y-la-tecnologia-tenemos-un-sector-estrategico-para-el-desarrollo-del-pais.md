---
category: Estado Real
date: 2021-10-02T06:00:29-03:00
thumbnail: https://assets.3dnoticias.com.ar/PEROMUS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti con Filmus: “en la ciencia y la tecnología tenemos un sector estratégico
  para el desarrollo del país”'
title: 'Perotti con Filmus: “en la ciencia y la tecnología tenemos un sector estratégico
  para el desarrollo del país”'
entradilla: En Rosario, el gobernador de la provincia y el ministro de la Nación participaron
  de la presentación de los proyectos seleccionados en la convocatoria “Emprende EBT”.

---
El gobernador Omar Perotti y el ministro de Ciencia, Tecnología e Innovación de la Nación, Daniel Filmus, participaron este jueves de la presentación de los proyectos seleccionados en la convocatoria provincial “Emprende EBT”, en el marco del nuevo espacio de desarrollo de tecnologías del Instituto de Procesos Biotecnológicos y Químicos de Rosario (IPROByQ-CONICET).

El objetivo de la propuesta es generar un espacio donde los emprendedores tecnológicos puedan recibir financiamiento para sus prototipos, y tengan la posibilidad de presentar sus proyectos a actores del ecosistema emprendedor de base tecnológica y potenciales inversores, proveedores y clientes.

En la oportunidad, el gobernador Perotti valoró estar “compartiendo un espacio de pensamiento estratégico común, que rescata el potencial de nuestra gente, y reconoce las cualidades de nuestro sistema científico y tecnológico”.

“Aquí tenemos un sector estratégico para el desarrollo del país; y no tenemos que permitir que no tengan el apoyo, el acompañamiento y la visibilización necesaria, por eso tenemos nuevos instrumentos para ese desarrollo”, dijo el mandatario, quien agregó que “estamos convencidos que es momento para que los talentos puedan quedarse en la Argentina, y tener acompañamiento y reconocimiento aquí”.

En esa línea, Perotti destacó la importancia de contar en Santa Fe con un consejo del conocimiento, y de “generar herramientas para lograr el desarrollo de las empresas y tener trabajo calificado desde la Argentina hacia el mundo”.

“Todos estos emprendimientos y desarrollo de este sector van a ser las que más van a facturar en la Argentina”, vaticinó el gobernador y felicitó a los ganadores de la convocatoria: “Argentina necesita este tipo de mensajes”.

**Política de Estado**

En tanto, el ministro Daniel Filmus coincidió en que “lo más importante es la articulación entre la Nación y la provincia, y entre lo público y lo privado”, y señaló que “un país como la Argentina lo que necesita es una sumatoria de esfuerzos”.

“Si hay algo que queremos para el futuro es transformar la ciencia y la tecnología en política de Estado, no nos puede pasar nunca más que nos paralicemos”, dijo el ministro, y recordó la gestión de Omar Perotti como senador nacional, donde presentó la ley de financiamiento para ciencia y tecnología.

“Es muy grato estar en una provincia que apuesta a la ciencia y la tecnología cuando muchas provincias esperan todo de Nación, en cambio esta provincia acompaña a la Nación, y esa articulación es la que nos muestra lo virtuoso de trabajar juntos en la misma dirección”, cerró Filmus.

Finalmente, la secretaria de Ciencia, Tecnología e Innovación de Santa Fe, Marina Baima, informó que la provincia triplicó su presupuesto para ciencia y tecnología, e indicó que el objetivo es “apoyar a investigadores y emprendedores en etapas iniciales de proyectos de investigación y desarrollo”, además de “fomentar alianzas entre empresas, grupos de investigación de universidades y centros de investigación o desarrollo tecnológico, para mejorar la competitividad del sector productivo a partir de soluciones y propuestas que integren nuevo conocimiento”.

El encuentro, llevado a la cabo en el edificio de Mitre 1998, en Rosario, contó también con la presencia de la presidenta del Conicet, Ana Franchi; el presidente de la Agencia Nacional I+D+i, Fernando Peirano; el director del IPROByQ, Hugo Menzella, y el rector de la UNR, Franco Bartolacci, entre otros.

**Doce proyectos**

En el acto se distinguieron los proyectos seleccionados en la convocatoria provincial “Emprende EBT”, de apoyo a proyectos de innovación tecnológica para emprendedores y empresas de base tecnológica, entre los que se presentaron: Pérgolas Bioclimáticas Inteligentes; Plataforma para la gestión de los procesos de distribución y logística; Dispositivo Móvil Sanitario Helper Table; Diagnóstica salud conectada; Plataforma Cloud para monitoreo y control de Tambos; Bioconversión de materiales orgánicos y residuos de poco valor provenientes de la industria cervecera; Material bio-based como alternativa al cuero; Desarrollo y validación de dos ensayos moleculares para el diagnóstico temprano y serotipificación del virus Dengue; Optimización de la producción de Natamicina; Desarrollo de un equipo acondicionador de señales para control de procesos en la industria alimenticia y la construcción; Sistema de monitoreo inteligente para celdas de baja, media y alta tensión, y Plataforma integral para la gestión de los procesos de distribución.

**Espacio IPROByQ**

Las nuevas instalaciones del Instituto de Procesos Biotecnológicos y Químicos (IPROByQ) de doble dependencia UNR-CONICET, incubarán nuevos emprendimientos científico-tecnológicos y para desglosar los alcances de las nuevas herramientas que promueve la Agencia Santafesina de Ciencia, Tecnología e Innovación (ASaCTeI).

El IPROByQ se encuentra en etapa de mudanza a un nuevo espacio, donde continuarán trabajando profesionales de distintas disciplinas químicas y biotecnológicas, para investigar y transferir sus conocimientos para desarrollar tecnologías de impacto medibles y acompañar en los procesos a estos proyectos.