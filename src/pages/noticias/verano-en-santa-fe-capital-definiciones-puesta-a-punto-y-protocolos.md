---
layout: Noticia con imagen
author: "  Por José Villagrán"
resumen: Verano  y protocolos
category: La Ciudad
title: 'Verano en Santa Fe Capital: definiciones, puesta a punto y protocolos'
entradilla: 'Fue una semana con muchas definiciones de cara a la estación estival
  y cómo la ciudad se prepara en medio de la pandemia. Semana de muchas reuniones
  entre el gobierno provincial y municipal. '
date: 2020-11-06T23:48:03.675+00:00
thumbnail: https://assets.3dnoticias.com.ar/santa-fe-playas.jpg
link_del_video: ''

---
Ya se asoman varias definiciones pensando en el movimiento turístico en la capital.

Con un nivel de contagios que se mantiene en la ciudad de Santa Fe, las actividades del intendente Emilio Jatón y su gabinete giraron en torno a reuniones con funcionarios y ministros del gobierno provincial para ir analizando las alternativas y protocolos que deberán llevarse a cabo durante los próximos meses.

Entendiendo que, debido a la situación pandémica, se hará mucho turismo interno y la ciudad debe prepararse para eso, la semana deja muchas definiciones al respecto.

En primer lugar, el día martes se realizó un encuentro entre funcionarios del ejecutivo provincial y municipal con el fin de de avanzar en el diseño de un plan de trabajo ante el comienzo de la temporada de verano teniendo en cuenta las medidas de distanciamiento y los protocolos necesarios en el marco de la pandemia de coronavirus, así como también para instrumentar el Programa Santa Fe Plus en la ciudad.

La reunión estuvo encabezada por el secretario de Turismo provincial, Alejandro Grandinetti, quien estuvo acompañado de manera virtual por el subsecretario de Inversión y Procesos de Fiscalización y Control del Turismo, Santiago Sánchez.

En tanto que, por el lado de la municipalidad de Santa Fe estuvieron participando el secretario de Producción y Desarrollo Económico, Matias Schmüth; y el director de Turismo, Franco Arone.

“Estuvimos coordinando las tareas y los protocolos que van a elevar a los ministerios de Salud y de Trabajo, para que los ciudadanos y ciudadanas puedan disfrutar de los espacios abiertos y de a poco ir entrando en la temporada de verano”, dijo Grandinetti. En tanto que para Arone "Santa Fe Plus es una iniciativa innovadora que va a permitir que la ciudad de Santa Fe tenga una buena temporada de verano."

Pero la noticia más importante se dio el día miércoles por la mañana en la Estación Belgrano. Allí, el intendente Jatón recibió a la ministra de Salud Sonia Martorano y a los ministros Roberto Sukerman y Jorge Llonch, de Trabajo y Cultura respectivamente.

La novedad más importante que se conoció fue la habilitación para que las colonias de vacaciones puedan funcionar durante el verano santafesino. “Vamos a presentar los protocolos con los anuncios correspondientes a su debido tiempo” confirmó Sukerman, quien además afirmó que “estamos trabajando con las empresas en todo el corredor de la rutas provincial Nº 1 y nacional Nº 11, que tienen complejos de cabañas, guías de pesca, hotelería, etc., y queremos alentar esas actividades económicas.”

Por su parte el mandatario santafesino expresó que “los protocolos generales nos van a dar otro marco porque, allí, van a tener que convivir las actividades culturales, las familiares y las de grupo. Seguramente, vamos a empezar a trabajar en temas específicos en cada lugar, porque cada sitio de la ciudad tiene su idiosincrasia”.

### **¿Cómo se está preparando la ciudad capital para el comienzo de temporada?**

Tras la reuniòn de mitad de semana con funcionarios provinciales, Emilio Jatón también anunció que la inauguración de la temporada de verano en la ciudad capital se verá retrasada hasta el mes de diciembre. “Se va a demorar mucho más: no va a ser el 15 de noviembre, sino que estamos pensando en los primeros días de diciembre”, concluyó.

Aún así, ya se han tomado varias iniciativas pensando en qué actividades permitir y cómo se hará la implementación de los protocolos.

El primer paso se dio con la licitación de los paradores (7 en total) de las Costaneras Este y Oeste. Hasta el momento, se otorgaron cuatro licitaciones: en el Espigón I y en el Espigón II, ambos en la Costanera Oeste, y los otros dos, en la Costanera Este: uno en el balneario Piedras Blancas y el otro en el sector conocido como Punta Norte. Para el resto, habrá un segundo llamado cuya apertura será el 11 de noviembre.

En lo que respecta a los protocolos, desde el municipio se está pensando en un abordaje integral donde se coordinen las acciones estratégicas y operativas con los administradores de los paradores, los guardavidas, el personal municipal, los ciudadanos y los turistas.

En este punto, se va a insistir con el distanciamiento social y seguro; se realizarán acciones de sensibilización y campañas de difusión para que la población esté atenta a los protocolos; y se dispondrán de las medidas de seguridad sanitarias para las personas que trabajarán en los servicios y prestaciones de los paradores concesionados por la Municipalidad.

De igual modo, durante la semana se informó sobre el avance de las obras en algunos espacios públicos. Por ejemplo, en ambas costaneras se está realizando movimientos de la arena, despeje de vegetación de las playas, recuperación de veredas y trabajos de pintura.

En los espacios verdes también se están llevando tareas de mantenimiento. Las mejoras en el Parque Sur llevan un avance del 80%, donde se concretan pruebas en el sistema de chorros ubicado en los tres piletones, se demarca un sector para encuentros recreativos y deportes playeros y los juegos de plaza fueron restaurados por completo.

Del mismo modo, ya se inició la intervención del Parque Garay, en la zona de los piletones. Allí se hacen tareas de rellenado en las fisuras, para que no pierda agua o suba arena y luego se va a pintar. También se hará toda una área nueva gastronómica que tal vez no esté terminada para esta temporada, para que puedan disfrutar todos los santafesinos, entre otras intervenciones.

Se está pensando en una temporada atípica de verano, donde el municipio y el gobierno de la provincia deberán centrar los esfuerzos en tratar de hacer cumplir las disposiciones sanitarias y los protocolos, siempre y cuando se mantengan los niveles de contagio y a la espera por la llegada de la vacuna.

***

###### Comunicación con Franco Arone: [audio](https://assets.3dnoticias.com.ar/franco-arone.mp3 "Franco Arone")

***