---
category: La Ciudad
date: 2021-05-22T06:00:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/municipales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Municipales exigen el dictado de normas para convocar a trabajar solo a esenciales
title: Municipales exigen el dictado de normas para convocar a trabajar solo a esenciales
entradilla: Para Festram en los municipios y comunas las disposiciones son contradictorias
  a las resoluciones presidenciales.

---
La situación sanitaria que vive la provincia fue advertida reiteradamente junto al pedido de vacunación para el sector. La decisión adoptada por el residente de la Nación debe tener estricto cumplimiento con el objetivo de reducir inmediatamente la circulación, los contagios y las muertes por coronavirus, récord en Santa Fe por las interpretaciones propias de las autoridades políticas, más que las sanitarias.

Es obligación primordial de las Organizaciones Sindicales preservar la Vida y la Salud tanto de los trabajadores y las trabajadoras que representamos como la de la comunidad de cada una de las localidades. Por lo expuesto, Festram resuelve:

1\. Las Autoridades Municipales y Comunales deberán convocar mediante el sistema de guardias rotativas al personal mínimo e imprescindible dictando los actos administrativos específicos como requisito legal obligatorio. Los mismos serán efectuados en consenso con cada Organización Sindical.

2\. Los Sindicatos por sí o por medio de los Comités de Salud y Seguridad Laboral (Ley 12.913) deberán evitar la circulación innecesaria de trabajadores suspendiendo aquellas actividades que no resulten imprescindibles para la lucha contra la pandemia. Ningún trabajador o trabajadora, que no haya recibido la vacunación podrá ser obligado a cumplir servicios de alta exposición de contagios en las zonas declaradas de Alto Riesgo y de Alerta Epidemiológica y Sanitaria.

3\. Queda expresamente prohibido convocar a trabajar a quienes están enmarcados dentro de la población de riesgo.

4\. Cuando en alguna Municipalidad o Comuna se verifica la existencia de contagios directos, o como consecuencia de transmisión comunitaria, la administración permanecerá cerrada en todas sus dependencias para permitir su desinfección o sanitización.

5\. En caso de fallecimiento de un compañero o compañera por coronavirus en ocasión de trabajo, el gremio podrá determinar el cese de actividades en señal de duelo y en repudio por la falta de vacunación.

6\. Expresamos nuestro incondicional apoyo al presidente en su lucha por la preservación y la vida de los Argentinos, cumpliendo y haciendo cumplir todas las medidas sanitarias dispuestas por el Gobierno Nacional.

7\. Exigimos que el gobierno provincial, mediante las Autoridades Ministeriales pertinentes adviertan a los Ejecutivos Municipales y Comunales los alcances de los anuncios presidenciale s y que cesen en adoptar medidas contra los trabajadores y trabajadoras municipales, como por ejemplo la de aquellos Intendentes que intentan reducir salarios del personal exento de prestar servicios.

8\. Expresamos nuestro más enérgico repudio a los sectores retrógrados que por razones políticas promueven las marchas y movilizaciones de carácter criminal con la propagación del peor drama humanitario que se vive en nuestro pueblo.

Por último, manifestamos nuestro pesar y solidaridad con todos los familiares de las y los Compañeros fallecidos por coronavirus y señalamos la disposición de Festram de acompañar a la familia municipal en estos graves momentos.