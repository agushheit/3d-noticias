---
category: Agenda Ciudadana
date: 2021-06-08T08:05:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/procrear.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: 'Procrear II: en 20 meses planean construir 198 viviendas en el Parque Federal'
title: 'Procrear II: en 20 meses planean construir 198 viviendas en el Parque Federal'
entradilla: Este lunes se suscribieron los convenios para la construcción de las unidades
  habitacionales que tendrán una inversión de más de $1.233 millones

---
Este lunes se suscribieron convenios para construir 237 viviendas del plan Procrear II, en el marco del Programa Casa Propia, que lleva adelante el Gobierno Nacional en el Parque Federal de Santa Fe.

Las unidades habitacionales se desarrollarán en el Parque Federal de la ciudad de Santa Fe, donde se construirán 198 viviendas Procrear II, con una inversión de más de $1.233 millones y un plazo de obra de 20 meses.

En tanto, para la localidad de Gálvez se firmó el convenio de posesión que permitirá la transferencia de tierras al Programa Procrear II, para ejecutar 39 viviendas con una inversión de $ 162.096.701 y un plazo de obra de 12 meses.

El gobernador Omar Perotti participó este lunes de una videoconferencia encabezada por el ministro de Desarrollo Territorial y Hábitat de la Nación, Jorge Ferraresi.

El Desarrollo Urbanístico en el Parque Federal comprende la construcción de 198 nuevas unidades funcionales con una superficie total 18.449.06 m² (se incluyen 2823,68 m² de estacionamiento). Se construirán 194 nuevas viviendas de dos dormitorios con 64 m² y 4 viviendas de dos dormitorios, aptas para personas con discapacidades, también de 64 m². Las mismas se desarrollan en una manzana dentro de un conjunto de vivienda, desarrollado en un basamento de planta baja y 3 pisos, y dos torres que van desde el 4to. al 19vo. piso.

En tanto que en Gálvez se construirán 39 nuevas viviendas unifamiliares en dúplex con una superficie total de 2.527 m². Se edificarán 38 viviendas de dos dormitorios con 65 m² y una vivienda de dos dormitorios, apta para personas con discapacidades, también de 65 m².

Cabe señalar que los interesados podrán inscribirse a poco de finalizar la obra en los sitios que estarán habilitados a tal fin, a través de la Secretaría de Hábitat, dependiente del Ministerio de Infraestructura, Servicios Públicos y Hábitat de la provincia.

En la oportunidad, Perotti afirmó que “estamos muy entusiasmados por estos convenios. Vienen muy bien y son necesarios para movilizar mano de obra. Ojalá que, vacunación mediante, nos encuentre recorriendo el terreno con un nivel de actividad muy importante”, afirmó el gobernador y agradeció al ministro nacional.

Por su parte, Ferraresi sostuvo que “lo importante es seguir trabajando articuladamente, como nos pide el Presidente, en una política federal de vivienda, buscando distintas articulaciones. Para nosotros, poder generar trabajo y concretar sueños a partir del hábitat es maravilloso y la verdad es que con la provincia venimos trabajando de manera ejemplar, con objetivos claros y con un ida y vuelta muy fluido, sin ustedes sería imposible".

Del encuentro virtual también particiaron la ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana; los intendentes de las ciudades de Santa Fe, Emilio Jatón; y de Gálvez, Mario Fissore; y el secretario de Hábitat, Urbanismo y Vivienda, Amado Zorzón.

La titular de la cartera de Infraestructura santafesina dijo que “esto forma parte de un gran programa de Hábitat para la construcción de las viviendas. A pesar del momento que estamos atravesando, decimos que nadie puede dudar que los gobiernos provincial y Nacional, trabajando en forma coordinada, no perdemos de vista el objetivo de darle respuestas a nuestra gente”.