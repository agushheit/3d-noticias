---
category: La Ciudad
date: 2021-03-27T06:15:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/incluir.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La ciudad de Santa Fe adhirió al Plan Incluir
title: La ciudad de Santa Fe adhirió al Plan Incluir
entradilla: El programa tiene como finalidad mejorar la calidad de vida de las personas
  y grupos familiares en los barrios.

---
El Concejo de Santa Fe dió el visto bueno al Mensaje Nº 1, el primero de 2021 remitido por la administración de Emilio Jatón: otorgó la adhesión de la Municipalidad local, en todos sus términos, al “Plan Incluir” creado por Decreto Nº 1184/20 del Poder Ejecutivo Provincial.

El Plan Incluir tiene como finalidad “mejorar la calidad de vida de las personas y grupos familiares en los barrios mediante la generación de obras de infraestructura estratégica, vinculadas a la mejora del hábitat, el equipamiento barrial, el saneamiento y el acceso a energía y agua seguras; fortalecer las redes sociales del barrio promoviendo el encuentro, la participación y la convivencia en el espacio público, para prevenir la violencia interpersonal”.

También, “impulsar la participación social y ciudadana en espacios barriales, donde autoridades locales y provinciales interactúen con las y los vecinos, a fin de priorizar en conjunto los problemas a resolver., y abordar a las familias desde una perspectiva integral asegurando el acceso a derechos fundamentales”.

En el artículo 3° del referido decreto provincial se establece que la autoridad de aplicación del Plan Incluir “será el Ministerio de Gestión Pública, el cual tendrá la responsabilidad primaria de su desarrollo integral y ejercerá además las funciones de coordinación, supervisión, monitoreo y evaluación de su ejecución”, estando facultado para “Recepcionar, evaluar y seleccionar proyectos, programas u obras a ser financiados total o parcialmente con los recursos del Plan, los que podrán ser presentados por los municipios, comunas u organismos competentes del Estado Provincial”.