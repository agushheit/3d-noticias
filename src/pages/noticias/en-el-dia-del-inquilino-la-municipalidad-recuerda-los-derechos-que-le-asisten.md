---
category: La Ciudad
date: 2021-10-05T06:00:56-03:00
thumbnail: https://assets.3dnoticias.com.ar/INQUILINO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: En el Día del Inquilino, la Municipalidad recuerda los derechos que le asisten
title: En el Día del Inquilino, la Municipalidad recuerda los derechos que le asisten
entradilla: 'La dirección de Derechos y Vinculación Ciudadana atiende las consultas
  e inquietudes de los vecinos y vecinas de la ciudad que forman parte de relaciones
  de contratos de alquileres. '

---
Cada primer lunes de octubre se celebra el Día del Inquilino, una oportunidad propicia para visibilizar la situación de muchos santafesinos que conforman una relación locativa. En ese marco, la municipalidad recuerda que a través de la Oficina del Inquilino, se brindan diferentes servicios destinados a las partes que intervienen en este tipo de operaciones.

Incluida en la dirección de Derechos y Vinculación Ciudadana, la Oficina del Inquilino brinda recomendaciones a tener en cuenta, en el momento de iniciar un contrato de alquiler, renovarlo o finalizarlo. También interviene por medio de audiencias de facilitación y otras herramientas de resolución pacífica de conflictos, para lograr acuerdos entre quienes locadores y locatarios.

Durante el año en curso, el área cobró relevancia gracias a la nueva Ley de Alquileres y sus reglamentaciones, dictadas durante el aislamiento preventivo y obligatorio dispuesto por la pandemia de Covid-19. En ese sentido, se visibilizó su importancia para los vecinos y vecinas de la ciudad. Cabe destacar que la Oficina acompaña no sólo a los inquilinos consultantes sino también a los propietarios que muchas veces necesitan asesoramiento tanto en relación a sus derechos como a sus obligaciones.

**626 consultas**

El director de Derechos y Vinculación Ciudadana municipal, Franco Ponce de León, informó que en lo que va del año, la Oficina del Inquilino respondió 626 consultas de vecinos en materia de alquileres y consorcios, tanto de manera presencial como virtual. De igual modo, señaló que el canal de contacto más utilizado en ese período fue la línea celular, mediante la aplicación de mensajería Whatsapp.

A ello sumó que se llevaron adelante reuniones con los diferentes sectores, tal es el caso de organizaciones de inquilinos, cámaras inmobiliarias y administradores de consorcios, entre otros. Además, el área visitó todas las oficinas de Distrito, para conversar con vecinalistas sobre las problemáticas de inquilinos, propietarios y administradores. Y se brindaron charlas gratuitas para abordar los puntos salientes de la nueva ley de alquileres.

Ponce de León recordó que “la relación contractual se basa en dos pilares fundamentales: el equilibrio de derechos y obligaciones entre ambas partes y la confianza que debe sostener ese vínculo”. Por ello, señaló la importancia de “la equidad para acortar distancias entre las partes de un contrato locativo”.

“El 2020 fue un año atravesado por muchas variantes: la pandemia, la nueva Ley, el aislamiento, el teletrabajo, la pérdida o disminución de los ingresos económicos; todo esto generó una nueva relación entre las ambas partes involucradas en un contrato de locación. Por ello, desde la Oficina del Inquilino nos encomendamos asistir estos procesos de transformación de la realidad, obteniendo mayor profundidad y más herramientas a la hora de ayudar y acercarnos a los santafesinos”, afirmó el director.

**Vías de comunicación**

La Oficina del Inquilino atiende todas las consultas de manera presencial, de lunes a viernes de 7:15 a 13 horas en el Predio Ferial Municipal (Las Heras 2883, ala suroeste); el Whatsapp 3425315450; por teléfono, al 4574119; al correo electrónico derechosyvinculacionciudadana@santafeciudad.gov.ar; o en la Oficina Virtual del municipio, a través del siguiente[ link](https://oficinavirtual.santafeciudad.gov.ar/web/inicioWebc.do?opcion=noreg).