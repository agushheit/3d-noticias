---
category: La Ciudad
date: 2021-07-21T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/vacunacion-covid-4.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Santa Fe evalúa pedir un certificado de vacunación para algunas actividades
title: Santa Fe evalúa pedir un certificado de vacunación para algunas actividades
entradilla: La ministra de Salud de Santa Fe, Sonia Martorano, señaló que el pase
  sanitario es una posibilidad para recuperar "algo de normalidad"

---
La ministra de Salud de Santa Fe, Sonia Martorano, admitió que se evalúa implementar un pase sanitario o certificado de vacunación que habilite para la realización de determinadas actividades como asistir a recitales, restaurantes y bares. “Pedir el carné de vacunación permitirá un mayor nivel de seguridad. No tenemos el formato ni la ficha porque tenemos que avanzar hasta tener a toda la población con una dosis colocada”, expresó la funcionaria.

El certificado o carné de vacunación para poder cumplir con algunas actividades ya fue anunciado oficialmente este lunes por el gobernador de Buenos Aires, Axel Kicillof, y el intendente de Rosario, Pablo Javkin, también lo mencionó como una posibilidad incluso para promover aún más la campaña de vacunación.

La titular de la cartera sanitaria de Santa Fe expresó este martes que la idea de exigir la presentación de un certificado de inoculación para realizar ciertas actividades “es algo que se está evaluando”.

“Eso es así porque se puede ir volviendo a algunas actividades. No se tratará de una normalidad como la conocíamos antes de la pandemia, pero sí quizás este tipo de certificados para ingresar o participar de algunos eventos como un recital. O los bares, que ahora funcionan con aforo reducido. Pedir un carné de vacunación para entrar a esos lugares permitirá tener una mayor seguridad. Pero antes, lo fundamental, es tener a todos al menos con una vacuna colocada”, remarcó Martorano.

La funcionaria provincial aclaró que había algunas provincias, como Córdoba, que no tienen el mismo de nivel de inscripciones en sus campañas de vacunación. “En Córdoba fueron el 60 por ciento los que expresaron su voluntad de vacunarse. En cambio, en Santa Fe fue más del 80 por ciento. Ese es un buen número. Por otra parte, estamos haciendo campaña para más jóvenes. No creo que sea que no quieren vacunarse. En estos días se incrementaron las inscripciones".

La supuesta reticencia de las personas más jóvenes para inscribirse en el plan de vacunación "tuvo que más que ver con que percibían veían las vacunas como más lejanas. Otros ya estaban inscriptos, pero otros no pensaban que íbamos a estar en este grupo a esta altura del año”.

“Hoy estamos vacunando a la población entre 18 a 25 años. Les pedimos a todos que se anoten para que nos podamos organizar, pero vamos bien con la campaña”, agregó.