---
layout: Noticia con imagen
author: .
resumen: La educación es esencial
category: La Ciudad
title: Solicitan que los últimos años del secundario puedan culminar el ciclo en
  forma presencial
entradilla: Tras un año de mucha incertidumbre y muchos cambios, la educación no
  fue la excepción.
date: 2020-11-14T18:07:54.489Z
thumbnail: https://assets.3dnoticias.com.ar/educaci%C3%B3n-escencial.jpg
---
Desde la agrupación “Padres Organizados” se solicita, a nivel nacional, la posibilidad de que los 5 y 6 años del secundario puedan tener un último día de clases en forma presencial, despedirse de sus compañeros, docentes e instituciones y cerrar un ciclo en un año que, sin dudas, quedará marcado en cada uno de nosotros para siempre.

“Comenzamos como un grupo de padres preocupados por la realidad que vemos con nuestros hijos. Vemos que se termina el año, nuestros chicos fueron a la escuela 1 semana en todo el año y están por despedirse de este ciclo esta etapa de estudiantes apagando una computadora. Hoy nuestro pedido obedece a una necesidad de terminar este ciclo de una forma más sana emocional y psicológicamente”, sostuvo Adelina Pizzarulli, miembro de la agrupación Padres Organizados Santa Fe.

Por su parte, Mariano Cejas, impulsor de la agrupación en Santa Fe comentó que “Padres organizados surge como una preocupación ante la situación de suspensión de clases presenciales con la declaración de la pandemia allá por el mes de marzo y que deja a miles de estudiantes sin la posibilidad de asistir a la escuela. Con una alternativa donde valoramos la intensa labor de los docentes que a través de las herramientas digitales intentaron continuar con el servicio educativo, pero reconocemos que esas herramientas son insuficientes.”

Ambos representantes concuerdan con que la falta de sociabilización de los chicos ha afectado muchísimo el desempeño de los mismos en todos los niveles educativos, tanto nivel inicial, primario como secundario. “la imposibilidad de terminar un ciclo sin verse, sin compartir y sin sociabilizar es un vacío enorme y no ayuda a que puedan emprender un nuevo camino” sostuvo Cejas quien, por otro lado, habló de casi 1 millón y medio de chicos que quedaron desvinculados del sistema educativo durante el 2020. “es un panorama complejo, donde no vemos que los gobiernos lo tengan en su agenda y, si bien este movimiento de padres logró instalar el tema en la sociedad, no vemos que las autoridades nos brinden una respuesta”.

Si bien durante esta semana, el Honorable Concejo Municipal de la Ciudad de Santa Fe aprobó de manera unánime un proyecto para solicitar que se realicen los actos de colación grados en los niveles inicial, primario, secundario y terciario, de establecimientos públicos y privados, por el momento, tanto el gobierno nacional como el Gobierno de la Provincia de Santa Fe no autorizan los actos de colación presenciales, aún en ambientes abiertos y con cumplimientos de protocolos. Tampoco brindan ningún tipo de definición con respecto al inicio del ciclo educativo del próximo año y la preocupación aumenta.

“Esta pandemia ha dejado una gran desigualdad en materia educativa, porque no todas las familias tienen acceso a herramientas tecnológicas y, aquellas que tienen algún medio de conexión no siempre pueden garantizar el efectivo cursado cuando las familias son numerosas y tienen varios niños en edad escolar. Mis hijos me dicen ‘Mamá, es una locura que podamos ir a los bares a comer o tomar algo, pero no a la escuela” culminó Adelina.