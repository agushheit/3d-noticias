---
category: Agenda Ciudadana
date: 2021-11-03T06:15:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/alberto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Presidente propuso "crear mecanismos de pagos por servicios ecosistémicos"
title: El Presidente propuso "crear mecanismos de pagos por servicios ecosistémicos"
entradilla: Alberto Fernández aseguró que países como la Argentina "constituyen un
  sostén a los medios de vida de todo el planeta y contribuyen a la seguridad alimentaria
  mundial".

---
El presidente Alberto Fernández expuso este martes su propuesta de "crear mecanismos de pago por servicios ecosistémicos" y ratificó el compromiso de la Argentina con el Acuerdo de París, al participar de la Cumbre de Líderes de la Conferencia de las Naciones Unidas sobre el Cambio Climático (COP26) que se desarrolla en Glasgow.  
  
Allí, el Presidente también volvió a subrayar la necesidad de aplicar los Derechos Especiales de Giro del Fondo Monetario Internacional para "un gran pacto de solidaridad ambiental" y remarcó la necesidad de "comprometer aportes concretos, en el marco del principio de responsabilidades comunes pero diferenciadas, que estén acompañados por medios para su implementación".  
  
"Debemos crear mecanismos de pago por servicios ecosistémicos, canje de deuda por acción climática e instalar el concepto de deuda ambiental", dijo Fernández en su discurso ante la Sesión Plenaria de la Cumbre que se desarrolla en la ciudad escocesa de Glasgow.  
  
Al explicar su propuesta, el mandatario argentino señaló que "es necesario aplicar la emisión de los Derechos Especiales de Giro del Fondo Monetario Internacional a un gran pacto de solidaridad ambiental, que incluya a los países de bajos ingresos y renta media, y que sirva para extender los plazos de las deudas y la aplicación de menores tasas".  
  
"Debemos fortalecer el multilateralismo ambiental e impulsar la capitalización de los Bancos Regionales de Desarrollo", indicó el Presidente.  
  
En ese marco, el mandatario reafirmó el compromiso de la Argentina con la Convención Marco de las Naciones Unidas sobre el Cambio Climático y su Acuerdo de París, y ratificó que el país "adopta la lucha contra el cambio climático como política de Estado".  
  
Fernández detalló, a continuación, las diferentes acciones que viene implementando el Gobierno argentino en ese sentido.  
  
"Elevamos nuestra Contribución Determinada Nacional (NDC) un 27,7% respecto a la de 2016. Presentamos una Segunda Comunicación de Adaptación, con metas, lineamientos y necesidades para abordar esta agenda. Asumimos el compromiso de favorecer la inversión y el desarrollo de encadenamientos productivos nacionales para avanzar con una matriz energética inclusiva, estable, soberana, sostenible y federal", reseñó.  
Asimismo recordó que "impulsamos un complejo productor y exportador de hidrógeno como nuevo vector energético" y, en el marco legal, señaló que fue enviado al Parlamento "un proyecto de ley sobre electro movilidad".  
  
En tanto, anunció que adoptará "medidas profundas para erradicar la deforestación ilegal, tipificándola como delito ambiental" y el envío al Parlamento de un nuevo proyecto de Ley de Presupuestos Mínimos de Protección Ambiental de los Bosques Nativos, y detalló que, en la dimensión cultural, están promoviendo "el tratamiento de la Ley Federal de Educación Ambiental".  
  
Según Fernández, "se trata de compromisos tangibles, concretos" que ratifican el compromiso de la Argentina en la lucha mundial contra el cambio climático.

"Como he manifestado en varias oportunidades, para avanzar con la agenda de transformaciones necesarias debemos crear fuentes de financiamiento innovadoras y nuevas reglas e incentivos globales", resumió en su exposición, que inició agradeciendo al primer ministro británico, Boris Johnson, "por la invitación y la organización" de la conferencia.  
  
Según aseveró Fernández, "la crisis sanitaria de la pandemia Covid-19 no hizo sino desnudar una crisis mucho mayor y multidimensional de insostenibilidad y desequilibrio que afecta al ambiente, la sociedad y la economía", y remarcó que "esta triple crisis amplió las brechas preexistentes, entre países y personas".  
  
Al mismo tiempo, refirió que el impacto sobre los recursos y bienes naturales comunes ha superado límites que plantean desafíos concretos que deben ser atendidos".  
  
En este escenario, dijo que "países como la Argentina constituyen, a través de sus ecosistemas, un sostén a los medios de vida de todo el planeta, al tiempo que contribuyen, de modo decisivo, a la seguridad alimentaria mundial".  
  
Para el mandatario argentino, "los aportes concretos que debemos comprometer deben inscribirse en el marco del principio de responsabilidades comunes pero bien diferenciadas".

"Estos aportes deben estar acompañados por medios para su implementación que nos permitan una transición hacia una economía limpia, con menos carbono y resiliente a los impactos del cambio climático", resumió.  
  
Hacia el final de su disertación, el jefe de Estado argentino hizo un llamado a "cumplir los compromisos y obligaciones asumidas por las economías desarrolladas y garantizar el acceso a los 100 mil millones destinados a impulsar la acción climática".  
  
Es que en el marco de la cumbre líderes o representantes de más de 100 países firmaron un compromiso para frenar y revertir la deforestación, en el primer logro concreto del encuentro internacional en el Reino Unido.  
  
El acuerdo se firmó en el tercer día de la COP26 y segundo y último día de la cumbre parte de la conferencia, cuando decenas de líderes mundiales, entre ellos el presidente Fernández, detallaron sus compromisos con la lucha contra el cambio climático.  
  
"La Argentina no tiene intención de que esta COP quede solo en palabras", dijo Fernández y propuso la creación de un Comité político y técnico sobre "financiamiento climático, con representación equitativa de países desarrollados y países en desarrollo".  
  
El Presidente propuso que ese Comité "trabaje en la definición de una hoja de ruta sobre cómo movilizar los fondos necesarios, que reconozca el principio de responsabilidades comunes pero diferenciadas y que tenga en cuenta el endeudamiento y las limitaciones estructurales, así como las necesidades de bienestar social".  
  
"Es momento de actuar, hagámoslo juntos", concluyó el jefe de Estado su exposición ante los líderes mundiales.  
  
En el marco de la Cumbre de Líderes de la Conferencia de las Unidas sobre el Cambio Climático (COP26) que se desarrolla en Glasgow, en el Reino Unido, el Gobierno argentino firmó hoy un compromiso para frenar y revertir la deforestación junto a líderes y representantes de más de 100 países, según se informó hoy oficialmente.  
  
La firma del acuerdo fue uno de los logros concretos de la Cumbre, junto con el Compromiso Global de Metano, también firmado por Argentina y otras naciones en el encuentro internacional sobre el clima.  
Argentina también firmó hoy el Compromiso Global de Metano, una iniciativa lanzada por Estados Unidos, la Unión Europea y sus socios para reducir las emisiones globales de metano, a fin de mantener el objetivo de limitar el calentamiento a 1,5 grados Celsius el alcance.  
  
El compromiso fue firmado por más de 100 países que representan el 70% de la economía mundial y casi la mitad de las emisiones antropogénicas de metano.