---
category: Deportes
date: 2021-04-13T08:04:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/river-colon.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Marisa Lemos
resumen: 'SE PERDIO UNA BATALLA PERO NO LA GUERRA '
title: 'SE PERDIO UNA BATALLA PERO NO LA GUERRA '
entradilla: 'En su primera derrota del torneo, colón perdió contra River por 3 a 2
  en el Monumental con un jugador menos por la expulsión de Castro, por la novena
  fecha, zona A, de la Copa de la Liga Profesional de fútbol (LPF)

'

---
El joven Lucas Beltrán (35m.) abrió la cuenta para el equipo que conduce el DT Marcelo Gallardo. Sobre el final del primer tiempo, Cristian Bernardi logró el empate para el Sabalero.A los 7 minutos del segundo tiempo, el volante Alexis Castro vio la tarjeta roja tras un codazo arriba al defensor de River, David Martínez.

 A los 13 minutos del segundo tiempo, Fabrizio Angileri volvió a poner a River arriba en el marcador con un tiro libre.Cuando corrían 28 minutos de la segunda mitad, el árbitro cobró penal para el “Millo” tras una mano de Moschión. Montiel fue el encargado y extendió la diferencia por 3 a 1.  

A los 39 del segundo tiempo, Rodrigo Aliendro descontó para el Sabalero de cabeza tras una pelota parada. Sin embargo, no le alcanzó al equipo del “Barba” y fue 3 a 2 para River en el Monumental. Con este resultado, Colón sigue puntero de su zona, pero perdió el invicto de 8 partidos que mantenía en esta copa.