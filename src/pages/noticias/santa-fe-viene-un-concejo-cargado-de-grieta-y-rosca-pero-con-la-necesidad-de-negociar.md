---
category: La Ciudad
date: 2021-11-16T06:15:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejppicado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Luciano Andreychuk / El Litoral
resumen: 'Santa Fe: ¿viene un Concejo cargado de grieta y rosca, pero con la necesidad
  de "negociar"?'
title: 'Santa Fe: ¿viene un Concejo cargado de grieta y rosca, pero con la necesidad
  de "negociar"?'
entradilla: La primera mayoría la sigue teniendo el oficialismo, pero perdió un escaño.
  Y la segunda (Juntos por el Cambio) aglomera sectores de una coalición que proyecta
  recuperar la Intendencia en 2023. El PJ quedó rezagado.

---
La elección del Concejo santafesino, que dio lugar a una inesperada reconfiguración del mapa político legislativo, abre una paleta de lecturas posibles e interrogantes. Y de lo que se viene, el primer eje que aparece es la tensión de fuerzas que podría haber entre el interbloque oficialista que responde al intendente Emilio Jatón, sector que logró un gran repunte de votos (y anímico, pues todo quedó alicaído tras las PASO), y la segunda mayoría, que detenta la coalición de Juntos por el Cambio.

 Dicho de otro modo: esa tensión de fuerzas deviene de dos modelos de ciudad opuestos de cara a las elecciones ejecutivas de 2023. El primer modelo es el del frente que gobierna y que pretende seguir consolidando el proyecto, ahora con la figura de Laura Mondino potenciada como la carta que va a salir a bancar fuerte los trapos en la arena del debate político-deliberativo.

 El otro modelo de ciudad es del sector que gobernó la ciudad durante 12 años, otrora “socio” del Frente Progresista. La coalición de Juntos por el Cambio ahora quedará conformada por tres referentes del sector del ex intendente José Corral, uno del riñón del ex intendente Mario Barletta (ambas extracciones provienen del denominado Grupo Universidad), y un edil más que es Pro de linaje puro.

 ¿Qué pulseadas podrían esperarse? ¿Habrá grieta y más rosca política en las sesiones del Legislativo local? Probablemente sí, y cabe recordar que el contexto de la agenda pública durante 2022 será propicio para que esto se dé: se acercan discusiones sobre el relleno sanitario (hoy de escasa vida útil, y con proyectos para reconvertirlo en metropolitano); el sistema de transporte por colectivos (¿se van a mandar o no los pliegos de licitación al Concejo?); y el horizonte de la Terminal de Ómnibus (¿se reactivará el masterplan?), entre otros temas clave para los santafesinos.

 Las decisiones que se tomarán, las palabras que se dirán en el recinto, las declaraciones públicas de unos y otros, inexorablemente se expresarán con la vista en el 2023, o sea, mirando retener y fortalecer (en el oficialismo) la Intendencia de la ciudad, o recuperarla, en el caso de Juntos por el Cambio. Otro dato no menor será saber si el bloque oficialista retiene la Presidencia, o si se la “arrebatan” de las manos. ¿Podría ocurrir esto? Poco probable, aunque no se descarta.

 **Yo consensúo, tú consensúas...**

Pero al momento de tratar un tema importante -que impactará directa o indirectamente en la ciudadanía-, llega la hora de “negociar”, verbo infinitivo tomado aquí como sinónimo de consensuar, ésta última elegante palabra diplomática en su acepción, pero difícil en su puesta en práctica.  

 Aquí aparece primero el PJ, hoy disminuido por la mala elección: proyectaba obtener tres bancas de máxima, se quedó con una y perdió otra. Puertas adentro del partido corren autocríticas: “Algo no vimos bien en esta elección. Hay que empezar de nuevo...”, se lamenta una fuente partidaria.

 ¿Cómo jugarán los tres ediles del justicialismo en una instancia de consenso? ¿Seguirá siendo la oposición moderada que fue hasta ahora? ¿O bien fijarán posiciones más férreas?

 El oficialismo necesitaría esos votos (y otros) en, por ejemplo, una reñida autorización de toma de deuda que pida el intendente Jatón (de hecho, en el Proyecto de Presupuesto 2022 del Ejecutivo solicita visto bueno del Concejo de un empréstito por $ 400 millones para obras), ya que para eso se requieren dos tercios de las voluntades.

 Y luego aparecen otros tres concejales que no por estar en bloques unipersonales son menos importantes: es el escaño ocupado por Barrio 88, Guillermo Jerez, el de Unión Federal (Juan J. Piedrabuena) y el de Alianza Mejor, es decir, Saúl Perman. ¿Quién estará más cerca del oficialismo, o de Cambiemos, si es que se meten en esa lucha por dos modelos de ciudad? 

 Jerez, por ejemplo, ha mostrado cierta cercanía con el FPCyS, pero también tuvo críticas dichas a viva voz en el recinto de Salta 2943; Piedrabuena parece ser un filoperonista pero “no de adentro”, no del perottismo. Y Perman es una incógnita, y lo será hasta que asuma en su banca y pueda vérselo como legislador local, no ya como el outsider “simpático” y “antisistema” dando abrazos gratis en la Peatonal.

 La incógnitas (apenas algunas de todas las que podrían plantearse) están sobre la mesa. Resta empezar a descifrarlas, y para ello no hay oráculo que valga: las respuestas sobre cómo serán las relaciones de fuerzas en el Concejo versión 2022 irán surgiendo una vez que asuman los ediles electos y comienza a girar la rueda de la política legislativa local.