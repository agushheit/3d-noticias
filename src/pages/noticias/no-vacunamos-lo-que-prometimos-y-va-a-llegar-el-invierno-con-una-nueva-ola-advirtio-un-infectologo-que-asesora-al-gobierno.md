---
category: Agenda Ciudadana
date: 2021-02-26T07:31:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/lopez.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: DS
resumen: "“No vacunamos lo que prometimos y va a llegar el invierno con una nueva
  ola”, advirtió un infectólogo que asesora al Gobierno"
title: "“No vacunamos lo que prometimos y va a llegar el invierno con una nueva ola”,
  advirtió un infectólogo que asesora al Gobierno"
entradilla: Lo dijo Eduardo López. quien admitió un “déficit” de dosis. Y sostuvo
  que es fundamental inmunizar a más de 7 millones de adultos mayores. Crítica a Axel
  Kicillof.

---
El infectólogo Eduardo López, uno de los miembros del comité de expertos que asesora al Gobierno desde el inicio de la pandemia, admitió que no se llegó al número de vacunados prometido para febrero y que, a raíz de ello, advirtió que, de no avanzar con premura en la vacunación, se podría repetir una nueva ola de contagios, como la del invierno pasado.

"Lo que más me preocupa es que no llegamos a vacunar lo que habíamos prometido: 4 millones de dosis en enero de la vacuna Sputnik V y 14 millones en febrero. No han llegado y esto sí que no es bueno", sostuvo López, en declaraciones a Radio Rivadavia.

"Si usted no vacuna, va a ser lo mismo que el año pasado. Va a llegar el invierno y a empezar a aumentar y una nueva ola. Creo que esto es muy importante", agregó.

Para el infectólogo es crucial que antes de la llegada del frío se cubra "el target" principal en el que debería focalizarse el plan de vacunación: los mayores de 60 años. "Nuestro objetivo es de 7.400.000 adultos mayores para vacunar, como ideal. antes de que llegue el invierno", detalló para luego advertir: "Estamos con un déficit de vacunas que tenemos que subsanarlo lo más rápidamente posible".

López consideró que para evitar una nueva ola de contagios de la magnitud que hubo durante 2020 "tenemos tres meses o cuatro por delante" en los que "si trabajamos fuerte, creo que se puede evitar o por lo menos minimizar".

"La vacuna, aún con la primera dosis, tiene una eficacia del 71%, tanto la de Astrazeneca como la Sputnik V y además previene la enfermedad grave". explicó el especialista, pero volvió a enfatizar en su preocupación: "Ahora, si nosotros seguimos corriendo la fecha de arrancar con una vacunación masiva en el adulto mayor, entonces estamos en problemas para ver qué pasa en el mes de mayo o junio".

Según Lopez, en la actualidad, la pandemia en Argentina muestra cifras de "casos amesetados". "Ha caído de los 14 mil (contagios) de fin de diciembre, estamos entre los 7 y 8 mil, que es una meseta alta. Y tenemos una mortalidad que tampoco es baja", remarcó.

En la entrevista radial, el asesor epidemiológico del Gobierno no esquivó referirse a la polémica del Vacunatorio VIP montado en el Ministerio de Salud. "Es un episodio desafortunado y triste", sostuvo pero estimó que los ciudadanos "lo vieron como un hecho pésimo, pero igual la gente se quiere ir a vacunar, especialmente el adulto mayor, que está muy cansado del distanciamiento social y el aislamiento".

**Una dura crítica a Axel Kicillof**

Pero fue cuando a López le consultaron sobre las declaraciones de Axel Kicillof, que sus críticas se encendieron. El gobernador bonaerense había dicho no tener manera de corroborar si los que se dieron la vacuna son o no personal de la salud si mienten en una declaración jurada. "Eso es incorrecto", cruzó el infectólogo a la pregunta en el programa de Nelson Castro.

"Hay que tratar de pedirle a los políticos, en general, que no se metan con la ciencia porque siempre pierde la ciencia. Hay una matrícula nacional, tanto para las enfermeras como para los médicos (para chequear los datos). A mí me sorprende que digan eso, es fácil de verificar", se quejó el experto.