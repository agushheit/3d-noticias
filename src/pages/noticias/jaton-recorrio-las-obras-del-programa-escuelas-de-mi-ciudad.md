---
category: La Ciudad
date: 2021-10-27T06:00:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESCUELAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón recorrió las obras del programa “Escuelas de mi ciudad”
title: Jatón recorrió las obras del programa “Escuelas de mi ciudad”
entradilla: A través del programa que impulsa la Municipalidad, financiado por el
  Fondo de Asistencia Educativa (FAE), avanzan los trabajos en esta institución educativa
  de barrio La Esmeralda, así como otras de Santa Rosa de Lima.

---
“Escuelas de mi ciudad” es un programa que lleva adelante la Municipalidad, con financiamiento del Fondo de Asistencia Educativa (FAE). El objetivo es apoyar a las instituciones educativas públicas de la ciudad con la adquisición de mobiliario hasta la licitación de obras de mantenimiento y reparación en el entorno. Esta acción se puso en marcha en febrero y, actualmente, se está trabajando en la Escuela Nº 42 “General Gregorio Las Heras” (Av. Aristobulo Del Valle 8550), de barrio La Esmeralda. Hasta allí llegó esta mañana el intendente Emilio Jatón para recorrer las refacciones.

En esta escuela se llevan a cabo obras sobre la vereda de hormigón saneado en calle José Cibils, se construyen canteros sobre Aristóbulo del Valle y se dejó previsto un contrapiso para poner losetas graníticas con táctil para discapacidad visual. Asimismo, se comenzó con la limpieza de los muros de fachada con el fin de preparar la superficie para pintarlos. También se está haciendo el hormigonado de contrapiso en calle Aristóbulo del Valle para la preparación del espacio donde va a ser colocada la garita de colectivo y los accesos desde la calle.

Las obras también apuntan a mejorar fachadas y veredas mediante la limpieza, reparaciones y pintura (tanto de paredes como de rejas), cambio de cielorrasos, arreglos en las bajadas pluviales. En un futuro se instalarán nuevas “Veredas para jugar”.

La subsecretaría de Gestión Cultural y Educativa del municipio, Huaira Basaber, contó: “Mientras avanzamos con estas obras, seguimos presentando propuestas para la refacción, el mantenimiento y mejoras en los frentes y fachadas de otras escuelas. De esta manera empezamos a jerarquizar cada institución educativa, en cada barrio de la ciudad, generando otros ingresos y una mejor accesibilidad para cada una. Ubicamos garitas para el acceso al transporte y la novedad es que estamos poniendo Veredas para Jugar, que son espacios lúdicos que los niños y niñas pueden disfrutar”.

En esta línea, destacó la importancia de esta comisión del FAE, y que el intendente haya puesto en marcha todas estas propuestas, que “sea voluntad política y educativa, poder llevar adelante como municipio cada uno de estos proyectos”, manifestó la subsecretaria de Gestión Cultural y Educativa del municipio.

**Trabajo conjunto**

Por su parte, Susana Busularo, directora de la Escuela General Gregorio Las Heras, agradeció a los funcionarios tanto municipales como de la Regional IV de Educación y a los representantes del FAE “que estuvieron atentos a las necesidades”. “El cambio es notable, el ingreso y egreso de los alumnos es otro, se mejoró el entorno para cuando los padres traen a los chicos, la fachada, en definitiva todo es importante”, dijo la docente.

Destacó también que “estas mejoras van a evitar que haya accidentes en esta avenida que es muy concurrida”, y luego agregó: “Con esto se ordenó el estacionamiento frente al establecimiento educativo y por eso estamos muy contentos y agradecidos”.

**Obras importantes**

De la recorrida también participaron Mónica Henny delegada de la Regional IV de Educación e Ivana Pintos de la Federación de Cooperadoras Escolares de la Provincia de Santa Fe. Esta última, también destacó la importancia de poder hacer obras de esta magnitud: “Históricamente no hubo comisión que enfrentara este tipo de obras y desde las cooperadoras escolares proponemos, acompañamos y nos pone muy orgullosos de participar de este movimiento cooperador y de poder devolverle a las escuelas lo que les corresponde”.

Es necesario aclarar que se están interviniendo todas las escuelas de la ciudad, en la medida que se hacen los pedidos por parte de los directivos. “En algunos casos son obras de magnitud, en otros son de menor escala pero no menos importantes como arreglos de los sanitarios, limpieza de los tanques de agua, cortes de pasto, entre otros”, detalló Pintos.

Por último, la concejala Laura Mondino destacó también la importancia de este tipo de acciones. “Es un trabajo para destacar por parte del municipio, porque son obras que las propias escuelas estaban demandando. El intendente Jatón siempre tuvo la firme decisión de que los fondos del FAE son para las escuelas”, cerró la concejala.

Además de este establecimiento se interviene en las fachadas y entornos del Jardín Nº 173, del Centro de Educación Física Nº 52, del CEPA Nº 49, del Cecla Nº 20, de la Escuela Nº 809 y la ESSO Nº 512, del barrio Santa Rosa de Lima.