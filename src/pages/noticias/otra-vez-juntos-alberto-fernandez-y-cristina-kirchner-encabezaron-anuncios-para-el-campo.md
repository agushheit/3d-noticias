---
category: El Campo
date: 2021-10-01T06:15:05-03:00
thumbnail: https://assets.3dnoticias.com.ar/CAMPO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Otra vez juntos, Alberto Fernández y Cristina Kirchner encabezaron anuncios
  para el campo
title: Otra vez juntos, Alberto Fernández y Cristina Kirchner encabezaron anuncios
  para el campo
entradilla: Es la primera aparición pública conjunta luego de las PASO. Presentaron
  la "Ley de Fomento al Desarrollo Agroindustrial", junto con el ministro de Agricultura
  y Ganadería, Julián Domínguez.

---
El presidente Alberto Fernández encabezó esta tarde en el Museo del Bicentenario de la Casa Rosada la presentación del proyecto de ley de “Fomento al Desarrollo Agroindustrial”, en un acto del que también participó la vicepresidenta Cristina Fernández. Este es el primer acto público al que concurren los dos máximos mandatarios desde las pasadas PASO.

La iniciativa apunta a incrementar las exportaciones de bienes y servicios del sector y promueve el agregado de valor para crear empleos y promover inversiones, según informaron fuentes oficiales. La Ley se presentará en el Museo del Bicentenario. Además de Cristina Kirchner, entre otros funcionarios en los anuncios también estuvieron presentes el ministro de Agricultura, Ganadería y Pesca, Julián Domínguez y el Jefe de Gabinete, Juan Manzur.

Desde el gobierno de Alberto Fernández proponen agregar valor a los productos para crear empleos y promover inversiones, incrementar las exportaciones de bienes y servicios del sector, aumentar el ingreso de divisas. Ese será el marco formal del hecho político.

“Esta ley es el resultado de un trabajo de más de un año, es el resultado de escucharnos y buscar puntos de encuentro, de ver cómo conciliamos intereses, que no necesariamente son siempre los mismos”, afirmó Alberto Fernández al tomar la palabra con Cristina Fernández de Kirchner a su lado. Y resaltó: “Esta ley reivindica la cultura del encuentro y el diálogo”.

Por otro lado, el presidente llamó a “dejar de lado a los cultores del no se puede” y a “construir el país que nos merecemos, el país que le diga sí al productor agropecuario, a la ciencia y la tecnología, y no a la división de seguir parados en el mismo lugar. Los convoco a decir sí, hagamos la Argentina que queremos".

El ministro de Agricultura, Ganadería y Pesca, Julián Domínguez, sostuvo: “No hay posibilidad de crecer si no hay intereses en pugna y al Estado le toca privilegiar el interés general”. Además, resaltó la “incidencia de la biotecnología frente a los cambios climáticos”. Agregó que el proyecto busca “generar confianza para propiciar inversiones a largo plazo”.

**La Ley**

La iniciativa, que el Presidente enviará al Congreso y se generó en el marco del Consejo Agroindustrial Argentino (CAA) –tiene más de 60 cámaras agrupadas–, es para las nuevas inversiones “en todo tipo de producción agropecuaria y agroindustrial” y pone foco en las pymes.

De acuerdo al texto, se prevén distintos beneficios impositivos. Al respecto, se fija una “amortización acelerada en 3 años, en lugar de 10 años o más de acuerdo al tipo de inversión en cada sector del agro”. Por otra parte, se plantea el beneficio para inversiones en construcciones -excluida viviendas- e infraestructura, “como mínimo en la cantidad de cuotas anuales, iguales y consecutivas que surja de considerar su vida útil, reducida al cincuenta por ciento (50%) de la estimada”, según detalla un resumen del proyecto.