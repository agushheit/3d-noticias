---
category: Agenda Ciudadana
date: 2021-05-23T06:15:32-03:00
thumbnail: https://assets.3dnoticias.com.ar/dnu.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Rige el DNU que establece los parámetros para los próximos 9 días
title: Rige el DNU que establece los parámetros para los próximos 9 días
entradilla: El Decreto de Necesidad y Urgencia emitido por el Gobierno Nacional establece
  un aislamiento estricto desde las cero horas de este sábado 22 hasta el próximo
  30 de mayo.

---
El Gobierno difundió el Decreto de Necesidad y Urgencia que establece un aislamiento estricto desde la medianoche de este sábado  y hasta el 30 de mayo en las zonas del país con mayor riesgo epidemiológico, una medida con la que apunta a "evitar consecuencias irreversibles para la salud pública" por el avance de la pandemia de coronavirus.

A las restricciones que estaban vigentes se suman desde esta medianoche en los grandes conglomerados urbanos la suspensión de actividades económicas, industriales, comerciales, de servicios, culturales, deportivas, religiosas, educativas, turísticas, recreativas y sociales hasta el 30 de mayo, medida que estará vigente también durante el fin de semana del 5 y 6 junio.

En esas zonas de alto riesgo las personas solo podrán desplazarse para aprovisionarse de artículos de limpieza, medicamentos y alimentos y artículos de necesidad en comercios esenciales y retiro de compras autorizadas, siempre en cercanía a sus domicilios.

El decreto, que incluye una serie excepciones, señala en sus fundamentos que el nivel actual de circulación viral "produce un altísimo riesgo de saturación del sistema de salud, evidenciado por la ocupación de camas y el número de personas internadas en UTI y genera demanda crítica de insumos necesarios para la atención de los pacientes".

"Omitir la adopción de medidas oportunas y razonables, focalizadas y transitorias, fundadas en evidencia científica y en la experiencia internacional para evitar estas consecuencias significaría asumir el riesgo de que se produzcan consecuencias irreversibles para la salud pública", añade.

También aclara que la gestión de la pandemia "no se puede fragmentar, porque lo que sucede en cada Provincia o en la ciudad de Buenos Aires impacta tarde o temprano en las otras zonas" del país.

El nuevo Decreto de Necesidad y Urgencia (DNU) mantiene en forma general hasta el 11 de junio próximo en todo el país la anulación de viajes grupales de egresados o turísticos, las reuniones sociales de más de 10 personas, la suspensión de asistencia al trabajo para personas de riesgo y otras disposiciones que estaban vigentes.

El DNU 334/2021, difundido a las 22, indica que para las zonas de "bajo riesgo" se aplican las reglas de conducta generales, los protocolos aprobados para cada actividad y el aforo del 50 por ciento en superficies cerradas, y rigen las suspensiones establecidas para todo el país, informó oficialmente el Gobierno nacional.

En las zonas de "mediano riesgo" será facultad y responsabilidad de los gobernadores adoptar "en forma temprana" medidas que disminuyan la circulación para prevenir los contagios.

En las regiones donde la circulación del virus alcanza altos niveles, podrán realizarse salidas de esparcimiento en espacios públicos al aire libre de cercanía, en horario autorizado para circular, y dando cumplimiento a las reglas de conducta generales y obligatorias, sin necesidad de tramitar el permiso para circular para estas actividades.

No obstante, no se podrán realizar reuniones de personas, ni concentraciones, ni prácticas recreativas grupales, y tampoco se podrá circular afuera del límite del partido, departamento o jurisdicción del domicilio de residencia.

La gastronomía solo podrá trabajar mediante delivery y take away, refiere el DNU, mientras que el transporte público quedará habilitado para las personas autorizadas para el desempeño de sus actividades, los casos especiales, atención de salud y vacunación, quienes deberán portar el "Certificado Único Habilitante para Circulación - Emergencia Covid-19".

**Excepciones a la circulación**

Podrá circular sin restricciones el personal de Salud, Fuerzas de Seguridad, Fuerzas Armadas, actividad migratoria, Servicio Meteorológico Nacional, bomberos y control de tráfico aéreo.

También podrán hacerlo las autoridades superiores de los gobiernos nacional, provinciales, municipales y de la Ciudad, trabajadores del sector público en todos los niveles convocados por las respectivas autoridades.

Incluye además a miembros del Poder Legislativo y las dotaciones, integrantes de la Corte Suprema de Justicia de la Nación y personal del Poder Judicial de la Nación que dispongan las autoridades correspondientes.

Además, podrá circular el personal diplomático y consular extranjero acreditado ante el Gobierno argentino, de la Cruz Roja y Cascos Blancos.

También podrán hacerlo personas que deban asistir a otras con discapacidad, a familiares que necesiten asistencia, a personas mayores, a niños, a niñas o a adolescentes; que deban atender una situación de fuerza mayor; afectadas a la realización de servicios funerarios, entierros y cremaciones; afectadas a la atención de comedores escolares, comunitarios y merenderos.

En la misma línea incluye al personal que se desempeña en los servicios de comunicación audiovisuales, radiales y gráficos; afectado a la obra pública y a tareas de seguridad en demoliciones; a supermercados mayoristas y minoristas y comercios minoristas de proximidad, de alimentos, higiene personal y limpieza, farmacias, ferreterías, veterinarias y provisión de garrafas.

También estarán exceptuados de las restricciones de seguridad el personal vinculado a las actividades vinculadas a la cadena de valor e insumos de la industria de la alimentación; de higiene personal y limpieza; de equipamiento médico, medicamentos, vacunas y otros insumos sanitarios; como así también a los de las actividades vinculadas con la producción, distribución y comercialización agropecuaria y pesca.

Otros rubros habilitados para la circulación son los de telecomunicaciones, internet fija y móvil; servicios digitales y las actividades de mantenimiento de servidores; comercio exterior; recolección, transporte y tratamiento de residuos sólidos urbanos, peligrosos y patogénicos; mantenimiento de los servicios básicos (agua, electricidad, gas, comunicaciones) y atención de emergencias; transporte público de pasajeros, de mercaderías, petróleo, combustibles y Gas licuado del petróleo.

Incluye además el reparto a domicilio de alimentos, medicamentos, productos de higiene, de limpieza y otros insumos de necesidad; servicios de lavandería; servicios postales y de distribución de paquetería; vigilancia, limpieza y guardia.

Están contempladas las guardias mínimas que aseguren la operación y mantenimiento de yacimientos de petróleo y gas, plantas de tratamiento y/o refinación de petróleo y gas, transporte y distribución de energía eléctrica, combustibles líquidos, petróleo y gas, estaciones expendedoras de combustibles y generadores de energía eléctrica.

Otros que estarán exceptuados serán los miembros de la Sociedad del Estado Casa de Moneda, servicios de cajeros automáticos, transporte de caudales y todas aquellas actividades que el Banco central autorice.

La actividad bancaria con atención al público será exclusivamente con sistema de turnos, mientras que las autoridades de la Comisión Nacional de Valores podrán autorizar la actividad de una dotación mínima de personal y la de sus regulados en caso de resultar necesario.

Están habilitadas las operaciones de centrales nucleares, los hoteles afectados al servicio de emergencia sanitaria, los aeropuertos, los garajes y estacionamientos con dotaciones mínimas; además de la inscripción, identificación y documentación de personas; las prestaciones profesionales a domicilio destinadas a personas con discapacidad; y los establecimientos para la atención de personas víctimas de violencia de género.

Se mantienen las guardias médicas y odontológicas, y la atención médica y odontológica programada será con sistema de turno previo, como así también los laboratorios de análisis clínicos y centros de diagnóstico por imagen.

Está permitido el traslado de niños, niñas y adolescentes para vinculación familiar; y podrá circular el personal de la Administración Nacional de seguridad Social (Anses).

Quedan exceptuadas de las restricciones de circulación, sin autorización al uso del transporte público de pasajeros, las personas que trabajen en las industrias y que se realicen bajo procesos continuos cuya interrupción implique daños estructurales en las líneas de producción y/o maquinarias.

También aquellos que retiran alimentos en locales gastronómicos de cercanía; los de producción y distribución de biocombustibles; los de exploración, prospección, producción, transformación y comercialización de combustible nuclear; los de los servicios esenciales de sanitización, mantenimiento, fumigaciones y manejo integrado de plagas; los de actividades vinculadas con la producción, distribución y comercialización forestal y minera. Actividades vinculadas a la protección ambiental minera.

Estarán habilitados los talleres para mantenimiento y reparación de automotores y motocicletas para transporte público, vehículos de las fuerzas de seguridad y de las Fuerzas Armadas, los vehículos afectados a las prestaciones de salud y al personal con autorización para circular, conforme la normativa vigente.

También están incluidos los talleres para mantenimiento y reparación de bicicletas; y las ventas de repuestos, partes y piezas para automotores, motocicletas y bicicletas, bajo la modalidad de entrega puerta a puerta.

Podrán trabajar los establecimientos que desarrollen actividades de cobranza de servicios e impuestos, las personas que deban trasladarse para realizar viajes al exterior; las industrias que realicen producción para la exportación.

Y también estará habilitada la venta de mercadería ya elaborada de comercios minoristas a través de plataformas de comercio electrónico, venta telefónica y otros mecanismos que no requieran contacto personal con clientes y únicamente mediante la modalidad de entrega a domicilio o retiro, que en ningún caso podrán abrir sus puertas al público.