---
category: La Ciudad
date: 2021-08-09T06:15:34-03:00
thumbnail: https://assets.3dnoticias.com.ar/boleta.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Así será la boleta única para las Paso de concejales en Santa Fe
title: Así será la boleta única para las Paso de concejales en Santa Fe
entradilla: El Tribunal Electoral definió que las 42 listas que competirán en las
  elecciones primarias se repartan en partes iguales en las dos columnas.

---
El Tribunal Electoral definió cómo se conformará la boleta única para las próximas elecciones y la exhibió ante los apoderados de los partidos y frentes que competirán el próximo 12 de septiembre en las Paso. El criterio que se adoptó fue el de repartir 21 listas por columna para contener a las 42 que son opción para ordenar los candidatos para las elecciones generales del 14 de noviembre.

En un momento se evaluaba dejar 22 filas en la primera columna y 20 en la segunda. Los frentes y partidos que podían quedar al final de la primera columna tenían sólo una lista y eso posibilitaba una mayor flexibilidad para estar en el final de la primera columna o al principio de la segunda.

Algunos apoderados consultados por la prensa recordaron que las únicas veces que la primera columna quedaba más corta que la segunda era cuando tomar esa determinación evitaba cortar la continuidad de una interna. Este año, con 21 listas por columna, la boleta quedó pareja e incluso la segunda hilera de candidatos es levemente más larga. Eso se debe a que en la primera hay siete fuerzas políticas dirimiendo sus internas –entre cada una de ellas hay una leve separación– y en la segunda son 10.

Algunas fuerzas políticas creen que se debería hacer un retoque en la ley o resolverlo mediante una reglamentación del Ejecutivo para establecer un criterio de cómo se debería resolver el reparto de listas en la boleta única.