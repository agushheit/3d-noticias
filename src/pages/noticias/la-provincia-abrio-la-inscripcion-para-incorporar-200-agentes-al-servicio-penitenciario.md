---
category: Estado Real
date: 2021-08-31T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/peni.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia abrió la inscripción para incorporar 200 agentes al servicio
  penitenciario
title: La provincia abrió la inscripción para incorporar 200 agentes al servicio penitenciario
entradilla: Los ingresantes desempeñarán funciones en diferentes penitenciarias de
  Rosario.

---
La Secretaría de Asuntos Penales y Penitenciarios de la provincia informó que se encuentra abierta la inscripción para el ingreso de 200 aspirantes para trabajar en la órbita del Servicio Penitenciario provincial y en las unidades penitenciarias del sur de la provincia, como personal Subalterno, del escalafón Cuerpo General, a fin de desempeñar eventualmente funciones como Celadores y/o Auxiliares de Celadores en las Unidades Penitenciarias Masculinas.

Al respecto, el secretario de Asuntos Penitenciarios, Walter Gálvez, explicó que la medida “obedece a la necesidad de aumentar la capacidad de gestión del Servicio Penitenciario y a las nuevas construcciones carcelarias que dispuso el gobierno, con lo cual se insta a aquellos ciudadanos que quieran ingresar a trabajar en el Servicio Penitenciario que se registren”.

Los puestos a cubrir son en el Gran Rosario: el Instituto Detención de Rosario (U.3), Complejo Penitenciario de Rosario (U.5), Instituto Penitenciario de Rosario (U.6), Complejo Penitenciario de Piñero (U.11) y Unidad Penal N° 16 de Rosario. “Son 200 nuevos ingresos y los aspirantes deberán inscribirse en la página oficial del gobierno de la provincia”, agregó el funcionario.

El personal seleccionado será incorporado al curso de capacitación que, con una duración de 180 días, se dictará en establecimientos seleccionados en zona sur, coordinado y dirigido por personal de la Escuela Penitenciaria de la Provincia, a cuyo término quienes lo aprueben egresarán con el grado de Subayudante – Cuerpo general – Personal Subalterno.

Entre las características del puesto solicitado se indican la disposición a trabajar con personas privadas de la libertad; compromiso con el sentido del orden, disciplina y responsabilidad; disposición a actuar conforme a los derechos y obligaciones reglamentados en la institución; disposición al trabajo en equipo respondiendo a gestiones de mando; adaptabilidad y flexibilidad frente a los cambios; y disponibilidad para cumplir tareas laborales de acuerdo a las necesidades del servicio, en tiempo y lugar que se asigne.

**Condiciones de ingreso**

Los interesados deberán cumplir con los siguientes requisitos:

> Ser argentino

> Acreditar antecedentes honorables y de buena conducta

> No tener actuación contraria a los principios de la libertad y la democracia, de acuerdo con el régimen establecido por la Constitución Nacional

> No haber sido separado de la Administración Pública por Exoneración

> Tener entre 21 y 28 años de edad al momento de la inscripción

> Haber aprobado el ciclo de estudios secundarios o polimodal

> Poseer las aptitudes psíquicas y físicas que establece la reglamentación

> Aprobar las pruebas de capacidad y competencia que se determinen

> Tener domicilio real, menor a 60 kilómetros de distancia de las unidades en las cuales se cumplirán funciones –Instituto Detención de Rosario (U.3), Complejo Penitenciario de Rosario (U.5), Instituto Penitenciario de Rosario (U.6), Complejo Penitenciario de Piñero (U.11) y Unidad Penal N° 16 de Rosario-, con una antigüedad superior a los dos años.

**Preinscripción**

Los interesados deberán preinscribirse mediante la página web www.santafe.gob.ar/ms/spsf. Tendrán tiempo de hacerlo hasta el 10 de septiembre de 2021, plazo que se prorrogara de forma automática sin necesidad de autorización alguna, hasta tanto se cubran las vacantes asignadas.

**Documentación**

Los aspirantes deberán presentar la siguiente documentación:

> Certificado de Estudios Secundarios aprobados (copia certificada por ante autoridad judicial o escribano público)

> Certificado de Vecindad extendido por autoridad policial

> Fotocopia D.N.I. (certificadas por ante escribano público o autoridad judicial)

> Fotografía color 3x3 frente, fondo blanco, sin cubre cabeza

> Planilla de Antecedentes policiales y/o Judiciales, entregada por la Unidad Regional

> Constancia Nº C.U.I.L (Clave Única de Identificación Laboral)

> Partida de Nacimiento debidamente certificada

> Certificado de Antecedentes Penales otorgado por el Registro Nacional de Reincidencia

> Constancia de preinscripción online.

Dicha documentación deberá entregarse a partir del 13 de septiembre y hasta el 7 de octubre de 2021, en días hábiles, de 8:00 a 13:00 horas, en el

Instituto de Detención de Rosario (U.3), ubicado en Estanislao Zeballos 2951 (Rosario); en el Complejo Penitenciario de Rosario (U.5) (27 de Febrero 7899); o en el Complejo Penitenciario de Piñero (U.11) - Ruta 14 km 3 ½ (Piñero).