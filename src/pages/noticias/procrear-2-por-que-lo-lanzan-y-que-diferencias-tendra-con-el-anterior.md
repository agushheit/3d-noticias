---
category: Agenda Ciudadana
date: 2020-12-27T12:15:08Z
thumbnail: https://assets.3dnoticias.com.ar/2712procrear.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: Diario UNO'
resumen: 'PROCREAR 2: por qué lo lanzan y qué diferencias tendrá con el anterior'
title: 'PROCREAR 2: por qué lo lanzan y qué diferencias tendrá con el anterior'
entradilla: El ministro Jorge Ferraresi afirmó que el Procrear 2 tendrá algunos cambios
  con respecto a la versión anterior del programa para construir viviendas.

---
Uno de los mayores déficit de la Argentina es el habitacional. Millones de personas no cuentan con su vivienda o viven en condiciones inhumanas, por la pobreza que se ha profundizado en este año y por lo inalcanzable de los precios de las casas de acuerdo a los sueldos que mayormente cobra la población asalariada. El gobierno parece haberse percatado de la necesidad y por eso realizó el anuncio, sin mucha claridad aun, de un Procrear 2.

Quien lo hizo fue el ministro de Desarrollo Territorial y Hábitat, Jorge Ferraresi. Anticipó que el Gobierno definirá próximamente el lanzamiento del plan Procrear 2. En ese sentido, el ex intendente de Avellaneda, que asumió su actual cargo hace poco más de un mes en reemplazo de la santafesina María Eugenia Bielsa, afirmó que introducirán algunos cambios con respecto a la versión anterior del programa.

***

![](https://assets.3dnoticias.com.ar/2712procrear1.webp)

***

Jorge Ferraresi planteó en primer término su idea con el Procrear 2 es modificar «algunos paradigmas filosóficos» con respecto a todos los programas de acceso a la vivienda en general. «La Argentina ha construido, históricamente,  viviendas de primera y de segunda. Las viviendas sociales son lugares donde en un dormitorio no entra un placar, donde no hay posibilidad de crecimiento y donde, nosotros que pensamos en una Argentina que se recomponga y pensamos que el día de mañana el habitante podría tener un auto, no tiene donde guardarlo. Entonces queremos cambiar y hacer una sola calidad de vivienda», expresó en declaraciones a C5N.

En esa misma línea, el ministro consideró: «Se estigmatiza (diciendo) que las viviendas se regalan. A partir de este tiempo todas las viviendas se pagan, en un sistema de recuperación solidaria  donde, con una articulación entre los bancos y la ANSES, vamos a generar un código de descuento directo del salario de los que tienen trabajo y de los planes sociales de los que tengan planes sociales».

Jorge Ferraresi se refirió específicamente al próximo Procrear 2 y reforzó ambos conceptos. «El Procrear tiene una necesidad fundamental en el tema del recupero financiero. Tenemos que hacer un replanteo porque, en el inicio, el Procrear se realizaba con aportes de la ANSES que, a partir de su economía, no aportó más, entonces, eran aportes únicamente del tesoro nacional. Debemos generar un mercado de capitales que aporte a ese programa para iniciar un proceso que tenga que ver con el recupero de la inversión», explicó e insistió en que la nueva política será «no definir dos niveles de viviendas, sino un solo tipo».

Por último, el funcionario adelantó otra diferencia sustancial con respecto al primer Procrear: «Que no haya que tener condiciones económicas para acceder a un crédito, sino las condiciones para poder devolver en función de un programa solidario».

<br/>

<span style="font-family: helvetica; font-size: 1.3em; font-weight: 800;"> **Más viviendas**</span>  
   
En otro anticipo sobre su gestión al frente del Ministerio de Desarrollo Territorial y Hábitat, Jorge Ferraresi había adelantado este lunes el lanzamiento del plan «Casa Propia-Construir Futuro», que prevé la construcción de 264.000 unidades habitacionales en los próximos tres años, con una inversión cercana a los 900.000 millones de pesos, en el marco de una política de desarrollo territorial, urbano y habitacional de alcance federal.

El anuncio se dio en el contexto del primer acercamiento entre el presidente Alberto Fernández y los mandatarios provinciales, luego de la sanción de la ley de Capitales Alternas. La normativa fue promulgada el 4 de diciembre y oficializó el listado de 24 ciudades argentinas elegidas para realizar reuniones del Gabinete nacional, en encuentros políticos y de gestión con participación de gobernadores y los ministros dispuestos por ellos. 

La primera cita fue el lunes pasado en Río Grande, en la provincia de Tierra del Fuego. Allí, Ferraresi suscribió, a través de videoconferencias con los gobernadores de Chaco, La Pampa y Buenos Aires, acuerdos para la incorporación de esas provincias a los distintos planes de viviendas del Gobierno nacional.

«El presidente Alberto Fernández nos encomendó el trabajo de generar un plan de viviendas que sea federal y que también contenga el concepto del derecho», dijo el Ministro, quien señaló: «a partir de este tiempo vamos a empezar a erradicar del vocabulario el concepto de vivienda social. Las viviendas tienen que ser dignas,  generar la posibilidad de arraigarse, de tener una sustentabilidad en función del lugar donde uno trabaja, donde articula con el transporte», agregó Jorge Ferraresi.