---
category: La Ciudad
date: 2021-08-04T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/BA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: '"BA más Cerca" llegó a la ciudad Santa Fe'
title: '"BA más Cerca" llegó a la ciudad Santa Fe'
entradilla: A través de un convenio con la Municipalidad de Santa Fe, el trailer móvil
  “BA más Cerca”, a cargo del Ente de Turismo de la ciudad de Buenos Aires, visita
  la ciudad desde este martes 3 de agosto.

---
El vehículo recorre diversas regiones del país con una propuesta de promoción turística, contenido cultural y actividades lúdicas interactivas. También habrá sorteos para ganar un viaje a Buenos Aires, ya que la iniciativa se enmarca en un intercambio entre las ciudades para promover la reactivación turística.

“Con gran entusiasmo retomamos las visitas por las diferentes plazas del país, esta vez de la mano de este trailer que nos permitirá mostrar todos los atractivos turísticos que tiene Buenos Aires y que los puedan disfrutar de forma lúdica y cultural. Además, nos permite continuar trabajando en fortalecer el turismo interno a través de los vínculos con los destinos que vamos a visitar.” expresó Diego Gutiérrez, director general de Promoción Turística de Buenos Aires.

“Trajimos el programa Buenos Aires más cerca, que va a recorrer 16 mil kilómetros de todo el país, de acá a fin de año, e invita a seguir trabajando en equipo con la Secretaría de Turismo, con la que venimos trabajando desde el mes de enero y hemos firmado una carta acuerdo para potenciarnos como destino”, explicó Gutiérrez. “Queremos contarles a todos los santafesinos -añadió el funcionario- cómo está preparada nuestra ciudad para recibirlos, cuáles son las propuestas culturales y gastronómicas, cómo hemos reconvertido los espacios al aire libre, con testeos gratuitos al llegar para transitar la ciudad con todos los protocolos...en definitiva, contarles que los extrañamos durante más de un año y medio y los necesitamos”. “Buscamos que en cada visita a la ciudad de Buenos Aires se lleven una experiencia memorable y estamos convencidos que con este programa lo vamos a lograr durante todo el año”, auguró.

Mientras, Jimena Cuello, en representación de Puerto Plaza manifestó a El Litoral la satisfacción por recibir la iniciativa: “Que podamos tener en la ciudad de Santa Fe un pedacito de Buenos Aires para que todos los que hoy se acercaron al puerto, ya sea a trabajar o a disfrutar, puedan conocerla un poquito más y tenerla en su radar para cualquier escapada, corta o larga, es gratificante y estamos contentos de ser parte de esto”, aseguró.

Por su parte, Franco Arone, Director de Turismo de Santa Fe, indicó que la actividad “es parte de una agenda importante de intercambio de experiencias y articulación, donde la ciudad de Santa Fe va a ir a la ciudad de Buenos Aires a promocionar su oferta turística como el camino internacional con la Manzana Jesuítica, la propuesta gastronómica, nuestra naturaleza, nuestra historia...invitamos a cada ciudadano del país que quiera visitar la ciudad de Santa Fe con una decisión estratégica del intendente Emilio Jatón para posicionar turísticamente a la ciudad en el país”. Además, el funcionario reveló que durante las vacaciones de invierno llegaron a la ciudad de Santa Fe muchos turistas porteños y opinó que “eso es alentador, porque habla de que esta articulación de promoción inversa empieza a dar resultados”.