---
category: Estado Real
date: 2021-02-11T06:46:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/ate.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: 'Ley Micaela: La provincia firmó un convenio con el gremio ATE para su implementación'
title: " Ley Micaela: La provincia firmó un convenio con el gremio ATE para su implementación"
entradilla: En un acto encabezado por la secretaria de Estado de Igualdad y Género,
  Celia Arena, junto al secretario general de ATE, Jorge Hoffman, se llevó adelante
  este la rúbrica del acuerdo.

---
El Gobierno de Santa Fe, a través de la Secretaría de Estado de Igualdad y Género, suscribió este miércoles, un convenio para la implementación de la Ley Micaela con las autoridades de la Asociación Trabajadores del Estado (ATE). Esta instancia, como ya se realizó con UPCN, permitirá el entrenamiento de formadores y formadoras en los conceptos de la ley para que la asociación gremial pueda continuar adelante con capacitaciones.

“Sumamos otro gremio de la envergadura de ATE para avanzar en la instrumentación de la Ley Micaela, que es una de los compromisos del gobernador Omar Perotti. Lo importante es aunar esfuerzos en este trabajo que viene haciendo el gremio en este sentido, el que viene haciendo la Secretaría de Estado de Igualdad y Género como entidad de aplicación de la Ley Micaela en la provincia”, sostuvo Celia Arena, titular de la cartera de Igualdad y Género provincial.

“Esta forma de trabajo conjunto implica el abordaje junto a los equipos del gremio, para que estén certificados como formadores y formadoras para dejar la capacidad instalada. Y lo haremos no solo en este si no que en cada gremio para avanzar en esto que tiene que ver con los derechos humanos de la mujeres y las diversidades sexuales”, agregó.

Por su parte, Jorge Hoffman, secretario general de ATE, declaró: “Este es un compromiso compartido con la Secretaría de Estado de Igualdad y Género, con el Gobierno de Santa Fe y tenemos toda nuestra predisposición para trabajar en conjunto para construir una sociedad donde se erradique la violencia, fundamentalmente la violencia de género”.

Del acto participaron además de Arena y Hoffman, Elsa Splendiani y Silvia González, integrantes de la Comisión Directiva de ATE y del espacio ATE Género y Diversidad, además de delegados y delegadas, entre otros.