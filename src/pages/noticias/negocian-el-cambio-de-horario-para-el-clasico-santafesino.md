---
category: Cultura
date: 2021-05-07T08:35:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/cultura.png
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia articula con la Nación la asistencia económica a trabajadores
  y trabajadoras del sector cultural de la Región Centro
title: La provincia articula con la Nación la asistencia económica a trabajadores
  y trabajadoras del sector cultural de la Región Centro
entradilla: Alcanzará a casi nueve mil beneficiarios. Santa Fe aportará una cifra
  similar a la que invierte el gobierno nacional.

---
El ministro de Cultura de Santa Fe, Jorge Llonch, participó de un encuentro virtual junto a la secretaria de Cultura de Entre Ríos, Francisca D’Agostino; la presidenta de la Agencia Córdoba Cultura, Nora Bedano, y el titular de Cultura de la Nación, Tristán Bauer, que tuvo por objetivo compartir el informe del programa de asistencia económica Cultura Solidaria en la Región Centro, que alcanzará a casi nueve mil artistas y trabajadoras y trabajadores de la cultura.

En ese sentido, Llonch indicó que –con características diferentes– “la provincia de Santa Fe, a través de los colectivos culturales e instituciones que representan a artistas y trabajadores de la cultura, aportará una cifra similar a la que invierte la Nación, destinada a quienes se vieron obligadas/os a dejar de trabajar por el cierre de los espacios a causa de la pandemia”.

Asimismo, el ministro santafesino manifestó que “en 2020 fue posible organizar y ejecutar los programas que arribaron desde la Nación gracias al trabajo desarrollado por el Ministerio a lo largo y ancho del territorio a través de los representantes de cultura de los 365 municipios y comunas, lo cual permitió que en 2021 se cuadruplicara la cifra de asistencia económica a las y los artistas”. El año pasado esa asistencia alcanzó los 19 millones de pesos y en este período llegará a 85,5 millones.

Llonch, en su rol de vicepresidente primero del Consejo Federal de Cultura, sugirió a Bauer la coordinación de acciones conjuntas entre la Nación y las provincias para organizar que las y los artistas realicen sus contraprestaciones a esa ayuda en centros culturales e instituciones.

Para el funcionario santafesino, “este trabajo federal generó que muchos más artistas, trabajadoras y trabajadores de la cultura se inscriban, dando continuidad y profundizando las líneas de asistencia implementadas en 2020”.

“Esta nueva etapa de asistencia económica se concretará a través de las instituciones (vecinales, clubes, etc) para poder llegar al universo de todos los artistas santafesinos con una mayor inversión”, explicó Llonch.

“En consonancia con el espíritu de la política, se busca además que las y los beneficiarios lleven a cabo acciones solidarias a modo de contraprestación, en su propia comunidad, a través de charlas, asesorías técnicas, talleres y presentaciones o espectáculos artísticos”, finalizó el ministro.

También participaron la secretaria de Desarrollo Cultural del Ministerio de Cultura de la Nación, Lucrecia Cardoso; la directora nacional de Integración Federal y Cooperación Internacional, Ariela Peretti, y el director de Coordinación del Consejo Federal de Cultura, Gustavo Romero, entre otras y otros.