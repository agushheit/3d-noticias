---
category: Estado Real
date: 2021-04-14T07:41:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/capitani.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Capitani participó de una reunión sobre el impacto de la Tarjeta AlimentAR,
  a 15 meses de su implementación
title: Capitani participó de una reunión sobre el impacto de la Tarjeta AlimentAR,
  a 15 meses de su implementación
entradilla: El encuentro fue encabezado por el ministro Arroyo y se transmitió en
  vivo por el canal de YouTube de la cartera social de la Nación.

---
El ministro de Desarrollo Social, Danilo Capitani, participó del encuentro “Tarjeta Alimentar: estudios sobre el alcance e impacto a 15 meses de su implementación”, a través de una reunión virtual, que se transmitió en vivo este martes por el canal de YouTube del Ministerio de Desarrollo Social de la Nación.

El encuentro fue encabezado por el ministro nacional Daniel Arroyo, y participaron ministros y ministras de todo el país; con el objetivo de reflexionar, discutir y socializar los resultados obtenidos por diferentes estudios que analizaron el impacto de la implementación de la Tarjeta AlimentAR sobre hogares y niños y niñas destinatarios en el marco de la pandemia; y los desafíos para el 2021 en el marco de las políticas sociales y especialmente aquellas destinadas a garantizar la seguridad alimentaria.

Al respecto, Capitani destacó la importancia de estos espacios de diálogo y agrego que “para planificar e instrumentar políticas sociales es imprescindible contar con información confiable, continua y pertinente, que permita realizar un diagnóstico, monitoreo y evaluación de la situación social, mas aun en contextos tan dinámicos y complejos como en los que nos toca gestionar".

**TARJETA ALIMENTAR**

Es una de las políticas sociales que ha implementado el Ministerio de Desarrollo Social de la Nación para enfrentar la situación de inseguridad alimentaria que atraviesa un alto porcentaje de familias argentinas. Esta herramienta de inclusión ha sido fundamental para atravesar la circunstancia extraordinaria de la pandemia de COVID-19.

Desde su instrumentación, se ha avanzado en generar información social relevante alrededor de diferentes aspectos de esta política alimentaria que permitan repensar, orientar y mejorar sus resultados en el nuevo escenario post-pandemia.

La tarjeta, definida por el ministro Daniel Arroyo como “una política nutricional para combatir el hambre", surge como respuesta inmediata a un brutal proceso de deterioro económico y social en el país que se produjo en los últimos años y que puso en riesgo la situación nutricional y el acceso a una alimentación sostenible para un porcentaje relevante de familias argentinas.

**PRESENTES**

Del panel participaron la secretaria de Inclusión Social, Laura Alonso; la investigadora docente de la Universidad Católica Argentina y la coordinadora del proyecto IR PISAC-COVID19- Universidad Nacional de La Matanza, Ianina Tuñón; el director del Instituto de Investigación, Social, Económica y Política Ciudadana (ISEPCI), Isaac Rudnik; y el especialista en Inclusión Social y Monitoreo de UNICEF, Sebastián Waisgrais.