---
category: Agenda Ciudadana
date: 2021-02-14T07:30:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/VACUNADOCENTES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Cómo será el orden de vacunación de los docentes y personal de educación
title: Cómo será el orden de vacunación de los docentes y personal de educación
entradilla: El Consejo Federal de Educación separó a los docentes en cinco grupos
  -según grados y funciones- y estableció el orden en que recibirán las vacunas.

---
El Consejo Federal de Educación, conformado por los 24 ministros del país, estableció este viernes el orden de vacunación contra el coronavirus para los docentes, para lo cual se los dividió en cinco grupos, informó el Ministerio de Educación.

En el primer grupo estará el personal de dirección y gestión, el de supervisión e inspección, los docentes frente a alumnos y alumnas de nivel Inicial (incluye ciclo maternal), los de primer ciclo del nivel primario, (1°, 2° y 3° grado) y los de educación especial.

En el segundo grupo figura el personal de apoyo a la enseñanza, todo otro personal sin designación docente pero que trabaja en establecimientos educativos de la educación obligatoria en distintas áreas y servicios (maestranza, administración, servicios técnicos, servicios generales, y equivalentes).

En el tercero estarán los docentes frente a alumnos y alumnas de nivel primario, del segundo ciclo de 4° a 6°/7° grados.

El cuarto grupo estará conformado por los docentes frente a alumnos y alumnas de nivel secundario, de educación permanente para jóvenes y adultos en todos sus niveles e instructores de formación profesional.

Finalmente, en el último grupo estarán los docentes y no docentes de institutos de educación superior y universidades.

Además, se aprobó la creación del Observatorio del Regreso Presencial a las Aulas que funcionará con la participación de representantes del Consejo y los sindicatos nacionales y el Ministerio de Salud de la Nación.

También el Consejo Nacional de Calidad de la Educación, la Defensoría Nacional de Niños, Niñas y Adolescentes, entre otros organismos.

Durante la asamblea, se establecieron además las pautas vinculadas a la asistencia alternada por turnos, días o semanas; la planificación de contenidos y reorganización de la currícula.

También la priorización en actividades presenciales de materias que no pueden ser desarrollados plenamente sin presencialidad, la generación de espacios de socialización y el respeto de horarios para las y los docentes.