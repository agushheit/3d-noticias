---
category: La Ciudad
date: 2021-10-26T06:00:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/BONDI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Cómo se planea el servicio de colectivos en la ciudad de Santa Fe
title: Cómo se planea el servicio de colectivos en la ciudad de Santa Fe
entradilla: "\"Estamos en un camino de reconstrucción del sistema y falta mucho\",
  dijo a El Litoral el subsecretario de Movilidad y Transporte de la Municipalidad
  de Santa Fe, Lucas Crivelli.\n\n"

---
Desde este lunes, los colectivos urbanos de la ciudad de Santa Fe pueden transportar pasajeros sin restricciones de cantidad. Es decir que se dejan de lado las medidas sanitarias de distanciamiento social dispuestas en la pandemia de coronavirus. Aunque solicitan que se mantenga el resto de las medidas sanitarias, como la ventilación cruzada y el uso de tapabocas. En tal sentido, ahora los coches de las líneas locales podrán transportar hasta 20 pasajeros parados, además de los sentados.

La medida fue dispuesta por el Gobierno Nacional y la Municipalidad de Santa Fe la acató. A través de la Resolución 389/2021 publicada en el Boletín Oficial, se indicó además que los colectivos urbanos deberán "garantizar la máxima frecuencia de sus servicios" y "contar con una correcta ventilación de los vehículos".

En este sentido, se precisó que las unidades de transporte automotor sin aire acondicionado "deberán circular con la totalidad de los ventiletes y/o ventanillas abiertos durante todo el viaje a fin de mantener la máxima circulación de aire", tal como venía sucediendo, al igual que las unidades que cuentan con aire acondicionado. Estas últimas, en épocas de altas temperaturas, podrán encender sus equipos de climatización pero no cerrar las ventanillas. Idéntica normativa le corresponde a las formaciones y coches ferroviarios.

Son habituales las quejas de los usuarios por las demoras en la frecuencia y la falta de coches de las distintas líneas de colectivos para viajar por la ciudad. También se pudo comprobar que en horarios pico muchos coches viajaban cargados de pasajeros por sobre lo que la medida sanitaria había dispuesto. Ahora, con la posibilidad de transportar mayor cantidad de pasajeros, esta realidad quedará "dentro de la norma".

**El futuro del servicio**

"Estamos viendo un repunte en la cantidad de pasajeros", dijo -en diálogo con El Litoral- el subsecretario de Movilidad y Transporte de la Municipalidad de Santa Fe, Lucas Crivelli, en relación a la demanda de la ciudadanía en el actual contexto de pandemia.

Y agregó luego que "estamos todavía muy lejos de la calidad de servicio que la Municipalidad pretende para hablar de aumento de tarifas. Estamos en un camino de reconstrucción del sistema y falta mucho".

\-Ya sin las restricciones, ¿qué planifica la Municipalidad para los próximos dos años de gestión que les quedan para hacer sostenible el servicio de colectivos urbanos? -consultó El Litoral a Crivelli.

\-Para responder esa pregunta primero necesitamos tener en claro cuál será el esquema de financiamiento, es decir, cómo serán los subsidios nacionales a mediano y largo plazo. Luego hay que revisar la prestación actual de cada línea, aunque esto está atado a lo primero.

\-¿Hubo una recuperación del nivel de pasajeros?

\-Si bien hubo una recuperación, sigue estando en un 75 por ciento respecto de lo que era la situación pre pandemia

\-Sin las restricciones de cantidad de pasajeros, ¿se alcanzarán los niveles pre pandemia o hay un cambio sociocultural que impactará en el servicio?

\-Hay un cambio que va a impactar. No creo que se alcancen los valores de pre pandemia, tras casi dos años de un servicio restringido. En el mejor de los casos, las expectativas son que se alcance el 80 por ciento de los pasajeros que había en 2019, por ejemplo, que de por sí fue un año con baja cantidad de pasajeros. De 2017 al '19 se perdieron casi 4 millones de pasajes -apuntó Crivelli.

Más adelante, el funcionario dijo que "de acá a dos años, con una reestructuración del sistema y con más coches en calle, a lo mejor se pueda volver a aumentar la cantidad de pasajeros", finalizó.