---
category: Agenda Ciudadana
date: 2022-01-30T09:53:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/070121-costanera.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Servicio Meteorologico Nacional
resumen: 2021 fue uno de los siete años más cálidos de la historia a nivel mundial
title: 2021 fue uno de los siete años más cálidos de la historia a nivel mundial
entradilla: Lo informó la OMM. Además, te contamos qué sucedió en Argentina, que registró
  su quinto año más cálido desde 1961.

---
De acuerdo a la Organización Meteorológica Mundial, en el 2021 la temperatura promedio global fue, aproximadamente, 1,11 °C superior a los niveles preindustriales (1850-1900). Así, este es uno de los siete años consecutivos (2015-2021) más cálidos de la historia, con más de 1°C respecto al periodo de referencia. 

Los siete años más cálidos registrados se dieron desde 2015, con 2016, 2019 y 2020 liderando la tabla. Además, el organismo remarcó que todo apunta hacia un calentamiento global sostenido, con concentraciones nunca observadas de gases de efecto invernadero (GEI), entre otros indicadores de cambio climático, como el aumento del nivel medio del mar y la extensión del hielo marino. Desde los años ochenta, cada decenio - periodo de diez años - fue más cálido que el anterior, y se prevé que esta tendencia continúe. 

**Efecto La Niña**

Los eventos de La Niña entre 2020 y 2022 supusieron una disminución transitoria de las temperaturas promedio globales, pero aún así, el último año fue uno de los más cálidos registrados. 

Por su parte, el secretario general de la OMM, Petteri Taalas, comentó:

**“2021 fue más cálido que años anteriores en los que los efectos de La Niña se sintieron. El calentamiento global a largo plazo, fruto del incremento de las concentraciones de gases de efecto invernadero, es ahora mucho mayor que la variabilidad interanual de las temperaturas medias mundiales causada por los condicionantes climáticos de origen natural”.**

La Niña es un fenómeno que produce un enfriamiento a gran escala de la temperatura de la superficie del océano en las partes central y oriental del Pacífico ecuatorial, además de otros cambios en la circulación atmosférica alrededor del mundo. Sus efectos en el tiempo y el clima suelen ser opuestos a los del fenómeno El Niño.

Además, Taalas también remarcó: 

**"El año 2021 será recordado por la temperatura récord de casi 50 °C registrada en Canadá, comparable a los valores que se observan en el desierto caluroso sahariano de Argelia, por la excepcionalidad de las precipitaciones y por las inundaciones mortales que azotaron Asia y Europa, así como por la sequía que castigó partes de África y América del Sur. Los impactos del cambio climático y los peligros debidos al clima tuvieron efectos devastadores que alteraron la vida de las comunidades en todos los continentes".**

En abril 2022 se publicará la versión final de este informe, tras el reporte preliminar en octubre 2021 que se dio a conocer en el marco de la 26° edición de la COP26.

**2021 fue el quinto año más cálido en Argentina**

Tras el análisis de las temperaturas registradas a nivel nacional en los últimos 12 meses, la conclusión fue determinante: 2021 quedó posicionado en el quinto lugar entre los años más cálidos desde 1961, con una anomalía de +0,58°C.

En particular, **la región que más sintió el incremento de las temperaturas fue Patagonia**, que finalizó con una anomalía de temperatura promedio de +1.1°C. Con este valor **se quebró por varias décimas de grado el récord histórico de calor**, que era de +0.77°C (1998).

La República Argentina marca una tendencia a registrar años cálidos. Los 7 años con mayor temperatura en el territorio se registraron después de 2010.

 

Por su parte, la Directora del SMN Celeste Saulo detalló:

"El aumento de la temperatura a nivel global también se manifiesta en nuestro país, en 2021 específicamente lo hizo en la Patagonia, y es una expresión evidente del Cambio Climático. Es urgente tomar medidas para responder a este escenario, y los servicios meteorológicos tienen un rol central, sobre todo a través de los sistemas de alerta temprana. Pero también con el monitoreo, como el caso de Argentina, que realiza observaciones desde hace 150 años. Esto es lo que nos permite dar cuenta de los cambios en el clima, y le permite al país diseñar estrategias de mitigación y respuesta"

El fenómeno de La Niña también tuvo impactos en nuestro país en el último año, tanto en las temperaturas como en las precipitaciones. De las anomalías de temperatura extremadamente cálidas a las lluvias escasas, como fue el caso de Patagonia, que atravesó, además del año más cálido, también el más seco desde 1961.