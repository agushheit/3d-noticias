---
category: Agenda Ciudadana
date: 2021-06-15T08:01:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/sukerman.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: 'Vacunación VIP: las explicaciones de Sukerman no conformaron a la oposición'
title: 'Vacunación VIP: las explicaciones de Sukerman no conformaron a la oposición'
entradilla: Desde el socialismo aseguran que hubo muchas preguntas que quedaron sin
  respuesta durante la interpelación al ministro de Gobierno de la provincia. El video
  con la audiencia completa.

---
Por iniciativa del bloque socialista, el ministro de Gobierno de la Provincia, Roberto Sukerman, fue interpelado este lunes en Diputados acerca del supuesto vacunatorio VIP que funcionaba en Granadero Baigorria, como también sobre la marcha de la campaña de inoculación contra el covid en Santa Fe. Tras el encuentro virtual, los legisladores de ese bloque mayoritario manifestaron su disconformidad.

“Las veces que tenemos diálogo con el Ejecutivo nos pasan un power point o nos explican algunos detalles técnicos, y sin embargo cuestiones más básicas, quedan sin respuesta”, comentó por LT10 Erica Hynes. Entre esas cuestiones, se refirió a “quién tomó la decisión de que el vicedirector de la EPE pudiera vacunarse de manera incorrecta como personal de salud; quien le ofreció, le facilitó eso”.

Tampoco queda claro, según la diputada opositora, “si hay un listado de funcionarios” en la misma situación que aquél, o si es posible cruzar un registro de autoridades con el de vacunación, de manera de establecerlo.

“Prometer que para adelante no se van a cometer estas irregularidades es un discurso bastante recurrente. Pero, ¿qué pasó en esos meses?, ¿podemos saberlo o conocerlo? Nos quedan promesas de mayor profundización en la investigación (por parte de Sukerman) pero concretamente, respuestas, no”, lamentó.

Por otra parte, remarcó que si bien los funcionarios provinciales garantizan que existe un protocolo, desde la Cámara no lo han recibido ni visto por escrito. “No se ha transparentado. Nos dicen que son guías que vienen de Nación”.

“Esto no es un hecho mínimo, no es un hecho administrativo, no es un mero error. Acá hubo una cuestión de discrecionalidad, hubo gente que se vacunó con privilegios y eso es grave. Todos queremos que la vacunación sea un éxito pero no por eso podemos dejarlo pasar. Rendir cuentas no es un obstáculo para hacer las cosas bien y rápido. Es parte de eso”, cerró.

**Desde el Frente Social y Popular**

Por su parte, Carlos del Frade dijo que las cuatro horas de exposición del Ministro dejaron "sabor a poco", ya que esperaba una explicación política de cómo entender lo que había sucedido en Granadero Baigorria.

"No hubo ni un esbozo de explicación, ni siquiera una teoría de lo que pasó. El ministro solo repitió durante cuatro horas que el gobierno no oculta nada. Queríamos que estuviera un ministro político, y no de Salud, para que explique cómo funcionan los mecanismos de control para garantizar la transparencia en democracia", definió el legislador.

    https://youtu.be/0lAOIOh9wY4