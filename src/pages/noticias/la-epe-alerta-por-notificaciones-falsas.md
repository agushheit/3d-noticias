---
category: La Ciudad
date: 2021-02-20T06:30:27-03:00
thumbnail: https://assets.3dnoticias.com.ar/EPE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Epe Alerta Por Notificaciones Falsas
title: La Epe Alerta Por Notificaciones Falsas
entradilla: La Empresa Provincial de la Energía advierte a la población para que no
  sea engañada sobre la imposibilidad de ingreso a los domicilios para revisión del
  medidor.

---
La Empresa Provincial de la Energía (EPE) denuncia la distribución de una notificación falsa atribuida a la imposibilidad de ingreso a los domicilios para revisión del medidor. El mismo está siendo entregado con la clara intención de confundir a los usuarios.

El documento no está numerado, ni tiene información específica, copia los isologos de la Empresa Provincial de la Energía y el Ministerio de Producción de la Provincia de Santa Fe, sin especificar una fecha exacta y con un sello impersonal, que carece de cualquier tipo de validez.

La EPE alerta a la población, para que no sea engañada en su buena fe.

Ante cualquier duda, la Empresa solicita comunicarse a través de las vías oficiales o en la oficina comercial más cercana.