---
category: Estado Real
date: 2021-01-10T10:42:47Z
thumbnail: https://assets.3dnoticias.com.ar/100121-provincia-aportes.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Aportes para establecimientos educativos de los departamentos Las Colonias
  y Castellanos
title: Aportes para establecimientos educativos de los departamentos Las Colonias
  y Castellanos
entradilla: Son casi 2 millones de pesos destinados a cubrir distintas necesidades
  en las escuelas primarias Nº 826 de Elisa y N° 990 de Campo Almendra, y el Centro
  de Educación Física Nº 56 de Frontera.

---
El Gobierno Provincial, a través del Ministerio de Educación, entregó Fondos de Asistencia de Necesidades Inmediatas (FANI) a la Escuela Primaria N° 826 «Pablo Antonio Pizzurno» de Elisa, en el departamento Las Colonias, en el Centro de Educación Física N° 56 de Frontera y en la Escuela Primaria N° 990 «Manuel Belgrano» de Campo Almendra, en el departamento Castellanos.

La secretaria de Gestión Territorial Educativa, Rosario Cristiani, visitó la Escuela Primaria N° 826 «Pablo Antonio Pizzurno» para hacer entrega de la suma de $ 750.000 que serán destinados para refaccionar el espacio del Taller de carpintería que funciona en la institución. Los trabajos que se realizarán son el recambio del cielorraso, acondicionamiento de contrapisos, colocación de pisos mosaicos y adecuación de la instalación eléctrica.

Cristiani junto a la directora de la Escuela, Miriam Susana Paz; la vicedirectora María Mercedes Paira y el Tesorero de la Cooperadora escolar recorrieron las instalaciones que cuenta con: 5 aulas, laboratorio, taller de carpintería, 2 SUM, pasillos y patios. La escuela cuenta con una matrícula de 200 alumnos en dos turnos.

En la oportunidad, Cristiani manifestó que «hay que volver a habitar la escuela de manera diferente, vamos a necesitar desdoblar los grupos. Hay que pensar en una escuela flexible en el uso de sus espacios, en el uso de sus tiempos, **hay que pensar otro tipo de organización escolar** y eso se puede hacer situadamente. Cada equipo directivo tiene que mirar los espacios, analizar las matrículas, ver de qué manera los puede distribuir, por dónde van a ingresar, cómo serán los recorridos, cómo se van a ocupar las aulas».

En ese sentido, la funcionaria puntualizó que «**el protocolo es general**, lo armó el Ministerio de Educación, en función del Consejo Federal de Educación, en conjunto con todas las provincias y fue aprobado, pero después, **cada institución tiene que tomar los lineamientos generales y adecuarlos a sus espacios.** La escuela tiene condiciones para trabajar aplicando los protocolos y asegurando el cuidado de los chicos».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">APORTES A ESCUELAS DE CAMPO ALMENDRA Y FRONTERA</span>**

En esta oportunidad, la secretaria de Gestión Territorial Educativa, Rosario Cristiani, estuvo acompañada del senador por el departamento Castellanos, Alcides Calvo, y autoridades locales, donde se hicieron presentes en el Centro de Educación Física N° 56 de Frontera, para entregar aportes por un valor de $ 997.131, que serán destinados a la construcción de un módulo sanitario.

De la misma forma, recibió aportes FANI la Escuela Primaria N° 990 «Manuel Belgrano» de Campo Almendra por la suma de $236.001, que se destinarán a reparaciones en sanitarios de docentes y en la cocina del establecimiento: revoques en paredes, colocación de cerámicas en paredes y pisos, recambio de artefactos sanitarios, recambio de cañerías y griferías, como así también recambio de bajo mesada, bacha y adquisición y cambio de calefón.

Ambas instituciones fueron visitadas por la funcionaria el pasado 18 de noviembre donde se produjo un encuentro con las autoridades de las escuelas y se relevaron las necesidades lo que produjo esta entrega de aportes FANI de manera inmediata.