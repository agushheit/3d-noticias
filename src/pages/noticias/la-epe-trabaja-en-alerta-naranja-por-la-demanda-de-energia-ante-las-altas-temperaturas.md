---
category: La Ciudad
date: 2021-12-29T06:15:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/EPE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: La EPE trabaja en alerta naranja por la demanda de energía ante las altas
  temperaturas
title: La EPE trabaja en alerta naranja por la demanda de energía ante las altas temperaturas
entradilla: 'La demanda de energía por parte de los usuarios es sostenida desde hace
  varios días por la ola de calor y quedó al 90% del pico histórico de hace 10 días.

'

---
Desde hace más de una semana todo el parque eléctrico de la EPE está exigido casi al extremo. Pero a diferencia de los picos de calor, que luego daban descanso con la llegada de tormentas y la reducción de la temperatura, esta ola de altas temperaturas con sequía hace que funcionen al límite sin parar por la gran demanda.

En números de consumo, son más de 2.200 megavatios en forma constante desde la semana pasada, y en un escalón por debajo de los 2.446 megavatios anotados como cifra récord, según informa el diario La Capital de Rosario.

Según las previsiones meteorológicas, la temperatura por encima de los 35 grados va a profundizarse, con lo cual no se descartan marcas cercanas a lo histórico.

Si bien el verano pasado, con la mayoría de la población en los hogares, hubo fuertes picos de consumo, los eventos de máxima demanda eran escalonados y alternados. Los ciclos de calentamiento y enfriamiento le permitían un descanso a las redes.

“Pensamos un verano con cortes, nada muy diferente a lo que pasa en esta época en el resto del país, donde los ciclos térmicos de sobrecarga y enfriamiento hoy no están presentes. El sistema está tensionado y estresado de manera continua y permanente”, destacaron directivos de la EPE.

**Audiencia pública para actualizar las tarifas de la EPE**

La Empresa Provincial de la Energía, que actúa bajo la órbita del Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Resolución Nº 1278/21, llevará adelante una audiencia pública con el objeto de escuchar y recabar las opiniones de los santafesinos que deseen expresarse sobre la propuesta de adecuación del cuadro tarifario efectuado por la Empresa Provincial.

En este sentido, desde la empresa de Energía se recordó que durante 2020, la provincia en línea con el gobierno nacional tomó la decisión de congelar las tarifas debido al fuerte impacto en lo sanitario, económico y social de la pandemia. Y que en 2021, se dispuso porcentajes de incrementos máximos escalonados, que se aplicaron en el caso de usuarios residenciales, comerciales, industriales e institucionales, en un 14% para mayo, 8% en julio y 9% en noviembre.

A tal efecto, la audiencia pública constituye el ámbito para escuchar y recabar las opiniones de los santafesinos, y además, rendir cuentas de las acciones, obras y avances realizados por la EPE.