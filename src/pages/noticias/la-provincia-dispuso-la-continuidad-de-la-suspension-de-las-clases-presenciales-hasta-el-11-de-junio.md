---
category: Estado Real
date: 2021-06-06T06:15:02-03:00
thumbnail: https://assets.3dnoticias.com.ar/clases.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia dispuso la continuidad de la suspensión de las clases presenciales
  hasta el 11 de junio
title: La Provincia dispuso la continuidad de la suspensión de las clases presenciales
  hasta el 11 de junio
entradilla: La decisión se da en el marco de la extensión de las restricciones provinciales
  y conforme a lo establecido en los acuerdos federales para los dispositivos escolares.

---
El Ministerio de Educación prorroga la suspensión de las clases presenciales en todos los establecimientos educativos de la provincia. Esta decisión se da debido a que Santa Fe continúa en zona de alto riesgo y alarma epidemiológica según los indicadores sanitarios del Ministerio de Salud.

Durante la semana comprendida entre el 7 y el 11 de junio, los establecimientos educativos permanecerán abiertos con asistencia del personal escolar docentes y asistentes escolares, sin el dictado de clases presenciales continuando con el vínculo pedagógico en la distancia entre educadores y estudiantes.

Cada institución estará encargada de organizar los equipos docentes para desarrollar: clases virtuales cuando sea posible, comunicaciones con telefonía celular, distribución de materiales de lectura, diversos recursos impresos y guías de actividades para orientar las experiencias de aprendizaje de las alumnas y los alumnos conforme el conocimiento de cada contexto, cada grupo en su nivel y modalidad sosteniendo comunicación y relación en el proceso educativo.

Del mismo modo, podrán implementar consultas y acompañamientos a las y los estudiantes en trayectorias más fragilizadas a fin de producir las mediaciones necesarias y habilitar un trabajo continuo y sistemático con tareas en los hogares brindando los andamiajes pedagógicos contextualizados, conforme los acuerdos institucionales construidos para tal fin.

Cabe destacar que durante este período se mantendrán las tareas de sanitización e higiene de los edificios escolares y la distribución de los módulos alimentarios.