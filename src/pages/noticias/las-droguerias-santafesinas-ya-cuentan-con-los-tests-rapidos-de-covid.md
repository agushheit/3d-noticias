---
category: La Ciudad
date: 2021-04-28T08:58:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/autotest-covid.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Las droguerías santafesinas ya cuentan con los tests rápidos de covid
title: Las droguerías santafesinas ya cuentan con los tests rápidos de covid
entradilla: Aclaran que no es “un autotest que se pueden realizar en las casas o en
  farmacias, sino que lo tiene que hacer un profesional de la salud, específicamente
  un bioquímico”.

---
Se trata de los test con un funcionamiento similar a los de embarazos. Según explicó al Móvil de LT10, Alicia Caraballo del Colegio de Farmacéuticos de Santa Fe, estos empezaron a aparecer en las distintas droguerías.

“Es en momento no había tests en el mercado, y había una autorización por 60 días que hay que ver si Anmat la renueva. Están disponibles en las droguerías para la compra de las farmacias” agrego Caraballo.

Sin embargo, aclaró que no es “un autotest que se pueden realizar en las casas o en farmacias, sino que lo tiene que hacer un profesional de la salud, específicamente un bioquímico”.

Su funcionamiento es el siguiente: se realiza mediante una toma de muestra nasofaríngea, y luego se coloca dentro de un tubo con un reactivo que permite la disolución y eso va a la prueba. Después de unos minutos otorga el resultado.

El costo ronda en los $2.400, dependiendo de la marca. Sin embargo, para adquirirlos, las farmacias deben tener una demanda real para comprar el producto.