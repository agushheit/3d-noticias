---
category: Agenda Ciudadana
date: 2022-01-09T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/MORALES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: La UCR pidió al Presidente que "haga cumplir los Consensos logrados" en la
  Celac en materia de Derechos Humanos y democracia
title: La UCR pidió al Presidente que "haga cumplir los Consensos logrados" en la
  Celac en materia de Derechos Humanos y democracia
entradilla: '"Es urgente que el Presidente haga cumplir los Consensos logrados en
  esa organización respecto a la vigencia de la democracia y de los derechos humanos
  en sus países", resaltó el radicalismo en un comunicado.

'

---
La Unión Cívica Radical (UCR) pidió al presidente Alberto Fernández que como titular de la Comunidad de Estados Latinoamericanos y Caribeños (Celac) "haga cumplir los Consensos logrados" en el organismo internacional en materia de Derechos Humanos y democracia.

"Es urgente que el Presidente haga cumplir los Consensos logrados en esa organización respecto a la vigencia de la democracia y de los derechos humanos en sus países", resaltó la UCR en un comunicado.

A horas de que la Argentina asumió la Presidencia Pro Tempore (PPT) de la Celac durante un año, el radicalismo solicitó que "se apliquen las sanciones correspondientes a los que persiguen opositores y no garantizan elecciones libres, sin proscripciones y sin fraudes".

En ese marco, el centenario partido político advirtió que "sin los apoyos de Nicaragua, Venezuela y Cuba la Argentina no ocuparía ese lugar".

"La Argentina no debe renunciar a la política de Derechos Humanos y de defensa y promoción de la democracia", enfatizó la UCR.

Por último, indicó que "el Gobierno sostiene que busca construir consensos y que no llevará adelante una política contra otras organizaciones internacionales ni países", y completó: "Tendrá que demostrarlo".

En esa línea, el PRO advirtió sobre la posibilidad de que el Gobierno nacional haya hecho o haga "concesiones" a Cuba, Nicaragua y Venezuela "para obtener votos o evitar vetos" en su intento por quedarse con la Presidencia Pro Tempore de la Celac.

A través de su Secretaría de Relaciones Internacionales, el partido con el que la UCR comparte Juntos por el Cambio, subrayó la "necesidad de que el Gobierno nacional promueva la restauración y vigencia de la democracia y los derechos humanos en todos los países del continente".

La Argentina fue elegida ayer por consenso para ejercer la Presidencia Pro Tempore del bloque regional, que incluye a Venezuela, Nicaragua y Cuba, pero excluye a Estados unidos y Canadá.

Actualmente, Brasil tampoco forma parte de la Celac, dado que el presidente Jair Bolsonaro decidió retirar al país vecino del organismo.