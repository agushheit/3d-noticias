---
category: Agenda Ciudadana
date: 2021-10-24T06:00:51-03:00
thumbnail: https://assets.3dnoticias.com.ar/g20.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Presidente viaja a Roma para participar del G20 y luego a cumbre climática
  en Glasgow
title: El Presidente viaja a Roma para participar del G20 y luego a cumbre climática
  en Glasgow
entradilla: 'El mandatario tiene previsto participar de  de la Cumbre de jefes de
  Estado del G-20, en la cual insistirá en la necesidad de un marco multilateral para
  la reestructuración de la deuda. '

---
El presidente Alberto Fernández viajará la semana próxima a Italia para participar de la Cumbre de jefes de Estado del G-20, un foro que reúne a las principales economías del mundo y las naciones en desarrollo y que tendrá como eje central la "recuperación global pospandémica", y luego irá a Escocia para intervenir en la Conferencia de las Naciones Unidas sobre el Cambio Climático (COP26).  
  
Según informaron a Télam fuentes oficiales y tal lo cual lo hizo esta semana en un foro de finanzas del G-20, Fernández volverá a postular la "necesidad" de un marco multilateral para la reestructuración de la deuda de los países de renta media ante el "riesgo generalizado de crisis de deudas externas en los países en desarrollo".  
  
Argentina tiene vencimientos por 17.800 millones de dólares de capital para el 2022 y 18.800 millones para el 2023, de acuerdo a los números que difundió el ministro de Economía, Martín Guzmán, quien cuestionó la deuda por 44.154 millones de dólares que contrajo el expresidente Mauricio Macri.  
  
La Cumbre de Roma será la 16ª reunión del G20 a nivel de Jefes de Estado y de Gobierno y, a diferencia de la realizada el año pasado en Arabia Saudita, que fue virtual, ésta será nuevamente presencial, con lo cual hay mucha expectativa ya que en ese marco se pueden entablar reuniones bilaterales.  
  
En su paso por Roma, el Presidente estará acompañado por el canciller Santiago Cafiero; el ministro Guzmán y el embajador argentino en Washington y sherpa argentino del G-20, Jorge Argüello.  
  
Fuentes oficiales remarcaron la "relevancia regional" que significará la presencia de Fernández en el foro mundial más importante del mundo, ante la ya anunciada ausencia del presidente de México, Manuel López Obrador, y el interrogante sobre si asistirá el de Brasil, Jair Bolsonaro.  
  
El evento se celebrará en Roma el sábado 30 y domingo 31 de octubre, con la participación de los líderes de los países integrantes de ese bloque mundial, sus pares de los países invitados y los representantes de algunas organizaciones regionales.  
  
Las reuniones de trabajo de Jefes de Estado y de Gobierno se llevarán a cabo en el Centro de Convenciones de Roma 'La Nuvola', en el distrito EUR de la ciudad.  
  
Por su parte, el primer ministro italiano, Mario Draghi, anunció el miércoles que los jefes de Estado harán foco en la discusión del cambio climático -como antesala de la reunión de Glasgow-, el Covid-19 y la "recuperación global pospandémica".  
  
En la cumbre del G20 de noviembre del año pasado, que organizó Arabia Saudita, Fernández había evaluado que "el mundo transita hacia niveles alarmantes de desigualdad" en cada uno de los países y a nivel "global entre países que viven realidades diferentes", en momentos en que el coronavirus estaba expandido por todo el planeta.  
  
"La pandemia dejó en evidencia esa desigualdad, a la que debemos atacar para vivir en un mundo más equilibrado", expresó el jefe de Estado en su intervención del año pasado ante el foro mundial, que se realizó de forma virtual, bajo el lema "Hacer realidad las oportunidades del siglo XXI para todos".  
  
**A Escocia, por el cambio climático**

Tras la cumbre del G20, el Presidente se trasladará de Roma a Glasgow, Escocia, donde el 1 y 2 de noviembre se celebrará la Conferencia de la Organización de las Naciones Unidas (ONU) sobre el Cambio Climático (COP26), bajo el lema "Uniendo al mundo para hacer frente al cambio climático".  
  
Semanas atrás, al confirmar su participación en la cumbre climática, Fernández adelantó que Argentina presentará una "estrategia de largo plazo de neutralidad de carbono", anuncio que se conoció tras una videoconferencia con el secretario de Estado para el Desarrollo Internacional del Reino Unido, Alok Sharma.  
  
En ese camino, el jefe de Estado disertó además en septiembre pasado con otros mandatarios regionales en la Cumbre Latinoamericana sobre Cambio Climático, denominada "Diálogo de alto nivel sobre acción climática en las Américas", de la que Argentina fue anfitriona y sirvió como antesala de la de Glasgow.  
  
"O globalizamos la solidaridad o globalizamos la indiferencia. Es imprescindible que la transición climática y energética sea justa, y no agigante las brechas de bienestar en nuestro planeta", dijo al disertar en esa oportunidad.  
  
El Presidente describió que "una triple crisis de pandemia, cambio climático y deuda golpea duramente a los países de renta media como Argentina" y planteó que es "urgente repensar una nueva arquitectura financiera internacional" que incluya "la capitalización de los bancos de desarrollo y el canje de deuda por clima".  
  
En esa línea, enfatizó que la Argentina "ha decidido aumentar su ambición climática y comprometer la neutralidad de carbono hacia 2050" aún "en medio de una situación de endeudamiento, que resulta inédita a nivel mundial" y analizó que "una crisis generalizada de deuda sólo agravaría la degradación ambiental".  
  
De esa cumbre participaron, entre otros, el Enviado Especial para el Clima de los Estados Unidos, John Kerry, y el Secretario General de las Naciones Unidas, António Guterres, impulsores de la reunión de Glasgow.  
  
El evento reunirá a representantes de unos 200 gobiernos con el objetivo de acelerar la acción climática para el cumplimiento del Acuerdo de París, que -en el marco de las Naciones Unidas- establece medidas para la reducción de las emisiones de gases de efecto invernadero.  
  
A fin de año, en diciembre, el presidente Fernández afrontará otro evento internacional gravitante, con la reunión en Brasil de los jefes de Estado del Mercosur, en un encuentro en el que -por la liberación de restricciones por la pandemia- se verá cara a cara con sus pares del país anfitrión, Jair Bolsonaro; de Uruguay, Luis Lacalle; y de Paraguay, Mario Abdo Benítez.  
  
En el marco de la agenda bilateral, días atrás, los cancilleres Santiago Cafiero y su par de Brasil, Carlos França anunciaron los "consensos necesarios" para la revisión del Arancel Externo Común que rige el Mercosur, tras una reunión en el Palacio de Itamaraty.  
  
La reducción de las tarifas de importación de productos fuera del Mercosur "contempla las distintas necesidades de los países miembros", expresó un comunicado conjunto, en el que también se indicó que la propuesta deberá ser presentada a los otros socios, Paraguay y Uruguay.