---
category: Estado Real
date: 2021-04-12T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/PROVINCIA.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia estará representada en el Congreso Interamericano de Ingeniería
  Sanitaria
title: La Provincia estará representada en el Congreso Interamericano de Ingeniería
  Sanitaria
entradilla: Será a través de Aguas Santafesinas en el evento organizado por la Asociación
  Interamericana de Ingeniería Sanitaria y Ambiental.

---
La provincia de Santa Fe estará representada, a través de Aguas Santafesinas, en el 27° Congreso Interamericano de Ingeniería Sanitaria y Ambiental organizado por la Asociación Interamericana de Ingeniería Sanitaria y Ambiental (AIDIS), una de las entidades más prestigiosa del continente en dichas temáticas.

Cinco trabajos técnicos superaron la preselección de ponencias establecida por AIDIS, por lo que profesionales de la Gerencia de Infraestructura, Técnica y Calidad de la empresa estarán a cargo de presentar las mismas.

La Asociación Interamericana de Ingeniería Sanitaria y Ambiental es una sociedad civil técnico-científica, sin fines de lucro, que congrega las principales instituciones de profesionales y estudiantes de las tres Américas dedicados a la preservación ambiental, a la salud y al saneamiento.

La institución mantiene una amplia colaboración con la Organización Mundial de la Salud (OMS), que garantiza el reconocimiento y representación en las asambleas y comités ejecutivos tanto en la OMS como en la Organización Panamericana de Salud (OPS), y tiene presencia en 32 países de América y del Caribe.

El congreso mencionado se llevará a cabo del 12 al 15 de abril de manera virtual. Estos son los trabajos santafesinos que se presentarán:

>> Caracterización de las descargas de camiones atmosféricos: a cargo del Ingeniero Químico Gustavo Blainq, especialista en proceso de tratamiento de líquidos cloacales, perteneciente al departamento de Gestión Ambiental. Se hace mención especial a los operadores de los diez Centros de Recepción de Camiones Atmosféricos a cargo de ASSA y al personal de las áreas Residuales e Insumo y Residuales y Orgánicos pertenecientes a los laboratorios de Calidad Santa Fe y Rosario respectivamente.

>> Líquido residual de baños portátiles: a cargo del Ingeniero Químico Sebastián Canavoso, jefe del departamento de Gestión Ambiental. Se destaca que el trabajo ha sido desarrollado de manera conjunta con los profesionales del Centro de Ingeniería Sanitaria (CIS) de la Facultad de Ciencias Exactas, Ingeniería y Agrimensura (FCEIA) de la Universidad Nacional de Rosario (UNR) y de la Municipalidad de Rosario. Se enfatiza sobre la experiencia de trabajo colaborativo e interdisciplinario desarrollado en el marco de la Comisión Público Privada de Sustentabilidad Ambiental Rosario (CIMPAR), que, al propiciar la interacción de organizaciones públicas y privadas con intereses diversos, permitió abordar satisfactoriamente una problemática compleja y relevante.

>> Acueducto Pead de 1200 mm de diámetro. Innovación en diseño de piezas especiales: a cargo de la Ingeniera Civil María Laura Rodríguez, responsable del área Obras Especiales, y del Ingeniero en Construcciones Miguel Durando, jefe del departamento Recuento de Agua No Contabilizada (RANC), ambos pertenecientes al área de Infraestructura y Técnica Zona Sur.

>> Implementación de medios cromogénicos para el recuento de coliformes totales y escherichia coli en agua tratada por la técnica de filtro por membrana: a cargo de la Bioquímica Amalia Hort, jefe del área de microbiología del Laboratorio de Calidad Santa Fe y la Licenciada en Biotecnología Melisa Gerard, analista del laboratorio mencionado.

>> Validación secundaria de agar cromogénico para coliformes para el recuento e identificación de coliformes totales y escherichia coli en agua tratada por técnica de filtro por membrada: a cargo de las mismas profesionales que desarrollaron en trabajo anteriormente mencionado.