---
category: Agenda Ciudadana
date: 2021-12-09T06:00:41-03:00
thumbnail: https://assets.3dnoticias.com.ar/ALBERTOYCRISTINA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Alberto Fernández: "Está claro que Cristina y yo en muchas cosas no pensamos
  igual, el que decide finalmente soy yo"'
title: 'Alberto Fernández: "Está claro que Cristina y yo en muchas cosas no pensamos
  igual, el que decide finalmente soy yo"'
entradilla: '"Cristina y yo no somos iguales. Nosotros hicimos un frente que nuclea
  al peronismo y a muchas otras fuerzas y, por lo tanto, no hay una mirada homogénea
  sobre la realidad", enfatizó el jefe de Estado.

'

---
El presidente Alberto Fernández afirmó que en muchas cosas no piensa igual a la vicepresidenta Cristina Kirchner, pero aclaró que él es quien decide, y dijo estar "muy seguro" respecto del rumbo que deben tomar las negociaciones para reestructurar la deuda con el FMI.

"Cristina y yo no somos iguales. Nosotros hicimos un frente que nuclea al peronismo y a muchas otras fuerzas y, por lo tanto, no hay una mirada homogénea sobre la realidad", resaltó Fernández. Durante una entrevista concedida a Ian Bremmer, el fundador de la consultora de riesgo político Eurasia Group, el jefe de Estado aseguró que en la coalición gobernante "hay una mirada diversa".

En ese marco, afirmó que "Cristina es una persona muy importante en la política argentina", y argumentó: "Fue ocho años presidenta y es una persona que tiene, sin duda, una personalidad que en términos políticos trasciende en mucho en la Argentina".

Alberto Fernández sostuvo que hay "un relato mediático" que plantea a la vicepresidenta oponiéndose a una acuerdo con el Fondo Monetario Internacional, y destacó: "Eso no es cierto".

"Cristina y yo tenemos una historia común en ese punto, de asumir un país endeudado y tener que pagar las deudas que otros tomaron", enfatizó el Presidente. Consultado sobre la última carta que escribió la ex mandataria para referirse a la negociación con el FMI, respondió: "La lectura es que cuando ella lo escribe en una carta pareciera ser, o muchos lo quieren leer, como si me fijara un rumbo".

"La verdad es que yo estoy muy seguro de cuál es el rumbo que tenemos que tomar y tengo la certeza de que es el mismo que quiere Cristina", subrayó.

Además, el jefe de Estado completó: "Ella se expresa como siempre se expresó y actúa como siempre actuó. Lo que pasa es que ahora como no es la Presidenta, cuando ella habla se lee de otro modo. Yo lo leo con la misma naturalidad que siempre la leí".

"Generalmente, conciliamos posiciones, pero ahora la idea de que yo soy el bueno y Cristina es la mala es una falacia. La idea de que no siempre estamos de acuerdo es cierta, no siempre estamos de acuerdo", manifestó.

Al respecto, el Presidente confió que cuando no están de acuerdo en alguna idea tratan de "salvar las diferencias como dos personas racionales, hablando y conversando", y graficó: "Cuando hay que bajar el martillo lo hace el Presidente".

Por último, Fernández afirmó que "la pandemia es lo más parecido a caminar en un pantano", y explicó: "Uno no sabía dónde pisaba y se hundía".

"La pandemia para el que le tocó gobernar ese tiempo fue un momento particularmente ingrato, porque la pandemia a uno lo obligó a trabajar en terreno desconocido", reflexionó.