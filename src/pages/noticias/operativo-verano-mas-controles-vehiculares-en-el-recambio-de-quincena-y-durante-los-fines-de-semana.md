---
category: Estado Real
date: 2021-01-16T04:37:29Z
thumbnail: https://assets.3dnoticias.com.ar/operativo-verano.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Operativo Verano: más controles vehiculares en el recambio de quincena y
  durante los fines de semana'
title: 'Operativo Verano: más controles vehiculares en el recambio de quincena y durante
  los fines de semana'
entradilla: Las tareas se desarrollan en diferentes puntos del territorio, con especial
  atención en las zonas turísticas.

---
Las tareas se desarrollan en diferentes puntos del territorio, con especial atención en las zonas turísticas y las ciudades de Rosario y Santa Fe.

El subsecretario de Seguridad Vial de la provincia, Osvaldo Aymo, informó que con motivo del recambio de quincena, y durante los fines de semana, al incrementarse el flujo de tránsito se intensifican los controles vehiculares en el marco del **Operativo Verano**.

«Los estamos realizando en diferentes puntos, pero sobre todo en los alrededores de las ciudades de Rosario, de Santa Fe, y en las zonas turísticas», en tanto que «seguimos con el mismo control en las zonas limítrofes de la provincia», explicó el funcionario.

Al respeto, indicó que a los conductores se les exige la documentación básica y habitual, y agregó: «en este momento estamos pidiendo certificado de personal esencial para quienes circulen entre las 0.30 y las 6.30 horas».

Por otro lado, Aymo expresó: «vemos con preocupación el aumento de uso de celulares en rutas. En España detectaron que el uso de celular estaba por encima del consumo de alcohol y del exceso de velocidad como factor de riesgo». 

Por ese motivo, anticipó que desde la Agencia Provincial de Seguridad Vial están evaluando el uso de drones para controlar esta contravención. 

No obstante, aclaró que para incorporar esta herramienta «hace falta un cambio normativo para aplicarlo y ya estamos analizando la cuestión jurídica».