---
category: Agenda Ciudadana
date: 2021-02-25T06:00:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/baez.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Condenan a 12 años de prisión al empresario Lázaro Báez por lavado de dinero
title: Condenan a 12 años de prisión al empresario Lázaro Báez por lavado de dinero
entradilla: Se lo investigó por maniobras de lavado de activos por 55 millones de
  dólares. El veredicto incluyó a los cuatro hijos de Báez y a otra veintena de procesados

---
El empresario Lázaro Báez fue condenado este miércoles a 12 años de prisión en el marco del juicio oral en el que se lo investigó por maniobras de lavado de dinero por 55 millones de dólares entre 2003 y 2015.

La sentencia llegó luego de escucharse las últimas palabras de los imputados, con las exposiciones de Martín Erazo y Juan De Rasi, dos de los acusados, que en ambos casos se quejaron porque, en el marco del proceso, el tribunal no había aceptado planteos realizados por sus defensas en relación a las pruebas.

Báez está con prisión domiciliaria desde septiembre pasado, cuando la Cámara Federal de Casación Penal ordenó darle ese beneficio, y sigue las alternativas del juicio por videoconferencia, al igual que sus cuatro hijos.

El juicio comenzó en octubre de 2018 y en los primeros tramos del debate se preveía una duración de ocho meses, pero se sumaron inconvenientes que corrieron esos plazos hasta que, finalmente, la pandemia de coronavirus derivó en la suspensión de las audiencias desde el 20 de marzo último hasta junio pasado, cuando se reanudó de manera virtual.