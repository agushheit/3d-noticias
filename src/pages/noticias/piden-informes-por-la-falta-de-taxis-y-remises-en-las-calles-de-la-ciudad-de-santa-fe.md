---
category: La Ciudad
date: 2021-11-28T06:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/concejitus.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Piden informes por la falta de taxis y remises en las calles de la ciudad
  de Santa Fe
title: Piden informes por la falta de taxis y remises en las calles de la ciudad de
  Santa Fe
entradilla: Carlos Pereira presentó un pedido de informes para que el Ejecutivo informe
  la cantidad de licencias o permisos de taxis y remises que se encuentran sin habilitación.

---
La realidad que enfrenta el sistema de taxis y remises en la ciudad fue uno de los temas abordados en el Concejo Municipal. Desde el bloque de Juntos por el Cambio, Carlos Pereira presentó un pedido de informes para que el Ejecutivo informe la cantidad de licencias o permisos de taxis y remises que se encuentran sin habilitación; la cantidad de licencias o permisos que han sido dados de baja, y también si existen demoras en la tramitación de la habilitación anual y, en tal caso, los motivos de esta situación.

 El reclamo de vecinos por la dificultad de acceder a un taxi o remís es uno de los reclamos recurrentes en la capital provincial en los últimos meses. "Conseguir un coche se ha convertido en una pesadilla diaria para muchos. Los usuarios del sistema relatan esperas muy prolongadas y, en algunos horarios o días, la imposibilidad lisa y llana de poder tomar algún coche", remarcó concejal.

 Asimismo, apuntó que durante la pandemia, tanto taxistas como remiseros "debieron afrontar meses muy duros, con caída del pasaje, lo que derivó en situaciones en donde muchos no pudieron mantener los sueldos de choferes, o el pago de patentes y otros impuestos". En ese contexto, muchas licencias o permisos no pudieron ser renovados, por lo que muchos coches no pudieron volver a la calle. "El Municipio no tuvo ninguna política clara para atender estas situaciones", criticó Pereira.