---
category: Agenda Ciudadana
date: 2022-02-01T11:38:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/bariloche-g84db00f5d_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Came
resumen: Bariloche fue el destino mas elegido en el Pre-Viaje
title: Bariloche fue el destino mas elegido en el Pre-Viaje
entradilla: En lo que va de esta temporada los destinos locales recibieron a 19,7
  millones de turistas, que realizaron un gasto directo de $342.830 millones.

---
Según un nuevo relevamiento de la Confederación Argentina de la Mediana Empresa (CAME), entre la segunda quincena de diciembre y fines de enero 19,7 millones de turistas eligieron pasar sus vacaciones en destinos locales, cifra que representa un 58% más que en la temporada 2021. Por otro lado, el gasto directo fue de $342.830 millones, que, medido a precios constantes, subió un 93,1% frente al año pasado y 1,2% más en relación a la temporada pre-pandemia. El récord de contagios de Covid-19 por la variante Ómicron, que generó una nueva ola de casos en todo el país, motivó que los turistas eligieran viajar a lugares más cercanos, pero optando por estadías más prolongadas en relación al comportamiento histórico.

En este período, el gasto promedio diario registrado fue de $3.723,9 por turista y la estadía media se ubicó en los 4,7 días, frente a 3,9 días del año pasado. El programa Pre-Viaje fue determinante, con 4,5 millones de beneficiarios en lo que va de la temporada y con facturas de servicios turísticos por casi $100 mil millones. Solo en la segunda quincena de enero fue utilizado por 650 mil personas, por un gasto de $16 mil millones. El destino más elegido en el Pre-Viaje fue Bariloche.

El sondeo de la entidad también permitió cuantificar el movimiento de los excursionistas, que son aquellos que se movilizan para pasar períodos cortos, que pueden ser el día o parte de una jornada, en destinos cercanos a sus hogares. Este verano, 21,1 millones de personas aplicaron a este segmento, impactando con un gasto total de $33.070 millones. Este movimiento tuvo que ver, en muchos casos, con el regreso de eventos y fiestas en localidades.

Sumando el total de turistas y excursionistas, en el período relevado se registraron en total 40,8 millones de viajes con un impacto económico directo que ascendió a $375.900 millones. El 11,2% de los viajes se hicieron a la Costa Atlántica, con 2,2 millones de visitantes.

“La variante Ómicron sigue siendo un asunto no superado en Argentina, ni en el mundo. A pesar de eso, el número de viajes no sufrió una disminución, y se notó un cambio de tendencia, que dio prioridad a los destinos nacionales, favorecidos con el programa Pre-Viaje, y que se vio en la menor cantidad de viajes a destinos internacionales. El movimiento benefició a la actividad comercial en las ciudades turísticas del país, especialmente al rubro gastronómico”, detallaron desde CAME.

\- El Partido de la Costa, Mar del Plata, Pinamar, Villa Gesel, Bariloche, Mendoza, Villa Carlos Paz, Monte Hermoso, San Martín de los Andes, Salta, Federación y San Rafael son algunas de las ciudades con mayor caudal turístico.

\- Las plazas hoteleras, cabañas, departamentos, quedaron colmados, con ocupación casi plena los fines de semana en muchísimas ciudades. Fue muy difícil llegar a destino sin reservas, algo que cada vez se hace menos.

\- El clima fue muy bueno hasta el 20 de enero aproximadamente. Después tuvo altibajos, pero que no desalentaron a los viajeros.

\- El Covid prácticamente no incidió en la cancelación de viajes, pero sí fueron frecuentes los casos de turistas que debieron estar algunos días en resguardo por tener el virus. Ayudó que la variante Ómicron es muy suave.

\- El tipo de cambio alto, los trámites sanitarios para ingresar a otros países, sumado a la incertidumbre sobre la tendencia futura del covid-19, favorecieron a los destinos nacionales, que fueron priorizados por sobre países como Brasil, Uruguay, Chile y Miami, destinos que compiten con los argentinos en los sectores de la población con ingresos medios y altos.

\- En materia de precios, este verano la Argentina fue competitiva frente a opciones como Uruguay, Brasil y Chile. Y el pre-viaje acentuó esas diferencias, especialmente en los sectores de ingresos medios que fueron los grandes usuarios de ese programa.

\- Lo que más se buscó fueron las zonas de playas (mar, río, arroyos), seguidas por las zonas de termas y montañas. También hubo mucho turismo que fue detrás de las propuestas culturales, recreativas y deportivas, que cada vez tienen más impacto en las decisiones de viajes.

\- Como viene ocurriendo todos los años, en 2022 se continuaron sumando nuevos destinos al circuito turístico nacional. Incluso se observa un flujo muy activo de gente en ciudades que solían tener sus temporadas fuertes en otras épocas del año y ahora la tienen también en verano. Un caso claro fue Bariloche, que con el impulso del pre-viaje, vivió enero como si fuera su temporada alta. Lo mismo sucedió en otros destinos de la Patagonia, que fueron los más favorecidos de ese programa,

\- La variedad de destinos que ofrece la Argentina, más el incremento en la oferta de servicios y la mejora en la calidad de la infraestructura, en la atención al visitante y en las propuestas recreativas, son un atractivo para retener al residente nacional y atraer al extranjero.

\- La actividad comercial en las ciudades de veraneo se vio muy beneficiada, especialmente el rubro gastronomía, que trabajó activamente.

\- Para la primera quincena de febrero se esperan muy buenos resultados. La segunda quincena será más restringida porque hay ciudades como CABA donde ya se anunció que las clases en el nivel primario comenzarán este año el 22 de febrero.

\- Pero el cierre de esta temporada lo dará nuevamente el feriado de carnaval, que arranca el 28 de febrero y este año promete un gran despliegue de turistas por todo el país. Las ciudades ya están preparando una oferta turística muy amplia para esa fecha.