---
category: Estado Real
date: 2021-10-29T06:15:45-03:00
thumbnail: https://assets.3dnoticias.com.ar/reunionrosario.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El gobernador se reunió con integrantes de la justicia federal de Rosario
title: El gobernador se reunió con integrantes de la justicia federal de Rosario
entradilla: 'Omar Perotti remarcó la necesidad de cubrir los cargos vacantes en la
  Justicia y crear una nueva estructura. '

---
El gobernador Omar Perotti presidió este jueves una reunión de trabajo con Jueces, Fiscales y Defensores Públicos de la Justicia Federal de Rosario, con la intención de coordinar esfuerzos y trabajar de manera conjunta para hacer frente a las problemáticas que enfrenta la seguridad en la ciudad.  
  
Participaron de la actividad el presidente de la Cámara Federal de Apelaciones de Rosario, Aníbal Pineda; el presidente de la Corte Suprema de Justicia provincial, Roberto Falistocco; el ministro de Seguridad, Jorge Lagna; y el secretario de Justicia, Gabriel Somaglia, entre otras autoridades provinciales y de la Justicia Federal.  
  
Luego del encuentro, el gobernador destacó “la valía de este tipo de reuniones. Poder coordinar los esfuerzos es lo que estamos haciendo en esta primera reunión de trabajo. De esto se sale con más trabajo y mayor coordinación, colaborando entre todos los que tenemos responsabilidades”, señaló Perotti.  
  
A continuación, se refirió a las falencias que posee la Justicia Federal: “Tenemos menos herramientas, menos posibilidades para dar esta batalla, si no tenemos una estructura Federal que nos acompañe. Para eso, se requiere la cobertura de vacantes”, describió. Y agregó: “Necesitamos una nueva estructura. Hay reclamos de nuevas estructuras en todo el país, pero no creo que haya urgencias o prioridades tan marcadas como las que los santafesinos sentimos que tiene nuestra provincia”, expresó el gobernador.  
  
En ese sentido, Perotti explicó que el Poder Ejecutivo Nacional ya recibió el trabajo del Consejo de la Magistratura, con los postulantes para cubrir los cargos vacantes en la justicia Federal, que luego debe ser tratado en el Senado de la Nación. “Necesitamos, primero, cubrir las vacantes, porque estamos hablando de una estructura existente que es raquítica y sin la cobertura de esas vacantes. Esa es la prioridad”, detalló.  
  
Finalmente, el gobernador remarcó que “sin dudas, este trabajo conjunto que empieza a dar resultados, es lo único que va a cambiar el humor social. No importa si depende del Poder Ejecutivo, Legislativo o Judicial, se requieren respuestas, de que cada uno de nosotros ponga lo mejor, los mayores niveles de colaboración y remover obstáculos, como la falta de equipamiento e infraestructura necesaria para un bien funcionamiento”, concluyó Perotti.  
  
Por su parte, el presidente de la Cámara Federal de Apelaciones de Rosario, Aníbal Pineda, destacó que “es la primera vez que un gobernador en funciones viene a una reunión de trabajo en la Justicia Federal, en más de 110 años”. Luego agregó que “también hago una autocrítica, y es que no pudimos poner en agenda las necesidades de la Justicia Federal, pero es el momento de trabajar en conjunto y aunar esfuerzos”, sostuvo.  
  
“Está claro –continuó Pineda- que la Justicia Federal requiere muchas reformas, muchas transformaciones y fortalecimiento. Necesitamos el compromiso y el trabajo de los tres poderes en la provincia de Santa Fe, para que exijamos a nivel nacional la reforma”, finalizó.