---
category: Agenda Ciudadana
date: 2021-09-04T06:15:39-03:00
thumbnail: https://assets.3dnoticias.com.ar/restriccionesmayo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Coronavirus: amplían el horario de comercio, locales gastronómicos y circulación
  nocturna'
title: 'Coronavirus: amplían el horario de comercio, locales gastronómicos y circulación
  nocturna'
entradilla: 'Las medidas regirán desde este sábado y hasta el 1° de octubre. '

---
La provincia de Santa Fe anunció este viernes por la tarde las restricciones que regirán desde este sábado y hasta el 1° de octubre en el marco de la pandemia de coronavirus.

Entre las medidas se destaca la ampliación del horario de comercio, de atención en los locales gastronómicos y la circulación nocturna.

**ACTIVIDAD COMERCIAL MAYORISTA Y MINORISTA**

La actividad del comercio mayorista y minorista de venta de mercaderías, con atención al público en los locales, podrá extenderse todos los días de la semana hasta las 20 horas, con excepción de los kioscos que podrán permanecer abiertos hasta las 24 horas para la atención al público residente en su cercanía. Lo anterior sin perjuicio de la posibilidad de las farmacias de realizar los turnos de guardia. El factor de ocupación de la superficie cerrada de los locales, destinada a la atención del público, no podrá exceder del treinta por ciento (30%).

**ACTIVIDAD GASTRONÓMICA**

Los locales gastronómicos (comprensivo de bares, restaurantes, heladerías y otros autorizados a funcionar como tales, con concurrencia de comensales), deberán ajustarse a las siguientes disposiciones:

Los días viernes, sábados y vísperas de feriados, entre las 6 horas y la 1 hora del día siguiente;

El resto de los días de la semana, entre las 6 horas y las 24 horas.

Fuera de los horarios establecidos, sólo podrán realizar actividad en las modalidades de reparto a domicilio y de retiro, siempre que esta última se realice en locales de cercanía.

**COMPETENCIAS DEPORTIVAS AUTOMOVILÍSTICAS Y MOTOCICLÍSTICAS**

Se autorizan las competencias deportivas automovilísticas y motociclísticas, que podrán realizarse con concurrencia de hasta 1.000 espectadores.

En el caso que el establecimiento dedicado a la actividad cuente con sectores que funcionen de manera absolutamente independiente y mediando la autorización correspondiente de las autoridades locales, será posible la concurrencia de hasta un máximo de 3.500 personas (no excediendo cada sector independiente el número máximo de 1.000 asistentes).

La autorización queda sujeta al estricto cumplimiento de los protocolos específicos establecidos para las actividades deportivas y de las reglas generales de conducta. Para la asistencia a estos espectáculos, se requiere contar al menos con 1 dosis de vacuna Covid-19.

**CIRCULACIÓN VEHICULAR**

Queda autorizada la circulación vehicular en la vía pública de la siguiente manera:

Viernes y sábados: desde las 6 horas la hora 1 del día siguiente.

De domingo a jueves: desde las 6 horas hasta las 24 horas.

Quedan exceptuados quienes realicen actividades definidas como esenciales en la emergencia, comprensivas de las situaciones de fuerza mayor, y los desplazamientos desde y hacia los lugares de trabajo de los que desarrollan actividades habilitadas, incluidos los de los propietarios de los locales o establecimientos.

**ACTIVIDADES QUE PERMANECEN SUSPENDIDAS**

En todo el territorio provincial, permanecerán suspendidas las siguientes actividades:

a) Funcionamiento de locales de juegos infantiles y otros establecimientos afines en espacios cerrados o al aire libre, comúnmente denominados miniclubs o peloteros.

b) Discotecas.

c) Funcionamiento de bares, restaurantes, patios de comidas y espacios de juegos infantiles ubicados en centros comerciales, paseos comerciales o shoppings y demás establecimientos comprendidos en el artículo 2° inciso e) apartado 4 de la Ley N° 12069, con asistencia de clientes a los locales; salvo para los bares, restaurantes y demás locales gastronómicos que tuvieran ingresos y egresos exteriores independientes, a los que pueda accederse sin transitar por los espacios interiores de circulación donde se ubican los comercios, sin habilitar corredores internos entre las zonas, a los fines del control efectivo del coeficiente máximo de ocupación de las superficies cerradas.

d) Realización de eventos culturales y recreativos relacionados con la actividad teatral y música en vivo que impliquen concurrencia de personas en espacios abiertos de acceso libre para el público.

e) Actividad artística en plazas, parques y paseos.

**COMPETENCIA DE AUTORIDADES LOCALES**

Las autoridades municipales y comunales podrán disponer en sus respectivos distritos, en consulta con el Ministerio de Salud, mayores restricciones que las establecidas en el presente decreto.

Las autoridades municipales y comunales, en concurrencia con las autoridades provinciales competentes, coordinarán los procedimientos de fiscalización necesarios para garantizar el cumplimiento de las medidas dispuestas en el presente decreto, de los protocolos vigentes y de las normas dispuestas en virtud de la emergencia sanitaria.