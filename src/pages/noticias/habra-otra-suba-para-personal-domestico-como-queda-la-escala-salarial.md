---
category: Agenda Ciudadana
date: 2021-03-17T04:12:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/domestica.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias y Agencias
resumen: 'Habrá otra suba para personal doméstico: cómo queda la escala salarial'
title: 'Habrá otra suba para personal doméstico: cómo queda la escala salarial'
entradilla: El nuevo incremento será del 10 por ciento y se sumará al 10 por ciento
  otorgado en diciembre y al 8 por ciento de febrero. La grilla para supervisores,
  caseros, niñeras, personal del limpieza y otros.

---
En abril llegará el tercer y último incremento para empleadas domésticas y servicios de ciudades en general, que permitirá llegar al 28 por ciento de aumento pactado en diciembre del año pasado. El nuevo incremento que se aplicará en dos semanas será del 10 por ciento y se sumará al 10 por ciento otorgado en diciembre y al 8 por ciento de febrero.

De acuerdo con la Unión Personal de Auxiliar de Casas Particulares (Upacp), los sueldos tendrán los siguientes incrementos, dependiendo de la categoría del empleado y la modalidad de trabajo, con o sin retiro.

**Supervisores**

**Coordinación y control de las tareas efectuadas por dos o más personas a su cargo.**

Hora con retiro: $223,50. Hora sin retiro: $244,50. Mensual con retiro: $27.928. Mensual sin retiro: $31.108,50.

**Personal para tareas específicas**

Cocineros/as contratados en forma exclusiva para desempeñar dicha labor, y toda otra tarea del hogar que requiera especial idoneidad del personal para llevarla a cabo.

Hora con retiro: $211,10. Hora sin retiro: $232. Mensual con retiro: $25.946,50. Mensual sin retiro: $28.883.

**Caseros**

Personal que presta tareas inherentes al cuidado general y preservación de una vivienda en donde habita con motivo del contrato de trabajo.

Hora: $199,50. Mensual: $25.315

**Personal de asistencia y cuidado de personas**

Comprende la asistencia y cuidado no terapéutico de personas, tales como: personas enfermas, con discapacidad, niños/as, adolescentes, adultos mayores.

Hora con retiro: $199,50. Hora sin retiro: $223,50, Mensual con retiro: $25.315, Mensual sin retiro: $28.211.

**Personal para tareas generales**

Prestación de tareas de limpieza, lavado, planchado, mantenimiento, elaboración y cocción de comidas y, en general, toda otra tarea típica del hogar.

Hora con retiro: $185. Hora sin retiro: $199,50. Mensual con retiro: $22.765,50. Mensual sin retiro: $25.315