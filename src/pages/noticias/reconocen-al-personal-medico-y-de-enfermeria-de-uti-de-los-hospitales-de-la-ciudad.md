---
category: La Ciudad
date: 2021-07-08T08:56:07-03:00
thumbnail: https://assets.3dnoticias.com.ar/terapiajpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Ines Larriera
resumen: Reconocen al personal médico y de enfermería de UTI de los hospitales de
  la ciudad
title: Reconocen al personal médico y de enfermería de UTI de los hospitales de la
  ciudad
entradilla: Es por la labor de estos profesionales en las unidades de Terapia Intensiva
  de los hospitales Cullen e Iturraspe. La iniciativa fue de la concejala Inés Larriera
  y contó con el apoyo de todo el cuerpo legislativo

---
Este miércoles, en el recinto de sesiones del Honorable Concejo de Santa Fe  se entregó la declaración de interés municipal a los médicos, médicas, enfermeros y enfermeras de las Unidades de Terapia Intensiva de los hospitales Cullen e Iturraspe de nuestra ciudad, por sus trabajos comprometidos frente a la pandemia generada por el COVID-19.

"Este es un reconocimiento muy especial a quienes están trabajando hace más de un año ininterrumpidamente y casi desde el anonimato porque no los conocemos tanto. Sabemos que todo el personal médico está abocado a cuidar y preservar la salud de los santafesinos y santafesinas pero nos pareció importante destacar en especial este trabajo coordinado que realizan los profesionales médicos y enfermeros" declaró Inés Larriera, autora del proyecto.

“Queremos visibilizar el trabajo que realizan, han tenido que aprender sobre la enfermedad y contener emocionalmente a las familias de los pacientes que están en las terapias. Son un orgullo para nuestra ciudad contar con ustedes”.

La concejala aprovechó la oportunidad para recordar “que nos tenemos que seguir cuidando, esto no terminó, y el trabajo que ustedes realizan sigue siendo intenso. Por eso queremos que los santafesinos y santafesinas los conozcan y sepan lo que están entregando día a día”

Por su parte, el doctor Néstor Carrizo, jefe de la Unidad de Terapia Intensiva del hospital Cullen expresó: “Hemos vivido una situación que nunca pensamos vivir, el nivel de exigencia operativo y emocional fue llevado al extremo en esta pandemia. La diferencia en relación a otras situaciones que vivimos en años previos es que esto es continuo y por muchos meses; la pandemia nos puso a todos a prueba”.

Carrizo agregó que “a pesar de todo tenemos satisfacciones cuando nos toca salvar vidas o cuando podemos recuperar personas. Y este tipo de reconocimientos también nos fortalece”.

A su turno, la doctora Agostina Gargatagli, en representación de los médicos del Hospital Iturraspe manifestó: “Queremos darles las gracias por este reconocimiento, hace más de un año que venimos trabajando en forma sostenida con los pacientes. Todos los que estamos en terapia intensiva estamos cansados, pero ponemos lo mejor de nosotros para que los pacientes salgan adelante”.

“Esta enfermedad nos transformó la vida a todos, tuvimos mucho que aprender. La línea entre la vida y la muerte es tan fina que yo insisto mucho con la responsabilidad y el cuidado de la población” sostuvo Sergio Aguilar, el jefe de Enfermería del Hospital Iturraspe.

“Gracias por este reconocimiento, nos hace muy bien y vamos a hacerlo extensivo a nuestros colegas y a esos trabajadores que no se ven”, finalizó.

Por último, tomó la palabra Sandra Alvarez, licenciada en Enfermería y actual jefa del personal del hospital Cullen y expresó: “Quiero agradecer a todos ustedes por esta distinción. Y en especial al grupo de profesionales que tengo a cargo que están dando todo para que los pacientes se recuperen”.

Del acto participaron los concejales Inés Larriera, Carlos Pereira y el presidente del Concejo, Leandro González. De manera virtual, la entrega fue seguida por las concejalas Laura Spina, Valeria López Delzar, por el director del Hospital Cullen, Juan Carlos Poletti y por distintos profesionales y trabajadores de la salud.

![](https://assets.3dnoticias.com.ar/unnamed.jpg)