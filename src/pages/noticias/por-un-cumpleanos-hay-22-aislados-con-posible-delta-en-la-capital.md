---
category: La Ciudad
date: 2021-09-15T06:15:30-03:00
thumbnail: https://assets.3dnoticias.com.ar/DELTASANTAFE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Por un cumpleaños, hay 22 aislados con posible Delta en La Capital
title: Por un cumpleaños, hay 22 aislados con posible Delta en La Capital
entradilla: Lo confirmaron desde el Ministerio de Salud. El origen son tres menores
  de diez años con covid positivos que volvieron de viaje y fueron a un cumpleaños.

---
Este martes el secretario de Salud, Jorge Prieto, y la ministra de Salud, Sonia Martorano, confirmaron que hay 22 personas aisladas por sospechas de estar enfermos con la variante Delta de Covid. Confirmaron que el origen del brote está vinculado a un cumpleaños que se festejó en un country privado de Santo Tomé en el que participaron tres menores que habían vuelto de Estados Unidos días atrás. Los pequeños son asintomáticos y se habían realizado el testeo rápido que dio negativo, pero a los días con el PCR se descubrió que tenían la cepa.

Prieto además describió que se confirmaron 10 casos en toda la provincia, a partir de controles en las fronteras santafesinas. "Ocho casos fueron detectados en Rosario, uno en Rafaela y otro en la ciudad de Santo Tomé. En cuanto a la alta probabilidad de estos tres casos que también corresponden a la ciudad de Santo Tomé, en este momento se está haciendo la secuenciación para confirmarlo, nosotros creemos en que haya alta probabilidad porque la provincia cuenta hoy con un test que es un tamizaje rápido que puede dar una probabilidad sobre qué variante tenemos en frente. Hay una pequeña demora en la cuestión diagnóstica. En 48 o 72 horas vamos a tener la confirmación. Estamos frente a una variante (Delta) que tiene un periodo muy corto de incubación, y la alta contagiosidad", relató.

Al ser consultado por los incumplimientos de estas personas sospechosas, Prieto dijo que respetaron los protocolos que incluyen la realización de PCR y un test rápido en el aeropuerto que en su caso dio negativo. "Por más que el test rápido de negativo en Ezeiza, hay que cumplir la cuarentena, es muy importante para poder disfrutar de este momento que nos muestra un optimismo. Al séptimo día nosotros indicamos un PCR luego del aislamiento. Con estos tres casos que son asintomáticos aparece la positividad de variantes Delta, se tamiza y se cree que hay una alta probabilidad por eso se está analizando. Aparentemente hay un incumplimiento de un día previo, porque el día once tendría que haber realizado la PCR y el día anterior asisten un cumpleaños tal vez pensando que su test les iba a dar negativo, que fue el gran error. Recordemos que no hay vacunas para menores de diez años", contó el secretario.

Por su parte, Martorano destacó que a nivel provincial solo el cinco por ciento de positividad en los testeos. "Los índices son buenos con una provincia que está en verde. La ocupación de camas es del 58 por ciento en Santa Fe, pero esto va muy de la mano con los cuidados que se están haciendo y de la vacunación. Esta semana vamos a llegar seguramente al 50 por ciento de dos dosis colocadas, estamos en 1.700.000 ya, y 2.400.000 con una dosis", detalló.