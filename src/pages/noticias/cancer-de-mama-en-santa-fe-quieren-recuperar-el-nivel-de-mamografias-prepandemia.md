---
category: La Ciudad
date: 2021-10-11T06:15:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/mama.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Cáncer de mama: en Santa Fe quieren recuperar el nivel de mamografías prepandemia'
title: 'Cáncer de mama: en Santa Fe quieren recuperar el nivel de mamografías prepandemia'
entradilla: El número de estudios cayó un 70% en el sector público durante 2020. Para
  promover las prácticas, abrirán consultorios a demanda durante octubre.

---
Las mamografías cayeron hasta el 70% en los efectores púbicos provinciales si se compara lo que pasó en 2020 con lo que sucedía en 2019. La pandemia de Covid postergó controles médicos indispensables en muchas especialidades, como la oncología. Por temor de los pacientes a contagiarse y porque los hospitales estaban abocados a los enfermos de coronavirus

Ahora, la Agencia de Control de Cáncer del Ministerio de Salud de Santa Fe busca recuperar los niveles de atención prepandemia.

Por eso, durante octubre, donde se conmemora en el mundo el mes de concientización del cáncer de mama habrá distintas actividades de difusión, y sobre todo, se abrirán los consultorios a demanda para que las mujeres, que son las más afectadas por este cáncer, puedan acceder con facilidad al chequeo correspondiente que incluye una mamografía y en muchos casos, además, una ecografía mamaria.

El cáncer de mama tiene una alta incidencia en América Latina. Los casos severos aumentaron en 2020 a causa de la postergación de los controles.

Sólo el año pasado se diagnosticaron 210 mil nuevos casos en Latinoamérica y hubo 57.500 fallecimientos por cáncer de mama.

Según publica La Capital, en Santa Fe, según datos de la Agencia, el cáncer de mama representa el 17% del total de tumores registrados por año. Es el primero en incidencia en las mujeres de la provincia.

**Chequeos necesarios**

La campaña de mamografías de este año busca que regresen a los controles especialmente las mujeres de entre 50 a 70 años que son las que más chances tienen de tener un tumor maligno en sus mamas (los hombres también pueden ser diagnosticados pero representan apena el 1% de los casos).

De todos modos, los especialistas recuerdan que puede darse a cualquier edad por eso es necesario anualmente visitar al ginecólogo o mastólogo, y hacerlo particularmente antes de los 50 años aquellas mujeres que tienen antecedente familiares.

Si hay una abuela, madre, hermana que tuvo cáncer de mama a edades tempranas esa mujer debe hacerse la primera mamografía 10 años antes de la edad en la que fue diagnosticada su familiar. Es decir que si la madre tuvo cáncer a los 40 deberá hacerse el chequeo a los 30.

**Diagnóstico precoz**

La médica Graciela López de Degani, directora de la Agencia de Cáncer de Santa Fe comentó que en distintas ciudades de la provincia, durante octubre habrá "una modalidad diferente con atención a demanda" y enfatizó: "Queremos recuperar los niveles de atención prepandemia".

La especialista señaló que durante 2020 cayeron los controles más del 70% pero destacó que en los últimos tres meses de este año las mujeres volvieron a acercarse a los hospitales.

Las áreas en las que se realizan las mamografías y ecografías mamarias (que en algunos casos estuvieron cerradas por estar ubicadas en los mismos lugares donde se realizaban los estudios pulmonares a pacientes Covid) hoy tienen turnos disponibles.

"Hemos recuperado la actividad en un 30% y buscamos llegar a niveles previos a la pandemia ya que encontrar un tumor pequeño permite tener altas tasas de curación", dijo.

Incluso, en tumores más avanzados "les pedimos a las pacientes que no se desanimen, que se controlen, que sigan con los tratamientos indicados porque las chances de tener una buena calidad de vida e incluso curarse, existen".

Muchas mujeres demoran los exámenes médicos por temor o porque minimizan los riesgos. "No es raro escuchar que alguien no se hace la mamografía porque tiene miedo de encontrarse algo y es justamente lo contrario a lo que debe hacerse y hay personas que se encuentran un bulto en la mama y eso las paraliza y no consultan", dijo López de Degani.

**Qué tener en cuenta**

Si bien el cáncer de mama puede presentarse a cualquier edad e incluso sin ningún tipo de antecedente familiar hay factores de riesgo para considerar porque pueden incrementar el riesgo de enfermarse.

* Ser mayor de 50 años.
* Antecedentes personales de cáncer de mama o de enfermedad preneoplásica de mama.
* Antecedentes familiares de cáncer de mama.
* Tratamiento previo con radioterapia dirigida al tórax.
* Terapia de reemplazo hormonal.
* Primera menstruación a edad temprana.
* Edad avanzada en el momento del primer parto o nunca haber tenido hijos.
* Además el sobrepeso, el sedentarismo, y consumo excesivo de alcohol son factores que se pueden evitar para disminuir el riesgo de tener cáncer de mama.