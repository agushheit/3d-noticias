---
category: La Ciudad
date: 2021-06-26T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/Jardines.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El lunes los Jardines Municipales vuelven a las clases presenciales
title: El lunes los Jardines Municipales vuelven a las clases presenciales
entradilla: 'Los jardines municipales que integran el Sistema Municipal de Educación
  Inicial retoman las actividades con las clases presenciales el próximo lunes. '

---
El intendente Emilio Jatón anunció que este lunes, los Jardines Municipales vuelven a la actividad presencial. Según dijo, esto es posible gracias a que los y las docentes, como así también los y las asistentes infantiles, fueron incluidos en el plan de vacunación.

El secretario de Educación y Cultura del municipio, Paulo Ricci, explicó que desde el próximo lunes “está regresando el sistema de presencialidad escalonada que venimos implementando en los jardines municipales desde el pasado 17 de febrero, cuando las actividades se vieron interrumpidas por la falta de vacunación de nuestros docentes municipales”.

En ese sentido, señaló que, desde el 7 de junio, “esto se resolvió afortunadamente con la vacunación de todo el equipo docente y de asistentes infantiles del sistema municipal de educación inicial”.

“Para nosotros es una noticia muy importante, pero además es una alegría saber que a partir de este lunes 28, con las mismas modalidades de cuidado, de alternancia y de burbujas que veníamos aplicando desde el 17 de febrero, casi 1500 niños y niñas que conforman el sistema de educación municipal en los 17 jardines de la ciudad, van a recuperar la presencialidad.

**Situación epidemiológica**

Sobre el panorama del Covid-19, Jatón dijo que “no cambió demasiado, la ocupación de camas sigue siendo muy alta; sí bajaron son algunos casos, pero todavía son muy altos y por eso tenemos que seguir muy atentos. Y como siempre decimos, el peligro no ha pasado”.

Luego agregó: “Hasta que no logremos bajar la cantidad de casos no podremos volver a la normalidad, siempre tenemos una línea aplanada de alrededor de los 200 casos, pero no estamos por debajo de eso y eso implica que las camas siguen ocupadas y no nos permite tomar nuevas medidas”.