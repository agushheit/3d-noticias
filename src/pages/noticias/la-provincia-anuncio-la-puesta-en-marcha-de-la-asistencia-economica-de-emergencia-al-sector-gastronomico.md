---
category: Estado Real
date: 2021-06-03T07:56:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/bares.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia anunció la puesta en marcha de la Asistencia Económica de Emergencia
  al Sector Gastronómico
title: La provincia anunció la puesta en marcha de la Asistencia Económica de Emergencia
  al Sector Gastronómico
entradilla: Hasta el viernes 25 de junio se podrá solicitar este aporte no reintegrable
  para acompañar a los comercios y servicios de gastronómicos afectados por las medidas
  sanitarias en el marco de la pandemia de coronavirus.

---
El Ministerio de Producción, Ciencia y Tecnología, a través de la Secretaría de Comercio Interior y Servicios, anunció este miércoles que ya está disponible en la página oficial del Gobierno de Santa Fe el formulario de solicitud de la Asistencia Económica de Emergencia al sector gastronómico. Este instrumento de ayuda complementaria será por tres meses y acompañará a la actividad a través del pago de 10 mil pesos a cada trabajador en concepto de parte del salario, con el propósito de aliviar la carga en la estructura de costos fijos de la gastronomía.

Aquellos interesados en acceder a dicha asistencia, deben dirigirse al sitio web oficial de la provincia, al botón “Coronavirus” e ingresar a la sección “Programa Asistencia Económica de Emergencia Gastronomía 2021”. También pueden acceder directamente al formulario haciendo click en el siguiente enlace: [https://servicios.santafe.gov.](https://servicios.santafe.gov. "https://servicios.santafe.gov.") Cabe destacar que la asistencia alcanza a bares, restaurantes, comedores y heladerías.

El anuncio lo realizaron los ministros de Producción, Ciencia y Tecnología, Daniel Costamagna, de Trabajo, Empleo y Seguridad Social, Juan Manuel Pusineri luego de un encuentro que mantuvieron en un bar de la ciudad de Santa Fe y en el que también participaron el secretario de Comercio Interior y Servicios, Juan Marcos Aviano, el subsecretario de Fiscalización, Facundo Osia, el presidente de Asociación Empresaria Hotelera Gastronómica de Santa Fe, Rodolfo Verde, el representante de la Cámara de Bares, Leonidas Bonaveri, el secretario General de UTHGRA, Demetrio Álvarez y el propietario de Bar el Parque, Rubén Di Siena.

Al respecto, Costamagna indicó: “Seguimos acompañando a todos los sectores afectados, como lo venimos haciendo en el último año. Hoy sumando la actividad gastronómica, a lo que ya se había implementado para el sector hotelero y a otros sectores, y que se complementan con los beneficios tarifarios e impositivos. Realmente es una inversión importante de la Provincia, de alrededor de 3000 millones de pesos”.

“Trabajamos para transitar esta circunstancia de la mejor manera y salir de ella con las empresas marchando y de pie, que es el desafío. Cuidar la salud de los santafesinos, de las empresas, comercios, servicios, industria, y de todos aquellos que hacen fuerte a la provincia”, agregó el ministro.

Por su parte Pusineri manifestó: “Siempre sostuvimos que la riqueza de la provincia tiene que ver con empresas de todos los rubros y con sus trabajadores. Apoyar la actividad es mantener el empleo y en ese sentido es que se vienen desarrollando, por parte de la provincia un esquema de asistencia combinado con nación y municipios. Somos plenamente conscientes que la mejor opción es trabajar con horarios normales pero sabemos que hoy por las restricciones sanitarias eso no es posible. Es por esto que pusimos en marcha este paliativo esperando volver lo antes posible a la actividad plena en este sector y en los demás sectores afectados”

Haciendo referencia a las características de la ayuda, Aviano detalló: “Esta asistencia se extenderá por 3 meses para el sector gastronómico y se suma a todas las demás asistencias que el Estado nacional, provincial y los municipales están disponiendo. En el caso de Nación, el Repro2 es un acompañamiento muy fuerte a las empresas gastronómicas, tanto para empleados monotributistas o un autónomo que con su familia lleva adelante su pequeño bar o comedor. Entendemos que junto con lo impositivo y lo tarifario, se está dando una respuesta frente a esta situación”.

Asimismo, el funcionario resaltó que “se trata de más de 8 mil trabajadores registrados en toda la provincia, alrededor de 3 mil locales gastronómicos y titulares de empresas que sabemos que están sosteniendo sus llaves de comercio con mucho sacrificio, por lo que este aporte es un reconocimiento a ello. Estas inversiones, junto a las de otros rubros en asistencia directa, sumarán 1.400 millones de pesos a los más de 1.100 millones que ya se invirtieron en subsidios a sectores afectados”.

**VOCES DEL SECTOR**

Al tomar la palabra, Ruben Di Siena, comentó que “la asistencia salió justo en un momento crucial, ya que nos encontramos en una situación bastante complicada. Por suerte el gobierno hizo eco de lo que estábamos solicitando desde las cámaras. Con esta ayuda esperamos seguir con nuestros negocios abiertos y así mantener nuestras fuentes de trabajo”.

Rodolfo Verde agregó: “Quiero agradecer al gobierno provincial por ser uno de los primeros en entender la situación y accionar para seguir conservando las fuentes de trabajo. Santa Fe fue un precursor, ya que, ahora Entre Ríos y San Juan están imitando las asistencias que ha otorgado nuestra provincia”.

Por último Demetrio Álvarez resaltó: “Realmente desde el primer momento nunca dudamos de salir a trabajar juntos, porque de esta nadie se salva solo. Desde el 9 de abril estamos en diálogo con Pusineri, esta es una ayuda muy importante para los trabajadores gastronómicos. Agradezco al gobernador Omar Perotti y sus ministros que fueron pragmáticos en este trabajo y supieron dar una respuesta rápida en el marco de esta pandemia”.