---
category: La Ciudad
date: 2021-01-12T09:56:15Z
thumbnail: https://assets.3dnoticias.com.ar/12121-balance-controles.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: La Municipalidad realizó un balance sobre los operativos del fin de semana
title: La Municipalidad realizó un balance sobre los operativos del fin de semana
entradilla: Desde las áreas de Salud y Deportes destacaron el acatamiento de los santafesinos
  a las medidas sanitarias. Además, confirmaron que las actuaciones continuarán todo
  el tiempo que sea necesario.

---
Funcionarios de la Municipalidad de Santa Fe realizaron este lunes, un balance de los operativos especiales concretados entre el viernes y el domingo, en la zona de la costanera y los parques del Sur y Garay. 

Las tareas fueron llevadas adelante por más de 200 agentes municipales, entre ellos, integrantes del área de Salud, el Cobem, la GSI, tránsito y Deportes. Además, se sumó personal de Salud de la provincia y agentes de Policía.

La Secretaría de Control y Convivencia Ciudadana dispuso la participación de personal de las áreas de Tránsito y Guardia de Seguridad Institucional, quienes acompañaron desde puestos fijos y recorriendo las diferentes zonas.

En los espacios mencionados se fijaron postas que concretaron test de olfato, toma de temperatura, entrega de volantes con información sobre el Covid-19 y control del uso de tapabocas. Las tareas disuasivas tuvieron como objetivo evitar las aglomeraciones y recordar las medidas de seguridad entre los asistentes.

El director de Salud municipal, César Pauloni, recordó que los controles se orientaron principalmente al cuidado de la población que se moviliza por los espacios más utilizados durante el verano. 

En el caso del área de Salud, en conjunto con otras secretarías, se llevaron adelante diferentes líneas de trabajo, tal es el caso de la higienización de manos, la entrega de casi 5 mil folletos y la realización de más de 500 test de olfato que al igual que el control de temperatura, era optativo. A ello agregó que a las diferentes acciones se sumaron rondas para recordar la importancia del uso del tapabocas y hacer hincapié en el cuidado de cada ciudadano.

«Vimos un alto acatamiento de las burbujas en diferentes puntos, principalmente en los parques. En líneas generales, la población logró adecuarse a las medidas», afirmó Pauloni. 

En ese sentido, señaló que una de las actividades consistía en explicar a las personas las medidas sanitarias a tener en cuenta cada vez que entraban o salían de su burbuja.

Respecto de la continuidad de los operativos, el director indicó que los mismos se mantendrán durante toda la temporada, intensificándose los fines de semana y con la participación de todas las áreas de la Municipalidad, en los diferentes puntos que reúnen a vecinos y vecinas, y turistas. 

Según dijo, «el objetivo está centralizado en el cuidado de las personas. La idea es reforzar el cuidado individual para cuidarnos entre todos», concluyó.

Por su parte, el subdirector de Deportes del municipio, Axel Menor, describió la tarea que tuvo a su cargo el personal del área: «más que nada hicimos concientización entre los ciudadanos para que no nos relajemos», explicó.

Entre otras cosas, los profesores de educación física estuvieron abocados a recorrer las zonas de playas y parques para organizar las burbujas y recordar a los santafesinos la importancia de respetar esos espacios. En total se colocaron cerca de 300 círculos de 5 metros de diámetro, los cuales permitieron la separación para convivir en el espacio público.

Además, especificó que Deportes tuvo a su cargo la organización de torneos de ajedrez, _beach volley_ y fútbol tenis durante el fin de semana, instancias que fueron aprovechadas por los agentes para recordar las medidas de seguridad tendientes a prevenir la transmisión del Coronavirus.