---
category: Agenda Ciudadana
date: 2021-01-26T09:33:13Z
thumbnail: https://assets.3dnoticias.com.ar/festram.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Festram
resumen: FESTRAM denuncia a la Corte Nacional ante la OIT
title: FESTRAM denuncia a la Corte Nacional ante la OIT
entradilla: Consideran que ese tribunal promueve la violación a la libertad sindical
  y a la negociación colectiva. El estado argentino adhirió a esos convenios.

---
La Federación de Sindicatos de Trabajadores Municipales de la Provincia de Santa Fe, en uso de las atribuciones que le otorga el Art. 24 del Estatuto de la OIT, resolvió promover una RECLAMACIÓN ante la Organización Internacional del Trabajo, a los fines que este Organismo basal del Derecho Internacional del Trabajo, integrante de la Naciones Unidas, intervenga en la reconsideración de un reciente fallo de la Corte Suprema de Justicia de la Nación.

En el fallo mencionado, el máximo tribunal nacional, en lugar de limitarse a resolver el tema de tratamiento, “abusa de su autoridad” y se explaya descalificando la Paritaria Municipal (Ley 9.996) y a la propia Constitución de la Provincia de Santa Fe, mencionando en sus considerandos que la Carta Magna Santafesina y la Ley de Paritaria Municipal están en “tensión” con la Autonomía Municipal y en consecuencia con el Art. 123 de la Constitución Nacional. Esta decisión pone en situación de inestabilidad laboral y salarial a miles de trabajadores del sector que hoy, además, cumplen un papel esencial en la lucha contra la Pandemia.

Sospechosamente los cortesanos santafesinos, que provienen de abajo de los zócalos de la democracia, Lorenzetti y Rossatti “exhortan” al Gobernador Perotti a “dictar las normas necesarias para el debido cumplimiento del mandato que emerge del art. 123 de la Constitución Nacional. (Autonomía municipal)”.

Lamentablemente, la primera reacción del Ejecutivo Santafesino es elaborar un Anteproyecto de Ley, para destruir el sistema de negociación colectiva entre los Municipios y Comunas de Santa Fe y sus trabajadores, ignorando hasta el momento el Convenio 144 OIT vinculado con la consulta a las Organizaciones Sindicales en el marco del Diálogo Social.

Esa decisión del Ejecutivo Provincial -de elaborar dicho ante proyecto unilateral e inconsulto- viene a desmantelar una negociación colectiva con vigencia desde el año 1987 (solo interrumpida en las gestiones neoliberales de Menem y Reutemann). No cabe duda que se quiere eliminar una Paritaria que ha garantizado durante 20 años la plena vigencia de la paz social y el equilibrio de los intereses de las prestaciones públicas de las Administraciones Locales con los derechos de los trabajadores a salarios y condiciones de trabajo dignas.

Es por esto, que FESTRAM denunciará ante la OIT la violación a la libertad sindical y la intromisión del Estado en la negociación colectiva, sin descartar recurrir a la Comisión Interamericana de Derechos Humanos si el Gobierno Provincial utiliza este artilugio para impedir  el funcionamiento de la Paritaria Municipal.

Lamentablemente, tanto la Corte como el Gobierno Provincial incurren en una acción propia de los conceptos de autonomía municipal surgidos del Consenso Washington y pretenden socavar el modelo sindical argentino, vinculado a la negociación colectiva por rama, intentando aplicar en el sector municipal la negociación por empresa, que claramente debilita la capacidad de negociación de los trabajadores.

Asimismo, si el Poder Ejecutivo y /o la Legislatura proceden a evaluar un anteproyecto de derogación de la paritaria municipal, solicitaremos desde FESTRAM el cumplimiento del Convenio y los compromisos que OIT tiene acordado con la Provincia de Santa Fe.