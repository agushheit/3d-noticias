---
category: La Ciudad
date: 2021-10-14T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/ESCRITURAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón entregó las escrituras de sus viviendas a 50 familias de la ciudad
title: Jatón entregó las escrituras de sus viviendas a 50 familias de la ciudad
entradilla: 'Se trata de unidades habitacionales ubicadas en los barrios San Lorenzo,
  12 de Octubre, Estanislao López, Nueva Esperanza, San Martín, San Pantaleón, Coronel
  Dorrego y Barranquitas. '

---
Este miércoles, durante un acto realizado en la Estación Belgrano, el intendente Emilio Jatón otorgó las escrituras traslativas de dominio a 50 familias de la capital provincial. Las unidades habitacionales se ubican en los barrios San Lorenzo, 12 de Octubre, Estanislao López, Nueva Esperanza, San Martín, San Pantaleón, Coronel Dorrego y Barranquitas, donde el municipio lleva adelante un proceso de regularización dominial.

En la oportunidad, Jatón reconoció que la entrega de títulos de propiedad “es para mí, uno de esos actos que me traspasan, que me marcan, porque sé lo que significa una escritura. Lo he vivido con mi padre al momento de ver cómo la guardaba en el lugar más preciado de la casa”, contó.

El intendente admitió que “llegar a este proceso no es fácil; es largo y son muchas las instancias que debemos atravesar”. No obstante, confirmó que el objetivo de la gestión es llegar a las mil escrituras entregadas, por lo que desde el municipio “estamos trabajando con ustedes por algo que se merecen, por algo que es un derecho y por algo que esperaron durante mucho tiempo”.

“Hoy se van a llevar la escritura, hoy la van a guardar y van a saber que es la herencia de sus hijos y que van a poder decir ‘esta casa es mía’. Este es un gran momento”, concluyó.

**Seguridad jurídica**

Desde que inició la gestión, el intendente lleva otorgados 170 títulos de propiedad a vecinos y vecinas de diferentes barrios, un proceso que continuará próximamente con 80 nuevas entregas. Cabe recordar que el trabajo de regularización dominial se realiza en forma conjunta entre la Municipalidad y los colegios de Agrimensores y de Escribanos.

En ese sentido, se informó que existen otros 250 legajos con el trámite avanzado y 630 mensuras en proceso, de los barrios Marcos Bobbio, Barranquitas Sur y Loyola, entre otros.

Al respecto, la directora del Ente Autárquico Santa Fe Hábitat, Paola Pallero, indicó que “la escrituración de lotes municipales es una política que nos fijamos desde el primer día de gestión. La pandemia no nos frenó y seguimos trabajando; por eso estamos muy contentos”, afirmó. Del mismo modo, señaló que el título de dominio “representa la seguridad de la familia, la posibilidad de dejar una vivienda a sus hijos o de animarse a hacer un arreglo porque saben que es su hogar. También contribuye al sentido de pertenencia al barrio y al espíritu de convivencia entre vecinos y vecinas”, remarcó.

La funcionaria agregó que la escrituración es “un derecho básico, junto con obras de mejoras que estamos realizando en muchos barrios de la ciudad como San Agustín, Loyola, La Ranita, Yapeyú, Atilio Rosso, Estrada, Los Hornos, 29 de Abril, Playa Norte, Varadero Sarsotti y Transporte, entre otros. Por eso también hablamos de transformación: porque son obras y acciones que cambian las condiciones de vida en cada barrio”, dijo, por último.

Por su parte, la concejala Laura Mondino felicitó “a todas las familias que hoy reciben su escritura, que es el documento más importante para pensar en el futuro; es el documento que soñamos, anhelamos y esperamos durante mucho tiempo”. Según indicó, “desde el día uno, el intendente Emilio Jatón puso como prioridad llevar adelante estos trámites, que tienen que ver con darle seguridad jurídica a los habitantes de todos los barrios”.

“Estamos muy contentos de llevar adelante está política y queremos seguir en este camino. Por eso, sabemos que aún falta entregar nuevas escrituras pero estamos trabajando para que sean más las familias que puedan recibirlas este año”, agregó Mondino.

**“Nuestro techo”**

Josefa Benitez es una de las vecinas de la capital provincial que recibió su escritura. Vive en barrio Estanislao López y cuenta que hace 40 años habita esa casa.

“Estoy muy emocionada, con mucha alegría. Hace 40 años que vivo ahí, crié seis hijos, 50 nietos y muchos biznietos, pero son tantos que ya no los puedo contar”, refiere. Visiblemente conmovida, asegura que está “muy contenta porque estuve años esperando esto. Es una alegría muy grande, un regalo para el día de la madre. Estoy muy agradecida”, reconoce.

Josefa relata que hace más de 40 años era inquilina: “yo alquilaba y había visto que se vendía un ranchito chiquito. Con mi marido, que falleció hace 12 años, lo compramos y nos fuimos. Era muy bajo, se nos inundaba y tuvimos muchos problemas para rellenar el terreno, Pero empezamos de a poquito, con una piecita, después otra y así arrancamos. Ahora ya es una humilde casita, es nuestro techo”, concluye.

**Participantes**

En el acto también estuvieron el presidente del Concejo Municipal, Leandro González, el concejal Paco Garibaldi, funcionarios municipales, representantes de entidades intermedias, referentes de organizaciones barriales, vecinos y vecinas.