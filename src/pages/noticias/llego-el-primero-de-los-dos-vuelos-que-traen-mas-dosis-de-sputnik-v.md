---
category: Agenda Ciudadana
date: 2021-03-01T07:15:04-03:00
thumbnail: https://assets.3dnoticias.com.ar/SPUTNIK.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Llegó el primero de los dos vuelos que traen más dosis de Sputnik V
title: Llegó el primero de los dos vuelos que traen más dosis de Sputnik V
entradilla: El Airbus 330-202 proveniente de Moscú arribó a las 19.53 en el Aeropuerto
  Internacional de Ezeiza.

---
El primero de los dos vuelos de Aerolíneas Argentinas que traen desde Rusia nuevas dosis de la vacuna Sputnik V contra el coronavirus arribó a las 19.53 en el Aeropuerto Internacional de Ezeiza.

El Airbus 330-200, matrícula LV-GIF, despegó desde el aeropuerto Sheremétievo a las 3.15 (9.15 hora argentina), tras haber concretado con demoras la carga de las dosis de vacuna.

Esta aeronave fue la primera en aterrizar en la capital rusa, y permaneció en suelo moscovita poco más de 15 horas, debido a complicaciones que impidieron el normal procedimiento de carga de las vacunas.

Finalmente, culminado el proceso, emprendió el regreso hacia Buenos Aires con la nominación AR1061, mientras la segunda aeronave continuaba esta mañana con la estiba de las vacunas.

El segundo vuelo, que había partido de Ezeiza el sábado a la madrugada (a la 0.29), con demoras respecto al horario previsto inicialmente debido a un problema técnico, aterrizó en Moscú a las a las 21.40 (15.40 hora argentina), luego de 15.20 horas de vuelo.

A cargo de los vuelos están 10 tripulantes, entre pilotos y copilotos, que se irán alternando al comando de la nave, a los que se suman despachantes, técnicos y personal de carga, para llegar a un total de 18 personas en cada avión.

Tal como ocurrió con viajes anteriores a Moscú, los vuelos se concretan de forma directa -sin escalas técnicas-, bajo la denominación de 'ferry', es decir, cuando la aeronave va vacía, sólo con su tripulación de cabina y técnicos de mantenimiento y de carga, según el caso.