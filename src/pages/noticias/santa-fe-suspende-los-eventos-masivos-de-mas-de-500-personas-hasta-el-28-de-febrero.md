---
category: La Ciudad
date: 2021-12-31T06:15:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/eventos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Santa Fe suspende los eventos masivos de más de 500 personas hasta el 28
  de febrero
title: Santa Fe suspende los eventos masivos de más de 500 personas hasta el 28 de
  febrero
entradilla: La restricción abarca la actividad de boliches, salones de fiestas, recitales
  y festivales, en espacios cerrados o abiertos, públicos y privados.

---
Mediante el Decreto Provincial N° 3374 publicado este 30 de diciembre, el gobierno provincial estableció suspensiones y fijó límites horarios para varias actividades, en lo que parece un intento por reducir la circulación viral y con ello, la escalada de casos de coronavirus en la bota santafesina. Como primera medida, suspendió en todo el territorio provincial y “con carácter preventivo”, desde el dictado del decreto y hasta el 28 de febrero próximo, “los eventos masivos organizados de más de 500 personas que se realicen en espacios abiertos, cerrados o al aire libre, públicos o privados”. 

 Quedan comprendidas en la suspensión de eventos masivos las actividades “con concurrencia de personas en discotecas, locales bailables, salones de fiestas, bailes, recitales, festivales o similares”, enumera el texto administrativo, que lleva la firma del gobernador Omar Perotti.

 No obstante, el decreto aclara que las autoridades municipales y comunales, previa consulta al Ministerio de Salud provincial, “podrán autorizar en sus jurisdicciones la realización de eventos con concurrencia de hasta 1.000 personas”, siempre y cuando se cumplan las condiciones requeridas en la infraestructura de las instalaciones o lugares donde se desarrollen estas actividades.

 **Horarios**

 En el artículo 6, el decreto establece que en todo el territorio provincial, habrá horarios “tope” para una batería de actividades de adquisición de bienes básicos, recreativas y productivas. Así, para el rubro del comercio mayorista y minorista de venta de mercaderías, con atención al público en los locales, esta actividad podrá extenderse todos los días de la semana hasta las 21 horas, con excepción de kioscos que podrán permanecer abiertos hasta la hora cero para la atención al público residente en su cercanía. “Esto sin perjuicio de la posibilidad de las farmacias de realizar los turnos de guardia”, se aclara. 

 Fuera del horario indicado “sólo podrán realizar actividad comercial a través de plataformas de comercio electrónico, venta telefónica y otros mecanismos que no requieran contacto personal con clientes y únicamente mediante las modalidades de entrega a domicilio o retiro”, las que deberán concretarse en el horario de 6 a las 21 horas.

 Con respecto a la actividad de los locales gastronómicos (bares, restaurantes o heladerías, con concurrencia de comensales), los días jueves, viernes, sábados y víspera de feriados, podrán abrir hasta la hora 5 del día siguiente. El resto de los días de la semana, hasta la hora 4 del día siguiente. 

 Asimismo, la actividad deportiva de deportes individuales o grupales; gimnasios, natatorios y establecimientos afines, se deberán desarrollar entre las 7 y las 24 horas; y las actividades religiosas en iglesias, templos y lugares de culto, correspondientes a la Iglesia Católica Apostólica Romana y entidades religiosas inscriptas en el Registro Nacional de Cultos, entre las 8 y las 22 horas.

 Los cines y complejos complejos cinematográficos podrán recibir asistentes entre las 10 y la hora 1 del día siguiente; en cuanto a pesca deportiva y recreativa en la modalidad desde costa y embarcados, navegación recreativa o deportiva, y las actividades de guarderías náuticas, podrán funcionar entre las 7 y las 21 horas.

 

Juegos infantiles y otros establecimientos afines, en espacios cerrados o al aire libre, comúnmente denominados “peloteros”, podrán funcionar entre las 8 y las 24 horas; con excepción de los ubicados en centros y paseos; también, la práctica de competencias deportivas será entre las 8 y las 23 horas, y la actividad en hipódromos y agencias hípicas, entre las 7 y las 20 horas.

 La actividad de los casinos y bingos, “cumplimentando las reglas generales de conducta y de prevención y los protocolos oportunamente aprobados para la actividad”, seguirá todos los días de la semana entre las 10 horas y las 5 horas del día siguiente.

 Y la actividad de los salones de eventos, fiestas y similares, para la realización de eventos sociales (con tope de 500 personas, como ya se ha dicho), será los jueves, viernes, sábados y víspera de feriados hasta las 5 horas del día siguiente, el resto de los días de la semana hasta las 4 horas del día siguiente.

 El decreto, asimismo, ratifica la exigencia de pase sanitario, a los nuevos criterios de días de  aislamiento en casos de Covid-19 confirmados, sospechosos y contactos estrechos, el refuerza el cumplimiento de las medidas preventivas. En este sentido, el barbijo o tapabocas “será obligatorio en espacios cerrados de ingreso público y al aire libre, cuando se diera la concurrencia simultánea o la cercanía de personas ajenas al grupo conviviente”.

 

 

 

 

 