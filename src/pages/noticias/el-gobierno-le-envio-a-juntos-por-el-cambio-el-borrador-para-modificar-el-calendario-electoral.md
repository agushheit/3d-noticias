---
category: Agenda Ciudadana
date: 2021-04-10T06:15:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/DOCJXC.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: El Gobierno le envió a Juntos por el Cambio el borrador para modificar el
  calendario electoral
title: El Gobierno le envió a Juntos por el Cambio el borrador para modificar el calendario
  electoral
entradilla: El documento propone trasladar las PASO al 12 de septiembre y las Generales
  al 14 de noviembre, "por única vez" y "en contexto de pandemia" de coronavirus.

---
El ministro del Interior, Eduardo de Pedro, envió a las autoridades del bloque de Juntos por el Cambio un borrador para modificar el calendario electoral "por única vez" y "en contexto de pandemia" de coronavirus, con el objetivo de que sea discutido por las distintas fuerzas políticas que integran la coalición, informaron fuentes oficiales.

El proyecto propone trasladar la elecciones Primarias, Abiertas, Simultáneas y Obligatorias (PASO) del 8 de agosto al 12 de septiembre y las Generales del 24 de octubre al 14 de noviembre, añadieron los voceros consultados por Télam.

La oficialización de listas y candidatura e inicio de campaña serán 45 días antes de la elección, el 30 de julio próximo.

**Los fundamentos**

Entre los principales fundamentos, "este aplazamiento permitirá dejar atrás el período de bajas temperaturas y mayor circulación de enfermedades respiratorias y avanzar en la campaña de vacunación", indicaron las fuentes.

Además, añadieron que "las fechas propuestas permiten respetar todos los plazos fijados por el Código Nacional Electoral, salvo dos excepciones: con el propósito de garantizar un plazo suficiente para la realización del escrutinio definitivo de las PASO, el proyecto propone reducir de 50 a 45 días previos a la elección general el plazo para que las juntas partidarias proclamen sus candidatos".

La iniciativa propone trasladar la elecciones Primarias, Abiertas, Simultáneas y Obligatorias del 8 de agosto al 12 de septiembre y las Generales del 24 de octubre al 14 de noviembre

La iniciativa propone trasladar la elecciones Primarias, Abiertas, Simultáneas y Obligatorias del 8 de agosto al 12 de septiembre y las Generales del 24 de octubre al 14 de noviembre

"En un régimen democrático representativo y republicano, la pandemia nos plantea el desafío de garantizar el pleno ejercicio de los derechos políticos individuales y colectivos y la transparencia del proceso electoral en un contexto excepcional en el que debemos evitar la aglomeración de personas y su desplazamiento en transporte público y a su vez implementar las medidas sanitarias necesarias para contener la propagación del virus", completaron.

Por otra parte, ante el planteo del bloque de Juntos por el Cambio de realizar una serie de reformas al sistema electoral, de Pedro aclaró que los cambios en la metodología son impracticables con un calendario electoral en marcha.

En ese marco, se resolvió avanzar en la integración de una comisión para que el Congreso y el Ejecutivo discutan propuestas para mejorar el sistema electoral argentino.