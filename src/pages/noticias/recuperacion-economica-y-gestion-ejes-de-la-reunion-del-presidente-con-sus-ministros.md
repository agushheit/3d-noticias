---
category: Agenda Ciudadana
date: 2021-08-21T06:15:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/REUNIONMUSEO.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Recuperación económica y gestión, ejes de la reunión del Presidente con sus
  ministros
title: Recuperación económica y gestión, ejes de la reunión del Presidente con sus
  ministros
entradilla: '"El Presidente expresó su reconocimiento a todos los ministros por la
  tarea que se viene llevando a cabo", dijo Santiago Cafiero respecto a lo que fue
  la reunión que se extendió durante aproximadamente una hora y media.'

---
El presidente Alberto Fernández encabezóeste viernes una reunión con su Gabinete de ministros en la que impartió "lineamientos de gestión y políticos", a la vez que repasó los ejes en los que se apoya la recuperación económica que comienza a vislumbrarse y las prioridades de la administración que estarán reflejadas en el presupuesto 2022.

Así lo informó el jefe de Gabinete, Santiago Cafiero, en declaraciones a la prensa tras el encuentro que se realizó en el Museo del Bicentenario de la Casa Rosada y que se extendió por aproximadamente una hora y media.

"El Presidente expresó su reconocimiento a todos los ministros por la tarea que se viene llevando a cabo", dijo Cafiero al informar sobre el contenido del encuentro, que reunió a todo el Gabinete, con la excepción de la ministra de Salud Carla Vizzotti, que se encuentra en Rusia con el objetivo de profundizar el trabajo colaborativo con el Instituto Gamaleya en relación a la provisión de vacunas Sputnik V contra el coronavirus.

"El sentido de la reunión fue dar lineamientos de gestión y políticos", dijo Cafiero, quien remarcó que esto debe hacerse en el marco del respeto de la Ley electoral, es decir cumpliendo con el cronograma que imponen las próximas elecciones, pero sin entorpecer el "buen funcionamiento de la gestión", que "viene dando pasos firmes".

Así, de cara a las primarias abiertas, simultáneas y obligatorias (PASO) del 12 de septiembre y las elecciones legislativas del 14 de noviembre, Cafiero explicó que se trató "cómo será el abordaje" de la campaña teniendo "siempre como marco normativo el respeto de la ley electoral, que marca "actividades que se pueden hacer y otras que no" y con el eje puesto en que "no se detiene la gestión, que viene dando pasos firmes".

En ese encuentro, remarcó Cafiero, se evaluaron también los programas de asistencia instrumentados el año pasado, cuando "la pandemia golpeaba del modo más crudo" y destacó que comienza a vislumbrarse una recuperación de la situación.

"La recuperación es muy visible desde varios puntos de vista", indicó, y precisó que el Estimador Mensual de Actividad Económica (EMAE) del Instituto Nacional de Estadísticas y Censos (Indec) arrojó un crecimiento de más de 11 puntos, una recuperación de 23.000 puestos de trabajo en la actividad industrial y un crecimiento del 64 por ciento de la tasa de uso de la capacidad instalada de las empresas, por encima de la del 2018.

Destacó la recuperación del consumo entre "junio y julio", a partir de los programas Ahora 12 y Ahora 30, "que están dando buen resultado" y que "ya están por encima de las cifras de 2019", cuando el actual Gobierno llegó a la Casa Rosada, lo que consideró un hecho auspicioso "ya que se empieza a vislumbrar una salida de la pandemia".

"Nos queda por recuperar cuatro años muy malos para la economía y para el país, que es una tarea y un compromiso del Presidente de llevar adelante esa recuperación", expresó al hacer referencia a lo que dejó el anterior gobierno macrista.

"Nosotros venimos a reafirmar ese compromiso, que es encender la economía, recuperar el empleo, todos los institutos y fundamentalmente las herramientas de política pública que hacen necesario a un Estado que debe recuperar un modelo de producción y de empleo", argumentó, y añadió que todo ello "se había abandonado tiempo atrás" y es "lo que se tiene que recuperar ahora".

Tras descartar cambios en el gabinete tras las elecciones, Cafiero adelantó que el presupuesto 2022 "va a expresar el volver a recuperar las prioridades, los niveles de inversión y obra pública", y "recuperar el espíritu de lo que fue el presupuesto 2021".

En ese sentido, recordó que el Presupuesto 2021 tuvo como hitos una mirada sobre género y niñez, teniendo en cuenta lo fundamental que es "tener políticas integrales en toda la gestión para abordar las problemáticas, que son bien complejas y se tienen que abordar de un modo multifacético".

Agregó que el Presupuesto 2022 va a apuntar a "seguir ampliando la capacidad en cuanto a la infraestructura y la obra pública", con "una fuerte inversión" .

Recordó que en 2021. con lo invertido "no sólo se recuperaron más de 1.800 obras públicas sino también vivienda".

"En dos años y con la pandemia de por medio llevamos entregadas una mayor cantidad de viviendas" que en los cuatro años del gobierno de Cambiemos, hoy Juntos por el Cambio o Juntos, puntualizó Cafiero.

Explicó que fue posible "porque hubo voluntad política del Presidente de definir que haya un ministerio específico que se ocupe de esa tarea y de avanzar con los presupuestos necesarios para que esas áreas puedan desarrollar y desplegar toda su agenda".

Además, recordó que hasta diciembre está en vigencia la ley de emergencia económica para contemplar los efectos sobre los sectores que más sufrieron las consecuencias de la pandemia por coranvirus.

En cuanto a la causa por el cumpleaños que celebró la primera dama Fabiola Yáñez el 14 de julio de 2020 en la Residencia de Olivos, Cafiero reiteró que el Presidente "está a disposición de la justicia", pero aclaró que "todavía no tiene ninguna citación".

"Siempre va a estar dispuesto a responder a la justicia, como siempre lo ha hecho, como un hombre de derecho", manifestó el jefe de Gabinete, y respondió: "Con respecto de Olivos, el Presidente ya explicó y ya quedó detallado, así que nosotros no encontramos más temas para agregar".

Sobre la posibilidad de cambios en la seguridad en la Quinta de Olivos admitió que "permanentemente se está revisando la seguridad del Presidente y de la Quinta de Olivos, que es la residencia" del mandatario, para que "tenga todas las medidas de cuidado y que tenga naturalmente por delante perspectivas de que si ha habido un descuido se revise".

Pero agregó que "no" es por "la cuestión puntual del hecho público del cumpleaños de la primera dama "sino porque la seguridad del Presidente es esencial para todos los argentinos y argentinas", concluyó el jefe de Gabinete.

Participaron de la reunión de Gabinete encabezada por el Presidente, y con Cafiero en la mesa principal; los ministros de las Mujeres, Géneros y Diversidad de Argentina, Elizabeth Gómez Alcorta; de Desarrollo Social, Juan Zabaleta; de Desarrollo Territorial y Hábitat, Jorge Ferraresi; de Desarrollo Productivo, Matías Kulfas; de Trabajo, Claudio Moroni; de Turismo y Deportes, Matías Lammens; de Ciencia, Tecnología e Innovación, Roberto Salvarezza; de Educación; Nicolás Trotta; de Agricultura, Luis Basterra; de Justicia; Martín Soria; de Defensa, Jorge Taiana; de Obras Públicas; Gabriel Katopodis; de Economía, Martín Guzmán; de Seguridad, Sabina Frederic; del Interior, Eduardo de Pedro; de Relaciones Exteriores, Comercio Internacional y Culto; de Medio Ambiente, Juan Cabandié; de Cultura, Tristán Bauer; y de Transporte, Alexis Guerrera.

También asistieron el secretario General de la Presidencia, Julio Vitobello; de Asuntos Estratégicos, Gustavo Beliz; y de Prensa y Comunicación, Juan Pablo Biondi; y la vicejefa de Gabinete, Cecilia Todesca.