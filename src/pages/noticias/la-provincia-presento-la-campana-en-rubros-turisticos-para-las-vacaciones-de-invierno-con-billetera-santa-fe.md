---
category: Estado Real
date: 2021-07-14T08:25:53-03:00
thumbnail: https://assets.3dnoticias.com.ar/turismo.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: La provincia presentó la campaña en rubros turísticos para las vacaciones
  de invierno con Billetera Santa Fe
title: La provincia presentó la campaña en rubros turísticos para las vacaciones de
  invierno con Billetera Santa Fe
entradilla: Los sectores del rubro turístico tendrán los beneficios del Programa durante
  todo el período. Además se comentaron los avances de una plataforma digital para
  promocionar la oferta turística santafesina.

---
El ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, encabezó este martes, en el Salón Walsh de la sede de Gobierno en Rosario, la presentación de la campaña y la promoción en rubros turísticos para las vacaciones de invierno con Billetera Santa Fe y los avances de una plataforma para comercializar la oferta turística santafesina. Estuvo acompañado por los secretarios de Turismo, Alejandro Grandinetti y de Comercio y Servicios, Juan Marcos Aviano y de la actividad participaron también representantes de Plus Pagos y del sector turístico santafesino.

Durante la presentación, Costamagma expresó: “La iniciativa prevé la utilización de una herramienta como Billetera Santa Fe para el apoyo y acompañamiento a todos los turistas santafesinos que pasen las vacaciones de invierno en nuestra provincia. Durante estos 15 días podrán acceder a todos los beneficios que el Programa tiene para los santafesinos, y de esta forma continuar acompañando a los sectores vinculados a la hotelería, alojamientos, gastronomía y todo lo que tiene que ver con la estructura turística de nuestro territorio. Es una fuerte apuesta a esta industria que, en Santa Fe, cada día presenta más y mejores alternativas para visitar”.

A su turno el Secretario de Turismo destacó: “Con la firme decisión del gobernador Omar Perotti pusimos la meta de generar más empleo y más arraigo, de allí las herramientas que pusimos en funcionamiento, como Billetera Santa Fe, Boleto Educativo Gratuito y, en particular, con el turismo receptivo, estamos convencidos que debemos generar mayor actividad económica. En ese marco innovamos en la forma de comercializar: identificamos que teníamos una gran deuda y avanzamos en una herramienta que aún no existe en otras provincia, un esfuerzo conjunto para que la provincia ponga al servicio de los prestadores turísticos la posibilidad de vender y comercializar desde una plataforma digital”.

“Esta tecnología es hecha en Santa Fe, por santafesinos y va a estar disponible a partir del 30 de agosto. Estamos trabajando en incorporar más prestadores en esta herramienta, que nos permitirá tener nuevos emprendedores turísticos. A diferencia de otras plataformas, estamos desarrollando una herramienta que nivelará la cancha, ya que los pequeños y medianos empresarios podrán mostrar sus servicios. A partir de capacitaciones, de la oferta en esta plataforma y  generando infraestructura tecnológica, podrán acceder a más potenciales clientes de forma gratuita, en todo el país y el mundo”, remarcó Grandinetti.

Respecto al sector, Grandinetti aseguró: “Nos estamos posicionando muy bien como destino, según datos del Ministerio de Turismo de la Nación estábamos entre los diez primeros destinos de la Argentina, en noveno lugar; y un buscador como Google tiene a la provincia de Santa Fe en el sexto lugar entre los destinos turísticos más buscados. Son indicadores que dan cuenta que hemos logrado, luego de una promoción muy fuerte que arrancamos en 2020, instalar a la provincia como un lugar a visitar. Ahora la tarea es darle contenido, mejorar las prestaciones, consolidar a los que tenemos y generar nuevos emprendimientos”.

Por último, Aviano resaltó el funcionamiento de Billetera Santa Fe: “Estamos en el orden de los 3.000 millones de pesos en reintegros, que mueven la economía y el comercio local. En Rosario recién se está comenzando a notar, pero en la ciudad de Santa Fe, el centro comercial ya marca estudios que, el incremento de la actividad económica, se da a partir de Billetera Santa Fe. El principal objetivo del Programa es mover la economía, fundamentalmente en el sector comercial que está muy golpeado”.