---
category: La Ciudad
date: 2021-07-10T06:15:46-03:00
thumbnail: https://assets.3dnoticias.com.ar/SUAREZ.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Carlos Suárez: "Somos la mejor opción de Juntos por el Cambio para la ciudad"'
title: 'Carlos Suárez: "Somos la mejor opción de Juntos por el Cambio para la ciudad"'
entradilla: 'El actual concejal de UCR-Juntos por el Cambio, buscará renovar su banca
  en las próximas elecciones. '

---
“Queremos seguir trabajando en lo que importa; y para nosotros lo que importa es lo que nos transmiten los vecinos y las instituciones de la ciudad. Esto se ve reflejado en quienes conforman nuestra lista, personas convencidas en que la única forma de recuperarnos es mirar hacia adelante junto a los santafesinos”, afirmó.

Carlos Suárez será candidato a concejal en Juntos por el Cambio. “Santa Fe y los santafesinos debemos repararnos mirando hacia el futuro, construyendo una ciudad que equilibre las miradas y las percepciones de todos. Esto se puede ver en la lista que conformamos para las próximas elecciones y por eso estamos convencidos que somos la mejor opción en Juntos por el Cambio”, indicó.

El actual concejal de UCR-Juntos por el Cambio buscará renovar su lugar en el Concejo Municipal. “Durante la pandemia estuvimos acompañando y trabajando junto a quien más lo necesitaba: el comerciante, el gastronómico, el emprendedor, el transportista, las vecinales, los clubes. Queremos seguir trabajando junto con ellos y las instituciones en lo que importa para los santafesinos y para que la ciudad vuelva a avanzar”, sostuvo el legislador.

**Pluralidad y experiencia**

Con respecto a los integrantes de la lista que encabeza Carlos Suárez, el candidato a concejal de Juntos por el Cambio afirmó: “Nos acompañan personas que son verdaderos referentes en la tarea que realizan, pero además militantes de los partidos que conforman Juntos por el Cambio”.

En este sentido, detalló que el segundo lugar en la lista lo ocupa Florencia Maresca, empresaria del sector turístico y militante del PRO. En tanto, en el tercer lugar se encuentra Sergio Acosta, docente y director de una escuela pública d e la ciudad y militante barrial.

“En el cuarto lugar de la lista se encuentra la psicóloga Carla Korol, con quien ya venimos trabajando en colaborar a reparar el daño que la pandemia nos dejó”, agregó Carlos Suárez. Asimismo, Francisco Ramón y Soledad Santillán, dos jóvenes de la UCR, también conforman la lista; junto a Carlos “Cucho” Ferrero, militante del PRO y referente de básquet en la ciudad; y Nancy Jorge, vecina de barrio El Pozo, comprometida con las instituciones del barrio y el trabajo en seguridad.