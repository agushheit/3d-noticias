---
category: Agenda Ciudadana
date: 2021-03-22T06:15:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/segunda-ola.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Las cinco medidas que prepara el gobierno para enfrentar la nueva ola de
  covid
title: Las cinco medidas que prepara el gobierno para enfrentar la nueva ola de covid
entradilla: De acuerdo con informaciones periodísticas surgidas del seno del gabinete,
  Alberto Fernández firmaría el martes un decreto de necesidad y urgencia en el que
  fijará una serie de nuevas restricciones.

---
El presidente Alberto Fernández firmaría el martes próximo un decreto de Necesidad y Urgencia en el que fijaría una serie de nuevas restricciones para afrontar las consecuencias de una nueva ola de coronavirus.

Según trascendió, el mandatario con una nueva estrategia sanitaria para aplacar los efectos de la segunda ola del coronavirus.

En concreto, se apuntará a desalentar el turismo, reducir la frecuencia de los vuelos al exterior, controlar con rigurosidad a los argentinos que hacen cuarentena al regresar y además minimizar los cruces informales entre las fronteras con Brasil y Bolivia.

 Gendarmería Nacional y las fuerzas de seguridad provinciales controlarán el flujo informal en las fronteras de ambos países.