---
category: La Ciudad
date: 2021-04-22T07:41:49-03:00
thumbnail: https://assets.3dnoticias.com.ar/cancha.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Robo y destrozos en un fútbol 5
title: Robo y destrozos en un fútbol 5
entradilla: Por la madrugada del miércoles, delincuentes rompieron la puerta de ingreso
  y se llevaron varios elementos del lugar ubicado en Sarmiento al 5500.

---
Este miércoles por la madrugada, delincuentes ingresaron a robar a un fútbol 5 ubicado en Sarmiento al 5500. Provocaron destrozos y se llevaron varios elementos.

Según el relato de Rubén que es dueño del lugar, alrededor de las 2 de la mañana sintió ruidos “creíamos que eran golpes de los gatos que saltan por los tinglados”. En realidad, se trataba de delincuentes que estaban barreteando las puertas para entrar.

Los ladrones rompieron la puerta de un depósito y se llevaron computadoras, la caja que manejan a diario en la cancha, pelotas. Como dato insólito, también robaron un tupper que les dan a los jugadores cuando llevan para comer un asado o picada, junto con los platos y cubiertos que también les proveen en el lugar.  

Incluso, arrancaron un televisor que estaba en la pared. Esto, sumado a los daños que provocaron los malvivientes para entrar y que ahora piensan en reforzar y poner candados.

Además, Rubén comentó que tuvieron hechos menores, pero es la primera vez que sufren un robo de estas características. “Muchas veces nos ha robado vasos los que juegan o cosas del mostrador, pero algo así no nos había pasado nunca”.