---
category: Agenda Ciudadana
date: 2020-12-12T12:32:19Z
thumbnail: https://assets.3dnoticias.com.ar/conferencia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'OPERATIVO HISTÓRICO: La provincia de Santa Fe se prepara para recibir la
  vacuna contra el Covid-19'
title: 'OPERATIVO HISTÓRICO: La provincia de Santa Fe se prepara para recibir la vacuna
  contra el Covid-19'
entradilla: La ministra de Salud, Sonia Martorano, y el secretario de Equidad de Nación,
  Martín Sabignoso, ultiman detalles para una vacunación “masiva, simultánea y eficaz”.

---
Este viernes, funcionarios y funcionarias provinciales recibieron a autoridades nacionales con el objetivo de avanzar en el operativo de vacunación histórico contra el Covid-19 que se avecina en el país.

**La distribución, indicaron, será “federal” e “inmediata” a partir del arribo de las dosis que, estimaron, se producirá entre los últimos días de diciembre y los primeros de enero.**

En ese marco, la ministra de Salud, Sonia Martorano, brindó detalles junto al secretario de Equidad de la Nación, Martín Sabignoso.

“Estamos trabajando toda la logística de la vacunación, algo que es muy importante a nivel mundial. Necesitamos vacunar mucha gente en simultáneo y en poco tiempo”, expresó Martorano, y sumó: “La provincia viene trabajando en un comité interministerial, ya que se requiere de muchas miradas: Salud, Seguridad, Desarrollo Social y Ambiente. Todos colaborando para que esta logística se desarrolle del modo más correcto posible”.

Y agregó: “Hoy recibimos al secretario de Equidad para recibir sugerencias y la mirada federal que nos interesa desde el gobierno nacional, algo que se planteó desde el inicio de la pandemia y que no se veía hacía mucho tiempo”.

Por su parte, Sabignoso destacó que “la provincia ha trabajado de manera ejemplar la identificación de estos residentes desde el inicio de la pandemia y ha tenido un abordaje especial, por lo que ha podido minimizar lo más posible los contagios allí”.

En ese sentido, dijo que “también estamos trabajando en la identificación y priorización de las personas que tienen alguna patología de base, son grupos con los cuales se está trabajando para convocarlos y planificar la vacunación en lugares accesibles y seguros”.

<br/>

## **ALMACENAMIENTO DE LAS DOSIS**

Por otro lado, el funcionario nacional explicó que “**hay una logística que forma parte de la cadena de frío, porque se necesitan menos de 20 grados, para lo cual adquirimos tecnología apta para este fin**. Por eso decimos que a la masividad y a la simultaneidad hay que agregarle la logística del frío. Santa Fe tiene una industria de refrigeración muy importante, por lo que nos va a permitir acceder a todo lo que necesitamos”.

<br/>

## **ADULTOS MAYORES**

Asimismo, Martorano señaló que “en el mundo se prioriza como el personal de salud y aquellos que están en la primera línea. También las personas que residen en geriátricos, hemos trabajado muchísimo con estas personas, que están priorizadas y nominalizadas, iremos hacia ellos, comenzando por los lugares que alojen a más de 90 personas, luego a los que son de 30 a 60 y posteriormente en los que permanecen menos de 30 personas”.

<br/>

## **60 MILLONES DE VACUNAS**

En otro orden, Sabignoso resaltó el carácter histórico de la campaña: “Todos los años se administran 40 millones de dosis, ahora estamos planificando 60 millones de dosis para argentinos y argentinas en un período de un semestre. Por eso es tan importante trabajar previamente, tener un diagnóstico preciso sobre la población de riesgo en la provincia, sobre las capacidades que queremos implementar, los registradores, la refrigeración, entre otros puntos”.

En esa línea, la ministra añadió: “La provincia está trabajando muy bien, y en pocos días, cuando lleguen las vacunas, vamos a poder aplicarlas”.

<br/>

## **ESPACIOS ADECUADOS**

Consultada sobre los lugares en los cuales se va a realizar el operativo de vacunación, Martorano manifestó que “se están evaluando todas las posibilidades, porque en esta época estival las temperaturas son altas, y si bien se buscan espacios abiertos, quizás para este tipo de población hoy no sea lo más recomendable; por ello los estudios que estamos realizando”.

<br/>

## **DISTRIBUCIÓN FEDERAL**

En conferencia de prensa, el secretario de Equidad se refirió también a los criterios de distribución y explicó que “ayer hubo un anuncio muy importante del presidente Alberto Fernández y el ministro de Salud, Ginés González García, acerca del contrato. En este caso, **con la Federación Rusa, que nos garantiza 20 millones de dosis, con lo cual vamos a poder vacunar a 10 millones de personas (son dos dosis por cada una)**.

Y sumó: “Inicialmente estaremos recibiendo a fines de diciembre y principios de enero las primeras dosis y se comenzarán a aplicar inmediatamente en todo el país porque la distribución se realizará con un criterio de equidad con todas las provincias, tal como hemos trabajado durante toda la pandemia con el objetivo de que la Argentina sea cada vez más federal y que el país sea un potenciador de las decisiones que toman las provincias en beneficio de la gente”.

<br/>

## **LA PANDEMIA CONTINÚA**

A su vez, Sabignoso hizo especial hincapié en “algo que destaca siempre el ministro Ginés González García, y es que la pandemia no ha pasado. Por supuesto que la vacuna nos va a permitir ir saliendo de esta situación; en principio nos va a permitir disminuir mortalidad y posteriormente nos ayudará a generar inmunidad en la población y bajar el número de contagios, pero la posibilidad de una segunda ola, como estamos viendo en Europa, es por lo que tenemos que seguir cuidándonos entre todos, respetando las pautas de higiene, las medidas de distanciamiento. Se aproximan las fiestas y todos queremos estar reunidos con nuestros afectos, pero queremos ser muy cuidadosos. La mejor manera de compartir es esa”.

<br/>

## **VACUNAS SEGURAS**

Finalmente, consultado sobre las dudas que se plantean ante la denominada vacuna rusa, el funcionario nacional afirmó que “las vacunas que comencemos a aplicar en el país van a cumplir con los requerimientos y evaluaciones de los estándares nacionales e internacionales que garanticen la seguridad y la efectividad de la vacuna para proteger la salud de los argentinos y argentinas”