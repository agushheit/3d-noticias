---
category: La Ciudad
date: 2021-03-01T07:15:50-03:00
thumbnail: https://assets.3dnoticias.com.ar/puerto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se cargaron más de 4800 toneladas de cereales en el Puerto de Santa Fe
title: Se cargaron más de 4800 toneladas de cereales en el Puerto de Santa Fe
entradilla: La actividad productiva en el Puerto Santa Fe viene creciendo de manera
  exponencial. Este fin de semana se completó la carga de un convoy de 3 barcazas.

---
En el puerto de Santa Fe, los silos están operando casi al límite de su capacidad, almacenando trigo, maíz, sorgo y soja, que llega en camiones a la terminal portuaria de agrograneles. Con casi 30 embarques concretados y 80.000 toneladas exportadas sólo por agua, desde el Puerto ya se piensa en nuevas alternativas.

"El crecimiento de la actividad y la demanda propia del servicio nos impulsó a buscar alternativas y este tipo de embarcaciones nos permite aumentar el volumen total de carga en cada una de las recaladas. Optimizando el trabajo y mejorando costos para los productores y exportadores", aseguró el presidente del Entre Administrador del Puerto de Santa Fe, Carlos Arese.

**Puerto Productivo**

El Puerto trabaja con exportaciones de pymes de la región, que encuentran en Santa Fe una puerta para enviar sus productos al mundo: "Nos gusta decir que somos el puerto productivo de las pymes, porque le damos oportunidades a los pequeños productores de trabajar de forma directa sin intermediarios", finalizó.

**Articulación con Trabajo de la Nación**

El ministro de Trabajo, Empleo y Seguridad Social de Santa Fe, Juan Manuel Pusineri, recibió al subsecretario de Articulación Territorial de Nación, Gerardo Girón, en el Parque Industrial Sauce Viejo, donde visitaron distintas instalaciones y conversaron con referentes de organizaciones empresariales para interiorizarse acerca de distintos aspectos de la realidad productiva de Santa Fe.

Al respecto, Pusineri destacó que "estamos viendo una recuperación de la actividad y el empleo muy importante. Nuestra obligación es acompañar y facilitar ese proceso con los programas de capacitación, de inserción laboral para jóvenes y de género, trabajando en conjunto con la Nación, los municipios, los empresarios y las organizaciones sindicales".

En el encuentro también participaron el secretario de Coordinación y Gestión del MTEySS, Juan Gonzalez Utges; el presidente del Centro Comercial, Industrial y Agropecuario de Sauce Viejo, Julio Cauzzo; y el presidente Comunal, Pedro Uliambre.