---
category: Estado Real
date: 2021-05-27T08:42:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/hotel-cerradojpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: ''
resumen: Continúa la inscripción a la asistencia económica para trabajadores del sector
  Hotelero
title: Continúa la inscripción a la asistencia económica para trabajadores del sector
  Hotelero
entradilla: Hasta el 15 de junio hay tiempo para solicitar los beneficios que brinda
  el programa que busca sostener los puestos de trabajo y las unidades económicas
  del sector.

---
El gobierno provincial, a través de la Secretaría de Turismo del Ministerio de Producción, Ciencia y Tecnología, recuerda que se encuentra abierta la inscripción para acceder a los beneficios del Programa de Asistencia Económica para Trabajadores del sector Hotelero, iniciativa generada a partir del decreto 448/21 del gobernador Omar Perotti que tiene como objetivo preservar los puestos de trabajo y las unidades económicas abocadas al desarrollo de la cadena turística.

A partir del programa, se otorgarán aportes no reintegrables de 10 mil pesos mensuales por trabajador por 6 meses a personas que desarrollen tareas en el sector hotelero local y se encuentren debidamente registrados mediante el formulario N° 931 de la AFIP. Para la instrumentación de la ayuda se prevé una inversión total de 169.980.000 de pesos.

Al respecto, el secretario de Turismo, Alejandro Grandinetti, indicó: “Esta asistencia se suma a las ya existentes para apoyar al sector empresarial en el mantenimiento de los empleos, que de otra forma corren riesgo de perderse, por ser considerado un sector crítico ante la segunda ola de Covid-19”.

El funcionario destacó que “esta asistencia tiene una aplicación e implementación similar al Repro 2, herramienta que ya fue utilizada por el sector. Además, es muy importante resaltar que la asistencia provincial no es excluyente del aporte nacional”.

Para solicitar el beneficio, la gestión debe ser realizada por el empleador a través de un formulario de inscripción online en el sitio web oficial del gobierno provincial. Para acceder al mismo ingresar al siguiente enlace:[ www.santafe.gob.ar/ms/covid19/programa-asistencia-economica-de-emergencia-turismo/]( www.santafe.gob.ar/ms/covid19/programa-asistencia-economica-de-emergencia-turismo/)

Se recuerda además que el plazo límite de presentación de las solicitudes es el 15 de junio del 2021, inclusive.