---
category: Estado Real
date: 2021-07-21T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/venado-tuerto.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Perotti en Venado Tuerto: “priorizamos el trabajo y la educación por sobre
  cualquier otra acción”'
title: 'Perotti en Venado Tuerto: “priorizamos el trabajo y la educación por sobre
  cualquier otra acción”'
entradilla: El gobernador encabezó este martes en esa ciudad, el acto de apertura
  de sobres para la construcción de la Escuela de Educación Secundaria Orientada N°238,
  que no cuenta con edificio propio.

---
El gobernador Omar Perotti encabezó este martes la apertura de sobres para la construcción del edificio de la Escuela de Educación Secundaria Orientada N°238 Ricardo Torres Blanco, de Venado Tuerto, que cuenta con un presupuesto oficial de $69.722.756 y un plazo de 365 días.

La institución tiene 56 años de vida y no posee edificio propio. En el 2004 se comenzó a trabajar en dicho proyecto y en marzo de 2015 se dio inicio a la edificación, pero en diciembre de ese mismo año las tareas se discontinuaron y quedó concretada sólo en un 40 por ciento.

“Siempre que uno está en una licitación o la inauguración de un edificio tiene valoraciones y consideraciones importantes, pero las escuelas son diferentes; un edificio escolar tiene una connotación distinta, es donde se materializa, para muchos, el compromiso directo para alentar a quienes desarrollan la magnífica tarea de formar educando y a quienes asisten”, aseguró el mandatario santafesino.

En tanto, sobre la suspensión de las obras, Perotti señaló que “cuesta entender que en la Argentina, teniendo recursos, paremos obras. Porque hemos tenido momentos en los que la situación económica llevó a que se pare todo tipo de actividad, pero que se paralicen obras de escuelas es lo que más cuesta entender, porque allí está la prioridad, sobre todo, en algunos establecimientos que no tienen su edificio”.

En ese sentido, el gobernador garantizó que “cada obra que ponemos en marcha, en la que se firma un convenio que necesita recursos, lo hacemos cuando tenemos la certeza de que esos recursos están. Nos gusta hablar menos; muchos relatan, cuentan gestiones, pero es mejor hacer. Es la mejor forma de volver a establecer un esquema de confianza. Nosotros hacemos, vamos a ser los garantes de que esta obra se haga sí o sí”, dijo el gobernador.

“La educación es una prioridad para nosotros, y por eso planteamos que todos los chicos y las chicas deben estar en la escuela”, subrayó Perotti, quien explicó que la pandemia de Covid-19 “generó incertidumbres, desafíos y realidades muy diversas en toda la provincia, donde hubo establecimientos con buena infraestructura y, por otro lado, docentes recorriendo barrios, campos o trabajando con radios comunitarias”, recordó.

Más adelante, el gobernador planteó que “sin dudas no fue lo que deseábamos, pero tengo que reconocer el enorme esfuerzo que se hizo desde el Ministerio y desde cada una de las escuelas, los docentes y directivos, en tratar de mantener ese vínculo activo. Vamos a volver después de las vacaciones con todos los niveles en toda la provincia, con todos los cuidados, que tan bien han funcionado y organizado en las escuelas”.

**TRABAJO Y EDUCACIÓN**

En otro tramo de su discurso, el gobernador dijo que la “prioridad es que los chicos vuelvan a la escuela”, y sentenció que “lo que queremos dejar en claro es que priorizamos el trabajo y la educación por sobre cualquier otra acción, y eso es lo que queremos transmitirles, porque además de volver a clases, seguramente la ministra Adriana Cantero y todo su equipo, en esta semana van a estar planteando que también vamos a ir incrementando la cantidad de tiempo para dedicarle a cada uno de nuestros alumnos y alumnas”.

Finalmente, expresó que “necesitamos hacer obras que le cambien la vida a la gente en el día a día. Seguramente en este barrio va a haber muchos agradecidos. En ese convencimiento es que estamos aquí, para dar la certeza de que, por esta forma, o la que sea, esta obra se va a terminar”.

**DETALLES DE LA OBRA Y OFERTAS**

La obra cuenta con un presupuesto oficial de $69.722.756, una superficie de 1.551 metros cuadrados, y hall de ingreso, SUM, depósito, cantina, centro de recursos multimediales, siete aulas, taller, laboratorio, galería, grupo sanitario, preceptoría, dirección, vicedirección, secretaría y sala de reuniones.

Para ejecutar estas tareas se presentaron dos ofertas: la primera, de la empresa Coirini SA, que cotizó $135.412.771; y otra de la firma Adobe Construcciones SRL, que ofertó $ 94.474.335.

**CHICOS Y CHICAS EN LA ESCUELA**

Previamente, el secretario de Integración y Fortalecimiento Institucional, José Freyre, recordó que “durante muchos años y sobre todo cuando se acercaban los procesos electorales, siempre aparecía alguien que prometía reiniciar la obra, terminarla, hacerse cargo. Y la verdad que todo ese esfuerzo de directivos, docentes, de todos aquellos que gestionábamos y soñábamos con poder concretar ese edificio propio se veía frustrado”.

Y agregó que “cuando el gobernador asumió, una de las primeras acciones que lanzó fue que todos los chicos y las chicas debían estar en la escuela, tomando la educación como un rol central, junto a la ministra, con todos los intendentes y presidentes comunales trabajando para que todos nuestros pibes y pibas estén en las escuelas”, dijo Freyre.

En tanto, el intendente local, Leonel Chiarella, agradeció al gobernador “por estar nuevamente en nuestra ciudad. Para nosotros es importante recibirlo, que pueda venir seguido a Venado Tuerto y que cada vez que venga sea para dar buenas noticias y para abrir sobres de licitación. Eso es importante para la ciudad y para cada una de las obras que se llevan adelante”.

Y por último indicó que “en este caso, con una obra muy importante que tiene que ver con la educación como meses atrás fue la apertura y la puesta en funcionamiento de las obras en el Colegio Industrial. Ahora con una nueva iniciativa que tiene atrás de cada sobre que se va a abrir hoy el esfuerzo, el compromiso, el seguimiento, la paciencia de muchas de ellos: docentes, directivos, cooperadores, que son quienes son los verdaderos protagonistas”.

**EDIFICIO PROPIO**

La Escuela de Educación Secundaria Orientada N°238 Dr. Ricardo Torres Blanco fue creada en 1964 por la necesidad de que los jóvenes trabajadores pudieran terminar sus estudios secundarios en horario vespertino. Con el tiempo se fue extendiendo a alumnos que por muchas razones no lograban insertarse en otras escuelas, y también para aquellos que son sostén de familia, o madres y padres adolescentes, y para niños del barrio que terminaban séptimo grado.

La escuela cuenta con un edificio sede en la Escuela Primaria N°582, con una matrícula de 172 alumnos y un edificio Anexo en la Escuela Primaria N°1.325, con 132 alumnos.

En el año 2004 se comenzó a trabajar en el proyecto de un edificio propio. Fue incluida en el Programa “700 Escuelas” en 2006, y al caer este programa ingresó al “Más Escuelas”. Se continuaron las gestiones y en 2014 se produjo la licitación y apertura de sobres.

Comenzaron las obras en marzo de 2015, pero en diciembre de ese mismo año el Gobierno Nacional ya no envío fondos, por lo que la empresa constructora se retiró, quedando la obra en un 40 por ciento de construcción.

**RECORRIDA Y AUTORIDADES PRESENTES**

En el marco de su visita a Venado Tuerto, el gobernador recorrió también el Hospital Nodal Dr. Alejandro Gutiérrez, y las obras de desagües urbanos, constituida por varios tramos emisarios pertenecientes a distintas cuencas, según la topografía del lugar y los proyectos de pavimentos existentes y proyectados. A partir de esto es que la traza de los emisarios coincide en gran medida con la de los canales a cielo abierto existente.

De las diferentes actividades participaron, también, la secretaria de Gestión Territorial Educativa, Rosario Cristiani; el delegado de la Región VII, Sebastián Roma; y el diputado nacional Esteban Bogdanich, entre otros.