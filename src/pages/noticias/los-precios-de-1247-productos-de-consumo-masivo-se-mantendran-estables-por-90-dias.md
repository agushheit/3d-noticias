---
category: Agenda Ciudadana
date: 2021-10-14T06:15:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/precios.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Los precios de 1247 productos de consumo masivo se mantendrán estables por
  90 días
title: Los precios de 1247 productos de consumo masivo se mantendrán estables por
  90 días
entradilla: Son productos de los rubros de alimentación, higiene y limpieza, y otros,
  que deberán retrotraer sus precios al 1 de octubre y mantenerlos inalterados hasta
  el 7 de enero.

---
El flamante titular de la Secretaría de Comercio Interior, Roberto Feletti, anunció este miércoles un acuerdo con principales empresas de consumo masivo y cadenas de supermercados para mantener los precios estables de 1.247 productos de consumo masivo -que incluyen los del programa Precios Cuidados- durante 90 días, hasta el 7 de enero de 2022.  
  
Esta tarde, en un encuentro que mantuvo con la prensa, Feletti señaló que el acuerdo de precios incluye a productores, abastecedores, cadenas comercializadoras de supermercados, mayoristas y minoristas, e implica "retrotraer los precios de más de 1247 productos de consumo masivo al 1 de octubre por 90 días, hasta el 7 de enero del año próximo".  
  
Para ello, cada una de las empresas que participaron del acuerdo deberán enviar durante la jornada los listados de precios firmados por su apoderado legal.  
  
El paso siguiente contempla la publicación de los mismos este jueves, de manera que los consumidores puedan contar con la información sobre los 1247 productos de consumo masivo que integran el acuerdo, en los que se encuentran rubros como alimentación, higiene y limpieza.  
  
Este listado incluirá también a los productos que componen el programa Precios Cuidados, por lo que los valores vigentes en la actualidad seguirán corriendo hasta el 7 de enero próximo, informó el funcionario.  
Feletti sostuvo que la Secretaría de Comercio Interior tiene como objetivo ajustar "la política de ingresos del Gobierno con la política de precios. El indicador que vamos a usar para definir esta situación tiene que ver con la correlación del impacto de la canasta básica alimentaria en el salario de un trabajador formal".  
  
"Esa relación, cuando inició nuestro Gobierno, era del 9% y hoy es del 11%, es decir, que a pesar de que tuvieron paritarias, ésta no alcanzó a compensar el precio de los alimentos en el poder adquisitivo del salario", puntualizó el funcionario.  
  
Durante el encuentro con los empresarios, Feletti también presentó a los flamantes subsecretarios Débora Giorgi y Antonio Mezmezián, quienes se desempeñarán al frente de las subsecretarías de Políticas para el Mercado Interno y Acciones para la Defensa de las y los consumidores, que están en la órbita de Comercio Interior.  
  
Desde la cartera de Comercio Interior informaron que el objetivo de los encuentros fue el de iniciar el "canal de comunicación" con el sector privado y explicar los lineamientos con los que va a trabajar el equipo de la Secretaría en esta nueva gestión.  
  
En ese marco, Feletti aseguró que se busca "sellar un acuerdo para darle consistencia a la política de ingresos del Gobierno Nacional y aumentar el poder de compra de las y los argentinos. Queremos compatibilizar sus márgenes de ganancia y planes de negocio con la expansión del consumo por cantidad y no por precios".  
  
"Necesitamos parar la pelota para que los alimentos no sigan limando a los salarios", agregó, para luego transmitir su invitación a "trabajar en conjunto para que las y los argentinos puedan celebrar las fiestas en familia, después de un año en donde no se pudo cumplir con la tradición".  
  
La convocatoria también tuvo entre sus ejes la necesidad de plantear lineamientos destinados a cuidar el acceso a los productos de la canasta básica a los consumidores, ampliar los mercados y dialogar acerca de los programas de la Secretaría, la Ley de Góndolas, además de fortalecer el monitoreo de las iniciativas.

![Se trabaja junto con empresarios y supermercadistas en expandir el listado de productos incluidos en Precios Cuidados, de 670 a 900 ](https://www.telam.com.ar/advf/imagenes/2020/03/5e77a1b082981_1004x565.jpg "Se trabaja junto con empresarios y supermercadistas en expandir el listado de productos incluidos en Precios Cuidados, de 670 a 900 ")**Los participantes**

Por el sector privado participaron el representante de Vital, Eduardo Pochisnky; de La Anónima, Nicolás Braun; de Coto, Guillermo Calcagno; de Día, Martín Tolcachir ; de Changomás, Matías Grondona y Juan Quiroga; de Carrefour, Pablo Lorenzo y Francisco Zoroza; de la Asociación de Supermercados Unidos (ASU), Juan Vasco Martínez.  
  
También estuvieron por Savore Entre Ríos, Héctor Pavan; de Savore Pergamino, Nelson Figeredo; de Savore Lanas, Juan Carlos Menchikian; de FABA Savore, Fernando Savore; de la Cámara Argentina de Supermercados (CAS) y la Federación Argentina de Supermercados y Autoservicios (FASA), Pedro Oroz y Víctor Palpacelli; de Diarco, Pablo Bertolissio; de Maxiconsumo, Víctor Fera; de NINI, Pablo Tome; de Makro, Pedro Balestrini; de Yaguar, Matías Montalván y Nicolás Prat; y de la Cámara Argentina de Distribuidores y Autoservicios Mayoristas, Adrián Scharovsky.  
  
En cuanto a las empresas productoras de bienes masivos, asistieron las autoridades de la Coordinadora de las Industrias de Productos Alimenticios (COPAL), Daniel Funes de Rioja y Carla Martin Bonito; de Swift, Alejandro Spataro; de Alicorp, Jonathan Gerszberg; de Ledesma, Claudio Terres; de Sancor, Carlos Olano Melo; de Prodea, Ignacio Sainz Trapata; de Ilolay, Luis Andreo; de Danone, Diego Buranello; de Cabrales, Diego Polli; de Coca Cola, Esteban Agost Carreño; de Unilever, Karen Vizental; y de Fecovita, Marcelo Saraco, entre otros.