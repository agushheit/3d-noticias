---
category: Estado Real
date: 2020-12-23T10:12:17Z
thumbnail: https://assets.3dnoticias.com.ar/2312concurso-docente.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Concurso de cargos de educación secundaria
title: La provincia inició el concurso de cargos de educación secundaria
entradilla: Los ofrecimientos son 542 y se realizan de modo online hasta el 29 de
  diciembre.

---
Con la premisa de continuar con los procesos de titularización pendientes, que han sido acordados con los sindicatos docentes, **el Ministerio de Educación inició la titularización para cubrir cargos de base de Educación Secundaria, Técnica y de Educación Permanente de Jóvenes y Adultos**.

En ese sentido, los ofrecimientos previstos en el concurso se realizan de manera online, respetando los criterios definidos en la convocatoria concursal y aplicando procedimientos similares a los realizados en forma presencial. El ofrecimiento de 542 cargos se realizará hasta el 29 de diciembre.

## **El concurso**

El procedimiento, que es absolutamente inédito, prevé que los aspirantes en cargos deberán ingresar al **portal de Gestión Educativa** y luego a **Mi Legajo**; y en el **Menú Ofrecimiento** seleccionar la opción **Selección de Vacantes**.

Para la ocasión, están habilitados únicamente los aspirantes que tengan orden de mérito en los escalafones de titularización definitivos publicados correspondientes a Cargos (no catedráticos) y para los cuales existan vacantes disponibles.

Los concursantes podrán elegir todas las vacantes que sean de su interés y estén disponibles en función del escalafón correspondiente.

Particularmente, en aquellas situaciones en que los aspirantes se encuentren escalafonados en más de un cargo y seleccionen vacantes de diferentes cargos, deberán indicar además el orden de preferencia para cada tipo de cargo. Este orden de preferencia se utilizará en aquellos casos en que los aspirantes pudieran acceder a una o dos vacantes en función de su incompatibilidad.

## **Controles**

El sistema también tiene prevista la presencia de controles excluyentes para cada vacante seleccionada por los aspirantes y no se confirmará la selección si no se cumple con las siguientes condiciones:

◾ En ningún caso los aspirantes podrán acceder a más de dos cargos como titular en el sistema educativo provincial de modo que los docentes que tuvieran dos o más cargos titulares, no podrán seleccionar ninguna vacante.

◾ Los aspirantes a titularizar podrán hacerlo hasta en un máximo de tres establecimientos educativos oficiales, tanto de gestión pública como de gestión privada del respectivo nivel, en este caso Secundario. Si ya fuera titular en tres establecimientos o más de ese nivel al momento de la convocatoria, en ningún caso podrá aumentar su dispersión laboral.

Por otra parte, también se controlarán criterios de incompatibilidad, considerando los niveles máximos de unidades de acumulación en función de los puntajes correspondientes a las vacantes seleccionadas y a los cargos/horas cátedra que se visualicen en la opción Mis Cargos/Horas (para aspirantes ya titulares).