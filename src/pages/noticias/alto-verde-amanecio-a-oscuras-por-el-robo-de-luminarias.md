---
category: Agenda Ciudadana
date: 2021-10-11T06:15:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/altoverde.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Alto Verde amaneció a oscuras por el robo de luminarias
title: Alto Verde amaneció a oscuras por el robo de luminarias
entradilla: Volvieron a robar las conexiones del sistema de luminarias de ingreso
  al distrito costero de Alto Verde.

---
Este sábado por la mañana los vecinos de Alto Verde amanecieron a oscuras por el robo que se registró en el camino de ingreso al distrito costero de las conexiones de las luminarias.

Se trata de un nuevo robo de cables y de elementos de iluminación que sufre el municipio local en la zona de Alto Verde, sumado a los reiterados hechos de robos y vandalismo sobre el sistema de luminarias ubicado en distintos tramos de la Ruta Nacional 168.

Lo cierto es que personal de la Dirección de Control de la municipalidad de Santa Fe informó la situación. El subsecretario de Control de la Municipalidad, Guillermo Álvarez, manifestó su malestar por lo ocurrido en las redes sociales y pidió "que cada uno haga lo que tiene que hacer por la ciudad".

"Otra vez quedó sin luz el acceso a la manzana 1 de Alto Verde. Es una locura lo que estamos viviendo con los episodios de robos y vandalismo en distintos lugares de la ciudad", subrayó el funcionario municipal y continuó agregando: "Reponer y reparar todos estos daños se hace con fondos de los vecinos y vecinas que podrían invertirse en nuevas obras para la ciudad".

"Los vecinos nos reclaman más y mejor iluminación; invertimos en obras pero todas las semanas nos encontramos con estos episodios de inseguridad que afectan a todos los santafesinos", finalizó Álvarez.