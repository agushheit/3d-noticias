---
category: Agenda Ciudadana
date: 2021-06-28T06:00:01-03:00
thumbnail: https://assets.3dnoticias.com.ar/SAPPIA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Ante la recalificación de MSCI, los radicales piden "no cargar las tintas
  sobre el actual gobierno"
title: Ante la recalificación de MSCI, los radicales piden "no cargar las tintas sobre
  el actual gobierno"
entradilla: '"No hay que dramatizar en exceso el cambio de calificación del país",
  y pidió "no cargar las tintas sobre el actual Gobierno", sostuvieron desde la Convención
  Nacional de la UCR.'

---
El presidente de la Convención Nacional de la UCR, Jorge Sappia, sostuvo este domingo, ante la recalificación de la consultora estadounidense MSCI sobre la Argentina, que "no hay que dramatizar en exceso el cambio de calificación del país", y pidió "no cargar las tintas sobre el actual Gobierno".

En un documento firmado también por el titular de la comisión de Economía de la Convención, Miguel Ponce, se afirmó que "el país ya estaba fuera de los mercados voluntarios de fondos, y de hecho por ello se debió recurrir al programa Stand By del FMI en 2018", y se pidió "entender lo que significa" esta situación y cómo revertirla.

"En primer lugar, es preciso no cargar las tintas sobre el actual Gobierno, ya que los problemas que enfrenta el país para atraer la inversión privada son de larga data y los gobiernos que se han sucedido, con diversos enfoques económicos a veces hasta antagónicos, no pudieron evitar que esos problemas se profundicen", sostuvo Sappia.

"La Argentina fue mercado de frontera por largo tiempo, por múltiples razones que predominantemente han tenido con ver con la aversión al riesgo de los inversores privados corporativos y por una larga historia de inestabilidad institucional, controles a los flujos de capitales y frecuentes cambios de marcos regulatorios (lo que a veces se llama 'inseguridad jurídica', no sin cierta intencionalidad)", remarcó.

"Vivimos una situación inédita, apelamos al patriotismo y la responsabilidad para encontrar soluciones inéditas".

Sobre la calificación de "mercado emergente" que el país obtuvo en 2018, en el Gobierno de Mauricio Macri, Sappia sostuvo que "fue casi una apuesta a que se dejara atrás esa historia y, lamentablemente, ni la política ni la realidad económica acompañaron las expectativas: aún antes de las elecciones y el cambio de gobierno en 2019, volvieron los controles de cambios y al movimiento de capitales".

En la misma línea se había expresado el Jefe de Gabinete, Santiago Cafiero, cuando recordó que "sorpresivamente en junio de 2018, Argentina había sido calificada como mercado emergente cuando en nuestro país ya empezaba a dar síntomas de una crisis de balanza de pagos" y agregó que "todo está en el plano de la especulación financiera, al cual la Argentina le dijo que no lo considera viable para el desarrollo económico del país".

Para el dirigente radical la pandemia y las decisiones del Gobierno para enfrentarla fueron sumadas a "decisiones políticas fruto de una orientación diferente de la economía, ha revivido todos los controles y los problemas de la década anterior y ha agregado otros nuevos. En ese contexto, era inevitable la pérdida del grado de emergente del país; lo que no se esperaba era el pasaje a mercado 'independiente', es decir, 'fuera de los mercados'".

"Paradójicamente y contra las lecturas mayoritariamente tremendistas que se han hecho desde el anuncio, esto puede interpretarse como una imposibilidad transitoria de evaluar la real situación del país y ser, por tanto, reversible de manera más o menos rápida (en unos años y no en décadas)", sostuvo.

"Resolver los problemas estructurales de la Argentina requiere de un plazo que necesariamente abarcará varios períodos gubernamentales; ningún partido o grupo político podrá, por sí solo, enfrentar estos enormes retos con prescindencia de ese amplio Acuerdo Político, Económico y Social que la UCR viene reclamando desde hace tiempo, y que el desafío de la hora hace más necesario que nunca", remarcaron los dirigentes.

"Vivimos una situación inédita, apelamos al patriotismo y la responsabilidad para encontrar soluciones inéditas", cerraron.