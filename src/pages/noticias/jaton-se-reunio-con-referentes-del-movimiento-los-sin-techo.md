---
category: La Ciudad
date: 2020-12-23T10:41:20Z
thumbnail: https://assets.3dnoticias.com.ar/2312jaton.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: Jatón se reunió con referentes del Movimiento Los Sin Techo
title: Jatón se reunió con referentes del Movimiento Los Sin Techo
entradilla: El intendente de Santa Fe e integrantes de la organización no gubernamental
  mantuvieron un diálogo en temas que son fundamentales para atender los barrios de
  la ciudad, principalmente, los del Noroeste.

---
El intendente Emilio Jatón mantuvo, el lunes 21 de diciembre, una reunión con referentes del Movimiento Los Sin Techo, en la sede de la entidad que funciona en el Colegio Mayor Universitario. 

En la ocasión se abordaron varios temas, entre ellos, la agenda de gobierno municipal para el 2021 y las prioridades de la gestión, así como las intervenciones proyectadas para los barrios de la ciudad, principalmente los del Noroeste.

En representación de Los Sin Techo, participaron su coordinador José Luis Ambrosino, junto a José Luis Zalazar, Jorge Jourdan y Silvana Mana, esta última de los Jardines del movimiento. Por la Municipalidad también estuvo Paola Pallero, titular de la Agencia de Hábitat.

El encuentro forma parte del diálogo que el intendente mantiene con distintas instituciones y sectores con miras a construir en conjunto las acciones de trabajo de la Municipalidad en la ciudad con eje en el Plan Integrar.

***

![](https://assets.3dnoticias.com.ar/2312jaton1.webp)

***

El Movimiento Los Sin Techo es una organización no gubernamental que, desde su creación por parte del padre Atilio Rosso, trabaja para el desarrollo integral y la organización comunitaria del sector relegado de la ciudad de Santa Fe. En este sentido, la coordinación de agendas entre municipio y organización fue uno de los objetivos en los que trabajarán ambas partes.