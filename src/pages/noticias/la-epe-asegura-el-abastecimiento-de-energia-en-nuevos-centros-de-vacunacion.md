---
category: Estado Real
date: 2021-06-20T06:00:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/EPE.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La EPE asegura el abastecimiento de energía en nuevos centros de vacunación
title: La EPE asegura el abastecimiento de energía en nuevos centros de vacunación
entradilla: La Empresa Provincial de la Energía despliega personal de áreas operativas
  en el marco de la logística que realiza el gobierno provincial.

---
La Empresa Provincial de la Energía realizó una serie de trabajos para asegurar la continuidad del abastecimiento de electricidad en los centros de vacunación, particularmente en La Redonda y en La Esquina Encendida, de la ciudad de Santa Fe.

Los trabajos consistieron en la instalación de un grupo generador como soporte para la demanda específica en pos de la conservación de las vacunas, como así también en el tendido de una red de preensamblado especial, desde la subestación transformadora ubicada en las inmediaciones de Av. Facundo Zuviría y Estanislao Zeballos. Asimismo, todos los días se controla el estado de los generadores, y todas las semanas se ponen a prueba para optimizar la respuesta frente a contingencias.

El Presidente de la EPE, Mauricio Caussi, destacó el compromiso que toda la organización está mostrando para la contención de la pandemia en general, y con el proceso de vacunación en particular, que es la máxima prioridad que tiene el Gobierno en estos momentos.

Caussi remarcó a su vez el trabajo articulado con las distintas esferas del Gobierno, coordinando diversas acciones con los distintos ministerios, en particular Salud, Infraestructura, y de Desarrollo Social, como así también a nivel de autoridades de los ejecutivos municipales, concejales, presidentes comunales, en el marco de la logística que se necesita en materia de provisión del servicio eléctrico.

Por su parte, el secretario de Políticas Sociocomunitarias, Ignacio Martínez Kerz, con quien permanentemente se coordinan acciones desde la Empresa en la ciudad capital, ponderó el compromiso del Directorio de la EPE y de todo su personal, para responder en cualquier momento ante las necesidades que surgen en el armado de un plan de vacunación nunca visto.

Kerz afirmó que, junto al Ministerio de Salud, se habilitarán vacunatorios en: Centro de salud Fonavi barrio Centenario, Chalet, Evita barrio La Florida, Barranquitas, Villa Hipódromo, Altos del Valle, Sarmiento Juventud Unida del Norte, Acería, Yapeyú, Centro de Salud n° 9, barrio Nueva Pompeya y Demetrio Gómez, de Alto Verde.

De esta manera, dijo el funcionario, los centros de salud barrial se suman a los cuatro puntos ya utilizados para la vacunación contra el Covid en la ciudad de Santa Fe: La Esquina Encendida, Centro de Educación Física Nº29, Cemafe y el viejo Hospital Iturraspe. En estos es indispensable la provisión regular del servicio eléctrico para asegurar las condiciones de las vacunas a inocular.

Este domingo se realizará el acto oficial por el Día de la Bandera

El gobernador de la provincia, Omar Perotti, encabezará este domingo, el acto central en conmemoración del Día de la Bandera Nacional, que se celebrará a partir de las 9:45 horas, en el Monumento a la Bandera, ubicado en la ciudad de Rosario.

El presidente de la Nación, Alberto Fernández, participará de forma remota, desde la Quinta de Olivos, mientras que el intendente de la ciudad, Pablo Javkin, acompañará al gobernador en el lugar.

En primer término, a las 8 horas, Omar Perotti izará la bandera nacional en la Isla Sabino Corsi. Este hecho no reconoce antecedentes en la larga historia de celebraciones del 20 de junio y consiste además en la puesta en valor de lo que será la construcción de un eco-monumento en homenaje a la Bandera Nacional y su creador, Manuel Belgrano en virtud de cumplirse, en 2022, los 210 años de la creación de la enseña nacional y en dicho lugar, se evocará el emplazamiento de la mítica Batería Independencia que sirvió para la defensa de los ataques fluviales realistas.

A continuación, desde las 9:45 horas se realizará el izamiento de la bandera nacional en el Monumento a la Bandera.

Seguidamente, a las 10.15 horas, se entonarán las estrofas del himno nacional argentino en el Propileo, donde luego harán uso de la palabra en primer término el intendente Javkin; luego el gobernador Perotti y para cerrar el evento, el presidente Fernández, quien realizará la jura a la bandera junto a niños y niñas desde Olivos.

Finalmente, a las 11 horas, se llevará adelante un desfile aéreo, a cargo del Ministerio de Defensa de la Nación, que será acompañado por la Banda Militar de música "Cabo Teodoro Fels" perteneciente al Liceo Aeronáutico Militar de la localidad de Funes.