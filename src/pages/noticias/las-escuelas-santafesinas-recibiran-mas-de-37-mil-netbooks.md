---
category: Agenda Ciudadana
date: 2021-07-29T06:00:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/NETBOOKS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Las escuelas santafesinas recibirán más de 37 mil netbooks
title: Las escuelas santafesinas recibirán más de 37 mil netbooks
entradilla: 'Será a partir de un convenio firmado este miércoles entre el gobernador
  Omar Perotti y el ministro de Educación de la Nación, Nicolás Trotta. '

---
El gobernador Omar Perotti y el ministro de Educación de la Nación, Nicolás Trotta, firmaron este miércoles el convenio de cesión gratuita de material tecnológico en el marco del Plan Federal Juana Manso. El acuerdo permitirá que la provincia reciba 37.442 netbooks que beneficiarán, en esta primera etapa, a 455 escuelas santafesinas.

La rúbrica se llevó adelante tras una visita que realizaron el gobernador y el ministro nacional a la empresa Tic Air Computers, que se dedica a la fabricación y ensamble de los equipos informáticos que se distribuirán a todo el país y está ubicada en la ciudad de Rosario.

En la oportunidad, el gobernador Perotti manifestó que “la pandemia desnudó las diferencias que se dan particularmente en la educación y la tecnología cuando el Estado no está presente. Hay que mirar para atrás, lo malo que ha sido la interrupción de un programa como Conectar Igualdad, ya que hubiésemos tenido en esta pandemia una realidad muy distinta, porque cada alumno hubiese estado con un dispositivo en su casa”, recordó.

Además, el gobernador santafesino destacó que “en lugar de repartir cuadernillos, podríamos haber repartido un pendrive. Por eso, la decisión del Presidente nos parece de alto valor estratégico para afirmar, definitivamente, la búsqueda de igualdad de oportunidades”, enfatizó.