---
category: La Ciudad
date: 2020-12-02T17:44:47Z
thumbnail: https://assets.3dnoticias.com.ar/ROBO-BELGRANO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: LT10'
resumen: Robaron en los galpones de la Estación Belgrano
title: Robaron en los galpones de la Estación Belgrano
entradilla: Ocurrió en la madrugada, en el galpón que está a la altura de calle Avellaneda.
  Los delincuentes se robaron algunos elementos de motocicletas retenidas por infracciones.

---
La municipalidad de Santa Fe denunció en la comisaría tercera un robo que sufrió uno de los galpones de la Estación Belgrano (el que está a la altura de la calle Avellaneda).

![](https://assets.3dnoticias.com.ar/ROBO-BELGRANO1.jpg)

Los delincuentes hicieron un hueco en la pared y se llevaron elementos de motocicletas retenidas por el municipio (asientos, accesorios y combustible).

Carlos Pereira, concejal de Juntos por el Cambio, sostuvo que "estamos al tanto de que están utilizando un galpón de la Belgrano para guardar fotos retenidas porque está colapsado el corralón municipal. Deberían hacer subastas y compactaciones (que este año no se hicieron). Pero llama la atención un robo en este lugar, a metros del lugar donde se guardan las camionetas de la GSI y la cantidad de robos que sufrieron los edificios municipales en este año (jardines, escuelas de trabajo, etc.)"

Además, Carlos Pereira agregó que "muchos edificios municipales quedaron sin la recomendable custodia de la GSI".