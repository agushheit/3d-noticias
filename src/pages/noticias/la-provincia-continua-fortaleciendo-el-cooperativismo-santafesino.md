---
category: Estado Real
date: 2021-07-03T06:00:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/COOPERATIVISMO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia continúa fortaleciendo el cooperativismo Santafesino
title: La Provincia continúa fortaleciendo el cooperativismo Santafesino
entradilla: Se entregaron matrículas a 12 nuevas cooperativas de Santa Fe, Santo Tomé
  y Rafaela.

---
La Secretaría de Desarrollo del Ministerio de Producción, Ciencia y Tecnología, entregó este viernes matrículas a 10 nuevas cooperativas de Santa Fe, Santo Tomé y Rafaela. Las mismas prestarán servicios de mantenimiento, construcción, reciclado, asistencia técnica, recreación, deportes y proveeduría.

El acto se llevó a cabo en las instalaciones del Molino Fábrica Cultural en la ciudad de Santa Fe, y contó con la participación del director provincial de Economía Social, Agricultura Familiar y Emprendedurismo, Guillermo Tavernier, quien expresó: “Estamos entregando 10 nuevas matrículas, lo que nos lleva a entender que hay muchos santafesinos y santafesinas que están viendo en el cooperativismo una herramienta de desarrollo. Creemos que el esquema cooperativo/asociativo es generador de oportunidades, de puestos de trabajo y de proyectos productivos, es algo que nos entusiasma. Ya entregamos a lo largo del año más de 50 matrículas, nos pone contentos como Gobierno provincial acompañar cada uno de estos procesos y que puedan desarrollar sus unidades productivas”.

Por su parte, el jefe de la Agencia Territorial, dependiente del Ministerio de Trabajo de la Nación, Daniel Mendoza, manifestó: “Venimos articulando con la Provincia desde el inicio de la gestión. Nuestra cartera tiene cinco líneas de apoyo a cooperativas, por eso es importante su formalización para que ingresen en estos programas. Es una alegría siempre recibir nuevas cooperativas, eso implica que hay gente que quiere trabajar, que quiere unirse al movimiento cooperativa y para nosotros es la punta de lanza para la reactivación económica; por eso es una alegría estar hoy aquí”.

Asimismo, la concejala de la ciudad de Santa Fe, Jorgelina Mudallel, valoró que “es una herramienta muy necesaria en este contexto de crisis apostar a la economía solidaria a partir de generar trabajo en las distintas cooperativas de nuestra ciudad. En ese sentido, presentamos una ordenanza porque queremos que el Municipio tenga un porcentaje para las cooperativas en la obra pública y acompañamos las iniciativas del gobierno provincial para seguir fortaleciendo este esquema”.

El secretario General de la Central de Trabajadores de la Argentina (CTA) de Santa Fe, José Testoni, participó de la actividad representando a la Cooperativa Santa Fe Sustentable, sostuvo: “Se creó con las unidades productivas que vienen trabajando desde hace mucho tiempo, pero sin una unidad jurídica como la que estamos logrando en el día de hoy. Es un trabajo conjunto entre el INAES, la Dirección de Cooperativas de la provincia, de los ministerios de Producción y de Trabajo, y de los compañeros que hace tiempo vienen trabajando con cierta informalidad. Estamos muy felices porque este es un paso muy necesario, apostando a un movimiento cooperativo provincial y nacional que tiene que volver a poner a la Argentina de pie”.

Finalmente, el presidente de la Cooperativa “Sembrando Vida, Cosechando Esperanza”, Emanuel López, afirmó: “Estamos agradecidos de que se haya podido concretar este proyecto, que nos hayan entregado la matrícula. Es un avance muy grande del conjunto de Provincia y Nación para poder llevar a cabo la integración de las familias con pocos recursos que viven en el norte de la ciudad de Santa Fe”.

Recibieron la matrícula las siguientes cooperativas: Cooperativa de Trabajo “Shekina” Limitada (mantenimiento), Cooperativa de Trabajo "Santa Fe Sustentable" Limitada (mantenimiento), Cooperativa de Trabajo "Micaela Garcia" Limitada (mantenimiento), Cooperativa de Trabajo "Construir Popular" Limitada (construcción), Cooperativa de Trabajo "Desafío Eco Santoto" Limitada (reciclado), Cooperativa de Trabajo "Movimiento Obrero Vecinos en Lucha " Limitada (construcción), Cooperativa de Trabajo "Sem. VI.C.E." Sembrando Vida Cosechando Esperanza Limitada (mantenimiento y construcción), Cooperativa de Trabajo Regeneractivo Limitada (asistencia técnica/prof. medioambiente), Cooperativa de Trabajo Los Amigos Limitada (construcción y mantenimiento) y Asociación Mutual Asociados a Clubes de la Liga Santafesina de Fútbol (recreación, deportes, proveeduría).