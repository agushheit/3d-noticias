---
category: La Ciudad
date: 2021-01-05T11:50:31Z
thumbnail: https://assets.3dnoticias.com.ar/predio-estacion-Belgrano.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 'Fuente: El Litoral'
resumen: Gestionan "custodiar" 30 hectáreas de la ciudad para proyectar planes urbanísticos
title: Gestionan «custodiar» treinta hectáreas de la ciudad para proyectar planes
  urbanísticos
entradilla: 'Son tierras fiscales ubicadas detrás de la Estación Belgrano y en los
  barrios Cabal y Los Troncos, donde hay asentamientos irregulares. Las acciones administrativas
  se realizan ante la Aabe. '

---
**Obtener esa autorización permitiría intervenir bajo protocolo en los asentamientos, y luego buscar alternativas para la construcción de viviendas.**

El gobierno provincial ya remitió los expedientes a la Agencia de Administración de Bienes del Estado (Aabe) en los que solicita obtener la custodia de unas 30 hectáreas de terrenos fiscales de tres zonas puntuales de la ciudad: 22,4 hectáreas de los terrenos detrás de la ex Estación Belgrano, unas seis manzanas en barrio Los Troncos y otro tanto en Cabal. 

En los tres casos hay ocupaciones irregulares. La intención: tras obtener la autorización administrativa de la custodia legal, abordar la problemática de los asentamientos y luego, planificar a futuro proyectos urbanísticos.

La gestión está avanzada y «los expedientes ya están en curso. Esperamos la resolución final, pero estimamos que la Aabe nos autorizaría lo que solicitamos», la dijo a El Litoral el secretario de Hábitat, Urbanismo y Vivienda provincial, Amado Zorzón. 

**¿Qué implicaría tener la custodia de esos terrenos fiscales?** Que, desde ese momento, el gobierno santafesino tiene legitimidad jurídica para poder actuar. «Y la idea es que, frente a las ocupaciones irregulares, se trate de intervenir rápidamente para evitar confrontaciones y buscar soluciones consensuadas», añadió.

La provincia ya cuenta con un protocolo de actuación ante usurpaciones irregulares (elaborado interministerialmente entras las carteras de Infraestructura y Hábitat, Desarrollo Social, Derechos Humanos, Género), que se distribuyó entre los 365 distritos de la bota santafesina. 

«Ese protocolo representa una sugerencia de cómo intervenir ante una situación de ocupación, y consta de una serie de pasos. Cada jefe distrital puede aceptarlo o no; pero tuvimos buena recepción, porque la idea es que, ante el derecho humano de tener un terreno y una vivienda, se plantea buscar una alternativa solución para cada caso particular y por la vía legal, descartando -subrayó Zorzón- el desalojo por la vía violenta».

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">Proyecciones</span>**

Con la custodia en mano, lo que se proyecta es diseñar intervenciones urbanísticas en los lotes de esos tres sectores (detrás de la Belgrano, Cabal y Los Troncos). Consultado sobre qué tipo de planes de vivienda se llevarían adelante, el funcionario expresó que se analizan diversas alternativas: «Se están haciendo consultas a varias universidades (como la UTN), junto a nuestras propias propuestas, sobre todo es el terreno detrás de la Estación Belgrano, que es el más extenso».

Respecto de este último sector, Zorzón adelantó que ya hay estudios de urbanización social que se realizaron sobre toda esa zona. Pero hasta tanto no esté la custodia legal, no se puede hacer nada: solo ir planificando, y recién después se resolverá qué planes de urbanización se desarrollarán.

En gran parte de este amplio predio, hay muchos particulares que fueron haciendo «ampliaciones» de vivienda sobre terrenos que son de Nación. Consultado sobre cómo se debe actuar ante estos casos complejos, Zorzón respondió: «esta situación es más complicada. Pero antes que el desalojo no pacífico, está la vía legal. Vamos a ir viendo caso por caso, llegado el momento. Sabemos que hay una cuestión social detrás. Por eso digo que todo esto amerita un estudio serio y un trabajo compartido con organizaciones sociales para poder adelante proyectos bien diseñados».

En el predio detrás de la Estación hay algunos asentamientos. Lo mismo en Cabal y Los Troncos. En barrio Transporte, la provincia ya tiene la tenencia del predio donde hubo una usurpación (detrás del CIC).

 Zorzón consideró que el ejemplo de cómo se solucionó el conflicto por la toma en un área de ese barrio fue «exitoso». «Porque más allá de la angustia y la incertidumbre que le genera a la gente, se trabajó y aún se trabaja con cada una de esas 15 familias. Sin la custodia debe intervenir la Aabe y el juez federal, es un proceso engorroso. Con la custodia, la intervención es más ágil».

La Ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, había declarado que la urbanización de los barrios populares y el registro de terrenos fiscales (ver Relacionada) tienen como Norte «elaborar políticas concretas para comenzar a saldar el déficit habitacional a través de diferentes programas, como Santa Fe Sin Ranchos, Lote Propio, Núcleos Básicos -etcétera-, para equilibrar los territorios con la infraestructura necesaria y accesos a la educación, la salud y los servicios públicos».

Obteniendo la custodia de esos terrenos, el gobierno santafesino podría hacerse cargo del mantenimiento y vigilancia de los terrenos, evitando futuros asentamientos y proyectando desarrollos urbanísticos de barrios populares y planes de viviendas.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">RUTFU</span>**

El Registro Único de Tierras Fiscales Urbanizables (RUTFU) es un sistema donde se va volcando toda la información sobre tierras urbanizables de toda la bota santafesina. «Aún no está completo, porque necesitamos la información de muchos municipios que no completaron hasta ahora la documentación necesaria. Una vez que pueda actualizarse, será una herramienta muy valiosa para gestionar el suelo pensando en dar la posibilidad de construir viviendas», aseguró Zorzón.

También se analiza en cada caso en índice de demanda habitacional. «Pues no es lo mismo la demanda que hay en las grandes ciudades, como Santa Fe y Rosario, que la que puede haber en una pequeña localidad de mil o dos mil habitantes. Es algo que siempre implica análisis», cerró.