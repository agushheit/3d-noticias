---
category: Agenda Ciudadana
date: 2021-01-18T09:00:15Z
thumbnail: https://assets.3dnoticias.com.ar/ypf.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Diario Uno
resumen: Otra vez, en menos de 24 horas, YPF aumentó los combustibles
title: Otra vez, en menos de 24 horas, YPF aumentó los combustibles
entradilla: La empresa estatal subió los precios para equipar los valores con el resto
  de las petroleras. El aumento se aplicó en las primeras horas de este domingo. Mirá
  como quedaron los precios en Santa Fe.

---
A poco menos de un día de haber anunciado oficialmente el aumento de los combustibles un 3,5%, la empresa estatal YPF, dispuso este domingo otra suba de precios con el objetivo de equiparar el porcentaje modificado por las petroleras privadas. Con dos incrementos en menos de 24 horas, el aumentó llegó al 6 por ciento.

Así lo confirmaron a UNO Santa Fe integrantes de la Cámara de Expendedores de Combustibles, que alertaron de la maniobra ejecutada por la petrolera estatal.

En esta oportunidad "se coordinó" con las petroleras una suba programada y escalonada en porcentajes, que llevó a que YPF aumentara el sábado un 3,5% y debiera en 24 horas, ajustar los precios para equiparar los valores con los precios dispuestos por Shell, Axion y otras firmas.

Así quedaron los precios:

**Precios de YPF**

• Súper: $79,10

• Infinia: $90,20

• Infinia Diésel: $82,90

• Diésel 500: $70,30

**Precios SHELL**

• Súper: $81,17

• V-Power nafta: $93,28

• Fórmula DIesel: $79,58

• V-Power diesel: $85,81

**Precios AXION**

• Súper: $80,80

• Quantium: $92,60

• Diesel: $72,99

• Quantium diesel: $85,39