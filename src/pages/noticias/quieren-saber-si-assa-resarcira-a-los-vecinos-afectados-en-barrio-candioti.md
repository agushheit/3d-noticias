---
category: La Ciudad
date: 2021-09-29T06:00:13-03:00
thumbnail: https://assets.3dnoticias.com.ar/ASSA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Quieren saber si Assa resarcirá a los vecinos afectados en barrio Candioti
title: Quieren saber si Assa resarcirá a los vecinos afectados en barrio Candioti
entradilla: La Dirección de Derechos y Vinculación Ciudadana de la municipalidad le
  envió una nota a la empresa. Los análisis revelaron que el agua no era apta para
  consumo humano por tener altas concentraciones de aluminio.

---
La Dirección de Derechos y Vinculación Ciudadana de la ciudad de Santa Fe le envió una nota a Aguas Santafesinas (Assa) y al Enress (Ente Regulador de Servicios Sanitarios) consultándole sobré qué medidas se están "evaluando de forma indemnizatoria o resarcitoria" para los usuarios de barrio Candioti, quienes padecieron severos inconvenientes con el servicio de agua potable.

Cabe recordar que el pasado miércoles, trascendió que los análisis de tres muestras de agua, tomadas en diferentes domicilios en barrio Candioti, determinaron que el agua no era apta para consumo humano.

Un insumo químico utilizado para el tratamiento del agua, denominado Policroruro de Aluminio, fue lo que provocó las altas concentraciones de aluminio en el agua, generando que la misma no cumpla con los parámetros de calidad y no se pueda consumir.

Franco Ponce de León, titular de la Dirección de Derechos Ciudadanos detalló que "días atrás enviamos notas tanto a Assa como a Enress; solicitándole información acerca de lo sucedido y sobre qué medidas se están llevando adelante para que acciones como estas no vuelvan a ocurrir".

En declaraciones a la prensa, comentó que mediante "el trabajo cotidiano que tenemos con la Defensoría del Pueblo nos hicimos del Informe del Enress que a través de distintos análisis planteaba que las muestras tomadas, claramente, el agua no era apta para consumo humano".

Advirtió que "son varias las problemáticas que han sucedido a raíz de este inconveniente, y enumeró: "Primero, el derecho como usuarios a tener información clara y precisa; más con un servicio público tan importante. Y por otro lado, lo vinculado con la salud pública".