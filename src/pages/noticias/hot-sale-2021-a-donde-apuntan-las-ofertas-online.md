---
category: Agenda Ciudadana
date: 2021-05-06T08:41:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/hotsale.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Hot Sale 2021: a dónde apuntan las ofertas online'
title: 'Hot Sale 2021: a dónde apuntan las ofertas online'
entradilla: La próxima semana comienza una nueva edición en el país y te contamos
  los detalles. Habrá mega ofertas bomba y horas sorpresa.

---
La Cámara Argentina de Comercio Electrónico llevará a cabo una nueva edición de Hot Sale, el evento de compras online más importante del país, que se realizará el 10, 11 y 12 de mayo. Además de los descuentos programados, habrá horarios con ofertas bomba para aprovechar. 

Como cada año el evento contará con las clásicas Mega Ofertas Bomba y Horas Sorpresa. En esta edición CACE revela los horarios en los que sucederán estas ofertas para que los usuarios puedan organizar sus compras.

Mega Ofertas Bomba: se realizará una vez por día, durante los tres días de evento y serán descuentos especiales en varias categorías. Las Mega Ofertas Bomba estarán disponibles sólo por una hora y/o hasta agotar stock y serán ofertas superadoras que se encontrarán en la web durante el 10, 11 y 12 entre las 12 y las 13.

Horas Sorpresa: integrarán ofertas agrupadas por categorías. Las mismas tendrán una hora de duración y aparecerán durante distintos momentos del día. El evento contará con una Horas Sorpresas por categoría y las podrán encontrar en la web oficial del evento 4 veces al día.

Los días y los horarios

**Lunes 10/5**

* Electro y Tecno: de 09 a 10hs
* Emprendedores: de 14 a 15
* Automotriz: de 16 a 17
* Viajes: de 18 a 19.

**Martes 11/5**

* Bebés y Niños: de 09 a 10
* Muebles, Hogar y Deco: de 14 a 15
* Cosmética y Belleza: de 16 a 17
* Servicios: de 18 a 19.

**Miércoles 12**

* Indumentaria y Calzado: de 09 a 10
* Deportes y Fitness: de 14 a 15
* Varios: de 16 a 17.
* Alimentos y Bebidas: de 18 a 19.

Las categorías para esta edición

Los cambios de hábito y de consumo experimentados desde el comienzo de la pandemia se reflejan en la configuración de las categorías y empresas participantes.

Entre las 13 categorías en las que se dividen las empresas participantes se destacan hogar, electro y cosmética con más de 100 empresas inscriptas en cada una y con un crecimiento de entre el 40 y 60 por ciento en cantidad de participantes respecto de la edición anterior. Además, más de 200 empresas de indumentaria preparan sus ofertas y la categoría alimentos y bebidas presenta un crecimiento del 50 por ciento entre ediciones y su variedad de opciones de productos de compra cotidiana.

En esta edición se suman dos nuevas categorías, emprendedores y servicios, que con más de 30 empresas inscriptas ya demuestran una tendencia al crecimiento en la adopción del comercio electrónico en estos rubros.