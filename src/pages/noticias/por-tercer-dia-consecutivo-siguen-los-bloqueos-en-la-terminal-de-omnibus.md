---
category: La Ciudad
date: 2021-04-14T07:43:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/paro.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: Por tercer día consecutivo siguen los bloqueos en la terminal de ómnibus
title: Por tercer día consecutivo siguen los bloqueos en la terminal de ómnibus
entradilla: Transportistas de turismo independientes siguen con las medidas de fuerza
  en la zona de la terminal a la espera de respuestas de Nación. Siguen cortes y reducciones
  de tránsito

---
Por tercer día consecutivo, transportistas de turismo independientes a nivel nacional, pero con fuerte repercusión y resonancia en la ciudad de Santa Fe, siguen bloqueando el ingreso a la Terminal de Ómnibus Manuel Belgrano y realizando cortes en la zona de macrocentro hasta tanto no haya una definición que llegue desde Buenos Aires.

Hasta el momento, los trabajadores cortan accesos por Alem y Belgrano y por Suipacha. El resto son reducciones de calzada con presencia de inspectores de la municipalidad en la zona de Rivadavia y Suipacha

**Lo que piden los transportistas de turismo**

**Petitorio**

Desde el sector de Transporte de Pasajeros de Turismo Autoconvocados, nos reunimos con el objetivo de reclamar y proteger nuestro derecho de poder ejercer libremente nuestra actividad comercial con normalidad. Siempre hemos cumplido y colaborado con las medidas que impuso el gobierno nacional. En estos momentos no podemos seguir sin realizar nuestras tareas ya que no tenemos recursos económicos suficientes para poder afrontar nuevamente la situación que sufrimos en el año 2020 de tener 9 meses sin trabajar ni recibir ninguna ayuda económica ni del gobierno nacional ni provincial. Estos meses desde que autorizaron desarrollar nuestra actividad, muchas empresas volvieron a recurrir a préstamos, créditos para poder reactivar la fuente laboral.

Las empresas de turismo cumplieron en su totalidad con los protocolos establecidos como de transportar de 80% de los pasajeros asumiendo las pérdidas que esto implica.

Por lo expuesto anteriormente solicitamos de forma urgente.

1\. Solicitamos el libre desempeño de nuestra fuentes de trabajo, con la igualdad que poseen las empresa de líneas y transportes aéreos.

2\. Solicitamos la eximición del Decreto 235/2021,

3\. Declarar la emergencia económica para el sector.

4\. Habilitar terminales con testeos rápidos de los pasajeros como para nuestros choferes.

5\. Cancelación del pago de ingresos brutos y ganancias por el periodo que dure la pandemia.

6\. Implementar un subsidio por los meses de interrupción de nuestra actividad o lucro cesante de nuestras unidades.

7\. El pago de sueldos de nuestros empleados y cargas sociales.

8\. Ampliación del vencimiento de habilitación nacional de las unidades con antigüedad de 15 años, incluyendo vehículos modelos año 2007.

9\. Solicitamos gasoil subsidiado para unidades con habilitación nacional.

10\. Solicitamos con urgencia una reunión con autoridades provinciales o nacionales para dejar asentado en acta el libre desempeño de nuestra actividad siempre cumpliendo con los protocolos establecidos.