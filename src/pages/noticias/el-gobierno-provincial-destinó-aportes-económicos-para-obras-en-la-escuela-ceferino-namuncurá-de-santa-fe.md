---
layout: Noticia con imagen
author: Nota de la Redacción de 3DNoticias
resumen: Obras en Escuelas
category: La Ciudad
title: El gobierno provincial destinó aportes económicos para obras en la
  Escuela Ceferino Namuncurá de Santa Fe
entradilla: Perotti recorrió el edificio escolar y entregó fondos para el
  entubado de desagües pluviales y la construcción de cielorrasos y pisos de
  todo el hall central del edificio escolar.
date: 2020-11-22T13:49:02.059Z
thumbnail: https://assets.3dnoticias.com.ar/ceferino.jpeg
---
El gobernador Omar Perotti recorrió la escuela de Educación Secundaria Orientada y modalidad Técnica Profesional particular incorporada N° 2025 Ceferino Namuncurá de la ciudad de Santa Fe, y entregó aportes económicos para obras en el edificio de la institución educativa.

"Nuestra escuela está en un proceso de refacción y ampliación del edificio, que tiene ya 4 años de duración", precisó la directora del establecimiento, Sonia Rey; y agradeció "este enorme aporte que va a ser de mucha importancia para poder continuar con las obras de refacción".

Al respecto, Rey precisó que "las obras que se van a encarar en los próximos días tienen que ver con el entubado de desagües pluviales, y la construcción de cielorrasos y pisos de todo el hall central del edificio escolar".

Por último, la directora manifestó que "estamos esperando ansiosos la vuelta a clases, realmente es necesario poder mantener un vínculo con los chicos; y creo que va a ser enorme su sorpresa, porque a principio del próximo año va a estar terminada la obra, para que puedan disfrutar de las instalaciones".

En tanto, el subdirector, Juan Manuel Birollo, precisó que "todos los recursos que obtenemos son para el mantenimiento de la escuela, y si no tenemos ayuda no podemos realizar ninguna de estas obras. Estos fondos son muy bienvenidos, porque de esta manera vamos a poder seguir avanzando y recibir a los chicos como se merecen, en un ámbito agradable y digno para que puedan seguir aprendiendo".

"La escuela cuenta con 550 alumnos, en 20 divisiones distribuidas en turno mañana y tarde; y contamos con 3 terminalidades: una tecnicatura en electrónica; y dos medias que son Ciencias naturales y administración", concluyó Birollo.

Acompañaron al gobernador, el senador nacional Roberto Mirabella; el secretario de Prácticas Socio Comunitarias, Ignacio Martínez Kerz; el senador por el departamento La Capital, Marcos Castello; y la concejala Jorgelina Mudallel.