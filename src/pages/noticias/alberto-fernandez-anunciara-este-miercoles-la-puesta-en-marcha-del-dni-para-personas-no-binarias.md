---
category: Agenda Ciudadana
date: 2021-07-21T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/NOBINARIES.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Alberto Fernández anunciará este miércoles la puesta en marcha del DNI para
  personas no binarias
title: Alberto Fernández anunciará este miércoles la puesta en marcha del DNI para
  personas no binarias
entradilla: El objetivo con este nuevo DNI es garantizar el derecho a la identidad
  de género de las personas que no se auto perciben femeninas ni masculinas, indicaron
  fuentes de la cartera de Interior.

---
El objetivo con este nuevo DNI es garantizar el derecho a la identidad de género de las personas que no se auto perciben femeninas ni masculinas, indicaron fuentes de la cartera de Interior.

El presidente Alberto Fernández anunciará mañana la puesta en marcha del Documento Nacional de Identidad (DNI) para personas no binarias.

El acto se realizará a las 11.30 en el Museo del Bicentenario de la Casa Rosada, informó el Gobierno nacional.

Fernández estará acompañado por los ministros del Interior, Eduardo de Pedro; y de Mujeres, Géneros y Diversidad, Elizabeth Gómez Alcorta; y por el director del Registro Nacional de las Personas (Renaper), Santiago Rodríguez.

El objetivo con este nuevo DNI es garantizar el derecho a la identidad de género de las personas que no se auto perciben femeninas ni masculinas, indicaron fuentes de la cartera de Interior.

Con la entrega de estos tres primeros DNI, la Argentina se convertirá en el primer país de la región en la ampliación y reconocimiento de estos derechos, añadieron las fuentes.