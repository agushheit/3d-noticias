---
category: Agenda Ciudadana
date: 2021-06-03T07:59:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/controles.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: A dos semanas del aislamiento estricto, no hay cambios en la curva de contagios
  en Santa Fe
title: A dos semanas del aislamiento estricto, no hay cambios en la curva de contagios
  en Santa Fe
entradilla: El modelo predictivo diagramado por especialistas del Conicet muestra
  que la curva de contagios sigue con la misma evolución que hace dos semanas y el
  contacto social se mantiene.

---
**A partir de las nuevas medidas restrictivas dispuestas por el gobierno provincial el pasado 20 de mayo a causa del gran impacto de la segunda ola de** [**contagios**](https://www.unosantafe.com.ar/contagios-a21678.html)**, especialistas del Instituto de Matemática Aplicada del Litoral (Imal), dependiente del Conicet, elaboraron un modelo predictivo que muestra la evolución de la** [**curva**](https://www.unosantafe.com.ar/curva-a28709.html) **de** [**casos**](https://www.unosantafe.com.ar/casos-a44042.html) **en el departamento La Capital. Lo cierto es que actualmente no se observan cambios en la curva de infectados con respecto a los índices de hace dos semanas atrás.**

Las proyecciones de los especialistas apuntan a que a principios de junio el Departamento La Capital llegará a la suma de los 60.000 contagios totales, pero a partir de allí esta cifra puede tener un aumento exponencial o aplacarse dependiendo algunos factores.

![](https://media.unosantafe.com.ar/p/8a8662672c5f764debe8030d08b33362/adjuntos/204/imagenes/030/071/0030071479/curva-contagios-covidjpg.jpg?0000-00-00-00-00-00 =642x361)

En el gráfico se muestra una línea punteada de color violeta que marca la evolución actual de la curva de casos en el departamento La Capital, a lo que le sigue una **curva roja** que sería la predicción asociada a la continuidad de estos índices tal como se manifiestan actualmente. **La curva negra corresponde a un aumento exponencial de casos, asociado al retorno de actividades sociales en ámbitos de baja aireación, previendo que a finales de junio serán 80.000 los casos de Covid en el departamento.**

La **curva verde**, en cambio, corresponde a una atenuación del impacto de los contagios en La Capital de una manera marcada, aunque esta evolución predictiva está condicionada a que se cumplan a toda costa los cuidados individuales y sociales, además de sostener en el tiempo las restricciones ya impuestas. En esta curva, el descenso de casos totales a causa del cuidado social y sanitario extremo arroja que a finales de junio serán 68.000 los infectados totales.

**UNO Santa Fe** dialogó con el Ingeniero Ernesto Kofman, investigador del Conicet que participó en el proyecto de desarrollo del modelo predictivo frente a la evolución de los casos en Santa Fe. A propósito de esto, manifestó: "No hay mucha certeza de lo que vaya a pasar con esto, pero en Santa Fe la curva de contagios está dando tal cual con nuestros parámetros, no está cambiando nada el contacto social. Si uno se fija 15 días atrás los índices de contacto social son los mismos a los actuales".

Consultado por los efectos de las restricciones, el especialista sostuvo que "a fin de esta semana se tendría que analizar si tuvieron efecto las medidas para bajar los índices de contacto social". Y agregó: "El cálculo que realizamos está hecho en base a la curva de contagios, pero cuando se adoptan estas medidas lo que suponemos es que este índice va a cambiar, pero todavía no hubo un cambio notable".

**Cómo es el modelo utilizado**

Kofman explicó a **UNO** el modelo utilizado, el cual divide a la población en cuatro tipos de categorías: "**susceptibles**, que son las personas que pueden contagiarse. Los **expuestos**, que son las personas que se contagiaron pero que no tienen la capacidad de contagiar porque se demora unos días para empezar a tornarse infeccioso. **Infectados**, que son las personas que ya están contagiando y los **removidos**, que son las personas que se están recuperando y que al menos por un tiempo no pueden volver a contagiar o la probabilidad de que lo hagan es muy baja.

Esa es la base del modelo predictivo que elabora el Imal, agregando el investigador que "si un día se contagian 100 personas pasan de ser susceptibles a ser expuestos, para luego pasar a ser infectados. Las personas van pasando de categoría en el modelo a medida que se van contagiando. La pregunta es cuantas personas se contagian cada día".

Para esto, el parámetro que toman los expertos es el **“número reproductivo básico”**, que dice a cuantas personas personas contagia del virus una persona infectada en promedio mientras dura su período infeccioso. Según Kofman, "un ejemplo de esto podría verse de la siguiente manera: **si cada infectado contagia el virus a dos personas en promedio y hay 1.000 infectados, éstos van a contagiar a otras 2.000 personas".**

Esto depende de la categorización de la persona en contacto con el infectado, puesto que solo pueden contagiarse los “susceptibles” que son solo una fracción de la población. En este sentido el experto sostuvo que "el problema puntual es reconocer cual es el número reproductivo básico, entonces **para encontrarlo tomamos cuanto tiene que valer ese número para explicar la curva de contagios en las últimas dos semanas".**

"Nosotros con esto elaboramos una medida del contacto social. Para exponer a muchas personas a ser contagiadas del virus hay que estar en contacto con muchas personas. Para adelante, lo que hacemos es suponer que el contacto social sigue igual, que empeora o mejora en un 25%.", concluyó el investigador del Conicet.