---
layout: Noticia con imagen
author: Nota de la redacción de 3DNoticias
resumen: Delta del Paraná
category: Estado Real
title: Nación entregó equipamiento para los Faros de Conservación ubicados en
  las islas del Delta del Paraná
entradilla: Es para fortalecer la presencia institucional y operativa en el
  territorio, y proteger el humedal. “Hoy podemos mostrar con hechos cómo
  estamos tomando cartas en el asunto”, aseguró Gonnet.
date: 2020-11-21T12:19:42.123Z
thumbnail: https://assets.3dnoticias.com.ar/faros.jpg
---
La ministra de Ambiente y Cambio Climático de Santa Fe, Erika Gonnet, participó este viernes del acto en el que el ministro de Ambiente y Desarrollo Sostenible de la Nación, Juan Cabandié, entregó equipamiento para los tres faros de conservación ya emplazados en la zona del delta del río Paraná.

Las herramientas entregadas consisten en cámaras de vigilancia, estaciones meteorológicas, drones, elementos de comunicación, y distintos tipos de vehículos para trabajar en los Faros de Conservación, que son una respuesta integral al problema ambiental del delta y que consiste en establecer una red de áreas protegidas para fortalecer la presencia institucional y operativa en el territorio, y proteger el humedal.

“Si no tendríamos un Ministerio de Ambiente a nivel nacional que nos haya asistido en toda esta situación que venimos pasando desde principio de año sería peor, hoy contamos con helicópteros y aviones hidrantes que en estos momentos están trabajando, y sin esas herramientas para las provincias sería muy complicado”, señaló Gonnet, durante el acto llevado a cabo en la Reserva Natural “Isla del Sol” de Villa Constitución.

![](https://assets.3dnoticias.com.ar/faros1.jpeg)

La ministra precisó que “la provincia va a contar con tres Faros de Conservación, esto que venimos a hacer es mostrar los hechos concretos”, indicó, y añadió: “Nosotros seguimos dando respuestas, y vamos a seguir aceptando todas las críticas que nos hacen. Hoy podemos mostrar con hechos cómo estamos tomando cartas en el asunto, y podemos dar cuenta de eso”.

Además, señaló que la presencia del ministro en la provincia “da mucha tranquilidad” porque “está cerca, se hace cargo, está dando respuesta a las demandas”.

## **Presencia constante**

“Estamos cumpliendo lo que habíamos comentado, estamos presentando 11 drones, 33 personas que se formaron y capacitaron para manejar esos drones, equipamiento meteorológico, y vamos a hacer entrega de 100 cámaras trampas, y de temperatura”, detalló Cabandié en referencia a los elementos entregados.

explicó que “en esto faros la presencia de Parque Nacionales va a ser constante, y establece una estrategia de cercanía con los isleños. Son cuatro faros, queremos que sean más, la proyección que tenemos en las 3 millones de hectáreas del delta es tener 20 faros, y de esa manera tener una cobertura amplia, para que podamos tener alertas tempranas, y al mismo tiempo evitar los fuegos a partir de la modificación de métodos productivos junto a los isleños”.

Finalmente, remarcó la “necesidad de poder pensar en un nuevo paradigma para el buen vivir de las próximas generaciones; tenemos que poder sobre la mesa la necesidad de modificar métodos que degradan lo ambiental, y que producen daño sanitario y ambiental irreparable”.

A su turno, el intendente local, Jorge Berti, definió a la actividad como “un hecho histórico para la ciudad ante hechos históricos que están sucediendo en el mundo, y se están dando esas respuestas históricas que se necesitan”.

“Se necesitaba tomar decisiones, y vinimos a hacernos cargo de los problemas”, dijo Berti, y convocó a “seguir trabajando, a no bajar los brazos, esto lo podemos hacer entre todo sin que nadie quede afuera”.

En tanto, el presidente de la Administración de Parques Nacionales, Daniel Somma recordó que “en agosto compartimos con el ministro y el gobernador la puesta en marcha de esta política pública”, dijo, y añadió: “Este es un tesoro que tenemos que conservar todos, con ciencia y tecnología, y el esfuerzo de toda la familia parquera”.

![](https://assets.3dnoticias.com.ar/faros2.jpeg)