---
category: La Ciudad
date: 2022-01-05T06:00:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/Veranocapital.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Verano Capital: agenda del municipio para enero'
title: 'Verano Capital: agenda del municipio para enero'
entradilla: "La Municipalidad informa la grilla de actividades diseñada para enero.
  Habrá cultura, recreación y deportes en diferentes barrios de la ciudad.\n\n"

---
Hace algunas semanas, la Municipalidad volvió a poner en marcha el programa Verano Capital. El plan se propone elaborar una grilla de actividades recreativas, deportivas y culturales que se renueva semanalmente, desarrollándose en distintos barrios de la ciudad. Todas las propuestas oficiales son abiertas y gratuitas, y están pensadas para todas las edades.

La intención es que vecinas y vecinos de la ciudad se apropien de los espacios públicos, por lo que la mayoría de las sedes son lugares muy concurridos durante el verano. No obstante, la idea de descentralizar los escenarios se mantiene, de modo que los barrios tengan espacios de referencia y de fácil acceso. Tal es el caso de la Estación Mitre, que estrena una agenda colmada de actividades para el disfrute de los habitantes del sur del ejido urbano.

El detalle de cada una de las propuestas está disponible en las redes sociales oficiales de la Municipalidad ([Facebook](https://www.facebook.com/SantaFeCapitalok), [Instagram](https://www.instagram.com/santafecapitalok/?hl=es-la) y [Twitter](https://twitter.com/_SantaFeCapital)) o en la web [www.santafeciudad.gov.ar](http://www.santafeciudad.gov.ar/).

**Para las infancias**

En los parques del Sur, Garay, Federal y Estación Mitre, todos los sábados y domingos de verano, habrá una oferta lúdica. Entre otros espectáculos, se programó

* 16 de enero: Circo Litoral y Teatro de Títeres Municipal en el Parque del Sur (Av. Illia y San Jerónimo), de 18 a 20 horas
* 23 de enero: Circo Litoral y Mago Fernán en el Parque Garay (Salvador Caputto 3800), de 18 a 20 horas
* 30 de enero: Circo Litoral y Mago Fernán en el Parque del Sur (Av. Illia y San Jerónimo), de 18 a 20 horas

**Cine al aire libre**

El ciclo volverá este verano. En enero, las proyecciones serán:

* 11 de enero: “El hijo del otro”, en el Parque del Sur, a las 19 horas
* 18 de enero: “También la lluvia”, en la Costanera Oeste, a las 19 horas
* 25 de enero: “Short Term 12”, en el Parque del Sur, a las 19 horas

**“Mi ciudad como turista”**

Los paseos seguirán programándose toda la temporada. La posibilidad de completar las visitas con un guía especializado y de manera gratuita, se mantendrá aunque con horarios modificados, adaptados al verano:

* Sábado 8 de enero a las 19 horas: Casco Histórico
* Sábado 15 de enero a las 19 horas: Paseo Peatonal
* Sábado 22 de enero a las 19:30 horas: Paseo Cementerio
* Sábado 29 de enero a las 20 horas: Manzana Jesuítica, que incluirá la propuesta “Relatos, estrellas y juegos”

Cabe recordar, además, que los circuitos pueden realizarse de manera autoguiada, scanneando los códigos QR disponibles en los puntos de partida o a través de [Spotify](https://open.spotify.com/show/0TghWaTuOn4LAnbja2YuMC) y de la [web de la Municipalidad](https://santafeciudad.gov.ar/secretaria-de-produccion-y-desarrollo-economico/turismo/mi-ciudad-como-turista/).

**Música en los paradores**

La música se dará cita en los paradores de playa todos los fines de semana del mes. Desde las 20 horas, habrá bandas locales en vivo y DJ’s para convocar a las juventudes:

* 15 de enero en Pinta Parador: “Los Carpinchos”
* 22 de enero en Santa Kite: “Interfaces”
* 29 de enero, en Parador 5: Gonzalo Villarino

**Deportes**

La actividad física a nivel recreativo fue una constante durante todo el año, en los diferentes espacios. Este verano se suman disciplinas en los siguientes horarios:

* Atletismo: lunes, miércoles y viernes, de 19 a 20 horas en la Dirección de Deportes (Av. Almirante Brown 5294)
* Beach vóley: martes, miércoles y jueves de 8.30 a 10 horas en la dirección de Deportes; martes, miércoles y jueves de 18 a 20 horas en el Parador Santa Fe de la Costanera Este
* Beach wrestling: martes y jueves de 18 a 19 horas para chicos de entre 10 y 13 años; y de 19 a 20 horas, a partir de los 13 años, en ambos casos, en la Dirección de Deportes
* Club de corredores**:** lunes a viernes de 9 a 10:30 horas en la Estación Mitre


* Educación Física Infantil (EFI): lunes, miércoles y viernes de 10 a 11:30; martes y jueves de 17 a 18:30 horas en la Estación Mitre


* Funcional: lunes, miércoles y viernes de 17 a 19 en el Polideportivo de Alto Verde; martes y jueves, de 17 a 19 horas en la Costanera Este.
* Fútbol beach: lunes, miércoles y viernes, de 17 a 19 horas en la Dirección de Deportes
* Gimnasia: lunes a jueves de 9 a 10:30; y viernes de 17 a 18:30 horas en la Estación Mitre
* Mami vóley**:** martes y jueves de 20 a 22 horas en barrio el Pozo; y los mismos días, de 17:30 a 19:30 en el Polideportivo de Alto Verde
* Ritmos: martes y jueves de 17 a 19 horas en el Polideportivo de Alto Verde; y los sábado de 17 a 20 horas en el Parador Santa Fe de la Costanera Este
* Rugby**:** lunes y miércoles de 17:30 a 19 horas en la Dirección de Deportes
* Tenis: lunes, miércoles y viernes, de 7:30 a 9 horas; martes y jueves, de 17 a 19 horas en la Dirección de Deportes
* Vóley: miércoles y viernes de 17:30 a 19 horas en la Estación Mitre
* Newcom: lunes, miércoles, viernes y sábados de 18.30 a 20 horas en la Dirección de Deportes

**Museos**

Durante enero, los museos de la ciudad permanecerán abiertos para ofrecer una alternativa no sólo a los turistas, sino también a los vecinos y vecinas de la capital que pueden visitarlos en vacaciones. Los horarios serán los siguientes:

* Casa Museo César López Claro (Piedras 7352): miércoles a sábados, de 9:30 a 12:30
* Centro Experimental del Color (bulevar Gálvez 1150): miércoles a domingos, de 17 a 20
* Museo de la Constitución Nacional (1° de Mayo y Avenida de la Constitución): jueves a domingos, de 9:30 a 12:30 y de 17 a 20
* Museo del Colegio Inmaculada (San Martín 1540): miércoles a sábados, de 9.30 a 12.30 y de 17 a 20
* Museo Municipal de Artes Visuales Sor Josefa Díaz y Clucellas (San Martín 2068): miércoles a domingos, de 17 a 20