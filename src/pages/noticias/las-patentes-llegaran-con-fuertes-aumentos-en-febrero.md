---
category: Estado Real
date: 2021-01-25T09:27:44Z
thumbnail: https://assets.3dnoticias.com.ar/patente.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Las patentes llegarán con fuertes aumentos en febrero
title: Las patentes llegarán con fuertes aumentos en febrero
entradilla: El titular del organismo, señaló que los incrementos se darán por una
  modificación en la base imponible sobre el valor de los vehículos, que no realiza
  la provincia. Algunos vehículos pagarán el doble que en 2020.

---
Las patentes llegan con sustanciales incrementos. El titular de la Administración Provincial de Impuestos  (API), Martín Ávalos, confirmó que el organismo estatal estudia medidas para “morigerar” los aumentos en las patentes automotores, que están siendo repartidas en los domicilios. Por los incrementos en la base imponible -el valor de los vehículos- están llegando boletas, dependiendo el modelo del vehículo, de más del doble de los montos que se pagaban en 2020.

Avalos explicó que en el caso de la patente automotor “no se hizo ningún tipo de modificación de alícuotas, lo único que cambió es la base imponible que no depende de la provincia y que es la valuación de los vehículos", y explicó: "Hemos visto que esta tabla de valuaciones ha generado unos saltos muy importantes en algunos tipos de vehículos. Estamos estudiando una serie de medidas al respecto para ver de qué manera se puede morigerar esto".

Para Ávalos, hay actualizaciones que son razonables, en el orden del 25 o 30 por ciento, y hay otros vehículos que  han tenido un salto de un 70 o un 80 por ciento en el valor. Eso aplicada luego la alícuota da un valor de patente a pagar elevado”, agregó.

En ese sentido, recordó que “los vencimientos en las patentes recién empiezan a operar para fines de febrero” por lo que “hay un margen para evaluar posibles medidas”. Si bien desde el organismo se busca llevar tranquilidad a los propiertarios de vehículos a los que ya les llegó la patente con los nuevos montos, la realidad es lo que dice el papel.

**INMOBILIARIOS, RURAL Y URBANO**

Con respecto al Impuesto Inmobiliario Urbano y Rural, desde API se informó que se propuso la suspensión del coeficiente de convergencia con la cual la repotenciación que se daba año a año, en este 2021 no se va a dar. Se trata de una actualización mínima, muy por debajo de los índices de inflación, y que en promedio es del 22 por ciento en el Urbano y el 24 por ciento en el Rural, en promedio.

Respecto a Ingresos Brutos y Sellos, se informó que no se hacen modificaciones. Lo único que hay para destacar son beneficios. Para todos los sectores afectados por la pandemia se establecen exenciones en el caso de las Declaraciones Juradas entre los vencimientos de septiembre de 2020 a marzo de 2021 inclusive, lo que significa un alivio para los sectores afectados.

**Atención en el API**

Un dato a tener en cuenta es que en la página del gobierno de la provincia de Santa Fe se indica que aún no se pueden imprimir las boletas de patentes del 2021. La explicación se da con la siguiente leyenda:

Atención: La impresión de boletas del año 2021 aún no se encuentra disponible.

Si posee cuotas pendientes de liquidar de años anteriores, por favor sírvase efectuar la liquidación correspondiente haciendo click aquí