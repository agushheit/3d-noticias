---
category: Agenda Ciudadana
date: 2021-02-09T08:17:15-03:00
thumbnail: https://assets.3dnoticias.com.ar/jxc.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Juntos por el Cambio pide la vuelta a las aulas
title: Juntos por el Cambio pide la vuelta a las aulas
entradilla: Es una iniciativa nacional que tendrá su réplica este martes en la provincia.
  Se harán clases abiertas en diferentes ciudades. En esta capital será desde las
  18 frente al Ministerio de Educación.

---
Concejales, diputados provinciales, referentes de Juntos por el Cambio pero también padres y madres de alumnos que concurren a escuelas de diferentes localidades santafesinas reclamaron por el retorno de la presencialidad a las aulas. Lo hicieron a través de una conferencia de prensa en la que se anunció un cronograma de actividades que se realizarán este martes en todo el territorio provincial con aulas abiertas para visibilizar el tema. En rigor, la iniciativa es de índole nacional y tendrá su réplica en Santa Fe.

En diálogo con los medios, los dirigentes plantearon su preocupación por "la falta de protocolos claros y por la ausencia de coordinación del transporte público de pasajeros para acompañar el proceso de vuelta a clases". Como se sabe, el desplazamiento tanto de alumnos como de docentes y asistentes escolares es uno de los aspectos que más preocupa a la hora de garantizar todos los cuidados sanitarios recomendados.

> La actividad que planifica Juntos por el Cambio es similar a otras que ya se han realizado durante los últimos meses. Se denominará "Juntos por la Educación", y se extenderá de 9 a 18.

El planteo de este sector político se sustenta en que "los estudiantes son los más conscientes de la necesidad de no perder más tiempo y de la importancia que tiene el hecho de regresar a las aulas con las medidas de prevención adecuadas".

En cuanto al esquema que se prevé para esta ocasión, los dirigentes anunciaron una sucesión de "clases abiertas" que se llevarán a cabo de manera simultánea. En tal sentido, anunciaron que a las 18 de este 9 de febrero, la actividad se concentrará frente al Ministerio de Educación, en el caso de la ciudad de Santa Fe. En Rosario, en tanto, el lugar de convocatoria será el Monumento a la Bandera. En Reconquista, será en la Plaza Central; en San Jorge, en la Plaza San Martín; en Firmat, en el parque "12 de Octubre"; en San Lorenzo, en plaza General San Martín; en Rafaela, en Plaza Pizzurno; en Casila, en la Plaza de los Mástiles; y en San Justo, en la explanada de la Escuela Normal.

![](https://assets.3dnoticias.com.ar/Etu5Z7kWYAI5qHr.jpg)