---
category: Agenda Ciudadana
date: 2021-01-11T10:42:23Z
thumbnail: https://assets.3dnoticias.com.ar/tren-carga.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: El Litoral'
resumen: 'Transporte de cargas: los costos aumentaron un 35% en 2020'
title: Los costos del transporte de cargas aumentaron un 35% en 2020
entradilla: "«Se continuó con una marcada aceleración de los costos que se iniciara
  en agosto, luego de que durante la primera parte del año se registraran valores
  por debajo del 2%», según el estudio de FADEEAC."

---
Los costos del transporte de carga aumentaron 3% en diciembre, con una marcada aceleración en el segundo semestre, que llevó el indicador anual a un alza del 35%.

Así surge del índice elaborado por la Federación Argentina de Entidades Empresarias del Autotransporte de Cargas (FADEEAC), dado a conocer en las últimas horas.

La cámara empresarial alertó sobre la «evidente aceleración de los costos en los últimos cinco meses, donde buena parte de los rubros exhibió fuertes aumentos».

Los indicadores del 2020 se ubican por debajo de los años anteriores, ya que en 2018 habían subido 61,5% y en 2019 un 47%.

El incremento de diciembre se produce luego del aumento récord de 5,52% en octubre y el aumento del 3,04% en noviembre.

«Se continuó con una marcada aceleración de los costos que se iniciara en agosto, luego de que durante la primera parte del año se registraran valores por debajo del 2%», según el estudio de FADEEAC.

El estudio, realizado por el Departamento de Estudios Económicos y Costos de la entidad, mide 11 rubros que impactan directamente en los costos de las empresas de autotransporte de cargas.

FADEEAC señaló que el aumento en los combustibles de diciembre llevó el aumento anual del gasoil al 20,8%, tras haberse incrementado 45,5% en 2019 y 77% en 2018.

El trabajo resaltó también los incrementos en neumáticos (6.68%), material Rodante (5.60%), reparaciones (3.37%), lubricantes (6.80%), seguros (2.18%) y gastos generales (0.36%), en diciembre en comparación con noviembre último.

En todo el año, los precios de los neumáticos lideraron el incremento de costos, con un aumento del 71,2%.

El estudio de FADEEAC concluyó que el aumento acumulado del año anterior «cierra con un número muy cercano al de la inflación minorista».

<br/>

## **Inflación**

El INDEC dará a conocer el jueves el Índice de Precios al Consumidor (IPC) de cobertura nacional correspondiente a diciembre, mes para el cual las consultoras privadas estimaron incrementos de entre 3,6% y el 4%, con lo cual el acumulado anual cerraría en torno del 35%.

El ministro de Economía, Martín Guzmán, estimó el viernes que «2020 terminará alrededor de 35%», dentro del rango que buscaba el Gobierno nacional, y ratificó que la idea es «que la inflación se vaya reduciendo 5 puntos porcentuales año a año», tal como lo refleja el Presupuesto 2021.

El jueves, el INDEC dará a conocer su Índice de Precios al Consumidor que registró en noviembre un incremento del 3,2%, con lo que mostró una desaceleración respecto del aumento de 3,8% verificado en octubre.

Sobre la base de esos números, la inflación acumulada en los primeros once meses del año es de 30,9%, 17,4 puntos porcentuales menos que en igual período de 2019.

Para diciembre, de acuerdo al Relevamiento de Expectativas del Mercado (REM) que realiza el Banco Central, los analistas estimaron que la tasa de inflación mensual sería de 4,0% en el último mes del año 2020.

En el caso de la previsión que realiza la consultora que conduce el economista Orlando Ferreres, los precios subieron en diciembre 3,6% promedio y en el año acumularon un alza de 33,6% interanual, de acuerdo al índice propio.

Para la consultora Ecolatina el IPC estimado de diciembre fue de 4% en nivel general, confirmando una aceleración respecto de noviembre.