---
category: Agenda Ciudadana
date: 2021-04-25T06:15:44-03:00
thumbnail: https://assets.3dnoticias.com.ar/CORTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: El Gobierno pule la presentación ante la Corte y apunta a las "contradicciones"
  de la Ciudad
title: El Gobierno pule la presentación ante la Corte y apunta a las "contradicciones"
  de la Ciudad
entradilla: Este martes, en las dos primeras horas del día judicial, vencerá el plazo
  de cinco días hábiles que le dio la Corte Suprema al Estado Nacional para ofrecer
  prueba. El proceso lleva los tiempos del trámite "sumarísimo".

---
El gobierno nacional presentará este lunes ante la Corte Suprema de Justicia las pruebas que llevaron al presidente Alberto Fernández a firmar días atrás el Decreto de Necesidad y Urgencia (DNU) en el que se dispuso que se suspendiera la presencialidad en las escuelas del Área Metropolitana de Buenos Aires (AMBA) por un período de 15 días, hasta el próximo viernes 30.

La presentación estaba terminando de ser pulida durante este fin de semana por miembros de la Procuración del Tesoro de la Nación (PTN), que agrupa a los abogados del Estado Nacional, junto con integrantes del equipo jurídico de la jefatura de Gabinete, según informaron a Télam fuentes seguras.

Este martes, en las dos primeras horas del día judicial, vencerá el plazo de 5 días hábiles que le dio la Corte Suprema al Estado Nacional cuando le notificó la demanda para que pudiera ofrecer prueba y dispuso imprimirle al proceso los tiempos del trámite "sumarísimo".

**La posición del Gobierno**

La presentación de la PTN, que encabeza Carlos Zannini, apuntará a demostrar que –contra lo que dijo el Gobierno de la Ciudad de Buenos Aires- el DNU 241 es constitucional porque dado que no elimina ningún derecho, sino que restringe alguno –como la presencialidad en las escuelas del AMBA- para preservar la salud de la población en un contexto de emergencia sanitaria por la segunda ola de coronavirus.

La presentación incluirá, además, un informe elaborado por el Ministerio de Salud que incluirá los datos epidemiológicos y los indicadores en los cuales se basó el Gobierno Nacional para dictar la medida que cuestionó la administración porteña.

La negativa de Larreta a acatar el DNU y su decisión de judicializarlo a través de la presentación de una acción declarativa de inconstitucionalidad "encierra una contradicción: el propio jefe de Gobierno porteño había avalado durante todo 2020 las medidas del Gobierno Nacional, muchos más restrictivas que esta que ahora cuestiona", indicaron a Télam fuentes del Poder Ejecutivo.

La presentación estaba terminando de ser pulida durante este fin de semana por miembros de la Procuración del Tesoro de la Nación, que encabeza Carlos Zannini.

La presentación estaba terminando de ser pulida durante este fin de semana por miembros de la Procuración del Tesoro de la Nación, que encabeza Carlos Zannini.

**“Una restricción y no la eliminación de un derecho”**

La presentación buscará poner de relieve que el decreto con el que se dispuso la suspensión de la presencialidad en las escuelas del AMBA durante 15 días implicó "una restricción y no la eliminación de un derecho; es algo que se puede hacer cuando está en juego otro derecho superior", señalaron a Télam abogados que trabajan en la presentación.

Se resaltará además que se trató de una medida temporal dispuesta en un contexto de emergencia y se citarán decenas de antecedentes jurisprudenciales que avalarían la decisión del Ejecutivo, de los cuales la más añeja data de 1887, según pudo reconstruir en el tiempo.

La decisión de utilizar todo el plazo que otorgó la Corte para responder la demanda y no hacerlo antes tuvo que ver con que en el gobierno nacional consideran que el que está en falta es el jefe de Gobierno porteño que, tras negarse a acatar el DNU, hizo lo propio con un fallo judicial del fuero Contencioso Administrativo.

La presentación buscará poner de relieve que el decreto con el que se dispuso la suspensión de la presencialidad en las escuelas del AMBA durante 15 días implicó "una restricción y no la eliminación de un derecho".

La presentación buscará poner de relieve que el decreto con el que se dispuso la suspensión de la presencialidad en las escuelas del AMBA durante 15 días implicó "una restricción y no la eliminación de un derecho".

**Las contradicciones señaladas**

Ocurre que Larreta basó su decisión de dar continuidad a las clases presenciales en un fallo de la sala cuarta de la Cámara de Apelaciones en lo Contencioso Administrativo, Tributario y de Relaciones del Consumo de la Ciudad de Buenos Aires que ordenó dejar sin efecto el artículo del DNU referido a ese tema, pero luego se negó a acatar otro de un juez en lo Contencioso Administrativo Federal que anulaba al primero.

"La otra contradicción de Larreta es que él mismo se presentó ante la Corte reconociendo que se trataba de una cuestión federal pero luego desacató un fallo del fuero federal e hizo lo que le indicaba uno de un tribunal porteño. ¿Por qué no hizo entonces su propia presentación ante el Tribunal Superior de Justicia de la Ciudad?", se preguntó un abogado que trabaja en los aspectos técnicos del escrito que presentará el Gobierno Nacional.

En el máximo tribunal, en tanto, esperan la presentación del Gobierno Nacional para correrle inmediata vista a la Procuración General de la Nación que ya se expidió en su momento en favor de la competencia originaria de la Corte Suprema pero no opinó sobre el fondo de la cuestión.

"La otra contradicción de Larreta es que él mismo se presentó ante la Corte reconociendo que se trataba de una cuestión federal pero luego desacató un fallo del fuero federal".

"La otra contradicción de Larreta es que él mismo se presentó ante la Corte reconociendo que se trataba de una cuestión federal pero luego desacató un fallo del fuero federal".

**Una decisión "no judicial"**

Fuentes del máximo tribunal señalaron que, de todas formas, cada uno de los jueces ya trabaja en su propia posición, aunque para terminar de definir los votos esperarán a ver los datos que presente el Gobierno Nacional.

También desde el máximo tribunal señalaron que la presentación de Larreta incluía algunas deficiencias como el hecho de que fue presentada un viernes y no incluyó la solicitud de que se habilitaran horas y días inhábiles por lo que el pedido de que se dictara una medida cautelar que suspendiera los efectos del DNU no fue tratado.

"Se trató de una cuestión mal planteada por el demandante", señalaron fuentes que habitan la Corte Suprema que, además, señalaron que no debería ser un tribunal judicial el que decida si los chicos y chicas deben o no concurrir a los colegios.