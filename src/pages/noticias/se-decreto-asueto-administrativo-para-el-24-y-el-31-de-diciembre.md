---
category: La Ciudad
date: 2021-12-21T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/municipalidadssueto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Se decretó asueto administrativo para el 24 y el 31 de diciembre
title: Se decretó asueto administrativo para el 24 y el 31 de diciembre
entradilla: "La administración pública municipal trabajará sólo con guardias mínimas,
  los viernes previos a las celebraciones de Navidad y Año Nuevo.\n\n"

---
La Municipalidad informa que este viernes 24 de diciembre y el próximo 31 de diciembre se decretó asueto administrativo para el personal. Es en el marco de la celebración de las tradicionales fiestas de Navidad y Año Nuevo.

Se mantendrán, de todas maneras, las guardias mínimas para garantizar la atención ciudadana en lo referente a servicios esenciales.