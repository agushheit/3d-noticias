---
category: La Ciudad
date: 2021-11-18T06:00:16-03:00
thumbnail: https://assets.3dnoticias.com.ar/videovigilancia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'El "Gran hermano": ¿cuántas cámaras de seguridad hay en Santa Fe y cuáles
  funcionan?'
title: 'El "Gran hermano": ¿cuántas cámaras de seguridad hay en Santa Fe y cuáles
  funcionan?'
entradilla: Algunas dependen del municipio y otras, de provincia. Pero no todas están
  activas. ¿Qué pasó con la vinculación entre ambos sistemas? Los vecinos esperan
  respuestas.

---
¿Cuántas cámaras de seguridad hay en la ciudad? ¿Cuántas funcionan hoy? ¿Qué barrios o zonas de la ciudad cuentan con videovigilancia? ¿Qué pasó con la unificación entre los sistemas municipal y provincial? Estas son algunas de las preguntas que se hacen los vecinos todos los días en los barrios de la ciudad. El Litoral se las trasladó a las autoridades.

 El tema principal es la prevención del delito. Y las cámaras contribuyen a monitorear distintas zonas a donde ocurren. Todos los reclamos de los vecinos son canalizados institucionalmente a través de las vecinales y, a su vez, existe una red que las integra, la cual hace las veces de representante ante las autoridades municipales y provinciales. Una de las caras visibles de esa red es la vecinalista de barrio Fomento 9 de Julio, Susana Spizzamigio. Ella participa de las reuniones a donde se discuten las medidas de seguridad y dice que todavía esperan "respuestas".

"En la zona norte no hay cámaras en avenidas", advierte la vecinalista, "creo que llegan hasta el Cementerio", arriesga. "Y en Aristóbulo del Valle y otras avenidas no hay cámaras porque el cableado es muy caro", cuenta. "Están en perspectivas para ampliarlo pero no se hizo", dice, en base a lo que le responden en las reuniones.

 **Ojos urbanos**

 Esta semana el Municipio anunció la instalación de nuevas cámaras de videovigiliancia para el centro y sur de la ciudad. Las mismas eran instaladas en San Martín y Monseñor Zazpe, en San Martín y General López, y General López y San Jerónimo. Y en una segunda etapa, se continuará por General López, hasta la Estación Mitre. Las imágenes se recepcionan en el Centro de Monitoreo Municipal.

 Tras el anuncio, El Litoral consultó al secretario de Control y Convivencia Ciudadana municipal, Fernando Peverengo, cuántas son las cámaras con las que cuenta la ciudad y cuántas de ellas están en funcionamiento.

 "Nosotros (la Municipalidad) tenemos 190 cámaras y vamos a llegar a 194", contestó Peverengo. "Mientras que la Provincia tiene más de 400 cámaras", dijo. Y sobre las que están en funcionamiento, contestó: "Hoy por hoy tenemos operativas 132 cámaras municipales". Luego mencionó el funcionario que están aguardando la resolución de un expediente "para activar 23 cámaras (de las 62 que no funcionan) de un tendido que fue vandalizado, porque se robaron la fibra óptica". Y dijo que el mantenimiento es permanente, "porque se caen algunas y se activan otras todo el tiempo".

 Por su parte, ante la consulta de El Litoral desde el Ministerio de Seguridad provincial se informó que cuentan con otras 156 cámaras domo, 260 cámaras fijas y 34 LPR (cámaras lectora de patentes). De ese total, en la actualidad hay 13 puntos de monitoreo sin conexión.

 Además, desde la provincia están gestionando $ 20 millones del plan Incluir para la instalación de nuevas cámaras en la ciudad.

 Ante la consulta respecto de qué zonas de la ciudad son las que cuentan con videovigiliancia y cuáles todavía no lo tienen, el secretario de Control y Convivencia Ciudadana municipal dijo que las municipales están en "la zona del Metrofé, la Peatonal y algún punto del Centro". Mientras que la provincia monitorea el resto de las avenidas y centros comerciales, según informaron voceros de Seguridad, aunque no especificaron cuáles son esas avenidas.

 Un tema pendiente es la unificación entre ambos sistemas de videovigilancia, el municipal y el provincial. "Seguimos esperando para compatibilizar el sistema con el de la Provincia, para que no estemos mirando lo mismo con la superposición de recursos humanos y tecnología", dijo Peverengo.

 -El ministro de Seguridad provincial, Jorge Lagna, había dicho tras la última reunión del Consejo de Seguridad en abril que la compatibilización era inminente. ¿En qué quedó eso?

 -Seguimos esperando que los técnicos puedan avanzar para compatibilizar ambos sistemas y solucionarlo.

 -¿Es posible ampliar la red de cámaras de seguridad a los barrios?

 -La Municipalidad tiene previsto controlar las áreas comerciales, a donde hay mayor tránsito de personas. En lo que estamos avanzando es el tema de las alarmas comunitarias e institucionales.

 Tras la reunión del Consejo de Seguridad que se hizo en abril pasado, el ministro de Seguridad anunció la creación de un Centro de Operaciones Policiales y de Análisis Criminal (COP). "Habrá un gran salto tecnológico para contener todo lo que ocurra en la calle en una central -dijo Lagna-. El mismo se concretará con un aporte nacional de 3 mil millones de pesos y estará en funcionamiento en septiembre u octubre". Es decir que ya debería estar implementado.

 En una entrevista realizada por El Litoral la semana pasada, el intendente Emilio Jatón confesó: "Cuando llegamos (diciembre 2019) teníamos el 50% de las cámaras que no funcionaban, hoy ya casi funcionan todas y vamos a incrementarlas, también las alarmas comunitarias. Tenemos cámaras obsoletas e incompatibles con el sistema provincial y estamos trabajando para compatibilizar los dos sistemas con la idea de que ellos monitoreen la seguridad y nosotros la ciudad para no superponernos. Pero se necesita inversión".

 La vecinalista Spizzamiglio lo resume perfecto: "El software de Provincia y el de la Municipalidad son distintos y no están coordinados. Este problema viene desde la gestión socialista. Es inminente la solución pero no sabemos si se hizo".

 Y desde el Ministerio de Seguridad reconocieron que "todavía los técnicos se encuentran tratando de solucionar el problema de compatibilidad que hay entre ambos sistemas".

 Mientras tanto, en los barrios siguen esperando más videovigiliancia. "En Fomento 9 de Julio contamos con las cámaras de Jerárquicos (un privado), así que al menos tenemos eso", apunta la vecinalista. "Lo que hace falta es que el municipio tome cartas en materia de seguridad, fortaleciendo la prevención con la GSI, con más móviles, y ampliar la red de cámaras a todos los barrios y no sólo a las avenidas", finalizó Spizzamiglio.