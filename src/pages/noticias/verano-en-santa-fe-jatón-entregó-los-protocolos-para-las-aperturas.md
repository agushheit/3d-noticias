---
layout: Noticia con imagen
author: "Fuente: LT10"
resumen: "Verano: Protocolos de apertura"
category: La Ciudad
title: "Verano en Santa Fe: Jatón entregó los protocolos para las aperturas"
entradilla: El intendente se reunió con la ministra de Salud provincial, Sonia
  Martorano para evaluar las acciones que prepara el municipio para la
  habilitación de distintas actividades y espacios públicos.
date: 2020-11-20T11:21:27.611Z
thumbnail: https://assets.3dnoticias.com.ar/jaton-martorano.jpeg
---
La ministra de Salud provincial, Sonia Martorano y el intendente Emilio Jatón se reunieron este jueves para coordinar las acciones de control y cuidados para la temporada de verano en la ciudad.

De esta manera, Jatón presentó tanto el **operativo en el que estuvo trabajando la Municipalidad**, como **los protocolos para las distintas actividades y espacios públicos**. Los mismos se pondrán en marcha cuando en la capital se inicie oficialmente la temporada estival.

La intención es que todas las actividades se desarrollen de manera segura, tanto para los santafecinos como para los turistas que arriben a disfrutar de sus vacaciones en la capital provincial.

Según se indicó, la lista entregada por el intendente contempla protocolos para agencias de viajes y turismo; visitas guiadas; turismo náutico; turismo MICE; turismo de reuniones; turismo de naturaleza; playas, balnearios y solariums; hotelería y gastronomía; centros de información turística; espectáculos públicos en el Anfiteatro Juan de Garay; salas públicas y privadas (música, teatro circenses, danzas, poesía); estaciones; bares con programación; espectáculos culturales en espacios públicos (plazas, parques, paradores, calles y veredas).

**“Se viene una etapa donde habrá aperturas y hay que estar codo a codo trabajando”**, resaltó el mandatario local.

Al respecto, adelantó que ya hay un cronograma para habilitar actividades en diciembre y enero como la apertura de playas, paradores, parques y los espectáculos al aire libre.

En esta línea, Martorano señaló que “seguimos con una mesa de trabajo constante para tener el verano más seguro”.

“En la ciudad de Santa Fe tenemos playas y mucho espacio público, por lo que hoy empezamos a trabajar en una mesa con la presentación de lo que hizo cada equipo, para evaluar los protocolos y ver los probables cronogramas”, dijo en cuanto a la habilitación de actividades para el verano 2020-2021.

Además, la funcionaria planteó su preocupación acerca de la ocupación de camas que ronda el 90%, donde una parte importante son no covid. “Cuando analizamos la situación en contexto vemos que tenemos casos covid, pero muchas camas no covid relacionadas con accidentología”.

No obstante, informó que en los últimos días se reforzaron las camas en terapia, los respiradores y las camas generales. **“Santa fe va a poder dar la respuesta pertinente, pero lo más importante es el cuidado de cada uno de nosotros”**, destacó la ministra.

En este sentido, con el municipio apuntan a reforzar los controles dentro y fuera de la ciudad, pero sobre todo apelan a la responsabilidad individual.