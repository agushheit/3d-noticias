---
category: La Ciudad
date: 2020-12-11T11:00:08Z
thumbnail: https://assets.3dnoticias.com.ar/jaton1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3DNoticias
resumen: 'Reparación histórica: se rectificaron los legajos de trabajadores municipales
  detenidos-desaparecidos'
title: 'Reparación histórica: se rectificaron los legajos de trabajadores municipales
  detenidos-desaparecidos'
entradilla: También se incorporaron dos hijos de ex empleados desaparecidos. “Hoy
  es un día histórico que no hay que dejar pasar por alto”, indicó el intendente Emilio
  Jatón.

---
El acto en conmemoración por el Día de los Derechos Humanos que realizó la Municipalidad de Santa Fe tuvo dos momentos de reparación histórica inéditos en la ciudad. 

Por un lado, **se rectificaron los legajos de trabajadores municipales que fueron detenidos desaparecidos durante la última dictadura militar**.

Por otro lado, **ingresaron a planta permanente dos hijos de trabajadores y trabajadoras municipales cesanteados y desaparecidos**. Se trata de Facundo Maggio, hijo de Norma Beatriz Valentinuzzi, y Laura Candioti, hija de Edmundo Jerónimo Candioti.

El acto fue presidido por el intendente Emilio Jatón y contó con la participación de integrantes del Foro por los Derechos Humanos, ex trabajadores municipales que fueron presos políticos, el presidente del Concejo, Leandro González, entre otros.

“Hoy es un día histórico que no hay que dejar pasar por alto”, indicó Jatón. En tal sentido, el mandatario les dio la bienvenida a los trabajadores que se incorporan a la Municipalidad: “Facu y Laura, bienvenidos al municipio, es un derecho que se han ganado. Los derechos humanos son fruto de una conquista de generaciones y este acto de reparación histórica es importante y necesario”.

En ese marco, el intendente consignó: “Desde el Estado estamos haciendo una reparación histórica, hay que seguir defendiendo las conquistas y ampliando derechos”. Y agregó que “este día es muy importante para que nos juntemos, para que pensemos, para que la historia se nos meta en el presente y, con este presente, hagamos un futuro mejor”.

Por su parte, el director de Derechos Humanos y Vinculación, Publio Molinas, destacó: “Es un placer participar de esta jornada histórica en la que se reparan los legajos de compañeros municipales detenidos desaparecidos en la última dictadura militar e ingresan dos hijos, a través de una Ordenanza que fue promulgada dos años atrás. Es un acto histórico que pedían los compañeros hace mucho tiempo y que hoy, gracias a la decisión política del intendente, se pudo concretar”.

En esa línea, Molinas añadió que este “es un acto reparatorio por parte del Estado. Los legajos tenían historias inventadas y es muy simbólico que ahora conste la condición de detenidos desaparecidos”.

Cabe remarcar que la incorporación se enmarca en la ordenanza Nº 12.119, impulsada en su momento por Emilio Jatón.

# **Reparación documental**

En ese marco, Jatón entregó a los familiares las copias del Decreto Nº 278, que en el artículo uno dispone:

> “la inscripción de la condición ‘detenido-desaparecido’ en los legajos de Norma Beatriz Valentinuzzi, Edmundo Jerónimo Candioti, Guillermo Alberto Perot, Edi Ana Cravero, Miguel Oscar Anzardi y Ricardo Marcelino Alvarez, los cuales revistaban como agentes de planta permanente dependientes de la administración pública municipal”.

En consonancia, el texto señala “proceder al saneamiento y reparación documental histórica de sus legajos, dejando constancia en los mismos que los reales motivos que determinaron la interrupción de sus vínculos laborales ha sido consecuencia del accionar del terrorismo de Estado”.

<br/>

![](https://assets.3dnoticias.com.ar/jaton3.jpg)

# **Por los que ya no están**

“Yo hoy puedo repasar aquellos años, pero hay compañeros que no pueden hacerlo”, indicó Miguel Rico, jubilado municipal, preso político e incansable militante por la lucha de los derechos humanos. En ese sentido, Rico destacó la trascendencia de “poder corregir los legajos que decían ‘despedidos por causas injustificadas’; ninguno fue por causa injustificada. Por eso para mí es importante poder traer un pedacito de ese pasado al presente y rescatar la memoria”.

Con respecto a la incorporación de hijos de detenidos desaparecidos, Rico recordó que a uno de ellos “lo tuve alzado de bebé, y hoy ya se convirtió en un hombre”. En consonancia, se manifestó “muy contento por esta reparación, solo resta un lugar en la Municipalidad para llevar una flor a los que ya no están”.

Facundo Maggio es un reconocido percusionista que desde hace años da diversos talleres en los barrios de la ciudad y que, a partir de hoy, trabajará en Cultura de la Municipalidad, como lo hizo su madre, una bailarina que -según aseguran los que la vieron-, deleitó a los espectadores sobre el escenario del Teatro Municipal. Para Facundo, hoy es un día “histórico, estoy muy movilizado por la memoria, los testimonios, el relato de los compañeros sobrevivientes de mi mamá y del padre de Laura”.

“En tiempos como estos, es una celebración tener trabajo, es sumamente importante sumarme a un área donde ya venía trabajando en el territorio”, expresó Facundo y añadió: “el recuerdo permanente a mis padres, en particular a mi madre que fue una artista muy jugada”.

# **Presentes**

También estuvieron el secretario de Gobierno municipal, Nicolás Aimar; el secretario de Educación y Cultura, Paulo Ricci; el director de Derechos y Vinculación Ciudadana, Franco Ponce de León; las concejalas Laura Mondino, Luciana Ceresola, Jorgelina Mudallel, y el concejal Guillermo Jerez.

<br/>

![](https://assets.3dnoticias.com.ar/jaton2.jpg)