---
category: La Ciudad
date: 2021-10-10T06:15:20-03:00
thumbnail: https://assets.3dnoticias.com.ar/DESCACHARrado.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Fumigar no, descacharrar sí: las claves para prevenir el dengue'
title: 'Fumigar no, descacharrar sí: las claves para prevenir el dengue'
entradilla: "La aplicación de insecticidas elimina a mosquitos adultos pero no a larvas
  y huevos. Se recomienda eliminar recipientes que acumulan agua y favorecen la reproducción
  del insecto.\n\n"

---
El Ministerio de Salud, a través de la dirección de Epidemiología, recordó la necesidad de comenzar con acciones individuales, locales y provinciales, para reducir las posibilidades de circulación de Dengue y otras enfermedades transmitidas por el mosquito Aedes aegypti, en un contexto en donde las mismas podrían convivir con el Coronavirus y confundirse, ya que se presentan con algunos síntomas similares.

 Al respecto, la directora de Epidemiología de la provincia, Carolina Cudós, dijo que "a diferencia del verano pasado, dadas las recientes aperturas y flexibilizaciones, en los próximos meses va a haber más movimiento de personas entre provincias y entre países limítrofes; por lugares y zonas en las que hubo dengue todo el año y/o en donde es endémico".

 "Esto supondrá - agregó - mayor riesgo de contagio de estas dos enfermedades, por lo que los cuidados se deberán redoblar. Además, ambas presentan algunos síntomas parecidos, lo cual requerirá una mayor atención al momento de los cuidados, prevención, diagnósticos y tratamientos. Tendremos que evitar que tanto el Dengue como el Coronavirus proliferen".

 Por otra parte, Cudós enfatizó: "No es momento todavía para fumigar ni tampoco esa es la medida más recomendada, porque solo mata mosquitos adultos, en muy poca cantidad; además porque mata y afecta a otras especies de animales e insectos que son depredadores naturales de los mosquitos o que cumplen una función muy importante en el ambiente".

 "Lo que sí ya hay que hacer, y fundamentalmente en el verano, es descacharrar, deshacerse de objetos inservibles y de todo aquello que acumule agua porque con los primeros calores y lluvias comienzan a nacer los mosquitos Aedes aegypti", destacó la directora de Epidemiología.

 Y recomendó: "Comenzar ahora a revisar los patios, el jardín, el techo, la terraza; destapar las canaletas de los techos; observar que los tanques de agua tengan las tapas bien puestas y cerradas. Las zonas en donde hay aljibes o se junta agua para consumo humano o animal, o para trabajar, pueden usarse larvicidas (mata la larva del mosquito impidiendo que llegue a estado adulto)".

 **En cada localidad**

 En otro orden, Carolina Cudós dijo que "ya hay gobiernos locales en distintos puntos de la provincia haciendo las tareas de descacharrado, concientización, consejería casa por casa, algo que cada municipio y comuna tendrá que profundizar en un trabajo territorial, que podrá articularse con equipos provinciales".

 "Es importante comenzar ahora, incrementar estas acciones en verano y sostenerlas hasta el otoño", concluyó.

 **Acciones simples**

 Como se dijo, la fumigación no es suficiente para erradicar al mosquito.

 La aplicación de insecticidas es una medida destinada a eliminar a los mosquitos adultos pero no a los huevos y a las larvas y su implementación debe ser evaluada por las autoridades sanitarias ya que solo se recomienda en momentos de emergencia, y siempre debe ser acompañada por la eliminación de todos los recipientes que acumulan agua en las casas y espacios públicos.

 La medida fundamental es eliminar las posibilidades de alojamiento de los mosquitos para la colocación de huevos. Por eso, no es suficiente ninguna acción si quedan recipientes de agua estancada, en donde depositan huevos.

 "A diferencia del verano pasado, dadas las recientes aperturas y flexibilizaciones, en los próximos meses va a haber más movimiento de personas entre provincias y entre países limítrofes; por lugares y zonas en las que hubo dengue todo el año y/o en donde es endémico".

 Síntomas: la enfermedad se manifiesta con fiebre alta mayor a 38.0º C, dolores musculares intensos, dolor detrás de los ojos, dolor de cabeza, malestar, falta de apetito, y manchas rojas o sarpullido en el cuerpo, que puede picar

 **LA ENFERMEDAD**

El Dengue es una afección viral aguda que afecta a personas de cualquier edad, siendo más grave en personas mayores y con enfermedades crónicas, como la diabetes.

Se transmite a través del mosquito Aedes aegypti, que necesita agua para proliferar. Los períodos del año con mayor transmisión de casos son el verano y el otoño.

Existen 4 tipos de virus de Dengue (serotipos 1, 2, 3 y 4). Cada persona puede tener los 4 serotipos, pero una infección causada por un determinado serotipo le confiere inmunidad de por vida contra ese serotipo en particular.

El mosquito transmisor del Dengue muere a los 14 grados de temperatura, pero los huevos y las larvas (primera etapa de desarrollo del vector) sobreviven hasta un año, aún con frío y sequedad. Estos pueden eclosionar al llegar temporadas de calor y lluvias, incrementando la población del vector.