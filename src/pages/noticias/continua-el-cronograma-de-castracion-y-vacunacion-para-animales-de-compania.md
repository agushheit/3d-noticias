---
category: La Ciudad
date: 2021-09-15T06:00:42-03:00
thumbnail: https://assets.3dnoticias.com.ar/CASTRACIONES1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Continúa el cronograma de castración y vacunación para animales de compañía
title: Continúa el cronograma de castración y vacunación para animales de compañía
entradilla: La Municipalidad informa los puntos en que se llevarán adelante los operativos.
  Ambos servicios se prestan de manera gratuita.

---
La Municipalidad informa los lugares donde continúa la campaña de castración y vacunación móvil para animales de compañía. Se trata de servicios gratuitos que se ofrecen a todos los vecinos y vecinas de la ciudad, a los efectos de controlar la población felina y canina.

Cabe recordar que, en el caso de la castración, se realiza con turnos programados, por lo que se solicita a los interesados, ingresar a la web oficial www.santafeciudad.gov.ar/turnos. Allí se debe buscar el botón para “Solicitar un nuevo turno” y completar el formulario con los datos requeridos. Posteriormente, el sistema ofrece escoger la fecha, la hora y la sede a la cual concurrir. Vale señalar que el servicio se presta de lunes a viernes, excepto feriados, en el horario de 8 a 12.

Como ya es habitual, en los IMUSA del Parque Garay (Obispo Gelabert 3691) y del Jardín Botánico (San José 8400), se atiende de mañana y de tarde en los horarios de 8 a 17.

En tanto, desde ayer -lunes 13- hasta el viernes 17 de septiembre, también se realizarán castraciones y vacunaciones en el Distrito Suroeste (Vecinal Mosconi, Av. Mosconi 1957) y en el Distrito Este (Centro de Ex Combatientes de Malvinas, Pedro Vittori 4212).

Por otra parte, desde el lunes 13 hasta el viernes 24 de septiembre, el cronograma continúa en el Distrito Norte (vecinal Bernardino Rivadavia, Misiones 5184),en el Distrito Oeste (Barranquitas Oeste, Pedro Centeno 4100), Distrito Noroeste (vecinal El Tránsito, Pavón 5368) y en el Distrito La Costa (Vecinal El Pozo, Leloir y Boedo).

Asimismo, todos los días se realizan vacunaciones y castraciones en la Protectora de Animales, los turnos se solicitan al 342 5496 736.