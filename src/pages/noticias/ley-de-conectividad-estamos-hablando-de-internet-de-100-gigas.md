---
category: Agenda Ciudadana
date: 2021-10-23T06:15:54-03:00
thumbnail: https://assets.3dnoticias.com.ar/conectividad.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: 'Ley de Conectividad: "Estamos hablando de internet de 100 gigas"'
title: 'Ley de Conectividad: "Estamos hablando de internet de 100 gigas"'
entradilla: La ley de Conectividad era uno de los proyectos impulsados por el gobierno
  de Omar Perotti desde el primer día de la gestión

---
La Cámara de Diputados de Santa Fe aprobó este jueves la Ley de Conectividad, un Programa de Inclusión Digital y Transformación Educativa llamado “Santa Fe + Conectada” que permitirá poner en marcha un plan estratégico de conectividad en toda la provincia con el propósito de garantizar a la comunidad el acceso universal a las Tecnologías de la Información y las Comunicaciones (TICs).

En diálogo con los medios el diputado provincial y Jefe de Bloque del Partido Justicialista (PJ), Leandro Busatto, explicó que la fibra óptica tiene como objetivo dotar de Internet a la mayor parte de la población de Santa Fe, y la Red Provincial de Datos permite conectar todos los sectores públicos: hospitales, escuelas, juzgados e instituciones públicas, para que haya comunicación directa y transferencia de datos en tiempo real.

“Es un proyecto ambicioso, pero que viene a generar el proceso de inversión en infraestructura de conectividad más importante de la historia de la provincia de Santa Fe. Estamos planteando construir 4.000 kilómetros de fibra óptica y la provincia tiene construida solamente, como rama troncal, 160 hasta ahora, que es la traza de autopista Santa Fe Rosario”, contó el diputado, advirtiendo que la obra llegará a todos los santafesinos en 5 o 6 años.

Se estima que los beneficiarios serán aproximadamente tres millones de personas y el programa, con un plazo de ejecución de cuatro años, alcanzará a todas las ciudades y comunas santafesinas.

“Hay un proceso de disparidad enorme en la conectividad que hay que solucionar con infraestructura y con la posibilidad de tener una obra como la que estamos planteando”, dijo Busatto.

**Santa Fe + Conectada**

En este sentido, relató que el Enacom relevó en el último trimestre del 2019 que el 72% de las escuelas de la provincia no tenía conectividad. Es decir, 7 de cada 10 escuelas no estaba conectada a Internet, lo que representaba el 54% del alumnado; el otro 28% que tenía conexión a Internet de distinta calidad representaba el 46% del alumnado. “Acá hay dos datos: uno, la gran mayoría de las escuelas de Santa Fe no tiene conexión a Internet o no tiene una buena conexión; y dos, las que tienen buena conexión representan los grandes centros urbanos y fundamentalmente los centros dentro de esas ciudades”.

Busatto expresó que este caso es “emblemático”, aunque sucede algo parecido en los hogares, ya que el 70% de la provincia tenía nula conectividad o regular.

“Las obras de conectividad son obras que probablemente si la pandemia no hubiera llegado al mundo y a la Argentina no la hubiéramos tenido que agregar con tanta vehemencia; pero hoy nos ha interpelado esta pandemia, nos ha exigido tener una sociedad que obviamente aplique las TICs ya como como garantía del acceso a los procesos de transformación de producción, de educación, de socialización”, precisó Busatto.

Este plan requiere un monto de 124.670.000 dólares para su implementación, y de ese total, 100 millones (aproximadamente el 80 por ciento) serán cubiertos por el Banco de Desarrollo de América Latina; en tanto los 24.670.000 dólares restantes serán afrontados por el Estado santafesino con recursos propios. La ley que aprobó la Cámara de Diputados autoriza precisamente al Gobierno a tomar esa deuda.

La norma diseñada por el Poder Ejecutivo provincial había sido enviada a la Legislatura el 29 de septiembre de 2020 y contaba hasta ayer con media sanción del Senado. Consultado por la tardanza en aprobar este proyecto, Busatto dijo que técnicamente no cambió nada, pero “probablemente las posiciones de algunos sectores de la oposición se fueron modificando también en función de la presión que hay en algunos sectores de la sociedad para que tuviéramos mejores niveles de conectividad”.

Y reconoció: “También hemos mejorado ostensiblemente, en estos últimos meses, el diálogo con algunos sectores de la oposición, lo cual nos tendría que estar sirviendo para dejar de impedir que estas cosas ocurran”.

Con respecto a la capacidad de los santafesinos de adquirir dispositivos tecnológicos, el jefe del bloque justicialista dijo que no le preocupa tanto ya que cuando se hizo entrega de las netbooks del Plan Conectar Igual los estudiantes no tenían buenos núcleos de conectividad. Además, dijo que se pueden volver a hacer entrega de aparatos.

“Estamos hablando de Internet de 100 gigas, lo cual es desconocido el nivel de velocidad de navegación”, afirmó Busatto. “Santa Fe tiene un ancho de banda promedio de navegación de 21 megabytes por segundo. Estamos hablando de saltar la barrera del sonido para la velocidad. Con respecto a los dispositivos garantizo que hoy hay un excedente de dispositivos en algunos lugares y lo que falta muchas veces es la conectividad”.

**¿Hacia una ley de protección de datos?**

Finalmente, el diputado reconoció que la regulación del mundo virtual debe ser apuntalada. “Las leyes son muy muy básicas. Hay ley de protección de datos pero va a haber que regular hacia adelante un montón de situaciones que se van dando, aparte con un nivel de dinamismo que la ley muchas veces queda obsoleta”, explicó.

“Lo que hay que lograr es ver cómo se regula ya en el ámbito nacional, no le compete a la provincia de Santa Fe, una ley de protección de datos que permita obviamente que transmitir nuestras nuestra información sea muy segura”.

Y concluyó: “Estamos hablando de información sensible en serio; estamos planteando conectar todos los efectores públicos de salud, por lo cual la historia médica de cualquier paciente puede estar navegando en una red de datos a la que hay que proteger y realmente lo que hay que garantizar es toda la confidencialidad del caso”.