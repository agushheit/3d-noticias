---
category: Agenda Ciudadana
date: 2021-09-23T06:15:12-03:00
thumbnail: https://assets.3dnoticias.com.ar/GUZMAN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: Se canceló ante el FMI el primer pago de capital del megapréstamo que tomó
  Macri en 2018
title: Se canceló ante el FMI el primer pago de capital del megapréstamo que tomó
  Macri en 2018
entradilla: Para la cancelación el país empleó una parte de los nuevos recursos que
  recibió hace un mes de parte del organismo por la ampliación de los derechos especiales
  de giro.

---
La Argentina canceló al Fondo Monetario Internacional (FMI) el primer pago de capital por alrededor de US$1.900 del megapréstamo que tomó el gobierno de Mauricio Macri en 2018, para lo cual empleó una parte de los nuevos recursos que recibió hace un mes de parte del organismo por la ampliación de los derechos especiales de giro (DEG).

Así lo confirmó el ministro de Economía, Martín Guzmán, quien en diálogo radial especificó que esta cancelación se enmarca "en el sentido de responsabilidad" que significa para el Gobierno llevar adelante una reconstrucción de la macroeconomía.

Guzmán dijo que lo que se busca ante todo, es defender los intereses del pueblo argentino, a diferencia de la administración anterior "que no utilizó un solo dólar para mejorar la capacidad productiva".

En concreto, la Argentina canceló vencimientos por un total de US$1.880 millones, lo que determinó que las reservas internacionales del Banco Central (BCRA) cerrarán hoy en US$ 43.178 millones, luego del giro al FMI por el primer pago de capital de la deuda, precisaron a Télam fuentes de la entidad monetaria.

La operación no involucró un giro de fondos al organismo, sino que se debitó de la cuenta argentina en el organismo, donde se encuentran los DEG (la moneda del FMI) pertenecientes al país que se sumaron a las reservas del Banco Central el 23 de agosto pasado.

"Enfrentar este pago que se le hace al FMI no tiene que ver con que nos guste o no, sino con un sentido de la responsabilidad, entendiendo que hay caminos que son realmente desestabilizantes y peores", consideró el ministro en declaraciones radiales.

Guzmán fustigó con dureza el accionar del macrismo al dejar una deuda con el FMI por "US$ 45.000 millones, de los cuales US$ 21.000 millones de utilizaron para el repago de deuda y los restantes US$ 24.000 millones para fugar divisas".

En este sentido, resaltó que "el FMI fue uno de los peores problemas que nos dejó la administración anterior, generando una vergonzosa deuda y una escasez de los instrumentos disponibles para enfrentar la crisis".

Tras confirmar que el pago se realizó con parte de los US$4.335 millones en la moneda del FMI que recibió el país como miembro del organismo, el funcionario destacó "el rol de la Argentina en distintos foros internacionales para el desembolso de los DEG", que tuvieron por objetivo mitigar los efectos de la pandemia de coronavirus.

El Gobierno argentino está abocado en la actualidad en alcanzar un nuevo programa financiero con el FMI, que estaría cerrado para el año próximo, según los datos anticipados en el proyecto de Presupuesto 2022 enviado la semana pasada al Congreso.

"El camino que buscamos construir es el de un acuerdo (con el FMI) en el cual se haga lo que nosotros creemos que le haga bien a la Argentina", subrayó Guzmán.

Ayer, el presidente Alberto Fernández, celebró la medida de ampliación de los DEG al hablar ante la Asamblea de las Naciones Unidas (ONU).

Más allá de esto, el Presidente resaltó que "Argentina es un país que ha sido sometido a un endeudamiento tóxico e irresponsable con el Fondo Monetario Internacional, insostenible, un 'deudicidio'".

El mandatario dijo también que la prórroga de la suspensión de servicios de deuda impulsada por el G-20 es una medida "provisoria e insuficiente" y planteó la necesidad de un "acuerdo multilateral sobre reestructuración de deuda soberana".

Sobre este aspecto, hubo un acercamiento en relación a la posición que intenta tomar los Estados Unidos, que es el principal accionista del FMI.

En una reciente entrevista para el podcast de la revista Americas Quarterly, el director para el hemisferio occidental del Consejo de Seguridad Nacional de la Casa Blanca, Juan González, indicó, que el Gobierno argentino resultó "increíblemente receptivo" al vínculo con la Casa Blanca, y destacó el trabajo del embajador Jorge Argüello.

González, mano derecha del presidente Joe Biden en relación con asuntos de América Latina, reiteró que buscan "ayudar a facilitar" el diálogo con el FMI, y que la experiencia sirva a otros países.

"La forma en la cual la Argentina resuelva su relación con el FMI enviará una señal poderosa a los mercados emergentes que están atravesando ellos mismos unas crisis económicas muy problemáticas", dijo González.

"Nos hemos involucrado activamente para tratar no solo de desarrollar una relación bilateral constructiva con la Argentina, sino también de ayudar a facilitar el diálogo con el FMI", agregó