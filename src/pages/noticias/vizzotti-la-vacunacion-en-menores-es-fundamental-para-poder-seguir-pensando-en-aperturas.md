---
category: Agenda Ciudadana
date: 2021-10-17T06:15:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/LAVIZZOTTI.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Vizzotti: La vacunación en menores "es fundamental para poder seguir pensando
  en aperturas"'
title: 'Vizzotti: La vacunación en menores "es fundamental para poder seguir pensando
  en aperturas"'
entradilla: '"La vacunación desde los 3 años de edad es fundamental para poder sostener
  los logros y poder seguir pensando en aperturas", señaló la ministra de Salud.'

---
La ministra de Salud, Carla Vizzotti, sostuvo este sábado que el avance de la vacunación contra el coronavirus en niños y adolescentes es "fundamental para poder seguir pensando en aperturas".  
  
"Estamos con una campaña de vacunación que está avanzando rapidísimo y la vacunación desde los 3 años de edad es fundamental para poder sostener los logros y poder seguir pensando en aperturas", dijo Vizzotti esta mañana en declaración para CNN Radio.  
  
La ministra aseguró que "estamos con una confianza de la población en las vacunas muy alta" y precisó que ya se encuentran vacunados con una dosis "el 90 por ciento de la población mayor de 18 años".  
  
Por este motivo, el siguiente paso para la cartera de Salud es "la perspectiva de la vacunación adolescente y pediátrica con dosis disponibles" en los próximos meses.  
"Tenemos una reducción de casos y muertes, con unas 800 internaciones, que es el número más bajo desde el inicio de la pandemia", remarcó Vizzotti.  
  
Si bien la ministra se mostró optimista con respecto a los pronósticos, señaló que "no podemos decir esto ya paso hasta que en el mundo la población acceda a la vacunación".  
  
Por eso indicó que, hasta ese momento, "vamos a tener la posibilidad de que emerja una nueva cepa y también con la Delta que todavía no es predominante en Argentina, pero esta aumentando".  
  
Por otro lado la ministra se refirió a la aparente ralentización del proceso de vacunación y explicó que esa situación "sucede en todos los países que llegan a una vacunación elevada".

"Hasta hace poquito teníamos vacunas para mayores de 18 años y se ralentiza porque no teníamos personas que necesitaban iniciar su esquema de vacunación", dijo.  
  
Asimismo afirmó que "ahora que tenemos vacunas para menores de 18 años, tenemos más población para vacunar".  
  
Vizzotti: "Ahora que tenemos vacunas para menores de 18 años, tenemos más población para vacunar". Foto: Marcelo Ochoa

  
Según los datos brindados por la ministra, el 90 por ciento de los mayores de 18 años tienen una dosis aplicada, y el 72 por ciento el esquema de vacunación completo.  
  
Para Vizzotti, todavía "tenemos más demanda que oferta", por lo cual todavía no hay un número importante de personas que no se quieran vacunar.  
  
Con respecto al 10 por ciento restante, Vizzotti lo atribuyó a "dificultades en el acceso la tecnología" y otra parte a "personas que tengan dudas".  
  
"Lo que han decidido las provincias es ir casa por casa para poder despejar ese 10 por ciento", recordó.  
  
En referencia a la posibilidad de una tercera dosis, la ministra dijo: “La tercera dosis está pensada y tenemos estipulado tener esas dosis durante el primer semestre de 2022”.  
  
Para finalizar, Vizzotti se refirió al proceso de votación para el próximo 14 de noviembre y aseguró que "con el porcentaje de vacunación que tenemos y con la situación epidemiológica genera que las elecciones sean de muy bajo riesgo".  
  
"Estamos trabajando para ir adecuando los protocolos para que siempre sean seguros", concluyó.