---
category: Deportes
date: 2021-12-19T06:00:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/colonensantiago.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Fue un caos en el ingreso de los hinchas de Colón al estadio
title: Fue un caos en el ingreso de los hinchas de Colón al estadio
entradilla: 'A las 18 se habilitó el ingreso para los hinchas de Colón al estadio
  Madre de Ciudades para la final y hubo complicaciones con quienes buscaron meterse
  sin entradas

'

---
Desde temprano, los hinchas de Colón se ubicaron en los alrededores del estadio Madre de Ciudades de Santiago del Estero para esperar la final del Trofeo de Campeones ante River. La apertura estaba pautada para las 18, pero muchos decidieron comenzarán las filas antes para tratar de estar entre los primeros.

Vale recordar que se vendieron 11.000 entradas, pero hay casi la misma cantidad de gente en las cercanías y el fan fest. Ni el calor, ya que se dieron picos de 40 grados, detuvieron la pasión, que se lleva a flor de piel.

Más allá de que la gente de River también estaba prácticamente ni se notó al no contar con alojamiento, porque los santafesinos coparon todo con antelación.

Hubo algunas situaciones complejas a resolver por la policía, ya que los que luego quisieron entrar al fan fest no podían hacerlo con una camiseta representativa. Todos eran de Colón y por eso hubo momentos de tensión. Pero la cosa no quedó solo en eso.