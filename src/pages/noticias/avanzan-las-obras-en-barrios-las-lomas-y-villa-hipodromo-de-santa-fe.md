---
category: La Ciudad
date: 2021-06-17T08:54:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/sin-techo.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Avanzan las obras en barrios Las Lomas y Villa Hipódromo de Santa Fe
title: Avanzan las obras en barrios Las Lomas y Villa Hipódromo de Santa Fe
entradilla: El gobierno provincial, a través del Programa de Erradicación de Viviendas
  Precarias está por finalizar 200 casas en barrio Las Lomas, además de obras de infraestructura
  en barrio Villa Hipódromo de la ciudad de Santa Fe

---
La ministra de Infraestructura, Servicios Públicos y Hábitat, Silvina Frana, recorrió las obras de vivienda e infraestructura que se llevan adelante en el marco del Programa de Erradicación de Viviendas Precarias, entre el gobierno provincial y el movimiento de “Los Sin Techo”, destinado a reemplazar “ranchos”, por casas de construcción sólidas. Los trabajos están próximos a finalizarse en los barrios Las Lomas y Villa Hipódromo de la ciudad de Santa Fe.

Durante la recorrida en Villa Hipódromo, la ministra Frana expresó: "Estas obras de infraestructura son una intervención muy importante para este barrio, con trabajos que tienen que ver con asfalto, cordón cuneta, y con una novedad que es prolongar el entubamiento del Estado de Israel, que va disminuir el riesgo hídrico en la zona".

Y añadió: "Obras como éstas, no solamente le devuelven la dignidad de vida a los vecinos y vecinas generando ambientes más saludables, sino también mejoran la seguridad de un barrio. En este sentido, desde el gobierno provincial y con la decisión del gobernador Omar Perotti, se definió ampliar la inversión llegando a montos superiores a los 220 millones de pesos". Y prosiguió, "Tenemos la vocación de trabajar con obras en los barrios más alejados del centro, porque estamos convencidos que también merecen la presencia del Estado, y así, transformar el día a día de cada vecino", finalizó la funcionaria.

**LAS LOMAS**

Durante la visita al barrio, la ministra valoró el compromiso del movimiento de Los Sin Techo: "Las obras las venimos realizando junto con el movimiento de Los Sin Techo, que hace 35 años que vienen trabajando en este mismo sentido, con un fuerte compromiso. En este caso particular, estamos en barrio Las Lomas con el programa de sustitución de ranchos para que cada familia tenga condiciones de habitabilidad, y acceso a los servicios básicos. En poco tiempo vamos a terminar de construir 200 viviendas, que fue el primer convenio firmado con la institución, y continuar avanzando en un nuevo convenio como nos encomendó el gobernador", anunció Frana.

Por su parte, el secretario de Hábitat, Amado Zorzón, destacó el trabajo conjunto que se viene realizando: "Con este programa, en Santa Fe estamos con el movimiento de Los Sin Techo, que con su vasta experiencia nos allanan el camino, pero también estamos trabajando con las localidades que ya han realizado la tramitación correspondiente para eliminar estas viviendas precarias, como es el caso de Reconquista, Helvecia, Florencia, Rosario, Centeno. Y como decía la ministra, tiene que ver con una política de Estado que prioriza la dignificación del ser humano y el trabajo mancomunado con las organizaciones".

Finalmente, el referente del movimiento “Los Sin Techo”, José Luis Salazar mencionó: "Estamos trabajando codo a codo con el gobierno de la provincia, que tomó la decisión de que las familias tienen que vivir dignamente, y nosotros estamos plasmando esta idea, para que en poco tiempo las familias de este barrio tenga su vivienda propia. Y como decía Atilio Rosso ´siempre se puede hacer algo por el otro´, y el gobierno entendió esto, y destinó los recursos para lograrlo".

![](https://ci3.googleusercontent.com/proxy/6FsxulxrFFm8B18HyR2CCAp3YVWQS4tI7KLr35Z1KnI0I3wgd5ZWw-Tsg7ZQOpLxZ1xJKVu6mw65-clR1YMqR9uE1XWfrXmWJJ33wVlfallQupy2rcBJBBNfChhXk63-h50YXisg1HTNLkEvYixxSnLpo-VIGn5p58HDpN6B6b6jNFdo=s0-d-e1-ft#https://www.santafe.gob.ar/noticias/media/cache/thumb_no_full/recursos/fotos/2021/06/2021-06-16NID_271295O_1.jpeg)