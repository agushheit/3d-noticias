---
category: La Ciudad
date: 2021-08-13T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: '"Tendremos que pasar un verano sumamente crítico", advirtió el presidente
  de Aguas'
title: '"Tendremos que pasar un verano sumamente crítico", advirtió el presidente
  de Aguas'
entradilla: Lanzaron una campaña para concientizar sobre el uso responsable y solidario
  del agua potable. Apunta a evitar el derroche en la limpieza de las veredas, entre
  otras medidas. Y evalúan reubicar la toma sobre el Colastiné.

---
“El agua en nuestras manos, el agua en buenas manos”, se denomina la campaña que encara Aguas Santafesinas con el objetivo de impulsar en la ciudad la utilización responsable y solidaria del agua potable, particularmente en la limpieza de las veredas, aún más necesaria en el contexto de la bajante del río Paraná que estamos sufriendo.

La campaña cuenta con la adhesión del Sindicato Único de Trabajadores de Edificios de Renta y Horizontal (SUTERyH) y el Sindicato Santa Fe de Trabajadores de Obras Sanitarias (SISFTOS).

Con acciones que apuntan a la concientización ciudadana y a replicar conductas positivas, se recuerda la vigencia de las ordenanzas en la ciudad de Santa Fe que establecen como horario permitido para el lavado de veredas los martes y jueves de 0 a 18 horas, y los sábados de 8 a 18 horas.

La ordenanza 9551 de 2017 establece además que “el lavado de veredas está permitido únicamente mediante el empleo de baldes o en su defecto mangueras provistas de sistemas de corte, tipo gatillo o interruptores o pulsadores, en el extremo de salida de agua de la misma. Además, el agua, jabón o detergente utilizado en el lavado de veredas deberá ceñirse a lo estrictamente necesario para tal fin, prohibiéndose su uso desproporcionado y abusivo”.

En ese sentido, se estima que una manguera abierta durante una hora insume 500 litros de agua potable, el consumo promedio de dos personas por día. Mientras que utilizando diez baldes de agua -más que suficientes para lavar una vereda-, se necesitan solo 100 litros.

“Quizá estas medidas parecen insignificantes ante el tamaño inconveniente que estamos teniendo con la bajante histórica del río Paraná, pero si sumamos el esfuerzo de cada uno en cada edificio, en cada hogar, tendremos el resultado positivo para llevar a cada hogar el agua potable, que es esencial en este contexto de pandemia”, dijo el presidente del directorio de ASSA, Hugo Morzán, durante una conferencia de prensa brindada este jueves al frente de un edificio ubicado en Avda. de los 7 Jefes 3695, frente a la Costanera santafesina.

Y en el mismo sentido, advirtió Morzán que los santafesinos “hacemos un uso superior al normal. Quizá por ser ribereños suponemos la infinitud del recurso, pero hoy el río Paraná nos puso en jaque e interpela nuestras costumbres, para que seamos más responsables con el consumo de agua”

“Así como la pandemia nos enseñó el uso del tapabocas, esperamos que esta situación crítica nos enseñe el uso responsable del recurso”, reflexionó el presidente de ASSA, quien luego se mostró preocupado ante la temporada estival, en la que se incrementará en consumo.

“En este momento deberíamos para algunas bombas para realizar mantenimiento, pero estamos con el funcionamiento a pleno para garantizar la calidad del servicio”, dijo Morzán. “De esta forma, tendremos que pasar un verano sumamente crítico, porque el Instituto Nacional del Agua nos informa que el pico crítico de bajante llegará en noviembre, cuando crece el consumo debido a las altas temperaturas”.