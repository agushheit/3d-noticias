---
category: Agenda Ciudadana
date: 2021-02-24T06:53:06-03:00
thumbnail: https://assets.3dnoticias.com.ar/anses.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: DS
resumen: 'Vacunatorio VIP: la ANSeS intimó formalmente a jubilarse al fiscal que investiga
  el escándalo'
title: 'Vacunatorio VIP: la ANSeS intimó formalmente a jubilarse al fiscal que investiga
  el escándalo'
entradilla: Eduardo Taiano, que imputó a Ginés González García, recibió este martes
  la notificación del ente previsional.

---
El fiscal federal Eduardo Taiano, a cargo de la investigación sobre el Vacunatorio VIP en la que ya imputó al exministro de Salud, Ginés González García, recibió este martes la notificación formal de la ANSeS para que, en el plazo de un mes, regularice su trámite de retiro o se jubile.

La intimación se formalizó cuando el fiscal recibió la carta notificatoria en la mañana de este martes. El organismo que conduce la camporista Fernanda Raverta había anunciado la medida y advertido a más de 200 jueces y fiscales para que regularizaran sus trámites de jubilación pendientes o los daría de baja.

Desde ANSeS indicaron que la notificación se trata del mismo proceso ya iniciado tras el anuncio de la semana pasada. Así y todo, la coincidencia de los tiempos burocráticos con las investigaciones abiertas en la Justicia genera críticas y preocupación entre los magistrados.

La decisión administrativa de la ANSeS fue en paralelo y a tono con los embates contra el Poder Judicial que verbalizó el presidente Alberto Fernández y que tuvieron foco en críticas a la Corte Suprema.

La ANSeS comenzó a enviar la semana pasada una serie de intimaciones a varios magistrados para que definieran si querían jubilarse o, caso contrario, dar de baja sus trámites pendientes.

En la lista figuran fiscales como Carlos Stornelli y Raúl Plee, y camaristas federales como Eduardo Riggi, Martín Irurzun y Carlos Mahiques, entre otros. También la juez federal María Servini y su par de la Corte, Elena Highton de Nolasco.

Clarín pudo saber que Taiano, de 62 años, recibió este martes su notificación. Y al menos el camarista Leopoldo Bruglia sería uno de los jueces que también recibió en las últimas horas la nota que fuerza a tomar una decisión. Tienen un plazo de treinta días.

Los requisitos para jubilarse como miembro del Poder Judicial o del Ministerio Público de la Nación son tener 60 años cumplidos -sea hombre o mujer-, 30 de servicios y 20 con aportes computables. Además de 15 años continuos o 20 discontinuos de carrera judicial.

"Son muchos los funcionarios judiciales que, con la edad suficiente para acceder a la jubilación, inician el trámite ante el organismo previsional pero lo mantienen stand by, para poder completarlo rápidamente en cualquier momento", explicaron desde el organismo, para justificar el pedido generalizado.

Las fuentes del organismo insistieron en que la intimación le llega específicamente a aquellos funcionarios judiciales que iniciaron su trámite jubilatorio: entregaron documentación, pero dejaron abierta la gestión. El timing, sin embargo, abre interrogantes.

El requerimiento de ANSeS se dio en medio de una ola de fuertes presiones del Gobierno hacia la Justicia, tanto desde el kirchnerismo duro como del propio Presidente.

Sea con decisiones administrativas, declaraciones públicas o anuncios oficiales, el objetivo pareciera ser siempre el mismo: dejar contra las cuerdas a los jueces y fiscales que investigan y juzgan a los ex funcionarios kirchneristas en expedientes por corrupción.

​Taiano llegó a la Fiscalía Federal 3 en abril de 1993 como adjunto y dos años después quedó como titular, cargo que ocupa hasta la actualidad. Sabe de causas sensibles al poder. Tuvo que actuar en dos denuncias por enriquecimiento ilícito contra el expresidente Néstor Kirchner, una que apuntaba al período entre 1995 y 2004, y otras desde ese último año hasta 2007.

En 2005, un hijo del fiscal fue víctima de un secuestro el mismo día que a Taiano se le vencía el plazo para apelar el sobreseimiento de Kirchner. No lo hizo. Esa primera causa sobre la fortuna de la familia entonces presidencial se cerró. En la segunda, Taiano pidió que se archivara por "inexistencia de delito".

También fue el fiscal que pidió la elevación a juicio de la causa por la venta de dólar futuro, por la que Cristina Kirchner está procesada. La vicepresidenta intenta impedir el inicio del juicio oral por ese caso mediante artilugios dilatorios: hará un descargo en una audiencia ante la Cámara Federal de Casación Penal que tendrá lugar el 1° de marzo.

Por el último escándalo del Vacunatorio Vip, el lunes Taiano imputó al ex ministro de Salud y a su sobrino Lisandro Bonelli, quien se desempeñaba como jefe de Gabinete en la misma cartera. La jueza federal María Eugenia Capuchetti también ordenó allanar el Ministerio de Salud para avanzar con la investigación.

En su gira por México, Alberto Fernández redobló la apuesta este martes y criticó directamente esa investigación: "Les pido a los jueces que hagan lo que deben. Terminemos con las payasadas", cuestionó en desacuerdo con que la eyección del exministro González García tuviera consecuencias penales por su accionar.

Casi en paralelo a esas declaraciones, el fiscal del caso recibía el ultimátum administrativo del ente previsional.