---
category: Agenda Ciudadana
date: 2021-07-01T08:06:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/Cárceles-celulares.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Más de tres mil celulares secuestrados en cárceles en la provincia de Santa
  Fe
title: Más de tres mil celulares secuestrados en cárceles en la provincia de Santa
  Fe
entradilla: 'Sólo en el penal de Piñero, donde se fugaron ocho internos el fin de
  semana, se detectaron más de 900 dispositivos de enero a la fecha. "Las vías de
  ingreso son innumerables", admiten las autoridades. '

---
Según reveló a El Litoral el secretario de Asuntos Penitenciarios, Walter Gálvez, en los últimos seis meses se logró secuestrar a través de las requisas en los diferentes penales de la provincia un total de 3025 teléfonos celulares. Y sólo en la Unidad Penitenciaria Nro 11 con asiento en Piñero, se detectaron en el mismo período 946 dispositivos. Como se sabe, su tenencia en manos de los internos está prohibida, pero los números demuestran hasta qué punto se ha extendido su uso. De hecho, volvieron a ser los propios reclusos los que desde sus móviles difundieron imágenes de la fuga que se produjo en el sur provincial, el pasado fin de semana. Y es un hecho, que los utilizaron para planificar la fuga, así como los emplean para pergeñar otros ilícitos.

 "Los canales de ingreso de los celulares a los penales son innumerables y de lo más variado", admitió el funcionario. "Ingresan a través del personal penitenciario que los llevan 'pegados' al cuerpo; a través de los familiares que van a las visitas y los llevan en zonas íntimas que uno no puede revisar; también, a través de los profesionales del derecho o abogados por lo que ya hay uno detenido y otros denunciados; además, por lo que se llaman 'palomas' que son los que arrojan desde y en las inmediaciones de los tejidos. Y hasta en latas de tomate perfectamente selladas", detalló.

 Gálvez admitió que la circulación de los aparatos implica un verdadero "comercio" dentro de las cárceles. "Un celular dentro de los penales puede pagarse desde seis mil hasta 50 mil pesos; imaginemos que si quien ingresa es un penitenciario, con dos celulares que venda en el mes, tiene casi un segundo sueldo", planteó. Sobre esa base, consideró que "la única manera de revertir y controlar" la situación es incorporando mayor tecnología. "No hay otra alternativa, porque hay controles corporales que no se pueden hacer y es así como se ingresan no sólo celulares, sino también sustancias", concluyó.