---
category: Agenda Ciudadana
date: 2021-12-30T06:00:57-03:00
thumbnail: https://assets.3dnoticias.com.ar/GANANCIAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: Sin la oposición, el oficialismo convirtió en ley en el Senado las modificaciones
  al impuesto de Bienes Personales
title: Sin la oposición, el oficialismo convirtió en ley en el Senado las modificaciones
  al impuesto de Bienes Personales
entradilla: 'Con 37 votos afirmativos y uno solo negativo (de Alejandra Vigo), el
  Senado aprobó la iniciativa que Juntos por el Cambio rechazó por estar en desacuerdo
  con la suba del impuesto para los grandes contribuyentes.

'

---
Sin la presencia de Juntos por el Cambio en el recinto del Senado por considerar que la sesión era "nula", el oficialismo consiguió hoy convertir en ley las modificaciones al impuesto sobre los Bienes Personales.

Con 37 votos afirmativos y uno solo negativo (de Alejandra Vigo), el Senado aprobó la iniciativa que había vuelto con cambios de la Cámara de Diputados, y que Juntos por el Cambio rechazó por estar en desacuerdo con la suba del impuesto para los grandes contribuyentes que había impulsado el oficialismo.

La sesión corrió serios riesgos de caerse ya que el senador catamarqueño del Frente de Todos, Guillermo Andrada, había anunciado por la mañana de este miércoles que había dado positivo de Covid-19.

Al haber perdido la mayoría luego de las últimas elecciones, el Frente de Todos precisaba para el quórum una asistencia perfecta de sus senadores y además sumar la participación de su dos legisladores (habitualmente) aliados Alberto Weretilneck (Juntos Somos Río Negro) y Magdalena Solari Quintana (Frente de la Concordia misionero).

Con la ausencia de Andrada, se caía el quórum y por lo tanto la sesión, pero finalmente desde el Frente de Todos lograron convencer a la riojana ex Cambiemos Clara Vega y con 38 legisladores presentes -ni uno más de los necesarios- quedó habilitada la reunión en el recinto para votar el proyecto de Bienes Personales.

Juntos por el Cambio calificó de "nula" la sesión tras denunciar que el quórum se consiguió 40 minutos después del horario de convocatoria a la sesión, es decir, diez minutos después del tiempo reglamentario (la tolerancia de acuerdo a los estatutos es de 30 minutos).

En ese sentido, la principal bancada opositora decidió no participar de la sesión y no dar el debate sobre la ley, luego de oponerse a los cambios implementados por el Frente de Todos en la Cámara de Diputados.

En cambio, Vigo (esposa del gobernador Juan Schiaretti) sí participó y dio en el recinto los argumentos que fundamentaron su rechazo a la iniciativa del oficialismo.

"Este aumento no viene acompañado ni de un alivio ni de aliento a las empresas. En este proyecto la progresividad impositiva no demuestra ser demasiado racional. En pocos pasos, la escala teóricamente progresiva lleva rápidamente a las escalas más altas", advirtió.

Por su parte, Weretilneck dio las razones de su respaldo a la ley y destacó, entre otras cuestiones, que la "actualización por el IPC de manera anual de los mínimos imponibles significa previsibilidad para los contribuyentes".

El proyecto eleva de 2 a 6 millones de pesos el tope del mínimo no imponible para Bienes Personales, además lleva la alícuota de 1.25 a 1.5 por ciento para quienes tengan declarados bienes por más de 100 millones de pesos, y a 1.75 por ciento en patrimonios superiores a los 300 millones.

Se mantiene la alícuota del 2.25 para bienes declarados en el exterior.

Por otra parte, la iniciativa estipula que los inmuebles destinados a casa-habitación del contribuyente, o del causante en el caso de sucesiones indivisas, no estarán alcanzados  
por el impuesto cuando resulten iguales o inferiores a 30 millones de pesos -actualmente es de 18 millones de pesos-.

Además, esos montos "se ajustarán anualmente por el coeficiente que surja de la variación anual del Índice de Precios al Consumidor (IPC), que suministre el INDEC, correspondiente al mes de octubre del año anterior al del ajuste respecto al mismo mes del año anterior".

En Diputados, el oficialismo había logrado aprobar estos cambios en el tributo con una diferencia mínima de votos: 127 positivos contra 126 rechazos.

La iniciativa -presentada por el Frente de Todos- había sido aprobada por la Cámara alta por unanimidad meses atrás, pero luego el oficialismo lo dejó en stand by en Diputados y este fin de año resurgió por un reclamo de Juntos por el Cambio para tratarlo antes del 31 de diciembre, de manera que las correcciones tuvieran vigencia desde el 1 de enero de 2022.