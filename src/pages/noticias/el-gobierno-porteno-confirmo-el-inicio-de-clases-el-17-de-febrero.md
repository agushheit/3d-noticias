---
category: Agenda Ciudadana
date: 2021-01-16T04:19:55Z
thumbnail: https://assets.3dnoticias.com.ar/Larreta-docentes.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: Télam / El Litoral'
resumen: El Gobierno porteño confirmó el inicio de clases el 17 de febrero
title: El Gobierno porteño confirmó el inicio de clases el 17 de febrero
entradilla: 'Representantes del Ministerio de Educación de la Ciudad de Buenos Aires
  y de los sindicatos docentes mantuvieron una reunión sobre el inicio de clases. '

---
**Los dirigentes sindicales pidieron garantías sanitarias para volver a las aulas.**

El Gobierno porteño, representado por el subsecretario de la carrera docente del ministerio de Educación, Manuel Vidal, dialogó con los representantes sindicales en dos tandas: a las 10.30 recibió a referentes de UDA, UTE, Camyp, Adef, Sedeba, Amet, Seduca, Adia y Suetra; y a las 14, a dirigentes de Compromiso, Sadop, Edupec, AEP, CPD, Ademys, Sutecba, ASED y Suetra.

En las reuniones, el Gobierno porteño ratificó que las clases comenzarán el 17 de febrero y manifestó a los representantes sindicales que «la presencialidad es una prioridad absoluta», dijeron a Télam fuentes consultadas.

En ese sentido, desde el Gobierno de Horacio Rodríguez Larreta se transmitió que «la Ciudad considera que la presencialidad es fundamental y no hay más margen de tiempo que los chicos y las chicas puedan perder».

«Si todos nos cuidamos y respetamos los protocolos, la escuela es un lugar seguro», ponderó el Gobierno porteño.

Durante la reunión, los funcionarios plantearon que «se está realizando un trabajo conjunto con el Ministerio de Salud porteño para avanzar en la vuelta a clases con protocolos seguros y todas las medidas de higiene y seguridad».

El encuentro se dio en el marco del proceso participativo que desarrolla el Ministerio de Educación de la Ciudad, por el que se dialogó con especialistas, ONG, gremios docentes, y durante enero también serán convocados padres y alumnos de escuelas porteñas.

Desde el sector docente, el sindicato UTE-Ctera planteó en un comunicado que «el aumento de contagios en la Ciudad de Buenos Aires y la falta de espacios adecuados en los edificios escolares para el desarrollo de actividades sin descuidar la salud, hacen que no estén dadas las condiciones epidemiológicas ni de infraestructura para el retorno a las clases presenciales el 17 de febrero».

Angélica Graciano, titular de UTE-Ctera, propuso establecer un semáforo epidemiológico para evaluar el regreso a clases, según la cantidad de casos positivos de coronavirus por cada 100.000 habitantes.

A través de un hilo de Twitter, Graciano explicó que «este índice consensuado puede permitir la implementación de estrategias responsables que tiendan al cuidado de la salud de la comunidad educativa, con pautas certeras y claras para la circulación de los docentes y estudiantes».

«Por último, exigimos la implementación inmediata de un esquema de vacunación para el personal docente y no docente y la creación de los cargos que sean necesarios para sostener los probables esquemas mixtos», afirmó Graciano.

La dirigente sostuvo que los docentes quieren volver a la presencialidad, pero que esa decisión debe depender de la situación epidemiológica y no de presiones a través de operaciones políticas y mediáticas.

En tanto, la secretaria gremial de Ctera, Alejandra Bonato, indicó tras la reunión que el contenido se recupera, pero las vidas no y remarcó que actualmente se dan las mismas cifras de contagios que en junio de 2020.

La referente sindical advirtió que las aulas son muy chicas y es necesario invertir en infraestructura, y afirmó que aún falta la vacunación para los docentes.

«Me parece que hay que adecuar los edificios a la situación epidemiológica. Nosotros venimos a escuchar la propuesta y no ha habido ninguna propuesta. Hubo varias posibilidades, una es mitad presencial y mitad virtual. Lo que estamos pidiendo es lo mínimo y ser serios en el marco de esta pandemia como nunca atravesó la humanidad», señaló en declaraciones con el canal Todo Noticias (TN).

En tanto, Ademys cuestionó que el Gobierno porteño reciba a los gremios en dos reuniones y en su cuenta de Twitter manifestó que se trató «una clara maniobra para dividir las voces de la docencia que nos oponemos a las políticas del gobierno de la Ciudad».

El secretario adjunto de Ademys, Jorge Adaro, dijo en declaraciones a Télam que el Gobierno porteño quiere volver a la presencialidad, sin ningún tipo de propuesta que atienda a la realidad y sostuvo que siguen planteando que cada maestro tiene que trabajar con grupos de 30 alumnos.

«No están planteando burbujas con grupos reducidos», expresó el dirigente tras el encuentro y manifestó que «nosotros seguimos planteando que es poco serio».

En ese sentido, dijo que se trata de una propuesta que es realmente inviable, que no ha aportado de modo objetivo que han hecho algo distinto y consideró que «como es tan improbable, trasladan la responsabilidad a los docentes y sindicatos».

«No están mostrando ningún tipo de preocupación por los docentes y los alumnos, se manejan con golpes de efecto», destacó el dirigente y afirmó que «quieren volver a un esquema pre pandemia, casi como se trabajaba antes».

Adaro opinó que «esta reunión es cáscara vacía, tiene por objeto instalar una iniciativa política sin ningún tipo de elemento».