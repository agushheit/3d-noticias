---
layout: Noticia con imagen
author: Infobae
resumen: Nuevos DNI
category: Agenda Ciudadana
title: "El Gobierno lanzó un nuevo DNI: cuáles son los cambios, cómo se gestiona
  y qué pasará con las versiones anteriores"
entradilla: Fue presentado por el ministro del Interior, Eduardo  de Pedro.
  “Estamos saldando una deuda con nuestra propia historia y nuestra propia
  identidad”, consideró.
date: 2020-11-25T12:04:00.626Z
thumbnail: https://assets.3dnoticias.com.ar/dni.jpg
---
El ministro del Interior, Eduardo *Wado* de Pedro, y el secretario de Malvinas, Antártida y Atlántico Sur de la Cancillería, Daniel Filmus, presentaron el nuevo Documento Nacional de Identidad que incluye una actualización que incorpora la plataforma bicontinental al mapa del territorio nacional.

El anuncio formó parte de la celebración del Día de la Soberanía Nacional y pone en valor la extensión territorial del país, a partir del trabajo de articulación entre la cartera de Interior, el Registro Nacional de las Personas (Renaper), el Archivo General de la Nación (AGN) y la Cancillería Argentina, para cumplir con la Ley 26.651, que establece la obligatoriedad de la exhibición pública en actos de Gobierno del mapa bicontinental de la República Argentina.

“Estamos saldando una deuda con nuestra propia historia y nuestra propia identidad” consideró de Pedro y resaltó que “el reconocimiento de la soberanía que se extiende sobre una porción de la Antártida y sobre las Islas Malvinas y del Atlántico Sur, también es parte de lo que nos define como argentinos y argentinas”.

**El nuevo DNI tendrá un costo de $300, que es el valor de la tarjeta. La versión digital no tendrá un costo adicional.**

El documento en formato digital cumple con las mismas funciones que el DNI tarjeta, excepto para votar y viajar al exterior. Para tener el DNI digital en el celular es necesario ser ciudadano argentino o extranjero, con residencia vigente, mayor de 14 años, y que tramite un nuevo ejemplar de DNI. Esta versión es opcional y no reemplaza a la tarjeta, sino que la complementa.

**¿Qué pasa con las versiones anteriores del DNI?** La Disposición 1154/2020 de la Dirección Nacional del Registro Nacional de las Personas aclaró que los DNI de nacionales y extranjeros se “seguirán expidiendo con las hojas personalizadas de datos aprobadas oportunamente, hasta agotar la existencia de materiales, momento a partir del cual se utilizará el diseño que se aprueba en la presente medida, manteniéndose vigentes todos los que fueran oportunamente emitidos, hasta tanto se efectivice su canje”.

![](https://assets.3dnoticias.com.ar/dni1.jpg)

Además, explicó que “como dijo el presidente Alberto Fernández, desde San Pedro, evocando a Juan Domingo Perón, la soberanía es el derecho y la responsabilidad que tenemos como pueblo y como Nación de ser dueños y artífices de nuestro propio destino”.

Por otra parte, Filmus, que encabeza la secretaría a cargo de la cuestión Malvinas, sostuvo que “la decisión de que el mapa bicontinental esté en los documentos nacionales de identidad tiene que ver con la importancia que cada argentina y argentino tome conciencia acerca de la soberanía sobre la Antártida, Malvinas y el Atlántico Sur y valore la enorme dimensión de nuestro país, que se extiende desde La Quiaca hasta el Polo Sur”.

“Este mismo mapa, con la nueva demarcación del límite exterior de la plataforma continental argentina que fue convertida en ley por unanimidad en el Congreso Nacional, llegará a todas las escuelas del país para que los chicos puedan apropiarse también de esta mirada”, aseguró Filmus.

En tanto, señaló que “hacer el anuncio en el Día de la Soberanía Nacional también tiene que ver con un eje central del gobierno, que es la defensa de los intereses soberanos como estrategia de desarrollo”.

El nuevo Documento Nacional de Identidad incluye una actualización el mapa de la República Argentina que se encuentra impreso en el frente y dorso de nuestro DNI, con la versión bicontinental actualizada para cumplir con lo establecido en la Ley 26.651 (Art. 1ro) de 2010.

Al momento de la presentación, **ya se cuenta con insumos para imprimir los primeros DNI físicos con el nuevo diseño, que saldrán durante la última semana de noviembre de la fábrica del Renaper**, donde se confeccionan.