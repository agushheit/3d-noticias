---
category: Estado Real
date: 2021-07-20T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/DELTA2.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Coronavirus: se detectó un segundo caso de un santafesino con la cepa delta'
title: 'Coronavirus: se detectó un segundo caso de un santafesino con la cepa delta'
entradilla: Es una turista proveniente de Estados Unidos que cumplió el aislamiento
  y ya recibió el alta correspondiente.

---
El Ministerio de Salud de la Provincia de Santa Fe, a través de la Dirección de Epidemiología, informa que se ha detectado un nuevo paciente de Coronavirus con la variante Delta en una viajera con residencia en el territorio provincial.

Se trata de una persona, proveniente de Estados Unidos el día 23 de junio, que desarrolló síntomas mientras realizaba el aislamiento en su domicilio, a los 6 días del arribo. Se estudió, dando resultado positivo. Continuó en aislamiento y con el control médico adecuado hasta el otorgamiento del alta epidemiológica. Sus contactos convivientes realizaron el aislamiento correspondiente y no desarrollando síntomas.

Se recuerda a la población que, para evitar la introducción y diseminación de esta nueva variante de preocupación, todos los viajeros provenientes del exterior deben arribar al país con un test de PCR realizado 72 horas previas al viaje. Una vez en el país se les realiza nuevamente la prueba en Ezeiza, y en caso de arrojar un resultado negativo, deberá cumplir el aislamiento estricto (sin contacto con personas que no hayan viajado) en su domicilio u otro lugar destinado para el mismo durante 7 días. Durante ese período serán monitoreados por si presentaran síntomas y se les realizará una nueva prueba PCR, pudiendo culminar el aislamiento si la misma resulta negativa.