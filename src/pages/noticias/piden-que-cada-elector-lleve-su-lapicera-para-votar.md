---
category: Agenda Ciudadana
date: 2021-09-08T06:15:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/MINISTERIO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Piden que cada elector lleve su lapicera para votar
title: Piden que cada elector lleve su lapicera para votar
entradilla: Es una de las recomendaciones que brindó el Ministerio de Salud de Santa
  Fe para las elecciones primarias de este domingo 12 de septiembre.

---
Desde el Ministerio de Salud de la provincia de Santa Fe se brindaron recomendaciones a la ciudadanía de cara a este próximo domingo 12 de septiembre, día en el que se desarrollarán las elecciones Primarias, Abiertas, Simultáneas y Obligatorias (PASO) en el país. "Quienes tengan algún síntoma compatible con Covid-19, estén cursando la enfermedad, sean contacto estrecho, o cumplan aislamiento, no deberán acercarse al local de votación designado", resaltaron desde la cartera sanitaria.

“Votar es un derecho que tenemos como ciudadanas y ciudadanos, argentinos o naturalizados, pero también debemos reconocer que estamos ante una situación muy particular frente a la pandemia del Covid-19”, explicó el secretario de Salud, Jorge Prieto.

En este contexto, el funcionario argumentó que “es importante que, frente a estas primarias, abiertas, simultáneas y obligatorias del próximo domingo 12 de septiembre, se tengan presentes todos los recaudos y recomendaciones actuales”.

Para ello, detalló: “Mantener la distancia social; asistir con el barbijo utilizado correctamente (cubriendo la nariz, boca y mentón); concurrir solo, sin compañía, si es posible; llevar lapicera; no cerrar el sobre con saliva; sanitizar las manos; respetar horarios del rango etario priorizado y recomendando las esperas en lugares abiertos. Estas son las claves fundamentales para sufragar sin riesgo”.

Cabe señalar que en cada lugar de votación y en cada mesa se contará con alcohol en gel para que las y los sufragantes puedan utilizar antes de votar.

Además, los establecimientos deberán cumplir con las medidas sanitarias dispuestas y contar con un facilitador sanitario por local con el objetivo de garantizar las condiciones establecidas y corroborar su cumplimiento.

**Casos en los que no deben concurrir a votar**

Si la persona es contacto estrecho de un caso positivo, está cursando la enfermedad o tiene algún síntoma compatible con Covid-19, no debe concurrir al lugar de votación. A partir del lunes 13 de septiembre podrá gestionar la eximición en www.electoral.gob.ar.

El secretario de Salud también advirtió que “toda persona que se encuentre cursando algún síntoma compatible con Covid-19 o contacto estrecho activo, le recordamos que debe comunicarse al 0800 555 6549. Esta línea se encuentra disponible las 24 horas del día y ha sido reforzada para optimizar las respuestas en esta fecha tan importante. Allí debe realizar esta denuncia para contar con la constancia correspondiente para justificar su ausencia en el acto electoral ante la Cámara Nacional Electoral”, cerró.