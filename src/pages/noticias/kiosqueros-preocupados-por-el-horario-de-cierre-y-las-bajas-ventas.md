---
category: Agenda Ciudadana
date: 2021-04-27T07:51:03-03:00
thumbnail: https://assets.3dnoticias.com.ar/kiosco.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de LT10
resumen: Kiosqueros preocupados por el horario de cierre y las bajas ventas
title: Kiosqueros preocupados por el horario de cierre y las bajas ventas
entradilla: Desde el sector se mostraron sorprendidos por la restricción horaria que
  los obliga a cerrar a las 20 cuando son un rubro esencial.

---
En el marco de las nuevas restricciones dispuestas por el Gobierno provincial, el comercio en general debe cerrar a las 19 y los negocios que venden alimentos a las 20.

Estos últimos abarca a los kiosqueros que están sorprendidos por esta franja horaria y consideran que fue una medida que se tomó sin consultarlos.  

En este sentido, Ricardo Mascheroni, integrante de la Cámara que nuclea al sector recordó que durante el 2020, los negocios estaban dentro de la categoría de esenciales y trabajaban hasta las 22 y algunas hasta las 23.

“No entendemos por qué de forma inconsulta el gobierno provincial restringió una actividad esencial a las 20 y permitió otras actividades no esenciales hasta más de las 22”.

Además, consideró que la restricción horaria los perjudica económicamente y perdieron muchas ventas.

Si bien, no realizaron una presentación formal, solicitan a las autoridades que revean las medidas tomadas porque “falta claridad”.

 A esta situación deben sumarle los hechos de inseguridad que los afectan a diario. En los últimos días, se incrementaron los robos y destrozos en kioscos y despensas del macrocentro.