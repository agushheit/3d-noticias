---
category: Agenda Ciudadana
date: 2021-09-14T06:15:59-03:00
thumbnail: https://assets.3dnoticias.com.ar/LLEGARONMAS.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Comenzó la distribución de más de 1,4 millones de vacunas Sputnik V, Pfizer,
  Moderna y CanSino
title: Comenzó la distribución de más de 1,4 millones de vacunas Sputnik V, Pfizer,
  Moderna y CanSino
entradilla: Las dosis serán repartidas entre este lunes y martes entre las 24 jurisdicciones
  del territorio nacional.

---
El Gobierno anunció que un total de 1.417.165 vacunas contra el Covid-19 correspondientes a la segunda dosis de Sputnik V, Pfizer, Moderna y Cansino empezaron a ser distribuidas entre las 24 jurisdicciones del territorio nacional.

Con el objetivo de avanzar con el plan de vacunación contra el coronavirus, entre este lunes y martes se repartirán 399.680 vacunas monodosis del laboratorio Cansino, cuyo segundo cargamento llegó el martes último a la madrugada, y que serán destinadas a poblaciones de difícil acceso como personas en situación de calle, migrantes y refugiados, entre otros.

Según informó oficialmente el Gobierno, también se repartirán de manera conjunta 760.625 dosis del segundo componente de Sputnik V para completar el esquema de vacunación, y 156.240 de Moderna para avanzar con la inmunización de adolescentes de 17 años.

Se hará lo mismo con las primeras 100.620 dosis de la vacuna del laboratorio Pfizer que arribaron a la Argentina el último miércoles como parte del contrato firmado por el Ejecutivo nacional para recibir 20 millones de vacunas durante este año, que estarán destinadas a adolescentes.

Asimismo, se distribuirán en todo el país 17.100 unidades de solución fisiológica estéril, como diluyente, 131.100 jeringas y 100.620 carnets de vacunación como insumos para la aplicación de la vacuna del laboratorio Pfizer ya que es una vacuna liofilizada.

Por otra parte, en la noche del pasado sábado arribaron al Aeropuerto Internacional de Ezeiza 530.300 vacunas AstraZeneca- Oxford y en la madrugada del domingo llegaron 1.116.010 Sputnik del componente 2, con las cuales la Argentina ya recibió más de 60 millones de dosis desde diciembre último, cuando arrancó el Plan de Vacunación, según resaltó la Casa Rosada.

De acuerdo al Monitor Público de Vacunación, hasta este lunes por la mañana se distribuyeron 51.999.944 dosis en todo el territorio, al tiempo que las aplicaciones totalizan 46.995.734. A su vez, 28.721.805 personas recibieron la primera dosis y 18.273.929 ya tienen dos aplicaciones.