---
category: Agenda Ciudadana
date: 2021-07-16T06:00:48-03:00
thumbnail: https://assets.3dnoticias.com.ar/CONFLICTUADOS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Santa Fe recibirá aportes económicos nacionales para la inserción social
  de jóvenes en conflicto con la ley
title: Santa Fe recibirá aportes económicos nacionales para la inserción social de
  jóvenes en conflicto con la ley
entradilla: En el marco de los convenios firmados entre los gobiernos provincial y
  nacional, la Dirección de Justicia Penal Juvenil dispondrá de los aportes con el
  objetivo de brindar herramientas educativas y de contención.

---
Los ministros de Gobierno, Justicia y Derechos Humanos y de Desarrollo Social, Roberto Sukerman y Danilo Capitani, junto al secretario de Justicia, Gabriel Somaglia, y al director de Justicia Penal Juvenil, Federico Lombardi, recibieron este jueves a funcionarios de la Secretaria Nacional de Niñez, Adolescencia y Familia (Sennaf), para avanzar en la implementación de los convenios de colaboración firmado entre Santa Fe y el Estado Nacional.

En la oportunidad, Sukeman destacó que “es fundamental el trabajo coordinado con Nación, desde los distintos ministerios, todas las políticas de inclusión son trascendentes. Este tipo de herramientas son fundamentales, para que en los casos en los que caen en el delito, el Estado los acompañe con políticas diferenciadas, teniendo en cuenta las convenciones internacionales de los Derechos del Niño”.

Por su parte, el secretario Nacional de Niñez, Adolescencia y Familia, Gabriel Lerner, aseguró que “en la secretaria concebimos que las posibilidades de mejorar las condiciones de chicas y chicos en nuestro país, es con un trabajo fuertemente articulado con las provincias, municipios y ONG”.

“La Sennaf preside el Consejo Federal de Niñez, creado por la Ley de Protección Integral de los Derechos de las Niñas, Niños y Adolescentes, y allí se desarrollan los acuerdos básicos de una adecuada política penal para adolescentes infractores, respetuosos de los derechos humanos”, indicó el secretario.

“En ese marco tenemos convenios de ida y vuelta con Santa Fe. El que respecta al Sistema Único Nominal brinda a la provincia un sistema digitalizado sobre menores infractores, y la permite a la Nación construir datos: cuantos jóvenes están privados de libertad en Argentina y cuantos están con medidas judiciales”, concluyó Lerner.

En tanto, el secretario de Justicia de Santa Fe destacó que “las autoridades de la Nación están al lado de la Provincia desde el inicio de la gestión en cuanto a las políticas relacionadas con menores entre 16 y 18 años de edad, que se encuentran en situación de infracción con la ley penal y tienen un régimen especial de alojamiento”.

“Hemos decidido, con el apoyo expreso del presidente de la Nación, Alberto Fernández, y del gobernador Omar Perotti, trabajar fuertemente en el proceso que transitan los menores, para insertarlos en la vida social lo más dignamente posible. Con el apoyo que hemos tenido en diferentes programas, en esta ocasión queremos agradecer en persona lo que hemos recibido”, concluyó Somaglia.

También el ministro de Desarrollo Social, Danilo Capitani, manifestó su satisfacción por el "trabajo mancomunado que venimos haciendo con el gobierno nacional, y en especial con el ministro Daniel Arroyo".

Y agregó: Estas políticas conjuntas, que tienen como eje la promoción de derechos de niñas, niños y adolescentes se ven reflejadas en la importante inversión que destinamos para garantizar los derechos de este sector de la población".

“Hay una firme decisión del gobernador Omar Perotti y del gobierno nacional de fortalecer las políticas sociales destinadas a las infancias, y en particular en este contexto de pandemia”, finalizó Capitani.

**LOS CONVENIOS**

Por último, el director Provincial de Justicia Penal Juvenil, Federico Lombardi, precisó que “son cuatro los convenios vigentes firmados con la Sennaf, de los cuales tres tienen que ver con talleres socioeducativos y fortalecimiento institucional, para que los jóvenes que están atravesando un proceso penal, puedan desarrollar actividades de formación, oficio y apoyo escolar”.

El cuarto “convenio es para desarrollar el Sistema RUN, que es el Registro Único Nominal, que va a permitir la recolección de datos por parte de Nación de cada una de las jurisdicciones, para poder elaborar políticas públicas efectivas, ya que el actual data de 1998; y a la provincia adquirir equipamiento y mobiliario”, detalló.

Además, los acuerdos prevén otorgar una beca a los jóvenes que transitan el proceso penal en libertad, dándoles la oportunidad de inserción social a través de la educación.

Para finalizar, Lombardi aseguro que “se trabaja para incluir a los adolescentes que están dentro del sistema penal, cambiarles la subjetividad y que pueda crear un proyecto de vida en sociedad”.

También estuvieron presentes el subsecretario Nacional de Niñez, Adolescencia y Familia, Mariano Longo; y la directora Nacional de Adolescentes Infractores a la Ley Penal, Gloria Bonato.