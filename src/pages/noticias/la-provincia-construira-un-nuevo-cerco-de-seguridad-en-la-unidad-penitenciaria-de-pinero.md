---
category: Estado Real
date: 2021-09-18T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/PIÑERO.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La provincia construirá un nuevo cerco de seguridad en la unidad penitenciaria
  de Piñero
title: La provincia construirá un nuevo cerco de seguridad en la unidad penitenciaria
  de Piñero
entradilla: La obra, que brindará mayor nivel de protección para el penal, comprenderá
  la iluminación del perímetro, sumando 150 reflectores LED y la construcción de un
  muro que tendrá 1770 metros de largo y 5.65 metros de altura.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Arquitectura y Obras Públicas, llevará adelante un concurso de precios para construir un cerco de seguridad para la Unidad Penitenciaria Nº 11, ubicada en la ruta Provincial Nº 14, a la altura del kilómetro 13.5, en la localidad de Piñero, departamento Rosario.

Los trabajos, que tienen un presupuesto oficial de $191.842.542,83, contemplan un plazo de ejecución de 120 días y se licitarán el próximo miércoles 22 de septiembre, a las 11 horas, en el penal.

**La obra**

El cerco se conforma por elementos premoldeados de hormigón armado, columnas esquineras e intermedias y paneles A 20, con módulos de 6 metros. Los paneles tienen un espesor de 20cm, y el módulo se conforma con 4 paneles de 1.25m, logrando una altura de 4.75m de muro de hormigón armado. Por encima de los paneles, se coloca una concertina de alambre y cuchillas de hierro galvanizado, alcanzando una altura total de 5.65 metros.

Además, el pliego contempla los trabajos preliminares necesarios para efectuar la obra, así como también demoliciones y retiro de sectores del cerco existente, movimiento de suelo, limpieza y custodia.

El proyecto se incorpora la iluminación, conformada por 150 reflectores LED de 100W, para brindar mayor visibilidad y aumentar el nivel de seguridad del muro.