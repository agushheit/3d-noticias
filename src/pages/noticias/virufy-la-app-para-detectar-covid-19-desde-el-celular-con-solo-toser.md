---
category: Agenda Ciudadana
date: 2021-02-03T06:51:10-03:00
thumbnail: https://assets.3dnoticias.com.ar/virufi.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Infobae
resumen: Virufy, la app para detectar COVID-19 desde el celular con solo toser
title: Virufy, la app para detectar COVID-19 desde el celular con solo toser
entradilla: Tiene una precisión del 80% y fue desarrollada por una organización sin
  fines de lucro compuesta por investigadores de 25 universidades de 20 países.

---
Jóvenes profesionales de 20 países y representantes de prestigiosas universidades del mundo desarrollaron Virufy, una aplicación que permite diagnosticar COVID-19 mediante el análisis de la tos de un usuario. Con inteligencia artificial (IA), la app tiene una precisión del 80%, con una base de datos de la tos de usuarios con síntomas similares al virus. Es posible donar audio de la tos para agrandar la base de datos.

Virufy es una organización sin fines de lucro compuesta por más de 50 investigadores internacionales de 25 universidades (como Stanford y Princeton), de 20 países diferentes como Inglaterra, Japón, Estados Unidos, Argentina, Brasil, Colombia, México y Perú. La app web (no se descarga) utiliza IA para evaluar la probabilidad de que una persona esté infectada con COVID-19.

El equipo de Virufy validó su algoritmo de aprendizaje automático basado en miles de toses de América Latina, Europa y Asia para distinguir entre los sonidos de tos positivos y negativos del SARS-CoV-2 con un 80% de precisión, hasta ahora.

La app tiene el objetivo de entrenar su algoritmo para lograr una precisión aun mayor y comprender mejor cómo suena en la tos la COVID-19. Desde la organización quieren ampliar su base de datos de tos en América Latina, una región que continúa registrando un aumento alarmante de casos positivos.

[![Mira el video](https://assets.3dnoticias.com.ar/virufy-app-covid-19.webp)](https://youtu.be/UxA09RtTBwU "Reproducir")

Virufy no reemplaza las pruebas de diagnóstico de grado hospitalario y debe usarse junto con los controles de síntomas y temperatura. La detección temprana e inmediata alentará a llevar a cabo la cuarentena informada.

Amil Khanzada, ingeniero de software de Silicon Valley y fundador de Virufy, dijo: “Solicitamos a las personas que viven en Argentina, Bolivia, Brasil, Colombia, México o Perú, y que presenten síntomas similares a COVID-19 que visiten virufy.org/app y donen su tos para que podamos unirnos para poner fin rápidamente a esta pandemia mortal”.

Por su parte, Marcos Deza, voluntario argentino de Virufy, explicó a Infobae: “La idea de Virufy es que el mundo entero pueda trabajar para acabar con la pandemia. Es compatible con cualquier teléfono, ya que no es una aplicación que haya que descargar en este momento, sino que es una aplicación web porque estamos en instancia de recolección de datos. En cuanto la aplicación puede lanzar resultados instantáneos, que es a lo que apuntamos, probablemente ahí sí sea algo descargable, o no, todavía no se sabe, pero la idea es que sí”.

Respecto de la opinión que merece este tipo de desarrollo, Evangelina Cueto, pediatra y subinvestigadora en protocolo de una de las vacunas de COVID-19, señaló a Infobae: “Esta app es una herramienta tentadora en tanto aporta datos sobre la semiología de la COVID-19, pero en este momento no sustituye ni a las pruebas diagnósticas vigentes ni la interpretación clínica. Muy pocas veces se reduce un signo o un síntoma a la clave diagnóstica, nunca es uno, es un conjunto. En el caso de la COVID-19, la tos no es un signo cardinal”.

En este sentido, señala que profundizar en la semiología de las enfermedades, es decir, en el conjunto de signos y de síntomas que las componen, ha sido históricamente una de las búsquedas fundamentales de la medicina. “Sin embargo, un signo o síntoma de manera aislada (en este caso la tos), sin ser interpretada dentro de un cuadro global, rara vez compone una clave diagnóstica”, agregó.

Por otro lado, Lilian Teston, coordinadora de departamento de epidemiología del grupo Stamboulian, dijo a Infobae: “Es un algoritmo interesante que intenta analizar predictores de enfermedad severa, tiene la limitación de que no todos los pacientes presentan sintomatología respiratoria. Como bien aclaran los científicos que la crearon debe acompañarse de otros síntomas para ampliar la población estudiada. En este momento de la pandemia todas las herramientas son necesarias para conocer la evolución de la infección y poder realizar cualquier intervención que prevenga el desarrollo de enfermedad grave”.