---
category: La Ciudad
date: 2021-12-04T06:00:23-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGENDADICIEMBRE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Agenda cultural: diciembre de grandes eventos y ferias en la ciudad'
title: 'Agenda cultural: diciembre de grandes eventos y ferias en la ciudad'
entradilla: A partir del trabajo con otras instituciones, iniciativas independientes
  y producciones propias, la Municipalidad invita este mes a disfrutar de encuentros
  convocantes que regresan después de una pausa en 2020.

---
La programación de diciembre profundiza la recuperación de la actividad cultural y celebra el regreso de encuentros que durante el año pasado no pudieron realizarse o lo hicieron en formatos adaptados. Además del XVI Argentino de Artes Escénicas que organiza la UNL; y la XXVII Feria del Libro, que comenzaron el miércoles, para las próximas semanas están previstos el Gran Concierto Homenaje a Ariel Ramírez en una coproducción de la Municipalidad con el Ministerio de Cultura de la provincia de Santa Fe; el XXIII Festival de Jazz de Santa Fe, La Diseña y la tradicional feria navideña de los Artesanos del Sol y la Luna, entre otras propuestas.

**Encuentro con la lectura**

La XXVII Feria del Libro de Santa Fe se realiza en el Mercado Progreso (Balcarce 1635), a partir de una organización conjunta entre la Municipalidad, la Universidad Nacional del Litoral y el Ministerio de Cultura de la provincia. Hasta el domingo, de 18 a 23 horas se pueden recorrer con entrada gratuita los stands de librerías, editoriales y bibliotecas; y participar de mesas de lecturas, talleres, presentaciones de libros y música en vivo.

Entre las propuestas del fin de semana, hoy a las 20, en el patio del Mercado, se hará entrega del Premio Literario de la Municipalidad de Santa Fe 2021 a la escritora Marianela Alegre por “…y en el medio el río”. El certamen reconoce por primera vez a producciones dentro del género dramaturgia, a partir de una serie de modificaciones que se realizaron para ampliar los alcances del concurso. En el encuentro se entregarán las menciones a Cecilia Arellano, Lautaro Ruatta y Julieta Vigo, que también fueron seleccionados por el Jurado que integraron Sandra Ester Franzen, María Rosa Pfeiffer y Silvia Susana Beltrán.

A la misma hora, pero en la Sala Bicentenario, la historiadora Magdalena Candioti presentará “Una historia de la emancipación negra. Esclavitud y abolición en la Argentina”, que publicó el sello Siglo XXI Editores. Cristiana Schettini Pereira modera el encuentro sobre esta obra en la que Candioti se pregunta por el lugar tienen los esclavos de origen africano en la narrativa identitaria argentina.

El sábado 4 a las 19, se presentará el ensayo “Los más solos de la tierra: acerca del Síndrome de Asperger” de Silvia Susana Beltrán, que ganó el Premio Literario en 2020, donde reflexiona sobre la relación entre una madre y su hijo, diagnosticado con Asperger, y analiza la soledad que deviene de esta condición. En su veredicto, el Jurado destacó que se trata de “un ensayo híbrido, que utiliza recursos propios del testimonio y la autoficción, en el que la experiencia personal y la observación directa son herramientas válidas para la construcción de conocimiento, aunque no de un conocimiento científico y aséptico, sino afectivo”.

**Funciones en el Teatro Municipal**

Con la impronta federal que lo caracteriza, el XVI Argentino de Artes Escénicas propone hasta el 5 de diciembre charlas, talleres, presentaciones y obras de Río Negro, Buenos Aires, Córdoba, Mendoza y Santa Fe, seleccionadas en una convocatoria abierta. El encuentro se desarrolla en el Foro Cultural Universitario y en el Teatro Municipal “1° de Mayo” y es organizado por la Universidad Nacional del Litoral, con el auspicio de la Municipalidad.

Las funciones en la Sala Marechal comienzan a las 20 horas, y en Sala Mayor fueron programadas a las 21.30. El viernes, en la Sala Marechal se presenta “La Moreira”, una relectura con perspectiva de género de la obra inaugural del Teatro Argentino “Juan Moreira”, de Eduardo Gutiérrez, que dirige Florencea Fernández. En el escenario mayor, el Grupo Concertado La Puerta de Oro interpreta a Ilo y Teo, dos hermanos migrantes que están varados frente a un puerto que se erige como la esperanza de entrar a un nuevo mundo. “La puerta de oro” es el título de la puesta, con dramaturgia y dirección general de Arístides Vargas.

El sábado 4, en la Sala Marechal, Laura Ortiz y Diana Lerma del grupo Convención Teatro (Córdoba), llevan a escena “Lengua Madre” con dirección de Nicolás Giovanna y Daniela Martín. La obra se basa en la novela de María Teresa Andruetto sobre los mecanismos de la memoria, las distintas formas del exilio y las lentas construcciones de la identidad. Esa misma noche, pero en la Sala Mayor, el Equipo Espacio Callejón presenta “Luz Testigo”, escrita por Tomás Afán, Marina Artigas, Rubén De la Torre, Julián Marcove y Agustín Meneses. Cinco historias separadas que, ensambladas por Javier Daulte, logran reflejar con mayor profundidad las sensaciones vividas durante el año 2020, sin necesidad de hablar directamente de lo que sucedió.

Las entradas para las actividades del Argentino de Artes Escénicas son gratuitas. Para las obras, estarán sujetas a la capacidad de las salas y se podrán retirar en el Foro Cultural UNL (9 de julio 2150) el mismo día de cada función a partir de las 12 horas.

Por fuera de esta programación, como parte de las muestras de fin de año que realizan las academias y estudios de danza, el domingo 5 a las 20.30 horas, en la Sala Mayor, se realizará “Festival Panambí”. Las entradas anticipadas, se venden a $400; y el día de la función, a $500.

**Noches corales**

Agrupaciones santafesinas protagonizan un encuentro organizado por la Municipalidad para seguir revalorizando la actividad lírica y el canto. Estudio Coral Meridies y el Coro Polifónico Merced se presentaron anoche en el Anfiteatro “Juan de Garay” (Avda. Illia y San Jerónimo), bajo la dirección de Virginia Bono y Rodrigo Naffa respectivamente. Este viernes desde las 21, en el mismo escenario, será el turno de la Agrupación Coral Ars Nova junto al Coro de la UTN y Gloriana, el coro de rock y pop de la Asociación Argentina de Cultura Inglesa. La entrada es gratuita y se puede retirar previamente en la boletería del Teatro Municipal, de 9 a 13 y de 17 a 21.

**Somos Música**

Las ocho orquestas para las infancias que integran Somos Música se encontrarán el sábado 4 de diciembre en el Anfiteatro para despedir el ciclo 2021, en un concierto abierto a todo público. A las 19 horas, se presentarán los Ensambles de Guadalupe, Alto Verde, Barranquitas y Cabaña Leiva; y a partir de las 21, los de Las Flores, El Pozo, Yapeyú y Santa Rosa de Lima. La entrada es gratuita y se puede retirar previamente en la boletería del Teatro Municipal, de 9 a 13 horas y de 17 a 21 horas.

**Museo Ocasional de un Paisaje Increíble**

BIENALSUR, la Bienal Internacional de Arte Contemporáneo del Sur organizada por la Universidad Nacional de Tres de Febrero (UNTREF), desarrollará el sábado 4 entre las 11 y las 18 horas en la Isla Clucellas, un proyecto que celebra las identidades y la cultura del Litoral argentino.

El proyecto, que lleva por nombre “Museo Ocasional de un Paisaje Increíble”, durará un solo día. Incluirá una colección de arte popular junto a obras de las colecciones de los museos Histórico Provincial Brigadier General Estanislao López, de Ciencias Naturales Florentino Ameghino y el Etnográfico y Colonial Juan de Garay.

Con acompañamiento curatorial de Florencia Battiti, la exposición busca poner en diálogo una variedad de piezas que rescatan la memoria social, las identidades y la cultura de esa particular zona de la geografía argentina.

La propuesta está coordinada por las artistas Ana Vogelfang y Julieta García Vázquez y cuenta con la participación de docentes y alumnos de la Escuela Provincial de Artes Visuales Profesor Juan Mantovani, el Museo Municipal de Artes Visuales Sor Josefa Díaz y Clucellas, Ayelén Coccoz, Juliana Frías, Florentino González, María Hernández y familia, Tusi Horn, Celeste Medrano, Analía Molinari, Antonela Peretti, Ivan Rosado, Ratona Colectivo y Liro, Marta y Melina Cecilia Lucero.

El lugar de encuentro para el cruce es “Puerto Piojo”. Habrá salidas en embarcaciones pequeñas hacia la isla Clucellas a las 11, 12, 13, 14, 15 y 17 horas. Los regresos serán a las 11.45, 12.45, 13.45, 14.45, 15.45, 16.45, 17.45 y 19 horas. Se recomienda asistir provistos de agua, protección solar, repelente de insectos, calzado cerrado y vestimenta adecuada a un lugar agreste sin instalaciones sanitarias. En la isla se contará con seguridad, asistencia sanitaria y punto de hidratación.

**Salas y Museos**

Desde anoche, se puede visitar la Casa Museo López Claro después de una primera etapa de obras para reparar el inmueble y mejorar las condiciones de guarda del patrimonio artístico que el artista y su compañera María Brizzi donaron a la ciudad. Con la reapertura del espacio ubicado en Piedras al 7352 quedó habilitada la muestra “López Claro. Inspiración americana” compuesta por obras del período americanista, realizadas entre 1950 y 1955, después de los viajes por Ecuador y Bolivia. Se puede visitar de martes a viernes, de 9.30 a 12.30; y los sábados, de 17 a 20 horas. Siempre con entrada libre y gratuita.

De miércoles a sábado, de 9.30 a 12.30 y de 17 a 20; los domingos y feriados, de 17 a 20 horas se puede recorrer el “Salón Litoral 2021”, en el Museo Municipal de Artes Visuales “Sor Josefa Díaz y Clucellas” (San Martín 2068). La muestra reúne obras de 30 artistas seleccionados entre 227 que se postularon de todo el país. El montaje incluye la obra de María Paula Massarutti que recibió el Premio Adquisición, las de Virginia Abrigo, Soledad Rolleri, Jerónimo Veroa, Luciana Paoletti y Daniela Arnaudo, que recibieron premios estímulo; y tres que recibieron menciones honoríficas de Gisela Ajzensztat, Rodrigo Illescas y María Eugenia Pérez.

A partir de este mes el Museo de la Constitución se podrá visitar de jueves a domingos, de 17 a 20 horas, con entrada libre y gratuita. Además de recorrer las salas previstas en la visita se podrá participar de la intervención artística “Disposiciones transitorias” que continúa con la coordinación de Victoria Ferreyra y Raquel Minetti del colectivo Proyecto Deatres, en la Sala Cero del espacio ubicado en Avenida Circunvalación y 1º de Mayo.

Noelia Bustaver, artista santafesina conocida también como Aome Katze, presenta “Keep calm and 화이팅” en el Centro Experimental del Color (ala oeste de la Estación Belgrano, Bv. Gálvez 1150). La muestra reúne pinturas, instalaciones interactivas y objetos. Todas las obras son inéditas y algunas de ellas fueron pensadas exclusivamente para este espacio. Su propuesta fue seleccionada en la convocatoria 2021 para proyectos expositivos en Museos y Espacios Municipales de Arte. “Keep calm and 화이팅” puede visitarse de martes a viernes, de 9.30 a 12.30 horas, sábados y domingos de 17 a 20 horas.