---
category: La Ciudad
date: 2021-09-18T06:00:35-03:00
thumbnail: https://assets.3dnoticias.com.ar/BIENALSUR.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: BIENALSUR llegó a Santa Fe
title: BIENALSUR llegó a Santa Fe
entradilla: Este jueves se celebró la apertura de BIENALSUR en la ciudad, con la inauguración
  de “Cómo llegan las flores a la tela” y “Manos randeras tejiendo recuerdos” en el
  Museo Municipal de Artes Visuales.

---
“Para la ciudadanía y para los trabajadores del arte es todo un desafío entrar en este entramado latinoamericano: nuestros artistas santafesinos están ahora dentro de BIENALSUR, que anda por el mundo y hoy está en Santa Fe”, confirmó Emilio Jatón este jueves por la noche en la apertura de dos de las muestras con las que la ciudad es sede de la 3° edición de la Bienal Internacional de Arte Contemporáneo de América del Sur, plataforma de colaboración artística que incluye a más de 400 artistas en un centenar de sedes de 50 ciudades, en 23 países, organizada por la Universidad Nacional de Tres de Febrero (UNTREF).

Santa Fe se suma a la Bienal con dos muestras en el Museo Municipal de Artes Visuales: “Cómo llegan las flores a la tela” y “Manos randeras tejiendo recuerdos”; y otra en el Centro Experimental del Color (CEC) ubicado en el ala oeste de Estación Belgrano, “Paraje vecino”, que ya pueden visitarse con entrada libre y gratuita.

El secretario de Educación y Cultura del municipio, Paulo Ricci, destacó que la participación de Santa Fe “es una manera de llevar Santa Fe al mundo, así como una decisión de llevar a la cultura como mascarón de proa de la gestión, con la reactivación cultural que por suerte estamos pudiendo llevar adelante en estos meses”.

**Paraje vecino**

“El territorio como motivo de la interpretación artística, los barrios, nuestra zona en la costa del litoral, es lo que podemos ver en ‘Paraje vecino’, con una calidad artística enorme que pone en valor oficios y prácticas ancestrales”, describió Ricci sobre la puesta en el CEC, “Paraje vecino. Prácticas relacionales en La Boca”, que cuenta con el acompañamiento curatorial de Florencia Qualina y Leandro Martínez Depietri de BIENALSUR.

Esta muestra reúne obras y procesos de Ángeles Rivero, Florencia Horn, Luciano Giardino, Valeria Barbero y Laura Hotham, artistas seleccionados para participar de la residencia artística organizada por la Municipalidad en el Paraje La Boca (Alto Verde), que se desarrolló junto a vecinas y vecinos de la comunidad, durante febrero y marzo de este año.

Las producciones que se presentan son resultado del trabajo conjunto en diálogo con objetos y obras provenientes de los acervos del Museo Municipal de Artes Visuales Sor Josefa Díaz Clucellas, el Museo César Fernández Navarro, el Museo César López Claro y el Museo de Liro, desarrollado por una vecina de La Boca. Puede visitarse de martes a viernes, de 9.30 a 12.30, y los sábados y domingos, de 17 a 20 horas, hasta el 14 de noviembre.

**Producciones comunitarias**

Leandro Martínez Depietri, parte del Equipo curatorial de BIENALSUR, explicó que la presente edición que se desarrolla hasta diciembre se propone interpelar al público de otras maneras. “En este caso estamos trabajando con obras que provienen de producciones comunitarias.‘¿Cómo llegan las flores a las telas?’, con la curaduría de Constanza Martínez, trabaja con las mantas santiagueñas que tienen un enorme valor artístico, y son habitualmente hechas con fines utilitarios, son cobijas, que acá estamos repensando desde el arte contemporáneo para acercar otra mirada sobre este patrimonio de nuestro país”, describió la muestra que reúne obras de Guido Yannitto, Chiachio & Giannone, Luis González Palma, Emma Herbin, Ariadna Pastorini, Verónica Ryan y Nushi Muntaabski y de la Colección Teleras de Belén Carballo y Ricardo Paz.

A su vez, puntualizó Martínez Depetri, “va en diálogo con el trabajo de de Alejandra Mizrahi en ‘Manos randeras tejiendo recuerdos’, que pone en valor el tejido de mujeres de El Cercado, una localidad rural de la zona de Monteros, en la provincia de Tucumán, de 500 años de antigüedad, muy desconocido y de enorme riqueza”, sobre la muestra que reúne randas de Margarita Ariza, Silvia Amado, Anice Ariza, Claudia Aybar, Gabriela Belmonte, Tatiana Belmonte, Norma Briseño, Cristina Costilla, Silvia González, María Magdalena Nuñez, María Marcelina Nuñez, Elba Sosa, Yohana Torres, Mirta Costilla, Ana Belén Costilla Ariza, María Laura González, Magui Ariza, Elva Aybar, María Ofelia Belmonte, Camila Nieva, Ely Pacheco, Giselle Paz, Agustina Sosa, Sara Rosa Sosa y Marta Dolores Nuñez, tejedoras tucumanas. Ambas muestras pueden visitarse en el Sor Josefa hasta el 31 de octubre de miércoles a sábados, de 9.30 a 12.30 y 16 a 19 horas, y los domingos de 16 a 19.

“Esto se cruza con la muestra de CEC que propuso la Municipalidad en Museo Abierto, con un grupo de artistas contemporáneos trabajando con los vecinos de La Boca, y creo que son un excelente ejemplo de lo que es el trabajo comunitario desde las artes y cómo el arte puede activar nuevos sentidos sobre el territorio y sobre la producción cultural”, afirmó el curador.

**Memoria a través del arte**

Por su parte, Aníbal Jozami, rector de la Universidad Nacional de Tres de Febrero (UNTREF), explicó que “Bienal Sur es un proyecto que tiende a estar simultáneamente en la mayor cantidad posible de lugares de los cinco continentes, en cada edición vamos creciendo y Santa Fe es una ciudad que muchos de nosotros conocemos bien, con un gran historia de militancia política universitaria”, en relación a la tradición de la Reforma Universitaria de 1918 y el espíritu compartido de llevar la Universidad, los museos y las artes hacia las comunidades y los públicos.

“Estas piezas textiles tienen una altísima calidad pero sin embargo nunca se muestran en Argentina; por ejemplo en Salta hicimos entrar por primera vez al Museo de Bellas Artes la producción de las comunidades originarias de la frontera argentina con Bolivia y Perú. Estamos intentando llegar a través del arte a las emociones y la mentalidad, para reflexionar sobre nuestro pasado, presente y futuro, eso es BIENALSUR”, afirmó Jozami.