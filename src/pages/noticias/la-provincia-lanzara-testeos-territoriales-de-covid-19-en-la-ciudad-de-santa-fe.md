---
category: Estado Real
date: 2021-06-12T06:15:38-03:00
thumbnail: https://assets.3dnoticias.com.ar/TERRITORIALES.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia lanzará testeos territoriales de covid-19 en la ciudad de Santa
  Fe
title: La Provincia lanzará testeos territoriales de covid-19 en la ciudad de Santa
  Fe
entradilla: Se trata de cinco unidades que funcionarán en diferentes puntos estratégicos.
  Se podrá asistir sin turno previo.

---
En el marco de la segunda ola de Coronavirus, el Ministerio de Salud, despliega herramientas territoriales que garanticen la detección temprana de casos para cortar la cadena de contagios.

En ese sentido, desde este sábado 12 de junio funcionarán 3 puestos fijos de testeos en la ciudad de Santa Fe. Los mismos estarán ubicados en El Alero (French 1701); en la Estación Belgrano (Bv. Gálvez y Dorrego); y en barrio Centenario (Urquiza y JJ Paso).

Asimismo, desde el lunes 14 de junio se sumará un cuarto dispositivo en la Costanera (Almirante Brown 5294 - Centro Deportivo Municipal). Asimismo, a estas postas se les sumará el camión sanitario del operativo DetectAr que se trasladará por diferentes puntos de la ciudad.

Los distintos testeos territoriales fijos funcionarán todos los días de 9 a 13, sin turno previo.

Al respecto, Miguel Pedrola, integrante de la cartera sanitaria provincial, puntualizó que “el objetivo que tenemos es tratar de llegar a la mayor cantidad de personas a través del testeo rápido. Por eso, invitamos a todos aquellos que tengan al menos un síntoma o quienes sean asintomáticos y se consideren contacto estrecho”.

Por último, señaló que la provincia “está haciendo un fuerte operativo en cuanto a vacunación y en forma preventiva en cuanto a testeos. Sabemos que debemos llegar en forma rápida a testear para tener un diagnóstico oportuno y el aislamiento de esa persona, para evitar o cortar la tasa de contagios y empezar a ver caer la curva de casos en el territorio provincial”, concluyó.