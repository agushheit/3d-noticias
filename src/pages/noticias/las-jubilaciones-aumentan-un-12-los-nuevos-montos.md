---
category: Agenda Ciudadana
date: 2021-05-11T08:56:14-03:00
thumbnail: https://assets.3dnoticias.com.ar/jubilados.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: NA
resumen: 'Las jubilaciones aumentan un 12%: los nuevos montos'
title: 'Las jubilaciones aumentan un 12%: los nuevos montos'
entradilla: Con este incremento, el haber jubilatorio mínimo a partir de junio ascenderá
  a 23.064,70 pesos.

---
El aumento trimestral para las jubilaciones, pensiones y asignaciones será del 12,12 por ciento, según anunció Anses. Se trata del segundo incremento otorgado mediante la Ley de Movilidad impulsada por el presidente Alberto Fernández.

Este aumento también impacta en otras Asignaciones Familiares, como la de embarazo, prenatal, nacimiento, adopción y matrimonio. Con este incremento, el haber jubilatorio mínimo a partir de junio ascenderá a 23.064,70 pesos.

Por su parte, el haber máximo aumentará a 155.203,65 pesos. Y la Asignación por Hijo y la Asignación por Embarazo aumentarán a 4504 pesos.