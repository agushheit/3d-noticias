---
category: Deportes
date: 2020-11-30T11:32:49Z
thumbnail: https://assets.3dnoticias.com.ar/colon-centralcordoba.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Por Clara Blanco
resumen: 'Cortó la racha: Colón no pudo contra Independiente'
title: 'Cortó la racha: Colón no pudo contra Independiente'
entradilla: Con goles de Jonathan Menéndez y Alan Velasco, el Rojo se impuso en el
  Brigadier López. Mientras que para el Sabalero marcó Brian Fernández sobre el final
  del partido.

---
Colón recibió a Independiente por la fecha 5 de la Copa Diego Maradona y esta vez no pudo sumar. El Sabalero perdió por 2 a 1 con goles de Jonathan Menéndez y Alan Velasco, mientras que para el local descontó Brian Fernández.

En el primer tiempo, el equipo de Domínguez fue más que el Rojo y tuvo chances de abrir el marcador de la mano de Pulga Rodríguez a los 7 minutos y Christian Bernardi a los 25, en otra jugada.

En el complemento se vio un mejor rendimiento de Colón, pero fue donde llegaron los goles del rival. Menéndez y Velazco marcaron para darle la victoria al Rojo. Pero la historia no se iba a cerrar así, ya que sobre el final del partido Brian Fernández descontó marcando su primer gol con la camiseta del Sabalero.

Pese a esta derrota, Colón sigue siendo el puntero de la Zona 2 y está clasificado a la Fase Campeonato a falta de una fecha. El próximo rival del Sabalero será Central Córdoba, con el cual se enfrentará el viernes 4 a las 19:10 en Santiago del Estero.