---
category: Estado Real
date: 2021-03-15T06:00:31-03:00
thumbnail: https://assets.3dnoticias.com.ar/rincon.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: La Provincia proyecta las obras del sistema de defensa contra inundaciones
  en San José Del Rincón y Arroyo Leyes
title: La Provincia proyecta las obras del sistema de defensa contra inundaciones
  en San José Del Rincón y Arroyo Leyes
entradilla: Se contratará un servicio de consultoría para desarrollar el proyecto
  ejecutivo de las obras de protección contra inundaciones en el sector que se ubica
  al oeste de la ruta provincial N°1, en el departamento La Capital.

---
El Ministerio de Infraestructura, Servicios Públicos y Hábitat, a través de la Secretaría de Recursos Hídricos, celebrará un concurso público para contratar un servicio de consultoría a fin de desarrollar el proyecto ejecutivo del sistema de defensas, para el sector de los distritos Arroyo Leyes y San José del Rincón, que se ubica al oeste de la ruta provincial N° 1.

Cabe destacar, que el proyecto ejecutivo tendrá un plazo de realización de seis meses y demandará una inversión provincial de 10.762.969,13 de pesos.

Al respecto, el secretario de Recursos Hídricos, Roberto Gioria explicó que el proyecto ejecutivo a elaborar abarca tres partes: “por un lado, las obras de defensa ante inundaciones; en segundo lugar, los desagües pluviales del sector urbano defendido que descarguen hacia la defensa, con reservorios, estaciones de bombeo y descargas correspondientes. Y tercero, el diseño de medidas no estructurales relacionadas a la gestión ambiental y a la prevención hídrica, lo que significa elaborar una evaluación de impacto ambiental, la operación y mantenimiento de las obras, una propuesta de zonificación del uso del suelo de la futura área defendida y los lineamientos para un futuro plan de contingencia”.

**Concurso Público**

El acto se realizará el día jueves 18 de marzo, a las 9.30 horas, en la Comuna de Arroyo Leyes, ruta provincial Nº 1 Km. 12,5, de la localidad de Arroyo Leyes.