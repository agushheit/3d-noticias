---
category: La Ciudad
date: 2021-04-11T06:15:19-03:00
thumbnail: https://assets.3dnoticias.com.ar/plasma1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Prevención Salud y el CUDAIO, impulsan la idea de donar plasma
title: Prevención Salud y el CUDAIO, impulsan la idea de donar plasma
entradilla: Es una inédita acción conjunta del CUDAIO y la prepaga médica del Grupo
  Sancor Seguros bajo el slogan "Plasmá tu solidaridad". Será este lunes 19 de 8 a
  13, en San Martín 2061

---
Prevención Salud, la prepaga del Grupo Sancor Seguros, viene implementando una campaña orientada a que todas las personas recuperadas de una infección por COVID-19 donen el plasma de su sangre, ya que este posee anticuerpos que podrían beneficiar a quienes están cursando la enfermedad.

Bajo el concepto "Plasmá tu solidaridad", la empresa impulsa esta acción con foco en la provincia de Santa Fe, transformándose así en la primera prepaga del mercado argentino en promover una campaña de estas características.

En tal sentido, en la ciudad de Santa Fe y junto al CUDAIO (Centro Único de Donación, Ablación e Implante de Órganos), llevará a cabo la segunda jornada de donación de plasma. La cita será este viernes 19 de abril de 8 a 13 horas en la oficina de Prevención Salud, ubicada en San Martin 2061.

Todas aquellas personas interesadas en participar deben escribir al e-mail sumachancesdevida@prevencionsalud.com.ar o bien por WhatsApp, al 3493-520398. Para poder efectuar la donación hay que reunir los siguientes requisitos:

\- Tener entre 18 y 65 años de edad.

\- Estar clínicamente recuperado/a de la infección.

\- Contar con el hisopado nasal negativo o alta epidemiológica.

\- Realizar la donación a partir de los 14 días de la recuperación.

"Como empresa comprometida con la salud y el bienestar de las personas, y considerando el momento que estamos viviendo como país a raíz de la pandemia, consideramos de vital importancia que quien tenga la posibilidad de hacerlo, se sume a esta iniciativa, en un gesto solidario de inmenso valor", manifestó Carlos Hoffmann, Gerente General de Prevención Salud.

Los servicios de Prevención Salud son comercializados a través de la red de Productores Asesores de Seguros de Sancor Seguros de todo el país.

Para el año en curso, la prepaga se propone escalar en cantidad de afiliados, poner a su disposición más puntos de cercanía y sumar nuevos servicios digitales, en línea con el contexto actual. Más info: [www.prevencionsalud.com.ar](http://www.prevencionsalud.com.ar)

**Comienza este martes la campaña de vacunación contra la gripe en Santa Fe**

La primera semana se aplicará la vacuna a trabajadores de la salud y la siguiente al resto de la población destinataria, establecida a nivel nacional.

El Ministerio de Salud provincial iniciará este martes 13 de abril la campaña de vacunación contra la gripe o Influenza que anualmente, y según establece el calendario nacional, obligatorio y gratuito, se realiza a partir de la llegada del otoño.

Como sucede en todo el país y conforme establece el Ministerio de Salud de la Nación, se comenzará a vacunar a los trabajadores y trabajadoras de la salud en los propios hospitales, Centros de Atención Primaria de la Salud (CAPS) o SAMCO en donde trabajan.

Por otra parte, al personal de clínicas y sanatorios privados, a quienes el Estado provincial proveerá las dosis necesarias de vacunas antigripales para su inoculación.

**Quiénes deben vacunarse**

Para el resto de la población objetivo (destinataria) que establece la cartera sanitaria nacional en el calendario de inmunizaciones, la misma comienza el 19 de abril, una semana después.

“Las personas deberán acercarse al hospital, SAMCO, o CAPS más cercano a su domicilio, siempre y cuando el efector no esté dedicado a la vacunación contra el Coronavirus”, explicó Soledad Guerrero, responsable provincial de Inmunizaciones.

Posteriormente, Guerrero explicó que “la vacunación antigripal tiene por objetivo reducir las complicaciones, hospitalizaciones, muertes y secuelas ocasionadas por la infección por el virus influenza (A y B) en la población de riesgo”.

Como sucede en todo el país, la vacuna antigripal está disponible en forma gratuita para el personal de salud; mayores de 65 años; personas gestantes en cualquier momento del embarazo y puérperas; niñas y niños de 6 a 24 meses; y personas de 2 a 64 años con condiciones de riesgo como: obesidad, diabetes, enfermedades respiratorias, enfermedades cardíacas, inmunodeficiencias congénitas o adquiridas, pacientes oncohematológicos, trasplantados o personas con insuficiencia renal crónica en diálisis.

“Dentro de este grupo –precisó Soledad Guerrero– están las personas inmunodeprimidas, por ejemplo, quienes tienen VIH, y sus convivientes. Las personas con asma, fibrosis quística; esplenectomizados (a quienes se les extirpó el vaso sanguíneo); y a menores de 18 años que consuman crónicamente ácido acetilsalicílico”.

Y enfatizó: “Las personas de 2 a 64 años con factores de riesgo o comorbilidades solo podrán vacunarse con prescripción médica que indique cuál es su patología”.

Asimismo, recordó que “dentro de la población objetivo se encuentra el personal de Seguridad (Policía, Bomberos, Gendarmería, entre otros)”.

La vacunación contra la gripe se desarrollará en forma simultánea al plan de aplicación de las vacunas contra la Covid-19 y que debe haber un intervalo mínimo de 14 días en la aplicación de ambas vacunas.