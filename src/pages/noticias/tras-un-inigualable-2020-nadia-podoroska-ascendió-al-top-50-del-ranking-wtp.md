---
layout: Noticia con imagen
author: Por Clara Blanco
resumen: Podoroska en el top 50 de WTP
category: Deportes
title: Tras un inigualable 2020, Nadia Podoroska ascendió al top 50 del ranking WTP
entradilla: La tenista rosarina cerró el año en el Abierto de Linz y escaló al
  puesto 47 del ranking WTP. Había comenzado el año en la posición 255.
date: 2020-11-17T17:24:30.256Z
thumbnail: https://assets.3dnoticias.com.ar/tenis.jpg
---
La argentina Nadia Podoroska cerró su año de competencia entre las ocho mejores tenistas del Abierto de Linz (Australia). Con ese torneo logró llegar hasta la ubicación 47 del ranking WTA, el mejor puesto que obtuvo desde el inicio de su carrera profesional. La rosarina había iniciado el 2020 en la posición 255.

Podoroska experimentó un gran ascenso en su carrera tras haber llegado a semifinales de Roland Garros, una de las competencias más prestigiosas del tenis mundial. Cabe resaltar que era la primera vez que la argentina jugaba dicho torneo.

Con su temporada 2020 cerrada, la tenista se tomará dos semanas de vacaciones para luego iniciar una pretemporada de cara a los desafíos de 2021. El próximo año tendrá como primer objetivo el Abierto de Australia, mientras que el mayor torneo serán los Juegos Olímpicos de Tokio. “De ahora en adelante mucho tiempo no voy a tener. Serán estos días de descanso y después encararé la pretemporada para ir a Australia. Con todo el trabajo que hago desde hace tiempo creo que voy a poder sobrellevar todas las expectativas”, aseguró.