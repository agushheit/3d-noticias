---
category: Agenda Ciudadana
date: 2021-09-29T06:15:52-03:00
thumbnail: https://assets.3dnoticias.com.ar/BILLERETA.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Confirmaron qué pasará con la Billetera Santa Fe en 2022
title: Confirmaron qué pasará con la Billetera Santa Fe en 2022
entradilla: Hoy el programa tiene un millón cincuenta mil usuarios y más de 24 mil
  comercios adheridos.

---
El gobierno provincial confirmó que el exitoso programa de reintegros Billetera Santa Fe seguirá vigente durante en 2022 y adelantó que las partidas de fondos para el plan fueron incluidas en el proyecto de presupuesto oficial que será presentado el jueves próximo en la Legislatura provincial.

Así lo confirmó el secretario de Comercio Interior de Santa Fe, Juan Marcos Aviano, quien en ese sentido subrayó: “La Billetera Santa Fe seguirá el año próximo y la premisa que tenemos es que se sostenga durante toda la gestión del gobernador Omar Perotti. Independientemente de lo que trimestralmente se vaya modificando en cuanto a los términos y condiciones que el comercio y el consumidor tiene para operar con este programa, Billetera estará durante todo este gobierno”.

El ministro de Economía provincial, Walter Agosto, se reunirá este lunes con legisladores y entidades empresariales para presentar las medidas tributarias que incluirá el proyecto de presupuesto para Santa Fe. La iniciativa llegará el jueves próximo a la Legislatura provincial —la Casa Gris no solicitará prórroga—, dentro del plazo previsto por la Constitución santafesina par el 30 de septiembre. Dentro de las novedades de la llamada “ley de leyes” santafesina figura la inclusión de los fondos destinados al financiamiento de Billetera Santa Fe, al menos durante todo el año venidero.

En declaraciones a la prensa, Aviano afirmó que en el presupuesto 2022 “estará garantizado todo lo que se tenga que garantizar para sostener el programa, porque el sector comercial necesita el acompañamiento que brinda este plan de incentivo para las ventas. Además, es una ayuda para las familias santafesinas para adquiera mayor poder de compra para lo que es el consumo diario y también para productos que no son esenciales, pero que son rubros que fueron castigados por la crisis económica de 2018/2919 y que se profundizó luego con la pandemia de coronavirus”.

Al ser consultado sobre si había una estimación de partidas de fondos necesarios para sostener Billetera Santa Fe, el titular de Comercio no se atrevió a precisar montos “porque se van haciendo estimaciones de acuerdo a como vaya la economía. Este año se tenían previstos 3.500 millones de pesos. Se estimaba que podía requerir ese dinero con diez mil comercios adheridos y 550 mil usuarios hacia fin de año”.

“Estamos en más de un millón cincuenta mil usuarios y más de 24 mil comercios adheridos. Por eso, a veces las estimaciones que pueden hacerse hay que presupuestarias, pero luego cuando está la decisión de seguir adelante se hacen las modificaciones que correspondan”, agregó.

Aviano indicó que para el último trimestre de este año “no está previsto que se incremente el porcentaje de reintegros. Para el año próximo, el gobernador nos pidió reuniones periódicas con comerciantes y entidades que nuclean a distintos rubros que están viendo de forma muy positivo el desarrollo de Billetera Santa Fe”.