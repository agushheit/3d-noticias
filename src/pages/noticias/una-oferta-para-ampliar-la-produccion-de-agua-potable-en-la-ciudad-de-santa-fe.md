---
category: La Ciudad
date: 2021-12-30T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/AGUAS.jpeg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Una oferta para ampliar la producción de agua potable en la ciudad de Santa
  Fe
title: Una oferta para ampliar la producción de agua potable en la ciudad de Santa
  Fe
entradilla: Se construirá una nueva estación de bombeo. La obra se enmarca en el plan
  director que elaboró la empresa con un horizonte de crecimiento de la población
  hasta el 2050. Tiene un presupuesto oficial de 284 mil pesos. 

---
El Gobierno de Santa Fe, a través de Aguas Santafesinas, licitó la construcción de una nueva estación de bombeo de agua potable Sur para la ciudad de Santa Fe. La obra se enmarca en el plan director que elaboró la empresa para ampliar la capacidad de producción de agua potable con un horizonte de crecimiento de la población de la ciudad hasta el 2050.

 Se presentó una sola oferta, la que realizó la empresa Cocyar SA., por un monto de 229.749.111 pesos (más IVA). El presupuesto oficial es de 284 millones de pesos y tendrá un plazo de ejecución de 18 meses, que serán aportados por el ENOHSA –Ente Nacional de Obras Hídricas y Saneamiento-.

 Las obras consisten en la construcción de una estructura de doble altura para albergar las nuevas potentes bombas que se incorporarán, cuatro en total, tres operativas más una de backup.

 Estará conectada a la reserva de agua tratada existente en la actual planta y dispondrá de un nuevo el colector de bombeo hacia las cañerías primarias de salida de planta, el puesto de control, puente grúa para movimiento interno de las grandes bombas, sistemas de alimentación y transformador de energía de media tensión y demás instalaciones de apoyo y control.

 Esta instalación estará destinada a alimentar las redes de distribución de la zona centro-Sur de la ciudad, independizando la actual capacidad de bombeo existente que pasará a estar exclusivamente destinada a la zona Norte incrementado la oferta de servicio a ese sector que es hacia donde se expandió la ciudad y así mejorar la prestación del servicio.

 Con las obras licitadas y a licitarse para ampliar la capacidad de producción de agua potable, también se prevé la incorporación de tres nuevos grupos electrógenos de gran porte para poder mantener el servicio en casos de fallas en el suministro eléctrico: un nuevo grupo para la nueva estación de bombeo Sur, otro a instalarse en la nueva Toma de captación de agua cruda y uno más destinado a las unidades de producción.

 Estos grupos generadores se sumarán a los cuatro existentes en etapas críticas del proceso como son rebombeo La Guardia, las tomas de Colastiné y toma Hernández y el existente en la impulsión de planta.

 Finalmente, dentro de este conjunto de inversiones destinadas a minimizar interrupciones en nuestro servicio de agua por problemas de suministro eléctrico. Está prevista la ejecución de una obra que permitirá confiabilizar la alimentación de energía para la Planta y la nueva Toma, sumando una nueva línea de energía de red desde la estación transformadora de EPE "Santa Fe Centro". Esta segunda alimentación en media tensión permitirá que estos puntos críticos tengan tanto alimentación primaria desde "Transformadora Calchines" como de "Santa Fe Centro", aportándole al sistema una redundancia de alimentación y una robustez inédita en la continuidad del servicio. 