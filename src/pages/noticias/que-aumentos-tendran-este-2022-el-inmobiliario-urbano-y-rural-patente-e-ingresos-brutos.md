---
category: Agenda Ciudadana
date: 2022-01-03T06:15:00-03:00
thumbnail: https://assets.3dnoticias.com.ar/aumentos.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: UNO Santa Fe
resumen: Qué aumentos tendrán este 2022 el Inmobiliario Urbano y Rural, Patente e
  Ingresos Brutos
title: Qué aumentos tendrán este 2022 el Inmobiliario Urbano y Rural, Patente e Ingresos
  Brutos
entradilla: 'Cómo quedó la Ley Tributaria 2022 aprobada en la Legislatura santafesina
  esta semana. El detalle punto por punto

'

---
Se aprobó la Ley Tributaria 2022 en Santa Fe. La Cámara de Diputados introdujo una modificación que no contó con la la conformidad del Poder Ejecutivo, habrá que esperar hasta la promulgación de la ley para conocer los lineamientos definitivos. No obstante, ya se pueden comentar las principales disposiciones.

**Aumentos en el Impuesto Inmobiliario Rural:**

_• Partidas con rangos 1 y 2: 10%_

_• Partidas con rangos 3 a 9: 25%_

_• Partidas con rangos 10 a 11: 35%_

Los propietarios de hasta 50 hectáreas y que las exploten personalmente pagarán el mismo monto del año 2019 (se evitan los aumentos de 2020, 2021 y 2022).

**Aumentos en el Impuesto Inmobiliario Urbano:**

_• Partidas con rangos 1 a 3: 10%_

_• Partidas con rangos 4 a 6: 25%_

_• Partidas con rangos 7 y 8: 35%_

Los contribuyentes de impuesto inmobiliario urbano y rural que hayan estado al día al finalizar los años 2020 y 2021 tendrán un descuento equivalente a la última cuota del año 2022. Este beneficio se aplica de forma automática en la emisión del impuesto sin necesidad de solicitud del contribuyente. Además habrá un descuento por pago anticipado total anual del 35%, y otro del 25% por pagar las cuotas mediante adhesión al débito automático.

Se estableció un cambio en la coparticipación de la recaudación del impuesto durante 2022, y la recaudación se distribuirá del siguiente modo:

• 40% para el Tesoro Provincial

• 60% para los municipios y comunas

La distribución entre municipios y comunas (distribución secundaria) se realizará utilizando los mismos parámetros que se vienen aplicando (coeficientes surgidos de una fórmula polinómica que pondera en un 80% la participación de cada localidad en la emisión del impuesto y en un 20% la participación de cada localidad en la población).

**Patentes de vehículos**

El impuesto resultará de aplicar las alícuotas que prevé la Ley Impositiva Anual vigente sobre los avalúos de los vehículos, pero en ningún caso **el monto del impuesto correspondiente al año 2022 podrá superar en más de un 30% al monto del impuesto correspondiente al año 2021 (tope de aumento del 30%). No obstante, dicho tope no rige para los vehículos modelo 2022.** También habrá un descuento por pago anticipado total anual del 35% y otro del 25% por pagar las cuotas por débito automático.

**Impuesto sobre los Ingresos Brutos**

• Se actualizaron en un 40% los montos de ingresos brutos máximos anuales para determinar las industrias que quedan exentas.

• Se actualizaron los montos totales anuales que diferencian las alícuotas aplicables a las entidades financieras.

• Se incrementaron en un 40% los montos de impuesto mínimo.

• Para el Régimen Tributario Simplificado para los Pequeños Contribuyentes se incrementaron en un 40%: a) El monto de ingresos brutos anuales para poder ingresar o permanecer en el régimen; b) Los montos de ingresos brutos anuales de cada categoría; c) Los montos del impuesto mensual de cada categoría.

• Se prorroga hasta el 31/12/2023 el beneficio de estabilidad fiscal para Pymes santafesinas.

• Se prorroga hasta el 31/12/2022 el beneficio de reducción de alícuota a industrias santafesinas que no son Pymes.

**Moratoria**

Se establece un nuevo Régimen de Regularización Tributaria (el tercero en tres años consecutivos). El mismo comprende a las deudas por todos los impuestos provinciales (incluyendo Patente Única sobre Vehículos) devengadas hasta el 30/09/2021. La vigencia de dicho régimen será de 90 días (contados a partir del primer día hábil del mes siguiente a que API y el SCIT la reglamenten) y podrá ser prorrogado por dichos organismos por otros 30 días.

**Condiciones financieras aplicables:**

• Interés de actualización de la deuda: Al monto del impuesto a valores históricos se le sumará un interés calculado al 2% mensual simple.

• Forma de pago: contado tiene un descuento de intereses de actualización de la deuda del 80% sin tasa de interés de convenio; 12 cuotas tiene descuento de intereses del 30% y 1% de tasa de interés sobre saldos; 24 cuotas tiene 15% y 1,5% respectivamente y 36 cuotas 0% de descuento e interés del 2% mensual sobre saldos.

Por otro lado, se prorrogan los beneficios de “Alivio Fiscal” hasta el 31 de marzo de 2022 a actividades afectadas por la pandemia.