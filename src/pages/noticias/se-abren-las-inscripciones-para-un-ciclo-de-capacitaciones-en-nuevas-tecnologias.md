---
category: La Ciudad
date: 2021-03-23T07:20:17-03:00
thumbnail: https://assets.3dnoticias.com.ar/tecnologia.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Municipalidad de Santa Fe
resumen: Se abren las inscripciones para un ciclo de capacitaciones en nuevas tecnologías
title: Se abren las inscripciones para un ciclo de capacitaciones en nuevas tecnologías
entradilla: UNREAL Engine es una propuesta de formación gratuita sobre la utilización
  de nuevas tecnologías de creación 3D en tiempo real. Los interesados en participar
  tienen tiempo de inscribirse hasta el 11 de abril.

---
Se encuentran abiertas las inscripciones para el ciclo de capacitaciones en nuevas tecnologías “UNREAL Engine”, que impulsa la Municipalidad de Santa Fe en articulación con la Incubadora y Aceleradora The Rabbit Hole y el Cluster TIC de Santa Fe. El mismo se desarrollará del 15 de abril al 23 de agosto y está destinado a estudiantes y/o profesionales de disciplinas afines.

Esta iniciativa surge al detectar, por un lado, un incremento en la tasa de desempleo en la ciudad de Santa Fe y, por el otro, a que una de las mayores necesidades observadas en el sector TIC de la ciudad y alrededores corresponde a la vacancia educativa que poseen las y los jóvenes aspirantes a puestos de trabajo en proyectos vinculados a nuevas tecnologías.

En este sentido, el secretario de Producción y Desarrollo Económico municipal, Matías Schmüth, detalló que “según los últimos resultados de la EPH, la tasa de desempleo en la ciudad de Santa Fe alcanzó el 20%, aproximadamente, a fines del segundo trimestre del año pasado, llegando a picos históricos, que tornan completamente necesaria la intervención de la Municipalidad en esta problemática, para poder dar respuestas a la misma”.

La formación resultante de este ciclo abrirá posibilidades de empleos directos en uno de los sectores más dinámicos y con mayor potencial de la región. Por este motivo es que “se abre esta posibilidad, que tiene por objetivo brindar herramientas que enriquezcan las capacidades de la ciudadanía, fundamentalmente jóvenes, para aspirar a ocupar puestos de trabajo disponibles en este tipo de empresas”, expresó el funcionario.

Con la premisa de generar una agenda de formación y capacitación, “y con miras a apoyar el crecimiento de un sector que está en pleno desarrollo y que necesita de mano de obra calificada, es que articulamos en conjunto con el Clúster TIC para poder aprovechar esta oportunidad”, agregó Schmüth.

Por su parte, Mariano Obeid, miembro del Cluster TIC de Santa Fe y de la incubadora The Rabbit Hole, sostuvo que “a nosotros nos parece muy importante este tipo de capacitaciones porque, más allá de que tenemos recursos humanos muy capacitados egresados de la UNL o de la UTN, siempre está faltando ese pequeño plus que les permite estar a la vanguardia siguiendo las tecnologías de punta”.

Además, UNREAL Engine “es el motor detrás de juegos como Fortnite o de la parte animada de series como The Mandalorian, de Disney, los cuales son productos de primera línea; y esto nos genera una inclusión tecnológica como empresas sumamente importante para poder progresar y crecer y ser incluídos dentro del mercado global”, finalizó Obeid.

Este ciclo facilitará a sus inscriptos un conjunto de herramientas de desarrollo con posibilidad de aplicación en diversos ámbitos tales como la arquitectura, diseño industrial, producción de videojuegos y software en general.

En ese sentido, la capacitación propuesta busca sensibilizar y entrenar a profesionales, estudiantes y desarrolladores sobre nuevas tecnologías de creación 3D en tiempo real, desde visualizaciones de diseño y experiencias cinematográficas hasta juegos de alta calidad en PC, consolas, dispositivos móviles, realidad virtual y realidad aumentada.

La iniciativa, que está destinada a estudiantes y/o profesionales de disciplinas afines, comenzará el jueves 15 de abril y se extenderá hasta el 23 de agosto de este año, desarrollándose una clase y una instancia de consulta por semana.

**Financiamiento**

El financiamiento para la capacitación lo otorga la Secretaría de Industria, Economía del Conocimiento y Gestión Comercial Externa perteneciente al Ministerio de Desarrollo Productivo de la Nación por medio del programa “Capacitación 4.0 y Economía del Conocimiento para municipios”.

Pablo Martín Navajas será el docente a cargo de los encuentros del ciclo, que sumarán un total de 32 horas cátedra. Es ingeniero en Sistemas con 12 años de experiencia en diferentes tecnologías, plataformas y lenguajes de programación.

Se encuentra especializado en diseñar e implementar todas las arquitecturas de software en juegos, desarrollar la mecánica del juego y herramientas personalizadas en unidad, gestionar y definir las necesidades de la tecnología y el diseño de juego.

Conocimientos previos en Programación en C++, Programación Orientada a Objetos, Programación Orientada a componentes, Modelado 3D o bien experiencia en la temática (no excluyentes).

Mientras que los requisitos adicionales serán contar con un equipo informático con las siguientes características: i5 6ta generación 2.8GHZ, 8 Ram, 40gb de espacio en disco, Placa de video Nvidia 750 o superior.

**Inscripción y consultas**

Los interesados pueden inscribirse hasta el 11 de abril a través del siguiente enlace.

Para consultas sobre el ciclo se puede enviar un correo electrónico a emprendedores@santafeciudad.gov.ar