---
category: Deportes
date: 2021-06-13T06:00:43-03:00
thumbnail: https://assets.3dnoticias.com.ar/ARG4.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Lucia Romero para CAB
resumen: Cami Suárez, la historia de la debutante que brilló en su primera vez
title: Cami Suárez, la historia de la debutante que brilló en su primera vez
entradilla: Paso a paso, el recorrido de la talentosa base de 20 años que se hizo
  cargo de la responsabilidad de reemplazar a Gretter y se destacó como lo hace día
  a día en la Liga Nacional. Claro, esta vez en la Americup.

---
“Estar en la Selección Mayor era uno de mis sueños, al principio era como que no caía que me habían convocado y ahora lo que no puedo creer lo que viví en este debut. Sabía que si estaba Meli (Gretter) no iba a jugar tanto, pero con lo que me pasó me concentré en rendir. Me sentí preparada y lo único que tenía claro era que me iba a ir vacía de la cancha”.

Con tan sólo 20 años, a Camila Suárez le tocó vivir algo que a pocos y pocas les pasa. Debutar en una Selección nacional, a nivel internacional, como titular y reemplazando a la capitana y estrella del equipo. Pero, si encima, a ese debut se le suma que la rompes, todo parece de una película de Hollywood. Y sí, algo así le paso a esta base entrerriana, que sumó 13 puntos, cinco asistencias, cuatro rebotes y tres robos en sus primeros 33 minutos en la Mayor.

Cami llegó a la preselección luego de una gran temporada en Obras Basket y a un recorrido importante para su edad: ya ha atravesado cuatro temporadas de Liga Nacional, un Mundial U19 y unos Juegos Olímpicos de Playa 3x3, entre otros torneos que le aportaron una vasta experiencia pese a sus cortitos 20 añitos. Pero, claro, empecemos por el inicio. “Comencé a jugar al básquet a los 7 años, en el Club Almafuerte de Concepción del Uruguay. Mi mamá trabajaba cerca, una tarde decidió llevarme a entrenar y ver si me gustaba. Desde ese día ya me decidí a practicarlo porque me gustó mucho”, recordó en charla con Prensa CAB.

Desde muy joven tuvo la posibilidad de medirse con las mejores del país, tras jugar durante toda su infancia en Almafuerte, un club que le mostró por primera vez el deporte y le permitió enamorarse de la naranja. En el 2015, a sus 15, vistió por primera vez la camiseta de Tomás de Rocamora. Fue en la segunda edición del Torneo Federal Femenino, certamen en el que las Rojas lograron pasar la primera fase pero finalizaron su participación en la segunda, cerrando el campeonato con un récord de 2-4. Luego de este primer desempeño nacional continuó en el equipo. “Jugué en 2016 la SuperLiga Femenina, mientras jugaba los torneos locales para mi club Natal. En el 2017 seguí la carrera en Rocamora jugando para mi categoría y la Liga Nacional de primera hasta el 2019”, contó.

Entre Ríos es su casa y como jugadora tuvo la oportunidad de vestir la camiseta de la provincia en varias ocasiones. Sus torneos más destacados, en los que gritó campeón, fueron el Argentino de Selecciones en 2014 y los Juegos de la Región de 2015.

**Cuatro temporadas de Liga Nacional**

Sobre sus primeros pasos en la Liga Nacional, Suárez rememoró su debut: “Fue vs Lanús, tenía muchos nervios y ansiedad, pero tenía compañeras de mucha experiencia que trataron de que sacara afuera mis nervios. Traté de dar lo mejor de mí cuando estuve en cancha”. Su presentación fue el 6 de mayo del 2017 en la derrota de Rocamora ante las Granates por 68-52. Cabe mencionar que unos años después vestiría la camiseta del conjunto bonaerense en el Federal Femenino, instancia en la que supo coronarse campeona.

La jugadora nacida el 12 de julio del 2000 jugó un total de 49 partidos en cinco torneos disputados con las entrerrianas en la Liga Femenina a lo largo de tres años. En el 2019, su última campaña, promedió 17,1 puntos (33,8% de efectividad en triples), 4,6 rebotes, 2,6 asistencias y 1,7 robos en 33,6 minutos. “Mi experiencia fue buena, siempre me dieron la posibilidad de jugar y tener minutos en torneos de primera a tan corta edad y que de esta manera pueda ir desarrollándome como jugadora”, aseguró sobre su paso por el conjunto de Concepción del Uruguay.

Luego tuvo la oportunidad de cruzar el charco para ayudar a Bohemios de Uruguay a consagrarse en la liga local. Su gran paso por Rocamora y su rendimiento en el país vecino hizo que Obras Sanitarias se fijara en ella como refuerzo para la 2020. Suárez recuerda su decisión de dejar su ciudad natal para mudarse a ‘la gran ciudad’: “Las posibilidades se van dando de a poco, yo en eso soy muy paciente. A fines del 2019 se comunicaron conmigo y mi representante desde Obras para ver la posibilidad de sumarme. La verdad es que me encantó la propuesta porque es un gran club para poder seguir desarrollándome como jugadora y persona. El cambio lo sentí muy positivo, a pesar de estar lejos de casa me fui adaptando super bien y me han hecho sentir muy cómoda”. Pero la 2020 nunca llegó, el torneo debió cancelarse debido a la pandemia del Covid-19 y Camila tuvo que continuar su entrenamiento en casa esperando la oportunidad de volver a la actividad para debutar con su nueva camiseta.

Luego de un año tras aquella cancelación, y dos años después del cierre de la temporada 2019, la pelota volvió a picar el pasado 13 de febrero con el comienzo de la Liga Femenina 2021. Obras debutó con derrota ante Rocamora, el antiguo equipo de Camila. En aquel encuentro la base-escolta se destacó sumando 31 puntos, 10 rebotes y 4 asistencias, Su desempeño nunca bajó desde aquel cotejo inicial: tras dos meses de competencia cerró el torneo con un promedio de 18,5 puntos, 6,2 rebotes, 3,5 asistencias 2,1 robo 18,5 minutos y 17,7 de valoración por partido. “La verdad es que fue una temporada más que positiva, me sentí muy bien con el equipo, con la forma de jugar y la identidad que buscamos para nuestro juego. Sinceramente me siento muy cómoda, el equipo me dio muchísima confianza en esta temporada y eso lo valoro muchísimo”, aseguró Suárez al analizar su desempeño durante este año.

**El camino en las selecciones formativas**

“El primer llamado fue para un Plan de Altura y Talento, en 2014. Al año siguiente me llamaron para la preselección u15 que jugaban un Premundial, pero no quedé entre las 12. Eso fue muy triste para mí, pero entendí que quizá no era mi momento y que tenía muchísimas cosas por mejorar. Desde ese día empecé a trabajar más duro porque mi sueño era vestir esa camiseta”, confesó. El trabajo rindió sus frutos: además de ser una de las juveniles más destacadas de nuestra liga, finalmente tuvo la oportunidad de jugar con la Selección Argentina. Fue campeona en el Sudamericano U17, logró la clasificación en el FIBA Américas 2018 en México y disputó el Mundial de U19 de Tailandia 2019.

“Un recuerdo que tengo con la Selección es una gira por Francia en 2017, fue la preparación al Sudamericano, ahí nos quedamos en casa de familia, la gente de ahí nos alojaba y eran como nuestros "papás" durante el tiempo que estábamos en competencia, nos llevaban a los partidos, nos hacían la comida, todo como si estuviéramos en casa. Fue hermoso, teníamos hinchada, hacían banderas argentinas, y nos cantaban, eran súper fanáticos nuestros, me acuerdo que también se hicieron remeras, sentíamos tanto cariño por parte de ellos, fue increíble, te sentías como en casa”.

Además de jugar con las selecciones inferiores de nuestro país, tuvo la posibilidad de viajar a Qatar en el 2019, escenario en el que disputó los Juegos de Playa en la modalidad 3x3, otra disciplina a la que fue convocada para los entrenamientos de Selección que se realizarán en este 2021.

“La verdad que me siento muy bien, si bien es básquet, no es lo mismo que el 5x5, pero cuando me tocó jugarlo me adapté bastante rápido y me gustó mucho. La diferencia que noto es que el 3x3 es un juego mucho más rápido en todos los sentidos, es mucho más dinámico, intenso y potente. También tenés menos tiempo para resolver en ataque”, analizó sobre este desafío.

Ante la pregunta de qué siente al vestir la celeste y blanca Camila aseguró: “Demasiadas emociones encontradas, es muy difícil poder expresar en palabras lo que siento cuando me pongo esa camiseta, es algo muy lindo. Cada vez que escucho el himno se me eriza la piel, algo único, lo mejor que me pudo pasar como jugadora. ¡Es un tremendo orgullo!”.

**Cami en la actualidad**

“El apoyo de mi familia siempre fue el mejor, sin dudas que sin su apoyo hoy no estaría en el lugar que estoy, ellos son mi sostén y fuerzas para seguir. Son los que están en cada caída y en cada paso que doy. Son un estandarte muy importante en mi vida”, aseguró Camila, quien hoy aún se encuentra lejos de casa ya que tiene la cabeza puesta en los siguientes compromisos con la Selección. Pero jugar al básquet no es su única actividad. “Estoy estudiando el profesorado de Educación Física. En mis tiempos libres me gusta tomar mates, leer algún que otro libro, escuchar música”, informó.

Aunque, claro, por lo pronto, jugar al básquet le despierta sus mayores pasiones. Como este debut soñado que tuvo en San Juan de Puerto Rico.