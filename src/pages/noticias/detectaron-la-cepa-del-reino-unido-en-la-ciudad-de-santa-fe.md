---
category: Agenda Ciudadana
date: 2021-04-03T06:15:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/martoranojpg.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: 'Detectaron la cepa del Reino Unido en la ciudad de Santa Fe '
title: 'Detectaron la cepa del Reino Unido en la ciudad de Santa Fe '
entradilla: Así lo confirmó la ministra de Salud, Sonia Martorano, quien aseguró que
  esa persona y todos sus contactos están absolutamente aislados.

---
La ministra de Salud, Sonia Martorano, anunció que se detectó en la ciudad de Santa Fe con PCR positivo, al que se le hace la secuenciación genómica en el Inta de Rafaela con participación del Conicet y "da una cepa compatible con la del Reino Unido". La funcionaria aclaró que "esta persona está en aislamiento junto a todos sus contactos y no tiene antecedente de viaje, aunque sí tiene un contacto con viajeros".

Además, indicó que en la ciudad de Rafaela un paciente dio positivo por PCR con una cepa compatible con la de Manaos. "Esta persona no tiene antecedente de viaje y está aislada tanto ella como todos sus contactos. Los dos casos están en investigación", dijo.

"Para quienes ya están vacunados, incluso el personal de salud que ya tiene las dos dosis completas, la aparición de las nuevas cepas implica seguir cuidándonos. Alguien que se haya vacunado puede contagiarse. Se cree que, como ocurre con otras vacunas, va a tener una enfermedad más leve. Pero lo que vamos a lograr con esta vacunación es disminuir la tasa de mortalidad y la tasa de internación al lograr que sea más leve la enfermedad", indicó Martorano.