---
category: Estado Real
date: 2021-01-30T09:24:17Z
thumbnail: https://assets.3dnoticias.com.ar/leche.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Gobierno de Santa Fe
resumen: Santa Fe y Córdoba establecieron una agenda de trabajo conjunta para la lechería
title: Santa Fe y Córdoba establecieron una agenda de trabajo conjunta para la lechería
entradilla: La reunión fue encabezada por el ministro Daniel Costamagna y su par de
  Agricultura de Córdoba, Sergio Busso. Dialogaron sobre exportaciones y medidas para
  generar una mayor competitividad en toda la cadena de valor.

---
El ministro de Producción, Ciencia y Tecnología, Daniel Costamagna, junto a su par de Agricultura de Córdoba, Sergio Busso, encabezaron ayer en la ciudad de San Francisco, Córdoba, un encuentro de los equipos técnicos de las áreas de lechería de ambas provincias. 

La reunión, que tuvo lugar en la Sociedad Rural de esa localidad, se llevó adelante con el objetivo de delinear una agenda de trabajo conjunta para el apoyo y desarrollo al sector, de cara al 2021.

Luego del encuentro, Costamagna destacó que “haremos foco en un fuerte estímulo a las exportaciones, pidiendo una reconsideración de los aranceles para la leche en polvo y los quesos, buscando generar una mayor competitividad en toda la cadena lechera".

“El encuentro fue el primero de un cronograma que reunirá periódicamente a los equipos técnicos de ambas provincias, con el objetivo de trabajar de manera conjunta”, informó el ministro.

Entre otros puntos que se trataron en la mesa, cada provincia se explayó sobre las políticas con respecto a viviendas y caminos rurales; financiamiento para el sector; mejora en la calidad mediante asesoramientos técnicos; fortalecimiento del valor agregado; y control de buenas prácticas, entre otros. 

Además, decidieron plantear en forma conjunta ante el Consejo Federal Lechero la necesidad de que la actividad sea considerada como economía regional para encarar mejoras a nivel estructural.

Costamagna estuvo acompañado por la subsecretaria de Coordinación Agroalimentaria, María Eugenia Carrizo, y el director de Producción Lechera y Avícola, Abel Zenklusen.