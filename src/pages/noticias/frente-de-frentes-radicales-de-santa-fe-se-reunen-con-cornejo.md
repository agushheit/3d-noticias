---
category: Agenda Ciudadana
date: 2021-02-04T03:58:47-03:00
thumbnail: https://assets.3dnoticias.com.ar/UCR-radicales.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Frente de frentes: radicales de Santa Fe se reúnen con Cornejo'
title: 'Frente de frentes: radicales de Santa Fe se reúnen con Cornejo'
entradilla: 'La comitiva de santafesinos estará integrada, entre otros, por Carlos
  Fascendini, Santiago Mascheroni, Hugo Marcucci, Maximiliano Pullaro y Lisandro Enrico. '

---
Representantes de casi todos los sectores internos del radicalismo santafesino se reunirán en las próximas horas con el titular del Comité nacional, Alfredo Cornejo. El encuentro, según pudo saber El Litoral, fue solicitado para definir posiciones en torno del escenario electoral que se avecina, todavía inmerso en una profunda incertidumbre a partir de la indecisión respecto de si habrá o no primarias para definir candidatos a nivel país.

A nivel local, los dirigentes santafesinos particularmente pretenden interiorizar a la autoridad partidaria nacional acerca de cómo evoluciona la estrategia provincial de constituir un armado político básicamente no peronista; el denominado "frente de frentes".

En rigor, fue el propio Cornejo uno de los pioneros o cuanto menos promotores iniciales de la idea; fue Cornejo quien arengó a los radicales de Santa Fe para que trabajaran en pos de la unidad y de la construcción de un espacio más amplio que permitiese, incluso, sumar al socialismo.

Pero las posiciones en el partido centenario no logran homogeneizarse del todo porque el armado de ese súper espacio implicaría una confluencia entre Juntos por el Cambio y el Frente Progresista. De allí que el objetivo de la reunión con Cornejo sea obtener, también, un aval formal y público del Comité a esa estrategia, además del acompañamiento que se haya podido experimentar hasta aquí.

La comitiva de santafesinos estará integrada, entre otros, por Carlos Fascendini, Santiago Mascheroni, Hugo Marcucci, Maximiliano Pullaro y Lisandro Enrico. No será de la partida el exintendente de la ciudad, José Corral.

## **Por un aval**

"Queremos el acompañamiento de la conducción nacional en este proceso – dijo a El Litoral el diputado Julián Galdeano-. La aspiración es tener sintonía plena con el Comité. Los sectores que integramos la Comisión de Acción Política estamos convencidos de que esta es la estrategia (la del "frente de frentes") que el radicalismo debe impulsar. No podemos quedarnos en cuestiones pequeñas y sectarias; tenemos que construir una alternativa seria y no estar pensando en quién tiene que ser excluido. Quienes piensan en quienes sí y quiénes no, no tienen mucho para hacer en este espacio en construcción", dijo el legislador, en alusión tácita al sector de Corral.

A propósito de ese armado, Galdeano relativizó el "no" mediático de Miguel Lifschitz respecto de la integración del socialismo al mencionado frente. "Da la sensación de que hasta que no se dirima la situación interna que atraviesa el PS, todo lo que se diga se circunscribe a un mensaje hacia la militancia. Pero al margen de eso, esperamos que esa integración pueda producirse", planteó. Galdeano consideró que "por ahora, hay tiempo", pero advirtió que "en algún momento se tendrán que definir". Y sostuvo que "es muy difícil construir una coalición en Santa Fe solo en 2023 (cuando se renuevan autoridades provinciales), si no tenemos en cuenta los comicios de 2021 y las elecciones nacionales. Estamos frente a una polarización muy fuerte", alertó.

## **Esquema electoral**

El radicalismo será el primer partido invitado por la gestión de Omar Perotti a la ronda de conversaciones que iniciarán los ministros de Gobierno y Gestión para discutir – entre otros – los temas electorales que se avecinan. Frente a ello, la mesa provincial de la UCR provincial fue convocada para este jueves, a fin de definir qué posiciones se llevan a ese encuentro.

Al respecto, Galdeano adelantó que son partidarios de que las elecciones intermedias provinciales sean simultáneas a los comicios nacionales. En tanto, dijo que defienden la realización de las PASO. "Creemos que las primarias se tienen que hacer. Hoy no llegamos a diseñar un esquema de selección de candidatos que pueda reemplazar a las PASO. Por lo tanto, si no tenemos mecanismo que las reemplace, las primarias se tienen que hacer. Y creemos que eso es posible tomando todos los recaudos necesarios. Quizá sí es necesario _aggiornar_ los cronogramas electorales, para que los plazos coincidan, tanto en la instancia nacional como provincial", expresó. Asimismo, se mostró partidario de sancionar una ley que permita exceptuar de las primarias a aquellas localidades en los que haya listas únicas.