---
category: La Ciudad
date: 2021-01-12T10:00:14Z
thumbnail: https://assets.3dnoticias.com.ar/12121-clases-santa-fe.webp
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: '3DNoticias - Fuente: UNO Santa Fe'
resumen: Sin fecha de vacunación, los docentes condicionan la vuelta a clases presenciales
title: Sin fecha de vacunación, los docentes condicionan la vuelta a clases presenciales
entradilla: Desde Amsafe La Capital condicionaron el regreso a clases presenciales
  a la situación sanitaria y edilicia. Aún sin fecha para el inicio de la campaña
  de vacunación, se mostraron a favor del regreso a las aulas.

---
En ese sentido, pidieron discutir todos los aspectos en el marco de las reuniones paritarias, encuentros que está previsto se retomen a principios de febrero.

Este lunes, la ministra de Educación Adriana Cantero, brindó precisiones de cómo será el ciclo lectivo 2021 en Santa Fe. Dijo que será a través de un sistema bimodal, una semana las clases serán de manera presencial y otra virtual. Comentó que ya fue presentado ante el Consejo Federal de Educación, que fue aprobado y cuando el ministro (de Educación de la Nación) Nicolás Trotta visite Santa Fe, se definirá el formato posible para arrancar el año recuperando presencialidad. Además, dio como fecha de regreso a las aulas el 17 de febrero, para chicos de séptimo grado y quinto y sexto de secundario.

«Es algo con lo que estamos absolutamente de acuerdo. Creemos que la presencialidad es importante, por una cuestión pedagógica, social y psicológica», destacó Rodrigo Alonso, secretario de Amsafé La Capital.

Igualmente, advirtió que «cualquier discusión sobre presencialidad tiene que estar planteada poniendo en primer lugar la situación sanitaria y edilicia. Está prevista una reunión del comité mixto de salud y seguridad en el trabajo. Uno de los ejes que nosotros planteamos para la vuelta a la presencialidad no solamente tiene que ver con las condiciones sanitarias, sino también edilicias y de trabajo».

«Estamos esperando la reunión paritaria para poder discutir todos los temas pendientes. No solamente el tema salarial, que es un tema que quedó abierto, sino todo lo que tiene que ver con las condiciones de trabajo y la presencialidad», enfatizó Alonso.

Consultado sobre la posibilidad de retornar a las aulas el 17 de febrero, como planteó la ministra Cantero, el referente gremial insistió: «la discusión no será con relación a la fecha. La gran objeción está en las condiciones sanitarias, edilicias. Esperaremos el proceso de vacunación porque es importantísimo que a todos los docentes se nos pueda aplicar la vacuna para volver a la presencialidad».

Definió como «desafortunada» la experiencia que realizó el gobierno de la provincia en diciembre, en relación con un reencuentro de alumnos. Y añadió: «fue un intento de vuelta a la presencialidad en una etapa de pandemia que estaba en su pico (en el departamento La Capital). Se intentó una revinculación que pedagógicamente era cuestionable. Estamos convencidos de que ponía en juego la salud de los docentes y estudiantes de toda la comunidad educativa».