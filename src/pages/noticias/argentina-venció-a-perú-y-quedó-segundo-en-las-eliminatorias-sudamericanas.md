---
layout: Noticia con imagen
author: Por Clara Blanco
resumen: Eliminatorias Sudamericanas
category: Deportes
title: Argentina venció a Perú y quedó segundo en las Eliminatorias Sudamericanas
entradilla: Con goles de Nicolás González y Lautaro Martínez, la Albiceleste
  superó a la selección de Gareca por 2 a 0. Con este triunfo, el equipo de
  Scaloni cierra el año segundo en la tabla con 10 puntos.
date: 2020-11-18T14:18:17.980Z
thumbnail: https://assets.3dnoticias.com.ar/argentina-peru1.jpg
---
Argentina llegó a Lima en medio de una crisis política y social para cerrar el 2020. En el estadio Nacional de Lima, la Albiceleste venció a Perú por 2 a 0 con goles de Nicolás González y Lautaro Martínez.

La selección del Tigre Gareca no fue una complicación para los dirigidos por Lionel Scaloni, quienes dominaron el partido con tranquilidad. El marcador lo abrió González a los 16 minutos y desde allí Argentina siguió creciendo. Mientras que Perú no incomodó en ningún momento a Franco Armani, aunque tuvo algunos minutos de juego colectivo.

A los 27 del primer tiempo, el que amplió la diferencia fue Lautaro Martínez. El ex Racing definió ajustado contra el primer paro del arquero peruano pero puso el 2 a 0 para Argentina.

Con este resultado Argentina queda como único escolta de Brasil, líder de la tabla de posiciones con 12 puntos, a solo dos unidades y tercero, con uno menos, está Ecuador, conducido por el DT argentino Gustavo Alfaro.