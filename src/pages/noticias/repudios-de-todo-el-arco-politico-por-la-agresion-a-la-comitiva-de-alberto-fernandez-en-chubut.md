---
category: Agenda Ciudadana
date: 2021-03-14T06:15:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/ataquess.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Repudios de todo el arco político por la agresión a la comitiva de Alberto
  Fernández en Chubut
title: Repudios de todo el arco político por la agresión a la comitiva de Alberto
  Fernández en Chubut
entradilla: Mauricio Macri, Horacio Rodríguez Larreta y otros dirigentes coincidieron
  en que “la violencia no es el camino y la manera de expresarse”, después del ataque
  a piedrazos contra el Presidente en Lago Puelo.

---
Mauricio Macri y Horacio Rodríguez Larreta repudiaron la agresión que sufrió Alberto Fernández en su visita a Chubut para recorrer las zonas arrasadas por los incendios en esa provincia y en Río Negro, que dejaron como saldo una persona fallecida, más de 200 viviendas quemadas y daños importantes en el suministro de distintos servicios.

Los referentes de Juntos por el Cambio se expresaron en contra de los hechos de violencia después de que manifestantes en la localidad de Lago Puelo atacaran a piedrazos la camioneta que transportaba al máximo mandatario. Coincidieron con que esa “no es la manera de expresarse” y subrayaron la voluntad del diálogo para resolver diferencias. “La violencia nunca es el camino. Repudio lo sucedido hoy en Chubut durante la visita del Presidente. Cualquier diferencia tiene que ser resuelta por la vía del diálogo”, escribió el ex jefe de Estado en su cuenta de Twitter.

Del mismo modo, miembros del gabinete de Fernández comenzaron a repudiar el ataque. Como el ministro de Economía, Martín Guzmán, quien tuiteó: “Nuestro país necesita de más diálogos, más construcción colectiva, respeto por el otro y menos violencia. Tenemos un Presidente que trabaja cada día por una Argentina unida, más tranquila y con más oportunidades. Somos millones quienes queremos ese camino y lo vemos posible”

También se sumó al repudio Agustín Rossi, ministro de Defensa, quien, desde su cuenta de Twitter, escribió: “Repudio absoluto a la agresión desatada contra el Pte. @alferdezy su comitiva. La violencia nunca es el camino”. El ministro de Educación, Nicolás Trotta, por su parte, tuiteó: “Repudio la violencia contra el Presidente y su comitiva en Chubut, quienes viajaron para garantizar la respuesta del estado nacional frente a los incendios. Incomprensible el recurrente comportamiento antidemocrático de aquellos que se oponen a la minería en la provincia”.

Así, la Presidenta del Consejo Nacional de Coordinación de Políticas Sociales, Victoria Tolosa Paz, también repudió la agresión contra Fernández: “El presidente @alferdez no sobrevuela zonas, sino que se presenta personalmente para poner ministerios y organismos nacionales al servicio de quienes padecen. La agresión a la comitiva presidencial en #Chubut, es absolutamente repudiable y fuera de todo contexto”.

También se sumaron al repudio gobernadores, como el santafesino Omar Perotti, quien manifestó a través de su perfil de Twitter "Aceptamos las diferencias, pero nunca la agresión personal. La democracia permite manifestarse, pero sin odio. Nos solidarizamos con @alferdez"