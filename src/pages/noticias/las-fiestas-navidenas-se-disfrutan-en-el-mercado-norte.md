---
category: La Ciudad
date: 2021-12-09T06:00:11-03:00
thumbnail: https://assets.3dnoticias.com.ar/MERCADONORTE.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Las fiestas navideñas se disfrutan en el Mercado Norte
title: Las fiestas navideñas se disfrutan en el Mercado Norte
entradilla: Además de las mejores opciones para las compras, habrá sorteos y sorpresas.
  Se preparan talleres creativos para niñas y niños y hasta la visita de Papá Noel.

---
El Mercado Norte se prepara para festejar las fiestas navideñas junto a clientes y vecinos. A partir de mañana, el amplio espacio ubicado en la intersección de Santiago del Estero y Urquiza estará decorado para la ocasión. Además, se sortearán 4 premios en órdenes de compras. En tanto, como ya es habitual, Papá Noel visitará el Mercado para que las niñas y niños tengan una foto de recuerdo y, de paso, les dejen sus cartas con los pedidos de obsequios y buenos deseos.

Los sorteos se realizarán el jueves 23 y se podrá participar de dos maneras: una de ellas es por [instagram](https://www.instagram.com/mercadonortesf/), donde se sortearán dos órdenes de compras, para cualquiera de los locales del Mercado, de 10.000 pesos cada una.

La otra opción de participar de los sorteos es llenar un cupón, que se entrega luego de realizar cualquier compra. Habrá dos urnas para colocar los cupones, una en el supermercado y la otra en la oficina de Atención al Cliente. En este caso, las órdenes de compras son de 10.000 y 15.000 pesos.

**Una selfie con Papá Noel**

En tanto, el viernes 17 llega al Mercadito Papá Noel. Entre las 18.30 y las 20.30, las niñas y niños podrán tener una imagen con él. En ese mismo horario, un grupo de profesores brindarán talleres para enseñar a realizar objetos navideños.

Además, podrán completar las cartas con los pedidos especiales. Cuando se acerque la hora de cierre, Papá Noel encabezará una caminata hasta la Plaza Constituyentes, donde se hará una suelta de globos multicolores.