---
category: La Ciudad
date: 2021-04-27T07:53:08-03:00
thumbnail: https://assets.3dnoticias.com.ar/carpa-negra.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Lt10
resumen: Reclaman que el 29 de abril sea el “Día de la Memoria del Pueblo Inundado”
title: Reclaman que el 29 de abril sea el “Día de la Memoria del Pueblo Inundado”
entradilla: 'A días de un nuevo aniversario de la mayor inundación de la historia
  de Santa Fe, militantes instalaron la tradicional carpa negra en la explanada de
  la Legislatura. '

---
A tres días de que se cumpla un nuevo aniversario de la inundación del año 2003, militantes instalaron la carpa negra que suele montarse en la Plaza 25 de Mayo, pero esta vez en la explanada de la Legislatura. 

El objetivo, explicó María Claudia Albornoz, es reclamar que el Senado apruebe el proyecto de ley que designa al 29 de abril como “Día de la Memoria del Pueblo Inundado”, que cuenta con media sanción de Diputados. 

“Es una ley que creemos necesaria. Es un reconocimiento a tanta lucha y creemos que es fundamental para el trabajo en las escuelas. Hay que entender dónde está emplezada la ciudad, qué pasa con las defensas”, explicó Albornoz.

Además, anticipó que desde hoy y hasta el mismo 29, habrá proyección de documentales y distintas actividades, “con todos los cuidados necesarios” por la pandemia de Covid-19, y siempre hasta las 21:00 como lo establece la actual reglamentación. 

Finalmente, la activista por los derechos de los inundados postuló: “el Día de la Memoria me parece fundamental para que aprendamos en la ciudad de Santa Fe cómo cuidarnos. Y también, reconocer la solidaridad que en esos días nos abrazó para que podamos seguir viviendo: nos abrigaron, nos dieron un plato de comida caliente. De todo eso no nos olvidamos y es parte de esta efemérides”. “Nosotros a la memoria la tenemos tatuada en la piel. El oeste se acuerda. Ahora queremos que quede instituida desde la Legislatura”, cerró.