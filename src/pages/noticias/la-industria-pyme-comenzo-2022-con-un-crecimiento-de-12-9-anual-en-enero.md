---
category: Estado Real
date: 2022-02-21T10:59:37-03:00
thumbnail: https://assets.3dnoticias.com.ar/touch-screen-g2b65ea830_1920.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Prensa Came
resumen: La industria pyme comenzó 2022 con un crecimiento de 12,9% anual en enero
title: La industria pyme comenzó 2022 con un crecimiento de 12,9% anual en enero
entradilla: "-La demanda de productos industriales se mantuvo firme, y si bien la
  situación del sector viene mejorando paulatinamente, la inflación y el ausentismo
  complicaron al empresario en el primer mes del año. "

---
\-La actividad de la industria manufacturera pyme aumentó 12,9% anual en enero, con respecto al mismo de mes del 2021, a precios constantes. Frente a diciembre tuvo una baja mensual de 6,7%, esta responde a la estacionalidad propia del mes para la mayor parte de las empresas del sector.

\-La demanda de productos industriales se mantuvo firme, y si bien la situación del sector viene mejorando paulatinamente, la inflación y el ausentismo complicaron al empresario en el primer mes del año.

\-El uso de la capacidad instalada se ubicó en 68,7%, 4 puntos por debajo de diciembre, pero 1,2 puntos arriba de enero de 2021.

\-El 62,8% de las pymes consultadas evaluó la situación actual de su empresa como buena o muy buena, igual que lo hicieron en noviembre y diciembre. Pero se redujo a 62,3% la cantidad de firmas de la muestra que trabajaron con rentabilidad positiva (de 69% en diciembre).

Según el Índice de Producción Industrial Pyme (IPIP) elaborado mensualmente por CAME, el resultado de la producción industrial pyme de enero 2022, fue:

\-**Frente a diciembre 2021**: cayó 6,7%, con 10 sectores de los 11 rubros relevados en baja. Solo subió el ramo de **Productos metálicos, maquinaria y equipo** (+1,9%)

\-**Frente a enero 2021**: creció 12,9%, con 9 sectores en alza y 2 en baja.

\-**Frente a enero de 2020**: creció 13,2%, con 10 rubros en alza y 1 en baja.

**Análisis de sectores**

\-**Papel, cartón, edición e impresión**. Continuó siendo el sector de mayor expansión interanual, al crecer 70,9% frente a enero 2021. En la comparación mensual la producción se redujo 19,1%, esperable por la estacionalidad de buena parte de la actividad. La situación en esta rama manufacturera fue dispar de acuerdo al nicho de negocios. Así, mientras las empresas de impresión y gráficas vieron disminuir notoriamente su actividad frente a diciembre, las vinculadas al rubro escolar proveedoras de mayoristas, trabajaron más en relación a ese mes. Pero en la comparación mensual, excepto por cuestiones puntuales, la mayor parte de las empresas del sector, crecieron. Los dos principales problemas del mes, lo tuvieron las que utilizan papel importado como insumo, y aquellas que presentaron personal con ausencias por Covid. La inestabilidad del dólar es también un limitante para aquellas que tienen previsto inversiones.

\-**Indumentaria y textil**. La fabricación subió 8,6% anual en enero, 2,4% bianual (vs enero 2020) y cayó 6,6% frente a diciembre. Si bien enero no es el mejor mes para ese sector, las expectativas generadas por los pedidos de producción recibidos, son buenas para febrero y marzo. El inicio del ciclo escolar comenzó desde fines de enero a traccionar a muchas industrias de ese rubro.

\-**Metálicos, maquinarias y equipos:** la producción mejoró 1,9% anual, 5,2% bianual y 2,5% mensual. Fue uno de los ramos más afectados por el ausentismo, especialmente por las dificultades para reemplazar funciones entre obreros. Las lluvias intensas durante la segunda quincena del mes frenaron obras y construcciones, impactando también en las empresas orientadas a esa actividad. A pesar de eso, el sector creció, aunque los empresarios consultados señalaron que sin esas dificultades, y los problemas para conseguir algunos insumos, la actividad hubiera sido muy superior.

\-**Maderas y Muebles**: la elaboración en enero ascendió un 40,8% anual y 35,5% bianual. En la comparación mensual bajó 13,1%, por las vacaciones y la menor demanda de los clientes en ese mes. Sin embargo, hay buenas expectativas para febrero y marzo por el inicio de las clases y el mayor movimiento que eso implica. El sector produjo con el 77% de su capacidad instalada.

\-**Material de Transporte.** La producción subió 21,3% anual en enero, 25,9% bianual y se redujo 0,2% mensual. Si bien los pedidos de fabricación se mantuvieron firmes, las empresas se vieron con dificultades para incrementar la elaboración por el personal de vacaciones o enfermo. Algunos productores consultados mencionaron además que se les acortaron los plazos de pago para la compra de insumos, lo que les impidió la compra por falta de liquidez y por lo tanto, la producción.