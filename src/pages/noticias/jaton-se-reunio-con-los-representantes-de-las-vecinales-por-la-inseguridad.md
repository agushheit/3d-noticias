---
category: La Ciudad
date: 2021-03-13T06:15:58-03:00
thumbnail: https://assets.3dnoticias.com.ar/jaton.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: 3D Noticias
resumen: Jatón se reunió con los representantes de las vecinales por la inseguridad
title: Jatón se reunió con los representantes de las vecinales por la inseguridad
entradilla: " La Municipalidad también detalló el Plan de Iluminación, que tiene como
  finalidad reforzar las zonas críticas."

---
El intendente Emilio Jatón se reunió este viernes, en la Estación Belgrano, con integrantes de 16 vecinales para analizar los problemas en materia de inseguridad en cada uno de los distritos de la ciudad.

Participaron representantes de las vecinales ubicadas en los distritos: La Costa: las vecinales Colastiné Sur y El Pozo; Centro: 7 Jefes y Mariano Comas; Oeste: Los Hornos y Unión Progreso y Libertad de Barranquitas; Suroeste: Centenario y Juan de Garay; Este: Guadalupe Noreste y Alvear; Noroeste: Yapeyú Oeste y Loyola Sur; Noreste: Altos del Valle y Las Delicias; y distrito Norte: Pompeya Oeste y Villa Las Flores.

El encuentro, contó con la presencia de funcionarios municipales de las secretarías de Integración y Economía Social, Mariano Granato; de Control y Convivencia Ciudadana, Virginia Coudannes; y de Obras y Espacio Público, Griselda Bertoni.

Al término del encuentro, que se extendió por más de dos horas, Granato destacó la posibilidad de conocer la opinión de los vecinalistas, ya que ellos no forman parte del Consejo de Seguridad que será convocado próximamente por el intendente: “El tema de la seguridad atravesó la reunión y si bien no es un tema municipal, Jatón ha decidido asumir esa agenda previamente a la convocatoria al Consejo de Seguridad, espacio del cual las vecinales no participan, así que una buena parte de la agenda tuvo que ver con poder incorporarlos al Consejo”.

**Representar a los vecinos**

“El intendente decidió asumir el reclamo de las vecinas y vecinos para trabajar un tema tan complejo como la inseguridad, que requiere que todos los niveles del Estado pongan lo mejor de sí. Trataremos de abordar un tema tan complejo representando la voluntad de los vecinos, sin especulaciones políticas y con la seriedad que requiere este”, indicó Granato y añadió que durante el encuentro se comunicó a cada uno de los representantes barriales “el plan de inversiones sobre luminaria pública, que será de 150 millones de pesos, volcado en mejorar la iluminación. Tenemos un plan de acción que nos va a permitir mejoras en todos los barrios”.

Por otra parte, el funcionario destacó: “Para nosotros lo más importante es la apertura permanente al diálogo con todos los vecinos para trazar una agenda común”.

**Encuentros mensuales**

Durante la reunión se acordó que se llevarán a cabo encuentros de forma mensual. En esa línea, José Cettour, presidente de la vecinal Centenario, indicó: “Lo que se planificó y nos parece bueno es que cada 30 días vamos a tener una reunión, el segundo viernes de cada mes nos vamos a encontrar y a manifestar lo que pensamos los vecinalistas. La próxima será el 9 de abril”.

Con respecto al encuentro con los funcionarios municipales, el vecinalista destacó que “fue una reunión metodológica y de trabajo para ver cómo seguimos construyendo la ciudad entre todos. Consideramos que tenemos prepotencia de trabajo, lo hacemos ad honorem y nos preocupamos por los vecinos; y creemos que ser escuchados en estas reuniones es muy importante”.

En consonancia, Cettour repasó que si bien cada barrio tiene sus particularidades “hay cosas que son comunes y uno de los temas fue el de iluminación. La intendencia planteó todo un plan que abarca a unos siete u ocho barrios que son los que más estaban necesitando. Pero como comienzo tenemos esperanza de que esto llegue a buen fin”.

**Reunión en provincia**

Por su parte, Graciela Pieroni, presidenta de la Vecinal Parque Garay, aclaró que: “Luego de esta reunión nos vamos a reunir con representantes del Ministerio de Seguridad provincial debido a la grave situación que está pasando la ciudad en cuanto a seguridad. Veremos qué resultados tenemos, pero la intención es darle una solución al problema en el cual estamos siendo azotados permanentemente”.

En tanto, consignó: “Esperemos que con la tarea de iluminación que va a hacer la Municipalidad, que es importante y que hace a la seguridad y con la respuesta del ministerio, veremos a dónde llegamos. Esperamos que sea positivo y por el bien del vecino porque así de esta manera no se puede vivir más”.