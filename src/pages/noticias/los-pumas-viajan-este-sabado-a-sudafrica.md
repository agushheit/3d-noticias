---
category: Deportes
date: 2021-08-07T06:00:55-03:00
thumbnail: https://assets.3dnoticias.com.ar/pumas.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: Los Pumas viajan este sábado a Sudáfrica
title: Los Pumas viajan este sábado a Sudáfrica
entradilla: Los dirigidos por Ledesma abrirán la competencia ante los Springboks el
  sábado 14 de agosto desde las 12, hora de nuestro país.

---
El seleccionado argentino de rugby, Los Pumas, viaja este sábado rumbo a Sudáfrica para disputar el Rugby Championship, que se llevará a cabo entre el 14 de agosto y el 2 de octubre próximos y que aglutina a las potencias del hemisferio Sur.

Los dirigidos por Mario Ledesma, se medirán con Sudáfrica, vigente campeón del mundo, los próximos sábados 14 y 21 de agosto, en el inicio del torneo.

Luego de los choques frente a los Springboks sudafricanos, Los Pumas se medirán frente a los All Blacks el 11 de septiembre, en un encuentro válido a la tercera fecha que se llevará a cabo en la ciudad de Auckland; mientras que una semana más tarde disputarán la revancha en Wellington.

Finalmente, el representativo argentino afrontará los dos encuentros ante Australia: el 25 de septiembre en el Jones Stadium de la ciudad de Newcastle y el 2 de octubre en el GIO Stadium de Camberra.

Los Pumas en la gira internacional por Europa correspondiente a la ventana internacional de julio finalizaron invictos. Su primera presentación derrotó en la ciudad de Bucarest a Rumana por 24-17. Posteriormente igualaron frente a Gales, último campeón del torneo Seis Naciones, 20-20, y en la revancha ganaron 33-11, ambos encuentros disputados en la ciudad de Cardiff.

Actualmente, Los Pumas ocupan el séptima lugar del ranking oficial de la World Rugby.

Vale recordar que el plantel de Los Pumas está integrado por: Matías Alemanno, Gonzalo Bertranou, Emiliano Boffelli, Facundo Bosch, Rodrigo Bruni, Sebastián Cancelliere, Mateo Carreras, Santiago Carreras, Santiago Chocobares, Lucio Cinti, Santiago Cordero, Jerónimo De la Fuente, Bautista Delguy, Joaquín Díaz Bonilla, Felipe Ezcurra, Rodrigo Fernández Criado, Tomás

Gallo, Gonzalo García, Facundo Gigena, Francisco Gómez Kodela, Juan Martín González, Francisco Gorrissen, Facundo Isa, Marcos Kremer, Tomás Lavanini, Tomás Lezana, Juan Cruz Mallía, Santiago Mare, Rodrigo Martínez Manzano, Pablo Matera, Santiago Medrano, Ignacio Mendy, Domingo Miotti, Julián Montoya (capitán), Marcos Moneta, Matías Moroni, Carlos Muzzio, Matías Orlando, Joaquín Oviedo, Guido Petti, Enrique Pieretto, Ignacio Ruiz, Nicolás Sánchez, Joel Sclavi, Lautaro Simes, Santiago Socino, Nahuel Tetaz Chaparro, Nahuel y Juan Pablo Zeiss.