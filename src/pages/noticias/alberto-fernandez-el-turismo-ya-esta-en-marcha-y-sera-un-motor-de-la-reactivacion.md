---
category: Agenda Ciudadana
date: 2021-10-11T06:00:26-03:00
thumbnail: https://assets.3dnoticias.com.ar/alberto.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: Telam
resumen: 'Alberto Fernández: "El turismo ya está en marcha y será un motor de la reactivación"'
title: 'Alberto Fernández: "El turismo ya está en marcha y será un motor de la reactivación"'
entradilla: El Presidente expresó en su cuenta de Twitter su alegría respecto de las
  cifras que arrojó el fin de semana largo, con 4.250.000 de personas movilizadas
  en todo el país y un exitoso Programa Previaje.

---
El presidente Alberto Fernández afirmó este domingo que "el turismo ya está en marcha" y será "uno de los motores de la reactivación", al destacar el movimiento de personas en todo el país por el extenso fin de semana reforzado por los feriados del pasado viernes y este lunes.  
  
"Nos pone muy felices el movimiento turístico en todo el país, que mostró cifras superiores a la prepandemia y plena ocupación en los principales destinos", dijo el Presidente en su cuenta de Twitter, en un hilo de mensajes acompañado por una imagen con dos personas en un kayak y la leyenda "4.250.000 argentinas y argentinos se están movilizando por el país".  
  
Añadió que "el turismo es un gran generador de empleo e impulsor de las economías regionales. Por eso, será uno de los motores de la reactivación".  
  
Fernández resaltó que "detrás de cada turista hay un camarero, una cocinera, un transportista, una gerente, un proveedor y miles de personas cuya economía se moviliza" y enfatizó que "hicimos un esfuerzo enorme para que el turismo, uno de los sectores más afectados por la pandemia, siga de pie".  
  
En cuanto a las medidas para esa reactivación, destacó que "llevamos adelante #PreViaje, una inversión histórica del Estado Nacional, que ya fue utilizada por un millón de argentinas y argentinos en esta segunda edición".  
  
"Con el ministro (de Turismo y Deportes) Matias Lammens creemos que, a partir de los datos alentadores de este fin de semana largo, el retorno del turismo receptivo y el impulso de #PreViaje, vamos a disfrutar una temporada de verano histórica", añadió. Y enfatizó: "El turismo ya está en marcha".