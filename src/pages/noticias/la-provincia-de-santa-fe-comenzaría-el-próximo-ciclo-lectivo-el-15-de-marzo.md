---
layout: Noticia con imagen
author: "Fuente: DIario UNO"
resumen: Ciclo lectivo 2021
category: Estado Real
title: La provincia de Santa Fe comenzaría el próximo ciclo lectivo el 15 de marzo
entradilla: Ese fue el pedido que elevó el Ministerio de Educación de la
  provincia al Consejo Federal.
date: 2020-11-19T12:38:00.222Z
thumbnail: https://assets.3dnoticias.com.ar/escuelas.jpg
---
Las clases en Santa Fe comenzarían el 15 de marzo si prospera en el Consejo Federal el pedido realizado este miércoles por el Ministerio de Educación provincial. Si bien no trascendieron mayores detalles, en el pedido se incluyen los 180 días obligatorios de clase y la extensión del ciclo hasta el 20 de diciembre. También se ratificó que el año lectivo actual se prolongará durante febrero y hasta la primera quincena de marzo.

Hace unos días, la ministra Adriana Cantero había mantenido una reunión con sus pares de otras provincias y allí se determinó que cada provincia debía determinar la fecha de inicio de los ciclos lectivos del 2021.

## **Gobierno de la Provincia de Santa Fe**

Por otra parte, la provincia habría tomado la determinación que los actos de colación de este ciclo se posponen para febrero o marzo, situación que había generado muchos reclamos por parte de padres de alumnos de los últimos años de los ciclos primarios y secundarios. Incluso el gobernador Omar Perotti había deslizado la posibilidad de que pudieran realizarse con estrictos protocolos.