---
category: Agenda Ciudadana
date: 2021-07-17T06:00:24-03:00
thumbnail: https://assets.3dnoticias.com.ar/SACNUN.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: El Litoral
resumen: 'Sacnun: "El manejo coordinado de los recursos hídricos mejorará la calidad
  de vida de la gente"'
title: 'Sacnun: "El manejo coordinado de los recursos hídricos mejorará la calidad
  de vida de la gente"'
entradilla: La senadora nacional señaló que el convenio de creación del Comité Interjurisdiccional
  de la Región Hídrica de los Bajos Submeridionales “es una herramienta fundamental”.

---
La Senadora Nacional por la provincia de Santa Fe, María de los Angeles Sacnun, señaló que “el convenio de creación del Comité Interjurisdiccional de la Región Hídrica de los Bajos Submeridionales, es una herramienta fundamental para las provincias de Chaco, Santiago del Estero y mi provincia de Santa Fe, donde miles de hectáreas han quedado muchas veces segregadas, sin poder avanzar en los ámbitos de la producción, el desarrollo y el arraigo”

“Creemos que es sustancial que las tres provincias podamos continuar trabajando, aunque algunos anuncios ya han sido realizados por los tres Gobernadores en torno a los avances realizados en esta materia junto con los ministerios del Interior y de Obras Públicas de la Nación”.

En reunión de la comisión de Asuntos Constitucionales, celebrada este viernes y que preside Sacnun, se dio dictamen favorable a la aprobación del convenio suscripto entre Nación y esos Estados provinciales para formalizar las tareas conjuntas que se llevan adelante, en el marco del Comité Interjurisdiccional de la Región Hídrica de los Bajos Submeridionales

“Desde hace tiempo venimos pregonando que las problemáticas sean tratadas en clave interprovincial, los límites geográficos no deben significar de ninguna manera que abordemos nuestros problemas como si fueran compartimentos estancos. Debemos afrontar los problemas comunes y ésta es una oportunidad que nos permitirá recuperar para la producción y la generación de empleo, miles de hectáreas y salir de profundos problemas que se vienen arrastrando históricamente y que seguramente esta voluntad política expresada por el Gobierno Nacional y las tres provincias mejorará la calidad de vida de nuestros ciudadanos y ciudadanas”, finalizó la Senadora santafesina.

**CIRHBAS: objeto y fundamentación**

Tiene por objeto promover el manejo coordinado y racional de los recursos hídricos mediante planes, programas, proyectos y obras orientados a la resolución de la problemática de inundaciones, anegamientos y sequías que la afectan, entendida ésta como unidad de planificación, ordenamiento y gestión territorial.

La regionalización constituye un instrumento de las provincias para solucionar problemas comunes, maximizando las ventajas comparativas de cada uno de los entes locales que acuerdan crear en la región y en este sentido, la creación del Comité Interjurisdiccional de la Región Hídrica de los Bajos Submeridionales facilitará la búsqueda de consensos, la participación y la coordinación de las autoridades competentes en la materia entre las distintas jurisdicciones en un marco de cooperación, para desarrollar acciones que den solución a las problemáticas que afectan a la región y promover el manejo coordinado y racional de sus recursos hídricos.

El ámbito de aplicación del CIRHBAS comprende a la Región Hídrica de los Bajos Submeridionales, que abarca una amplia zona del sur chaqueño, del sureste santiagueño y del noroeste santafecino, de muy reducida pendiente, que es drenada principalmente por los ríos Tapenagá, Los Amores y Golondrina-Salado.