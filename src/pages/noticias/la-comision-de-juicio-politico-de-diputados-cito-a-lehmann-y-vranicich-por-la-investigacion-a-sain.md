---
category: Estado Real
date: 2021-04-08T06:13:40-03:00
thumbnail: https://assets.3dnoticias.com.ar/sain1.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: con información de Diario Uno
resumen: La comisión de Juicio Político de Diputados citó a Lehmann y Vranicich por
  la investigación a Sain
title: La comisión de Juicio Político de Diputados citó a Lehmann y Vranicich por
  la investigación a Sain
entradilla: También se pedirán copias de las licitaciones realizadas del Ministerio
  de Seguridad durante la gestión del exministro

---
Este miércoles se reunió la comisión de Juicio Político de la Cámara de Diputados de Santa Fe y tomó medidas –con el rechazo del PJ– para avanzar en la investigación contra el exministro de Seguridad, Marcelo Sain. Luego del encuentro que mantuvieron los miembros de esa comisión se decidió citar a la diputada nacional denunciante, Lucila Lehmann y a la auditora General del Ministerio Público de Acusación. También se pedirá información al Poder Ejecutivo sobre las licitaciones realizadas por el Ministerio de Seguridad durante la gestión del exministro. “Lo que está sucediendo es inédito y grave, en una situación donde hasta los propios fiscales están pidiendo no trabajar con el Organismo de Investigaciones”, dijo el legislador Joaquín Blanco.

La comisión de Juicio Político resolvió citar a la diputada nacional denunciante, Lucila Lehmann. “Será convocada para que pueda detallar las motivaciones de su denuncia y para que aporte nuevos elementos”, explicó el diputado Blanco. Por su parte, se hará lo mismo con la auditora general del Ministerio Público de la Acusación, María Cecilia Vranicich. “En uno de los escritos presentados por la Auditora, ella declara brindándole competencias a esta comisión. Queremos conocer su parecer y su forma de entender este proceso”, agregó el diputado.

También se solicitará al Poder Ejecutivo, a través de la Subsecretaría de Contrataciones y Licitaciones, y al Tribunal de Cuentas, todas las copias certificadas de las licitaciones y contrataciones que realizo el Ministerio de Seguridad durante la gestión del ex Ministro. “El fiscal Carlos Arietti nos manifiesta en un escrito que está en curso la investigación de la licitación, a todas luces amañada, para la compra de armas israelíes. Entonces queremos tener todos los elementos para tratar de entender si es un caso aislado o fue parte de una conducta de la gestión Sain”, detalló Blanco.

Además, se pedirá al Fiscal General de Santa Fe, Jorge Baclini, lo resuelto en la Junta de Fiscales del pasado lunes, a raíz de la decisión del fiscal regional de Rafaela, Diego Fernando Vigo, de ordenar a los fiscales de grado de no trabajar con el Organismo de Investigaciones mientras esté Marcelo Sain. “Lo que está sucediendo en términos institucionales es inédito y gravísimo. Queremos saber qué elementos tiene Baclini para resolver esta incompatibilidad manifiesta de la presencia del ex Ministro de Seguridad en el MPA”, aclaró el diputado.

Lo resuelto fue con los votos del bloque del Frente Progresista, del bloque de Cambiemos y del bloque Somos Vida y Familia, y se opusieron los diputados del peronismo.