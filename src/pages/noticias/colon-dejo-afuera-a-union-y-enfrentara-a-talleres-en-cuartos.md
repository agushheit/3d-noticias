---
category: Deportes
date: 2021-05-10T06:00:33-03:00
thumbnail: https://assets.3dnoticias.com.ar/colonunion.jpg
layout: Noticia con imagen
link_del_video: https://www.youtube.com/watch?v=7ZdLVrui8tM
author: La Central Deportiva
resumen: Colón dejó afuera a Unión y enfrentará a Talleres en cuartos
title: Colón dejó afuera a Unión y enfrentará a Talleres en cuartos
entradilla: Colón abrió el marcador a los 42 minutos del primer tiempo gracias a Delgado.
  García empató para el "Tatengue" a los 12 de la segunda etapa.

---
A pesar del empate, el clásico santafesino no defraudó. En el duelo válido por la fecha número 13 del torneo local, el "Sabalero" y el "Tatengue" definieron su futuro.

Con este resultado, Unión se quedó afuera de la próxima fase, mientras que Colón, quien ya estaba clasificado, se preparará para enfrentar a Talleres.

Los locales abrieron el marcador a los 42 minutos del primer tiempo de la mano de Rafael Delgado, quien apareció en soledad en el área y marcó el 1 a 0.

Sin embargo, ya en el segundo tiempo, el "Tatengue" encontró el empate con la ejecución de Juan Manuel García desde el punto de penal. Lamentablemente, el 1 a 1 no alcanzó y a Unión le faltó un gol más para conseguir el boleto a la fase final.