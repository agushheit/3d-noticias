---
layout: analisis
category: analisis
date: 2021-02-10T07:28:49-03:00
author: José Villagrán
title: "“FRENTES”: La palabra que une a peronistas, radicales y socialistas."
thumbnail: https://assets.3dnoticias.com.ar/jose_villagran.jpeg

---
Desde el gobierno hasta la oposición, todos miran las elecciones intermedias de este año. Todos empiezan a planear su estrategia electoral y a medir candidatos, sin dejar de indagar en lo que hacen los demás.

La política santafesina lejos estuvo de tomarse vacaciones. Todo lo contrario. A pesar de estar en enero, de a poco se están comenzando a inscribir las primeras líneas de este año electoral. 

Todos hablan de “frentes”. Frentes electorales que pretenden ser políticos y llegar a ocupar rangos de poder. Peronistas, radicales, socialistas y del PRO saben que, como está estructurada la escena hoy en día, con lo que tienen no les alcanza para imponerse holgadamente y que para convalidar un buen triunfo electoral es necesario hacer acuerdos. La pregunta del millón es ¿con quién conviene acordar?

**El “Frente Amplio” que sueña el radical**

El que más se ha movido en este “pase de año” veraniego en la provincia de Santa Fe ha sido el partido de la Unión Cívica Radical. El partido centenario cantó “primero” en este juego de ajedrez y le marcó la cancha al justicialismo pero también al Partido Socialista. 

Con los diputados provinciales Maximiliano Pullaro y Julián Galdeano a la cabeza, ya establecieron que la estrategia electoral de este año debe centrarse en garantizar la derrota del peronismo/kirchnerismo. Y la semana pasada lograron una jugada sumamente importante: conseguir el aval del partido a nivel nacional para que se construya ese tan nombrado “Frente Amplio” y logre derrotar al Omar Perotti. 

La táctica parece clara. Sumar a todos los sectores del radicalismo, incorporarlos a “Juntos por el Cambio” y convencer al socialismo para que se sume a la fuerza.  

“Es necesario la construcción de un espacio socialdemócrata que piense la provincia para el 2023, pero además que la defienda estos dos años del atropello K”, dijo al salir de la reunión en Capital Federal el ex ministro Pullaro, quien estuvo acompañado por Santiago Mascheroni, Carlos Fascendini, Darío Boscarol, Felipe Michlig, Hugo Marcucci y Lisandro Enrico. Es decir, acompañado por todos los sectores radicales internos. 

Es más, del encuentro con el presidente del partido y ex gobernador de la provincia de Mendoza Alfredo Cornejo, formó parte el senador por el departamento General López, Enrico, quien días atrás estuvo presente en la ciudad de Santa Fe, donde junto a un grupo de “Radicales Libres” se encontró con el intendente local, Emilio Jatón.

La estrategia radical se ve un tanto complicada. Porque el hecho de “convencer” al socialismo lleva a que éste defina sus autoridades partidarias, algo que ocurrirá en el mes de abril. Pero además, tendría que analizarse qué otras fuerzas políticas sumar. Por ejemplo, es sabido de la buena relación que entablaron en la Cámara de Diputados provincial el propio Pullaro con Amalia Granata. 

¿Se la puede sumar a este armado opositor? “Amalia Granata tiene potencial político y electoral” dijo el diputado radical a mediados del año pasado. “No hay que descartar que se sume”, indican desde su entorno.

Esto podría general una controversia con el socialismo, si se tiene en cuenta varios temas (entre ellos, el aborto), donde claramente tienen posturas disímiles pero coinciden en otros (como en la crítica a la gestión de Marcelo Saín al frente del ministerio de Seguridad).

Otra de las complicaciones que tiene la estrategia son los ya mencionados “Radicales Libres” que, haciendo honor a su denominación, se están moviendo libremente buscando posicionarse dentro del Frente Progresista si es que la mayoría del radicalismo se une con el Pro. 

Mientras tanto, en Juntos por el Cambio, hay quienes están esperando que el socialismo confirme su negativa para empezar a mover sus fichas. Uno de ellos es el ex intendente santafesino José Corral, de bajo vuelo político por ahora. No participó de la reunión con autoridades nacionales del partido (algunos dicen que no fue invitado) y en las últimas semanas se las repartió en recorridas por el interior de la provincia, charlas con Roy López Molina y el pedido de presencialidad al ciento por ciento en el regreso de las clases. 

Otro que analiza su futuro es el hoy diputado nacional Federico Angelini. Pegado a la gestión de Mauricio Macri, es otro de los que piensa en un armado con el socialismo adentro, lo que sería un hecho irónico ya que fue muy crítico de la gestión socialista durante los años de gobierno. Aún así afirmó: "Si el socialismo no ve esta realidad es porque hace una mala lectura del escenario político".

El radical que levantó vuelo esta última semana fue Carlos Fascendini, quien se reunión con ministros del gobierno de Perotti para hablar de las elecciones de medio término y lanzó una fuerte frase para el interior del frente progresista. 

**Salvar al Frente Progresista, la misión del socialismo**

Fascendini fue nada más y nada menos que vicegobernador del gobierno de Miguel Lifschitz, entre 2015 y 2019. Cuatro años no solo ejerciendo un cargo institucional de importancia dentro de un frente de gobierno sino que además un hombre muy cercano al ex gobernador. 

Sin embargo, eso parece haber quedado atrás. "El Frente Progresista ya es pasado" dijo en respuesta a las declaraciones de Miguel Lifschitz, quien había expresado que “no es lo que la sociedad nos está pidiendo” ante la consulta de integrar un frente opositor con sectores de “radicales amarillos” y el Pro. 

> El Frente Progresista debe esperar y decidir varias cosas internamente. Entre ellas, las elecciones del Partido Socialista, el espacio mayoritario. 

La contienda interna tendrá lugar el 18 de abril próximo y declarará las nuevas autoridades partidarias. Todo daría a suponer que la ex intendenta de Rosario, Monica Fein, será la que reemplace a Antonio Bonfatti en la presidencia del partido de la rosa. Aunque tenga rivales enfrente: por un lado, Roy Cortina, de alianza estrecha con el Pro en la ciudad de Buenos Aires, y Edgardo Di Polina del otro, enemistado con los sectores del socialismo actual que propone alinearse con sectores de izquierda como el del diputado provincial Carlos del Frade. Sus detractores lo acusan de querer convertir al partido en una aliado del Frente de Todos.

La trama dentro de la interna es compleja. El presidente a nivel nacional, Antonio Bonfatti no ha mantenido una buena relación con Lifchitz mientras este ejercía la gobernación. Sin embargo, esperan que su sector termine apoyando a Feín para cerrar esa grieta entre ambos. A cambio, la propuesta es acercar la candidatura de un viejo conocido: Ruben Galassi.

Otra de las cuestiones a definir es responder definitivamente a la “invitación” de los radicales díscolos que se están sumando a Juntos por el Cambio. Desde el entorno del ex gobernador afirman que su respuesta negativa se suscribe a no querer exponer al espacio a un rol insignificante dentro del “frente de frentes”. Además, sería cerrar un acuerdo con un sector con el que fue crítico y que también criticó. 

Por otro lado, deben sentarse a negociar con los “Radicales Libres”, que bajo las intendencias de Emilio Jatón en Santa Fe y Pablo Javkin en Rosario, plantean otra instancia dentro del frente. Y hago hincapié en esto último: no creen que la salida “superadora” sea cambiarse de bando. La pertenencia al Frente Progresista por parte de ellos está garantizada.

Pero, ¿quiénes integran este grupo? La estructura incluye al senador por el departamento General López, Lisandro Enrico, el diputado Fabián Palo Oliver, al secretario de Hábitat y mano de derecha de Javkin, Nicolás Gianelloni, a la presidenta del Concejo de Rosario María Eugenia Schmuck y al presidente del Concejo Municipal de Santa Fe, Leandro González. 

La propuesta de este grupo es clara. Con las intendencias de las dos ciudades más importantes de la provincia bajo el brazo,  la idea es ir articulando con representantes territoriales (intendentes y presidentes comunales) fieles y lograr una triangulación con el presidente de la Cámara Baja provincial. 

Además, desde el socialismo se ve con cierta atención como crece la relación de Javkin y Jatón, dos personas que no salieron de sus filas y que proponen renovar el progresismo santafesino. Además son dos mandatarios que no pierden de vista que se encuentran gobernando bajo un complicado panorama producto de la pandemia y el contexto de crisis económica, por lo que dependen de los aportes de los gobiernos, tanto provincial como nacional, que están en manos del peronismo.

Desde el Frente Progresista se ilusionan con la candidatura a senador nacional del ex mandatario rosarino. Sin embargo, Lifschitz no se ha pronunciado al respecto. Sigue, bajo su rol de presidente de la Cámara de Diputados de la provincia, encubriendo una recorrida por la provincia que hace presuponer que para él la campaña electoral ya empezó. 

**“Juntos” y “Todos”**

El peronismo también ya está pensando en las próximas elecciones. Con el respiro que le da la baja en la cantidad de contagios de Covid 19, el escenario político le permite tomar las riendas de algunas cuestiones. 

Los cambios producidos en el gabinete le vienen dando al gobierno de Omar Perotti, ese aire que necesitaba y levantar el “perfil político” que reclamaba el gobernador. 

Durante la última semana, los ministros Marcos Corach y Roberto Sukerman se reunieron con representantes partidarios de la Unión Cívica Radical para iniciar el diálogo sobre las unificación de las elecciones y la posible suspensión de las Primarias, Abiertas, Simultáneas y Obligatorias (PASO). 

En la primera idea, hay casi consenso. Y digo casi porque todavía faltan que se den las reuniones con el Partido Socialista y el Pro. Por lo otro, hay una espera para ver que se decide a nivel nacional y de ahí darle intervención a la Legislatura, si es que en definitiva se suspenden.

“Estamos generando consensos que nos permiten dar pasos importantes” dijo Sukerman quien además, junto a Corach, también llevan adelante las negociaciones por la ley de autonomía municipal. 

En tanto, el Partido Justicialista puso en funciones a sus nuevas autoridades partidarias, encabezadas por Ricardo Olivera, en un reconocimiento a la construcción electoral que hizo posible la unidad y victoria electoral del 2019. 

“Todos pusimos voluntad para construir la unidad del peronismo. Ahora hay que mantenerla”, dijo el propio Olivera, anticipando la ardua tarea que se le viene encima. 

“Se reafirmó la unidad de todos los sectores del peronismo santafesino", dijo el senador nacional y principal allegado a Perotti, Roberto Mirabella, que participó del acto que no tuvo como partícipe a Armando Traferri, enfrentado al propio gobierno.

El gobernador rafaelino estuvo presente a través de la plataforma zoom, donde dio un discurso que en la parte final dio lo mejor. Se lo escuchó autocrítico, donde reconoció que “al peronismo le hacen falta candidatos” en los últimos años y llamó a mejorar los vínculos, entendiéndose que este año y mes que lleva adelante de la gestión no hubo una buena comunicación. 

El respaldo más fuerte (de nuevo) del acto estuvo del lado del ministro nacional y principal aliado del gobierno provincial, Agustín Rossi, quien dijo que “necesitamos que en el 2023 Omar le coloque la banda de gobernador o gobernadora a un compañero o compañera”. 

La próxima historia que el peronismo santafesino comenzará a escribir es con otra aliada: María Eugenia Bielsa. Ya existieron algunos llamados entre el gobernador y la ex ministra. Se rumorea que detrás de esos diálogos se esconde un puesto pero nada se ha podido confirmar hasta el momento. Lo mismo sucede con espacios como el Movimiento Evita, La Cámpora y el Frente Renovador, que exigen mayor apertura dialoguista a este gobierno.

Así esta la política santafesina. Levantando temperatura al ritmo del verano santafesino. Y a la espera de que las cartas dejen de “orejearse” para comenzar a estar echadas.