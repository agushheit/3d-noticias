---
layout: analisis
category: analisis
date: 2021-02-15T03:41:22-03:00
author: Ignacio Etchart
title: "¿Para qué sirve la escuela?"
thumbnail: https://assets.3dnoticias.com.ar/ignacio1.webp

---
En pleno debate si la presencialidad en las aulas debe realizarse o no, el tema de la educación queda de lado. O tal vez, queda más sincerado de lo que se quiera o pueda interpretar.

Pues la presencia de un individuo en un espacio determinado no garantiza la educación, ni la transmisión de conocimiento, ni la erudición de los jóvenes, niñxs, y adultos también, por supuesto. La presencia de una o varias personas en un lugar sólo garantiza eso: estar en un espacio determinado durante un tiempo determinado.

Si la prioridad del debate educativo es volver o no las aulas, entonces no estamos hablando de “educación”. Estamos hablando de distribución de cuerpos en edificios durante un momento particular de los días.

Nada de lo que hasta aquí fue expuesto, ni lo que será más adelante, es palaba nueva ni santa. Todo está bien descrito en uno o dos capitulitos de "Redes o Paredes" de Paula Sibilia, que obviamente, no hace más que explicar a Foucault en campos no tratados de esa forma, ni con esa intención o con esa perspectiva, creo yo.

**Sobre su función**

La escuela es eso. También lo definió Corea en la Pedagogía del Aburrido. “La escuela es un galpón”, donde les enseñamos a lxs niñxs (aquí vuelve Sibilia) a que deben pasar tanto tiempo en tal lugar cumpliendo tales tareas en determinado orden durante un específico momento de su vida. Simplemente es un entrenamiento corporal, que deviene a mental y sobretodo espiritual, de quedarse quietitx por un rato… un buen rato.

Porque si el niñx aprende de entrada a cumplir un horario con un gesto corporal adecuado en una determinada ubicación, será un ser productivo para el desarrollo de una sociedad alienada, automatizada y consumidora, pero que antes se pretendía industrial.

¿O por qué existen las expresiones peyorativas y extremadamente correctivas como “des-ubicado”, “des-adecuado”-,”des-alineado”? Porque buscan eso, ubicar, adecuar, alinear una conducta, una posición a una forma temporoespacial predeterminada.

O los conceptos, en su mayoría referidos a las poblaciones postergadas: re-instertar, re-acondicionar, re-formar. “Refomar” particularmente suena espantoso, inhumano y violento, como todo el programa social, económico y político para esas personas.

Pero entonces, ¿por qué la escuela? ¿Por qué no direccionar o presionar conductas en instituciones como las fábricas y empresas, oficinas estatales, colegios de profesionales o asociaciones civiles? Pues es simple: “porque ya de adulto, al hombre no se le puede despojar su animalidad”, Kant, inicios del siglo XVII.

Este es el problema. Un siglo XXI pensado hace más de 300 años.

“La gente siempre dice que la pobreza se resuelve con educación. Pero lo que no aclaran, es que si la educación es para seguir teniendo pobres pero con buenos modales, o si la educación la deberían recibir los ricos, para que empiecen a preocuparse para que no la haya”, Capusotto y Saborido, desde el arte.

La ley Micaela de capacitación en materia de género en distintos ámbitos estatales y privados, desde la política, también es una expresión de que el problema del mundo no son los niñxs, sino los adultos.

**Los niños de antes**

No existían. O mejor dicho, existían durante muy poco tiempo. Rápidamente se convertían en adultos. El famoso cliché humorístico que navegó por las redes hace unas semanas (y ya dejó de verse, que dinámico todo), el cual parodiaba las prejuzgadas respuestas de los abuelos: “¡Ja! Porque yo cuando tenía tu edad...”.

Fue tal la magnitud del meme, que llegó a aparecer uno que rezaba:

Abuela – ¿Cuántos años tenes vos?

Nieta – 20.

Abuela – ¡Ja! Yo a tu edad tenía 25.

Si esto provoco una mínima risa al menos, se comprobaron dos hipótesis. Por un lado, que a todxs nos conmovió, por la razón que fuere. Y por el otro, es que esta parodia es más real de lo que aparenta. Lo cual, roza la tristeza.

“A la juventud la inventaron los Beatles”, señaló Pedro Saborido en una de esas tantas charlas que brindó por Instagram, ya ni recuerdo cual. “Coincido”, me dijo mi padre en Navidad.

Es decir, son relativamente pocas las generaciones jóvenes. ¿Cuántas? ¿5, 6?, ¿10?

Y no sólo eso. Las primeras generaciones de jóvenes fueron perseguidas, encarceladas, asesinadas, desaparecidas. En todo el mundo, al mismo tiempo durante la década del 70.

Luego, a la siguiente, la narcotizaron, los Chicago Boys, Reagan y Thatcher, la cocaína producida en Colombia y vendida en Los Ángeles para financiar el grupo de mercenarios denominados “Contras” (“Counterterrorism”), entrenados y armados por la CIA, cuya finalidad era acabar con los movimientos de izquierda en Centroamérica, inicialmente contra la Revolución Sandinistas en Nicaragua, comenzada en respuesta a la dictadura de Anastasio Somoza. Esto en los 80’.

Recién en los 90’ Latinoamérica aflojó con los genocidios planificados para entrar en generación planificada de pobres con la aplicación de los planes neoliberales instalados desde las subsidiarias de Estados Unidos, como son el FMI, el Club de París, el Banco Mundial, las multinacionales y la ONU con sus derivados.

Y durante el siglo XXI, ya con todo esto bajo el puente, se puedo respirar un poquito. Digan lo que digan, independientemente de las convicciones que se puedan postular, en ese momento los países latinoamericanos comenzaron a salir de un pozo que todavía tiene un larguísimo trecho por recorrer. Pero algo de oxigeno limpio se inhaló.

Después volvieron los liberales, Macri, Bolsonaro, Lacalle Pou, Moreno, Añez. Algunos por sufragio, otros por traición al partido que los colocó en las altas esferas de la administración estatal, y otras por un burdo, anacrónico y chabacano golpe de Estado, sustentado en plomo y sangre. Pero así les fue.

“La victoria y posterior derrota de Macri fue una reivindicación de la democracia”, me explicaba una camarada de la facultad. La gente eligió, votó según su deseo, y después volvió a votar, nuevamente según sus deseos.

![](https://assets.3dnoticias.com.ar/Portada de nota.jpeg)

**Divagué**

Perdón, me fui en el mediocre y pedante resumen de historia argentina. Pero es que la historia es eso. Un aparente bloque de hechos que en realidad están concatenadas, encadenados, envueltos en sí mismo como la serpiente Jörmundgander, o como la cíclica helénica, o el Eterno Retorno nietzchiano, y la repetición continua de representaciones ya conocidas de Freud, o la serie Dark de Netflix. Todo vuelve a su origen.

Por eso el presente deviene siempre al pasado. No hay otra forma de explicarlo. Otra frase de Saborido, miembro de la primera generación de jóvenes en Argentina, quien pronuncia el neologismo meme, “mema”. Un amor.

“Mi padre me mandaba a la escuela técnica para que pudiera ‘tener una profesión estable’, mientras Videla desarmaba la industria nacional”. Pero claro, Saborido Padre lo tenía bien claro: Pedrito debía ir a la escuela, estar en el aula, 5hrs seguidas para que después pueda hacer lo mismo en la desaparecida argentina industrializada, aunque en ese momento seguramente nadie gritó a viva voz por los medios contemporáneos “!tienen a las fábricas de rehenes!” o “!dejen trabajar a los chicos¡”.

En Europa sucede una segunda ola de contagios, en Sao Pablo debieron suspender las clases en varios distritos para frenar los casos, y acá “quieren” volver a las aulas. ¿Quienes quieren volver a las aulas? Los mismos que piden que no se tomen de rehenes a los chicos durante las medidas de fuerza docente por los reclamos salariales.

Una inflación del 36% y pretenden que los educadores trabajen con un salario por debajo del porcentaje, en el medio de una PANDEMIA MUNDIAL DE UNA GRIPE MORTAL SIN PRECEDENTES, bajo una infraestructura edilicia e institucional que hace décadas no logra re-adecuarse.

Es sorprendente como, tras 40 años de pedir por las condiciones de las escuelas, desde calefacción a gas natural, a que por favor pongan vidrios en las aulas porque a las 8am de agosto los mocos en las narices de lxs niñxs se transforman en estalactitas, o de hacer chistes porque nadie se quería sentar debajo de los ventiladores de techo de las aulas (si es que las tenían), de repente ahora todo el mundo quiere volver a esos lugares, pretendiendo además que se sigan estrictos, rigurosos e híper específicos protocolos de distancia entre cuerpos, uso de elementos de higiene y sanitización. ¿En qué cabeza cabe?

Se van a contagiar, van a contagiar docentes, a los administrativos, a burócratas, a perversos, a benignos, a niñxs, adultos, ancianos, padres, madres, hermanxs, abuelxs, desconocidos, amores y odiados. Van a contagiar a todo el mundo. De pedo que las terapias intensivas, luego de casi un año, bajaron el 50% de las camas de ocupación. ¿Qué necesidad? ¿Por qué tanta necedad?

Las clases en 2020 no se perdieron. Todo lo contrario. Se reforzaron los esfuerzos, humanos sobre todo, para que no suceda. Y se logró. A pesar de todo lo descrito en los párrafos previos, las clases se dictaron, en gran parte, gracias al cuerpo docente, a las personas que ahora quieren enviar como carne de cañón infectuosa.

Estamos en medio de una pandemia global. Al momento de este escrito, en el mundo murieron casi 2 millones y medio de personas, existen 107 millones de casos positivos y se recuperaron 60 millones. Se puede no creer en la existencia del virus pues, en definitiva, la cosmovisión del mundo depende de cada sujetx.

Pero eso no implica utilizar niñxs como palanca de puja política partidaria. “Ya estamos grandes para esas cosas”, enseñaba Don Aquiles en Luna de Avellaneda.

En fin, dejen de bolacear, quédense en sus casas, cuiden a quien tienen al lado. No es tan difícil, obviando claro cuestiones de estatus socioeconómico. Pero seguramente es mucho menos lío que contagiarse de coronavirus, o peor aún, que un ser querido lo haga, o muera por eso.