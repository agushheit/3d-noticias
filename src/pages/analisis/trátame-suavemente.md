---
layout: analisis
author: Por Sofía Gioja
category: analisis
title: Trátame suavemente
date: 2020-11-22T14:21:33.093+00:00
thumbnail: https://assets.3dnoticias.com.ar/Gioja.webp

---
**A Mavi la tironearon de un brazo para levantarla de la cama, la retaron por todo, no la dejaron caminar y le dijeron que se portara bien, porque si no ellas (las enfermeras) sabían hacer sufrir. “¡Se nos pueden caer los bebés, viste como es esto gorda!”**

### **En el caso bisagra**

Agustina Petrella, fue la primera mamá argentina que se presentó a juicio al comprobar que fue víctima de violencia obstétrica. No fueron respetados ni sus derechos, ni los derechos de su hija recién nacida.

Su historia empezó en 2017 cuando le dieron el alta médica luego del parto. En ese momento decidió hacer una denuncia en la Defensoría del Pueblo de la Nación. Hubo una auditoría y determinaron que, efectivamente, había existido violencia obstétrica, una de las seis formas de violencia contra la mujer estipuladas dentro de la de violencia de género.

Agustina sabía que existía la ley 25.929, de parto humanizado, y que tenía derecho a presentar en la clínica su plan de parto. Lo hizo y en la clínica se lo negaron: "Vos sos la que presentó la cartita? Acá no estamos para satisfacer caprichitos de los padres", le dijo la coordinadora de neonatología.

El día del nacimiento de su hija, a Petrella le habían hecho un monitoreo que indicaba que le faltaban como 15 días más para el nacimiento. Según sus sospechas, ese día, durante el tacto, el obstetra le colocó una cápsula de Prostaglandina para acelerar el trabajo de parto.

Su hija nació sin complicaciones, pero no se la entregaron al nacer. Una hora tardaron en darle a su beba. "Cuando fuimos a reclamar, la trajeron. Estaba dormida, bañada, aspirada y ya la habían pinchado. Al rato vomitó algo blanco y me di cuenta de que le habían dado leche de fórmula. Absolutamente todo lo contrario de lo que yo había pedido".

Según la Defensoría, existen menciones a la violencia obstétrica en casos de bebés o madres que murieron, pero hasta ese entonces, no había antecedentes de alguien que hubiera hecho un juicio por violencia obstétrica en sí misma.

![](https://assets.3dnoticias.com.ar/violencia-obstétrica.jpg)

Su caso se suma a otros similares, pero con diferentes aristas. Uno, es el de la misionera Paula Pisak, que inició un juicio por violencia obstétrica, en el que hubo mala praxis. Dio a luz por cesárea en noviembre de 2004 en la Clínica Privada Candia. El trato que recibió como parturienta derivó en descompensaciones, hipoacusia y parálisis muscular. Paula cuenta que aquella vez, lo que le practicaron fue una “_inne-cesárea_”, porque cuando llegó al sanatorio sus condiciones físicas estaban óptimas para tener un parto natural. Sin embargo, los médicos decidieron intervenirla quirúrgicamente sin darle ninguna explicación certera. “Según los peritajes médicos, (la grave secuela física) se debió a que me perforaron la médula con la epidural, y sumado al abandono, terminó en esto; porque si se daban cuenta a tiempo y me atendían con parches sanguíneos y un control responsable, se remediaba. Pero nadie estuvo allí conmigo, solo sobreviví. Un milagro, me dijeron en el Fleni”.

Otro es el de Johanna Piferrer a quien dejaron 9 horas internada en una maternidad (llena de recién nacidos) con su hijo muerto en la panza. “Me internan en Maternidad, les pido que me saquen de ahí, que mi situación era completamente diferente al resto de las otras familias, a lo cual me responden que no tenían otro lugar para mí. Me empiezan a explicar que iban a inducirme a un parto vaginal con medicación, ya que mi vida no corría peligro, les dije que no y pedí una cesárea. Frente a dicho pedido, me dijeron que no era una urgencia y qué la cesárea recién me la iban a realizar al otro día a la mañana, todo esto ya sabiendo (desde las 2 de la tarde) que Ciro estaba sin vida dentro de mi panza”. Se lo entregaron en una caja azul de archivo junto a un certificado de defunción que decía: "Feto NN masculino, 33 semanas de gestación, 2,300 kilos".

El primer informe que elaboró el colectivo _Ni Una Menos_ sobre violencia machista, en base a una encuesta nacional en la que participaron 60 mil mujeres, reveló que el 77 % sufrió, al menos, una situación de violencia obstétrica. Existen prácticas invasivas y muchas veces innecesarias, como el rasuramiento, los enemas, la episiotomía, la inducción del parto, las cesáreas que podrían evitarse, la obligación de adaptarse a la posición horizontal de las camillas, la negativa a permitir que la mujer entre acompañada a la sala de partos, la impaciencia o la hostilidad. Hay varias ONG y diversas iniciativas que denuncian estas experiencias de violencia obstétrica como una forma de la violencia de género.

La psicopedagoga Laura Gutman describe en su libro _La maternidad y el encuentro con la propia sombra_ las diversas prácticas efectuadas durante el embarazo y el parto de una mujer. “La internación supone permanecer tumbada y quedar a merced de los tactos vaginales frecuentes y realizados por varias personas (en los hospitales públicos, varios estudiantes de obstetricia hacen sus prácticas), mientras el tiempo corre en contra”. Las Cesáreas, son rutinarias en la inducción de los partos, es lógico que la mayoría de las cesáreas sean «fabricadas», habiendo exigido a la madre y al bebé una dinámica artificial en el trabajo de parto que finalmente «explota», tras lo cual son salvados gracias a la cesárea. ¿Es tan grave una cesárea? No, no es gravísima en sí: hoy en día las cesáreas salvan a muchos niños y a muchas madres, y es una maravilla que exista esta posibilidad sin grandes riesgos. Lo único grave es el número de cesáreas innecesarias que se practican en el mundo occidental por desconocimiento, por dinero, por estar al servicio de la comodidad de los profesionales y por la banalización que se ha hecho de esta práctica”.

El Ministerio de Justicia y Derechos Humanos de la Nación, bajo ley de Protección Integral para prevenir y sancionar la violencia contra las mujeres (Ley Nacional 26.485), estableció la violencia obstétrica como un tipo de violencia institucional y la definió como “aquella que ejerce el personal de salud sobre el cuerpo y los procesos reproductivos de las mujeres, expresada en un trato deshumanizado, un abuso de medicalización y patologización de los procesos naturales”.

![](https://assets.3dnoticias.com.ar/violencia_obstétrica-1.jpg)

Cualquier persona del equipo de salud puede ejercer violencia hacia la mujer durante la atención del pre parto, parto y post parto, y también post aborto. Esta violencia puede manifestarse de diferentes maneras: maltrato, falta de atención o consideración, intervenciones médicas injustificadas sobre el cuerpo de la mujer, falta de información sobre las prácticas médicas, falta del pedido de consentimiento informado o que le hayan negado el derecho a estar acompañada durante todo el proceso del parto, inclusive si el mismo fue por cesárea.

En el caso de las cesáreas, Perla Prigoshin, titular de la CONSAVIG (Comisión Nacional Coordinadora de Acciones para la Elaboración de Sanciones de la Violencia de Género) expresó: “cuando nosotros indagamos en las instituciones sobre el motivo, la respuesta es que los acompañantes no pueden ingresar por protocolo de las intervenciones quirúrgicas. Sin embargo, no hay demostración científica de que una persona con el equipamiento correspondiente aumente el riesgo de infecciones".

### **Violencia obstétrica en números**

**Falta de acompañamiento**: Se deja a la mujer sola, sin interlocutores de confianza ni testigos de lo que le sucede en su internación. Durante el trabajo de parto, el 25% de las mujeres estuvieron totalmente solas. Durante el parto, un 36%. Durante la etapa de posparto, el 20%

**Trato:** El 28% fue criticada por los médicos. Un 56% fue tratada con sobrenombres. Un 30% recibió comentarios irónicos o descalificadores. A un 36% las hicieron sentir que corrían peligro ella o su hijo/a Y un 47% no se sintió contenida.

**Información:** El 44% de las mujeres no fue debidamente informada sobre la evolución del trabajo de parto o su bienestar ni del de su bebé.

**Cesárea:** El 46% tuvo a su hijo por cesárea, siendo el porcentaje estándar de la OMS un 12-15%, de las cuales 39% fueron programadas. Dentro de este número, el 54% de las madres eran primerizas. Existe un mayor índice de cesáreas en el sistema privado, que se eleva hasta el 64%.

**Rotura artificial de bolsa**: El 70% de las encuestadas no recibió información clara, adecuada y completa y por ende no dio su autorización.

**Inducción del parto**: El 29, 7% de las encuestadas tuvo un parto inducido, siendo el porcentaje estándar de la OMS menor a 10%.

**Anestesia**: El 36% de las mujeres fueron anestesiadas sin haberlo solicitado.

**Prácticas sobre el/la bebé**: El 74% de las mujeres no recibieron suficiente información sobre las prácticas que realizaron sobre su hijo/a, por ende, tampoco dieron autorización para realizarlas. Aproximadamente el 45% de las mujeres no sabe o no recuerda qué prácticas fueron hechas sobre su hijo/a.

Además de la Ley de Protección Integral antes mencionada también existen la Ley de Parto Humanizado (Nº 25.929) y la Ley de Derechos de los Pacientes (Nº 26.529). Estas leyes son de cumplimiento obligatorio en todas las provincias del país, en todas las instituciones de salud, tanto en el ámbito público como en el privado.

Gran parte de las mujeres no están informadas acerca de lo que los médicos harán en su cuerpo y en el de sus hijos durante y después de parir. O les mienten, como le pasó a Soledad B. quien vino a parir a la capital santafesina, desde un pueblo del Norte, porque su médico era de acá y los hospitales son mejores. Soledad relata: “Cada vez que íbamos a consulta le preguntaba a mi obstetra si iba a estar en la cesárea (porque la beba venia sentada). Cuando llegó el momento de parir, me dejaron sola en el quirófano. Cuando pregunté, llorando del miedo, por qué el “Dr. B.” no estaba ahí, me dijeron que el doctor no entraba más a quirófano desde hace un montón de tiempo”. “En ninguno de mis dos partos estuvieron mis médicos”.

En Argentina más de la mitad de las madres, en algún momento se sintió en peligro, padeció el trato deshumanizado y fue criticada por sus actitudes: como por ejemplo llorar o gritar de dolor. El 70 % no pudo elegir una posición para parir que le fuera cómoda. Las acostaron en camillas, las conectaron a sueros y cables, sin siquiera poder moverse. Los bebés apenas tuvieron un primer contacto con sus mamás al nacer, cuando la Organización Mundial de la Salud dice que no hay que separarlos. En algunos centros de salud, luego de la cesárea, él bebe es limpiado, vestido y entregado al padre (que espera afuera) mientras la madre sale de cirugía y es devuelta a la habitación.

El contacto inmediato del bebe con su madre luego de nacer es conocido como “la hora sagrada”. Lo explica la doctora Constanza Soto Conti, médica de planta del Hospital Materno Infantil argentino Ramón Sardá: "El contacto piel con piel entre la madre y su hijo estabiliza la respiración y la oxigenación del bebé, mantiene sus niveles de glucemia, estabiliza la presión arterial, reduce las hormonas del estrés, disminuye el llanto, incrementa el estado de alerta tranquila, promueve el inicio precoz de la lactancia materna, y mantiene la temperatura, reduciendo el riesgo de hipotermia".

### **Violencia impune**

En Santa Fe, el problema con este tipo de prácticas es la naturalización de la violencia. Muchas las madres entrevistadas lo cuentan como “algo normal”:  “En mi época te hacían un enema y nadie decía nada porque era doctor”, “Cuando me cortaron para la cesárea le dije al médico que me dolía, que no había prendido la anestesia y me dijo que me aguantara, después me durmieron entera y me desperté en la maternidad”. Son muy pocas mujeres las que se animan a contar sus experiencias y las que no lo hacen es por ignorancia o “porque siempre fue así”.

Constanza L. cuenta cómo trataron, entre otros atropellos, de disfrazarle el procedimiento de parto: “Yo estaba con contracciones desde que llegué. Y me cambié como pude, me puse el camisón y llegaron las enfermeras. Hola mamá! Te vamos a poner el "suerito" me dijeron. ¿Qué suerito me van a poner? les dije. ¿Oxitocina me quieren poner? Porque yo no quiero que me pongan oxitocina, vengo bien con las contracciones. Entonces la doctora se molestó y me dijo ¿Vos hiciste un curso de preparto?, ¿Si, por? Le respondí. Mira, acá nos manejamos así, es por tu bien. ¿O vos qué querés?! Querés estar 14 horas con contracciones?!¡¿Sabes lo que es eso? ¿Vos querés sufrir 14 horas?

“Las contracciones vienen como en forma de olas” cuenta Constanza “empiezan suave, llegan al pico de dolor y bajan la intensidad, después de que me colocaron la oxitocina para acelerar ese proceso de contracciones, las contracciones no me daban respiro. ¡Ya no venían naturalmente como en forma de olas, entonces de verdad que me costaba hasta respirar!”

En junio del 2017 La Cámara de Senadores de la provincia de Santa Fe dio sanción definitiva al proyecto de adhesión a la ley nacional de parto respetado, que regula todos los derechos de las madres y sus hijas e hijos durante el proceso del embarazo y el nacimiento. Sin embargo, son muy pocas las instituciones públicas o privadas que aplican la normativa.

Los principales motivos por los cuales las madres no denuncian malos tratos antes, durante y después del parto, reside en la naturalización de ciertos comportamientos y en segundo lugar prefieren pasar por toda esa odisea con tal de que sus hijos nazcan sanos y salvos.

Ale M. tuvo a su hijo en una clínica de pueblito del interior y cuenta su experiencia: “A las 22hs la enfermera empezó a obligarme a hacer fuerza para pujar y yo no tenía contracciones. En la sala de parto empezó a pegarme en la frente obligándome a hacer fuerza. Para cuando el bebé quiso salir me desmaye. La médica de turno lo sacó como pudo, había nacido asfixiado. Estuvieron como 5 minutos para revivirlo entre la doctora y otro médico. Por suerte no hubo q lamentar la vida de mi hijo porque lograron resucitarlo. Pero estuvo muchos días moradito y con el cráneo tipo huevo”.

Por su parte Naty A. cuenta cómo fue su parto:“Dos semanas antes de que nazca mi segundo hijo, me internaron porque ya tenía contracciones. Me pasaron una medicación que era para 24hs en 45 minutos. Se olvidaron abierta el chapita del suero, por eso pasó tan rápido. Me dejaron sola y cuando me di cuenta y quise sentarme no pude. Se me cerró el pecho y mi corazón parecía q iba a explotar. No sé cómo hice para tocar el timbre y cuando llegó la enfermera y me vio empezó a los gritos. Tenía taquicardia y la presión había subido casi a 17. Cuestión que nadie se hizo cargo de la macana. Ni la doctora ni las enfermeras. Como al bebé no le pasó nada, quedó todo ahí” …” De todos modos, la pasé tan mal que cada vez que lo cuento se me hace un nudo en la garganta, y solo agradezco que Lorenzo nació sano y no le pasó nada. Tampoco conté a nadie lo q me pasaba, no sé por qué. Pero te puedo asegurar que ni mis hijos ni yo volvemos a pisar esa clínica…”

### **Parir en casa**

Si bien muchas mujeres planifican desde un principio parir de forma domiciliaria,transitar un embarazo en el contexto de una pandemia mundial cambió algunas reglas del juego. Por temor a asistir a un centro de salud y contagiarse, por cancelación de turnos o saturación del sistema, las consultas para obtener información sobre partos planificados en el domicilio crecieron un 30%.

Los datos fueron aportados por Francisco Saraceno, primer hombre egresado de la licenciatura en Obstetricia de la Facultad de Medicina de la UBA e integrante de la agrupación Las Casildas y del Observatorio de Violencia Obstétrica, en sintonía con la Semana Mundial del Parto Respetado. “En esta pandemia se está viendo que a un 70% de las mujeres se les han cancelado los turnos”. Sin embargo, aclaró que aun así la modalidad aún no es masiva, ya que “un poco menos del 1% tiene partos planificados en domicilio en Argentina, y la gran mayoría se centra en Capital y Buenos Aires”.

La tranquilidad de estar acompañada en un entorno conocido e íntimo, conservar la autonomía, poder moverse y cambiar de postura, no sentirse manipulada y saber que la intervención médica es la menor posible son algunos de los argumentos citados por las mujeres que han preferido su hogar al hospital para recibir al bebé.

Si el parto en casa se complica hay que ir lo más rápido posible al hospital. Ese traslado está siempre previsto, pero aumenta el riesgo frente a quienes están a solo a pasos de distancia de un quirófano.

En Argentina, estos partos son legales, pero muchas mujeres no lo saben. Además, hay un obstáculo económico: solo se realizan por fuera del sistema de salud público y no están cubiertos tampoco por los seguros privados, así que las familias que se deciden por esta opción, necesitan recursos económicos suficientes para costearlo o encontrar a profesionales que acepten algún trueque como parte del pago.

“Las personas que tienen la ocasión de ser testigos de partos respetados (fuera y dentro de los hospitales y clínicas) experimentan la sensación de ser partícipes de una evidencia: el mundo sería otro si las salas de partos fueran menos silenciosas, si en el inicio de la relación entre seres humanos hubiera lugar para las emociones, si la deshumanización no abarcara las áreas de la bienvenida al mundo”.

<hr style="border:2px solid red"> </hr>

##### **Sofía Gioja en** [**twitter**](https://twitter.com/SofiG83)