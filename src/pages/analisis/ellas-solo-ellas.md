---
layout: analisis
category: analisis
date: 2021-03-08T10:57:43-03:00
author: Marisa Lemos
title: Ellas, solo Ellas
thumbnail: https://assets.3dnoticias.com.ar/marisa.jpeg

---
_"Para mí el deporte es ganas de vivir. Es una conexión con la pasión interior que sale y da felicidad, pero sobre todo es compartir, aprender y amar es un gran cable a tierra que da paz"._ Tati Castaldi

_"Hablando de deportes en general, pienso que la mujer ha evolucionado un montón en este tiempo, se está haciendo notar y respetar y más que nada rompiendo estereotipos y derrumbando barreras sociales. Vamos en un crecimiento constante, evidente y notable en todos los deportes"._ Sole Hernández

Para algunos solo pueden significar palabras sueltas y sin sentido, pero para otros será ese no sé qué con el cual se identifican porque expresan en pocas palabras la fidelidad de lo que piensan.

A vos que estás leyendo esto te pregunto: ¿cuántas veces escuchaste la frase “ese es un deporte para hombres”? Pero es eso, SOLO UNA FRASE VACÍA Y SIN SENTIDO.

A través de los años, las mujeres han batallado en los diferentes ámbitos de la sociedad. En el deporte, tanto adaptado como convencional, cada vez hay más mujeres que destacan por su capacidad en diversas disciplinas, rompiendo limitaciones y estereotipos.

Los cambios son posibles si hay compromisos, responsabilidad, voluntad, convencimiento y, sobre todo, si se cuenta con las herramientas y los recursos. Sin duda, se han dado pasos en los últimos tiempos en la visibilización y valoración del deporte femenino.

Cuando se visibiliza el deporte femenino, estamos contribuyendo a crear referentes para las chicas jóvenes y estamos apoyando el empoderamiento de las nuevas generaciones para que se animen a ver más allá sin importar tu condición física y social. La lección es clara. Si bien son importantes las acciones o medidas puntuales en materia administrativo/deportivo, los cambios deben ser estructurales, tienen que ir a la raíz del problema e incluir la perspectiva de género en las políticas de gestión de la actividad física y el deporte para garantizar la plena igualdad de acceso, participación y representación de las mujeres a todos los niveles.

Uno siempre tiene la capacidad de ver más allá del muro, y eso es lo que hicieron estas señoritas. Se guiaron por sus instintos y no se quedaron sentadas a ver que pasaba sino que fueron por ellos sin importar el miedo, su condición y mucho menos el qué dirán.

Cada una tiene sus propias batallas y ELLAS, SOLAMENTE ELLAS, SABEN COMO LIDERARLAS.

***

![](https://assets.3dnoticias.com.ar/deporte-pic.webp)