---
layout: analisis
author: Por Ignacio Etchart
category: analisis
title: Odio Go Home
date: 2020-11-21T13:17:38.592+00:00
thumbnail: https://assets.3dnoticias.com.ar/Etchart.webp

---
En uno de los reportajes realizados al Che Guevara durante su gira por los Estados Unidos en 1964, el Comandante conceptualizó a la sociedad estadounidense de una forma muy particular. Frente a cualquier prejuicio que pudiese recaer sobre el líder revolucionario, el Che destacó que "el pueblo norteamericano no es culpable de la barbarie y de la injusticia de sus gobernantes, sino que también es víctima inocente de la ira de todos los pueblos del mundo".

Los Estados Unidos son un imperio, así como cada tiempo histórico de la humanidad tuvo sus potencias políticas y armamentísticas. No es ninguna novedad. Tal vez lo único novedosos sea que, a diferencia de otros tiempos, esta vez el imperio es global y simbólico.\
Dicho en otros términos: no sólo es un imperio con el poder de destruir toda vida en planeta decenas de veces, sino que también tienen la capacidad de emitir mensajes que derivan en prácticas, ideas y emociones con muchísima más dinámica, efectividad y eficiencia que cien ojivas nucleares.

Lo bueno de este panorama es justamente lo previamente descripto. La pluma sigue siendo más poderosa que la espada. La cuestión es que la pluma estadounidense (que además compra y condiciona a las más creativas plumas del mundo) es leída por todo ser humano que más o menos tenga acceso a un dispositivo mediático, desde una hoja de diario hasta el teléfono más nuevo disponible en el mercado.

Sin embargo, como bien definió el Che casi 60 años atrás, el pueblo norteamericano también es víctima de las violencias de sus gobiernos. Y dentro de ese pueblo, guste o no, también se encuentran las corporaciones mediáticas. Corporaciones a las cuales el saliente presidente Donald Trump decidió darles la espalda y utilizar redes sociales, especialmente Twitter, como medio de comunicación formal institucional.

![](https://assets.3dnoticias.com.ar/foto-ilustrativa.jpg)

Esto no fue determinante en su derrota, sino más bien su discurso de odio. Odio al inmigrante, al negro, a la mujer, al pobre, al otro en definitiva. El slogan “Make America Great Again” repica muy cerca del título de una de las películas que fundaron el Séptimo Arte, The Birth of a Nation de 1915, dirigida, escrita y producida por D. W. Griffith. Es la América de Griffith que Trump intentó volver a instalar en el siglo XXI.

La América retratada en la película, por más innovadora y creadora que fuere, no deja de ser una llanto melancólico a los Estados Unidos rural y esclavista, sureña y oligarca, snob, pudiente y pedante. Esa América que Tarantino describe hermosamente en Django Unchained, cuando los protagonistas visitan la estancia propiedad del personaje de Don Johnson. Dos películas, dos historias, un país. Sin embargo, los discursos son totalmente opuestos.

### **“El odio idiotiza”**

Esta fue una de las más replicadas frases con las cuales José “Pepe” Mujica se despidió de la vida política formal. Sin embargo, idiotas o no, el odio convoca.

Sería redundante citar la enorme cantidad de líderes políticos que construyeron su corpus de seguidores, adoctrinados o aficionados con un discurso de odio. Desde Hitler hasta el mismo Trump, pasando por Martínez de Hoz y Bolsonaro, el discurso separatista y desestabilizante del odio ha demostrado ser productivo, incluso en términos electorales.

Pero para que el rechazo discursivo se instale en el tráfico de sentidos que hace a la vida de los sujetos, necesita apoyarse en el objeto odiado y así constituir las diferencias que permitieron aborrecer, en este caso, al negro, al pobre, al extranjero y por supuesto, a la mayor otredad occidental, la mujer, tal como lo definió John Lennon en “Woman is the nigger of the world” (la mujer es el negro del mundo).

No obstante, todas estas citas, menciones y personajes refieren a un tiempo particular: el Siglo XX. Para quienes estén por peinar canas o revisándose cada vez más seguido las entradas en el flequillo, y a veces les resulte extraño o curioso qué tienen en la cabeza las juventudes que están por venir (o que ya llegaron hace rato), tal vez les sirva la película The Fifth Element (El Quinto Elemento) de 1997, dirigida por el francés Luc Besson y protagonizada por Bruce Willis, Milla Jovovich y Gary Oldman.

Como toda película de ciencia ficción, la parte de “ciencia” en su género lo es solamente por un tiempo. Hoy 1984 de George Orwell es más una crónica que una novela futurista distópica. Pero volviendo al Quinto Elemento, y a la acertada escena donde el personaje extraterrestre de Jovovich aprende con un dispositivo que recolecta toda la información conocida por la humanidad, la cataloga por letra y sistematiza para su aprendizaje. Bastante similar al internet, ¿no?

Quienes ya la hayan visto, seguramente recuerden (porque es además el clímax del personaje de Jovovich) cuando la ingenua muchacha, extasiada por el aprendizaje adquirido gracias al dispositivo, llega a la letra “W”, específicamente a la palabra “war” (*guerra* en inglés).

Siglos de vestigios de guerras pasan como flashes frente a los ojos incrédulos pero incerrables de la extraterrestre mientras un intenso crescendo de la banda sonora aprieta el pecho hasta culminar en una explosión musical que acompaña la imagen del hongo de Hiroshima. Esta escena es la mayor expresión de qué tienen los jóvenes hoy en la cabeza.

### **Los discursos generan acciones**

La perfomatividad del lenguaje ya es algo innegable. Palabras que generan acciones, que construyen realidades, que proliferan otros discursos. Pero los discursos no son palabras. 

Un video es un discurso, así como también lo son una canción, una fotografía o un tweet.\
Si un disco musical provoca que los peinados crezcan o se recorten con tanta facilidad, imaginen lo que provoca la fotografía de un presidente saludando a un policía que mata por la espalda. O también, volviendo al Imperio, si el Emperador trata de “terroristas” (como lo cita un artículo de la BBC publicado el 3 de junio de este año) a los manifestantes que salieron en todo el país luego del asesinato de George Floyd en manos de la policía de la ciudad de Minneapolis.

¿Qué pensaría Leeloo, el personaje de Jovovich, al llegar a la M de “murder” (asesinato), si mira la grabación de la muerte Floyd? Seguramente lo mismo que aquellos miles de *terroristas* que salieron a las calles a protestar. Eso la convertiría, por ende, también en terrorista, palabra muy potente en el léxico estadounidense luego del atentado a las Torres Gemelas.

Esto genera el discurso de odio: división. Joe Biden no ganó las elecciones, Trump las perdió. Trump pertenece a aquella estirpe social que busca cada vez más representación política en un mundo cuyas nuevas generaciones ya no los toleran.

La homosexualidad, el racismo, la xenofobia, la misoginia, la aporofobia y miles de odios más que aún están sin denominar formalmente estarán siempre presentes, sí, pero ya es sabido que no producen paz ni armonía. Estamos hablando de una generación que conoce absolutamente todos los crímenes, defectos y perversiones de las antecesoras. Hijos y nietos cuyos padres ya no son modelos a seguir, impolutos e imperfectos.

Posiblemente la agenda de Biden en política internacional sea aquella que siempre ha llevado los Estados Unidos en su existencia. Bombardeos en pos de la democracia e intervenciones ilegales en procesos políticos ajenos. Probablemente así sea.\
Pero esa sociedad, víctima también de un odio global culpa de su gobierno, por lo menos se animó a elegir a un presidente que no prolifera discursos como:

### **Sobre política internacional**

![](https://assets.3dnoticias.com.ar/tweetborrado.png)

* “El calentamiento global es un invento creado por China para que la economía estadounidense no sea competitiva” – vía Twitter. Donald J. Trump (@realDonaldTrump) 6 Noviembre de 2012, hoy Tweet borrado obviamente.
* “Nuestro gobierno ahora importa inmigrantes ilegales y enfermedades mortales. Nuestros líderes (estatales) son ineptos” – vía Twitter. 9:55 a. m. Donald J. Trump (@realDonaldTrump) 5 de agosto de 2014 [https://twitter.com/realdonaldtrump/](https://twitter.com/realdonaldtrump/status/496640747379388416?lang=es)
* “Si gano, ellos serán devueltos, podrían ser ISIS” – mensaje sobre los refugiados sirios radicados en Estados Unidos durante la campaña presidencial del 2015. [https://www.youtube.com/MSNBC](https://youtu.be/cy4QZ5gdkyg)
* “¡La tortura funciona! El submarino es una forma menor de tortura, lo cual está complemente bien, aunque deberíamos profundizar en algo mucho más fuerte que el submarino”, señaló Trump durante una entrevista realizada en 2016, respecto a las tareas militares que realiza Estados Unidos en el extranjero. [https://www.youtube.com/CBS News](https://youtu.be/FSnR6EJlK6E)

### **Sobre política nacional**

* “John McCain no es un héroe de guerra”, expresó Trump respecto a su adversario durante la campaña por las Primarias del Partido Republicano en 2016. [https://www.youtube.com/CNN](https://youtu.be/wefb0ESoVDo) 

  En un país donde casi toda familia tiene en su haber al menos un ex combatiente, ya sea veterano o caído en combate, donde las Fuerzas Armadas son tal vez la institución más respetada por la sociedad, mencionar que un oponente político, capturado y torturado durante la guerra de Vietnam, único conflicto perdido por el Imperio, no es un “héroe de guerra”, no es poca cosa.
* “Yo podría pararme en el medio de la Quinta Avenida y dispararle a alguien y no perdería votantes”, pronunció Trump durante un encuentro del Partido Republicano realizado en Iowa el 23 de enero de 2016, durante su campaña presidencial. [https://www.youtube.com/CNN](https://youtu.be/iTACH1eVIaA)
* “Obama y (Hillary) Clinton son fundadores de ISIS”, fue uno de los argumentos que utilizó Trump durante la carrera por la presidencia en el 2016, donde Obama, presidente cuyo mandato estaba por finalizar, apoyaba a la candidata representante del Partido Demócrata. [https://www.youtube.com/ArirangNews](https://youtu.be/Hlck8rulk84)
* "Este es un país en el que hablamos inglés. ¡Hay que hablar inglés!", repitió Donald Trump durante la campaña para las elecciones presidenciales de 2016 en Estados Unidos. ¿En qué derivó este discurso? Pues...

![](https://assets.3dnoticias.com.ar/speak-english-imagen-utilizada-en-el-texto-.jpg)

Habría que mostrarle a Donald Trump la película de Martin Scorsese “Gangs of New York” (Pandillas de Nueva York), tal vez así aprenderá algo sobre el origen cosmopolita de la sociedad que supo liderar y representar. 

<https://www.bbc.com/mundo/noticias-internacional-50175637>

### **Y sobre la mujer**

* “Cuando eres una estrella, dejan que les hagas lo que quieras. Cualquier cosa puedes hacerles. Agarrarlas por la *p\*\*y*”. 

  Estás cavernícolas palabras fueron filtradas gracias a una grabación oculta realizada 2005 y divulgada por el [Washington Post](https://youtu.be/WhsSzIS84ks). 
* “Cara de caballo, buscavidas, gordas, feas, animales”. Hacia adversarias políticas, periodistas y denunciantes de abuso sexual contra él. Los adjetivos de Donald Trump hacia la mujer, sistematizados y resumidos en este artículo del New York times. <https://www.nytimes.com/2018/10/16/us/politics/trump-women-insults.html>