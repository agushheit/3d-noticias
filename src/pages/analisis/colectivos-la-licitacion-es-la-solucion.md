---
layout: analisis
category: analisis
date: 2022-03-02T09:14:47-03:00
author: Carlos Suarez (Concejal mandato cumplido de la ciudad de  Santa Fe
title: 'Colectivos: ¿La licitación es la solución?'
thumbnail: https://assets.3dnoticias.com.ar/suarez.jpg

---
Hace más de dos años junto a mis pares, propusimos desde el Concejo Municipal que se conformara una mesa con todos los actores vinculados al transporte, para abordar definitivamente la necesidad de un nuevo sistema de transporte por colectivos. Esta convocatoria debía realizarse reconociendo la complejidad del tema.

 

El transporte urbano por colectivos es un negocio subsidiado y con inequidades en la distribución federal de esos recursos. Tal es así que el Área Metropolitana Gran Buenos Aires concentra la mayor parte de esos fondos y esta situación, a lo largo del tiempo, generó que el servicio en el interior se deteriore hasta llegar a la realidad que hoy se vive en nuestra ciudad: un boleto caro y un servicio malo.

 

Pero también es cierto que el transporte está degradado por factores internos del sistema: incumplimiento de las frecuencias y recorridos, que además no cubren las demandas de una ciudad que crece; los coches son antiguos y están descuidados. Nada que no sepa el usuario.

 

También afecta al servicio, de modo externo, la situación de la infraestructura urbana en general y la inseguridad. Es decir, a la falta de recursos se suman factores que llevan a un transporte de pasajeros que día a día pierde usuarios, en lugar de ganarlos, profundizando aún más el desequilibrio económico.

 

Tampoco podemos dejar de observar que la pandemia modificó, de forma obligada en muchos casos, la forma de movernos y claramente esto también afectó al transporte por colectivos, disminuyendo considerablemente el corte de boletos diarios.

 

En cuanto a la ordenanza Nº 11580 del año 2009, vigente en la actualidad, pretendió hacer frente a la situación que se vivía en aquel momento, intentó resolver los problemas del servicio, sin vincularlos con la situación contractual con las empresas, que sigue siendo precaria. Con sus altos (pocos) y bajos (muchos), este modelo se sostuvo, desembocando en la situación actual.

 

El dilema que se nos presenta hoy es si la licitación es una solución al problema que tenemos con el transporte por colectivos. En lo particular, mucho tiempo sostuve que más que un problema de licitación, teníamos otros problemas: falta de control (el Órgano creado por la ordenanza no se consolidó, ni fortaleció de manera de llevar adelante un control más efectivo); los prestadores no actuaron responsablemente, quizás motivados o amparados, en la situación oligopólica en la que se mueven; y la mencionada inequidad en los subsidios y sus mecanismos de distribución, entre otros.

 

Como tantos otros aspectos de nuestra vida, la pandemia, impactó de manera contundente y definitiva en el sistema, los que nos pide modificaciones profundas y, como dije en otras oportunidades, que alumbren un nuevo sistema de transporte para la ciudad.

 

Es allí donde la licitación deberá aportar un nuevo transporte por colectivos para la ciudad, que será el que se "pueda tener". A la actual situación, licitación mediante, debemos transformarla en una oportunidad, no solo pensando en la regulación del sistema de transporte público, sino también en su transformación y en todas las formas de movilidad.

 

Una licitación puede avanzar en resolver el problema del transporte por colectivos si contempla: la multimodalidad, pensando en infraestructura urbana que, ubicada en lugares estratégicos, dé resguardo a otros medios de movilidad particular (autos, bicis, monopatines, etc.) y a partir de allí se concentren las salidas de colectivos. También se debe tener en cuenta colectivos con posibilidad de traslado de alguno de esos medios de movilidad; y a la inversa, que en otros puntos estratégicos de los recorridos puedan establecerse sistemas públicos o privados de medios de movilidad alternativos, como así también, establecer, como política permanente y de recuperación de usuarios, corredores seguros para llegar a dichos puntos.

 

Además, debe considerar subsidios diferenciados según la Línea de que se trate, equiparando con ello las Líneas menos rentables con aquellas que lo son. A partir de esta lógica, avanzar definitivamente en el establecimiento de líneas con circulación oeste-este en diferentes puntos de nuestra ciudad. ¿Por qué no contemplar la existencia de una línea testigo municipal? Otro punto a atender son los boletos diferenciados; que las frecuencias sean reales y que se pueda realizar el seguimiento de las unidades en tiempo real mediante la aplicación "Cuándo Pasa?", optimizando las condiciones de seguridad del usuario.

 

La normativa debe aspirar a la transformación de las garitas en centros de servicios, que las unidades estén limpias, cuenten con servicio de wi-fi, aire acondicionado, botón de pánico, GPS, que sean de funcionamiento sustentable. En tanto debe considerarse la capacitación permanente de los choferes a los fines que conduzcan adecuadamente, facilitando la convivencia vial en las calles, que dispensen buenos tratos a los pasajeros, entre otros criterios a atender.

 

Este encuadre normativo es primordial que esté acompañado del compromiso del Estado municipal, reflejado presupuestariamente en obras que mejoren la infraestructura vial; generen los puntos de transferencia multimodal; y potencien otros medios de movilidad a través del aumento y mejora de la red de ciclovías, de espacios peatonales de calidad con la incorporación de nuevos conceptos como el de "ciudad calma" y "supermanzanas".

 

Lo expresado anteriormente es conocido, hay experiencias, es posible y realizable. Se debe tener la valentía de encarar esta profunda crisis en que la pandemia puso al sistema de transporte por colectivos con una mirada inteligente, inclusiva, integral y audaz; pero también, invitando a la ciudad a soñar que podemos tener un sistema mejor, que quizás no sea el ideal, pero que la licitación, planteada con esta mirada que propongo, puede ser la base para tener los colectivos que los santafesinos queremos. Solo así, la licitación será la solución.

 

La licitación deberá aportar un nuevo transporte por colectivos para la ciudad. Debemos transformar la actual situación en una oportunidad, pensando en la regulación, en la transformación y en las otras formas de movilidad.

 

La normativa debe aspirar a la transformación de las garitas en centros de servicios, que las unidades estén limpias, cuenten con servicio de wi-fi, aire acondicionado, botón de pánico, GPS, que sean de funcionamiento sustentable. 

Noticia de: El Litoral (www.ellitoral.com) \[Link:[https://www.ellitoral.com/index.php/id_um/342863-colectivos-la-licitacion-es-la-solucion-tribuna-politica-opinion.html?origen=mobile](https://www.ellitoral.com/index.php/id_um/342863-colectivos-la-licitacion-es-la-solucion-tribuna-politica-opinion.html?origen=mobile "https://www.ellitoral.com/index.php/id_um/342863-colectivos-la-licitacion-es-la-solucion-tribuna-politica-opinion.html?origen=mobile")\]