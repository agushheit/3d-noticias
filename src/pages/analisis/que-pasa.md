---
layout: analisis
category: analisis
date: 2021-01-08T11:14:55Z
author: Por Sofía Gioja
title: "¿QUÉ PASA? "
thumbnail: https://assets.3dnoticias.com.ar/Gioja.webp

---
**<span style="font-family: Helvetica, sans-serif; font-size: 1em; font-weight: bold">Zapping de la primera semana del año 2021. Un montón de cosas, superpuestas, que pasaron, pasan o pasarán y que tal vez ya sabías. O no...</span>**

<br/>

![](https://assets.3dnoticias.com.ar/080121-meme-simpsons.webp)

<br/>

Así es, mis queridos lectores, en muy poco tiempo, pasaron muchas cosas. A algunos les pasaron cosas y a otros, las cosas les pasaron (por delante) como el cole de la línea 2, cuando va lleno al mediodía, en enero, y no para. O como la navidad misma, o ese auto que pasó barrenado, en la correntada en Villa Gesell, después del temporal del 6 de enero.

O como el raid que protagonizó Carolina Píparo que pasó _c@#*do_ con su auto, frente a una cámara de seguridad, con una moto y sus ocupantes enganchados en el guardabarros delantero. Esas chispas sí podían verse.

Más arriba, en EE. UU. estuvieron reventando a patadas el Capitolio:  los _cosplay_ que mandó Trump se pasaron de la raya y la repudrieron. Hablando de eso, en Claromecó los pibes también se zarparon feo y ahora los responsables se pasan la pelota, así como el Covid sigue pasando de gente en gente.

Ya pasó un año y es como que no pasó nada. Seguimos igual de bestias. «Cambiar, no cambió nada. Llegando hasta donde hoy llegué. Todo está como estaba. Todo está igual de bien», canta el de _Viejas Locas_.

¿A ver qué le pediste a los reyes? ¿Que abdiquen? No, así no van a pasar, mamita. No, por casa no pasaron tampoco (y eso que me olvidé la ensalada de rúcula afuera y un vaso con un culito de _Terma_ limón). Y hablando de cosas que no pasan, todavía no cobré diciembre. Igual no creo que pase...

Hablando de sueldos, resulta que Victoria Donda fue denunciada, el viernes pasado, porque la acusaron de haberle ofrecido un resarcimiento a su ex empleada con un plan social y un cargo en el Estado, según dice en la copia de esa denuncia. Si pasaba, pasaba… mala tuya, Vicky.

«Como los magos, dejemos que la luz de Cristo nos guíe», dijo el papa Francisco, o eso escuché en la tele mientras me lavaba los dientes. Eso pasó cuando volvió la luz a las 5:30 de la mañana. Me tuve que reír. Si, la EPE «me cortó toda la loooz» y no me pude dormir más. Esto va a seguir pasando. Anotalo.

La máxima para hoy es de 31°, va a llover para mi cumpleaños y ahora parece que, si te pasás de las 23 horas y te encuentran en la calle, se te complica. Ya no te dejan pasar una. El presidente habló con los gobernadores de cada provincia y todos se pusieron de acuerdo para que, de once de la noche a seis de la mañana no pase nada, de nada.

Otra cosa que pasa, que en realidad no pasa, es la impaciencia de los pescadores con el temita de la veda. No pasan los vehículos por el puente Rosario Victoria y tampoco pasa nada con las negociaciones.

¿Querés que te cuente lo que pasa en _Cobra Kai_? ¡Bueno, no! _Master Chef celebrity_, entonces. Lo que pasó es que Belu Lucius se burló de la Vicky Xipolitakis por un vestido de látex rosa. No, a mí tampoco me importa. Pasemos a otro tema mejor.

«Si hay que darse la vacuna, me doy la rusa, sin dudarlo», dijo Natalia Oreiro. Comentario que no pasó desapercibido en medio de la controversia generada por una ola de desinformación, devenida en un inminente desembarco comunista. Ahora, francamente, yo no sé qué pasó en el país de Putin para que le den tanta cabida a la Oreiro… posta, no sé qué pasó.

Nadie sabe, tampoco, qué pasó con las 400 dosis de la Sputnik vencidas. Resulta que una bocha de dosis de la vacuna contra el coronavirus tuvieron que ser descartadas por perder la cadena de frío, según informaron los de salud. Ahora sí, todos se lavan bien las manos. No voy a explicar el chiste.

Atención cabaleros: el 2021, para el horóscopo chino, es el año del buey de oro; los números de la suerte serán el 1 y el 4 (en todas las combinaciones), se recomiendan los colores blanco, amarillo, gris, plata y verde, y decorar con flores como el tulipán, gloria de la mañana (también conocida como manto de María, Diego de día o campanilla morada) y la flor de melocotón. Por el contrario, se aconseja evitar el azul y los números 5 y 6. ¿Qué te pasó Ludovica Squirru? Antes eras _chévere_...

El miércoles, pasaron «Sully, hazaña en el Hudson», otra vez. Justo lo enganché después de saber que el gobierno está evaluando la posibilidad de cerrar el Aeropuerto de Ezeiza para vuelos internacionales a partir de la segunda mitad de enero, para que no pase el _cobicho_. ¿Qué va a pasar al final?

En Europa todavía no pueden frenar los contagios de covid y tampoco se cumplen los plazos de vacunación. Se cuestiona bastante la gestión de la Unión Europea. Se habla de falta de previsión y estrategia errónea. Podía pasar… Era una posibilidad.

Finalmente, en lo que pasó a llamarse un «Brexit amistoso», la cuestionada Unión Europea le firmó los papeles de divorcio al Reino Unido. Sin penas ni glorias. ¿No pasó nada? ¿Qué onda UE?

Bueno gente, así pasó el debut, la primera semana de vida, del tan ansiado año nuevo. ¿No sienten como que pasó de todo, pero no pasó nada? ¿No es raro?

Quién lo diría… 2021. Acá lo tenés. Bueno, sigamos.

<br/>

<hr style="border:2px solid red"> </hr>

**Sofía Gioja en** [**twitter**](https://twitter.com/SofiG83)

Otras columnas de la autora en este portal:

[La odisea del regalo](https://3dnoticias.com.ar/analisis/la-odisea-del-regalo/) | [La culpa no es del chancho](https://3dnoticias.com.ar/analisis/la-culpa-no-es-del-chancho.algunas-reflexiones-sobre-el-consumo-ironico/) | [Como paquetes](https://3dnoticias.com.ar/analisis/como-paquetes/) | [Trátame suavemente](https://3dnoticias.com.ar/analisis/tr%C3%A1tame-suavemente/) | [Hasta que se metieron con el pan dulce](https://3dnoticias.com.ar/analisis/hasta-que-se-metieron-con-el-pan-dulce/) | [Los antivacunas ¿a qué se oponen?](https://3dnoticias.com.ar/analisis/los-antivacunas-%C2%BFa-qu%C3%A9-se-oponen-en-realidad/) | [Todo lo que no se hizo](https://3dnoticias.com.ar/analisis/todo-lo-que-no-se-hizo/)