---
layout: analisis
author: 'Por Matías Quintana '
category: analisis
title: Por qué se necesita desarrollar el tramo centro-norte de la hidrovía
date: 2020-10-30T19:50:12.180+00:00
thumbnail: https://assets.3dnoticias.com.ar/matias-quintana.jpg

---
La República Argentina es el país con mayor extensión sobre la Hidrovía Paraná-Paraguay, con varios puertos a disposición: desde Santa Fe al océano plenamente activos (San Lorenzo, Rosario, etc.) y desde Santa Fe al norte casi en desuso (Reconquista, Villa Ocampo, Barranqueras, etc.).

En la Cámara de Comercio Exterior de Santa Fe impulsamos la necesidad estratégica de integrar los puertos desde Santa Fe a Confluencia a través de una línea troncal de barcazas de diseño, denominadas “camión fluvial”, preparadas para auto descargarse, navegar en cualquier profundidad del río y gestionada por uno o más operadores portuarios worldclass, interesados en desarrollar el tramo centro-norte de la hidrovía bajo criterios de transparencia y compliance internacional.

Esta red troncal, análoga a las que funcionan en otras vías navegables del mundo como los ríos Yangtsé en China, Mississippi y Missouri en Estados Unidos o Rhin en Europa, aportará a la Argentina un eslabón indispensable para lograr la comodalidad, en la que el resultado final de la ecuación logística surge de la eficiencia del sistema en su conjunto y no de sus eslabones costeados individualmente. Esto se logra integrando solidariamente todos los medios de transporte: camión para las medias y cortas distancias, y trenes, aviones y barcos para las largas, para brindar un transporte especializado en conectar al productor con sus clientes.

La conexión de las regiones del centro-norte argentino, actualmente sin acceso a puertos de cercanía, con una red troncal de esta naturaleza, independiente del dragado y con embarcaciones altamente eficientes que se pueden construir en astilleros locales, potenciará el desarrollo de extensas áreas productivas desaprovechadas, contribuyendo a la eficiencia logística a una décima del costo económico y ambiental frente a cualquier otro medio utilizado aisladamente. Este concepto es una base práctica para amalgamar en un mismo paradigma logístico a pequeños y medianos puertos del centro-norte que, para existir, en vez de competir, tienen que cooperar en la captación de un enorme volumen de carga de contenedor es que actualmente viaja dispersa.

Para plasmar esta idea es primordial que el Estado oficie como facilitador y garante, y que operadores portuarios de nivel internacional tengan la visión y el compromiso ético de asociarse para modernizar al sistema logístico argentino a través de una flota de río interno que se posicione a mediano plazo entre las principales del mundo.

***

##### **Matías Quintana |** Director de la Cámara de Comercio Exterior de Santa Fe