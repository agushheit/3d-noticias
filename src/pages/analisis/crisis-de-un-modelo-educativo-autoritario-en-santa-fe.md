---
layout: analisis
category: analisis
date: 2022-11-22T06:15:09-03:00
author: Rafael Eduardo Micheletti, Docente y director de nivel secundario, forma parte
  del equipo de investigadores externos de Fundación Libertad e integra la asociación
  civil Docentes por la Educación.
title: "¿Crisis de un modelo educativo autoritario en Santa Fe?"
thumbnail: https://assets.3dnoticias.com.ar/escuela.jpg

---
Publicado  en el Diario El Litoral.

En los últimos años, Santa Fe ha implementado de forma intensiva un modelo educativo facilista sin precedentes, que ha arrasado con todos los indicadores de calidad educativa. El aprendizaje se ha vuelto una rareza, mientras la desigualdad educativa aumentó. El fracaso es tan estrepitoso como obstinado.

Las camadas llegan cada vez peores. El drama no está dado solo por los saberes que no se incorporaron, sino especialmente por los hábitos y competencias que no se desarrollaron (desde la capacidad de sentarse a estudiar hasta la de afrontar emocionalmente la presión de un examen o comprender una consigna).

Durante la pandemia, este modelo fue muy criticado, pero acatado. La excepcionalidad e incertidumbre de la debacle sanitaria elevó el umbral de tolerancia de la sociedad. Además, estaba la expectativa de que, finalizada la emergencia, algunos de los errores cometidos empezaran a corregirse.

Sin embargo, en noviembre del corriente, a poco de finalizar el año (como es costumbre de esta gestión), aparecieron una serie de directivas que hicieron añicos toda pretensión o expectativa de cierta normalidad y razonabilidad. En particular, la Circular 04/22 de la subsecretaría de Educación Secundaria, vuelve a imponer el facilismo extremo: crea "trayectos" que son combos de materias (aprobando una, te llevás otras gratis); abole las mesas de examen; flexibiliza y reduce la evaluación a los saberes mínimos; establece la aprobación por medio de "trabajos prácticos"; y habilita una promoción cuasi-automática (se avanza de año acumulando gran cantidad de materias adeudadas y se indica un trabajo interdisciplinario flexible, ajustado a las necesidades del alumno, para quienes terminan el nivel).

La prueba incontrastable de todo esto la ha suministrado la propia ministra. Consiste en la única estadística que a ella le importa y que ha mencionado: la baja de la repitencia. Se han tirado por la borda muchos años de trabajo de hormiga buscando bajar progresivamente la repitencia aumentando el aprendizaje. De repente, se bajó totalmente la vara del aprendizaje para que todos eventualmente pasen de año y egresen aunque no hayan aprendido. Otras estadísticas lo confirman. Según las últimas pruebas Aprender, Santa Fe ha descendido estrepitosamente en Lengua y Matemática y es una de las provincias con mayor desigualdad o brecha educativa.

Ante esto, la frustración de docentes, familias y directivos ha sido mayúscula. Se aceleró un proceso de autoorganización y resistencia de la sociedad civil que venía gestándose de forma lenta y paulatina. Padres por la Educación, Docentes por la Educación, sindicatos, directivos autoconvocados, etc., han coincidido en movilizarse para ponerle freno a la destrucción de cultura, valores y competencias que sigue sucediéndose. Prosperaron juntadas de firmas variadas, recursos administrativos, pedidos de audiencia, proyectos legislativos, pedidos de renuncia, etc.

A diferencia de lo que pasó durante la pandemia, hoy en día ya no hay excusas ante tanto facilismo y destrucción educativa. Ha quedado en evidencia cuál es la verdadera intencionalidad de esta gestión. Se han transparentado el móvil ideológico y el pensamiento dogmático. Lo único que importa es luchar contra la meritocracia. Eso ha pasado a ser un fin en sí mismo. No importan los resultados prácticos. Ni siquiera los conmueve que los más perjudicados sean los más humildes.

En este proceso de dogmatismo, prescindencia de la evidencia empírica, ausencia de diálogo e insistencia en el error, hay de trasfondo una cosmovisión autoritaria, propia del fanatismo. Y ello se ve también reflejado en un estilo autoritario de gestión ministerial.

> A diferencia de lo que pasó durante la pandemia, hoy en día ya no hay excusas ante tanto facilismo y destrucción educativa. Ha quedado en evidencia cuál es la verdadera intencionalidad de esta gestión.

Desde la asunción de Adriana Cantero al frente del Ministerio de Educación, se dejaron de publicar los anuarios de estadísticas educativas; escaló a niveles obscenos el adoctrinamiento ideológico; se alteró la jerarquía normativa y se destruyó la seguridad jurídica; se centralizó el poder, afectando la autonomía escolar; se soslayaron los concursos públicos; se organizaron elecciones con lista única para cuerpos colegiados; y se pretendió intervenir de facto en el sindicato docente, conforme ha sido denunciado. El gobernador mira para otro lado, como si no hubiera nada para hacer a su alcance. La ministra pone su energía en recorridos y visitas de carácter político.

Se combinan, entonces, el autoritarismo ideológico con el autoritarismo de gestión. Se construye un poder autoritario para imponer una utopía autoritaria a la fuerza, como sea. Los docentes y directivos están sobrepasados, vaciados de autoridad, exhaustos, mientras los alumnos avanzan por el sistema educativo y egresan sin necesidad de desarrollar competencias básicas, condenados de por vida.

Los propios alumnos se dan cuenta de que en nuestro sistema educativo todo da lo mismo; de que se gesta un entorno de anomia e impunidad que a largo plazo los perjudica, pero en el corto plazo los introduce en una vorágine facilista de la cual es muy difícil salir. Ya le "sacaron la ficha" al sistema: "¿Para qué voy a estudiar si después apruebo con un trabajito?", se le escucha decir a más de uno.

Es muy grande el daño que se ha hecho. Y salir de este pozo va a ser difícil. Llevará tiempo. Los hábitos y las competencias no se desarrollan de un día para el otro. Son un proceso delicado y artesanal que exige cierta complementación entre ministerio, escuela y familia, así como reglas claras e incentivos adecuados.

Se debe trazar un plan a largo plazo, progresivo, que empiece desde los grados inferiores de primaria. Es preciso generar inclusión con aprendizaje, y entender que no hay una oposición entre ambos conceptos. Lo muestra el sistema finlandés, tan elogiado, con extrema exigencia y extrema inclusión. Nadie se cae del sistema, siempre hay nuevas oportunidades, pero a nadie se le miente y a todos se les exige. Se incluye dando oportunidades y herramientas, no regalando materias, contenidos y títulos vaciados de sustancia y sentido.

La empecinada dictadura mental del igualitarismo mal entendido y la antimeritocracia va a seguir destruyendo la educación si la sociedad no la frena. Contra esto se rebela la comunidad educativa, en buena hora. ¿Será esta la crisis terminal y definitiva de este modelo educativo autoritario?

Debemos reemplazar dogmatismo por sentido común; utopía por idealismo; facilismo por mérito; vaciamiento y desgano por incentivo; cultura de la anomia por cultura del esfuerzo; prepotencia por respeto; igualitarismo por justicia; permisividad por igualdad de oportunidades; mentira por verdad; prepotencia por diálogo; demagogia por educación.