---
layout: analisis
category: analisis
date: 2020-12-09T11:57:15Z
author: Por José Villagrán
title: 'Un año de Perotti Gobernador: pandemia, disputas internas y un gobierno sin
  articulación'
thumbnail: https://assets.3dnoticias.com.ar/Villagran.webp

---
Este 10 de diciembre Omar Perotti cumplirá un año como gobernador de la provincia de Santa Fe. Con el Covid 19 en el medio, la gestión presentó dificultades para articular acciones políticas y también en el territorio. En la Legislatura, relación tensa con propios y extraños.

<br/>

![](https://assets.3dnoticias.com.ar/perotti.jpg)

<br/>

Hace 365 días que Omar Perotti ejerce la gobernación de la provincia invencible de Santa Fe. Pero eso no es todo. Desde hace 365 días el peronismo ejerce el poder oficialmente en la provincia después de 12 años de gestión socialista. Todo esto para decir que, por primera vez en más de una década, el justicialismo dejó su lugar de oposición para ocupar el lugar de oficialismo. Y como todo cambio, a este gobierno peronista le trajo complicaciones de gestión el amoldarse a un nuevo rol.

A este escenario hay que agregarle un evento mundial que no estaba en los planes de nadie: la pandemia por el Covid 19. La aparición del virus cambió la vida cotidiana de las personas e influyó en este primer año de administración perottista.

El 13 de noviembre, en una [nota publicada en _3DNoticias_](https://3dnoticias.com.ar/analisis/el-gabinete-de-perotti-un-equipo-rodeado-de-rumores "El gabinete de Perotti") dábamos cuenta de lo que decían algunos funcionarios del Ejecutivo:  “La pandemia nos cambió el enfoque de gobierno. Nos tuvimos que adecuar a una realidad que no teníamos pensada y rearmarnos en esta realidad”.

A decir verdad, la gestión comenzó con tensión política antes de asumir. La transición entre el Frente Progresista y el Partido Justicialista fue la antesala de lo que sería la relación entre las dos fuerzas en este 2020. La aprobación del presupuesto para este año por la anterior conformación legislativa y realizada por el anterior gobierno, las tensas reuniones para saber cuáles eran los números de la provincia y la aprobación de leyes sociales con la que el peronismo no estaba de acuerdo, iban marcando el futuro.

### **Un gabinete que nunca se consolidó**

No habían transcurrido ni 30 días que los rumores de renuncia ya estaban a la orden. En enero surgieron dos polémicas dentro del gobierno. La primera daba cuenta de la resolución del hasta entonces Jefe de Policía Victor Sarnaglia que habilitaba los agentes podrán tener una bala en la recámara de sus armas preparadas para disparar. La respuesta de la secretaría de Derechos Humanos no se hizo esperar. “La mano dura no sirve” escribió en su cuenta de twitter Lucila Puyol.

Al mismo tiempo, la “mala onda” entre el jefe policial y el ministro de Seguridad, Marcelo Saín, comenzaba a escribir las primeras líneas de una tensa relación.

**La primera dimisión que enfrentó Perotti dentro de su gobierno fue la de Carlos Parola al frente del Ministerio de Salud**.

El primer caso de contagio por Covid 19 en la provincia se dio el 14 de marzo, seis días antes que el gobierno nacional impusiera la cuarentena obligatoria.

**En los primeros instantes de la pandemia, el gobernador y el ministro eran los encargados de dar los partes sanitarios, uno a la mañana y uno a la tarde**. Hasta ahí, se podía observar a un ministro activo, que se encargaba de preparar el sistema de salud (con la llegada de respiradores y el armado de camas críticas) para que Santa Fe pueda resistir la llegada del Coronavirus.

**Pero todo eso se fue diluyendo**. Estructuras del anterior gobierno dentro del ministerio, sumado a la relación distante que se producía con Perotti y el desgaste que le llevaba ocupar el puesto, hicieron que los rumores de renuncia se hagan cada vez más fuertes, algo que finalmente sucedió el 10 de junio de este año.

**La segunda renuncia no está relacionada con el gabinete propiamente dicho, pero sí con una figura y cargo de peso como es la Jefatura de la Policía de la provincia**. A Sarnaglia lo había elegido personalmente el propio gobernador antes de asumir. A Saín la llegada de un policía retirado no le simpatizó mucho y más si la elección del mismo no pasó por sus manos.

La relación Saín – Sarnaglia, que contuvo reclamos salariales, filtraciones de audios y operaciones políticas que terminaron con la ida del jefe policial a mediados de año. El propio jefe de policía se enteró por una resolución ministerial que había sido desplazado de su puesto y que su reemplazo ya estaba designado.

**Los hechos se precipitaron cuando Sarnaglia fue citado a declarar como testigo en el marco de la causa por juego clandestino que se tramita en la ciudad de Rosario**.

Los fiscales lo convocaron a partir de que, en uno de los escritos del empresario del sector, Leonardo Peitti, apareciera escrita la expresión "Sarna", en alusión a una persona a la que se le debía pagar una suma de dinero por mes a cambio de protección. Esa suerte de abreviatura fue asociado directamente como diminutivo de Sarnaglia; de allí la citación.

Finalmente, se presentó en la oficina del gobernador el lunes 31 de agosto. Estuvieron reunidos aproximadamente 40 minutos y le presentó la renuncia. Durante ese fin de semana ya se había despedido de sus subalternos. Le expresó a Omar Perotti que su decisión se debía a “cierta incompatibilidad de caracteres con ciertos funcionarios”. Al mismo tiempo, Marcelo Saín expresaba en un acto público: “Sarnaglia ya fue”.

**La tercera renuncia fue la de Esteban Borgonovo al cargo del ministerio de Gobierno, Justicia y Derechos Humanos**. La misma se dio días después de que el gobernador anunciara que estaba pensando cambios dentro del gabinete, puesto que lo había pensado para una realidad que la pandemia modificó con claridad.

“Siento que en este ministerio (de Gobierno) es imposible trabajar sin un respaldo firme y explícito del gobernador. Es una tarea delicada y continua”. Así dejaba entrever que el respaldo del gobernador ya no estaba en el mismo lugar que cuando asumió.

**A partir de allí, la realidad es que los rumores de más cambios se acentuaron con el correr de los días**. No hubo un solo ministerio que se salvara de esas dudas de permanencia que el gobernador había sembrado. Además, la ida de Borgonovo fue un golpe para el espacio que integra la vicegobernadora Alejandra Rodenas, Nuevo Espacio Santafesino, de dónde provenía el ex ministro.

<br/>

### **La Legislatura, una relación tensa con propios y extraños**

<br/>

![](https://assets.3dnoticias.com.ar/legislatura.jpg)

La conformación de ambas cámaras de la Leguislatura fue un dolor de cabeza desde el comienzo para Omar Perotti. En diputados, la oposición del Frente Progresista obtenía no solo el control de la cámara sino además la presidencia en manos del gobernador saliente Miguel Lifschitz.

Y es justo afirmar que, así como el peronismo retornó desacostumbrado al poder tras 12 años, el socialismo volvió a ser oposición tras el mismo período de tiempo y mostrando una necesidad querer cogobernar.

**La primera discusión se dio en la transición**. La relación siguió ascendiendo en su tensión cuando el gobierno envió la Ley de Necesidad Pública, que en un primer momento el socialismo “volteó” en Diputados. Al mismo tiempo, tuvo que negociar con senadores de su mismo partido, pero distinta conformación política para que el bloque pueda mantenerse unido. Finalmente, la ley salió, pero recién con la crisis del coronavirus en marcha.

**Los reclamos de mayor diálogo con el Ejecutivo fueron constantes**, desde un sector del oficialismo y, obviamente, desde la oposición.

Algunos diputados manifestaron poder acoger a aquellos senadores que “parecen tener mayor afinidad con el socialismo que con el propio gobierno". Las palabras expresadas _off the record_ fueron “ensamblar” y “acoplar”. Casi toda una definición de lo que le está faltando al gobierno. Las dificultades no solo están en la articulación dentro del propio Ejecutivo, sino también afuera, con los demás poderes.

<span style="color: red;">**Perotti no logra cimentar un plan de gobierno y en legislatura no logró conseguir adeptos que puedan defender su gestión**.</span>

En Diputados no cuenta con los números suficientes, y en el Senado tiene una mayoría frágil, que amenaza con romperse en cualquier momento. Senadores de su propio riñón ya habrían dado ese paso, sino fuese porque la política conlleva mucho más que emociones del momento.

Lo sucedido antes de que finalice el periodo ordinario de sesiones con las ya mencionadas “leyes anti Saín” fueron un claro ejemplo. Ministros del gobierno manifestándose no a favor del ministro, sino en contra de las maniobras realizadas para perjudicar a alguien del Ejecutivo.

“Hay muy poco diálogo entre la Legislatura y el Ejecutivo y pasan estas cosas”, dijo el senador por el departamento San Lorenzo, Armando Traferri, el apuntado por los funcionarios de la Casa Gris como el responsable de las divisiones dentro del peronismo.

<br/>

![](https://assets.3dnoticias.com.ar/traferri.jpg "Traferri")

<br/>

Y es algo entendible desde ese punto de vista. A mediados de noviembre se produjo la renuncia del ministro de Gobierno, quien era el encargado de negociar desde el gobierno los acuerdos con la Legislatura. En su lugar asumió provisoriamente el ministro de Gestión Pública, Ruben Michlig, alguien que está más al tanto de las cuestiones territoriales que de la rosca política.

<span style="color: #cf0000;">**En este escenario, es muy difícil que leyes importantes que tiene pensado el gobierno provincial puedan ser votadas afirmativamente.**</span>

El presupuesto fue aprobado, pero con reparos y muy fuertes a la hora de la votación.

Ni la ley de seguridad ni la de conectividad (que es llevada como bandera de gestión) van a pasar el filtro de diputados.

<br/>

### **Un verano ¿para bajar la temperatura?**

Se dice que los cambios que faltan por realizarse en el gabinete, Perotti los haría a fines de diciembre con el objetivo de que se pierdan entre los festejos de fin de año y así evitar el impacto político.

Una vez definido este paso, el siguiente será buscar la ampliación del espacio e ir más allá de las pocas personas que asesora al gobernador para que ejecute las acciones. “Un espacio que ensanche las posibilidades de gestión” confió una fuente a 3DNoticias.

El 13 de noviembre, consumada la renuncia de Borgonovo, [en este portal dábamos cuenta del tipo de gabinete que pensaba Perotti](https://3dnoticias.com.ar/analisis/el-gabinete-de-perotti-un-equipo-rodeado-de-rumores/ "El gabinete de Perotti") para encarar su segundo mandato como gobernador de la provincia.

En los últimos meses, escribíamos, le pidió a la juventud que se defienda el accionar del Ejecutivo. Y que lo propio deben hacer sus ministros. Está convencido de que debe acercarse “más a la calle”. Y para lograr eso, piensa que a la gestión debe darle oxígeno.

“A Omar no le gusta que la oposición le ande marcando la cancha ni le imponga agenda”, nos dijo otra fuente del gobierno en su momento. Piensa que debe mejorar la relación con la oposición más dura a su gobierno, que ha sido marcada por fuertes cruces que contrarrestan su forma de hacer política.

Pero, además, cree que debe cerrar “filas adentro”. Acercar posiciones con distintos sectores dentro del peronismo (como La Cámpora) y apoyarse en los sectores que ya le juraron “lealtad hasta el último día de gestión” (como lo hizo el ministro de Defensa de la Nación Agustín Rossi).

**El 2020 ya es cosa juzgada**, inclusive para el gobierno de Perotti que, en su primer año, se vio envuelto en discusiones políticas con los de adentro y también con los de afuera y le dedicó poco tiempo a la gestión.

<hr style="border:2px solid red"> </hr>

<span style="color: #25282C;">**El gobierno provincial no tiene logros ni para defender ni para mostrar. Y así, es muy complicado sostenerse en una gestión que, además, tiene sus propias contradicciones.**</span>

<hr style="border:2px solid red"> </hr>

**José Villagrán en** [**twitter**](https://twitter.com/joosevillagran)

Otras columnas del autor en este portal:

[El presupuesto 2021 de Omar Perotti](https://3dnoticias.com.ar/analisis/el-presupuesto-2021-de-omar-perotti-que-dice-como-fue-votado-y-cuales-son-las-criticas/ "El presupuesto 2021 de Omar Perotti") | [Todos contra Saín](https://3dnoticias.com.ar/analisis/todos-contra-sain-el-ministro-que-resiste-los-embates-de-propios-y-extranos/ "Todos contra Sain") | [El gabinete de Perotti: un equipo rodeado de rumores](https://3dnoticias.com.ar/analisis/el-gabinete-de-perotti-un-equipo-rodeado-de-rumores/ "El gabinete de Perotti: un equipo rodeado de rumores") | [Que no se den PASOS en falso](https://3dnoticias.com.ar/analisis/que-no-se-den-pasos-en-falso/ "Que no se den PASOS en falso")