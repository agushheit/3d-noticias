---
layout: analisis
category: analisis
date: 2020-12-15T13:02:08Z
author: ENTREVISTA | Álvaro De Lamadrid
title: Argentina no necesita una reforma judicial, sino una reforma sindical
thumbnail: https://assets.3dnoticias.com.ar/de-la-madrid.webp

---
**El cierre del aeropuerto de El Palomar dejó claro que en la Argentina el peso sindical es mucho más fuerte que la necesidad del pueblo argentino**, o al menos es lo que piensa parte de la dirigencia opositora, que tras la decisión del Gobierno nacional de cerrar dicho aeropuerto, han salido a rechazar categóricamente esta actitud del oficialismo.

Justamente, el diputado nacional Álvaro De Lamadrid presentó un ambicioso proyecto de ley en el Congreso con el fin de «modernizar y democratizar» el sindicalismo en el país, ya que para el legislador nacional «Argentina no necesita una reforma judicial, sino una reforma sindical», dijo De Lamadrid a NCN.

La decisión del Gobierno de Alberto Fernández de cerrar el aeropuerto de El Palomar ha sido repudiada en las últimas horas por todo el arco político opositor del país. Personas como el propio expresidente de la nación se volcaron en contra de esta medida y lamentó que el oficialismo «no haya vuelto mejor».

Sin embargo, este es solo uno de los cientos de problemas que atraviesa el país, que está marcado negativamente por «las mafias y los monopolios del sindicalismo», o al menos es la opinión del diputado nacional Álvaro De Lamadrid, que en entrevista con NCN lamentó que Pablo Biró, titular de la Asociación de Pilotos de Líneas Aéreas, haya incidido en la decisión.

A juicio del diputado nacional, Biró «quería un monopolio aerocomercial y ganó otra vez el sindicalismo. Cerraron un aeropuerto, dejaron gente sin trabajo, dejaron de conectar puntos importantes del país, simplemente para que tenga más poder Biró y su monopolio con Aerolíneas Argentinas».

Justamente, y con el cierre de El Palomar como noticia, en días pasados el diputado del PRO introdujo en el Congreso de la nación un proyecto ambicioso que busca terminar con las mafias sindicales, o al menos ese es el objetivo de Álvaro De Lamadrid, a quien lo acompañaron en su iniciativa más de treinta legisladores nacionales.

«Este es un proyecto de modernización, democratización y transparencia sindical. Los sindicatos hay que reformarlos, no puede ser que los sindicalistas sean los que mandan hoy en la Argentina, deciden si se puede viajar, volar, circular, incluso si se puede estudiar, y que ya no representan a los trabajadores, sino que son castas que están desde hace 20 o 50 años manejando los sindicatos», opinó el legislador de Juntos por el Cambio.

«Hoy en Argentina es más fácil ser Presidente que ganar la presidencia de una asociación sindical», sentenció De Lamadrid.

En ese sentido, el diputado consideró que la Argentina «no necesita una reforma judicial como la que quiere hacer Cristina para convertirse ella en la Justicia, sino que necesita una reforma sindical para que podamos trabajar en paz, producir en paz, exportar en paz, para que los sindicatos no expulsen inversiones y cierren empresas como pasó con Mercado Libre y tantas otras y que en definitiva, este sindicalismo lo único que hace es atrasar al país y generar que no vengan inversiones y no se pueda producir», sentenció el diputado.

Además, el legislador de la UCR consideró que **otro punto negativo de los sindicatos es que «no defienden a los trabajadores, lo que hay es una obligación compulsiva de afiliación»**, sentenció.

<span style="font-family: helvetica; font-size: 1.25em; font-weight: 600;"> **Qué dice el proyecto**</span>

📰 [Lee el proyecto completo](https://assets.3dnoticias.com.ar/PROYECTO-DE-LEY-DEMOCRATIZACION-SINDICATOS.pdf "PDF")

Entre los puntos más sobresalientes, estipula **reglas de transparencia respecto a las elecciones**. Por ejemplo, se acorta el mandato a 3 años, se establece que se puede postular a la reelección una sola vez luego de un mandato cumplido. Además, prohíbe vínculos familiares respecto a sucesores en candidaturas (hijos, madres, esposas).

Otro punto interesante es que el proyecto estipula que **los sindicalistas deben presentar su declaración jurada**, además de brindar información a cualquier persona respecto a la Ley de información pública. A su vez, **no podrán ser candidatos quienes tienen procesos judiciales abiertos**, entre otras cosas.

<br/>

<hr style="border:2px solid red"> </hr>

**Álvaro de Lamadrid |** Diputado Nacional UCR - CABA.