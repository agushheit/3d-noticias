---
layout: analisis
category: analisis
date: 2020-12-06T12:35:23.000+00:00
author: Por Minu Bart
title: No me inviten
thumbnail: https://assets.3dnoticias.com.ar/minu.webp

---
No me inviten, no es la edad, no es la economía, no es la “urañez” reinante, no es la comodidad… se llama "practicidad". Se dice: "no me rompas".

No me invites al bautismo, si no soy familiar y no participo directamente en la ceremonia del próximo santificado bajo la fe cristiana. NO ME INVITES.

Creo fervientemente que es una venganza de los curas, que no disfrutan el fin de semana y se empeñan en cortarte el disfrute.

La hora clave son las CINCO de la tarde del sábado, ¡SABADO! Me cortás el partido, el tercer tiempo, la cortada de yuyos, el lavado del auto, los mates en la costanera, la siesta extendida del bendito sábado, mi desayuno post recupero del viernes. 

¿Entendés que estoy disfrutando de mi día sin horarios? Es el momento que tengo para estar bajo el febo directamente, el día que realizo mi práctica deportiva y despliego todas mis frustraciones de crack pensando que soy el Messi, el Mascherano o la Lucha local no descubierta a tiempo. 

**Es mi momento de libertad**. Y vos me invitás a ponerle horarios. Me invitás a bañarme, a no saber qué ponerme, a pensar en qué regalar, a morirme de calor porque el cura no puso un split, sino que tiene dos ventiladores en el atrio y uno cada veinte metros, donde lo único que mueve es la pollera larga de la tía Elena, que se hizo especialmente para la ocasión en seda fría y en los tonos que le dijeron en la sedería San Luis que "están de moda".

Los chicos lloran y con suerte se duermen. Los padres los acomodan de un brazo al otro, de la madrina al padrino (que aprende a alzarlo) o, en su defecto, en los brazos de la abuela que está ansiosa por tenerlo y ser la responsable de calmarlo. 

Los hermanitos se destacan por querer tener también su protagonismo, siendo el instante en el que uno se pregunta: ¿Y Jesús dijo: los niños vengan a mí? ¡Deberían estar AFUERA! Imposible lograr que se comporten como pequeñas momias. Así comienza el desfiladero hacia la puerta del templo… pensando que si gritan y juegan a las luchas, pero en la vereda, molestan menos o se vuelven insonoros: error. Ahí, la mitad de los asistentes miran al cura, perdidos en sus pensamientos mientras la otra mitad ojea el celular, la puerta y rompe filas. Comenzando con el desmadre y bullicio constante.

El cura, junto a algún monaguillo que tuvo la fortuna de ser elegido para acompañarlo en la ceremonia, en el mejor de los casos, si no es acompañado por la señora que cuida la iglesia y “ordena” a los asistentes. El cura comienza a recorrer los bancos, el crío en cuestión es pasado de brazos en brazos hasta llegar al extremo del banco para ser ungido con el aceite, cubierto con la manta, bendecido por el fuego sagrado que se traslada a la vela, que para esa altura el padrino ya quebró, y reacciona para tratar de mantenerla firme y no quemarse. 

Dentro de ese movimiento aparecen los fotógrafos profesionales y familiares devenidos en _paparazzis_, que disparan sus flashes sobre los hombros del señor cura tratando de retratar al pequeño recién santificado. El padre de la criatura trata de concentrarse, mientras la madre emocionada se seca las lágrimas a la par de las abuelas, el resto saca pañuelos para secarse las gotas que comenzaron a desplazarse por los laterales de la frente… previo movimiento de camisas y blusas, despegando las telas de espaldas, pechos y cinturas.

El que mejor la pasa, claramente es el tío o abuelo que salió a “cuidar” al hermanito del bautizado a la vereda.

Finalizada la ceremonia, se escucha: ¡PODEMOS IR EN PAZ! Esa orden la conocemos todos, y comenzamos a dar la vuelta sobre nuestros pasos, señal de la cruz mediante. ¡Pero no! Aparece la madre, madrina o quien se encargue de las fotos, llamando a familiares y amigos mientras hace fila en el altar para conseguir cazar al cura y ponerlo de adorno en la foto. 

Siempre falta alguno, pero la foto se saca igual, con algún familiar de otro grupo humano que quedo demorado saliendo del escenario central, la foto no sale limpia, no todos miran ni se abrazan y hasta en algunas ocasiones terminan tapando al bautizado, pero no importa, ya está la foto final decretada por la mayoría y sale el malón en procesión. 

Si los padres son considerados y comprenden el sacrificio en el que metieron a cada familiar y amigo, invitan a una ágape, picada o chopeteada para compensar el mal trago, si no, cada uno saluda en la puerta y se va con el objetivo final de llegar a su hogar y elegir la indumentaria apropiada para disfrutar de la comodidad de su hogar, previa entrega del souvenir destinado al mueble del living o la foto para la heladera, por unos meses hasta que decidís limpiar la casa de tantos chirimbolos y pasan a una cajita de los recuerdos, porque te da un no sé qué tirar el recuerdo.

Si no es un sábado, es el domingo ¡a las ONCE! Después de la misa de los niños. Listo, chau, me fui, ¡MADRUGADA de domingo! El único día que podemos dormir sin culpa, que la alarma del celular no lo tiene tildado ¿y tengo que madrugar para desayunar, bañarme, vestirme, peinarme e ir hasta la iglesia? ¡PARÁ! ¿Vos realmente me tenés afecto? Porque no me estaría quedando muy claro…

Haceme un favor: ¡No me invites! No me ofendo, es más, te lo agradezco de corazón.