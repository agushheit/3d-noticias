---
layout: analisis
category: analisis
date: 2020-12-18T12:06:14Z
author: Por Sofía Gioja
title: La odisea del regalo
thumbnail: https://assets.3dnoticias.com.ar/Gioja.webp

---
Un estudio realizado por la Universidad de Wisconsin (siempre inchequeable) demuestra que la persona que hace el regalo, piensa mucho más el objeto que la persona que lo recibe. 

Otro estudio, también inchequeable, dice que, para no pifiarle al presente, hay que comprar pensando en uno mismo. O sea, ¿me lo compraría para mí? Si, la respuesta es sí, hemos elegido sabiamente.

Ahora bien, ni todos los regalos son para todas las personas, ni todos los regalos son para cualquier festividad. O sí, pero, ¿para qué arriesgarnos? Digo, si tenemos una amiga que recién se puso de novia, o una hermana con siete meses de embarazo o un tío viudo de ochenta pirulos, y entramos buscando obsequios de navidad a un sex-shop, bueno, puede resultar confuso, ¿no? Al menos sospechoso. Claro que tenemos que comprar el regalo pensando como si fuese para nosotros… pero que no se note. ¿_Capichi?_

Siguiendo con mi análisis. El regalo individual, de persona a persona, ya caducó. Olvidémonos, es muy personal y falla en el 50% de los casos. Aparte, es aburrido. Me voy a referir ahora a un fenómeno muy en boga. Digno de un análisis sociológico.

En Santa Fe se estila, desde hace tiempo, eso de hacerse regalos grupales _random_. Sucede cuando somos muchas amigas y no queremos quedar mal con ninguna, ni tampoco bien con ninguna. Quiero aclarar que hablo en «modo mujeres» porque es el universo que conozco. Pero se puede trasladar  a cualquier ser humano, de cualquier género biológico o autopercibido. Si alguien quiere hacerme un aporte aclaratorio al respecto, es bienvenido.

Continuemos. Lo que decía recién, esto de hacerse regalos tipo «amigo invisible» sucede entre grupo de gentes de la primera, segunda o tercera edad, que ya no tienen ganas de renegar con nadie, pero tampoco quieren quedar como «lauchas», entonces, nobleza obliga, organizan regalarse «alguna cosita simbólica» por un monto «x». El monto predeterminado, pongámosle trescientos pesos, es para que no se descuelguen con algún pareo playero de mil pesos o con un payaso de cerámica, mal esmaltado de cien _pe_ que compraron en la caja del supermercado chino o una maceta de plástico pintada a mano por algún hijo en el jardín.

El payaso es lo peor. ¿Te comprarías ese payaso inmundo? No, ni en pedo, ¿no? Entonces, ¿por qué sos así? Sí, a vos te hablo… ¡No te hagas la sota!

En mi caso (permítanme la autorreferencialidad) en una ocasión me «tocó» un par de aros que, francamente, daba para el debate. Y eso que todavía no cumplo los cuarenta, mis amigas son de mi franja etaria y me gustan los aritos, pero no significa que vaya a usar esas pelotitas rojas de árbol de navidad llenas de purpurina. ¿Quién compró eso? ¿Pensando en quién? ¿Son _wachas,_ eh? ¡Qué bárbaro!

Claro, porque ese es el problema principal de comprar regalos usando la dinámica que contaba anteriormente. Todos los regalos van a una bolsa o caja común y, llegado el momento, se manotea a ciegas y al que le toca, le toca. Los resultados de la repartija son hilarantes, a veces devastadores. Porque pasa que compramos un regalo «medio pelo» pensando en zafar, pero siempre hay un correveidile en el grupo, alguien que va filtrando de _canuto_ quién compró tal cosa. Entonces, cuando se corre la voz de que hay regalos lindos, vamos a ir  especulando con no sacar el spray de tela «caricias de algodón», los tapabocas de Frida Kahlo, el Joker o Star Wars o las velas aromáticas de rosa mosqueta y lavanda.

Pero el destino es un ser malvado  y tal vez recibas aquello que diste. Como la vida misma. Entonces, metés la mano en la caja, con genuino nerviosismo, manoteás el envoltorio, medio tanteando, medio disimulando, y te vas a un rincón a ver qué te sacaste. Y ahí nomás, arrancás, haciéndote la intrigante mientras le das vueltas al paquetito, palpando la silueta, analizando el papel de regalo, mirando la tarjetita, preguntándole a la otra qué le tocó, si es pesado o si tiene olor… alargás el momento, postergás la pena, saboreás la decepción… porque en fondo ya sabés: sacaste el payaso mal esmaltado que vos misma compraste. Y eso, _my friends_, se llama karma.

Que tengan muy felices fiestas y compren regalos lindos. Y si no les gusta el suyo, disimulen. No sean _garcas._ ¡Chin chin!

<br/>

<hr style="border:2px solid red"> </hr>

**Sofía Gioja en** [**twitter**](https://twitter.com/SofiG83)

Otras columnas de la autora en este portal:

[La culpa no es del chancho](https://3dnoticias.com.ar/analisis/la-culpa-no-es-del-chancho.algunas-reflexiones-sobre-el-consumo-ironico/) | [Como paquetes](https://3dnoticias.com.ar/analisis/como-paquetes/) | [Trátame suavemente](https://3dnoticias.com.ar/analisis/tr%C3%A1tame-suavemente/) | [Hasta que se metieron con el pan dulce](https://3dnoticias.com.ar/analisis/hasta-que-se-metieron-con-el-pan-dulce/) | [Los antivacunas ¿a qué se oponen?](https://3dnoticias.com.ar/analisis/los-antivacunas-%C2%BFa-qu%C3%A9-se-oponen-en-realidad/) | [Todo lo que no se hizo](https://3dnoticias.com.ar/analisis/todo-lo-que-no-se-hizo/)