---
layout: analisis
category: analisis
date: 2021-01-18T10:36:22Z
author: Sofia Gioja
title: WHATSAPP MARK, ANTES ERAS CHEVERE
thumbnail: https://assets.3dnoticias.com.ar/Gioja.webp

---
La famosa plataforma había solicitado a los usuarios que aceptaran la nueva política antes del 8 de febrero, pero algo salió mal y se pospuso la fecha límite hasta el 15 de mayo mientras explican los cambios.

Como todos ya sabemos, WhatsApp cambió recientemente sus condiciones de uso y políticas de privacidad. Algunos de nosotros, de dimos “aceptar” sin leer, y otros, leyeron detenidamente los términos, como debe ser. La consecuencia era de esperarse, que explotaran las críticas en redes sociales y se plantearan serios cuestionamientos sobre la cantidad de datos que recopila la aplicación. No es cosa nueva. Solo que ahora se les dió por informarnos. El que avisa no traiciona.

Cabe aclarar que no aceptar las condiciones, no esta dentro de la propuesta. Si no aceptas, te quedas sin WhatsApp. Por eso recalco que nos informaron. Nunca nos preguntaron.

WhatsApp explica muy campante y sin remordimientos, que recopila información de los usuarios "para operar, proporcionar, mejorar, entender, personalizar, respaldar y promocionar" sus servicios. Pero tanto uso de verbos seguro esconde una mentira, es obvio. Nadie le cree, Don Zuckemberg.

En realidad, las empresas detrás de esto, recaudan muchísima más información. Esta información corresponde, por ejemplo, al número de teléfono que usaste para crear la cuenta y la información básica de tu perfil; la ubicación del dispositivo, si querés compartirla con tus contactos; los mensajes, de forma temporal y cifrada, si éstos no se entregaron correctamente o forman parte de un reenvío de archivos multimedia; la agenda de contactos, los estados compartidos y los datos de pagos. Incluso, los stickers. (Al señor que creó la app “Sticker maker” le pagaron una torta de plata, para que atreves de su app, nos puedan chorear data, no me caben dudas).

De forma automática, WhatsApp, también recopila información sobre la actividad del usuario en la aplicación (ajustes, interacción, frecuencia), de diagnóstico de servicio, o del dispositivo y la conexión (modelo, sistema operativo, nivel de carga de la batería, zona horaria, dirección IP). Instala, además, cookies "para operar y proporcionar los servicios, además de proporcionar servicios basados en Internet, mejorar las experiencias, entender cómo se usan los servicios y personalizarlos". En criollo, saben que tipo de celular usás, que sistema operativo tenés y que internet instalaste en tu casa.

Hay más. También usan la información proveniente de terceros, de otros usuarios, con los que el usuario contacta o que tienen el teléfono del usuario en su agenda, de denuncias por supuestas infracciones, de las empresas con las que interactúa en la app o de proveedores de servicios.

No nos espía la CIA, o busca contactos terroristas en nuestras agendas de contactos. Son empresas, como dije, que nos quieren vender cosas. De hecho, nos venden cosas. Pero no nos damos cuenta. Si no te gusta, ahí está la puerta. No lo dije yo, lo dijo Mark.

Al igual que Facebook o Instagram, WhatsApp comparte estos datos que recopila, con otras empresas del grupo, al igual que dichas empresas comparten datos del usuario con WhatsApp. ¿Vieron que cuando buscamos en Google, lugares para vacacionar, o el precio de un caniche toy, misteriosamente nos aparecen anuncios relacionados en Facebook? Bueno, por ahí va la cosa.

Resulta que después de semejante alboroto, la empresa, que había puesto como tope el 8 de febrero para aceptar o aceptar, informó que va a postergar la fecha, para que los consumidores puedan revisar y aceptar los nuevos términos con más tranquilidad. Ahora, el cambio será el 15 de mayo.

Pero se les escapó un detalle. Muchos usuarios abandonaron la app.

**PELIGRO DE MIGRACIÓN**

A raíz de esto, WhatsApp empezó a notar una estrepitosa caída en las descargas de la app y un repunte proporcional e inédito en la cantidad de usuarios nuevos que fueron mirando con cariño y pasándose a otras apps (la competencia). Estamos hablando de Signal y Telegram.

WhatsApp, al igual que Signal y Telegram, utiliza cifrado de extremo a extremo, lo que significa que las empresas (y las fuerzas de seguridad como la CIA o el FBI) no pueden leer nuestros mensajes.

Pero a diferencia de Facebook, que obtiene miles de millones de la extracción de datos de personas para publicidad targeted (dirigida a nuestros intereses a partir de la información que recolecta), Signal es administrado por una organización sin fines de lucro. Ponele.

Entonces, desde que comenzó la controversia, estas dos aplicaciones, reportaron cifras récord de descargas. Llegaron a estar en los puestos 1 y 2 de la AppStore de Apple, pocos días después del anuncio de WhatsApp. Ambas plataformas llevan años sosteniendo que son más seguras y tienen políticas más estrictas en la protección de datos de sus usuarios. Pero tampoco les vamos a creer. Por si las moscas.

Sensor Tower, una empresa de análisis de aplicaciones para móviles, dio a conocer el miércoles pasado un registro en el que Signal obtuvo 17.8 millones de descargas en Apple y Google del 5 al 12 de enero. Eso es 61 veces más que las 285.000 descargas que tuvo la semana anterior.

De hecho, el viernes 15 de enero, se cayó el servicio por la impresionante cantidad de usuarios que sumó: "Hemos estado agregando nuevos servidores y capacidad adicional a un ritmo récord todos los días de esta semana, pero hoy superamos incluso nuestras proyecciones más optimistas. Millones y millones de nuevos usuarios están enviando un mensaje de que la privacidad es importante, y estamos trabajando duro para restaurar el servicio para ellos lo más rápido posible”, explicaron desde el equipo de comunicaciones de Signal al medio PC Mag.

Telegram, por su parte, que ya es popular en distintas partes del mundo, tuvo 15.7 millones de descargas en el mismo período, lo que representó casi el doble de las 7.6 millones que registró la semana previa.

WhatsApp, en tanto, sumó 10.6 millones de descargas, menos de las 12.7 millones que tuvo la semana anterior.

**TODO ESTÁ GUARDADO EN LA MEMORIA**

Lo dijo León Gieco. Lo cierto es que, vayamos donde vayamos, nuestros datos nunca van a desaparecer. Hay quienes aseguran que, una vez que entras a la red, nunca más salís de ella. Y de hecho así es. Todo lo que intentemos eliminar mediante los habituales sistemas de borrado va quedar registrado de igual manera, ya sean de textos, fotos, vídeos, e-mails, conversaciones de chat o el historial de navegación en internet. Nunca se borran. ¿Estamos jodidos? Probablemente.

"No hay ningún sistema que garantice al cien por cien la eliminación de un archivo en un soporte digital", aseguran Daniel Creus y Mikel Gastesi, expertos en seguridad informática y autores del libro “Fraude Online: abierto 24 horas”.

"Es más fácil ocultar una infidelidad a tu pareja que a Google, que no tarda en ponernos anuncios de escapadas de fin de semana cuando nos lee mensajes románticos", denuncia Alejandro Suárez Sánchez-Ocaña, autor de Desnudando a Google, un libro que revela hasta qué punto este gigante de internet tiene fichados a sus millones de clientes.

Mas allá de sus problemas de pareja, este señor, Sánchez Ocaña, tiene razón cuando afirma que: "Cada vez que usamos Chrome, Youtube, Gmail o el Google, estos registran nuestros gustos, horarios, localización geográfica e intereses personales. Estas empresas ofrecen servicios buenísimos, pero no son gratis, como creemos ingenuamente. Pagamos con nuestra privacidad".

Lo que me resulta notorio es que cada cierto tiempo sale una nota que viralmente se difunde por Facebook en la que, supuestamente, quien la pega en su muro, le dice a Mark lo que puede hacer o no con sus datos. Para ello incluso, se surte de una serie de artículos de alguna ley desconocida o inexistente, en la cual se puedan amparar. Pero todo esto no sirve, no funciona. Y los usuarios por alguna razón que no entiendo, lo siguen compartiendo.

![](https://assets.3dnoticias.com.ar/yo_no_autorizo.png)

![](https://assets.3dnoticias.com.ar/callate.jpg)

No existe el anonimato en la red. En cuanto asomamos, nuestra presencia deja un rastro fácilmente localizable, sea cual sea la plataforma que utilicemos. Y no importa a quien le recemos o a quien denunciemos. Desde el momento cero, en que usamos internet, estamos autorizando a este monstruito a que nos usen de carnada comercial. Si aquella vez que apareció un cartelito, aceptamos sin leer...a llorar a la capillita.

**EL COMUNICADO OFICIAL**

Luego del revuelo mundial, desde la empresa salieron a aclarar que WhatsApp “quiere facilitar que las personas realicen compras y obtengan ayuda de una empresa directamente en WhatsApp. Si bien la mayoría de los usuarios utiliza WhatsApp para comunicarse con amigos y familiares, cada vez más personas se contactan también con empresas. Para aumentar aún más la transparencia, actualizamos las políticas de privacidad para describir que, en el futuro, las empresas pueden optar por recibir servicios de almacenamiento seguro de nuestra empresa matriz, Facebook, para ayudar a administrar la comunicación con sus clientes en WhatsApp. Aunque, por supuesto, depende del usuario si quiere o no enviar mensajes a una empresa en WhatsApp”.

“La actualización no cambia las prácticas de intercambio de datos de WhatsApp con Facebook y no afecta la forma en que las personas se comunican en privado con amigos o familiares en cualquier parte del mundo. WhatsApp sigue totalmente comprometido con la protección de la privacidad de las personas. Nos estamos comunicando directamente con los usuarios a través de WhatsApp acerca de estos cambios, para que tengan tiempo de revisar las nuevas políticas durante el próximo mes”, concluyeron.

Con esta inusitada apertura de paraguas legal, la plataforma se estaría lavando las manos, dejándonos la decisión final a nosotros, los usuarios. ¿Nos quedamos y le regalamos nuestra intimidad a una empresa que lucra con nuestros intereses, nos pasamos a otra plataforma (menos mercenaria) o nos vamos a vivir a una comunidad Amish?

Como dije, la decisión final es nuestra.