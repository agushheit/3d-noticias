---
layout: analisis
category: analisis
date: 2021-01-29T11:51:27Z
author: José Villagrán
title: 'Autonomía Municipal: la carta política que se juega Perotti para imponer su
  agenda'
thumbnail: https://assets.3dnoticias.com.ar/jose_villagran.jpeg

---
![](https://assets.3dnoticias.com.ar/autonomia.jpeg)

La idea es darle mayor poder y toma de decisiones a municipios y comunas. Las conversaciones comenzaron esta semana y seguirán en las que vienen. El “perfil alto” que pedía Omar Perotti empieza anotarse.

El gobierno activó políticamente y esta semana reunió a intendentes y presidentes comunales de la provincia para presentarles el proyecto de “Autonomía Municipal”. El encuentro se dio en el Salón Blanco de Casa de Gobierno y fue mixto. Algunos estuvieron presentes en el lugar y otros por videoconferencia.

Es un primer paso interesante que da el gobierno provincial, si se lo mira desde el lado estrictamente político. Muy pocas veces en su primer año de gestión, el Ejecutivo tuvo la iniciativa de plantear un tema “ganándole de mano” a la oposición. Uno recuerda, por ejemplo, lo que fue la discusión de la “Ley de Necesidad Pública” en los primeros meses de gobierno y hasta lo caótica y mal planteada que tuvo esa discusión, que llevó incluso a negociar con los senadores del propio espacio para que la voten a favor. Pareciera ser que este caso es distinto y lleva algo de aprendizaje.

Es que convocando a intendentes y presidentes de comunas, el gobierno contrarresta las críticas de ejercer una política cerrada y de difícil acceso. Y por contrapartida, gana en consenso y confianza. Y esa decisión es más amplia cuando se ve a quiénes decidió convocar Omar Perotti a la mesa: nada más y nada menos que a los mandatarios de Santa Fe y Rosario, las dos ciudades más importantes de la provincia, y los dirigentes que, a priori, ya adelantaron estar a favor de la propuesta.

Pero además, también gana en imposición sobre un tema en el cual hoy el gobierno de Perotti hace agua por todos lados: la seguridad. La idea, con este proyecto, es darle más poder y más responsabilidades a los municipios y comunas en temas como este. Es indudable que hoy la ciudadanía, y la sociedad en su conjunto, plantean problemáticas que requieren un accionar rápido, eficaz y de cercanía. Y de esos problemas que hoy aqueja a la provincia es la cuestión de inseguridad. Así, logra sacar del medio (por ahora) al tal cuestionado ministro de Seguridad, Marcelo Saín, y se posiciona el el centro del debate político.

¿Que plantea la “Autonomía Municipal”?

Muy pocos lo recordarán, pero en el año 2010 el senador por el departamento San Lorenzo, Armando Traferri, presentó en la legislatura santafesina un proyecto de autonomía municipal que obtuvo media sanción en la Cámara de Senadores y luego perdió estado parlamentario por falta de tratamiento en tiempo y forma de Diputados. Hoy, tanto Traferri como el Gobierno de Santa Fe se miran con desconfianza, por todo lo que está sucediendo en la causa que lo investiga.

Más acá en el tiempo, en 2016, el actual diputado nacional Federico Angelini ingresó en la cámara baja de la provincia el pedido de modificación de la Ley Orgánica de Municipios en su artículo 1 alegando que se buscaba “reconocer un régimen de autonomía para los municipios”, ya que “nuestra provincia es una de las pocas en el concierto nacional que aún no lo ha hecho”.

La misma idea presentó dos años después el por entonces senador por el departamento Rosario, Miguel Ángel Cappiello, y misma suerte corrió. Ninguno de esos tres proyectos logró la sanción definitiva.

La historia nos hace llegar a este principio de 2021, donde la idea vuelve a presentarse. "No estamos planteando algo formal, sino para mejorar la calidad de vida de los ciudadanía; la cercanía les hará la vida más fácil a todos" aclaró el ministro de Gobierno Roberto Sukerman.

Las ideas claras sobre el proyecto aún todavía no están. Sólo hay un boceto generalizado y las cuestiones se van a ir aclarando a medida que las reuniones y los encuentros se vayan sucediendo.

La autonomía le daría a los gobiernos locales importantes facultades: abriría la puerta a que se den una Carta Orgánica,  a definir sobre sus autoridades y cómo se eligen, y qué impuestos cobran. Pero además, generaría un intenso debate sobre cómo sería la distribución de recursos. Ampliar la base administrativa y el esquema de gobierno de las ciudades y pueblos llevaría a que se incrementen los gastos operativos para esas funciones.

El sanjavierino Mario Migno, en la reunión, planteó la necesidad de hacer una ingeniería que permita unificar mandatos y elegir todas las autoridades cada cuatro años, tanto en provincia como en municipios y comunas.

Otra de las ideas que surgió en el encuentro de este miércoles es aumentar la duración de los mandatos de los presidentes comunales, que actualmente es de dos años y se propuso a llevarlo a cuatro, al igual que los mandatos de gobernador, vicegobernador, intendente y concejales.

"Fortalecer la autonomía es fortalecer los procesos democráticos y el federalismo, que reclamamos como provincia y que tenemos que ser coherentes en ejercitarlo plenamente hacia adentro; este es un elemento trascendental que tenemos que marcar en estos años, afianzando la territorialidad, la autonomía y la descentralización", les señaló el gobernador Omar Perotti al abrir la reunión.

"Tenemos cada vez más responsabilidades y a veces no la podemos afrontar. El primer paso es extender los intendentes comunales, después vendrán otros" señaló el intendente de la ciudad de Santa Fe, Emilio Jatón. "Hay una cada vez mayor demanda de respuestas a los gobiernos locales y necesitamos mayores instrumentos", acotó el rosario Pablo Javkin cuando hizo uso de la palabra.

El primer apoyo público que cosechó la propuesta vino desde el Foro de Intendentes y Presidentes Comunales Radicales de Santa Fe, quienes en un comunicado afirmaron que “celebramos que se ponga en agenda este tema tan importante para lograr mayor institucionalidad, más democracia, más y mejores servicios para los ciudadanos” pero también piden que se analice cómo se financiarán las mayores responsabilidades de municipios y comunas.

En el documento “Rosario hacia la autonomía municipal. Un asunto de todos”, Miguel Lifschitz escribía en el prólogo que “los municipios se convierten entonces en escenarios de expresión de múltiples demandas, necesidades, luchas sociales que no son visibles en otros ámbitos. La ciudad es el ámbito de la diversidad. Es un espacio que habilita manifestaciones quizás más locales, pero no por ello menos vitales para la vida de cada vecina y cada vecino. En este sentido, la autonomía municipal –anclada en un federalismo genuino- es una herramienta que puede aportar a la construcción de una sociedad más justa, más solidaria, con mayor equidad. Y ésa es nuestra fundamental apuesta.

Por ello, es vital fortalecer a los municipios. La autonomía es una herramienta que va en este sentido”. En declaraciones de estos tiempos, el mismo Lifschitz se muestra a favor pero ahora pone reparos en la forma en la que se va a implementar.

El impulso de la iniciativa ya tuvo repercusión en Diputados. Tras conocerse la convocatoria de este miércoles, Ruben Giustiniani ya le pide a Perotti que incluya en sesiones extraordinarias su proyecto sobre autonomía municipal.

Impulso en año de elecciones

En este año electoral, hay que mirar y analizar todo en un doble sentido. Desde hace algún tiempo, Omar Perotti le viene “bajando discurso” de lo que piensa hacer este 2021 a los presidentes comunales e intendentes. Ya les anticipó que habrá obra pública y recursos dentro del “Plan Incluir” y a esto hay que agregarle esta nueva propuesta.

Es decir, este año de elecciones habrá plata para colocarla al territorio, siguiendo con la idea de mostrar mayor movimiento en el territorio y levantar el perfil del gobierno de cara a la gente.

El encuentro de ayer dejó satisfechos a los funcionarios provinciales. Por la repercusión, la aceptación y la la idea de estar tomando de a poco la centralidad de la política en Santa Fe.

Lograron sentar en la mesa a un grupo de intendentes encabezados por el rosarino  Javkin y el capitalino Jatón más seis presidentes comunales invitados especialmente (dos PJ, dos Frente Progresista y dos Juntos por el Cambio). Fueron más los representantes “opositores” los que se hicieron presentes que los que “juegan en el mismo equipo”. Pero con ellos, el gobierno puede dialogar de buena manera.

Debut exitoso para los flamantes ministros Corach y Sukerman. Este punto a favor del gobierno hay que dárselos a ellos.