---
layout: analisis
category: analisis
date: 2021-05-18T09:37:00-03:00
author: Florencia Morere
title: Maiameeeee
thumbnail: https://assets.3dnoticias.com.ar/Flor-Morere.jpg

---
Mucho se habló en las últimas horas sobre los viajes de turismo-vacuna a Estados Unidos. Gente a favor, gente en contra. Y la verdad que cada vez que lo hablo con alguien, sus argumentos me hacen pensar en ese “yoismo” que tenemos los argentinos.

Desde Jorge Rial a Mauricio Macri, hemos escuchado todo tipo de justificativos: "Efectivamente me voy a vacunar a Miami. La espera de un turno era larga y tediosa. Pero se bancaba. Incluso con el enojo de mi familia. Sin embargo, la actitud soberbia de Zannini me hizo tomar una decisión que venía evitando: no dejar en manos de los políticos mi salud" sostuvo el periodista de chimentos en su cuenta de Twitter.

Por su parte, el ex presidente de los argentinos descargó en su cuenta de Facebook: “Estando en EE.UU. pude comprobar que las vacunas se aplican en cualquier lado, desde las playas hasta los centros comerciales, e incluso en las farmacias. Yo mismo me he podido aplicar en una farmacia la vacuna monodosis de Johnson. Recordemos que la Argentina podría haber tenido a su disposición millones de vacunas que no supo negociar.”

En las últimas horas se supo que un concejal de nuestra ciudad, que exponía su descontento contra aquellos que se iban a vacunar al extranjero, viajó a Miami  Y SE VACUNÓ!!!.

Pero llevemos la charla a las vecinas del barrio, a las charlas de amigos y compañeros de trabajo: “Si pudiera hacerlo, lo haría. ¿Sabés cuando nos van a vacunar a nosotros?”; “Lo pagas en 18 cuotas. Vas, te vacunás y encima te relajas unos días en la playa”; “Lo estoy pensando. Sacar un préstamo e irme a vacunar allá. Tengo 30 años, no tengo ninguna enfermedad de riesgo. A mí no me vacunan más.” Fueron algunos de los argumentos que más escuché en los últimos días.

Ahora, yo me pregunto: ¿Esa gente vive en Narnia? ¿No sale a la calle? ¿No va al supermercado? ¿No ve la tele, no lee los diarios? Tenemos un 49% de pobres en el país, una inflación acumulada de 16%. Todos los días te aumenta la luz, el gas, el agua, la carne, el pan, la nafta. Y la pandemia viene para largo.

Hay mucha gente que quizás pueda ir a dejar sus ahorros al país del norte. El gasto por visitante ronda los 5000 dólares entre pasaje, alojamiento, comidas y traslado. Ahora ¿con qué cara le decís al pibe que te limpia el parabrisas en el semáforo por una moneda que no tenés nada para darle cuando venís de vivir la vida loca en las playas yankees? Cuando venís de colaborar con el crecimiento y la recuperación económica del país del norte y acá no sos capaz de ayudar a un pibe que debería estar en la escuela, pero está pidiendo monedas en el semáforo porque sino esta noche no come?

Mis padres y mi hermana son personal de salud y, gracias a Dios, ya tienen ambas dosis de la vacuna. Mi abuela, más de 80 y discapacitada, aún aguarda el turno para la segunda dosis. Mi hermano y yo no calificamos para la vacuna aún porque no tenemos comorbilidades y tenemos menos de 40 años. ¿La posibilidad de viajar a vacunarnos al exterior la tenemos? Si. ¿Lo haríamos? No. Creo que ni siquiera se me pasó por la cabeza la idea de irme a vacunar.

Los que me conocen saben que no comulgo con la línea política del gobierno nacional, mi decisión no pasa por el apoyo o el nacionalismo. Creo que de esta situación de mierda tenemos que salir todos juntos. Siento que hay que ser más empático.

Soy una afortunada por tener trabajo en este contexto, por poder dormir todas las noches en una cama abrigada y tener un plato de comida caliente en la mesa; por poder darme algún que otro gusto.

Obviamente me encantaría vacunarme. Me anoté como corresponde y espero que algún día me llegue el tan ansiado mail con el turno. Pero hasta que ese momento llegue, voy a seguir comprando las pastafrolas de mi vecina, las empanadas de mi compañera de trabajo, las bolsitas de residuos que me venden en los semáforos, porque de esta salimos entre todos.

Y a riesgo de revelar mi edad voy a evocar al gran Guillermo Nimo: “Por lo menos así, lo veo yo”.