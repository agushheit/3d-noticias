---
layout: analisis
category: analisis
date: 2020-12-09T11:36:28.000+00:00
author: Por Sofía Gioja
title: LA CULPA NO ES DEL CHANCHO. Algunas reflexiones sobre el consumo irónico
thumbnail: https://assets.3dnoticias.com.ar/Gioja.webp

---
Prendemos la tele, y en Crónica, Anabela Ascar habla de la peluca de Zulma Lobato, no nos gusta, pero lo vemos igual. Compartimos el video de “la tetita” y “la cermeza” de Wendy Sulca, no porque nos guste, en todo caso, nos resulta gracioso. Pero no nos reímos con ella, sino de ella. El video o el personaje nos parece re bizarro y buscamos un cómplice para mofarnos.

Twiteás y retwiteás las barrabasadas que dice Gisella Barreto sobre el aborto, las vacunas y la homosexualidad. Compartís los memes Rodrigo “el cheto de Banfield” Eguillor (el de “llamen a mi vieja”) que pasó de violador a “influencer” porque los medios le dieron cabida. Jorge Sonnante (el diácono trucho), El Presto, El Tipito Enojado, entre otros. Todos ellos son personajes mediáticos, a veces peligrosos que, ya sea por amor u odio, siguen en carrera porque nosotros los alimentamos.

<br/>

<span style="color: #cf0000;">**Esto es el consumo irónico: dar entidad a cosas que en realidad no nos gustan (y viralizarlas).**</span>

<br/>

_Anfibia_ hizo un informe que lo explica clarísimo: “La reproducción irónica sigue difundiendo mensajes opuestos a todo lo que pensamos. Lo hacemos porque nos resulta gracioso. ¿Qué más divertido que reírse en grupo de un programa de tele, un tipo de música o una postura política que nos resultan ridículos? El consumo irónico recrea y mantiene los límites simbólicos de nuestra comunidad”. ¡Auch!

Muchos sostienen que el consumo irónico no es un fenómeno nuevo, nació en la década de los 80 con la serie _Dallas_. Pero ahora volvió, gracias a las redes sociales.

Se deben acordar de María Elena Fuseneco de “Casado con hijos”. Todos tenemos un _sticker_ de ella en WhatsApp. El personaje de Erica Rivas es un fenómeno viral que arrancó casi a la par de Facebook.

En este caso, el personaje de María Elena encarnaba a una feminista, una mujer exitosa pero emocionalmente inestable y de carácter prepotente, que alimentó a las audiencias durante dos temporadas (empujadas un poco por el mismo programa) para que la tildaran de loca, machona e incluso alcohólica.

“Salud por eso” dice el _sticker_ de María Elena alzando una copa de tinto. La reproducción y viralización de un producto funciona en ambos sentidos. Por amor u odio. Pero el peligro radica principalmente en potenciar a personajes que en realidad deberían ser condenados social y penalmente. Y en pos de esa condena, le damos más trascendencia.

No me refiero a María Elena, claramente. Me refiero, por ejemplo, al youtuber ruso, Stas Reeflay, que expuso a su novia embarazada a temperaturas bajo cero y la mató de hipotermia. Todo salió en vivo por su canal y, por supuesto, reproducido miles de veces. A todos nos parece un hecho aberrante, pero insistimos en buscar y ver ese video.

Para la psicóloga clínica Alejandra Rodríguez, docente de la Universidad de Chile y USACH, esto también se relaciona con la necesidad de hacer lo prohibido.

<br/>

<hr style="border:2px solid red"> </hr>

<span style="color: #25282C;">**“El morbo se define como una característica  bastante común entre las personas, que consiste en la tendencia a querer ver, escuchar, oler o hacer cosas que están prohibidas en términos sociales. Es la tendencia consciente o inconsciente de buscar lo que transgrede las normas sociales”.** </span>

<hr style="border:2px solid red"> </hr>

<br/>

¿Se acuerdan de las fotos íntimas de Luciano Castro? A algunos les pareció un espanto que salgan a la luz y otros dieron rienda suelta a su sed de burla. Pobre tipo, no puede mandar “nudes” tranquilo. Pero las compartimos una y otra vez, por todas las redes. Hoy es un _sticker_ que dice “buen día, bebas”.

<br/>

### **El caldo de cultivo está en las redes sociales**

Tik tok parece no tener límites. Para aquellos que no son nativos digitales, resumidamente, TikTok es una app, como una red social que permite grabar, editar y compartir videos cortos (de 15 a 60 segundos) en loop (repetición), con la posibilidad de añadir fondos musicales, efectos de sonido y filtros o efectos visuales. Creativamente parece no tener límites. Pero el riesgo para los usuarios apáticos parece que tampoco.

Oficialmente, la aplicación requiere que los usuarios tengan al menos 13 años para acceder a ella, como sucede con otras redes sociales como Facebook, Twitter, Instagram y Snapchat. Además, cualquier persona menor de 18 años debe tener la aprobación de un padre, madre o tutor para usar la app, pero, como es sabido, muchos se saltan esta parte.

Una investigación de la BBC Trending descubrió a niños menores de 10 años usando la app al mismo tiempo que se localizó cierto tipo de contenido sexual o no apto para menores. La BBC también pudo identificar a varios usuarios que, una y otra vez, trataban de entrar en contacto con adolescentes.

Todo esto me lleva al siguiente punto que quiero desarrollar: “el fenómeno de los Tiktokers Tumberos”. No es peyorativo. Ellos mismos se hacen llamar de esta manera. Me refiero específicamente a  _Crisss59 y Ale666_ que “entre los dos suman casi 60 mil seguidores y varias causas penales”, dice un diario de  acá.

Los detenidos, casi siempre desde la celda, imitan al cantante puertorriqueño Bad Bunny, les ponen actuación a los audios más conocidos y hasta se graban mientras se tatúan entre ellos.

Otros utilizan los videos para responder preguntas de sus seguidores. Se graban a escondidas y a veces no tanto, dentro de las instalaciones de la Unidad Penitenciaria. Las transmisiones en vivo son otra historia, algo inédito en el sistema carcelario bonaerense. Ni hablar de del tema de la ilegalidad de los teléfonos dentro del penal. A ninguno de los 60 mil seguidores parece importarle. Ya fueron inmortalizados en varios diarios y medios digitales. Pero, pese a la viralización, siguen trasmitiendo hasta la fecha. Espero se entienda la contradicción…

Alejandro Seselovsky (columnista en _Crítica_ y _Rollling Stones_) dice: “el consumo irónico consiste entonces en el ejercicio narcisista de un espectador que se percibe mejor facultado que lo que está consumiendo. Y que precisamente lo consume para afianzar esa relación de jerarquías. Mira para despreciar. Y desprecia para, en la gimnasia del contraste, adquirir valor”.

Yao Cabrera es un youtuber de 23 años y es tendencia en Twitter. Cada vez que hay alguna noticia suya, las redes sociales explotan y se llenan de posteos y mensajes indignados por la impunidad de la que hace gala.

En 2017 llegó a los noticieros por haber sido filmado junto a otros amigos abusando de una chica en un hotel, después salieron fotos de él besando a sus fans de 12 y 13 años, y luego fue denunciado por corrupción de menores y por hacerle preguntas de índole sexual a menores de edad a las salidas de los colegios (para sus videos de YouTube).

Su historia no se termina ahí: fingió su secuestro, que lo apuñalaron, que lo golpearon en la calle y además tiene varias acusaciones por coacción y estafas reiteradas a chicas menores de edad. Hace poco fue demorado por violar la cuarentena y al día siguiente hizo una transmisión en vivo disfrazado de mujer en la que se burló de una chica trans y mostró fotos íntimas de la misma. Su cuenta de YouTube tiene 2 millones de suscriptores.

Y acá volvemos a la idea inicial de visibilizar y darles trascendencia a personajes que deberían quedar en el olvido. Bettina Martino, profesora de Teoría de la Comunicación II de la carrera de Comunicación Social de la FCPyS e investigadora de la UNCUYO, explica:

<br/>

<hr style="border:2px solid red"> </hr>

<span style="color: #25282C;">**“Son situaciones que tienen que ver con apologías de delitos, con discriminación, con xenofobia, maltrato. Se ha perdido la consciencia de que el otro mediatizado es otro u otra de carne y hueso, se toma distancia del sujeto real y con los memes más todavía, porque no hay forma ni siquiera de rastrear quién es el protagonista, dónde está, y no nos importa tampoco”.**</span>

<hr style="border:2px solid red"> </hr>

<br/>

### **Cuestión de códigos**

Hay otro problema. Somos responsables del mensaje que enviamos, pero no del mensaje que recibimos. Esto sucede cuando la comunidad interpretativa (grupo social que comparte los mismos códigos de comunicación) se vuelve más grande de lo esperado y el sentido del mensaje se pierde en el camino.

Eugenia Mitchelstein escribe para _Anfibia_: “Nuestro consumo irónico queda registrado, accesible más allá del momento puntual de la burla. Sin embargo, nuestra comunidad interpretativa no es infinita. Cuando una famosa conductora de noticiero presenta a un influencer de los últimos días en redes sociales, nos enojamos. Pero nosotros lo hicimos _trending topic_ y lo ayudamos a sumar miles de seguidores a su cuenta de Instagram. Tal vez no quedaba tan claro que lo hacíamos de manera irónica”.

Marino afirma que siempre las personas han hecho sobreinterpretaciones o han desviado la interpretación respecto de los sentidos originales que ha propuesto quien emite el mensaje.

<hr style="border:2px solid red"> </hr>

<span style="color: red;">**“El consumo irónico, sin quererlo, amplifica discursos y visibilidad, pero no con la misma intención de la ironía. Una consecuencia es justamente la ampliación de voces o discursos que están siendo parodiados, pero en esa amplificación no se puede controlar si se siguen parodiando o se siguen ampliando con su marca de origen, no se puede controlar cómo otras personas interpretan ese discurso”.**</span>

<hr style="border:2px solid red"> </hr>

Quizás solo sea cuestión de cambiar de canal, de sacarle el _like,_ de dejar de motivar _haters._ En vez de compartir estupideces con mensajes solapados, siendo irónicos o sarcásticos, ser claros a la hora de exponer nuestra postura.

Así que, antes de compartir aquello que odiamos, pensemos un toque en lo que dijo Damián Kuc en sus _Historias Innecesarias_: “el troll que alimentas hoy, mañana puede llegar al gobierno”. Ahora sí.

<br/>

<hr style="border:2px solid red"> </hr>

**Sofía Gioja en** [**twitter**](https://twitter.com/SofiG83)

Otras columnas de la autora en este portal:

[Como paquetes](https://3dnoticias.com.ar/analisis/como-paquetes/ "Como paquetes") | [Trátame suavemente](https://3dnoticias.com.ar/analisis/trátame-suavemente/ "Trátame suavemente") | [Hasta que se metieron con el pan dulce](https://3dnoticias.com.ar/analisis/hasta-que-se-metieron-con-el-pan-dulce/ "Hasta que se metieron con el pan dulce") | [Los antivacunas ¿a qué se oponen?](https://3dnoticias.com.ar/analisis/los-antivacunas-%C2%BFa-qu%C3%A9-se-oponen-en-realidad/ "Los antivacunas") | [Todo lo que no se hizo](https://3dnoticias.com.ar/analisis/todo-lo-que-no-se-hizo/ "Todo lo que no se hizo")