---
layout: analisis
category: analisis
date: 2021-02-08T06:04:20-03:00
author: Ignacio Etchart
title: Sobre la soberanía
thumbnail: https://assets.3dnoticias.com.ar/ignacio1.webp

---
Unx entiende que un Estado provincial, endeudado hasta la médula, y que a su vez forma parte de un Estado Nación también endeudado hasta el cogote, y encima es una deuda cuya moneda no debiera de existir en la región (al menos de esta forma pluri-multi-tipificada según su nivel de legalidad), obliga a tomar medidas drásticas para evitar un mayor deterioro de la economía, que se traduce, básicamente, en un mayor desempleo, mayor pobreza, mayor inseguridad y menor armonía social.

  Pero que un comunicado oficial lleve en el titulado la palabra “inventario” al referirse sobre importantes reservas de vida y agua dulce como son los humedales, por más que se intente recurrir a las mejores intenciones y las mayores ingenuidades, es imposible que esa palaba no haga ruido.

  ¿De qué hacemos inventarios? De cosas que están o que faltan, de objetos cuya presencia o ausencia merecen conformar una lista. Ya sea una que resuma placeres perdidos, como lo canta Joaquín Sabina, o una que discrimine qué le falta a la alacena de la cocina para poder transitar el día a día.

  Pero los humedales no sé si entran en esa categoría. No suena como algo que pudiese enumerarse, “un humedal, dos humedales”. Podremos imaginar miles de formas de medición, como el metro, la hectárea, la milla, los nudos, pero no podemos medir la vida.

**No es sobre cuántos**

  Ya lo sentenció Heidegger: “la naturaleza no es la puesta en escena donde el ser humano crea su vida. No es un simple decorado. Es un actor más en el drama de la vida, tal vez el actor más importante”.  En especial si en Argentina los humedales componen el 21% del territorio nacional. Vivimos en ellos, somos meros invitados, mal acostumbrados a la cada vez menos displicente hospitalidad que la tierra nos otorga.

  No son posibles reservorios de recursos ictícolas, hídricos, turísticos. Es la vida misma. No se puede explotar la vida, no se puede economizarla, administrarla. Bah, en realidad sí se puede, pues es el proyecto de la modernidad: medirlo todo, clasificarlo y explotarlo o desecharlo. No hay lugar para lo supuestamente inútil.

  Porque eso significa para el común del ojo cotidiano, para la dirigencia política aún anclada en el siglo XX (pero de los ochenta en adelante. Nadie quiere revivir la primavera de inicios de los setenta). Los humedales, como todo bello y sano paisaje natural son inútiles, a menos que se le saque provecho.

  “Explotación turística sustentable y ecoresponsable” cita otro comunicado oficial. ¿Cómo es posible que la explotación sea sustentable? ¿Nadie pensó acaso el abrumador oxímoron que este enunciado encarna? Son como las bombas democráticas enviadas desde el norte por todo el mundo. “Explotación de democracias sustentables”, suena hasta bello. Teléfono para el área de Prensa de Biden. 

  Lamentablemente el cuidado del medio ambiente sí es una cuestión generacional. Porque cuando una generación que roza lo indeseada (aquellxs nacidos del 90 en adelante, pues la militancia por la interrupción del embarazo no es otra cosa que salvaguardar a inocentes de la vida absurdamente brutal y deshumana que es la actualidad), crece entre incendios forestales, ecocidios por todo el planeta, CEOS de multinacionales siendo presidentes y militantes de la vida sana encarceladxs, se vuelve una cuestión generacional. 

**Wall Street**

  Hace unas semanas, jóvenes de entre 15 y 25 años de los Estados Unidos, coordinados por la red social Reddit, pusieron en jaque el sistema financiero mundial. No voy ahondar en el tema, sobran las columnas explicativas de como un negocio de videojuegos, gracias al compromiso altruista y a un desprecio por la voracidad de la multinacionales, fue rescatado de su cierre definitivo, mientras los magnates más escondidos del mundo perdían billones, que seguramente ya habrán recuperado.

  Hace unas semanas, el agua dulce cotiza en ese mismo lugar. Y debería ser una alarma, pero parece que sólo se limitó al chiste anecdotario. Porque si el agua dulce cotiza en Wall Street, eso quiere decir que al menos el 21% del territorio argentino cotiza en Chicago, y eso sin contar los causes que no pertenecen a los sitios Ramsar.

  Hace unos meses, el presidente Alberto Fernández, decreto mediante, delegó el control y mantenimiento de los buques que navegan el Río Paraná y de la Plata a una mera licitación a empresas nacionales e internacionales, ignorando un acuerdo establecido en agosto del año pasado con todas las provincias cuyo territorio limitan con estos ríos, y provocando que el Brigadier Juan Manuel de Rosas se revolcara en su tumba.

  En 2018 Nestlé y Coca Cola compraron y privatizaron la mayor reserva de agua dulce de Sudamérica, ubicada en el acuífero Guaraní, que hoy cotiza en Wall Street.

  ¿Ven las cosas que suceden cuando se sigue hablando con palabras como “inventario” o “explotación”? Nada, no sucede nada. Todo se mantiene exactamente igual, en las mismas condiciones de injusticia social y de inviabilidad para la vida en armonía.

  Es una cuestión generacional, pero solamente porque esta es la primera generación que lo milita con convicción. En el futuro, sí será agenda cotidiana, o nos encontrará en las ruinas narradas por Shelley.

![](https://assets.3dnoticias.com.ar/soberania.png)