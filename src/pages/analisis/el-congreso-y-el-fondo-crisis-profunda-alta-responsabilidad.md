---
layout: analisis
category: analisis
date: 2022-02-23T11:52:21-03:00
author: 'Jesús Rodríguez es Presidente de la Auditoría General de la Nación, el máximo
  órgano de control externo de la República Argentina. '
title: 'El Congreso y el Fondo: crisis profunda, alta responsabilidad'
thumbnail: https://assets.3dnoticias.com.ar/jesus rodriguez.jpg

---
Introducción

El 28 de enero, el Presidente Alberto Fernández anunció un acuerdo con el FMI y ese mismo día hizo efectivo un desembolso de U$S 731 millones, que complementa los U$S4.491 millones pagados en concepto de intereses, cargos y sobre tasas del préstamo del año 2018.

El convenio es, a diferencia del “stand by” del año 2018, un Acuerdo de Facilidades Extendidas de 24 meses de duración. El financiamiento convenido -por el total desembolsado en el programa anterior- incluye la reprogramación de los vencimientos que se repagan en 10 años con un periodo de gracia de 4 años y medio.

Cuando este acuerdo se concrete, será el número 22 de la lista iniciada en el año 1958 y el primero diseñado y firmado por una administración kirchnerista.

Los anuncios del Ministro de Economía en relación a lo que podríamos denominar “principio de acuerdo” se refieren a las trayectorias esperadas de algunas variables clave como el déficit fiscal, la emisión monetaria y la acumulación de reservas internacionales.

Los anuncios indican que el déficit primario (esto es, descontando el servicio de los intereses de la deuda) esperado en este año es del 2,5% del PIB y un 1,9% en el año 2023. Si comparamos estas metas con los resultados fiscales bien medidos, esto es considerando el déficit inercial del año 2021, entendido como el que se produciría de no mediar correcciones en el gasto, el desequilibrio fiscal de este año debería reducirse en más de dos puntos porcentuales del PIB en relación al año anterior. Es así, además, porque este año no estarán disponibles los ingresos extraordinarios de los DEG’s, resultado de la ampliación de capital del FMI, y del llamado impuesto a las grandes fortunas que rindieron 1,4% del PBI el año pasado.

Es obvio que el sendero de consolidación fiscal pone en el centro de la escena de la política pública a los subsidios económicos, energéticos y del sector transporte. Estos explican más del 80% del déficit primario del sector público. En ese terreno la cuesta es muy empinada. El facilismo económico de la administración y la grosera manipulación de los instrumentos regulatorios hizo que los subsidios energéticos que en diciembre de 2019 se habían reducido a 4.700 millones de dólares, puedan alcanzar 15.500 millones a fines de este año, si no hubiera actualizaciones de tarifas. Casi se igualaría la cifra del último año del gobierno de la Presidente Fernández de Kirchner.

El “principio de acuerdo” también requiere, además del compromiso de tener tasas de interés positivas en términos reales, que la emisión del Banco Central para cubrir el déficit primario se reduzca de un equivalente al 3,7% del PIB a tan solo 1% este año y que las reservas internacionales se incrementen en 5.000 millones de dólares.

No sabemos las hipótesis de crecimiento, inflación, la secuencia y magnitud de la corrección tarifaria, cómo será el sendero para incrementar las tasas de interés ni el movimiento del tipo de cambio, entre otros parámetros que fundamentan esas metas. Por lo tanto, es imposible hacer un juicio de valor sobre la pertinencia y probable grado de cumplimiento de las mismas.

A continuación comparto algunos comentarios que pueden ser útiles para el análisis.

 

¿Es necesario el acuerdo?

La economía argentina está estancada y exhibe desequilibrios notables. El PIB por habitante a fines de este año será casi un 15% inferior al de una década atrás. La evidencia de desequilibrios macroeconómicos fundamentales es que la inflación esperada supera el 50% anual, la brecha en la cotización cambiaría es de tres dígitos y el riesgo país está alrededor de los 1800 puntos.

En ese contexto, la regularización de las relaciones financieras con el exterior es una condición necesaria, no suficiente, para encarar los problemas estructurales de nuestra economía.

El “principio de acuerdo” evita el desastre, pero no garantiza el acceso al mercado voluntario de capitales, condición necesaria para posibilitar la inversión privada y la consiguiente creación de puestos de trabajo de calidad.

Por el contrario, la falta de acuerdo con el FMI asegura, ciertamente, la ampliación de la brecha cambiaria, el incremento del riesgo país, la aceleración de la inflación, el cierre de líneas de crédito privadas y oficiales de los países desarrollados, el cese de desembolsos de los organismos multilaterales de crédito y la caída del “swap” con el Banco de Ajuste de Basilea, entre otras derivaciones que conducen al derrumbe de la actividad económica, la destrucción de empleo y la pérdida de eslabones de las cadenas productivas de valor. Además de profundo y generalizado, el ajuste económico sería, muy probablemente, caótico.

Por otro lado, la falta de acuerdo con el FMI no es inocua. A diferencia de las obligaciones desatendidas con los acreedores privados, no hay “default” con el FMI sino que, de acuerdo a sus estatutos, se incurre en “arrears”, que puede traducirse como atrasos. La diferencia es tan simple como complejas sus consecuencias: los atrasos con el FMI no admiten ser refinanciados y, por lo tanto, para poder negociar un nuevo acuerdo con la institución deben ser, con antelación, cancelados totalmente.

 

¿Por qué se demoró el acuerdo?

La matriz populista del cuarto gobierno kirchnerista conlleva la necesidad de identificar “culpables” y, desde el primer día, la cuestión de la deuda y el acuerdo con el FMI del año 2018 fue, junto con el “lawfare”, la esencia y el núcleo de la formulación de su acción política.

Para ello, diseñó una estrategia sostenida en dos pilares: responsabilizar de manera exclusiva al gobierno de Cambiemos en el tema deuda pública, ignorando los orígenes y las causas de su crecimiento y, por otro lado, denunciar judicial y políticamente una complicidad entre funcionarios argentinos y el FMI, al tiempo que se exigió durante dos años, como condición previa, la ampliación de plazos y mejora de condiciones que no están contempladas en los estatutos del organismo.

El camino elegido respondía a una necesidad: la ilusoria búsqueda de ganar tiempo hasta después de las elecciones de renovación parlamentaria para evitar tomar decisiones que el gobierno no podía asumir sin poner en riesgo la estabilidad, o la propia existencia, del consorcio oficialista.

Así, otra vez, el exótico dispositivo de poder oficial -caracterizado por la debilidad del vértice del poder por la anomalía de su origen y, por otro lado, por la existencia de múltiples actores con capacidad de veto y bloqueo-, conspiró contra una solución técnicamente eficiente y políticamente viable al problema.

Pero, como nada es para siempre, el gobierno es enteramente responsable de haber desperdiciado la oportunidad de negociar en tiempos más propicios, incurriendo en costos evitables y quedando, al final del camino, con reservas netas de menos de 500 millones de dólares.

 

El papel del Congreso

El relato oficial sobre la deuda y el FMI incluyó el supuesto apartamiento del Congreso en la decisión, que en el caso del préstamo del año 2018 obedecía a favorecer la “fuga de divisas de los amigos de Macri”, por lo que se propició la sanción de una norma legal que, con el voto de la oposición, incluyó el requisito de la aprobación legislativa previa a eventuales acuerdos con el FMI.

En términos generales, la autorización al Ejecutivo a contraer endeudamiento está incluida de manera obligatoria en todos los presupuestos anuales. Pero en el caso de ser con organismos internacionales, el artículo 60 de la Ley 24.156 de Administración Financiera excluía de manera explícita esa obligación. Así, ninguno de los 21 acuerdos anteriores tuvo tratamiento en el Congreso, ni antes ni después de la sanción de la Ley 24.156 promulgada en el año 1992.

Con la nueva norma, sancionada en el año 2021 y aún no reglamentada, el Congreso debe aprobar el acuerdo y se entiende que, después de ese trámite legislativo, el Directorio del FMI consideraría la Carta de Intención y el resto de los documentos trabajados y acordados por el Ministerio de Economía y el Banco Central con el staff del organismo.

Es cierto que es poco lo que se sabe del contenido del “principio de acuerdo”, ya que se desconocen las medidas para cumplir las metas y, además, el Presupuesto del año se ejecuta bajo criterios de máxima discrecionalidad y arbitrariedad, luego del rechazo en la Cámara de Diputados, cuando el entonces Presidente del bloque oficialista insinuó su voluntad, luego concretada, de distanciamiento político respecto del Gobierno.

No obstante esos desarreglos institucionales, debe quedar claro que reclamar la normalización de las relaciones financieras con el exterior y autorizar la negociación que implica un acuerdo con el FMI no supone, en ninguna circunstancia, aprobar el contenido de esa negociación. Es, en todo caso, replicar lo actuado en ocasión de la reestructuración de la deuda con tenedores privados de bonos soberanos, donde la oposición reclamó y apoyó el acuerdo pero la negociación y sus particularidades quedó, como no puede ser de otra manera, en cabeza de la administración.

En este caso, además, hay que ratificar que el Congreso no formula la política económica que es responsabilidad del Poder Ejecutivo pero, en cambio, es de toda lógica exigir transparencia y rendición de cuentas para la adecuada tarea de control a cargo del Poder Legislativo.

 

La actitud de la oposición

Las reglas de decisión no pueden estar guiadas por los resultados de los sondeos de opinión pública que, en este caso, son claramente mayoritarios en relación a la necesidad de formalizar un acuerdo con el FMI.

Tampoco pueden admitirse posiciones que se justifiquen exclusivamente en la necesidad de diferenciación política frente al gobierno.

Las bases para formar un criterio de acción tienen que estar fundadas en lo que creemos que corresponde y es necesario hacer en esta hora decisiva y, por otro lado, en la responsabilidad de una fuerza política que aspira a gobernar los destinos de la Nación.

Nuestro discurso político debe tener como destinatario a una sociedad que padece múltiples urgencias y que reclama de su dirigencia una orientación en el rumbo para superar la crisis.

Nuestra argumentación política debe interpretar a una ciudadanía que no se resigna frente a las adversidades, que no acepta la incompetencia del gobierno y que se indigna frente a las obscenas disputas de poder en un oficialismo que sólo puede ofrecer utopías regresivas.

Las buenas prácticas de la política democrática nos conducen a posibilitar y facilitar la concreción del acuerdo con el FMI, en el convencimiento que, si se frustrara, sería un salto al vacío, con consecuencias extremadamente gravosas para el país y sus ciudadanos.

En ese carril de oposición al gobierno, pero no al país, nuestro comportamiento debe distanciarse de conductas suicidas como las de dirigentes políticos argentinos que, en tiempos no tan lejanos, alentaron a los organismos internacionales a suspender los desembolsos de préstamos comprometidos, o cuando celebraron en el Congreso el anuncio presidencial de la cesación unilateral de pagos.

Por el contrario, nuestra posición debe estar alineada con el ejemplo de otras fuerzas políticas que, en situaciones de crisis equivalentes, actuaron con máxima responsabilidad política.

Es el caso de Obama, que se reunió con el Presidente Bush y el aspirante republicano en los días previos a la elección presidencial para acordar las medidas que evitaran un crack financiero global y generalizado en la crisis de las hipotecas “subprime” en el año 2008. Ante la inminencia del pánico financiero los dos precandidatos suspendieron la campaña y, en un comunicado conjunto, coincidieron: “no importa cómo haya comenzado, todos tenemos la obligación de resolverlo”. Y agregaron: “el plan presentado al Congreso por el Gobierno de Bush tiene fallos, pero los esfuerzos por proteger la economía de EE.UU. no deben fracasar”.

Más cerca en la geografía, es la misma conducta que tuvo Lula quien, en su cuarto intento por acceder a la Presidencia, redactó la “Carta ao povo brasileiro“, en ocasión de acompañar la decisión del Presidente Cardoso en la negociación con el FMI en el año 2002.

Lula, que en el pasado había comparado la asistencia del FMI a su país con un “beso de la muerte”, se comprometió a obtener un superávit fiscal primario para 2003 del 3,75% del PIB, como mínimo, aunque finalmente en su gestión fue superior al 4%.

Y también fue similar la conducta del Frente Amplio en Uruguay, que no se permitió impedir ni obstaculizar los acuerdos con el FMI y la reprogramación de la deuda soberana del país hermano que afrontaba el contagio de la crisis argentina de principios de siglo. Esos acuerdos sentaron las bases de un crecimiento sostenido en los últimos 20 años, con reducción de la pobreza y la desigualdad, afirmados en una democracia de alta calidad institucional .

Es bueno recordar que esas conductas tuvieron reconocimiento social en las respectivas elecciones y, así como Obama y Lula fueron reelectos, el Frente Amplio gobernó durante tres turnos presidenciales.

La crisis estructural de la Argentina -evidenciada por el estancamiento económico y amplificada por una gestión oficial de la pandemia improvisada, vacilante, especulativa y prejuiciosa- no admite cálculo de conveniencia. Tampoco de mezquindad corporativa o egoísmo sectorial.

La primera y principal responsabilidad política de la dirigencia, en tiempos de miedos individuales y desconfianza social, es proveer certidumbre.

Eso, en estas horas decisivas, se traduce en evitar las permanentes recomendaciones para “agudizar las contradicciones” de los ultras de todas las épocas y de todas las ideologías.

Por ello, es bueno tener presente la reciente cita de Natalio Botana de la sentencia George Clemenceau: “La política de lo peor, es la peor de las políticas”.