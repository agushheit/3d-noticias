---
layout: analisis
category: analisis
date: 2020-12-09T11:24:03Z
author: Por Marisa Lemos
title: Todo sea por el juego
thumbnail: https://assets.3dnoticias.com.ar/Marisa-Lemos.webp

---
El Tribunal de Cuentas de la provincia formuló una observación total al decreto 998/20, por el cual **el gobernador Omar Perotti aprobó, el 21 de septiembre, la autorización a los concesionarios de los casinos Puerto Santa Fe S.A., City Center de Rosario y Casino Melincué S.A., a implementar, operar y explotar los juegos de azar a través de la modalidad complementaria denominada online o virtual por 15 años**.

La observación tiene tres ejes. Señala que "la modalidad online o virtual no se encuentra regulada en la actual ley 11.998, ya que ésta solo autoriza a los Casinos y Bingos físicos o presénciales", y que "el Poder Ejecutivo no se halla facultado para autorizar el desarrollo de la actividad mediante el dictado de una norma de inferior rango", como el decreto.

Luego de este decreto, que dispuso la **modalidad de captura de juego online**, el Casino de Rosario tardó aproximadamente un mes en poner en funcionamiento la plataforma online, ya que contaba con la misma en otras jurisdicciones.

En tanto, el Casino de Melincué informó que no podría afrontarlo y el de la capital aún no ha podido implementarla.

Como sabemos **los casinos de la provincia se encuentran cerrados desde el 20 de marzo del corriente año y al estar cerrados se perdieron fuentes de trabajo directas.** Se calcula que el número asciende a unas 15 mil familias.

A raíz de esto, salieron a la luz distintas declaraciones y denuncias por parte de la oposición y por eso cinco diputados de la Unión Cívica Radical hicieron una denuncia penal contra el gobierno provincial cuestionando tres decretos del Poder Ejecutivo por los cuales los tres casinos instalados en la provincia fueron habilitados para brindar el servicio de juego on line. También ayer fue presentado un pedido de citación a la Cámara de Diputados al vicepresidente de Lotería, Rodolfo Cattáneo para que brinde las explicaciones pertinentes sobre los decretos.

La presentación ante la Fiscalía de Delitos Complejos del MPA fue firmada por el jefe del bloque radical, Maximiliano Pullaro junto a Fabián Bastia, Silvana Di Stéfano, Sergio Basile y Marcelo González.

El legislador recordó que **el juego está prohibido en la provincia, salvo las excepciones que lo habilitan** y mencionó que la ley de Casinos habilitó tres salas en la provincia. "**Acá se habilitó una nueva modalidad de juego sin pasar por la Legislatura**" y comparó lo ocurrido en Misiones, Ciudad Autónoma de Buenos Aires y provincia de Buenos Aires donde hubo leyes que habilitaron la modalidad.

Por eso radicaron dicha denuncia y esperan que el MPA pueda hacer una profunda investigación.

### **LAS ACTUACIONES:**

En el marco de la investigación se realizaron varios allanamientos por juego clandestino en **un “megaoperativo” de más de 40 allanamientos simultáneos en la provincia de Santa Fe.**

Los operativos estuvieron a cargo de los fiscales de las ciudades de Reconquista y Vera, **Rubén Martínez y Gustavo Latorre** y autorizados por el juez ciudad **Martín Gauna Chapero**- los llevó a cabo el Organismo de Investigaciones (OI) y la Agencia de Investigación Criminal (AIC) en distintas localidades de la provincia: **Santa Fe, Reconquista, Vera, Malabrigo, Villa Ana, Wheelwright y Rosario**, donde se realizaron alrededor de 15 allanamientos.

En los allanamientos, de acuerdo a lo que trascendió hasta el momento, **se encontraron armas, dinero, estupefacientes, autos y documentación** que será de interés para la causa.

Entre los involucrados hay un ex jugador de Rosario Central de los años 80. Se trata de Marcelo Toscanelli, de 57 años, fue acusado por el fiscal de la localidad de Vera,

Nicolás Maglier, como jefe de una organización criminal dedicada a los juegos de azar clandestinos y a través de Internet el juez de primera instancia Martín Gauna Chapero, le dictó prisión preventiva por el plazo de ley, es decir, al menos dos años. Según informaron fuentes judiciales, la policía detectó el ingreso del ex futbolista al edificio el miércoles a primera hora, pero, cuando se realizó el operativo, el hombre ya no estaba.

Mediante videoconferencia el ex jugador fue acusado como autor del delito de asociación ilícita, en calidad de jefe. El fiscal Maglier le endilgó la autoría de los delitos de explotación, administración y organización de sistema de captación de juegos de azar sin la debida autorización y defraudación a la administración pública.

A Toscarelli le quedó abierta una posibilidad para que su defensa insista en el pedido de morigeración de prisión, ya que el ex jugador padece una grave enfermedad colorectal que deberá ser auditada por un médico forense del Servicio Penitenciario.

<hr style="border:2px solid red"> </hr>

**Marisa Lemos**

Otras columnas de la autora en este portal:

[Hijos del rigor](https://3dnoticias.com.ar/analisis/hijos-del-rigor/ "Hijos del rigor")