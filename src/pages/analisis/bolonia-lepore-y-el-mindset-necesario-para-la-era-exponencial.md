---
layout: analisis
category: analisis
date: 2022-01-13T06:00:15-03:00
author: Maurizio Morini / Gerente de innovación y Embajador para la transformación
  digital
title: Bolonia, Lepore y el "mindset" necesario para la era exponencial
thumbnail: https://assets.3dnoticias.com.ar/1619768476896.jpg

---
El nuevo alcalde Matteo Lepore (1) llevó a cabo una campaña electoral dual, en nombre de la renovación y de la discontinuidad con las políticas anteriores. Para lograr los objetivos señalados, es necesario un cambio de mentalidad en la administración pública, tanto en términos políticos como de gestión. Sin embargo, los primeros tres meses de gestión administrativa están marcados por la tradición y el liderazgo a la "antigua". Y si tardaran 100 días en dar la dirección que se proponen, nos estamos acercando a la fecha límite.  
   
Un libro que todos los administradores públicos, en especial los políticos, deberían leer de inmediato es “Exponencial” (2) de Azeem Azhar (3). Trata sobre nuestra sociedad en construcción, con cambios mucho más rápidos que en el pasado, pero con un corte muy claro en lo que respecta a la gestión pública.

No, con la lógica habitual de “seguir adelante”, sino con la indicación clara de que ante esta carrera científico-social-económica es fundamental y prioritario revitalizar la “gobernanza urbana”. Si no hacemos esto con un sentido “evolutivo”, no tendremos oportunidad de éxito.  
 En un mundo que tiende a generar hiperinformación, para crear comunidades de ciudadanos con mucha información, muy adaptados a las Tics e hiperconectados, es necesario involucrarlos en un nuevo proceso de “control democrático”, con un enfoque de interoperabilidad entre funciones y habilidades en diferentes contextos, y con 4 prioridades claves prioritarias y absolutas:

Transparencia

 Proximidad a las Personas

 Sensibilidad ante la explosión de derechos

 Valorización de los bienes sociales comunes.

 A decir verdad, estos temas no estaban tan alejados de las declaraciones de intenciones del nuevo alcalde. Pero lo ocurrido en la actividad reciente no parece tan acorde con estas afirmaciones. Pensamos en los temas relacionados con la innovación urbana, con el lazo norte, con ciertos nombramientos o no nombramientos, hasta la discusión interna del propio partido de origen del alcalde.

 Métodos antiguos y centralistas para la toma de decisiones, en una especie de "grupo exclusivo" de personas de referencia, reducida propensión/consideración a la opinión general: este es el retrato de estas semanas, tal como aparece ante los ciudadanos que también expresaron su voz.  
 En definitiva, nos encontramos con una especie de “balanza mágica”.  
 Esto no es bueno, pero se puede remediar, siempre que se adopte una nueva "mentalidad", capaz al mismo tiempo de comprender y utilizar la tecnología y ponerla al servicio de una nueva "comunidad", basada en la cooperación y la intercambio efectivo, de forma flexible, con instituciones renovadas de forma robusta para propiciar un cambio continuo

Pero no me limito a declaraciones generales. Intento señalar algunas referencias “metodológicas” sobre las que podemos orientarnos para avanzar hacia la transición evolutiva en la administración publica. Veámoslo:

Crear tiempos definidos para las respuestas de las oficinas públicas a los ciudadanos y empresas, con el objetivo claro de reducir los tiempos promedio actuales  
 2.) Comunicar a ciudadanos y empresas de acuerdo con un plan articulado y multifuncional (en sí mismo comunicado con anticipación)  
 3.) Crear cuadros temáticos digitales, regulados por facilitadores, sobre temas de interés común, para una escucha activa y constante (utilizando las plataformas disponibles)  
 4.) Crear y gestionar plataformas de conversión y educación profesional entre empresas locales, para todos los operadores de todas las edades.  
 5.) Desarrollar la interacción digital completa entre los distintos sujetos que operan en el territorio y a nivel temático supraterritorial (plan de interoperabilidad)  
 6). Difundir escuelas locales de innovación multiedad a nivel metropolitano, lideradas por jóvenes, en diversos temas a consensuar con empresas y organizaciones  
 7). Potenciar el territorio para el turismo sostenible a nivel metropolitano con políticas generalizadas.  
 8.) Desarrollar actividades concretas de economía circular a nivel municipal, con caminos ad hoc para los ciudadanos  
 9.) Apoyar el desarrollo de Pactos Educativos renovados en el ámbito escuela-familia en los territorios  
 10.) Apoyar la construcción de comunidades educativas como resultado del proceso a que se refieren los puntos anteriores, también a través del traspaso generacional de experiencias

Aplicando estas indicaciones metodológicas y por qué no añadir otras (siempre que sean sustancialmente exponenciales), y actuando de acuerdo con el mandato programa indicado, se podrá contribuir a cambiar verdaderamente la forma de pensar y actuar, requisitos fundamentales para hacer de Bolonia la "ciudad más progresista de 'Europa" (y tal vez no solo)

(1) Matteo Lepore, pertenece al Partido Democrático, electo el 4 de octubre de 2021 como Intendente de Bolonia, Emilia Romania, Italia

(2) Nos encontramos en medio de un cambio social radical, impulsado por una serie de “tecnologías exponenciales” que están transformando la forma en que vivimos e interactuamos entre nosotros y con el mundo que nos rodea. A medida que maduren la IA, la biología sintética, las energías renovables, entre otras, desafiarán nuestro tejido económico y social de manera caótica. En 2015, inicié “Exponential View” para ayudar a aclarar este caos. Sitio /https://www.exponentialview.co/

(3) Azeem Azhar es un empresario, inversor y asesor británico. Fundó PeerIndex en 2009 en 2014. En 2015, Azeem comenzó un boletín semanal, Exponential View.  
 Temprana edad y educación; Azeem recibió el título BA (Hons) en Filosofía, Política y Economía de la Universidad de Oxford en 1994.  
 Carrera profesional; Desde septiembre de 1994 hasta marzo de 1996, Azeem fue corresponsal de The Guardian. De 1997 a 1999, Azhar fue gerente de estrategia de BBC Online durante su período de formación y lanzamiento. En 2009, Azeem fundó PeerIndex, una de las primeras empresas de aprendizaje automático que proporciona análisis de personas influyentes en las redes sociales. En 2012, la startup ganó el Europas Grand Prix \[1\] y dos años más tarde fue adquirida por Brandwatch \[2\]. En 2015, Azeem lanzó uno de los primeros boletines semanales centrado en las tecnologías exponenciales y su impacto en la sociedad, Exponential View \[1\]. Azeem fue miembro del Panel de Consumidores de Ofcom entre 2005 y 2007. \[3\] Fue nombrado miembro del Comité de Cairncross para revisar la sostenibilidad de la prensa en 2007.

(4) Articulo original en https://cantierebologna.com/2022/01/05/bologna-lepore-e-il-mindset-necessario-per-leta-esponenziale/

_Traducción de José Batt_