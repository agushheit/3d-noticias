---
layout: analisis
author: Por Sofia Gioja
category: analisis
title: Todo lo que no se hizo
date: 2020-11-05T17:46:55.740+00:00
thumbnail: https://assets.3dnoticias.com.ar/Gioja.webp

---
El, ya vetusto, Modelo de Protocolo Latinoamericano de investigación de las muertes violentas de mujeres por razones de género (femicidio o feminicidio), detalla que, en un estudio mundial sobre homicidios del 2011, la Oficina de las Naciones Unidas contra la droga y el Delito (UNODC), señaló que las muertes violentas de mujeres eran principalmente causadas por sus parejas íntimas o en el marco de sus relaciones familiares, y que las mujeres tenían más probabilidades de morir dentro de sus hogares que fuera de este.

Ya entonces se sabía, a grandes rasgos, cómo prevenir un femicidio o, en todo caso, hacia donde apuntar para esclarecer el asesinato de una mujer. Se sabía que el agresor pertenecía al círculo íntimo o era conocido. Había denuncias (a veces muchas). Había signos, había evidencia, había pedidos de ayuda. Lo que no había: perimetrales,  tobilleras,  atención integral en violencias de género,  empatía y, mucho menos, sororidad.

Actualmente tenemos más herramientas, aun así los femicidios, contrariamente a lo especulado, aumentaron. El portal “Nodal” indica que, en el transcurso de este año (solo este año) ya hubo 168 femicidios, un 15% más que el año pasado. Un total de 168 casos de femicidios se registraron entre el 1 de enero y el 31 de julio de 2020 en nuestro país, lo que implica un 15% más de casos respecto al mismo período del año pasado, según un relevamiento realizado por el Observatorio de Femicidios de la Defensoría del Pueblo de la Nación, que atribuye el incremento a la dificultad de protección que atravesaron las víctimas durante la cuarentena por el coronavirus.

“El aislamiento social y preventivo contra el Covid-19 trajo aparejada la prohibición de la libertad o resguardo de la convivencia de la víctima con su agresor, y con ella, la dificultad de protegerse, independientemente de los diferentes dispositivos de comunicación que se implementaron”, indica el informe mencionado.

<br/>

![](https://assets.3dnoticias.com.ar/femicidiosmumala.jpg "MUMALA")

<br/>

Según el Observatorio de mujeres MUMALÁ, en el 44% de los casos, la víctima y victimario convivían, el 12% de las víctimas estuvo desaparecida, el 5% fue abusada sexualmente, el 3% era trabajadora sexual o estaba en situación de prostitución, el 6 % de las mujeres era migrante de otro país, el 4 % de las mujeres era migrante de otra provincia y el 2 % estaba en situación de calle.

El informe publicado por Nodal detalla que, del total de casos registrados por la defensoría, 13 fueron femicidios vinculados -es decir, aquellos en los que el propósito del agresor es provocar sufrimiento a la víctima, un agravante incluido en el inciso 12 del artículo 80 del Código Penal.

Las estadísticas crecen directamente proporcionales a cada femicidio. La semana pasada, Paola Tacacho de 32 años, fue asesinada de seis puñaladas en plena calle Tucumana. Su exalumno, Mauricio Parada Parejas, la tomó por la espalda cuando ella salía del gimnasio. Luego de matarla, se clavó el cuchillo en el corazón y murió en el lugar, rodeado de los vecinos que intentaron reducirlo tras el ataque. Parada Parejas no convivía con su víctima, no pertenecía a su círculo íntimo. Apenas la conocía, pero estaba obsesionado con ella.

El asesino de Paola Tacacho la hostigó durante, al menos,  tres años y había sido denunciado por la víctima trece veces en Tucumán y dos en la provincia de Salta. Quince denuncias tenía su asesino. Quince pedidos de ayuda no fueron escuchados.

Las mujeres no “aparecen muertas”: las matan; las matan los hombres, sus parejas, sus agresores están ahí, en su círculo íntimo, siempre estuvieron ahí. El Estado no.

"Hay una negligencia seria y esperamos que se tomen las medidas necesarias porque no es la primera vez que esto sucede en la provincia y refleja que hay un sector de la Justicia que no toma en serio las denuncias", sostuvo Marieta Urueña Russo, representante del colectivo _Ni Una Menos_ de Tucumán.

Tucumán es la segunda provincia del país con mayor tasa de femicidios: 5,31 por cada millón de habitantes. Primera está Jujuy, con una tasa de 7,78 casos por cada millón de habitantes. Y en tercer lugar Misiones, con una tasa de 4,76 cada millón.

En los casos de Santa Fe y de Misiones, los femicidios se duplicaron respecto al mismo período de 2019, mientras que en Córdoba disminuyeron un 50%, en Mendoza un 80% y en Corrientes y Entre Ríos un 35% en cada una.

El portal Nodal indica que los datos arrojados por el Observatorio _Ahora que sí nos ven_, al igual que el Observatorio de Mumalá, ponen a Santa Fe en un nuevo récord. De los 223 femicidios contabilizados desde enero hasta septiembre, 29 son de nuestra provincia. La segunda en cantidad absoluta en relación al número de habitantes.

No solo la pandemia del Covid-19 nos ubica como una provincia con más asesinatos de mujeres, sino que también por violencia de género. Santa Fe vuelve a ser parte de las áreas con mayor cantidad de femicidios en lo que va del año 2020.

La Ley nacional N° 26.485 de Protección Integral para Prevenir, Sancionar y Erradicar la Violencia contra las Mujeres comprende todos los ámbitos donde se desarrollen sus relaciones interpersonales en todas sus modalidades doméstica, laboral, institucional, contra la libertad reproductiva, obstétrica y mediática. Asimismo, en el Art 5 establece los siguientes tipos: violencia sexual, psicológica, física, simbólica o económica.

Al respecto, Liliana Loyola, presidenta de la Asociación Civil Generar expresa que “todos estos tipos de violencia son la parte que no se ve del iceberg y el femicidio es la punta de este iceberg. Es la violencia más extrema, la culminación de toda una situación de padecimiento, como le pasó a Paola (Tacacho)”.

Marcela Lagarde, politóloga mexicana y representante del feminismo latinoamericano, tipifica el femicidio como feminicidio cuando el Estado no da garantías a las mujeres y no les crea condiciones de seguridad para sus vidas ya sea en la comunidad, en la casa, en los espacios de trabajo, de tránsito o de esparcimiento.

El Estado es responsable de estas muertes y de todo lo que no se hizo para evitarlas. Loyola advierte que “para erradicar la violencia contra las mujeres es imperante el trabajo en prevención, poner la violencia de género en la agenda, muchísima más política pública, aplicación inmediata de la ESI y la Ley Micaela, el cumplimiento efectivo de la Ley de Paridad, aceitar los mecanismos de denuncia de hechos de violencia de género y sobre todo desarticular los estereotipos patriarcales”.

Si bien el gobernador de Santa Fe, Omar Perotti, recientemente se refirió a un abultado presupuesto para 2021, destinado a brindar ayuda a mujeres en situación de vulnerabilidad, es urgente que se empiecen a tomar medidas en el corto plazo y que las iniciativas no queden en lo discursivo.

La Asociación Civil Generar trabaja desde hace varios años en el tema de la violencia de género en la ciudad de Santa Fe. La institución se encarga de brindar asesoramiento, prevención y capacitaciones a mujeres en situación de vulnerabilidad. Para el 25 de noviembre, día internacional de la eliminación de la violencia contra la mujer, tienen previstas diferentes actividades en torno a esta problemática y hacen extensiva la invitación a quienes quieran formar parte. Las actividades serán hasta el 10 de diciembre, día de los derechos humanos.

**Si sos victima de violencia de género llamá a la línea gratuita 0800 777 5000. Atienden todos los días del año a cualquier hora. También podés solicitar asesoramiento o ayuda a la línea nacional 144.**

<br/>

<hr style="border:2px solid red"> </hr>

**Sofía Gioja en** [**twitter**](https://twitter.com/SofiG83)