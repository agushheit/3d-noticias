---
layout: analisis
category: analisis
date: 2021-01-13T13:28:06Z
author: Por José Villagrán
title: Sectores del radicalismo «apuran» al socialismo para conformar un nuevo frente
  electoral
thumbnail: https://assets.3dnoticias.com.ar/Villagran.webp

---
##### Con Maximiliano Pullaro y Felipe Michlig a la cabeza, radicales ya piensan en el armado electoral de cara a los comicios de medio término. Sostienen que para ganarle al peronismo hay que ampliar el frente.

***

![](https://assets.3dnoticias.com.ar/portada-13121.webp)

La escena política en la provincia de Santa Fe comienza a escribir sus primeras líneas en este 2021.

Los primeros en dar el paso fueron los radicales. O por lo menos un sector de ellos. Es que, a fines de la semana pasada, algunos dirigentes se reunieron para ir dándole forma al armado electoral de este año y lograron acordar la construcción de un frente amplio para ser competitivo, tanto en las elecciones provinciales como nacionales. Así lo expresaron Maximiliano Pullaro, Felipe Michlig y el ex intendente de la ciudad capital, Mario Barletta, en sus redes sociales.

[![Ver en twitter](https://assets.3dnoticias.com.ar/tweet-pullaro-070121.webp)](https://twitter.com/maxipullaro/status/1347353585886097411?ref_src=twsrc%5Etfw)

La pregunta del millón es: ¿a quién incluye ese «Frente Amplio» del que habla el radicalismo? ¿A quién se le abren las puertas y quién se les cierran?

En principio, ese frente es opositor. Así que las puertas están cerradas para cualquier dirigente peronista. Después, las puertas se le abren a cualquier sector. De hecho, este fin de semana, el exministro de Seguridad dijo que «para ganar tenemos que convocar a todos los sectores que piensen o estén dispuestos a pensar una alternativa diferente en la provincia». Se desprende de la declaración realizada, que tiene miras en los próximos comicios de mitad de año, pero también se mira de reojo el 2023.

<br/>

## **¿Quiénes son los que impulsan este nuevo frente electoral?**

Pullaro y Michlig son las cabezas visibles de esta iniciativa. Pero detrás de ellos hay otros nombres. Están el ex vicegobernador Carlos Fascendini (quien, junto a los otros dos nombrados, son los «radicales progresistas», los más allegados al socialismo), Julián Galdeano del sector del Mar, y Mario Barletta y Hugo Marcucci, la pata universitaria. Fuera, por ahora, está quedando el ex candidato a gobernador José Corral, que todavía no olvida el «fuego amigo» que padeció en las últimas elecciones.

Lo que marca una clara intención: marcar la unidad de la UCR, por un lado, y formar parte de la discusión fuerte, sin quedar relegado como venía sucediendo hasta las elecciones de 2019, donde se tenían que conformar con los segundos puestos en el armado de las listas, detrás del candidato socialista.

Algunos dirigentes radicales ven para las próximas elecciones, tanto en la provincia como en la Nación, un escenario de polarización.

En Santa Fe, la disputa se marcará entre el peronismo y la candidatura de Miguel Lifschitz. A nivel nacional, entre el Frente de Todos y (¿el reconvertido?) Juntos Por el Cambio.

Entienden que es casi imposible lograr una tercera alternativa que en estos meses pueda posicionarse como oferta electoral, y que, si no se logra la unidad del partido, la victoria del peronismo está asegurada. Además, ven que si no hay una estrategia a nivel nacional que baje mancomunadamente a la estrategia provincial, se obtendrán resultados dispares que pueden atentar contra la construcción a futuro.

De alguna manera se le marcó la cancha a Miguel Lifschitz dentro del Frente Progresista. Y lo hace un sector allegado al Partido Socialista.

<br/>

## **¿Qué se dice desde el socialismo?**

Hay varios aspectos a tener en cuenta en este ajedrez político de alianzas y frentes. El primero es que el Partido Socialista tiene elecciones partidarias y hay algunos sectores que entran en pugna. Al igual que la UCR, hay tres espacios que juegan internamente: el de Miguel Lifschitz, el de Antonio Bonfatti y el de Edgardo Di Polina. Los dos primeros peleados desde la gestión del dirigente rosarino, aunque las informaciones indican que hubo un acercamiento en los últimos tiempos. El tercero, con profundas diferencias a tal punto de terminar apoyando la candidatura a presidente de Alberto Fernández, cuando el resto del partido no se expresó.

Nadie habla de lo que hará Lifschitz, pero todos dan por descontado que será candidato a senador nacional. Desde que dejó su función de gobernador, continuó recorriendo pueblos y ciudades del interior de la provincia, afianzando su buena imagen y el hecho de ser quien tiene todos los votos dentro del Frente Progresista.

La decisión que tome el exmandatario se ajusta a lo que pueda pasar en 2023.

Es que, fuentes cercanas al presidente de la Cámara de Diputados aseguran que su futuro está en competir nuevamente por la Casa Gris dentro de dos años y la senaduría le quitaría presencia en el territorio, algo que está plasmando activamente.

Pero, además, hay una interna a dirimir dentro del Frente Progresista. La idea que plantea el radicalismo puede dejar heridos y huérfanos. La postura que caracterizó siempre al socialismo fue la de ser una alternativa de construcción política distinta al kirchnerismo y al PRO. Lograr identidad propia por encima de las polarizaciones. Algo que le trajo doce años de gobierno en la provincia, pero escasos resultados positivos en las elecciones generales.

En toda esta discusión entran en juego los intendentes de Santa Fe y Rosario, con posturas similares y muy cercanas en la visión política.

Emilio Jatón sostiene que al Frente hay que consolidarlo y que la apertura tiene que ser selectiva. Su límite es el radicalismo de José Corral y compañía, es decir, el universitario.

Pero también está Pablo Javkin, intendente de la ciudad del sur, que tiene vuelo propio y con el que el socialismo no comulga mucho. Es que todavía les cuesta digerir la derrota en su ciudad bastión y con alguien que está construyendo su espacio CREO en varias partes de la provincia, dialoga y negocia con el PRO en el Concejo Municipal y, como si esto fuera poco, tiene buena relación con la Casa Gris.

De hecho, quien se refirió a la «oferta» que planteó el radicalismo fue el diputado de la Coalición Cívica Ariel Bermúdez, que de plano descartó la alianza. «Es preferible perder mil elecciones que los valores y las convicciones. A veces lo amplio, de tan amplio, se destiñe hasta perder el color (...) si no fuimos a la fiesta de cumpleaños de Cambiemos, mucho menos iremos al velorio», avisó.

[![Ver en twitter](https://assets.3dnoticias.com.ar/tweet-bermudez-070121.webp)](https://twitter.com/dipbermudez/status/1347373182257422338?ref_src=twsrc%5Etfw)

Pero, además, respondió con dureza a la crítica del radical Julián Galdeano, que expresó que «Javkin es un buen dirigente, pero le falta equipo para dirigir la ciudad de Rosario». Declaración que se leería con la frase vulgar «con una mano te acarició y con la otra te abofeteo». Bermúdez utilizó su cuenta de Twitter para recalcarle que apoyaron desde su sector «al mejor equipo de los últimos 50 años».

Parecería que, internamente, la respuesta a la invitación está clara. No se avizora en el horizonte un frente amplio donde confluyan los sectores opositores a los gobiernos peronistas.

Desde el PRO encabezado por Federico Angelini, ven con buenos ojos la jugada movida por la UCR. Es más, el propio dirigente santafesino admitió que se trabaja para incorporar al radicalismo progresista y a los socialistas. «No hay otra, es más, para ganarle al peronismo hay que extender la coalición a otros sectores, como el de Amalia Granata y más», apuntó.

Así están dadas las cosas en los primeros días del 2021, donde los ojos se posan en la mitad del año, pero no se pierde de vista la carrera completa: llegar a los sectores de poder en 2023.

<hr style="border:2px solid red"> </hr>

**José Villagrán en** [**twitter**](https://twitter.com/joosevillagran)

Otras columnas del autor en este portal:

[El 2020 dejó 375 homicidios en la provincia de Santa Fe](https://3dnoticias.com.ar/analisis/el-2020-dejo-375-homicidios-en-la-provincia-de-santa-fe/) | [El peronismo cerró filas y mostró apoyo al gobierno de Perotti](https://3dnoticias.com.ar/analisis/el-peronismo-cerro-filas-y-mostro-apoyo-al-gobierno-de-perotti/) | [La Santa Fe de Perotti: sin paz ni orden](https://3dnoticias.com.ar/analisis/la-santa-fe-de-perotti-sin-paz-ni-orden/) | [Un año de Perotti gobernador](https://3dnoticias.com.ar/analisis/un-ano-de-perotti-gobernador-pandemia-disputas-internas-y-un-gobierno-sin-articulacion/) | [El presupuesto 2021 de Omar Perotti](https://3dnoticias.com.ar/analisis/el-presupuesto-2021-de-omar-perotti-que-dice-como-fue-votado-y-cuales-son-las-criticas/)