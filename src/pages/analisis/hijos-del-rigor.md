---
layout: analisis
category: analisis
date: 2020-11-28T13:52:09.000+00:00
author: Por Marisa Lemos
title: Hijos del rigor
thumbnail: https://assets.3dnoticias.com.ar/Marisa-Lemos.webp

---
Nunca pensamos que esto iba a pasar, era lejano a nosotros, pero llegó. Me acuerdo cuando vi en la tele lo que estaba pasando al otro lado del mundo, pedía a Dios por ellos y a la vez pensaba que esto nunca iba a llegar acá. Qué pensamiento tonto, ¿no?

De la nada y rápido como la luz, fue ganando terreno a pasos agigantados y seguíamos pensando que esto nunca iba a llegar acá, que era imposible. Pero llegó. Llegó para cambiarle la vida, la rutina, los proyectos a TODOS Y A TODAS y, aunque no nos dimos cuenta, la oscuridad tocó suelo argentino.

Tuvimos que cambiar varios aspectos de nuestro día a día, acostumbrarnos al estar encerrados todo el tiempo, a usar barbijo, a lavarnos continuamente las manos, a utilizar alcohol en gel, saludarnos con el codo, mantener la distancia y muchas cosas más que ya las sabemos y que no es necesario que las escriba. Pero lo más duro es evitar el contacto con nuestros seres queridos. ¿Cuesta? Si, muchísimo.

Al principio solo eran cifras que estaban muy por debajo de los demás países. Nos sentíamos aliviados y a la vez contentos de que nos tomen por ejemplo de lo bien que estábamos haciendo las cosas. De a poco, todo fue en aumento. Todo se fue por el tubo. Los casos aumentaron día a día y de la mano vino la muerte.

Y a la par de los casos también aumentaban los precios, la cifra de negocios cerrados, los casos de inseguridad y todo lo que rodea a esta pandemia, que no es causa directa, es “daño colateral” y todos, lamentablemente, lo conocemos bien.

¿Pensaban que el virus era joda, no? Algunos sí, otros no, pero el único modo de cambiar de opinión era cuando te tocaba de cerca. Como en mi caso. Yo pensaba que no nos íbamos a contagiar porque seguíamos el protocolo como se debía. Pero el bicho entró a casa de la mano de una persona inconsciente que se le ocurrió viajar en plena pandemia.

En mi caso, solo sufrí un resfriado común,  la que se llevó todos los números fue mi vieja. Verla en una pieza aislada, sola, me partía el corazón. Me encerraba en el baño a llorar del coraje y de la bronca que me daba verla ahí y no poder abrazarla, besarla y mucho menos acercarme. Fue horrible. Como me decía un amigo: “negra agradecé que está en tu casa y que está bien, gracias a Dios. Porque he visto personas que se las lleva la ambulancia y no vuelven”.

Eso era lo que le agradecía cada día, y lo sigo haciendo, porque gracias a dios mi mamá ya está bien. Ya la pasó.

Lo primero que hice fue ir corriendo a abrazarla y decirle cuánto la amaba. Siempre se lo digo, pero esta vez fue más de cerca y acompañado de un hermoso abrazo que fue el mejor que recibí.

Somos hijos del rigor, de eso no me queda duda. Solo cuando nos toca entendemos que esto es real, que no es un invento del gobierno, que la gente sí se muere y lo único que queda es dolor e incertidumbre.

Por eso, desde mi lugar te pido a vos que hagas las cosas como se debe. Que no pienses que porque sos joven o gozás de buena salud no te toca, no te llega. No pienses que sos inmune a todo, yo pensaba lo mismo.

Esperemos que esto pase, que llegue el día en que no haya ningún otro contagio y que al fin la oscuridad se disipe y vuelva a salir la luz.