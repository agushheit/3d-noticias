---
layout: analisis
category: analisis
date: 2021-01-05T11:57:27Z
author: Por José Villagrán
title: El 2020 dejó 375 homicidios en la provincia de Santa Fe
thumbnail: https://assets.3dnoticias.com.ar/Villagran.webp

---
Tales resultados se desprenden del informe preliminar realizado por la Secretaría de Política y Gestión de la Información y el Observatorio de Seguridad Pública. La Capital y Rosario, los departamentos más violentos de la provincia.

Números preocupantes deja el primer año de gestión de Marcelo Saín, al frente del ministerio de Seguridad de la provincia. Ya lo advertíamos en la nota del [17/12/2020 en _3DNoticias_](https://3dnoticias.com.ar/analisis/la-santa-fe-de-perotti-sin-paz-ni-orden/) cuando decíamos que el año que terminó, hace algunos días nada más, le competía «palmo a palmo» al 2016 como el año con más homicidios en la bota santafesina.

Al momento de escribir aquella nota, los homicidios llegaban a 360 en todo el territorio. Finalmente, **los estudios preliminares indican que Santa Fe terminó con 375 crímenes ocurridos durante todo el 2020, siendo los departamentos Rosario y La Capital los que encabezaron la tan lamentable estadística**. La nota de color de los números del año pasado es que el departamento Belgrano fue el único que terminó con sus registros en cero, es decir, sin muertes producidas por una persona diferente a la directamente interesada.

***

![](https://assets.3dnoticias.com.ar/violencia-santafe-1.webp)

***

La estadística también dice que se cometieron 1,024 muertes cada día. Es decir, más de un asesinato cada 24 horas. Del total de crímenes, 263 tuvieron lugar en la vía pública (el 70%), lo que habla del nivel de violencia que se vive en ciertos sectores de la provincia y donde las armas de fuego son las principales herramientas para cometer dichas matanzas.

Además, el estudio refleja que el 39,2% de las víctimas tenía entre 20 y 30 años, otro dato a tener en cuenta a la hora de analizar la realidad de los jóvenes.

En cuanto a los meses con más asesinatos, enero y septiembre encabezan el ranking (45 cada uno) seguidos por febrero (41) y marzo. En tanto, durante los meses de cuarentena estricta (abril – mayo), los valores tuvieron un brusco descenso (13 – 21, respectivamente), sosteniéndose en junio/julio y manteniéndose alto en los últimos cinco meses del año.

Así, **el 2020 termina siendo el más violento de los últimos cuatro años**, superado por poco, por el 2016 (382) e incrementándose considerablemente a 2019 (337).

***

![](https://assets.3dnoticias.com.ar/violencia-santafe-2.webp)

***

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">La Capital </span>**

**La ciudad de Santa Fe fue la más violenta de todo el departamento La Capital**, con 80 homicidios en el transcurso del año que acabó, de un total de 94. Los demás se reparten entre la ciudad de Santo Tomé (8) y las comunas de Sauce Viejo (4) y Monte Vera. De este modo, la violencia en el departamento capitalino termina siendo menor que en 2019 (103) y 2108 (94), pero mayor que en 2017 (82).

Los números se hacen más preocupantes cuando se los analiza con profundidad. El 75% de las muertes ocurrieron en la vía pública, superando la media a nivel provincial. Lo mismo ocurre a la hora de clasificarlos por rango etario: el 42,5% de las mismas tuvieron a víctimas de entre 20 y 30 años, con el agregado que la fase que va de 15 a 19 y de 30 a 34 presentan números casi idénticos (15 y 14 homicidios, respectivamente).

El primer trimestre fue el que registró los valores más elevados: 15 en marzo, 12 en enero y 10 en febrero, produciéndose un pico importante en septiembre, como en la provincia.

***

![](https://assets.3dnoticias.com.ar/violencia-santafe-3.webp)

***

La ciudad capital de la provincia cumple con todo lo anteriormente descrito, acaparando el 85% de los crímenes, de los cuales el 42,5% tuvo como víctimas a personas de entre 20 y 29 años.

***

![](https://assets.3dnoticias.com.ar/violencia-santafe-4.webp)

***

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700">Rosario</span>**

El 2020 mostró una Rosario fuera de control y sin respuestas visibles hasta el momento. La escalada de violencia, sumada a la crueldad de los episodios, tuvieron un núcleo formado en la ciudad del sur, pero también en las ciudades que están «pegadas», como lo son Granadero Baigorria y Villa Gobernador Gálvez.

En todo el departamento Rosario se produjeron 213 homicidios, siendo los meses de septiembre (28), febrero (27) y enero (23), los que más crímenes presentaron.

El patrón etario es levemente superior en cuanto a víctimas. Del total de asesinatos, 49 de los crímenes tuvieron como blanco a jóvenes de 19 a 24 años; y 40, a personas de 25 a 29 años. Entre ambas franjas, suman casi el 42%.

La violencia en el sur de la bota santafesina se evidencia con mayor crueldad al contemplar que el 78% ocurrió en la vía pública y, en número similar, fueron cometidos con armas de fuego (79,5%).

***

![](https://assets.3dnoticias.com.ar/violencia-santafe-5.webp)

***

Las ciudades con más homicidios en esa parte de la provincia son Rosario (con 171), Villa Gobernador Gálvez (con 22) y Granadero Baigorria (con 15).

En la ciudad de Rosario, el 80% de las muertes se vieron en la calle y casi el 47% vinculados al crimen organizado.

***

![](https://assets.3dnoticias.com.ar/violencia-santafe-6.webp)

***

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700"> Violencia de Género</span>**

El informe también contabiliza los casos de violencia de género en la provincia de Santa Fe durante el 2020. Según los datos oficiales, 24 mujeres fueron víctimas de la violencia machista, de las cuales 10 pertenecen al departamento Rosario y dos a La Capital.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.1em; font-weight: 700"> El 2021, con la misma violencia del 2020</span>**

El año nuevo no menguó para nada la violencia en la provincia, en general, y en los grandes cascos urbanos, en particular.

Heridos de arma blanca en los barrios San Agustín y Villa del Parque. Un nene de 9 años recibió un disparo de arma de fuego en la cabeza. Las primeras investigaciones hablan de una bala perdida en medio de los festejos. En barrio Centenario se investiga la muerte de un hombre de 61 años que presentaba en su cuerpo diversas heridas. Heridos de arma de fuego en barrios Santa Rosa de Lima, Los Troncos y El Pozo. Todos esos hechos, cometidos en la ciudad de Santa Fe.

El fin de semana sacudió a la ciudad de San Justo con el fallecimiento de Hugo Salvatelli, un referente de la Seguridad Privada en ese lugar. Salvatelli resultó gravemente herido (con traumatismo de cráneo) como consecuencia de un fuerte golpe que le propinó un masculino, mayor de edad, identificado como alías «Pololo», en las primeras horas del viernes 1° de enero, cuando los festejos de año nuevo organizados por el municipio local comenzaban a tener su fin.

Como si todo eso fuera poco, el domingo por la noche un hombre de 40 años fue asesinado con al menos cuatro balazos, por agresores que le dispararon desde un auto, cuando caminaba por una calle de la ciudad de Rosario.

<hr style="border:2px solid red"> </hr>

**José Villagrán en** [**twitter**](https://twitter.com/joosevillagran)

Otras columnas del autor en este portal:

[El peronismo cerró filas y mostró apoyo al gobierno de Perotti](https://3dnoticias.com.ar/analisis/el-peronismo-cerro-filas-y-mostro-apoyo-al-gobierno-de-perotti/) | [La Santa Fe de Perotti: sin paz ni orden](https://3dnoticias.com.ar/analisis/la-santa-fe-de-perotti-sin-paz-ni-orden/) | [Un año de Perotti gobernador](https://3dnoticias.com.ar/analisis/un-ano-de-perotti-gobernador-pandemia-disputas-internas-y-un-gobierno-sin-articulacion/) | [El presupuesto 2021 de Omar Perotti](https://3dnoticias.com.ar/analisis/el-presupuesto-2021-de-omar-perotti-que-dice-como-fue-votado-y-cuales-son-las-criticas/) | [Todos contra Saín](https://3dnoticias.com.ar/analisis/todos-contra-sain-el-ministro-que-resiste-los-embates-de-propios-y-extranos/) | [El gabinete de Perotti: un equipo rodeado de rumores](https://3dnoticias.com.ar/analisis/el-gabinete-de-perotti-un-equipo-rodeado-de-rumores/) | [Que no se den PASOS en falso](https://3dnoticias.com.ar/analisis/que-no-se-den-pasos-en-falso/)