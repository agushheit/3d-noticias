---
layout: analisis
category: analisis
date: 2020-12-17T11:46:06Z
author: Por José Villagrán
title: 'La Santa Fe de Perotti: sin paz ni orden'
thumbnail: https://assets.3dnoticias.com.ar/Villagran.webp

---
Fue el lema de campaña del justicialismo en 2019 para llegar a la gobernación, haciendo referencia a los altos índices de violencia que dejaba el socialismo tras doce años de gobierno. En su primer año de gestión, los crímenes y el desorden institucional estuvieron a la orden del día.

Durante todo el 2019, año de elecciones gubernamentales, los crímenes en la provincia de Santa Fe llegaron a ser 337, produciéndose un leve descenso del 5,6% con respecto al año anterior, donde llegaron a 357 los homicidios.

Desde que Rosario comenzó a perfilarse como una ciudad cooptada por el narcotráfico, el desfile de ministros que pasaron por el ministerio de Seguridad sin resolver el problema crítico de la violencia hizo de esta una problemática ascendente. Hasta que llegó Raúl Lamberto a mediados del 2012 y terminó de hundir al socialismo en esa tendencia. Los años más duros durante la gobernación del Frente Progresista se dieron en 2013, 2014 y 2015, plena gestión de Antonio Bonfatti.

**En 2014 se produjo el pico de muertes en Santa Fe**: 461 homicidios se contabilizaron en ese año según datos del Observatorio de Seguridad Pública de la provincia. En el ranking siguen el 2013 y 2015 con 438 y 432, respectivamente. Con un agregado más: nunca, en los doce años, el socialismo pudo bajar de 270 crímenes por año, logrando desde ese momento establecer un piso muy alto que hasta el día de hoy no se pudo romper.

![](https://assets.3dnoticias.com.ar/1712-opinion.png)

En ese contexto se basó, como principal estrategia, Omar Perotti para ganar las elecciones a gobernador del año pasado, con el tan famoso eslogan «Paz y Orden». Paz y orden que hoy están en jaque.

## **La Paz**

El santafesino está lejos de alcanzarla. Los robos, las muertes y los ajustes de cuenta ya son moneda corriente en la provincia. Se nos hizo cotidiano despertarnos y ver entre las principales noticias los crímenes y situaciones de violencia como portada principal.

El 8 de enero de este año, el ministro de Seguridad de la provincia, Marcelo Saín, mostró en conferencia de prensa los números de homicidios a los cuales estamos referenciando de los últimos años. Para ese momento, Santa Fe empezaba a inaugurar el año con 10 muertes en Rosario y 3 en Santa Fe Capital. Todo un anticipo de lo que sería el transcurrir del 2020.

<span style="color: red;">«Hay un contexto social de alto nivel de violencia y esa violencia está legitimada»</span>, dijo en esa ocasión Saín, comentando la crítica situación que se comenzaba a vivir.

Es cierto que la «herencia» que recibieron Perotti, Saín y compañía es pesada, complicada y compleja de resolver, pero también es cierto que en este primer año de gestión el peronismo no ha hecho mucho por resolverlo.

En los primeros cuatro meses, las muertes llegaron al número de 134 en todo el territorio, siendo los departamentos Rosario y Santa Fe los que mayor índice presentaron. Enero fue el más violento en materia de asesinatos; febrero no tenía tantos crímenes desde 2014; y marzo y junio desde 2016. Asimismo, abril de este año tuvo solo 13 homicidios, el número más bajo.

***

![](https://assets.3dnoticias.com.ar/1712-opinion1.png)

***

![](https://assets.3dnoticias.com.ar/1712-opinion1-1.png)

***

![](https://assets.3dnoticias.com.ar/1712-opinion2.png)

***

<br/>

El primer semestre cerró, con pandemia y cuarentena de por medio, con 189 muertes en toda la bota santafesina, de las cuales la mitad se dieron en el departamento Rosario y 51 en La Capital.

***

![](https://assets.3dnoticias.com.ar/1712-opinion3.png)

***

Si seguimos la progresión de los homicidios en Santa Fe notaremos que, pasado el mes de agosto de este año, los asesinatos llegaron a ser 247, una cifra superior a las contabilizadas en el mismo período de los años 2017, 2018 y 2019.

El departamento Rosario registró 129 homicidios entre enero y agosto de 2020. Esta cifra resulta más elevada que la registrada en 2019. En tanto, en el departamento La Capital la cifra alcanzó el número de 65 homicidios registrados en los primeros ocho meses del año., mostrando así la tendencia de crecimiento paulatino iniciada en el 2018, colocándose bastante por encima del número de 2017.

***

![](https://assets.3dnoticias.com.ar/1712-opinion4.png)

***

![](https://assets.3dnoticias.com.ar/1712-opinion5.png)

***

Pero si seguimos el recorrido por el resto de los meses de este 2020, los números se vuelven más escalofriantes. En septiembre ocurrieron 39 asesinatos, el más violento de los últimos tiempos. Noviembre terminó con 26, el número más bajo desde 2014. Este año, por ahora, es el que tiene mayor cantidad de asesinatos de los últimos cuatro. Hasta el 07 de diciembre se acumulaban 360 homicidios en la provincia: 164 en la ciudad de Rosario y 81 en Santa Fe capital, siendo las dos ciudades más violentas de la bota. Y hablo en pasado porque en diez días, por ejemplo, ocurrieron 3 muertes más en el departamento La Capital (uno en Sauce Viejo, otro en Santo Tome y el restante en Santa Fe ciudad).

<br/>

![](https://assets.3dnoticias.com.ar/1712-opinion6.png)

<br/>

Por lo que la estadística marca, que en toda la provincia de Santa Fe se produce más de un crimen por día, y si lo aplicamos a la ciudad capital, los números arrojan que hay una muerte violenta cada cuatro días.

<span style="color: red;">Lo que lleva a que este 2020 le pelee «palmo a palmo» el primer puesto al 2016 del año más violento de los últimos cuatro.</span>

<br/>

## **El orden**

Hay una realidad generalizada que comparten peronistas y frentistas del progresismo. Durante los doce años de gestión socialista, las críticas del justicialismo hicieron principal hincapié en lo deficitario que resultaban las políticas en materia de seguridad. En este primer año de gestión perottista, la mayoría de las críticas van dirigidas a ese punto también. ¿Casualidad?

Desde hace varios años que se viene insistiendo que uno de los problemas que tiene Santa Fe en materia de inseguridad es la policía. De hecho, acá también hay otra coincidencia. En 2012, primer año de la gestión Bonfatti, se intentó una reforma policial que después quedó trunca el escándalo del Jefe de la Policía Hugo Tognoli.

Ocho años después, Omar Perotti envió a la Legislatura tres proyectos de ley que buscan reformar la policía. Es decir, hay una lectura que realiza la política santafesina que apunta a la **necesidad de cambiar las estructuras policiales** para que dejen ser parte del problema y comiencen a ser parte de la solución.

En la primera conferencia de prensa del año que dio el ministro Marcelo Sain apuntó contra esto. «La presencia policial en la calle es muy importante, pero tiene que ser planificada. Lo que encontramos en Santa Fe es que sale a hacer patrullas al tuntún. No haya mapeo criminal, incluso en Rosario, donde se postuló que había una tecnología de planificación con aquello que le llamaban "el Ojo". Eso lo estamos reconvirtiendo».

Pero además advirtió el desorden institucional contabilizando que «de los 21500 policías que hay, apenas 12 mil están destinados a tareas preventivas. Si eso se divide en cuatro cuartos y se le descuenta un 20% por tareas administrativas en las unidades operacionales, tenemos un promedio de 2900 policías en tiempo real haciendo prevención en la provincia de Santa Fe».

El desorden que atraviesa la provincia está claro y es muy visible a los ojos de la sociedad santafesina. Cuando la gente afirma en las distintas manifestaciones que «la policía no se ve en la calle», tiene razón. Pero no hay que confundirse. No es esta la única razón.

**Hay un desorden que proviene de la conducción política.** Hay un tercer punto en el que el Frente Progresista y el Partido Justicialista coinciden. Ambos, en distintos momentos, fueron a buscar asesorarse por quien hoy maneja el ministerio de seguridad.

Es conocido que Marcelo Saín concursó para dirigir el Organismo de Investigaciones del Ministerio Público de la Acusación. Pero los vínculos llegan, incluso, cuando Miguel Lifschitz era intendente de Rosario. El resto de la historia es conocida, y ya la hemos contado en este portal. Fue el propio gobernador electo Omar Perotti quien lo buscó para ser ministro.

***

![](https://assets.3dnoticias.com.ar/1712-opinion7.png)

***

Entonces, la pregunta que surge es la siguiente: si el análisis que se hace de la realidad es casi el mismo, las ideas generalizadas van en un mismo rumbo y se piensa en la misma persona para que las resuelva (o sea parte de la solución), **¿por qué pasa lo que pasa en Santa Fe?**

**Es la pregunta que hoy la política santafesina no responde**. Socialista y peronistas coinciden en que Saín es la persona mejor preparada académicamente para entender la seguridad pública. ¿Qué pasa en el medio entonces?

¿La violencia de la provincia es de ahora? Ya vimos que no. La cosa viene desde mucho tiempo atrás.

¿Se profundizó en este primer año de gestión de Perotti? Si, los números también lo indican.

¿Hay un plan de gobierno en materia de seguridad? Eso es lo que no sabemos, porque ni el ministro ni el gobernador lo mencionan.

¿Es cambiar la flota de patrulleros? ¿Son las tres leyes que se enviaron a la Legislatura y a partir de ahí empezar a gestarlo? ¿Es combatir las mafias que están enquistadas en el poder y se mezclan con el poder político de turno y por eso Saín se pelea con propios y extraños? ¿Es cambiar las cúpulas policiales de las unidades regionales y las presente el propio gobernador y no el ministro como ha sucedido en Rafaela, por ejemplo? Nadie lo sabe.

En esta realidad vive la provincia de Santa Fe, a la cual le prometieron «Paz y Orden» por cuatro años y no la encuentra desde hace mucho tiempo.

**<hr style="border:2px solid red"> </hr>**

**José Villagrán en** [**twitter**](https://twitter.com/joosevillagran)

Otras columnas del autor en este portal:

[Un año de Perotti gobernador](https://3dnoticias.com.ar/analisis/un-ano-de-perotti-gobernador-pandemia-disputas-internas-y-un-gobierno-sin-articulacion/) | [El presupuesto 2021 de Omar Perotti](https://3dnoticias.com.ar/analisis/el-presupuesto-2021-de-omar-perotti-que-dice-como-fue-votado-y-cuales-son-las-criticas/) | [Todos contra Saín](https://3dnoticias.com.ar/analisis/todos-contra-sain-el-ministro-que-resiste-los-embates-de-propios-y-extranos/) | [El gabinete de Perotti: un equipo rodeado de rumores](https://3dnoticias.com.ar/analisis/el-gabinete-de-perotti-un-equipo-rodeado-de-rumores/) | [Que no se den PASOS en falso](https://3dnoticias.com.ar/analisis/que-no-se-den-pasos-en-falso/)