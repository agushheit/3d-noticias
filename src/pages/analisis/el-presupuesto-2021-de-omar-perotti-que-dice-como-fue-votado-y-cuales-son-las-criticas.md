---
layout: analisis
category: analisis
date: 2020-12-03T11:10:32.000+00:00
author: Por José Villagrán
title: 'El presupuesto 2021 de Omar Perotti: qué dice, cómo fue votado y cuáles son
  las críticas'
thumbnail: https://assets.3dnoticias.com.ar/Villagran.webp

---
La semana pasada el Senado lo votó casi en su totalidad. Diputados lo trató esta semana y lo devolvió a la Cámara Alta con modificaciones. Los bloques de izquierda, los que votaron negativamente.

En tan sólo cinco días, Omar Perotti logró que haya acuerdo entre los distintos bloques políticos de la Legislatura para que traten y casi le den sanción definitiva al presupuesto que va a regir el ejercicio 2021 del gobierno provincial.

La iniciativa contempla gastos por 518.812.624.000 pesos y recursos por 510.165.597.000 pesos. Es decir que hay previsto un déficit por algo más de 8.000 millones de pesos.

El crecimiento de los recursos respecto al 2020 es de 27,1%, mientras que las erogaciones suben un 27,6%. También el crecimiento estimado del PBI es del 5,5%; la inflación del 29%; el tipo de cambio será de 102,4 pesos por dólar; el consumo público crecerá un 2%; la inversión el 6,6% y el consumo privado el 5,6%.

### **El Senado, casi por unanimidad**

En la previa al tratamiento del proyecto, los legisladores de la oposición salieron a pedir que la estimación presupuestaria sea fruto del "consenso" y "diálogo político".

Eso en palabras del jefe de la bancada del Frente Progresista en el Senado, Felipe Michlig. "Buscamos que tenga una visión territorial equitativa, en donde se contemplen obras prioritarias a lo largo y a lo ancho de la geografía provincial", había expresado el representante del departamento San Cristóbal.

Inmediatamente, desde el gobierno provincial salieron a pasarle factura por lo sucedido hace exactamente un año donde, en medio de la transición de cambio de gobierno, Miguel Lifschitz decidió presentar una evaluación económica para este año donde no ejercería la gobernación y sin consultarle al gobernador electo.

"Tienen memoria selectiva. De cómo fueron las cosas deciden no acordarse. Por eso sorprende que tan libremente pidan consenso en presupuesto, cuando el año pasado no participamos de la elaboración para este año" manifestaron fuentes del ejecutivo provincial.

Los senadores peronistas tuvieron que convencer al Ministro de Economía Walter Agosto, explicándole que, si se oponía a las modificaciones que venían planteando los diputados frentistas, (y varios de ellos también) el presupuesto no pasaría el filtro de Diputados, donde el FPCyS tiene mayoría.

En ese clima se llegó a la sesión del jueves pasado, donde se dio la primera discusión, y sorpresivamente la Cámara Alta de la Legislatura logró lo que Michlig reclamaba en la previa: consenso político producto del diálogo.

En primer término y por una moción elevada por el senador Alcides Calvo (Castellanos), se modificó el orden de la sesión para tratar el proyecto de Presupuesto General de Gastos y Cálculo de Recursos correspondiente al Ejercicio Económico 2021. Al tratarse sobre tablas, el mensaje obtuvo media sanción de manera unánime y fue girado a la Cámara de Diputados.

Es que 57 de los 58 artículos en lo que consta el proyecto fueron aprobados de forma unánime. Tan sólo uno (en el que el gobierno provincial tenía expectativas) fue el que "dividió las aguas". No con la oposición, sino dentro del peronismo.

Es que el artículo 53 hablaba de la posibilidad que tenía el Ejecutivo de derogar a través de un decreto la ley que daba marco al "Plan Abre" (una de las políticas sociales más defendidas por la anterior gestión) para darle paso al "Plan Incluir", una política social donde el gobierno de Omar Perotti pretende llegar a la totalidad de municipios y comunas, cuestión que no sucedía con el anterior programa. Aquí, se produjo el contrapunto.

El senado aprobó la derogación de la Ley 13.896 con los votos afirmativos de los senadores y senadora del PJ y la abstención de los legisladores del bloque de la UCR. Pero en lo que estuvieron de acuerdo es que la iniciativa social de la actual gestión sea ingresada como proyecto de ley, para su análisis y posterior debate.

Esto contó con los 7 votos del FPCyS y 6 votos del peronismo que comanda el senador por el departamento San Lorenzo, es jefe de la bancada oficialista y está en relación tensa con funcionarios provinciales.

Quien salió a manifestar esta decisión fue el secretario de Articulación de Políticas Públicas, Marco Corach. Habló de "ninguneo" al Plan Incluir y que se buscará la forma de "gobernar sin que este revés nos impida llegar a todos".

![](https://assets.3dnoticias.com.ar/tw1.png)

### **En Diputados, las mayores críticas**

La Cámara de Diputados aprobó con modificaciones el proyecto de Presupuesto que había ingresado con media sanción del Senado.

La iniciativa contó con la aprobación de por los bloques del Justicialismo, Frente Progresista, Juntos por el Cambio, Somos Vida y Familia y del Frente Renovador.

Se opusieron los bloques del Frente Social y Popular-Ciudad Futura e Igualdad. En tanto que la diputada de Somos Vida, Amalia Granata, se abstuvo al momento de la votación en desacuerdo con el texto votado.

En esta instancia, las críticas al ejercicio 2021 que plantea el gobierno provincial fueron variadas. "No votamos una serie de discrecionalidades, en acuerdo y consenso con un grupo de senadores.

Discrecionalidades como tener libre disponibilidad de los fondos extrapresupuestarios; la posibilidad de transferir partidas de capital a partidas de cuenta corriente, que hemos limitado al 40%; o de vender sin previa autorización bienes ociosos de la provincia.

Tampoco aprobamos el endeudamiento en dólares que pidió el gobernador por 154 millones dada la compleja situación que estamos viviendo", dijo el diputado radical Fabian Bastía, presidente de la Comisión de Presupuesto y del ala del pullarismo.

Por su parte, la diputada socialista Clara García añadió que “la aprobamos aún sin haber tenido un ministro de Gobierno con quien dialogar y con un ministro de Economía que no solo no concurrió a la Comisión de Presupuesto, sino que no respondió pedidos de informe que presentamos para contar con datos adicionales y tampoco cumplió con la publicación de datos oficiales en tiempo y forma”.

Julián Galdeano, de Juntos por el Cambio, recordó los anuncios realizados por el gobernador Perotti el 1° de Mayo ante la Legislatura, y advirtió que “muchos de esos anuncios no tienen anclaje en el presupuesto de 2021”, como el programa Santa Fe Conectada, Consejo Joven, el apoyo del Banco de Santa Fe en materia de tecnología, garantías para pymes”.

Del Frade dijo que es el sexto presupuesto que vota en contra porque siempre se repite lo mismo, que es "la resignación de la política contra el poder económico" y que eso impide "revertir la profunda inequidad que hay en la provincia".

Se quejó de los 600 millones de pesos asignados para la Secretaría de Género, que significan 13 centavos de cada cien pesos y aseguró que lo mismo sucede con el Ministerio de Ambiente que se lleva 700 millones de pesos cuando "la provincia está incendiada de norte a sur".

Por su parte, Giustiniani consideró que "el presupuesto no está a la altura de las circunstancias" ya que no atiende las necesidades de los trabajadores de la salud, de las pymes y de todos los que fueron golpeados por la pandemia. "Además, no entiendo el apuro de tratar sobre tablas semejante ley a cuatro días de haber ingresado a la Cámara", señaló el legislador.

La única abstención fue la de Amalia Granata, quien manifestó "no voy a acompañar un presupuesto cuya única certeza es la inversión en capital per cápita de $1.697 mensuales con lo que queda objetivamente expuesto que se trata de un presupuesto desequilibrado”.

Por último, el diputado del oficialismo Ricardo Olivera expresó su conformidad ante la votación positiva del presupuesto y presentó los cuatro objetivos del presupuesto: "la reconstrucción del entramado productivo; inversión pública; el desarrollo de sectores esenciales como el educativo, la salud, la cultura, social; y generar un modelo de desarrollo provincial, que sea federal e inclusivo, y con perspectiva de género".

***

##### **José Villagrán  en** [**twitter**](https://twitter.com/joosevillagran)