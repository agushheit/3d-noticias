---
layout: analisis
author: Por Sofia Gioja
category: analisis
title: Hasta que se metieron con el pan dulce
date: 2020-11-20T14:27:48.474+00:00
thumbnail: https://assets.3dnoticias.com.ar/Gioja.webp

---
¡Hash, la navidad! Si, gente. Falta casi un mes y ya podemos ver la nieve artificial en las vidrieras del centro, los moños rojos calcinados al sol y los “Papa Noel”abrigados hasta la médula. Llega la tradición exportada de _morfar_ como si fuésemos a morir mañana (este año, particularmente) y degustar cuanta bebida alcohólica esté disponible.

Cada 24 diciembre, ya sabemos, no pueden faltar en la mesa: el lechón en todas sus variedades, el vitel tone, la ensalada de papa y mayonesa, el viejo y confiable fiambre alemán y el asado. Algún italiano retobado come pasta, algún otro desubicado pregunta si hay pollo o ensalada verde. No importa. La cuestión es _morfar_ hasta aflojarse la cincha.

Ahora viene la mejor parte. Lo dulce. Toda esta montaña de calorías, más los 30° o 32° grados que hace afuera, no tienen sentido de ser sin las pasitas con chocolate (que se comen de a puñados), el turrón de maní, las garrapiñadas (que también se comen de a puñados, no sea careta), el clericó (con las sobras del _escabio_), la ensalada de fruta con helado y el pan dulce. Y me quiero detener, si me lo permite, en este último ítem alimenticio.

![](https://assets.3dnoticias.com.ar/pan-dulce.jpg)

Si, el pan dulce. Esponjoso y noble pan dulce. No, no voy a contar un cuento de Landrisina, basta. La cosa viene jodida gente. Pongámonos serios. Basta de joda. Quiero hacer una denuncia. Inadi, Defensoría del pueblo, ONU, Indec, señor presidente… a ustedes les hablo. ¿Cómo pueden permitir semejante aberración? ¿No van a hacer nada? ¡Nos quieren robar el pan dulce! (ayúdenme a gritarlo agitando bien fuerte el puño).

Ni siquiera voy a ponerme a despotricar contra quienes todavía no tomaron medidas serias con la prohibición de la fruta abrillantada. No me quiero poner polémica. Pero el veto ya tendría que tener media sanción en el senado.

Y déjenme decirles, también, que intentar vender el pan dulce a dos _lucas_ es de Grinch (ese personaje verde y feo, como Milei, que odia la navidad). No puede ser, viejo. Dos mil pesos un pedacito de felicidad. Hemos tocado fondo y lo digo con gracia, pero sin risa. Nos quieren _chorear_ en cuotas. ¿Cómo que no sabe nada? ¿Dónde vive usted? Mire, lea esto y me cuenta.

Resulta que una panadería porteña se hizo viral, hace unos días, por el precio que le puso al pan dulce. Imagínese Twitter: re contra explotó cuando leyeron que los precios iban desde los $1.000 hasta los $2.000 por un mísero pancito. Pero, además del afano en los precios, la propuesta sugería, así como quien no quiere la cosa, descuentos por reserva anticipada y hasta la posibilidad de hacer el pago en cuotas. ¿De cuánto eran las cuotas de la pelopincho?

Lucila Bassi, gerenta de marketing de Pani, la panadería que sacó "una promoción" que consiste en el descuento del 10% (si se hace la reserva anticipada de pan dulce), aclaró muy campante que el precio de estos productos responde a un valor agregado. Si, mirá mi cara.

"Los de 1,3kg tienen relleno de dulce de leche y los de 1,1kg de chocolate. **El pan dulce viene presentado en una lata porque el _packaging_ es parte del costo del producto**. El diferencial es el peso por el relleno y la presentación", dijo. ¡Pero démelo en bolsita nomás, así, sencillito y en alpargatas, si no es pa´ regalo, doña! Aparte, ya tengo costurero…

Basta no se ría. Es grave. Desde la Cámara de Panaderos de Capital tuvieron que salir a “tranquilizar” a los consumidores, diciendo que el precio por el kilo de un producto premium no debería pasar de los $1.000. “Cuanto más se acerca diciembre, estos productos tienden a levantar el precio, pero $2.000 es una barbaridad. En una panadería barrial el precio por kilo puede ir de los $250 a los $500 o llegar a los $1.000, siempre depende de la calidad del pan dulce y la panadería”, dijo José Álvarez, presidente de la entidad. Pero claro hombre...

Por su parte, la Asociación de Panaderos dijo que el aumento en general está impulsado, sobre todo, por el de los frutos secos. Ponele. “La fruta tiende a levantar el precio del pan dulce. Por ejemplo, para 10 kilos de harina ponés 11 kilos de frutas, es decir, se hace una masa de 21 kilos en total y de ahí salen 30 panes dulces que tienen que tener un precio razonable, pero en la Argentina es así, en vez de bajarlos, los suben.“ ¿Qué fruta? ¿La abrillantada? Esto es una tomada de pelos.

El sitio Mdz, informó que no sólo en Buenos Aires podrán verse altos precios del pan dulce, sino que el incremento se extenderá al país por el aumento del costo de los frutos secos. Indicaron que, según la Asociación de Panaderos de Capital, la suba fue del 70 al 80%.

![](https://assets.3dnoticias.com.ar/meme-pan-dulce.webp)

¿Se da cuenta? ¿Cuáles son las opciones para Santa Fe? ¿Comer los mazacotes industriales que van desde los 90 a los 100 pesos? ¿Erradicar el pan dulce de la navidad? ¿O aplicar lo que aprendimos en cuarentena y arriesgarnos a prender el horno? ¿Usted qué dice?

Aaah frutos secos, dijo… no fruta abrillantada… ¡Ya estoy transpirando de los nervios, mirá! Si aumentan el pan dulce culpa de la fruta abrillantada, te paro el país. Sabelo.

***

##### **Sofía Gioja en** [**twitter**](https://twitter.com/SofiG83)