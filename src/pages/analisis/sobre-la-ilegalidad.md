---
layout: analisis
category: analisis
date: 2020-12-30T12:55:55Z
author: Por Ignacio Etchart
title: Sobre la ilegalidad
thumbnail: https://assets.3dnoticias.com.ar/Etchart.webp

---
Este escrito fue realizado en vísperas del debate en el Senado sobre la ley de Interrupción Voluntaria del Embarazo. Sin embargo, el mismo no se propone establecer argumentos sobre esta discusión en particular, pues al respecto bastan y sobran, dejando al individuo colectivizado la decisión final sobre la legitimidad o no de dicha reglamentación.

Porque lo que se debate en este momento no es una ley, una despenalización, una modificación a alguna línea pequeña entre millones de líneas pequeñas del Código Penal de la Nación. Lo que se debate, en realidad, es la contradicción, única expresión real de la condición humana.

La mayor y más clandestina contradicción que existe es la muerte. Mayor, porque es inexorablemente universal. Clandestina, porque recorre las sombras de lo cotidiano hasta que se vuelve innegable. Pues vivir implica sólo una cosa: morir. «Hacen bien en creer que van a morir», rezaba Lacan. «Eso les da fuerza. Si no lo creyeran así, ¿podrían soportar la vida que llevan?»

Vivir, básicamente, implica morir. Cada día se comienza con el deseo de transitar la muerte lenta y progresiva de la mejor manera posible, con el mayor goce, alivio y tranquilidad que se pueda practicar. No se le  puede negar a nadie el ejercicio del deseo de morir de la manera más viva posible, siempre y cuando, claro, no afecte a terceros.

No se puede prohibir, no se puede ilegalizar, simplemente no se puede. Porque va en contra de la condición humana. Y no se puede legislar, establecer un Estado Nación, un proyecto político, económico, social y cultural que contradiga la condición humana. Porque la única oposición a la muerte vital placentera, es la mortificación de la vitalidad mediante la violencia.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">Una breve genealogía</span>**

Históricamente, el plan de modernización instrumental de la burguesía, profundizado en los siglos XVIII y XIX, tenía una única consigna: aumentar la productividad y reducir costos.

Con el surgimiento de los Estados Nación, tras el deterioro de las monarquías europeas durante el siglo XIX, el proyecto moderno se trasladó al interior de los hogares. Ya no serán las fábricas y el ejército, solamente, quienes instalarán modos de pensar y actuar, sino también las familias, la naciente escuela pública, la academia, la(s) ciencia(s) y el aparato jurídico.

De ahora en más, será legal aquello que funcione para alimentar el aparato productivo de una sociedad. Aquello que distraiga, que deteriore, que reduzca la capacidad de la cadena de ensamble será prohibido. Drogas, rituales, amores, pecados, pasiones, vicios. Todo un mundo hedonista, propio de la condición humana, vedado del común de la gente y, por supuesto, por dialéctica, privilegiado (pues eso significa «privilegio», derecho privado) a quienes controlan la cadena productiva de una sociedad.

Y no hablo solamente en términos económicos, sobre qué, cómo y para quién producir, sino de todo aspecto de la vida humana socializada. Políticas, leyes, programas, proyectos, discursos, tecnologías, todo en pos de normalizar al individuo y transformarlo en un engranaje más de la gran maquinaria social. Existen varios lugares reservados para quienes osen correrse de la norma: manicomios, cárceles, casas y escuelas especiales, santuarios para lo incorrecto, para lo desviado, para lo anormal.

Y aquí es donde el aborto confluye con milenarias prácticas, tal como lo es el aborto mismo. Atentan contra la productividad del plan moderno instrumental. El único sexo que importa, es el sexo reproductivo, capaz de alimentar las líneas de ensamble fabriles que necesitan continuamente una renovación de carne dosilizada por la ley. Y si bien ya no existen fábricas, sino empresas, así como tampoco existen líneas de ensamble, sino cubículos, la maquinaria también dejó de necesitar carne productora. Ahora necesita carne consumidora.

<br/>

![](https://assets.3dnoticias.com.ar/StarChild-2.webp)

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">El bebé ingeniero</span>**

El bebé ingeniero es la máxima metáfora de la reproductividad instrumental. Por sobre todo, es blanco y de ojos claros, antropomorfo con sus extremidades presentadas de forma casi tierna, rozando una holgazana reproducción kubrickeana.

Como definía Sarmiento, epítome del proyecto burgués normalizador de nuestro país, al guacho: «chusma de haraganes. No trate de economizar sangre de gauchos. Este es un abono que es preciso hacer útil al país. La sangre de esa chusma criolla incivil, bárbara y ruda es lo único que tienen de seres humanos» (Carta a Bartolomé Mitre, 20 de septiembre de 1861).

O sobre los pueblos originarios: «por los salvajes de América siento una invencible repugnancia sin poderlo remediar. Esa canalla no son más que unos indios asquerosos a quienes mandaría colgar ahora si reapareciesen. Incapaces de progreso, su exterminio es providencial y útil, sublime y grande. Se los debe exterminar sin ni siquiera perdonar al pequeño, que tiene ya el odio instintivo al hombre civilizado» (El Nacional, 25 de noviembre de 1876).

«Chusma de haraganes, bárbara» por un lado, «progreso, hombre civilizado», por el otro. Aquí se explica el Bebé Ingeniero.

Antes que nada, es un bebé blanco, nacido en una cuna probablemente de clase media,  en un ecosistema material que le podría garantizar el desarrollo de una vida plena. No es un bebé mestizo, negro, aborigen, pobre, nacido en la basura, en una conjunción de chapas y cartones que se pretende albergue. El bebé por nacer es normal, no bárbaro.

Y segundo: es ingeniero. ¿Por qué no politólogo, sociólogo, deportista, incluso médico o albañil? Porque la razón instrumental, ante todo, coloca por encima a la matemática y al dominio que esta puede ejercer sobre la naturaleza, tanto humana como de la flora y la fauna.

Se necesitan seres normales para ser normalizados y así perpetuar la cadena de normas. Al negro no se lo puede educar, al pobre no se lo puede insertar en la sociedad. Pero al blanco clasemediero sí. Obviamente en esta metáfora jamás será una beba, pues qué lugar merece la mujer en la vida burguesa sino es anclada a la cocina para alimentar al varón transformador de la naturaleza, o atada a la cama para alimentar al sistema productivo.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">Otras ilegalidades</span>**

Es absurdo, imposible y ridículo ilegalizar acciones humanas. El ser humano es un ser, ante todo, relacional y emocional, no racional. Dictar leyes y pronunciarlas a voz viva jamás generará acciones que contradigan el placer humano. Y aquí repica muy cerca el argumento de Pino Solanas en el 2018.

Si las personas cumplen leyes es porque no les afecta totalmente la vida. Si la ley es infringida (siempre y cuando no afecte a terceros), no es producto de un individuo criminal. Es producto de un sistema que criminaliza al individuo.

El 3 de noviembre de este año, el estado de Oregon despenalizó la posesión, en pequeñas cantidades, de drogas pesadas como la cocaína y la heroína, además de legalizar el acceso a hongos alucinógenos para uso terapéutico. «Castigar a las personas por usar drogas y por la adicción es costoso y no ha funcionado. Más tratamiento, en vez de castigos, es un mejor enfoque», es uno de los fundamentos principales. Muy similar a los argumentos del Pepe Mujica, expresidente uruguayo, sobre las políticas aplicadas durante su gestión sobre el consumo, tenencia y producción de marihuana en el país vecino. Mayor rendimiento por un menor costo. Sigue siendo razón instrumental, es verdad, pero bueno, contradicción. En fin...

La despenalización de drogas duras en Oregon es la primera legislación de este tipo en los Estados Unidos, país tótem de la ilegalidad de las acciones humanas. Nación cuya Constitución prohibía que gente de diferente color viajara junta, usara un mismo baño, asistiera a los mismos espectáculos y celebraciones. 

![](https://assets.3dnoticias.com.ar/meme-mujeres-sesentas.webp)

Nación que inauguró la «batalla» contra la marihuana, fundamentada con el discurso del narcotráfico, cuando en realidad el plan era relacionarla con el movimiento hippie, que no sólo militaba el fin de la invasión norteamericana en el Sudeste Asiático (no se puede llamar «guerra», pues la última vez que Estados Unidos le declaró la guerra a un país fue durante la Segunda Guerra Mundial) sino que denunciaba, sin saberlo explícitamente, toda la maquinaria racional, instrumental y normalizadora del proyecto político burgués.

«La guerra contra las drogas fue ideada en 1968 para reducir a dos comunidades enemigas: la afroamericana y los grupos que se oponían a la Guerra de Vietnam. Al hacer que el público asociara a los negros con la heroína y a los hippies con la marihuana, y luego criminalizar ambas sustancias fuertemente, podíamos fragmentar sus comunidades», palabras adjudicadas al asesor político del presidente Richard Nixon, John Ehrlichman, cuya cita fue publicada por la Revista THC en noviembre del 2018.

![](https://assets.3dnoticias.com.ar/Ehrlichman-Nixon.webp)

Aborto, drogas, otredades. También se le puede sumar el divorcio, el matrimonio igualitario, la maternidad en soltería (porque jamás se condenó a un padre soltero), el voto femenino. Todas acciones inherente humanas, que reflejan un deseo humano, prohibido por la ley moderna. La línea histórica es tan infinita como unx desee extenderla.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.2em; font-weight: 700">Violencia</span>**

La ilegalidad deviene en clandestinidad. La clandestinidad en violencia. La percha, las rejas de una cárcel, la bala de una ametralladora Thompson en manos de un lacayo de Al Capone son lo mismo. Proponen la misma solución para un mismo problema: la prohibición de un acto inherente humano, con toda la contradicción humana que aquello implica.

Tras la muerte de Sean Connery revisité, después de mucho tiempo, _Los Intocables,_ de Brian de Palma. Película estrenada en 1987, por la cual Connery ganaría su único Oscar como actor de reparto, junto a un elenco compuesto por Robert De Niro, Kevin Costner y Andy García. Largometraje que básicamente resume todo este escrito.

Sobre el final de la película, el personaje de Elliot Ness, tras encerrar a Al Capone, genocida del hampa y traficante de alcohol durante los tiempos de la Ley Volstead (más conocida como la Ley Seca), a base de tiroteos, baños de sangre e ilegalidades aplicadas para combatir otras ilegalidades de gente sin uniforme, deja para la posteridad la única frase que debería cerrar todos los debates sobre la ilegalidad del goce humano, al recordar las consecuencias de una ley hoy casi risoria: «cuanta violencia».

<br/>

![](https://assets.3dnoticias.com.ar/Los-Intocables.webp)
<br/>
<hr style="border:2px solid red"> </hr>

**Ignacio Etchart**

Otras columnas del autor en este portal:

[Sobre la inmortalidad en la historia](https://3dnoticias.com.ar/analisis/sobre-la-inmortalidad-en-la-historia/ "Sobre la inmortalidad") | [Murió](https://3dnoticias.com.ar/analisis/maradona/ "Murió") | [Odio go home](https://3dnoticias.com.ar/analisis/odio-go-home/ "Odio go home") | [De un Goliat y múltiples Davids](https://3dnoticias.com.ar/analisis/de-un-goliat-y-m%C3%BAltiples-davids/ "De un goliat")