---
layout: analisis
author: Por José Roura
category: analisis
title: Una mamá, un papá y un celular
date: 2020-11-23T12:27:29.194Z
thumbnail: https://assets.3dnoticias.com.ar/1605938490246.jpg
---
**El Estado está presente. Vos estás solo.**\
Imaginate vos, con tu hije, volviendo de terapia. Imaginá a tu niñe pidiendo verte porque se siente mal, y querés ir, y estás lejos. Imaginate en el lugar. Y alguien te para, y te dice: "¿Adónde se dirige?"\
Ese es el poder.\
A partir de ese momento, estás solo.\
Suerte que filmaste, suerte que la ciudadanía se enteró. ¿Y si hubieses estado sin baterías?\
"No caminaron cinco kilómetros sino setenta metros, y se sentaron en un banco de un kiosco".\
Solos ante en poder.\
Ahora salen a pedir disculpas: siempre tarde. Deberían pedirles disculpas a los que no tienen celular y no filman los atropellos.\
Como le pasó a Facundo Astudilllo Castro. A Luis Espinoza. A Magali Morales. Ellos estuvieron solos ante el poder, sin celulares para grabar.\
¿Y el gobernador? ¿Y el gabinete? ¿Y los concejales? ¿Y los responsables de derechos humanos? ¿Y el jefe de policía? Mirá todos los que se hicieron los boludos.\
Una mamá, un papá y un celular.\
**Háganse cargo.**