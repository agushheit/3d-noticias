---
layout: analisis
category: analisis
date: 2021-02-26T00:02:40-03:00
author: Sofía Gioja
title: "¿A LAVAR QUÉ?"
thumbnail: https://assets.3dnoticias.com.ar/sofia.jpeg

---
La Agencia Nacional de Seguridad Vial (ANSV) dispuso que, de ahora en más, para obtener la licencia de conducir en el país, se deberá completar un curso sobre violencia de género.

![](https://assets.3dnoticias.com.ar/auto.jpg)

Así es, la violencia al volante es cosa del pasado y antes de que tengan el tupé de mandarnos a “_lavar los platos”_, van a tener que capacitarse. La nueva normativa aclaró que los contenidos serán obligatorios para ambos sexos y para obtener la licencia de conducir habrá que prepararse sobre "género, roles y estereotipos, identidad de género, violencia de género, tipos y modalidades de violencia".

La medida, fue firmada por el director de la ANSV Pablo Martínez Carignano. Todavía no entró en vigencia, pero seguro ya va a saltar como leche hervida algún Raúl, porque va contra sus principios de macho pistero. Chicos, tienen hasta marzo para entrar en razón.

De ahora en más, entonces, el programa de enseñanza que llevan adelante las escuelas de seguridad vial incluirá para el curso teórico práctico un apartado de “Género” en el que los futuros conductores y conductoras, deberán instruirse en temas vinculados a “roles y estereotipos; identidad de género; violencia de género; masculinidades: patriarcado y heteronormatividad; femicidios, travesticidios, transfemicidios y crímenes de odio, entre otras cuestiones”.

“_En caso de desaprobar el curso, el postulante podrá presentarse nuevamente una vez transcurridos 30 días hábiles, a contarse desde la fecha de desaprobación. El curso podrá reiterarse en estos casos, hasta un máximo de tres veces en el año_”, explicaron desde el ente.

Para complementar las resoluciones que se publicaron el Boletín Oficial, el Ministerio de Transporte de la Nación difundió también una “Guía especial” para adaptar los carteles y la señalización con perspectiva de género. La misma promueve, entre otras cosas, “_un uso del lenguaje inclusivo, reconociendo y visibilizando a las mujeres y diversidades, colectivos hasta ahora invisibilizados en el sector transporte, producto de los estereotipos y limitaciones culturales vinculadas a competencias supuestamente masculinas_”.

“Esta idea instalada socialmente de que las mujeres manejan peor que los varones no se condice con las estadísticas”. El director de la ANSV muestra datos que confirman que las mujeres mueren en mucha menor proporción que los varones en siniestros viales, conducen alcoholizadas significativamente en menor medida que ellos, usan con más frecuencia el cinturón de seguridad, el sistema de retención infantil y el casco si están en moto, y no participan en eventos violentos como bajarse del auto para trompear a otro conductor en algún incidente callejero. _“Claramente en la conducción las mujeres manejan mejor y como mínimo exigimos que se las respete_”, dijo el funcionario.

La Asociación civil "Luchemos por la Vida" hizo su último censo en el 2017. Con el nombre “Mujeres al volante” la investigación fue realizada en la Ciudad de Buenos Aires, sobre un total de 4.844 conductores de automóviles particulares. Los resultados fueron los siguientes:

* Uso del cinturón de seguridad: 19% más que los hombres
* Violación al semáforo en rojo: 57% menos que los hombres

  (Determinado en base al total de violaciones al semáforo en rojo observadas (237) y al índice de mujeres al volante).
* Uso del celular al conducir: 33% menos que los hombres
* Uso del casco en bicicleta: 61% más que los hombres.

Mujeres al volante: 21%

Hombres al volante: 79%

**Más pruebas**

A nivel internacional las mujeres, según "Luchemos por la vida", a igual cantidad de kilómetros recorridos, causan menos siniestros graves (con heridos y/o muertos) que los varones, en una proporción que va de dos a cinco veces (dependiendo del país de análisis).

En Argentina, la mujer conductora aventaja al varón conductor en su seguridad al volante. Esta tendencia, de los varones a arriesgarse en las calles y rutas, tiene como resultado que 7 de cada diez víctimas en el tránsito en Argentina son varones.

Otra información interesante a la hora de analizar el tema:

· Cada 100 autos controlados, 5 conductores son detectados alcoholizados, según los datos de operativos de alcoholemia federal; pero solo el 0,7 por ciento de mujeres está en infracción: es decir, menos de una cada 100.

· En relación al cinturón de seguridad, cuando se trata de una conductora el uso es del 58 por ciento; si maneja un varón baja al 47 por ciento, según datos de 2018, un promedio de todo el país.

· El sistema de retención infantil, las sillitas, también las usan más ellas cuando conducen: 35 por ciento contra 26 por ciento. En ciudad de Buenos Aires seguramente, el uso es mayor que en otras ciudades del país, pero las cifras reflejan promedios nacionales.

· En CABA –donde se hizo el relevamiento sobre este ítem—no se encontró ninguna mujer imputada por delitos de daños y amenazas en el marco de un siniestro vial, es decir, esas escenas en las que se ve a un hombre con palo u otro objeto que golpea –o intenta hacerlo—a otro conductor. Pero si hay casos de varones involucrados en causas judiciales.

· Sobre el uso del casco en moto, se repite la tendencia: el 70,6 por ciento de ellas lo usan, contra un 62 por ciento de varones.

Por otro lado, del total de licencias entregadas en Argentina, el 70 por ciento corresponde a varones y el 30 por ciento a mujeres, es decir, ellos están significativamente más al frente de un volante que ellas en la vía pública. Sin embargo, cuando se miran las estadísticas de siniestralidad de todo el país se observa que esa relación no se mantiene: el 81 por ciento de los fallecidos son varones y apenas el 17 por ciento, mujeres. Pero dentro de ese porcentaje de muertes femeninas, entre el 30 y el 35 por ciento corresponden a mujeres que eran acompañantes de un conductor masculino.

![](https://assets.3dnoticias.com.ar/licencias.jpg)

**Estadísticas Locales**

Santa Fe es la única provincia del país que cuenta con un mapa en linea en el que se cargan los datos de las personas fallecidas en siniestros viales día por día, hora por hora.

Las causas de los siniestros más frecuentes protagonizados por las mujeres suelen ser: errores en maniobras de giro en intersecciones, al circular marcha atrás y estacionar; mientras que, en los hombres, las causas más comunes son: sobrepaso inadecuado, exceso de velocidad y conducir alcoholizado. Últimamente se ha notado el incremento de siniestros relacionados a varones menores de edad o jóvenes en estado de ebriedad.

Según los datos preliminares correspondientes al 2020, de la dirección Provincial del Observatorio Vial de la Agencia Provincial de Seguridad Vial, en la capital, la mayor cantidad de accidentes fatales involucran a motociclistas (hombres). A nivel provincia, los accidentes fatales también están encabezados por motociclistas en ruta, le siguen los automóviles, camionetas, ciclistas, camiones y peatones. La mayoría de los casos con hombres al volante.

Las estadísticas demuestran que los hombres son mas “propensos” a sufrir accidentes viales y también que los adjetivos calificativos propinados a las mujeres dentro y fuera del auto ya no tienen cabida. Dicho esto, esperemos que la capacitación en materia de género realmente sirva para tomar conciencia. Y disculpen mi incredulidad, pero temo que pocos sean los que se tomen en serio la iniciativa y el resto solo haga el esfuerzo para obtener la licencia y nada más. Espero estar equivocada.

![](https://assets.3dnoticias.com.ar/siniestros-viales-2020.png)