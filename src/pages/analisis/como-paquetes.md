---
layout: analisis
category: analisis
date: 2020-12-01T11:43:27.000+00:00
author: Por Sofía Gioja
title: Como paquetes
thumbnail: https://assets.3dnoticias.com.ar/Gioja.webp

---
Entre tanto alboroto diario, hay cosas que se nos “escapan” o al menos nos obligan a detenemos en otros sucesos. Digo sucesos, por ponerle algún título. Pero la noticia no salió en muchos medios ni tuvo demasiada trascendencia; encima, la muerte de Maradona cambió el rumbo de la atención colectiva y ahí quedó, otra vez, lo importante tapado por lo urgente.

La semana pasada, dos hermanitos de origen africano fueron abandonados, aquí en Argentina, por una pareja de Bahía Blanca. Eduardo Rucci y Natacha Perrig  habían viajado previamente a África para incorporar a los niños a la nueva familia.

Isabel Johanning, quien lleva más de 20 años dirigiendo la institución Casa Emanuel en África,  que alberga a chicos en situación de adoptabilidad, dijo que todo comenzó con una carta de intención de adopción que la pareja de argentinos les envió. "Ellos cumplían con los requisitos, por eso los mandamos con nuestros representantes en la Argentina", contó.

Allí se inició un proceso "que demora mucho tiempo" y la pareja lo atravesó con éxito. Muchas parejas con intenciones de adoptar se sienten agobiadas o desanimadas por toda la burocracia que implica la pre adopción y desisten de la decisión. Burocracia que, ahora, resulta visiblemente necesaria por razones obvias.

Tras el juicio de adopción y el viaje de la familia para radicarse en la Argentina, una representante del hogar se encargó de hacer el seguimiento por videollamadas. "Cuando les preguntábamos cómo estaban decían que bien, que los chiquitos eran hiperactivos, pero nunca me dijeron que no podían con ellos".

La apariencia que daba la familia no indicaba problemas con los hermanitos mellizos. En eso coinciden todas las fuentes consultadas. Antes de borrar su perfil de las redes sociales, Rucci posteó para el Día del Padre, en junio pasado, una foto de su hija biológica con sus hermanitos adoptivos entre abrazos y risas.

Un vecino relató que el 17 de noviembre vio salir de madrugada a Rucci, solo y con los dos niños. “Si tenían problemas económicos hubiesen avisado. Pero no se entiende cómo salieron de la ciudad con dos chicos, viajaron 1.500 kilómetros y nadie les pidió papeles. Volvió sin los dos pibes y no hubo una sola denuncia de nada”. También contó que había veces que el matrimonio salía con su hija biológica y dejaba a los niños solos en la casa.

Según la denuncia judicial, el hombre llegó a la Comisaría de la Mujer de Bahía Blanca, con los niños agarrados uno de cada brazo (como quien deposita dos valijas en la cinta transportadora de un aeropuerto) y dejó expreso su deseo de entregar a los niños, desentendiéndose totalmente de las obligaciones paternas de cuidarlos, convivir con ellos, alimentarlos y educarlos.

Es curioso que nadie hable de amor o cariño. Ahí los dejó, con unos pocos papeles y un bolsito. Los niños preguntaban, sin entender, cuándo los iría a buscar su papá.

El expediente de la comisaria de la mujer donde dejaron a los menores decía: "El hombre refirió razones personales que dificultan su vinculación y sostenimiento de la vida familiar". Lo único cierto es que los miembros de la pareja, con una hija biológica de 11 años, eliminaron sus redes sociales y cambiaron de domicilio  luego del abandono.

Letizia Tamborindeguy, integrante del Equipo Interdisciplinario de la Comisaría de la Mujer y la Familia de Bahía Blanca, los recibió e inició una investigación de oficio. Al respecto, contó que “había falta de documentación, datos que no estaban del todo claros, son nenes que tienen su identificación de África pero que no tienen documentación de Argentina, hay fechas de salida del país, pero no de ingreso, cuestiones que nos hicieron mucho ruido", sostuvo.

Toda la secuencia se desarrolló en un contexto extraño, entre lo bizarro y lo indignante.

### **Es más común de lo que parece**

La Defensora de los Derechos de Niñas, Niños y Adolescentes, Marisa Graham, asegura que desde hace muchos años se vienen registrando casos similares al de los mellizos de seis años, abandonados por una pareja argentina, luego de 15 meses de haberlos adoptado en Guinea Bisseau (África) y por eso el artículo 21 del Código Civil sigue prohibiendo la adopción internacional.

"En su momento tuvimos casos de niñas y niños haitianos, en otra época rusos, ahora muchos ucranianos", dijo Graham en diálogo con Télam. "Es una situación más usual de lo que se cree, sólo que este caso se conoció porque los niños fueron abandonados en una comisaría", agregó la funcionaria.

"Sabemos, por la Asesoría Tutelar de la Ciudad de Buenos Aires, que 2 de cada 10 niños adoptados o en guarda preadoptiva de hasta 8 años son abandonados  y, entre los mayores de 8 años, ese porcentaje crece a 5 de cada 10".

Por otra parte, la defensora advirtió que se habla incorrectamente de "devolución" cuando en realidad "es un abandono de un hijo como cualquier otro, e igual de lamentable" porque "una vez que hay una adopción existe un vínculo filiatorio. Lo que antes llamábamos patria potestad, hoy lo llamamos responsabilidad parental".

### **Entre la caridad y la responsabilidad afectiva**

Para Graham, la raíz del problema radica en la "idealización de la adopción". Los argumentos para el abandono suelen ser de lo más variopintos, se lamenta. "Recuerdo el caso de una bebé argentina de 8 meses que la “devolvieron” porque dijeron que la familia no asimilaba una niña tan morochita”.

"La mayoría de los aspirantes a guarda con fines de adopción tienen un ideal de niño o niña a adoptar que, en general, son niños pequeños, de cero a tres años, y como en Argentina no hay abandono de bebés recién nacidos, algunos argentinos recurren al exterior"

También se suele vincular la adopción a la caridad y, la verdad,  no se puede tener hijos adoptivos o biológicos por ser caritativo nada más. Cuando suceden hechos como estos, hay familias "bien intencionadas" que se ofrecen a cuidar a los niños, pero sin tomar dimensión de lo que ello invierte. Adoptar a un menor debe ser una decisión bien pensada y, sobre todo, teniendo en cuenta que muchas veces no va a ser ni fácil ni feliz.

Cuando una ve y escucha a Nicole Neuman, golpeándose el pecho por el sufrimiento de esos niños abandonados y poniéndose disposición para adoptarlos, habría que preguntarle a la modelo cuánto tiempo de calidad (no de caridad) les brindaría a estos hermanitos.

"Me partió el alma, el corazón, en mil pedazos. Te juro que me pongo de voluntaria para adoptarlos, lo digo realmente. Me pongo a disposición", aseguró la modelo al aire en un programa de TV. Nadie duda de las buenas intenciones de Nicole y, probablemente, su preocupación por el bienestar de los niños sea genuina, pero de nada sirve la buena predisposición si los menores van a pasar la mayor parte del día solos, en su lujosa mansión, rodeados de miles de objetos, pero carentes de afecto. Los niños no son decorativos. Esa es mi opinión. Usted puede pararse del lado que le quede mejor.

Como explica Graham, “para los niños es muy difícil asimilar cada abandono”. Y los padres adoptantes deben poner los derechos del niño por sobre las necesidades de los padres. Los chicos no son un objeto para la realización personal de los adultos. Los vacíos no se llenan con niños. En todo caso váyase de viaje o lea un libro.

### **No hay que irse muy lejos**

Lalo Liberatti trabaja en la Secretaría de los Derechos de la Niñez, Adolescencia y Familia de Santa Fe y cuenta su experiencia en los años que lleva trabajando con niños y adolescentes en situación de vulnerabilidad. “Que devuelvan niños es algo que pasa casi mensualmente. En mi caso estuve dos años y medio con el mismo niño, lo adoptan, estuvo 1 año y 4 meses con la familia y lo devuelven. Durante ese mismo periodo a sus hermanas también las devolvieron dos veces.”

“La burocracia de la adopción es compleja”, comenta Lalo. Y aclara que hay cosas que no se saben,  como que existe un alto porcentaje de padres aspirantes a la adopción que buscan niños dentro de la primera infancia, o sea hasta 5 años. Y que los niños que están bajo la tenencia definitiva de Estado son preadolescentes. Lalo explica que no son compatibles la cantidad de postulantes, con los perfiles de los niños a disposición y es ahí donde se genera la demora. Los niños se vuelven mayores de edad dentro de los hogares porque nadie quiere niños grandes. Y la situación se complica mucho más si los niños tienen alguna patología.

“Un ejemplo es lo que les pasó a las hermanas (adolescentes) del niño que yo monitoreaba, que estuvieron el periodo de vinculación con una señora mayor, que vivía con su madre, que nunca tuvo hijos y trabajaba 12 horas. La señora se iba 12 horas y las niñas quedaban con una señora mayor en silla de ruedas. Entonces, las niñas pidieron volver al hogar de tránsito. De estos casos hay un montón”.

![](https://assets.3dnoticias.com.ar/mellizos.jpg)

### **El trabajo del RUAGA**

Magdalena Galli Fiant, directora del Registro Único de Aspirantes a Guarda con fines Adoptivos, cuenta que para los ya inscriptos y los nuevos aspirantes a adopción, existe nueva modalidad de búsqueda. A partir de ahora, quienes quieran adoptar pasarán por tres estadíos y prometió que serán períodos "más ágiles".

Quienes deseen recibir como hijo a un niño o a un adolescente deberán rellenar un formulario online como primer paso para comenzar a transitar el proceso de adopción.

Luego, deberán participar de talleres obligatorios donde se intentará que la pareja defina la "disponibilidad adoptiva", es decir, si esperan a un recién nacido, a grupos de hermanos y/o a adolescentes y si desean ser padres de un niño con discapacidad.

"Es importante esta instancia porque se producen fracasos en la relación entre los padres y el nuevo hijo, porque tenían otras expectativas. Además, se trata de evacuar dudas con personal especializado y entender cómo es el proceso que se está llevando a cabo", detalló Galli Fiant.

El RUAGA se encuentra actualmente trabajando en una alternativa que también se aplica en otras provincias. Su directora cuenta que, antes de realizar una convocatoria pública, donde llaman a todas las personas de todo el país que quieran inscribirse, primero traten de acudir a los inscriptos del Registro. “A pesar de que ya hayan definido su proyecto adoptivo, pensando a lo mejor en niños de hasta 8 años o 10 años, apelamos a puedan ponerse a pensar si aceptarían niños más grandes o a un grupo de hermanos más numeroso del que habían pensado”.

Galli Fiant explica que a veces se separan los hermanos y se buscan familias que puedan relacionarse entre ellas. Pero en primer lugar siempre buscan que el grupo de hermanos permanezca junto. Y advierte su preocupación por la cantidad de niños en la segunda infancia o adolescencia que supera a la cantidad de niños pequeños que buscan familias adoptivas. Su objetivo es pensar en estrategias para cortar con esa proporción inversa.

> **En este contexto, el RUAGA llama a una nueva convocatoria para la inscripción de aspirantes a adopción. Será desde el 1 al 10 de diciembre y se puede presentar la ficha de adopción de forma digital. La convocatoria está dirigida tanto a nuevos como antiguos aspirantes, inclusive para aquellos que tengan solicitud expirada.**

“Actualmente tenemos 14 situaciones, cuando hablamos de situaciones no solo nos referimos a niños o adolescentes, sino también a grupos de hermanos”. “Y en otros supuestos son chicos y chicas de diferentes edades que tienen alguna discapacidad o alguna enfermedad de tratamiento prolongado. Todos ellos esperan una familia”.

***

##### **Sofía Gioja en** [**twitter**](https://twitter.com/SofiG83)