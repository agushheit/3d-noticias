---
layout: analisis
author: Por José Villagrán
category: analisis
title: El gabinete de Perotti, un equipo rodeado de rumores
date: 2020-11-13T18:16:40.570+00:00
thumbnail: https://assets.3dnoticias.com.ar/Villagran.webp

---
Hoy se conoció la renuncia al cargo de Ministro de Gobierno de la Provincia de Santa Fe, Esteban Borgonovo. El primero en renunciar fue Carlos Parola, a la cartera de Salud, allá por junio de este año y en plena pandemia por Coronavirus. ¿Qué otros cambios piensa el gobernador?

En horas del mediodía, tweet de por medio, Esteban Borgonovo dio a conocer su indeclinable decisión de renunciar como Ministro de Gobierno, Justicia, Derechos Humanos y Diversidad. Confirmó que primero se lo comunicó al gobernador y luego lo dio a conocer públicamente, transformándose así en el segundo ministro en renunciar a su cargo en once meses de gestión. En su publicación, el ahora flamante ex ministro, hizo referencia a un ”año singular y complicado”, donde poco a poco su participación dentro de la estructura del poder ejecutivo fue perdiendo peso.

Sin lugar a dudas, su dimisión vino a poner identidad a rumores que comenzaron a sonar hace ya varios días. Es que hace poco más de dos semanas, Omar Perotti habló de “posibles cambios” dentro de su gabinete atendiendo que el diseño de los equipos de gestión que asumieron el 10 de diciembre de 2019 se pensó en vistas de una situación social "que ha sido drásticamente alterada", haciendo referencia a la pandemia. Y uno de los primeros cambios que empezó a sonar con fuerza fue el que sucedió hoy.

Es el mismo Borgonovo quien lo confirmó en declaraciones a medios radiales de la ciudad de Santa Fe. “Es difícil trabajar en un escenario de incertidumbre, donde el gobernador anunció cambios dentro del gobierno”. Habló de comodidad e incomodidad y marcó esa diferencia cuando empezaron a surgir esas informaciones. "Yo estuve siempre cómodo, pero me pareció que esa incertidumbre no me permitía cumplir cómodamente mi gestión", afirmó.

**¿Rubén Michlig superministro?**

El sucesor parece estar decidido por parte de Omar Perotti. O por lo menos, provisoriamente. Se trata del actual Ministro de Gestión Pública Rubén Michlig, quien se hará cargo de la estructura que deja vacante Esteban Borgonovo.

Michlig es un hombre de plena confianza de Omar Perotti y que forma parte de la mesa chica de las decisiones políticas que toma el gobierno provincial. Tal es así que, por ejemplo, fue uno de los designados para llevar adelante los encuentros con el gobierno de Miguel Lifschitz en la transición de gestión del año pasado. También dio informaciones en los reportes epidemiológicos sobre cambios en el transcurso de la cuarentena cuando el rafaelino no se hacía presente. Un hombre de mucha cercanía y de mucha “rosca” política.

El gobernador de la provincia ve con buenos ojos el andar de Michlig. Le reconoce ser uno de los pocos ministros que sale a “patear” el territorio y defender la gestión desde ese lugar.  Perotti entiende que su mandato tiene carencias en ese sentido y que necesita ser defendido de esa manera. Por eso no sería de extrañar que se lo reconozca al actual titular de la cartera con más poder.

El gobernador tiene dos caminos: definir un reemplazo natural para el cargo o plantear una reestructuración dentro del gobierno. Todo eso se está discutiendo en el seno íntimo de la conducción de la provincia.

**¿Habrá más cambios?**

Todo parece indicar que sí. A esta altura del año, y los acontecimientos, es más fácil decir cuál ministerio se mantendrá como viene, que arriesgar nombres y puestos.

El ministerio de Economía seguirá con Walter Agosto a la cabeza. Hay una mirada compartida entre los dos contadores y es una pieza clave para Perotti.

La cartera de Seguridad, lo mismo. Aunque las versiones siempre están a la orden del día. La renuncia de Marcelo Sain como ministro del área estuvo desde un primer momento. Pero la banca del titular del ejecutivo también siempre estuvo.

Hay un aspecto político que une a seguridad y economía en este momento: leyes importantes para el Ejecutivo. La primera ya presentó su reforma de la policía y es el propio Sain el encargado de defenderla. En tanto que Agosto se puso al hombro el pedido de endeudamiento para llevar adelante el plan “Santa Fe + Conectada”, que cuenta con el aval de Senadores, y espera el afirmativo de Diputados. Pero además, ya se empieza a discutir el proyecto de Presupuesto para el año que viene, el primero de la gestión Perotti, teniendo en cuenta que el de este 2020 fue armado por su antecesor Miguel Lifschitz.

El resto de las carteras que conforman el gabinete puede sufrir cambios. Desde Cultura, pasando por Educación, Salud y Trabajo. Ministros que pueden pasar de un ámbito a otro y ministerios que pueden ganar más peso político.

**El estilo de gabinete que piensa el mandatario para su segundo año de gestión**

“La pandemia nos cambió el enfoque de gobierno. Nos tuvimos que adecuar a una realidad que no teníamos pensada y rearmarnos en esta realidad” no se cansan de decir dentro del oficialismo. Pero también reconocen que hay falencias que se tienen que subsanar, sobre todo pensando en el año electoral que se viene.

Ya se dijo que el propio gobernador le pidió a la juventud que se defienda el accionar del Ejecutivo. Lo mismo les viene insistiendo a sus propios ministros. Está convencido de que debe acercarse “más a la calle”. Y para lograr eso, piensa que a la gestión debe darle oxígeno.

Desde el entorno del mandatario afirman que “a Omar no le gusta que la oposición le ande marcando la cancha ni le imponga agenda”. Ahí hay otro aspecto que el ex intendente de Rafaela quiere cambiar. Además, piensa que debe mejorar la relación con la oposición más dura a su gobierno, que ha sido marcada por fuertes cruces que contrarrestan  su forma de hacer política.

El 2021 ya está planteado y empieza a notarse en los cambios que se plantean. Perotti, como toda la sociedad, también desea que este 2020 llegue a su final.

***

##### **José Villagrán  en** [**twitter**](https://twitter.com/joosevillagran)