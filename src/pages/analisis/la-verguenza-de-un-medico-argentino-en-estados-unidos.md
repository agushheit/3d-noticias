---
layout: analisis
category: analisis
date: 2021-01-14T09:48:48Z
author: Por Javier Firpo para Clarín
title: La vergüenza de un médico argentino en Estados Unidos
thumbnail: https://assets.3dnoticias.com.ar/brian-hunis-opinion.webp

---
## «La gente se vacuna en Miami mientras va a un shopping»

<br/>

###### <span style="color: #ffffff; background: #EB0202; overflow: hidden; padding: 10px; border-radius:5px;">  Turismo de vacunación  </span>

<br/>

Brian Hunis está radicado en Florida. «Somos los pioneros en pioladas que avivan a los giles», se queja de sus compatriotas.

**«No es ilegal, pero es amoral** y no me extrañaría que algún vivaracho pronto organice un tour de vacunación», dice entre indignado y sorprendido Brian Hunis, oncólogo y hematólogo argentino, jefe de oncología del Memorial HealthCare System. «**La Argentina sigue siendo pan y circo**, donde los vivillos buscan llevarse una tajada más».

De Villa Devoto, Hunis vive en Estados Unidos hace 18 años. Residió en Nueva York, luego en Carolina del Sur, hasta que se afincó en Hollywood, localidad del condado de Broward, en el estado de Florida. **Lo suyo es la cultura del esfuerzo**. Se recibió en la Universidad de Maimónides a los 24 años, el mismo día que el expresidente Fernando de la Rúa abandonaba en helicóptero la Casa Rosada. «Ese día decidí irme», cuenta.

Rumbeó para Estados Unidos, donde debió homologar el título y hacer una exigente residencia hasta convertirse en un destacado especialista en oncología y hematología. «¿Qué derecho tiene **un turista de quitarle la vacuna a una persona residente** que genuinamente está esperando su turno? Me parece indignante lo de los argentinos y otros latinoamericanos que están viajando y hacen gala de su epopeya. ¡Con qué derecho!», se queja.

«El argentino viene a Miami como si nada y se vacuna mientras va de compras a un shopping o a la playa en el medio de la pandemia», dice con pudor y vergüenza.

Hunis sentó su posición en un posteo de Facebook, y con Clarín se muestra sumamente molesto por las imágenes que vio de Yanina Latorre, especialmente, y de Ana Rosenfeld, que hicieron público en sus redes que fueron vacunadas ellas o un familiar. «**Dicen que es gratis la vacuna**, ¡por favor! Con mis impuestos y con los impuestos de los que viven en el estado de Florida pagamos el desarrollo de la vacuna. ¿De qué estamos hablando? Yo les estoy pagando la vacuna a **ellas que andan haciendo ostentación** de cómo lograron vacunarse... Me producen vergüenza ajena», castiga el médico, que ya se aplicó la de Pfizer.

Cuenta Hunis que «como siempre **los argentinos somos los pioneros en estas pioladas que avivan a los giles** y hoy aquí hay gente de Panamá, México, Colombia y de otros lugares que han venido a sacarles la oportunidad a una enfermera, un policía o una repositora de supermercado que trabajaron durante toda la pandemia y **no tuvieron la suerte de conseguir un turno**. Son las ventajitas de siempre, de un producto (la vacuna) que no abunda, que se aprovechan de que el gobernador de Florida (Ron DeSantis), que es republicano, descentralizó el sistema sanitario».

El profesional sostiene que tiene pacientes con patologías importantes «que recibirían de muy buena manera la vacuna, pero no lo pudieron hacer. Yo podría traer a mis padres, que están en la Argentina, pero me parece un espanto... Quizás yo **estoy demasiado americanizado**, pero no queda otra, hace mucho que vivo aquí y las reglas del juego son otras, **si no me adapto a esta cultura no progreso**... No se puede vivir llevándose al mundo por delante».

Hace saber Brian que se está enterando «de varios casos de amigos, de padres o tíos de amigos o conocidos que vinieron o quieren venir. Me entero porque estoy en ese mundillo en el que las noticias corren, se pasan la bola unos a otros, **no hay ningún tipo de pudor, sacan pasaje y vienen**, por supuesto que son siempre los menos necesitados, los que más posibilidades tienen».

A los que se han contactado con Hunis, él directamente los despacha, intenta disuadirlos... «No está disponible para el que no es residente», les digo y me quedo tranquilo con mi conciencia, pero, insatisfechos con lo que escuchan, me replican “Pero si el papá de fulano fue y no tuvo problemas”. Entonces anda a consultarle al papá de fulano. Si no la ganan, la empardan, es increíble, pero es así, **les importa un bledo sacarle el lugar a la persona que realmente tiene prioridad** o más derecho».

Comparte el oncólogo que es vox populi que para contrarrestar el llamado turismo de vacunación «se va a terminar pidiendo **una prueba de residencia, como algún pago de servicio que certifique que uno vive aquí**. Pero es necesario que haya uniformidad de criterios. El CEO del Jackson Memorial Hospital dijo que no le importa de dónde venga la gente, mientras que donde trabajo yo, si bien no toma posición pública, de ninguna manera vacuna turistas».

Comprometido con la causa, en las últimas horas el médico argentino posteó en Facebook: «A mis amigos argentinos: **entiendan que, si vienen a Miami a darse la vacuna, se la están sacando a alguien**. No es que acá se vacuna quien quiera. Los maestros, policías, bomberos y pacientes menores de 65 años todavía esperan... También personas que viven en este país y pagan sus impuestos».

«Yo tengo (muchísimos) pacientes esperando su vacuna, y la gente viene a Miami como si nada **y se vacuna mientras van de compras a un shopping o a la playa** en el medio de la pandemia. En fin, típica viveza argentina... de última. Y lo peor de todo: se vacuna aquel que tiene plata para poder pagarse el pasaje y una estadía mínima de tres semanas», concluye.