---
layout: analisis
category: analisis
date: 2020-12-12T13:29:47Z
author: Por Ignacio Etchart
title: Sobre la inmortalidad en la historia
thumbnail: https://assets.3dnoticias.com.ar/Etchart.webp

---
![](https://assets.3dnoticias.com.ar/portadacolumna.jpg)

Cuando Tetis, nereida de Poseidón, dialogó por última vez con su hijo Aquiles, quien se encontraba perturbado por las injusticias recibidas por parte de Agamenón, rey de las polis griegas, la contradicción la invadía. Su dilema era un conflicto de deseos. Su hijo deseaba ser inmortal. Y Tetis sabía perfectamente que, para Aquiles, la única posibilidad de inmortalidad era la muerte. El deseo un hijo frente al deseo de una madre.

“Si te quedas, vivirás muchos años. Y tendrás hijos que te recordarán, y ellos tendrán hijos que te recordarán también. Pero con el tiempo los hijos de sus hijos dejarán de recordarte y serás olvidado. Si vas a Troya, jamás regresarás, pero serás recordado por toda la eternidad”.

El resto es historia.  Justamente eso, historia.

Los relatos helénicos que hoy se estudian desde la literatura, durante siglos fueron La Historia. No muy lejano, conceptualmente hablando, de los manuales Santillán que normativizaron generaciones enteras de estudiantes argentinos bajo un relato universal, unívoco, fragmentado, y ante todo, ajeno.

Se puede tomar como ejemplo la figura del Martín Fierro, de José Hernández. Principalmente la segunda parte del relato. Un gaucho matrero que las vivió todas, finaliza su vida transmitiendo valores de obsecuencia, conformismo y pasividad a su prole.

Paralelamente, y nada más alejado a la ficción, Juan Moreira moría ejecutado por una ley que oprimía su identidad guacha, así como también reprimía las identidades negras, originarias y de demás otredades.

La Historia argentina, al menos en su concepción mitrista y separatista, nunca logró representar la víscera de la sociedad argentina. Apenas, y por decisión política, claro está, se limitó a representar a 200 familias, victoriosas de las guerras civiles del siglo XIX, ubicadas principalmente en el Puerto de Buenos Aires.

¿Saben por qué existe la nomenclatura “interior” y “puerto”, como antinomias? Porque durante los 500 años de dominación europea, el territorio argentino (aunque ni cerca de estar delimitado como lo está ahora) era un simple viaducto de mercaderías. El centro productivo del Virreinato estaba en el altiplano, donde se encontraban la plata, el oro y demás joyas.

Argentina fue, durante 500 años, apenas una cabina de peaje. Pero el puerto, que conectaba la barbárica América con la civilizada Europa, se encontraba fuera del interior, es decir, en el exterior del territorio nacional. Aquí surge la idiosincrasia porteña, ciudad de comerciantes y propietarios.

Entonces tenemos una historia porteña, escrita por y para los porteños. El resto, el interior, era salvajada, carne de cañón para la política y para las guerras. A veces hay que celebrar la soberbia de la Generación del 80’. Hoy es fácil pensar que una “Conquista del Desierto”, ya por su denominación, no es otra cosa que un episodio violento, constituido en pólvora, sangre aborigen y una posterior pampeanización del territorio. No muy lejos cayó la manzana de la Conquista de América, evidentemente.

En un país de mestizos, negros, gauchos, coyas, mapuches, charrúas, guaraníes, quom y decenas de otredades más, la historia que los representa, los símbolos e íconos patrios que los constituyen son de naturaleza blanca, eurocentrista incluso.

<br/>

Fue por eso que la vida y muerte de Maradona impregnó en la sociedad como lo hizo. El ciudadano argentino no es blanco, como tampoco lo fue el Diego.

**Mucho se relacionó la muerte de Maradona con dos muertes más: la de Carlos Gardel y la de Eva Perón.**

![](https://assets.3dnoticias.com.ar/columna1.jpg)

El primero, máxima expresión del tango durante las primeras décadas del siglo XX. Género musical que se dedicó explícitamente a representar la miseria de la vida porteña. La Década Infame, es decir, el primer gobierno oligarca desde la aplicación de la Ley Sáenz Peña, descosió completamente el tejido social de la gran urbe.

Discépolo, en su famosa última grabación de radio previo a las elecciones de 1952, bien le señalaba a Mordisquito, personaje mitad ficticio y mitad real, que representaba a la clase política antiperonista, “yo no lo inventé a Perón y a Eva. Lo inventaste vos”.

Fueron las brutales políticas de exclusión del gobierno Infame que sentó las bases del peronismo. Sin injusticia social no habría necesidad de un movimiento que luchara por ella. Pero no fue así. Y Discépolo, letrista milonguero por excelencia, pues Cambalache es de su autoría, siempre culpó al peronismo por la muerte del aquel tango inicial.

“Los enamorados volvieron a ocupar los pórticos de las casas, reemplazando a los _cafillos_ y _fiolos_ que supieron allí estar”, comentaba Discépolo.

A estas tres muertes también se le puede sumar, aunque los productos de su lucha se concentraron en otras fronteras, la figura del Che.

***

![](https://assets.3dnoticias.com.ar/columna.jpg)

***

Relataba un periodista argentino cuyo nombre ahora no logro recordar, que mientras visitaba Nápoles había dos niños jugando al futbol al lado de un paredón, pintado con los rostros del Diego y del Che. “¿Quién es éste?”, preguntaba uno de los niños al otro. “Pues es el Diego”. “No, no. El otro”. “¡Ahh! Pues ese es el tatuaje del Diego”.

**Gardel, Eva y el Che. Con el tiempo, Maradona.** Una línea histórica que se opuso a la historia blanca, hegemónica, eurocentrista, apátrida y con perspectiva hacia el norte. Nuestro norte es el sur, arriba solo ha existido, existe y existirá opresión, represión y sumisión.

Por eso la muerte y vida de Maradona impregnó como lo hizo. Porque es Historia. A Gardel la mayoría no lo tiene tan presente. El Che recae en la controversia de ser un Comandante de Guerrilla, en un país donde se milita, hoy más que nunca, la vuelta del Falcón Verde. La contradicción, reflejo de la condición humana por excelencia.

A Eva se necesitó un gobierno de 12 años (de los cuales 8 estuvieron bajo el liderazgo de la mujer) para poder reinventarla en el presente, para lograr identificar las luchas del pasado con las miserias y necesidades del presente.

Pero el Diego se hizo solo. Incluso muchos se hicieron gracias a él, de la misma forma que muchos se hicieron gracias a Gardel, a Eva y al Che, pero en su contemporaneidad. Por eso el dolor de la muerte del Diego. Porque él representaba la historia enterrada, la historia prohibida. La historia del pobre, del mulato, del aborigen, del gaucho.

No se es inmortal. A la inmortalidad se la alcanza. Y muchas veces se la alcanza joven. Tal vez Maradona vivió un poquito de tiempo prestado, eso con dolor uno lo percibe. Pero también, y producto de un egoísmo atroz, lo agradece. A estos tres pilares de La Historia reprimida del siglo XX, nuestra necesidad de ellos fue su perdición.

Así como los griegos necesitaban de Aquiles para tomar la onceava Ilión, también conocida como Troya, y hacerse un lugar perpetuo en la historia a costa de su vida, también sucedió con Gardel y un avión, con Eva y un cáncer, con el Che y un fusil, con Diego y una exitoína.

<hr style="border:2px solid red"> </hr>

**Ignacio Etchart**

Otras columnas del autor en este portal:

[Murió](https://3dnoticias.com.ar/analisis/maradona/ "Murió") | [Odio go home](https://3dnoticias.com.ar/analisis/odio-go-home/ "Odio go home") | [De un Goliat y múltiples Davids](https://3dnoticias.com.ar/analisis/de-un-goliat-y-múltiples-davids/ "De un goliat")