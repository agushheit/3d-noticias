---
layout: analisis
category: analisis
date: 2021-07-30T06:00:21-03:00
author: Nicolás Iñaki Rabosto
title: Un acercamiento a las ideas de la libertad en el contexto real y municipal.
thumbnail: https://assets.3dnoticias.com.ar/IÑAKI.jpg

---
Para todo liberal, es compleja la participación política. Estar digiriendo esa constante pelea entre los valores y principios, que deben aplicarse en escenarios cubiertos por contextos plagados de realidad. Con tan solo ver los cambios que enfrenta el mundo, es posible pensar que la lucha por la libertad debe hacerse contextualizada y adaptada al cambio de era.

El camino hacia la libertad siempre está lleno de nuevos desafíos porque continuamente sus enemigos acechan, mutan y se camuflan en el correr de los tiempos. Se puede percibir el cambio de paradigma, que pasó de la disputa entre conservadores y liberales -marcando el siglo XIX-, a un cambio radical en las décadas del siglo XX -ligado al auge del “Estado Benefactor” y al “florecer del socialismo”- corriendo al compás -ya anacrónico- de Izquierdas y Derechas que conocemos. Hoy nos toca empezar a ser parte de estos cambios, tenemos la oportunidad de empezar a leer mejor los tiempos electorales, participando en espacios que promueven ideas apegadas a la libertad y al desarrollo humano adaptados a los nuevos tiempos del mundo.

La Argentina se forjó desde el espíritu pasional por la libertad, con un gran crecimiento para la integración al mundo y, particularmente, con una gran inversión en el desarrollo de su capital humano. Esta visión que llevo a nuestro país hacia un modelo que garantice la libertad, hoy debe plantear un nuevo norte hacia la Argentina del Siglo XXI.

La tarea que tenemos por delante es la de velar y luchar por esos valores de la sociedad libre que forjaron nuestro país, trabajando activamente para contribuir a cambiar las políticas públicas y a traer debates sobre la mesa que se han dejado de lado por varios años, a pesar de los múltiples reclamos de la sociedad civil a oídos sordos de la política en general.

Durante mi vida como estudiante, vinculado a la formación política y movimientos políticos tanto estudiantiles como partidarios; pude vislumbrar dos planos centrales del debate por la libertad desde una óptica terrenal y por sobre todo real: por un lado el desarrollo económico impulsado por mayores libertades y menos regulaciones; y por otro lado, una política orientada a la real integración de la Sociedad Libre, siendo conscientes de que la iniciativa privada es la verdadera motivadora de los cambios sociales y culturales que modifican el tejido social. Siempre que suelo hablar sobre política municipal santafesina tiendo a caer en la frase: “no hay mejor política municipal que la iniciativa privada de los vecinos.”

Una visión joven, integral y local, pensada desde y para el desarrollo de la libertad del ciudadano, priorizando el crecimiento personal y la integración de los individuos para la cooperación humana. Es parte del rumbo que necesitan las nuevas y futuras políticas públicas de las ciudades; siendo estas las principales motivaciones de las medidas comunes que afectan al tejido social más próximo al vecino que son las urbes.

El camino hacia la libertad recién comienza, existen dificultades en Argentina y específicamente en Santa Fe, que ya forman parte del sentido común –ni siquiera hablamos de libertad- pero por algo siempre debe empezarse. Nadie puede saber muy bien cuál es el comienzo, pero como dice Antonio Machado, incluido en Proverbios y cantares, “Caminante, son tus huellas el camino y nada más; Caminante, no hay camino, se hace camino al andar.”

Empezar a tener voces del sentido común - y la expansión de las ideas liberales- es un gran síntoma de la sociedad civil, pero este año electoral 2021 es fundamental para consolidar políticamente este espacio que brega por imponer una voz más apegada a los valores liberales y republicanos, pero que no pierda un norte de integración de integración económica interna y externa, social y de desarrollo humano. No debemos olvidar que las transformaciones políticas son largas, las sociales y culturales más aun, pero todo arranca por empezar a mirar, escuchar y en algún momento hablar.