---
layout: analisis
author: Por Ignacio Etchart
category: analisis
title: De un Goliat y múltiples Davids
date: 2020-11-17T14:39:20.116+00:00
thumbnail: https://assets.3dnoticias.com.ar/Etchart.webp

---
Marzo fue un mes que va a quedar marcado en la memoria de todos. Sucedieron hechos coyunturales que cambiaron la vida de muchas personas, y que dejaron marcas y huellas que tardarán muchos años en subsanar. Todo esto agravado por el coronavirus y la cuarentena. Como verán, este texto no refiere a la pandemia, sino al **ecocidio que lleva más de 9 meses consumiendo y aniquilando casi la totalidad de la flora y la fauna de la mitad del país.**

Como breve recorrido sobre los incendios en sí, se pueden relacionar dos factores centrales.

Por un lado, la variable natural. El año pasado, los incendios del Amazonas que colocaron al Presidente de Brasil, Jair Bolsonaro en el ojo de la crítica mundial, e incluso en los discursos de militantes ambientalistas como Greta Thunberg y Leonardo Di Caprio, no sólo implicaron consecuencias políticas y mediáticas.

También interrumpieron los sistemas de evaporación y humedización del aire llamados “Ríos del Cielo, generando una falta de lluvias que provocó una impactante bajante del caudal del río Paraná. Esta falta de agua secó muchos arroyos y lagunas internos del delta incendiado que hubiesen funcionado como cortafuego natural frente a las quemas. Fue así como el otoño e invierno de este año se caracterizaron por temperaturas cálidas y secas.

En segundo lugar, como siempre, se encuentra la mano humana concibiendo e interpretando al medioambiente como una simple fuente de recursos explotables. Sin embargo, durante todos estos meses, esta segunda variable, por más evidente que resultara para el común de la sociedad la intervención humana en los incendios, para los estados provinciales, poderes judiciales y demás organismos encargados de delitos ambientales, la cuestión no era tan clara.

<br/>

## **Lucha de discursos**

Durante meses, a través de medios formales y redes sociales, los dos actores centrales en este debate, que poca repercusión tuvo en los medios de tiraje masivo, estuvieron representados por diferentes entidades.

Por un lado, los acusadores, encarnados en los movimientos ambientales, ONG y vecinos de distintas localidades de la región. Con simplemente recordar las performances activistas realizadas en Rosario y demandas presentadas por organizaciones ante la justicia federal con sede en esa misma ciudad, ya se puede generar una imagen al respecto.

Del otro lado, distintos productores y empresarios de la agroindustria, que poseen gran parte de los territorios incendiados, quienes apelaban a la naturalidad de las quemas, producto de la sequía y aridez de las islas. Incluso, un productor agropecuario, en junio de este año, señaló en el programa De12a14 que “hay corrientes ideológicas que están a favor de la expropiación y en contra de la ganadería”. El productor hasta relacionó su teoría con la disputa por la aplicación de la Ley 125 en el 2008 y con el reciente intento de expropiación de la agroexportadora Vicentín. “Yo no veo rebuscada a esta idea. Sí veo rebuscado, que un grupo de 40 o 50 ganaderos nos ponemos de acuerdo de prender fuego simultáneamente una superficie” concluyó.

Palabras van y palabras vienen; denuncias se cajonean por falta de evidencias; fotos y videos desgarradores inundan las redes sociales mientras acusaciones y agravios se proliferan. Al mismo tiempo, los incendios continúan, animales y plantas mueren a lo largo de los 17.500km que abarca el delta del Paraná.

Entre las grandes trabas o, si se arriesga un poco más, pretextos, que han impedido que la Justicia Federal o de las provincias afectadas por los incendios, con los respectivos organismos que cooperan en el seguimiento y denuncia de crímenes ambientales, detecte y juzgue culpables, fue la falta de evidencia concreta y la delgada línea jurisdiccional. Sin embargo, a mediados de noviembre, ciertos grises adquirieron contundentes e innegables tonos coloridos.

Pero antes, y para comprender el reciente giro discursivo y, si todo marcha bien, judicial, que será descripto en los párrafos siguientes, primero se debe describir las fuerzas productivas que operan en la ciudad bonaerense de Ramallo.

<br/>

## **Sobre un territorio**

La ciudad se ubica en la frontera natural que el Río Paraná moldea entre las provincias de Buenos Aires y Entre Ríos. La ciudad es parte del eje San Lorenzo-La Plata, una aglomeración industrial que va desde Rosario hasta La Plata, que contiene alrededor del 50% de las fábricas del país. Posee un gran puerto cerealero, además de tener unidades productivas ganaderas, porcinas, equinas y lanas.

En este límite dibujado por el Paraná se ubican las islas Lechiguanas (del Quechua lláchiwána, “avispa que produce miel”), archipiélago fluvial perteneciente al departamento Gualeguay, ubicado en Entre Ríos. Desde marzo hasta la fecha, en este grupo de islas, que abarca un territorio aproximado de 250mil hectáreas, se han registrado más de 200 focos de incendios.

Esta zona del delta, además de ser territorio explotado por la agroindustria, también alimenta economías regionales y de pequeño o mediano funcionamiento. Fue así, luego que los incendios de ese territorio fueron apagados, pescadores y vecinos percibieron irregulares movimientos.

En primer lugar, el canal Zanjón de Gregorio, que irriga las tierras entrerrianas y es además un afluente muy transitado por los pescadores y vecinos, estaba bloqueado. Los reclamos realizados por los trabajadores fueron contestados con amenazas y agresiones. Pero esto sólo fue un motivador para ellos.

<br/>

## **Sobre una ciudadanía que hace justicia**

Por otro lado, la bióloga e investigadora del CONICET (Consejo Nacional de Investigaciones Científicas y Técnicas) Natalia Morandeira, especialista en el monitoreo de indicadores ambientales en humedales, hace once años que recorre las islas Lechiguanas indagando sobre la biodiversidad de la zona para su tesis de doctorado. El 16 de septiembre presentó una denuncia a la Secretaría de Ambiente de Entre Ríos sobre la situación en el archipiélago. Repitió esta acción varias veces, frente a otros organismos estatales, como el Ministerio de Ambiente de Nación y CORUFA (Consejo Regulador del Uso de Fuentes de Agua), entre otros, pero siempre resultaban inefectivas por falta de evidencias.

Paralelamente, Unidos por la Vida y el Medio Ambiente (UPVA) es una Organización No Gubernamental conformada por vecinos de Ramallo, conformada con el fin de seguir y denunciar las consecuencias ambientales que producen las distintas empresas instaladas a orillas del río Paraná.

El 22 de octubre, UPVA, en colaboración con Morandeira y los vecinos de las islas Lechiguanas, publicaron un escrito titulado _Donde hubo fuego, negocios quedan_ en su página de Facebook. En él acusan a las autoridades de Ramallo, pero así también a las entrerrianas y nacionales, de negligencia y de inacción frente a los incendios que allí ocurren.

Ese mismo día, en YouTube, UPVA publicó un video titulado _¿Quiénes son los dueños de los incendios?__, donde presentan irrefutables evidencias sobre los planes agrocorporativos aplicados luego de las quemas, que resultaron ser una ilegal e inhumana desmalezada del territorio. El material fue filmado con un drone e imágenes filtradas desde un helicóptero propiedad del empresario agrícola Gustavo Degliantoni, gracias a un previo aviso de los vecinos.

<br/>

## **El video y los culpables**

Registrado mientras sobrevolaban el Zanjón de Gregorio, el video es explícito, contundente e incluso alevoso. En él se reconoce una embarcación transportando un Mosquito (maquinaria terrestre para la fumigación de humedales), junto con barriles agrotóxicos altamente nocivos para la vida. Además se puede ver a una retroexcavadora mientras desvía el curso natural del canal y construye un terraplén.

Pero no fue solamente la presencia de maquinaria agrícola y acondicionada específicamente para la adaptación del territorio y transformarlo en un pool de siembra. También se registró una camioneta que transportaba insumos agropecuarios con la etiqueta "Forestargen", matrícula Nº 02251, cuyo dueño es Fabio Di Fonzo, agroindustrial oriundo de María Ignacia - Estación Vela, localidad bonaerense del partido de Tandil, y dueño de la empresa El Mapuche SRL.

<br/>

![](https://assets.3dnoticias.com.ar/ecocidio.jpg)

<br/>

Di Fonzo, quien en complicidad con otros empresarios de Ramallo, pretende explotar un terreno que abarca 4800 hectáreas correspondientes a las islas Lechiguanas, donde además clausuró el acceso al Zanjón de Gregorio, riacho que baña la zona. Él y sus hermanos Nicolás y Claudio pertenecen a la tercera generación de empresarios agroindustriales.  
Además de los territorios ocupados en Entre Ríos, los Di Fonzo poseen 4.000 hectáreas ubicadas en el centro y sudeste de la provincia de Buenos Aires y donde explotan cultivos de trigo, maíz, soja, girasol y tambos.

<br/>

## **A espera de una Justicia**

Es obvio que Di Fonzo no es el único culpable del mayor ecocidio en los últimos tiempos de nuestro país. Una devastación que llegó a ocupar el segundo ranking de incendios forestales en el mundo, luego de aquellos sucedidos en California.

No obstante, es un comienzo. Es un nombre, es un culpable y eso es evidente. Fundamentalmente, culpable de haber infringido la cautelar dictada el 1 de julio por el Juzgado Federal n° 2 de Paraná, cuando falló a favor de la Asociación Civil Cuenca Río Paraná y de la Asociación Civil foro Medio Ambiental. La medida impone la “prohibición absoluta de acciones humanas con capacidad para alterar el medio ambiente, especialmente la quema de recursos naturales, actividades que impliquen riegos de incendio aún de carácter accidental; construcción de diques y terraplenes de cualquier naturaleza o realización de actividades que pongan en riegos el ecosistema identificado en la demanda”.

Las tareas reservadas para la justicia, fuerzas seguimiento de delitos y demás organismos estatales estuvieron encarnadas por vecinos, científicos y ciudadanos. Eso será tema de conversaciones futuras. Por ahora, las evidencias están. De ahora en más, la justicia quedará en manos de la Justicia.

***

Link del post en el [Facebook de UPVA](https://www.facebook.com/ONGUPVA/photos/donde-hubo-fuego-negocios-quedanqui%C3%A9nes-lucran-con-los-incendios-de-las-islas-fr/3409421215808391/)

Video de evidencias publicado en la [canal de UPVA en YouTube](https://youtu.be/78FBo1YJ0zw)

***