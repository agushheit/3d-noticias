---
layout: analisis
category: analisis
date: 2020-11-27T11:14:56.000+00:00
author: Por José Villagrán
title: 'Todos contra Sain: el ministro que resiste los embates de propios y extraños '
thumbnail: https://assets.3dnoticias.com.ar/Villagran.webp

---
Desde que asumió al frente del Ministerio de Seguridad, siempre fue discutido. Dice tener el apoyo y la misma mirada sobre la inseguridad que la del gobernador Omar Perotti. Lo sucedido en la Legislatura la semana pasada escribió un nuevo capítulo en la política santafesina.

El jueves último la Legislatura santafesina dio el puntapié a un nuevo enfrentamiento político que hasta el día de hoy resuena y escribe nuevos capítulos.

Para ir en forma cronológica, en una "jugada en conjunto", Diputados y Senadores aprobaron en tratamiento sobre tablas dos leyes "anti Sain": cambiaron el régimen de incompatibilidades en los organismos del sistema penal alcanzando así a las normas orgánicas del MPA, del Servicio de la Defensa y del Organismo de Investigaciones, este último desde donde proviene Marcelo Sain antes de asumir como ministro. Pero, además, ambos cuerpos votaron por la afirmativa sobre los gastos reservados del Ministerio de Seguridad.

El primer proyecto se presentó a comienzos de la semana pasada por los socialistas Pablo Farías y Joaquín Blanco. Además, contaron con el apoyo de los radicales Maximiliano Pullaro y Fabian Palo Oliver. Es decir, que lo propuesto surgió de los dos sub-bloques principales de la mayoría de la Cámara. El segundo proyecto, formó parte de un acuerdo con los senadores del justicialismo.

El tratamiento exprés es lo que llama la atención. Los proyectos no se encontraban en el orden del día de ninguna de las dos cámaras, por eso su tratamiento sobre tablas. Pero, además, salió todo en una misma tarde. Primero, fueron aprobabas en Diputados y luego pasó a la Cámara Alta, donde los miembros estaban, literalmente, esperando que lleguen votadas por la Cámara Baja.

Desde ese momento, la disputa política fue creciendo.

**_Los cruces y las declaraciones_**

El primero en exponer su punto de vista fue el jefe de la bancada del oficialismo, Leandro Busatto. Retiro a los diputados y luego descargó munición pesada contra el Frente Progresista en un extenso hilo de Twitter. Habló de "maniobras" que "cuyo único fin es condicionar la gestión en Seguridad". Podríamos decir que ese fue el segundo capítulo.

La tercera parte la escribe el gobierno provincial. La red social del pajarito también fue la elegida para la escritura. El ministro de Gestión Pública Ruben Michlig habló de "goles en contra", sumándose así otro protagonista más: los propios senadores oficialistas.

Es que, gracias a los votos de los representantes de los departamentos, las leyes salieron por la afirmativa. A esos "goles en contra" se refirió el titular de la cartera.

Al día siguiente, todo fue sucediéndose con mayor celeridad. Es que la palabra de Sain salió a la luz. En declaraciones radiales, el ministro de Seguridad dijo que “son leyes importantes y las sancionaron sobre tablas para decir “acá estamos”, “nosotros somos los que manejamos el poder en la provincia”. Pero no todo quedó ahí. "Soy el ministro al que le arman dos leyes con nombre y apellido y no quieren que vaya a debatir a la legislatura. No quieren que Sain pase la puerta. Me tienen miedo", redobló la apuesta.

Los senadores del PJ no se quedaron atrás y quién salió a responder fue el propio titular de la bancada en el Senado, Armando Traferri, que lo hizo por partida doble. Primero, tomó la palabra para referirse a las palabras de Ruben Michlig, a quien le dijo que "también duele cuando los jugadores tiran zancadillas desde atrás". Pero la parte más dura fue cuando dijo que "las leyes no son para una persona, nadie es tan importante", en clara referencia a Sain

**_El fin de semana nadie se tomó descanso_**

Los fines de semana suelen ser de descanso en la política santafesina. Más teniendo en cuenta que el pasado último fue largo por el feriado del Día de la Soberanía. Pero este último fue la excepción. Porque toda la dirigencia peronista alineada al gobierno provincial salió a bancar fuertemente a Omar Perotti como a Marcelo Sain. Desde presidentes comunales pasando por concejales, intendentes y demás representantes políticos, inclusive propios funcionarios del ejecutivo.

![](https://assets.3dnoticias.com.ar/./analisis1.jpg)

Para que la historia se vuelva más interesante, el fin de semana suma más protagonistas. Es que la secretaria de Derechos Humanos del Gobierno de Santa Fe Lucila Puyol, disparó munición gruesa, también a través de Twitter. "Si te golpea el narcosocialismo de Pullaro y cía, sumados con la derecha conservadora, estamos haciendo las cosas bien" expresó.

![](https://assets.3dnoticias.com.ar/./analisis2.jpg)

Y así se sumó a la discusión política todo el aparato del Frente Progresista a responderle y hasta pedir su renuncia.

Pero la funcionaria lejos de bajar los decibeles, en conferencia de prensa manifestó que “no me cabe ninguna duda de que van en contra” de Sain y agregó que “creemos que el ministro de Seguridad vino a tener una posición clara, como lo ha indicado el gobernador, de que no tener ningún trato con el crimen”.

Además, al ser consultada sobre la acusación de narcosocialismo a la oposición en las redes, la secretaria de Derechos Humanos expresó que “son datos de la realidad de quiénes votaron la semana pasada un planteo que no tiene lugar, al menos, en este gobierno”.

![](https://assets.3dnoticias.com.ar/./analisis3.jpg)

Todo dentro del peronismo está en tensión permanente. La propia, la interna, que parece ser la que más heridas percibe y recibe. En donde propios se pelean por la política que lleva adelante Marcelo Sain como ministro de Seguridad.

Y hacia afuera también, donde la relación con la oposición más consolidada se deteriora cada vez más.

**_El gobernador solo habla con hechos_**

¿Cuál es la postura de Omar Perotti frente a toda la vorágine que se viene sucediendo?

Fiel a su estilo de evitar la confrontación y dejar que los demás hablen, el gobernador este fin de semana fue con el propio Sain a visitar las instalaciones el INVAP, donde se están desarrollando programas para la seguridad interior y Santa Fe ya firmó convenios de trabajo y colaboración.

Desde el gobierno afirman que esa visita ya estaba programada en conjunto. Pero no puede dejar de señalarse que tras una semana que nuevamente el ministro estuvo en el primer plano de la escena pública, las imágenes de ellos dos parecen ser una respuesta a todo lo que se plantea.

![](https://assets.3dnoticias.com.ar/./analisis4.jpg)

A diferencia de lo que plantean los rumores, la relación entre el titular de Seguridad y el mandatario provincial se deja ver. Y le dan la razón a Sain de que ambos comparten la mirada de cómo afrontar la inseguridad en la provincia de Santa Fe.

***

##### **José Villagrán  en** [**twitter**](https://twitter.com/joosevillagran)