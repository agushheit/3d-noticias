---
layout: analisis
category: analisis
date: 2020-12-31T11:33:16Z
author: Por José Villagrán
title: El peronismo cerró filas y mostró apoyo al gobierno de Perotti
thumbnail: https://assets.3dnoticias.com.ar/Villagran.webp

---
Organizado por intendentes, presidentes comunales y legisladores, el Partido Justicialista se reunió en Sauce Viejo para mostrar unidad y sostén a la gestión de Omar Perotti. Discursos varios, con anclaje en la unidad y las críticas a los 12 años del Frente Progresista.

***

![](https://assets.3dnoticias.com.ar/3112-perotti.webp)

***

La sede fue el camping del Sindicato de trabajadores de la Sanidad de la provincia de Santa Fe, en la localidad de Sauce Viejo. La convocatoria, a todos los sectores que conforman el gobierno provincial. 

Los faltantes, casi una obviedad: el bloque «Juan Domingo Perón», que encabeza Armando Traferri en la Cámara de Senadores. La vicegobernadora, que pertenece al espacio del senador por el departamento San Lorenzo, estuvo en el Congreso presenciando el debate del proyecto de la Interrupción Voluntaria del Embarazo.

Los presentes, de forma presencial o virtual, fueron miembros del gabinete provincial, intendentes, presidentes comunales y senadores. También concurrieron al acto los senadores Eduardo Rosconi y Cristina Berra, ambos con bloques unipersonales en la Cámara Alta provincial.

Bajo el lema «Santa Fe de Pie», el acto comenzó con un video sobre cómo el gobierno enfrentó la llegada de la pandemia a suelo santafesino. Algo predecible para ser sinceros. Lo que sirvió como puntapié para los discursos, que fueron heterogéneos.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 800">Gestión, firmeza, unidad y críticas al Frente Progresista</span>**

Ocho fueron los oradores del acto, que duró casi una hora. El último fue el gobernador Perotti, claro está. El primero en hacer uso de la palabra fue el intendente de Casilda, Andrés Golosetti. Lo siguió Luis Castellanos, mandatario de Rafaela, quien hizo un breve, pero contundente apoyo desde lo personal al gobernador. «Los que estamos en el territorio sabemos cómo nos has apoyado» dijo el rafaelino, quien además utilizó sus redes sociales para seguir demostrando su lealtad al gobierno.

El senador por el departamento La Capital también dijo presente, aunque desde la virtualidad, ya que no se encuentra en el país. Fueron las palabras más medidas que se escucharon en todo el encuentro. Roberto Mirabella, presente en el Congreso nacional, fue el que dio el puntapié inicial para los discursos políticos de peso. 

«Hay muchos que quieren frenar los cambios y al gobierno», tiró el hombre de mayor confianza de Perotti, quien en su alocución dijo algo similar. 

En esa tónica siguió Carlos de Grandis, intendente de Puerto General San Martín, representante del sur provincial. «La pelea no está dentro de los propios compañeros», reflexionó y apuntó hacia la oposición: «nos han maltratado en los últimos 12 años. Tenemos mucho que decir del socialismo, no tanto de nosotros». Terminó de hacer uso de la palabra con una contundente frase pensando no solo en las elecciones de medio término del 2021 sino también en el futuro del gobierno. «Están agazapados esperándonos. No sigamos peleándonos más, si no, no nos va a ir bien».

Luego llegó el turno de los presidentes comunales, que emplearon su tiempo para hablar sobre las realidades que vivían antes, con el socialismo en la Casa Gris, y cómo fueron ayudados por el actual ejecutivo provincial.

**¿Por qué fueron elegidos estos oradores?** Por dos cuestiones fundamentales. Como hemos dicho en varias oportunidades en las columnas de _3D Noticias,_ Omar Perotti quiere mostrar acción en el territorio. Cree que es la mejor manera de bajar la política a la calle. Por eso la representatividad en el acto de los presidentes comunales e intendentes. Les dio participación, los subió al escenario y les dio voz. Nada más y nada menos. Fueron ellos los que expusieron lo cotidiano que viven en los pueblos y ciudades. Además, muestra que confía más en ellos que en el gabinete propio. 

Por algo ninguno de los ministros de primera línea hizo uso de la palabra.

Pero también eligió a quiénes dirían las palabras que hablaran dentro de las filas del justicialismo. Castellanos, Mirabella y De Grandis, personas muy cercanas, que conforman la mesa de decisiones del gobierno provincial. Fueron estos tres, además del gobernador, fueron los encargados de «bajar línea» a la dirigencia peronista santafesina. Dirigentes de peso en el territorio y también puertas adentro del justicialismo.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 800">«El peronismo sabe separar la paja del trigo»</span>**

La antesala de discurso de Omar Perotti ya todos los conocemos. Conocemos la realidad política que hoy por hoy estamos viviendo. El interés estaba puesto en saber si el mandatario «se la jugaría» públicamente entre un sector u otro. Y si bien discursivamente no lo dijo, hay que ver quiénes asistieron al encuentro y quiénes no. Basta ver la figura del ministro de Seguridad Marcelo Saín y la ausencia de Armando Traferri, aunque días atrás lo había invitado al encuentro el presidente del Partido Justicialista Ricardo Olivera.

La frase más contundente que emitió el mandatario local apuntó a la oposición más fuerte que tiene el justicialismo: el socialismo que comanda Miguel Lifschitz. «Vemos a muchos periodistas preguntando por la interna del PJ, o por la pelea de los distintos sectores. Pero nadie se pregunta qué se hizo en los últimos doce años para que explote hoy la realidad», tiró, para seguir en ese mismo camino y afirmar que «a veces, los frenos que se ponen no son para que el gobierno arranque. Tuvimos varias de estas: presupuesto, leyes que no salían, no aparecían los recursos. Quieren frenar una forma distinta de hacer política.»

«Hay investigaciones de hechos que no sucedieron ahora, sino que vienen de gobiernos pasados. Esto es una discusión entre corrupción o decencia», completó.

<br/>

**<span style="font-family: Helvetica, sans-serif; font-size: 1.15em; font-weight: 800">¿Relanzamiento o muestra de poder?</span>**

El encuentro de dirigentes justicialistas en tiempo de pandemia tuvo el toque perottista que caracteriza al primer mandatario de la provincia. Por eso, en vez de ser un relanzamiento de la gestión, fue un gesto de apoyo a la misma. Esa es una parte de la respuesta a la pregunta.

En sus palabras se nota cuál es el horizonte que mira Perotti. <span style="color: #EB0202;">«Van a tener en mí a un gobernador trabajando siempre por la unidad de los santafesinos»</span>, afirmó. El mensaje es para afuera, pero también para adentro.

Claro está que, como también lo expresamos en este portal, la conformación de un gobierno con distintos sectores del peronismo hace que se tengan distintas miradas sobre el transitar y las medidas a tomar durante la gestión. Y que, si no se sabe mediar y tener el equilibrio necesario, puede pasar lo que le viene pasando hasta ahora al actual ejecutivo: roces, declaraciones desatendidas, rumores de renuncia y un equipo de trabajo carente de organización.

**Pero también es una muestra de poder.** Lo que hizo ayer Perotti fue demostrar que no necesita de ministros o senadores que actúen como interlocutores para poder hablar con los representantes del territorio y que ellos le respondan. Es una demostración de poder a medias. No debe olvidarse el gobernador que quienes mandan en los departamentos son los senadores y son ellos quienes, en una elección, deciden el futuro de los votos.

Es un equilibrio fino en el que debe manejarse el gobierno. El mensaje de unidad para mantener al gobierno y seguir en el poder fue lo que predominó ayer. Y por lo que dijo ayer, <span style="color: #EB0202;">«no hay unidad si no se cortan los lazos entre el Estado y el crimen organizado».</span>

<br/>

<hr style="border:2px solid red"> </hr>

**José Villagrán en** [**twitter**](https://twitter.com/joosevillagran)

Otras columnas del autor en este portal:

[La Santa Fe de Perotti: sin paz ni orden](https://3dnoticias.com.ar/analisis/la-santa-fe-de-perotti-sin-paz-ni-orden/) | [Un año de Perotti gobernador](https://3dnoticias.com.ar/analisis/un-ano-de-perotti-gobernador-pandemia-disputas-internas-y-un-gobierno-sin-articulacion/) | [El presupuesto 2021 de Omar Perotti](https://3dnoticias.com.ar/analisis/el-presupuesto-2021-de-omar-perotti-que-dice-como-fue-votado-y-cuales-son-las-criticas/) | [Todos contra Saín](https://3dnoticias.com.ar/analisis/todos-contra-sain-el-ministro-que-resiste-los-embates-de-propios-y-extranos/) | [El gabinete de Perotti: un equipo rodeado de rumores](https://3dnoticias.com.ar/analisis/el-gabinete-de-perotti-un-equipo-rodeado-de-rumores/) | [Que no se den PASOS en falso](https://3dnoticias.com.ar/analisis/que-no-se-den-pasos-en-falso/)