---
layout: analisis
category: analisis
date: 2021-02-08T07:17:37-03:00
author: Jorge Grispo para Infobae
title: 'Ciencia vs. Política: la autocracia de la vacuna lleva de la esperanza a la
  incertidumbre'
thumbnail: https://assets.3dnoticias.com.ar/vacunate.jpg

---
Si hay una cosa que no se puede hacer, bajo ningún concepto, es darle a la vacuna un uso político con fines electorales. Los “vacunatorios” no son propiedad de unos pocos.

Está en juego la salud de una nación entera.

En honor a la verdad, no es un dilema que se presente únicamente en nuestra empobrecida nación, pero como dice el refrán popular, “mal de muchos consuelo de tontos”.

Serán los hospitales, las farmacias, los centros de salud, las fuerzas armadas en los lugares inhóspitos, pero en ningún caso la grieta se puede dar el lujo de apropiarse de la inoculación nacional y popular.

Hacerlo nos degrada como nación. Podemos y debemos ser mejores que eso. No todo vale. No todo se puede romper. Y no todos los límites se pueden cruzar.

Inocular a la población es una obligación del Estado, no un concesión que se nos hace. Vacunar no es, ni debe ser, un capítulo más de los relatos salvajes de la política que nos tienen atrapados y sin salida.

Valga por caso la transmisión del despegue y regreso del avión de Aerolíneas Argentinas, cuya misión fue traer las primeras dosis de Rusia con “amor”, exhibiendo la manipulación que se pretendió realizar de actos que el estado nacional no podía dilatar.

La épica que se le imprimió desde la política al “Vuelo 1061 del General Alais” rememora el marasmo del Estado en el cumplimiento de objetivos impostergables, exponiendo la barbarie de los relatos de la política salvaje en su máxima expresión.

Luego siguió una inestabilidad destacable en la provisión de la vacuna que todas y todos estamos esperando, y, por el momento, sin certezas y con demasiada incertidumbre sobre la provisión real y concreta del líquido sanador.

De la esperanza a la incertidumbre pasamos en muy poco tiempo. Los argentinos estamos acostumbrados y somos resilientes. La inoculación masiva deberá aguardar mientras vemos que en otras naciones ya se han vacunado más de cien millones de personas.

En todo el mundo escasea la vacuna y hay complicaciones, pero en nuestro caso, esos problemas se potencian porque el pasado nos condena a una triste realidad actual.

Problemas hay en todas partes, pero en nuestro país lo grave es la sumatoria de los años de decadencia institucional que nos han postergado como sociedad y nos condenan a ser una aldea pobre al sur del mundo.

![Alberto Fernández recibió la vacuna Sputnik V](https://www.infobae.com/new-resizer/rnexx5pyBA1vehSEBXkCaG69osg=/420x236/filters:format(jpg):quality(85)//cloudfront-us-east-1.images.arcpublishing.com/infobae/2GISYBAVBNFO3CKEJVBEWGEVLE.jpeg)

Lo que debemos preguntarnos frente a estos cuestionamientos es: ¿Por qué nos pasa a nosotros? ¿Por qué seguimos dando vueltas sobre las mismas problemáticas, como el perro que pretende morderse la cola? ¿Por qué los monólogos de Tato Bores siguen siendo tan actuales?

Lamentablemente no hay una sola respuesta. Sería muy fácil. Es un conjunto de respuestas y muchas contradicciones, cuya sumatoria confluye en el desbarranco actual de nuestra nación.

Es la corrupción, es el silencio de los que callan, es la impericia, las ambiciones personales por sobre las sociales, son las leyes laborales que solo generan más desempleo, es la carga fiscal imposible de soportar, es la grieta. Son las vacunas que se ofrecen bajo la mesa a figuras públicas.

Es mucho más que todo eso junto. Lo cual nos transporta a otra dimensión.

La autocracia es un forma de gobierno que concentra el poder en una sola figura (a veces divinizada) cuyas acciones y decisiones no están sujetas ni a restricciones legales externas, ni a mecanismos regulatorios de control popular.

Tiene cierta conexión con el concepto de “superioridad”, en el sentido de ser más que los otros.

En síntesis, una autocracia se encuentra definida por el poderío y supremacía de un solo individuo frente al grupo que gobierna; en este sistema el autócrata tiene la potestad absoluta de regular leyes y reglamentos a conveniencia y sus seguidores atienden a sus órdenes con ciego fanatismo.

Hace décadas que los argentinos no vivimos tiempos de tanta incertidumbre, angustia y temor cómo en la actualidad (la dictadura militar, por caso, una inmensa mayoría de argentinos nacieron luego o eran niños y no la vivieron en su real dimensión).

La ciencia en el mundo tuvo un éxito rotundo. En menos de un año se desarrollaron y produjeron exitosamente varias vacunas que ya están siendo inoculadas. Con la llegada de la “vacuna” rusa tuvimos un manto de esperanza. Duró poco. Al ser una aldea pobre, nos toca como lógica consecuencia, ponernos en la cola de los laboratorios y esperar el “turno”.

Pasamos de la esperanza a la incertidumbre en muy poco tiempo, a consecuencia del desgaire de nuestros gobernantes. Pero eso no es lo único que debe preocuparnos.

En un año electoral, el “vale todo” para ganar los comicios, lamentablemente es la regla, dentro de la cual el uso político de la vacuna no parece ser la excepción y la cooptación de los afortunados inoculados el premio.

Con este incierto escenario por delante, arrancamos una año impar, donde ganar las elecciones serán el norte a seguir, y con ello la rémora de la clase dirigente de gobernar para ganar una elección sin centrar todos sus esfuerzos en sanar una nación arrasada por las cinco pandemias: salud, educación, instituciones, seguridad y economía.

Tanto la carencia de solidez moral de la dirigencia política, como los intentos reiterados de adoctrinamiento partidario mediante el uso del clientelismo político, dan mayor brío al calor de la grieta, y nos vuelven a poner a prueba, a la vez que nos marca el estilo convulsionado y decadente de nuestra forma de vida actual.

Tenemos una nación rica en pobres, con más del cincuenta por ciento de la población en estado de necesidad permanente que es asistida por el estado, cuyas dos necesidades primarias son comer y tomar agua. Esto genera una fuerte tensión en toda la sociedad.

En el medio, las tendencia autocráticas ponen en jaque a todas las instituciones al mismo tiempo, con fuerzas ajenas al Estado, pero que lo contaminan permanentemente, por caso, movimientos políticos que se apropian de empresas públicas como si fueran propias y las convierten en agencias de trabajo para su tropa.

Estamos siendo testigos silentes del despilfarro de lo público, y al mismo tiempo del hambre de la mitad de la población, valga como ejemplo funcionarios que ofrecen puestos pagados por el Estado a sus trabajadoras domésticas, donde se suman los dos aspectos que destacamos: la necesidad y el dispendio de lo público.

No se cuida la caja del Estado, se la usa para hacer política y mucho más en un año “impar”. Lo cual nos coloca ciertamente en un estado de incivilidad, donde se hace más cruda la lucha de la grieta salvaje. Las peleas por el control de las “cajas” del Estado es todo un tema en sí mismo que expone de la forma más cruda posible nuestra decadencia.

Prevalecen la irracionalidad del interés individual por sobre la racionalidad del bienestar de la sociedad, la cual anestesiada en algunos casos y cómplice en otros, tiene que ocuparse de la Pandemia, la falta de trabajo, los problemas que a diario vivimos por la falta de seguridad en nuestra calles, solucionar los dramas de la deseducación de nuestros hijos, pagar impuestos asfixiantes y todo aquello que implica vivir en estas tierras australes donde la incertidumbre es la regla del día a día.

Ínterin la grieta convive con las pugnas de las ansiedades electorales, los problemas de la sociedad quedan de lado porque está la urgencia de ganar una elección para perpetuar un proyecto o bien para recuperar la posibilidad de volver al gobierno, dependiendo de qué lado se lo mire.

Esa realidad nos impone reglas de juego que solo nos hacen peores como sociedad, con una fuerte propensión al fracaso recurrente. La autocracia de la vacuna es un pecado capital que termina traspasando todo los límites éticos y morales.

Esto nos abre la puerta a una nueva interpretación respecto del goteo con el que llega la vacuna, que seguramente será superado a medida que se acerquen las fecha del calendario electoral.

¿Simple coincidencia o sincronización perfecta? Un poco de las dos quizás. Nos queda el interrogante que la historia futura nos revelará en algún momento.

Solo falta que algún trasnochado pretenda que los vacunatorios se encuentren poblados de propaganda política o remeras que dan cuenta de la pertenencia a un sector de la grieta -como sucedió en alguna de las inundaciones que tuvimos en el pasado.

No por nada en la Provincia más “grande”, ya se está pensando. Provincia que fue definida por un referente de la opción como la “madre de todas las batallas” en el próximo turno electoral.

Mientras todo esto sucede, también pasan otras cosas. Tenemos a la mitad de la población con carencias básicas sin cubrir, lo que los coloca en la urgencia de recibir una mayor asistencia del estado, son más dependientes de él y por ende los hace más “cooptables”.

Y por cierto, son muchos votos. Son los que definen una elección. Es la fórmula clásica del clientelismo político vejatorio de la condición de los más necesitados.

Los pobres y carenciados, no son estigmatizados por su señalamiento, sino por la falta de tratamiento adecuado de sus necesidades. No participan del poder, de la riqueza, de una vida sin preocupaciones. Todo lo contrario.

Por eso son las verdaderas víctimas del fracaso nacional y popular que significa el modelo de país que venimos padeciendo por largas décadas. Lamentablemente para ellos el acceso a la vacuna se parece más a una utopía que a una realidad, la vacuna bajo “la mesa” no es para ellos.

El modelo autócrata asume para sí que existe un patrón dominante de asistencialismo. Sus consecuencias se manifiestan en los diversos grados de ayuda social, de tal modo que se terminan “reforzando” las desigualdades de los desiguales, a la vez que los alejan cada vez más de una vida de mejor calidad.

Basta con caminar un barrio carenciado para ver esa realidad que nadie quiere mirar de frente.

Hay claramente un patrón de financiamiento para los carenciados nacionales y populares, basado en características y rasgos culturales con sistemas de credos políticos dependiendo del lugar dónde se vote. No es lo mismo La Matanza que Arequito, más allá de sus hermosos paisajes campestres y su buena música.

La clase dirigente viene errando el camino. No se bajaron sus dietas, pero sí sancionaron más impuestos, que, en el caso del Aporte Solidario, no es más que un eufemismo marketinero propio del desabarranco que vivimos.

Pero también observo, con preocupación, que se privilegia la inoculación de la clase dirigente y un selecto grupo de acomodados por encima de los trabajadores de la alimentación, supermercados, farmacias, transporte, más un largo etcétera de actividades que durante la cuarentena “dura” y extra larga que hicimos salieron a ponerle el pecho a la pandemia y mantuvieron nuestra nación en funcionamiento.

¿Por qué esos trabajadores esenciales no son vacunados primero?

No nos olvidemos que la inflación -el impuesto que pagan los laburantes de a pie, los que no tienen chofer- les produjo en los hechos una baja sustancial de sus salarios.

El último en abandonar un barco que se hunde debe ser el capitán, según la tradición marina.

Traspolando el concepto, los últimos en vacunarse deberían ser los que están al frente de los destinos de nuestra nación, a la vez que debieron ser los primeros en bajar sus dietas como señal de solidaridad con el pueblo que gobiernan.

Las desigualdades no son iguales para todos. Los privilegios tampoco.