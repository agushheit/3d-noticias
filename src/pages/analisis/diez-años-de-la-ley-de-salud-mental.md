---
layout: analisis
author: Por Lucrecia Faccioli
category: analisis
title: Diez años de la Ley de Salud Mental
date: 2020-11-25T12:52:58.314+00:00
thumbnail: https://assets.3dnoticias.com.ar/lucrecia.webp

---
### **Derechos y resistencias**

El 25 de noviembre próximo se cumplen 10 años de la Ley 26657 que en su artículo 3 define a la salud mental: “como un proceso determinado por componentes históricos, socioeconómicos, culturales, biológicos y psicológicos, cuya preservación y mejoramiento implica una dinámica de construcción social vinculada a la concreción de los derechos humanos y sociales de toda persona.” O sea, no es solo una cuestión biológica o psicológica.

Tal vez porque los números redondos siempre invitan a una reflexión o porque hay voces que  se alzan en contra con tanta vehemencia por estos días,  me parece oportuno referir al **cambio de paradigma en los enfoques, tratamientos, abordajes y modalidades que consagró la Ley de Salud Mental**.

***

![](https://assets.3dnoticias.com.ar/salud-mental.jpg)

***

En primer lugar, cabe mencionar que **es la primera ley de salud de la República Argentina en considerar a la misma como un Derecho Humano** de las personas. En línea con los Principios de Naciones Unidas para la Protección de los Enfermos Mentales y para el Mejoramiento de la Atención de Salud Mental, adoptado por la Asamblea General en su resolución 46/119 del 17 de diciembre de 1991. Asimismo, la Declaración de Caracas de la Organización Panamericana de la Salud y de la Organización Mundial de la Salud, para la Reestructuración de la Atención Psiquiátrica dentro de los Sistemas Locales de Salud, del 14 de noviembre de 1990, y los Principios de Brasilia Rectores; para el Desarrollo de la Atención en Salud Mental en las Américas, del 9 de noviembre de 1990, se consideran instrumentos de orientación para la planificación de políticas públicas, tal como lo enuncia en su artículo 2.

Otra cuestión sumamente importante es la **presunción de la capacidad** **de todas las personas**. ¿Qué significa esto? Que toda persona con padecimiento subjetivo puede tomar decisiones por sí mismo.

Salvo que exista una declaración de incapacidad judicial y se haya designado un curador/a que sustituya su voluntad, esta medida es absolutamente excepcional y debe ser revisada cada tres años.

El Código Civil y Comercial de la República Argentina introdujo el instituto de la capacidad restringida y la posibilidad de proveer de apoyos a quienes lo necesiten para poder tomar algunas decisiones.

Esto implica básicamente **considerar a las y los usuarios/as de los servicios de salud mental como sujetos de derechos**.

El núcleo de este precepto, está contenido en la enumeración detallada de **Derechos** en 16 incisos del artículo 7, siendo el primero de ellos el siguiente:

> “Derecho a recibir atención sanitaria y social integral y humanizada, a partir del acceso gratuito, igualitario y equitativo a las prestaciones e insumos necesarios, con el objeto de asegurar la recuperación y preservación de su salud”

Algunas voces detractoras de esta Ley, invocan uno de estos derechos, el “Derecho a no ser sometido a trabajos forzados”, para desacreditar a la misma, alegando que jamás en una institución psiquiátrica sucede eso.

Desconocer que la vulneración de derechos ha sido parte de la historia de los manicomios de la Argentina es desconocer [Vidas Arrasadas, La segregación de las personas en los asilos psiquiátricos argentinos](https://www.cels.org.ar/common/documentos/mdri_cels.pdf), Informe sobre Derechos Humanos y Salud Mental en Argentina,   CELS,  2006.

El solo hecho de estar privado de la libertad durante años en una clínica u hospital psiquiátrico, como ha sucedido y como sucede, cercena la dignidad y la posibilidad de construir una vida con proyectos, lazos, afectos, singularidad, deseos.

Sabemos que en momentos de crisis alguien puede necesitar ser asistido desde una internación, lo que sería deseable es que fuera en un hospital o clínica general, y que una vez superado el momento  pueda continuar su tratamiento, en caso de ser necesario,  en forma ambulatoria, lo más cerca posible de sus afectos, familia, amigos, clubes y redes comunitarias.

En el artículo 9 dice:

> “El proceso de atención debe realizarse preferentemente fuera del ámbito de internación hospitalario y en el marco de un abordaje **interdisciplinario e intersectorial**, basado en los principios de la atención primaria de la salud. Se orientará al reforzamiento, restitución o promoción de los lazos sociales.”

Es mentira que la Ley de Salud Mental no permite las internaciones, es más, tiene un capítulo entero (VII) destinado a regular las mismas.

El principio que establece es:

> “La internación es considerada como un recurso terapéutico de carácter restrictivo, y sólo puede llevarse a cabo cuando aporte mayores beneficios terapéuticos que el resto de las intervenciones realizables en su entorno familiar, comunitario o social. Debe promoverse el mantenimiento de vínculos, contactos y comunicación de las personas internadas con sus familiares, allegados y con el entorno laboral y social, salvo en aquellas excepciones que por razones terapéuticas debidamente fundadas establezca el equipo de salud interviniente.”

Otro asunto no menor, que trata la Ley es el de los consumos problemáticos: “Las adicciones deben ser abordadas como parte integrante de las políticas de salud mental. Las personas con uso problemático de drogas, legales e ilegales, tienen todos los derechos y garantías que se establecen en la presente ley en su relación con los servicios de salud.”

Esta es una deuda pendiente, al menos en el ámbito de la provincia de Santa Fe, salvo honrosas excepciones. La gran deuda pendiente es la sustitución definitiva de las instituciones de internación monovalente cuyo plazo se cumplía en el año 2020.

Para poder llevar adelante estos lineamientos, la Ley creó un Órgano de Revisión Nacional con el objeto de proteger los derechos humanos de los usuarios/as de los servicios de salud mental, en el que fue designada la Dra. María Graciela Iglesias en el año 2013.

En la provincia de Santa Fe está en proceso de conformación el órgano de Revisión creado por Ley N° 1373/17.

El Decreto Reglamentario N° 603/13 creó la Comisión Nacional Interministerial en Políticas de Salud Mental y Adicciones (CONISMA) en el ámbito de la Jefatura de Gabinete de Ministro y  se convocó a las organizaciones de la comunidad que tengan incumbencia en la temática, en particular de usuarios y familiares y de trabajadores, para participar de un Consejo Consultivo de carácter honorario (CCHSMA) al que deberá convocar al menos trimestralmente, a fin de exponer las políticas que se llevan adelante y escuchar las propuestas que se formulen.

Estos mecanismos institucionales fueron instrumentados para garantizar la continuidad del camino iniciado en el 2010 y, a pesar de los esfuerzos realizados, aún persisten indicadores que reflejan la falta del enfoque de derechos en la atención de salud mental en la provincia de Santa Fe.

<hr style="border:2px solid red"> </hr>

**Lucrecia Faccioli**  |  Abogada. Trabajadora de Salud Mental