---
layout: analisis
category: analisis
date: 2020-12-22T12:30:18Z
author: Por Marisa Lemos
title: El trastorno de vivir alquilando
thumbnail: https://assets.3dnoticias.com.ar/Marisa-Lemos.webp

---
Cuando se nos termina el contrato y llega la hora de buscar un nuevo lugar para alquilar, nuestra vida se transforma, gobernada por el estrés, y la pregunta que nos acecha es ¿adónde voy a ir a parar?

Como sabemos (y los que no lo sabían se están enterando), conseguir una casa o un departamento que cumpla con nuestras expectativas y que sea acorde con nuestro presupuesto es  complicado. Sobre todo por el presupuesto, no nos olvidemos de que estamos en Argentina.

Hay algunas que cumplen con nuestras expectativas, pero se exceden en el presupuesto de dinero que tenemos disponible, están muy lejos, los requisitos son muy específicos o, peor aún, no aceptan  niños ni mascotas. Lo único que les falta  pedir es un unicornio rosado y una uña de dragón.

Es una experiencia que me tocó transitar varias veces en mi vida, la última hace escasas semanas.

Desde que supimos que nos teníamos que mudar, visitamos alrededor de diez casas. Algunas cumplían nuestras expectativas, pero  a un precio excesivo. O pedían muchos requisitos. Lo más exasperante era ver el deterioro de la vivienda en contraste con el precio del alquiler. ¡NO LO VALÍA! A través de fotos mostraban una cosa y, en vivo y en directo, la realidad lejos estaba de aquella imagen que nos había tentado.

<br/>

## **Datos**

Según algunas estadísticas, **el 40% de la población santafesina alquila**, pero lo más preocupante es que **los inquilinos destinan al alquiler un 41% de sus ingresos**.

¿Cómo llego a fin de mes? Es lo que todo inquilino (que destina casi la mitad de su sueldo para el alquiler) se pregunta día y noche, al punto de no conciliar el sueño.

**Según el INDEC, las principales preocupaciones a la hora de alquilar se reparten entre:**

* el precio
* los requisitos económicos
* los requisitos administrativos

Otro detalle a tener en cuenta es que las personas prefieren alquilar a través de una inmobiliaria que de forma particular, porque, si bien la inmobiliaria tiene más requisitos, brinda mayor tranquilidad a los inquilinos. Aunque a veces no te quieren alquilar un inmueble por el simple hecho de tener hijos y mascotas, y esto lo padecen más las mujeres.

Durante el transcurso de la pandemia, se aprobó la **ley de alquileres**, en la que se estipuló que **no se podían aumentar los alquileres ni desalojar a los inquilinos durante el tiempo de cuarentena**. Premisa que algunos propietarios e inmobiliarias cumplieron y otros no. El aumento de la tarifa, sumado a la imposibilidad de trabajar por la situación de aislamiento social, preventivo y obligatorio, llevó a que **el 60% de los inquilinos se atrasara en los pagos** y la deuda no solo creciera, sino que se acumulara.

<br/>

## **Conclusión**

Durante mi largo periodo de búsqueda de hogar, me he dado cuenta de que cada vez resulta más difícil conseguir un lugar decente,  y las inmobiliarias, lejos de aligerar los trámites, te complican hasta el límite que de querer tirar todo por la borda e irte a vivir debajo de un puente… para no usar una expresión  poco elegante.

Ustedes qué piensan: ¿Se aprovechan de las necesidades de la gente? ¡LA RESPUESTA LOS SORPRENDERÁ!

<hr style="border:2px solid red"> </hr>

**Marisa Lemos**

Otras columnas de la autora en este portal:

[Todo sea por el juego](https://3dnoticias.com.ar/analisis/todo-sea-por-el-juego/ "Todo sea por el juego") | [Hijos del rigor](https://3dnoticias.com.ar/analisis/hijos-del-rigor/ "Hijos del rigor")