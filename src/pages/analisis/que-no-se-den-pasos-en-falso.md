---
layout: analisis
author: Por José Villagrán
category: analisis
title: Que no se den PASOS en falso
date: 2020-11-11T23:29:48.826+00:00
thumbnail: https://assets.3dnoticias.com.ar/Villagran.webp

---
El gobernador de la provincia, Omar Perotti, dijo el viernes de la semana pasada que se está analizando la suspensión de las Primarias, Abiertas, Simultáneas y Obligatorias del próximo año. A diez meses de las mismas, se aduce que la actual situación pandémica requiere postergarlas.

Desde que Omar Perotti afirmó que "transitoriamente podríamos dejar las PASO del próximo año", la política de Santa Fe se vio envuelta en una discusión tan inesperada como innecesaria”.

El principal argumento que esgrime el primer mandatario santafesino es la actual situación sanitaria que atraviesan la provincia y el país por el Covid 19. Cierto es que la provincia, hoy por hoy, está dejando atrás el pico de contagios, sobre todo en el sur de la bota santafesina. Pero aun así, surge como interrogante la necesidad de instalar semejante debate.

A diez meses de las elecciones de medio término, el tiempo parece ser un argumento en contra de esa idea. El 2020 fue un año decididamente malo, angustiante y con muchas crisis. La sociedad no ve la hora de que estos doce meses queden atrás. Hay un estrés lo suficientemente notorio como para sacar de esta complejidad al electorado y meterlo en otro tema que también resulta estresante, por lo menos desde hace un tiempo a esta parte.

Al gobierno le faltó tacto y timing para poder instalar el debate. No supo (y no sabe) interpretar qué es lo necesario en este momento. Y además de hacerlo de esta manera, trae a colación un sistema que en la provincia ya caducó: el de lemas.

Hay un sector de la oposición que, desde la campaña del año pasado, insiste en vincular a este gobierno con los años 90 y el neoliberalismo. Ante la suspensión de las PASO, es  poco inteligente traer como propuesta  una especie de "neolemas". La ley de Lemas quedó definitivamente fuera de la estructura electoral desde hace años en la provincia y lo único que hace es rememorar esas viejas épocas en Santa Fe, donde los votos iban direccionados para un lado, pero terminaban en otro.

A veces da la sensación de que a la actual gestión provincial le cuesta, incluso, reconocer los caminos que elige y cómo los transita. No se nota que haya, dentro del ejecutivo, alguien que diga: "No, por acá no es". Se expone al gobernador a la crítica política constante, sobre una gestión que no tiene muchos resultados  para mostrar en condiciones excepcionales como las que nos impone el Coronavirus.

La semana pasada, el partido justicialista conformó la lista de unidad para renovación de autoridades. Ricardo Olivera seguirá presidiendo el peronismo santafesino. Un reconocimiento a quien, de forma silenciosa y haciendo un trabajo de hormiga, fue construyendo la unidad y la victoria electoral de Omar Perotti y Alejandra Rodenas. Cuando se decidió esto, el paso siguiente  fue una orden estricta a la militancia: defender la gestión de Omar Perotti y Alejandra Rodenas. ¿Cómo se puede defender una gestión que como "propuesta superadora" a determinada contingencia propone algo que ya está, ya pasó, ya fue?

Diez meses en este país es mucho tiempo, frase hecha si las hay, pero no por eso deja de ser cierta. El gobierno provincial está a tiempo de proponer algo más moderno y mejor presentado. Está a tiempo de darle tiempo a la sociedad para que descanse, se recupere y pueda salir a pensar en otros temas. Y además, está a tiempo de evitar que las PASO se den en falso.

***

##### **José Villagrán  en** [**twitter**](https://twitter.com/joosevillagran)