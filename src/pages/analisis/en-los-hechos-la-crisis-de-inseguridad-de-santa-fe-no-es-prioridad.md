---
layout: analisis
category: analisis
date: 2022-04-18T10:51:05-03:00
author: Juan Martin Diputado Nacional UCR
title: En los hechos, la crisis de inseguridad de Santa Fe no es prioridad
thumbnail: https://assets.3dnoticias.com.ar/juanchi.jpg

---
¿A qué parámetros objetivos responde la disparidad en la ejecución presupuestaria entre las provincias?

Se suele decir que un presupuesto manifiesta un plan de gobierno, una plataforma de acción política. Que ordena prioridades de una gestión y determina qué demandas se atenderán en primer lugar y cuáles deberán esperar. Esto es cierto, aunque relativamente. **Lo que indica efectivamente qué es prioridad para un gobierno es la ejecución, la materialización concreta de esa previsión presupuestaria.**

Es ahí donde las expresiones de voluntad y las manifestaciones rimbombantes se chocan de frente con los hechos y las acciones o inacciones de un gobierno.

  
**En el país en general, pero en la provincia de Santa Fe en particular, existe una demanda insatisfecha que crece exponencialmente: la seguridad ciudadana.** Ese anhelo profundo de millones de argentinos que quieren recuperar la tranquilidad perdida y vivir en paz.

Teniendo en cuenta esta demanda y analizando el presupuesto nacional, hay un dato que no deja de llamar la atención: **la ejecución de los recursos nacionales destinados a Santa Fe para esta finalidad, supera apenas el 100% pautado.** Visto así parecería un dato positivo. Pero en el mismo ítem, en otras jurisdicciones, la ejecución del gasto ha sido muy superior a la presupuestada originalmente.

Para ser precisos: **la ejecución de la finalidad Seguridad Interior del presupuesto nacional fue en 2021 un 130% superior al votado, registrando en función de la clasificación geográfica ejecuciones muy dispares.** En el tope de sobre-ejecución están la provincia de La Rioja (659%), San Luis (626%) y Catamarca (458%); en tanto que en el extremo inferior se encuentran Misiones (85%), CABA (100%), y la provincia de Santa Fe (107%). La inversión en la provincia de Santa Fe representa el 2,06% del presupuesto votado y el 1,69% del presupuesto efectivamente ejecutado.

Primera conclusión: como advertimos oportunamente, el presupuesto original quedó totalmente desactualizado por el **proceso inflacionario** (que a su vez significa mayor recaudación), y por tanto **ejecutar el 100% de lo presupuestado es desde ya muy pobre ante tamaño problema como el de la inseguridad en Santa Fe.**

De acuerdo con la inversión final que realizó el gobierno nacional en todo el país en seguridad (una sobre-ejecución del 130%), la provincia de Santa Fe dejó de percibir casi 700 millones de pesos. Ello sólo para alcanzar el promedio nacional. La arbitrariedad en la reasignación de partidas no guarda correlato alguno con las necesidades de una sociedad conmocionada como la santafesina, con resultados trágicos día tras día. En medio de una crisis de inseguridad que se lleva vidas, que implica para cientos de miles de santafesinos vivir encerrados o salir a la calle con temor a ser asaltado, el Estado nacional no hace ningún esfuerzo adicional. **Con suerte se limita a cumplir con una pauta que prontamente quedó desactualizada. Es casi una resignación.**

Es más, en la práctica, la relación parece ser inversa: a menor necesidad, mayores recursos. **¿A qué parámetros y fundamentos objetivos responde la disparidad señalada en la ejecución presupuestaria?** Seguramente otras provincias también presentan dificultades, pero el particular destrato a Santa Fe por parte del Frente de Todos, se hace manifiesto una vez más.

Llaman aún más la atención estas cifras cuando el mismo Jefe de Gabinete y el Ministro de Seguridad de la Nación se comprometieron a reforzar la presencia de efectivos federales en la provincia. Recordemos: debían llegar 1.575 efectivos federales a marzo de 2022 y no tenemos ninguna novedad ya promediando el mes de abril. Presentamos un pedido de informes solicitando detalles sobre esta situación y no hemos aún tenido respuestas.

Los números de la ejecución presupuestaria parecen poner en blanco sobre negro las razones del silencio: los 700 millones faltantes alcanzaban holgadamente para cumplir con el compromiso asumido de presencia federal en territorio santafesino ya en 2021. **Los agentes federales faltan porque Santa Fe no es prioridad para el gobierno.**

Pero no es solo un problema del gobierno nacional. Algo similar sucede en la provincia. La finalidad Servicios de Seguridad es la que menor ejecución tuvo: 105% contra la ejecución total del presupuesto del 140%, Servicios Económicos del 142%, Servicios Sociales del 133% o Administración Gubernamental del 110%. La finalidad Seguridad representó el 9,46% del presupuesto votado y el 7,1% del presupuesto ejecutado de la Administración Provincial.

El escenario es sombrío. La Provincia ejecuta a rajatabla el presupuesto (cuando sobre-ejecuta el resto de las partidas) y la Nación mira para otro lado, duplica, triplica y quintuplica los presupuestos de otros distritos y en Santa Fe solo se remite a cumplir.

No podemos seguir así. No hay lugar para excusas ni más tiempo que perder. Más allá de los anuncios, las fotos y las declaraciones altisonantes, para resolver la crisis de inseguridad que vivimos los santafesinos, necesitamos hechos concretos.