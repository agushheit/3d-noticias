---
layout: analisis
category: analisis
date: 2021-05-27T08:53:01-03:00
author: Jesús Rodriguez*
title: La trampa de la falsa antinomia
thumbnail: https://assets.3dnoticias.com.ar/JR.webp

---
La Argentina lleva 13 meses de emergencia pública y no tiene por delante un horizonte claro de retorno a la normalidad. En este contexto se hace imperioso agudizar la mirada y exigir que el Estado obre con transparencia, criterio, equidad, eficacia y rendición de cuentas. Las falsas antinomias, como “salud vs. economía” o “salud vs. educación” solo nos hacen perder valioso tiempo para gestionar la pandemia y disminuir sus múltiples efectos negativos. 

**Que no se quiebre**

Luego de que, en enero de 2020, la Organización Mundial de la Salud declarara la existencia de una emergencia de salud pública de importancia internacional, gobiernos de todo el mundo fueron decidiendo estados de excepción con el fin de brindar rápidas respuestas a la crisis.

Los estados de excepción pueden tomar diversas formas  pero en todos los casos tienen al menos dos grandes costos asociados: 1) la concentración de poder en el órgano ejecutivo, y 2) la restricción de libertades individuales.

La lógica detrás de los estados de excepción es que durante un período limitado -según la duración y el desarrollo de la crisis -, la respuesta rápida y prioritaria a la emergencia tiene un valor mayor para el conjunto de la población que la afectación de las reglas democráticas.

Mas allá de la justificación racional del caso, lo cierto es que 2020 fue el año de mayor retroceso mundial en libertades individuales del que se tenga registro en épocas de paz. Las voces globales debaten hasta qué punto la democracia -cuya calidad venía en disminución desde hace más de una década- puede soportar tamañas concesiones sin quebrarse. Se observa una suerte de “recesión” democratica.

**Peligro en el alargue**

En América Latina, todo es peor. Con tan sólo 8% de la población mundial, da cuenta de alrededor de 30% de los fallecidos por COVID a nivel global. Epicentro de la pandemia, la región además padece “comorbilidades”, como pobreza o desigualdad, que profundizan y prolongan la crisis socioeconómica. Así, los estados de excepción -que por propia definición deben ser transitorios- amenazan con extenderse en el tiempo.

La emergencia pública en Argentina fue decretada en marzo del año pasado (Decreto 260/2020) y luego extendida hasta el 31 de diciembre de 2021 (Decreto 167/2021).

La evolución de la pandemia en nuestro país al día de la fecha -con cerca de 300.000 infectados activos, más de 5.000 fallecidos en las últimas dos semanas y tan sólo 3% de la población con la vacunación contra el COVID-19 completada- permite suponer que el  Poder Ejecutivo intentara extender el estado de excepción  más allá de los plazos establecidos.

Aún cuando la pandemia finalmente esté relativamente bajo control, es decir cuando hayamos alcanzado la hoy lejana “inmunidad de rebaño” con al menos 70% de la población vacunada-,  las secuelas socioeconómicas que ya estamos padeciendo se profundizarán. Estas demoras en la salida de la crisis -la sanitaria, primero, y la socioeconómica, durante y después- nos colocan frente al riesgo de propiciar un alargue peligroso de la emergencia pública.

**Dos errores de cálculo**

La Argentina ingresó a la pandemia con un retroceso relativo a otros países de la región, situación que arrastra desde hace décadas producto de recetas populistas de gobierno y del facilísimo económico. Sobre esta situación de desventaja inicial, el Gobierno cometió dos errores de cálculo que profundizaron las diferencias en detrimento nuestro: primero, subestimó la pandemia; luego sobrestimó la cuarentena.

Una semana antes del Caso cero de COVID-19 en Argentina -un empresario que regresaba de un viaje a Europa-, el entonces Ministro de Salud de la Nación había afirmado que el coronavirus representaba un “riesgo moderado”. Pocos días después de ese hito, cuando ya se confirmaban nuevos casos, el Ministro confesó que había pensado que el virus tardaría más tiempo en llegar a la Argentina.

Ese primer error de cálculo impidió llevar a cabo medidas más eficaces en los puntos de control de migraciones y desperdició tiempo valioso en el que se podría haber comprado reactivos para detectar el virus.

Al reaccionar, a mediados de marzo, el Gobierno decretó una cuarentena estricta, que -junto con herramientas clave de contención social, como fueron el Ingreso Familiar de Emergencia (IFE) y la Asistencia de Emergencia al Trabajo y la Producción-resultaron respuestas adecuadas durante un tiempo.

El segundo error de cálculo se fue haciendo palpable a lo largo de los meses, cuando el Gobierno cayó en la inercia quincenal de prolongar la cuarentena. La política para controlar la pandemia se centró casi exclusivamente en las restricciones a la movilidad y a la actividad.

El Oxford Covid-19 Government Response Tracker es un repositorio global de evidencia sobre las políticas que los diversos gobiernos llevan a cabo para abordar la emergencia sanitaria actual. Recolecta datos en 20 categorías de respuestas ante el coronavirus, incluyendo cuarentena, salud, economía y vacunación en 186 países. Luego agrupa estas políticas en una serie de indicadores, incluyendo el “indicador de rigor”, que registra el número y la intensidad de las restricciones en una escala de 1 a 100. Argentina está entre los países de mayor rigor aplicado, junto con Nicaragua, Burundi, Bielorrusia, Kiribati y Tanzanía.

Ya en abril de 2020 desde la Fundación Alem advertimos que había un “enamoramiento” por parte del Gobierno Nacional con la cuarentena, sin señales de un plan de salida. Propusimos entonces una apertura ordenada y progresiva de la cuarentena, que buscara un equilibrio óptimo entre los aspectos sanitario, económico, social e institucional.

Sin embargo, la actual Administración continuó con su enfoque reduccionista y promovió un debate absurdo: salud vs. economía. Más allá de la falsa antinomia, los resultados dan cuenta de que finalmente no tuvimos éxito en ninguna de las dos dimensiones.

 

**Ni una cosa ni la otra**

En una muestra de 123 países, la Argentina duplica el promedio mundial en la caída del PBI. Con respecto a otros países de América Latina sobre los que hay datos disponibles, sólo Ecuador y Perú están peor. La estrepitosa caída productiva, del 10% del PBI, se llevó puestos un millón de empleos y generó tres millones de nuevos pobres en tan solo un sólo año.

Ahora bien, cuando pasamos a la salud, la Argentina triplica el promedio mundial en cantidad de fallecidos por 100.000 habitantes. Sólo tres países de la región están peor: Brasil, México y Perú.

Argentina mal desempeño en vacunación y crecimiento durante pandemia Covid 

Información extraída de presentación "El Estado de la Pandemia", Fundación ALEM, abril 2021, [http://fundacionalem.org.ar/assets/uploads/documents/el-estado-de-la-pandemia.pdf](http://fundacionalem.org.ar/assets/uploads/documents/el-estado-de-la-pandemia.pdf "http://fundacionalem.org.ar/assets/uploads/documents/el-estado-de-la-pandemia.pdf")

Al evaluar simultáneamente las dos dimensiones, es decir, cuánto cayó nuestra economía y cuánta gente se ha muerto por la COVID, sólo Perú está peor. Todo esto con un confinamiento 40% más riguroso que en el resto de los países, según el índice desarrollado por la Universidad de Oxford.

Los datos se corroboran en otro estudio, el Ranking de Resiliencia al COVID que elabora Bloomberg, en el cual la Argentina se ubica como el segundo país que peor manejó la pandemia entre 53 naciones relevadas. El primero en el listado es Brasil.

Este análisis demuestra cuán equivocado estaba el planteo salud vs. economía. Ahora estamos frente a otra falsa antinomia, que también nos lleva por el rumbo equivocado: salud vs. educación. Al menos un millón de niños y niñas han quedado totalmente excluidos de la escolaridad -en cualquiera de sus formatos-, número que representa el 12% de la población matriculada del año pasado, sin considerar la enseñanza universitaria. Los efectos presentes y futuros, tanto en la salud como en las oportunidades de progreso de esa población marginada, serán muy negativos y acrecentarán la pobreza y la desigualdad en nuestro país. Por eso, al igual que para la salida de la cuarentena, es importante tener una hoja de ruta para la reapertura educativa y un plan para contrarrestar los efectos nocivos que este período está teniendo sobre toda una generación de argentinos que son nuestro futuro. 

**El agravante de la opacidad**

Hubo un gran ganador de la pandemia en Argentina: la arbitrariedad. De la mano de decisiones opacas, se han producido -además de los enormes desajustes socioeconómicos mencionados- una gran cantidad de desequilibrios institucionales. 

Poder Ciudadano tiene un observatorio de compras COVID-19 y ha analizado hasta la fecha los expedientes de 86 organismos públicos por un monto de alrededor de $25.000 millones. Demostró que el 37% no había sido publicado en el portal de compras públicas. La Procuraduría Nacional de Investigaciones Administrativas, por su parte, documentó que, en una muestra de 232 expedientes de compras en diversos organismos, más del 40% no estaba publicado ni en el Boletín Oficial ni en la Oficina Nacional de Contrataciones. 

Una investigación de organizaciones de la sociedad civil demostró que en Argentina el 90% de las compras de emergencia de las jurisdicciones provinciales se llevaron a cabo sin licitación. 

Esta opacidad puede abrir las puertas a irregularidades, como se vio en el Ministerio de Desarrollo Social, cuando al inicio de la pandemia se realizaron compras de alimentos por valores muy por encima de los precios minoristas, situación que terminó con la renuncia de más de una docena de funcionarios de esa cartera.

En conclusión, la pandemia justifica medidas extraordinarias pero bajo ningún punto de vista debe dar lugar a la ausencia de controles.

**Las dos acciones más importantes hoy**

Las vacunas contra la COVID-19, desarrolladas en tiempo récord pero de lenta distribución global, representan un elemento importante no sólo para frenar los contagios y evitar muertes, en lo inmediato, sino también para empezar a imaginar el regreso a cierta normalidad y el comienzo de la reconstrucción socioeconómica.

Sin embargo, nuevamente por errores de cálculo sobre la magnitud y el alcance de la pandemia, el gobierno argentino fue demasiado selectivo al momento de sentarse con los productores para negociar suficientes dosis para vacunar a toda la población. Con el tiempo, se fue diversificando, aunque con ciertas restricciones y limitaciones, hasta llegar a tener contratos firmados por tres marcas de vacunas con cinco laboratorios.

Si bien a diario aparecen dudas acerca de efectividad a largo plazo de las vacunas y además convivimos con la amenaza de nuevas cepas en segundas y terceras olas que parecen no tener fin, lo cierto es que el arma número uno que existe hoy en día para combatir al SARS-CoV-2 es la vacunación.

Por eso, las acciones más importantes que puede llevar a cabo en este momento la Administración Pública Nacional son dos: comprar la mayor cantidad de vacunas posible en el menor plazo, y distribuir esas vacunas de forma rápida a toda la población, priorizando a los grupos más necesitados.

Esas dos acciones están siendo analizadas por la Auditoría General de la Nación en dos auditorías en marcha que, a mi entender, representan no sólo las más importantes de nuestro Programa Anual de Auditorías 2021 sino que se destacan por su relevancia en términos históricos del máximo organismo de control público externo de nuestro país.

**Compra de vacunas**

En el marco de la emergencia sanitaria, el 6 de noviembre de 2020 fue publicada en el Boletín Oficial la ley 27.573 de “Vacunas destinadas a generar inmunidad adquirida contra el COVID-19” que declara de interés público la investigación, desarrollo, fabricación y adquisición de las vacunas destinadas a generar inmunidad adquirida contra la COVID-19.

Esa ley facultó al Poder Ejecutivo Nacional, a través del Ministerio de Salud de la Nación, a realizar los trámites para adquirir vacunas que se desarrollen para enfrentar la enfermedad. Además estableció, entre otros aspectos, la autorización para incluir en los contratos de compra de: (i) la prórroga de jurisdicción a favor de los tribunales arbitrales y judiciales extranjeros, con posibilidad de renunciar a la defensa de inmunidad soberana; (ii) cláusulas de indemnidad patrimonial en favor de quienes participen de la investigación, desarrollo, fabricación, provisión y suministro de las vacunas; (iii) cláusulas de confidencialidad acordes al mercado internacional y a las leyes de Acceso a la Información Pública (27.275) y de los Derechos del Paciente (26.529); y (iv) otras cláusulas acordes al mercado internacional de la vacuna.

En su artículo 10 establece expresamente la remisión de los contratos a la Auditoría General de la Nación y a las autoridades de la Comisión de Acción Social y Salud Pública de la Honorable Cámara de Diputados de la Nación y de la Comisión de Salud del Honorable Senado de la Nación, con los recaudos necesarios para respetar la confidencialidad de los contratos.

La mencionada ley también exime del pago de derechos de importación y de todo otro impuesto, gravamen, contribución, tasa o arancel aduanero o portuario, de cualquier naturaleza u origen, para asegurar las vacunas contra el COVID 19.

A mi entender, el propósito de la legislación fue otorgarle al Poder Ejecutivo, a través del Ministerio de Salud, un instrumento legal específico que le permita un margen de negociación de contratos para atender, no sólo los requerimientos de la industria farmacéutica, sino también la necesidad de adquirir con celeridad y eficiencia la mayor cantidad de vacunas posibles en un mercado muy complejo, para intentar garantizar el acceso a vacunas en tiempo y forma por la población argentina.

Teniendo en cuenta la situación de excepcionalidad, el legislador previó la intervención de la AGN con el objetivo de dotar al proceso de compra de vacunas de un necesario y oportuno control externo.

En el documento Ejes temáticos propuestos por la Presidencia de la Auditoría General de la Nación enumero preguntas clave que he propuesto nos hagamos al abordar esta auditoría que se nos ha encomendado por ley.

Mientras la auditoría esta en marcha y hasta que tengamos sus resultados, los siguientes datos son de dominio público: la Argentina firmó, a la fecha, contratos por las vacunas Sputnik, Sinopharm, AstraZeneca, Covishield y Covax. Esos cinco contratos se refieren solo a tres tipos de vacunas, debido a que los últimos tres corresponden a una misma preparación biológica. En total se trata de 49 millones de dosis por un monto de US$ 380 millones y un precio promedio ponderado de US$ 7,8 por dosis. Hasta ahí, son sólo datos. Lo que resulta llamativo es que, habiendo desembolsado el año pasado mas de la mitad de los fondos para adquirir esas vacunas, sólo han llegado al país el 20% de las dosis.

**Distribución de vacunas**

El 30 de diciembre del año pasado, mediante la Resolución Nº 2.883/2020 del Ministerio de Salud, se aprobó el “Plan Estratégico para la Vacunación contra la COVID-19 en la República Argentina”, destacando entre sus considerandos que la Organización Mundial de la Salud recomienda la vacunación contra la COVID-19 como una herramienta de prevención primaria fundamental para limitar las consecuencias sanitarias y económicas devenidas de la pandemia y que esta campaña de vacunación constituye una estrategia de salud pública nacional, prioritaria, equitativa, solidaria y beneficiosa para el bienestar y la salud, tanto individual como colectiva de los ciudadanos. En este sentido, a fin de alcanzar un alto nivel de cobertura de vacunación en tiempo oportuno, se requiere de acciones articuladas, colaborativas y multisectoriales en todos los niveles de gobierno, junto con las organizaciones sociales.

El propósito y los objetivos de este Plan Estratégico son:

* Disminuir la morbilidad-mortalidad y el impacto socio-económico ocasionados por COVID-19.
* Vacunar al 100% de la población objetivo en forma escalonada y progresiva, de acuerdo con la priorización de riesgo y la disponibilidad gradual y creciente del recurso.

La resolución además detalla una serie de objetivos específicos, entre los que se destacan:

Establecer un orden de prioridad en la población objetivo a vacunar, teniendo en cuenta criterios científicos y éticos, en una situación de disponibilidad progresiva de dosis de vacunas; considerando la protección de los grupos con mayor riesgo, junto a grupos de población estratégicos necesarios para asegurar el desarrollo de actividades prioritarias.

Establecer ejes prioritarios esenciales para evaluar las metas de vacunación: tasas de cobertura, monitoreo continuo de seguridad y efectividad de las vacunas, en correlación con el impacto epidemiológico que produzca la vacunación sobre COVID-19.

Atendiendo la importancia del plan estratégico de vacunación como política pública de absoluta prioridad, el Colegio de Auditores Generales acordó por unanimidad agregar un proyecto de auditoría al Programa de Acción Anual luego de que ya hubiera sido aprobado y puesto en marcha.

A mi entender, hay cuatro dimensiones clave a relevar, según detallo en el documento Ejes temáticos propuestos por la Presidencia de la Auditoría General de la Nación, que sintetizo a continuación:

* GOBERNANZA (interacciones entre estructuras, procesos y cumplimientos de la normativa vigente)
* EFICACIA (cumplimiento de metas)
* EFICIENCIA (verificar que los tiempos e insumos sean razonables)
* EQUIDAD (Proporcionalidad-Federalismo) 

Los temas de equidad son particularmente sensibles en un contexto de creciente descontento regional por las enormes diferencias entre unos y otros que existen en América Latina y que la pandemia ha profundizado. En la Argentina, como se puede apreciar en este estudio que presentamos en la Fundación Alem el pasado abril, hay grandes asimetrías entre las provincias, desde el avance del proceso de vacunación y las camas de internación disponibles hasta los recursos económicos y la mayor o menor dependencia del sistema público de salud. Por otra parte, fue por un tema de equidad -o, más precisamente, de inequidad- que se precipitó la renuncia del primer Ministro de Salud que tuvo la actual gestión. El escándalo de los vacunatorios VIP demostró que la población, en su justo derecho, no está dispuesta a tolerar el tratamiento preferencial. 

**Aprender a aprender**

La Argentina no está sola frente a los problemas y desafíos que presenta la pandemia. Muchos de los temas acá mencionados están en discusión en otros países. Nos movemos en un terreno desconocido y es entendible un margen para el error y el desconcierto.

Sin embargo, no todos los sistemas políticos reaccionan con la misma eficacia. Los sistemas populistas son particularmente ineficaces, entendiendo al populismo según la definición de Felipe González que es la de aquellos estilos de gobierno que, ante problemas complejos, siempre propician soluciones sencillas y siempre señalan culpables externos.

El filósofo español Daniel Innerarity señala que el déficit del populismo se debe a tres razones: 1) los modos populistas de gobernar desconfían del aprendizaje dado por las mejores practicas internacionales (pasó en la Argentina); 2) los modos populistas de gobernar descreen del saber experto (pasó en la Argentina); y 3) los modos populistas de gobernar desconfían de los gobiernos cooperativos en los que distintos actores políticos pueden llegar a consensos – por el contrario creen en la posibilidad de imponer el orden social (pasó en la Argentina). 

Hay una oportunidad para recomponer la situación argentina en este “tiempo que nos tocó vivir”, como le gusta decir al Presidente. Es la oportunidad de que los argentinos nos reconciliemos con el conocimiento y dejemos de lado la especulación y la confrontación. La pandemia puede hacernos aprender a aprender. En tan sencillo acto encontraremos las soluciones y el camino para minimizar el impacto de la pandemia y reconstruir un mejor futuro.

_*Jesús Rodríguez es Presidente de la Auditoría General de la Nación, el máximo órgano de control externo de la República Argentina._