---
layout: analisis
category: analisis
date: 2021-02-01T06:11:41-03:00
author: Marisa Lemos
title: DESCANSO ETERNO, ¿BIEN CUIDADO?
thumbnail: https://assets.3dnoticias.com.ar/marisa.jpeg

---
Demás está decir que el cementerio es nuestra última morada y que es el sitio de descanso eterno de miles de seres que pasaron por esta vida.

Hace unas semanas se presentaron trabajos de reacondicionamiento en el Cementerio Municipal de Santa Fe de la  Vera  Cruz donde en la misma se demolieron tres secciones completas.

Allí ahora hay un espacio verde y una galería de circulación. En el transcurso de este año se continuará con la renovación de más secciones que hoy están en riesgo. También se colocarán 12 cámaras de videovigilancia y se pondrá nueva iluminación.

Aunque estas obras recién comienzan, son mantenimientos que tendría que haberse realizado hace muchísimo tiempo. Hay sitios que están en total abandono hace años, no solamente la infraestructura está deteriorada sino que también los pastos están súper altos, basura por donde mires, escombros, animales muertos estructuras sin terminar, mausoleos destruidos, tumbas abiertas y otras cosas que sabemos que no se utilizan para nada bueno... y NADIE SE HACE CARGO.

Es de público conocimiento que la Necrópolis es vandalizada constantemente. Los asaltos no le suceden solo a los visitantes sino también a los mausoleos donde se sustraen placas de bronce o de otro tipo de material que tenga valor. Pero este no es ningún motivo para que se  encuentre en ese estado.

Pero, ¿qué pretendo con esto? Mi intención es llegar llamar la atención del municipio local para tratar de aligerar los trámites por así decirlo. Ya que todos miran a un costado, el responsable y el que tendría que tener un poquito más de seguimientos de estos factores es nuestro querido intendente Emilio Jatón, ya que según los medios locales garantizó seguridad en las inmediaciones del cementerio aunque  su responsabilidad es velar por el bienestar de los ciudadanos de nuestra amada Santa Fe y creo que también debería preocuparse por los que ya no están entre nosotros, pero que de cierto modo dejaron su marca registrada y su legado entre sus allegados.

Por eso espero que estas palabras le sirvan de algo a los responsables y que empiecen a cumplir con su deber.