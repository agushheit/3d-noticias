---
layout: analisis
category: analisis
date: 2021-02-24T06:56:09-03:00
author: " Claudio Savoia para Clarín"
title: No fue un error, presidente, hubo varios delitos graves y es su obligación
  esclarecerlos
thumbnail: https://assets.3dnoticias.com.ar/ginés.webp

---
El arranque de furia con el que Alberto Fernández borró este martes desde México la aplaudida reacción inicial ante el escándalo del vacunatorio vip lo llevó a cometer varios errores conceptuales, entre los cuales uno de los más severos fue minimizar la interpretación del caso como una evidente colección de delitos.

Quedará al fiscal Eduardo Taiano seleccionar cuáles son los que a su juicio calzan mejor, y también a su colega Sergio Rodríguez, titular de la Procuraduría de Investigaciones Administrativas.

Pero un primer pantallazo evidencia el incumplimiento de los deberes de todos los funcionarios públicos que debían velar por la justa distribución de la vacuna Sputnik V según el orden de prelación que el gobierno estableció e hizo público en documentos oficiales: cualquier violación a ese orden, sea cual fuere el argumento de trascendencia con que se quiera barnizar, podrá ser interpretado como un delito federal.

Tampoco suena exagerado atribuir un abuso de autoridad a las decenas de funcionarios que lograron vacunarse sólo por la prerrogativa que les dieron sus cargos, libres de cualquier control molesto.

Las vacunas son un bien público, valioso a punto tan de que pueden alejar de la muerte a muchas personas, y escaso en Argentina por razones de las cuales el gobierno tampoco es ajeno. ¿Alterar el plan de vacunación para inmunizar a funcionarios, legisladores, secretarios, periodistas, bailarinas, amigos y entenados no es malversar aquellos recursos? En estos días, la mayoría de la cátedra en derecho penal se inclina a considerar que sí. Veremos qué dice la jueza María Eugenia Capuchetti, a cargo de las quince denuncias por el escándalo unificadas en su despacho.

Sigamos con la pedagogía. Todos los delitos enumerados precedentemente -junto con otros que por ahora no aparen en este caso pero podrían hacerlo, como cohecho para ser vacunado- corresponden al capítulo de delitos contra al administración pública, mejor conocidos como delitos de corrupción.

No es un "error" -palabrita que quiso imponerse con denuedo desde el domingo por la tarde- ni una equivocación que le puede pasar a cualquiera. Tampoco se trata de un hecho aislado, o un breve archipiélago de hechos aislados. En los cuatro días que lleva esta erupción hedionda se conocieron detalles de combis que iban y volvían al y desde el hospital Posadas para llevar y traer cajas con dosis de vacunas o lotes de vacunados vip, equipos médicos del hospital que se trasladaban al ministerio de Salud para atender a los privilegiados e incluso excursiones a domicilio para aplicar el esperado medicamento.

Esa mecanismo perfectamente engrasado y reiterado en el tiempo durante este verano bien puede describirse como una asociación ilícita, el más grave de los delitos por corrupción aunque también el más difícil de probar. Desde luego, por ahora sólo mantenemos la mira en el circuito de la felicidad digitado desde el ministerio de Salud, y no en otros tanto o más groseros que al menos hasta la semana pasada funcionaron en varios hospitales importantes del gran Buenos Aires, dirigidos por funcionarios provinciales, ejecutivos de la Cámpora o por los mismos intendentes locales.

Indicios sobran. Testimonios también. Fotos publicadas en las redes sociales hay cientos. Habrá que ver hasta dónde llega el coraje de los magistrados. Y también la presión pública para empujar esas agallas.

La elección de la vacuna contra el Covid como el santo grial del 2021, y su manejo secreto, muchas veces turbio por parte del gobierno, son ahora otro peso incómodo para caminar cuesta arriba ante la indignación pública. Los acuerdos con Rusia son oscuros. La caída del contrato con Pfizer también. El cronograma de llegada de dosis es desconocido, aún siendo piadoso con las pilas de promesas incumplidas en los últimos meses.

Los vuelos de Aerolíneas Argentinas a Moscú no los controla nadie, y nadie sabe tampoco cuántas dosis traen en verdad, más allá de las declaraciones públicas. El reparto de esas vacunas entre las provincias y municipios tampoco es transparente, y la aparición de supuestos "remanentes" rápidamente utilizados por los amigos del poder suenan a mal chiste mientras miles de médicos y enfermeras todavía no lograron vacunarse. A esa secuencia de misterios, alimentada por la ambición de salvar una crisis gravísima y el deseo miserable de capitalizar un posible alivio, sólo le faltaba un fósforo que incendiara la pradera. Lo encendió Horacio Verbitsky.