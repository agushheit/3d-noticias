---
layout: analisis
category: analisis
date: 2021-01-30T09:34:53Z
author: José Villagrán
title: Un aumento de boleto superior a lo aconsejado por el Órgano de Control y la
  inflación
thumbnail: https://assets.3dnoticias.com.ar/jose_villagran.jpeg

---
Así se desprende del dictamen elaborado por el Órgano de Control. La suba del boleto va desde $28,90 a $42,35; es decir, un 46,5% . Lo recomendado era un 34,35%. La inflación de un año fue del 44%. 

En las calles ya se sabía que el precio de colectivos iba a aumentar. Desde hace unos quince días se venía escuchando las voces de los empresarios del sector con el mensaje que “el precio debe rondar entre $50 y $55”. Desde hace quince días los santafesinos ya sabíamos que ese aumento se iba a terminar dando, pero faltaba que la municipalidad establezca el monto de esa suba.

Finalmente, esta semana, el ejecutivo municipal de la ciudad de Santa Fe hizo oficial el nuevo valor del boleto de transporte público de pasajeros: de casi $29 pasará a valer un poco más de $42 a partir del lunes 08 de febrero de este año.

El último incremento se había dado el 29 de septiembre del 2019, donde se estableció la tarifa que hasta ahora rige en el servicio. Es decir, efectivamente como comunicó el municipio, el valor estuvo fijo durante dieciséis meses. Pero además, en febrero del 2020 el intendente Emilio Jatón firmó junto al secretario de Transporte de la provincia, Osvaldo Miatello, un acuerdo donde congelaba ese precio hasta fines de abril. Algo que siguió sucediendo hasta llegar a este nuevo incremento.

Para poder hacerlo, el departamento ejecutivo tuvo que dar cumplimiento al artículo 16 de la ordenanza Nº 11.580 sancionada por el Concejo Municipal, que establece que el Órgano de Control debe dictaminar previamente a la decisión del municipio cuáles son las redeterminaciones de las tarifas.

Y es aquí donde el valor incrementado empieza a hacer ruido.

**¿Qué dijo el Órgano de Control en su dictamen?**

Antes de comenzar a responder esa pregunta, es importante hacer una aclaración técnica sobre cómo se establece el precio del boleto. Una explicación sencilla pero técnicamente compleja.

La norma mencionada con anterioridad establece, que para que haya aumento en el precio del mismo, “la variación total de los costos debe superar en más de un 5% el último valor redeterminado”. Este porcentaje se obtiene haciendo un cálculo matemático que se transcribe a continuación.

![](https://assets.3dnoticias.com.ar/1.jpeg)

Es una cuestión muy técnica pero imprescindible para que los aumentos en el transporte de colectivo sean autorizados. Y en este caso, el propio organismo da el visto bueno porque este requisito se cumple.

![](https://assets.3dnoticias.com.ar/2.jpeg)

Pero si se entra a analizar lo que argumenta el dictamen, hay contradicción entre lo que sugiere y lo que termina sucediendo en la realidad.

El organismo afirma que, para poder hacer el estudio de costos, tomó como referencia los subsidios enviados desde la Nación, las escalas salariales del Sindicato Único Tranviario Automotor, lo establecido por la Comisión Nacional de Regulación del Transporte, facturas de combustibles y la variación de los precios medidos tanto por el INDEC como por el IPEC.

El tiempo que toma como marco va desde Septiembre del 2019 (fecha del último incremento) hasta Octubre de 2020, ya que es el mes más próximo a la fecha donde se pueden encontrar datos oficiales estables. Allí concluyen que la variación del índice de Precios al Consumidor (IPC) termina siendo de un 44% para ambos organismos, tanto el nacional como el provincial. Por lo que el recálculo de la estructura de costos “arroja un valor de 34,35 %”.

![](https://assets.3dnoticias.com.ar/3.jpeg)

Es decir, llevar el precio del boleto a $42,35 representa un incremento del 46,5%. Esto es un 2,5% más que la inflación estimada y un 12,15% por encima de lo “recomendado”. La pregunta que sigue es la siguiente. **Si el estudio de la polinómica que establece la ordenanza da como suma un 34,35% ¿por qué el ejecutivo lo subió un 46,5%?** Pero además el otro interrogante que surge es ¿qué trabajador tuvo un incremento salarial en el último año superior al 46% 

La votación de los tres miembros del órgano fue dividida. Quien se opuso al aumento fue el contador público Josè Roura, quien manifestó que no se encuentran dadas las condiciones para dictaminar a favor de una readecuación de la tarifa vigente.

“Durante el año en curso (y en el contexto de la emergencia sanitaria por el Covid 19), se produjo una baja muy significativa de pasajeros. Ello provocó el ajuste a las unidades en circulación, y la frecuencia del servicio excediendo los máximos establecidos en las ordenanzas pertinentes. A su vez, las declaraciones públicas de miembros del ejecutivo municipal, sobre la creación de un nuevo sistema de transporte, con nuevos recorridos y frecuencias, genera más incertidumbre a la hora de establecer cuál es el monto que deben pagar los usuarios por el servicio. Por tal razón, no considero oportuno el procedimiento para la readecuación tarifaria.”, esgrimió.

**¿Que dice el municipio a todo esto?**

Quien salió a explicar este aumento fue el subsecretario de Movilidad y Transporte de la Municipalidad, Lucas Crivelli. Para el funcionario “en todas las variaciones de precios, los resultados terminaban siendo menores a los aumentos que finalmente se autorizaban” y que “se tomó la variación de los precios de los últimos dos años y no de uno como lo hizo el órgano de control”. Además informó una reducción importante del 60%. Pasaron de 150 mil o 160 mil pasajeros a 30 mil en abril, mayo y lograron recuperar un poco a 40 mil en el mes de diciembre.

Los concejales de la ciudad también dieron su punto de vista. Para el presidente del Concejo, Leandro González, explicó que “esa diferencia de 12% entre lo que establece el órgano y el aumento definitivo radica en que en 2019 no se terminó aplicando en ese aumento”.

Para los ediles de Juntos por el Cambio Carlos Pereira e Inés Larriera “en la situación de crisis, esta excesiva suba sólo ayudará a incrementar la crisis del sector, porque más gente buscará otras alternativas al transporte público” y además expresaron que “llama la atención también que el incremento se dé muy por arriba de lo recomendado por el Órgano de Control del Transporte, que realizó un recálculo de la estructura de costos que arrojó un valor de 34,35 %, tal como se lo informó en su dictamen al Ejecutivo”. 

Por su parte, Luciana Ceresola, consideró que el incremento es "bestial" y que muchos "trabajadores verán agravada su movilidad".

Por último, la concejala del Partido Justicialista Jorgelina Mudallel expresó que “a un servicio que no es eficiente y que no le resuelve el traslado en buenas condiciones a la gente, hay que sumarle este incremento.”