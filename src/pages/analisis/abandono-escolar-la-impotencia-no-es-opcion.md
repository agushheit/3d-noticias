---
layout: analisis
category: analisis
date: 2022-02-16T12:05:25-03:00
author: Juan Martin
title: 'Abandono escolar: la impotencia no es opción'
thumbnail: https://assets.3dnoticias.com.ar/juanchi.jpg

---
Estamos a días del inicio del ciclo lectivo en todo el país. Algunas jurisdicciones están abocadas a recuperar contenidos con niños, niñas y adolescentes que, afortunadamente y aún con muchas dificultades, no se han desvinculado totalmente del sistema educativo.

A su vez, es incierto el futuro escolar de miles de chicos que han dejado de cursar durante la pandemia, un grave problema que para revertir primero tenemos que reconocer. **Porque nunca es tarde y cada historia de vida detrás del abandono escolar, cuenta.** Pero para que no quede en un eslogan, en una consigna vacía, hacen falta determinación y gestión. Y la premisa básica para comenzar es justamente, contar, saber cuántos estudiantes han abandonado efectivamente la escuela.

Parece básico. Y lo es. Pero lamentablemente en el mes de febrero no sabemos aún con exactitud cuál es el número. Carecemos de estadísticas oficiales.

No llegamos hasta acá por casualidad. Era sabido que los cierres de 2020 y 2021 acarrearían, entre otras consecuencias, el abandono prematuro de miles de estudiantes. La potencial catástrofe educativa fue advertida por actores de la sociedad civil, organismos internacionales y las organizaciones de padres y docentes organizados. Por eso planteamos insistentemente la declaración de emergencia y la esencialidad del sistema educativo en todos los niveles y modalidades, pero nada de ello modificó las decisiones del gobierno nacional.

Es en este contexto que el ministro de Educación afirmó al pasar en declaraciones periodísticas que son cerca de 500.000 los chicos que a fines del ciclo lectivo 2021 seguían desvinculados del sistema. Pero es solo una estimación basada en datos de mediados de 2020 y que entre otras deficiencias, no contabiliza la educación inicial.

**Para que quede claro: tener información no sólo nos permitiría ir a buscar a quienes se fueron, uno a uno. También evitaría que muchos niños y niñas en situación de riesgo no engrosen las filas del abandono.** Porque podemos conocer mejor sus causas y motivos, y actuar en consecuencia. Prevenir es clave, pero solo podremos hacerlo de modo eficiente con información de calidad.

Desconocemos también qué datos maneja el Sistema Integral de Información Digital Educativa, y qué resultados han tenido programas de revinculación como “Acompañar. Puentes de Igualdad” y “Volvé a la Escuela”. Para este último se anunció una inversión de 5000 millones de pesos. Pero ¿cómo se destinan los fondos?, ¿a partir de qué base de datos y/o registros se busca casa por casa a quienes han abandonado sus estudios?

El Ejecutivo convocó a sesiones extraordinarias en el Congreso. Como se sabe, estas sesiones sirven para tratar temas que no pueden esperar a las ordinarias, y que marcan prioridades y urgencias de una gestión. En el temario propuesto no hay ningún proyecto vinculado al abandono escolar en la pandemia. Ninguno.

Días atrás, junto a diputados y diputadas de Juntos por el Cambio, presentamos un proyecto requiriendo información al Poder Ejecutivo y exigiendo acción urgente para que efectivamente, no sea demasiado tarde. No hemos recibido respuestas aún, pese a la urgencia que implica el comienzo inminente del ciclo lectivo. Un comienzo que, por cierto, no puede dilatarse por razón alguna. Porque no hay más lugar para excusas.

El daño está hecho. Al que perdió oportunidades y sufre en carne propia haber quedado al costado del camino, no le brinda respuestas la indignación impostada. Al contrario, lleva la discusión pública a una degradación intolerable y oculta el problema debajo de la alfombra, mientras lo que se espera de nosotros son resultados. **Acciones para lograr revincularse al sistema.**

En pleno siglo XXI, atravesando la cuarta revolución industrial, la información y el conocimiento implican poder. Y no contar con información, es básicamente, declararnos impotentes.

Ésta realidad nos duele y aleja del camino del progreso. Pero también nos interpela: la Argentina tiene un futuro de enormes oportunidades si se abraza a la revolución del conocimiento. **Podemos debatir cómo hacerlo, pero resignarnos a la impotencia no es opción.**

**Diputado Nacional por Santa Fe (UCR), Juntos por el Cambio**