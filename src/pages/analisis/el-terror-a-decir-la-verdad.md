---
layout: analisis
category: analisis
date: 2021-02-04T03:50:05-03:00
author: Lucila Lehmann - Diputada Nacional por Santa Fe - Coalición Cívica
title: El terror a decir la verdad
thumbnail: https://assets.3dnoticias.com.ar/educacion1.jpeg

---
Como dice Andrés Malamud: “La pandemia no fomentó autoritarismos, los hizo visibles”. Y una de sus armas más efectivas ha sido la de infundir miedo, particularmente en el empleado público, inhibiéndolos a expresar su descontento con algunas de las medidas del gobierno.

En estos días, el sector más hostigado es el colectivo docente, donde el 90% de los entrevistados se negaron a contestar una encuesta, aunque era anónima, sobre el regreso a las clases presenciales.

Sin lugar a dudas, la tarea de “psicopatear”, llevada adelante por sindicatos adictos al gobierno, ha dado resultado.

Los maestros con vocación de enseñar temen perder su puesto y con ello su magro salario. Se sienten vigilados por el delegado que ejerce un poder de comisario político como en los mejores sóviets rusos.

La mayoría de los maestros no renunciaron a los valores básicos de la convivencia civilizada, pero están amordazados por la incertidumbre de su futuro a raíz de la pandemia, pero también por la fuerte coerción para no regresar a dar clases.

A esos maestros con fuerte compromiso por enseñar, los padres deben protegerlos, pues la ideologización de lo que se enseña en la escuela pública, no solo constituye un delito, sino también formará a niños y jóvenes con resentimiento y estimulados para odiar.

Al día de hoy, hay muchas afirmaciones y pocas respuestas. ¿Se presentaron los protocolos y metodologías de trabajo para las actividades presenciales de docentes? ¿Se registró la cantidad de deserción de alumnos durante el 2020?

Unos días atrás, la Sociedad Argentina de Pediatría advirtió que es “imprescindible la vuelta a las escuelas en la modalidad presencial” no solo para la adquisición de conocimientos sino también para el fortalecimiento de aspectos emocionales y sociales, cuidados nutricionales, de salud y la realización de la actividad física. ¿Esto se tiene en cuenta a la hora de sentarse a negociar y discutir? O se toma en cuenta la lamentable declaración del Ministro Trotta quien opinó que “es una discusión falaz decir que la educación debe ser un servicio esencial”.

Seguir manteniendo las aulas cerradas significa seguir coartando el futuro de nuestros niños y precisamente, la reconstrucción de nuestro país empieza por la educación. Como dice Karl Menninger, un reconocido psiquiatra estadounidense: “Lo que se les dé a los niños, los niños darán a la sociedad”.