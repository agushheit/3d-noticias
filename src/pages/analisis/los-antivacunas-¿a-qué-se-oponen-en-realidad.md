---
layout: analisis
author: Por Sofía Gioja
category: analisis
title: LOS ANTIVACUNAS ¿A qué se oponen en realidad?
date: 2020-11-10T15:20:35.948+00:00
thumbnail: https://assets.3dnoticias.com.ar/Gioja.webp

---
Estamos inmersos en una realidad en donde hay gente que piensa (aún) que la tierra es plana, la gravedad no existe y la vuelta del comunismo es inminente. Dentro de este subsistema, nos encontramos con los  antivacunas. Quienes pregonan este movimiento, tienen todo un postulado basado en teorías conspiranoicas, secretos gubernamentales y estudios de dudosa comprobación empírica.

Lo cierto es que, a diferencia del resto, los antivacunas pueden convertirse potencialmente en un verdadero peligro para la salud.

Susana Pedroso, investigadora del Conicet, afirma que lo más preocupante de las pseudociencias (entendidas como falso conocimiento) es que están generando un protagonismo enorme en la vida pública y sostiene que, si los gobiernos e instituciones empiezan a regirse por estas falsas creencias, será realmente grave. “Estamos empezando a hablar de temas más complicados y para nada inocuos”.

La comunidad científica, en general, está de acuerdo en que las vacunas son probablemente, el mayor avance contra las enfermedades en la historia de la humanidad.

La Organización Mundial de la Salud (OMS), estima que se evitan entre 2 y 3 millones de muertes al año. La poliomielitis, por ejemplo, está cerca de ser erradicada, gracias a la inmunización - que también ha logrado rebajar la mortalidad del sarampión en un 74% en una sola década (2000-2010). Lo mismo sucedió con la viruela que recientemente (debido al abandono de la vacunación) tuvo en rebrote en algunos países de Europa, donde ya se creía extinta. Lo mismo sucedió en EEUU con el sarampión. Entonces ¿Por qué hay gente que aún no se vacuna?

### **Efecto Wakefield, Internet y fake news**

Andrew Wakefield, fue un médico británico que saltó a la fama tras publicar un estudio, en 1998, en donde se aseguraba que la vacuna de rubéola y sarampión causaban autismo.

Según sus detractores, el objetivo de Wakefield, era hacerse millonario con vacunas alternativas. El estudio de Wakefield y su tesis, que afirmaba que la vacuna triple vírica podía causar autismo, condujo a un descenso en los índices de vacunación en Estados Unidos, Reino Unido e Irlanda, y al consecuente aumento de los casos sarampión y paperas, provocando casos graves y fatales. Sus continuas advertencias en contra de la vacunación han contribuido a un clima de desconfianza hacia todas las vacunas y a la reaparición de otras enfermedades que se creían controladas. Así es como, el estudio, a pesar de resultar fraudulento, disparó el movimiento antivacunas en todo el mundo.

En 2019, en Estados Unidos, un adolescente fue noticia por desafiar a su madre antivacunas acusándola de basar sus teorías en lo que leía en Facebook.

Los antivacunas inundan Internet con falacias y mitos que exageran los efectos secundarios. Meten miedo en nombre de “lo natural” y en detrimento de “lo químico”,  y esbozan teorías conspirativas de las empresas farmacéuticas y los gobiernos.

Quienes consumen este tipo de información están totalmente convencidos de que las vacunas son negativas y ponen en peligro sus hijos y seres queridos. A otros, simplemente les llega “de oído” que las vacunas esconden algo malo y deciden evitar el supuesto mal momento aprovechando la inmunidad colectiva, ya que la mayoría de la población si esta vacunada.

La Doctora e investigadora Romina Libster afirma al respecto que no solo nos vacunamos para protegernos a nosotros mismos sino también al otro: “Una comunidad en donde nadie fue vacunado y nadie tuvo sarampión, por ejemplo, es un blanco fácil para la enfermedad”.También explica que las personas vacunadas actúan como un escudo y protegen indirectamente a aquellos que no pueden vacunarse. Entonces, si la inmunización individual se rompe, el escudo también se rompe y cualquier enfermedad se convierte en brote.

### **Otra “predicción” de los Simpsons**

En el capítulo 15 de la temporada 21, llamada _La bandera de Bart,_ emitido en el 2004, sucedió algo que dejó a los antivacunas con el cuchillo entre los dientes. En este episodio, Bart es perseguido por toda la cuidad de Springfield para ser vacunado, cuando finalmente es sometido e inoculado, (contra su voluntad; como sucede con cualquier niño), una reacción alérgica a la vacuna lo deja temporalmente sordo. El médico, luego de revisarlo, les entrega a sus padres un folleto informativo que reza en inglés “Pinchazo mortal; guía para padres en caso de que algo salga mal” mientras les dice que “es un buen momento para hablar sobre efectos colaterales o secundarios”

Libster sostiene que hay que ser muy claros y trasparentes con las vacunas cuando se habla con los padres, en este caso. Pero vale para todos. Explicarles lo que se ha logrado a lo largo de los años. Gracias a esto, las nuevas generaciones pueden gozar de un país libre de polio y sarampión. Agrega, además, que muchos médicos, a raíz de los nuevos brotes de sarampión, tuvieron que volver a entrenarse y aprender cómo tratar un caso, porque nunca habían visto uno.

### **El nuevo Coronavirus**

Los coronavirus son una extensa familia de virus que pueden causar enfermedades tanto en animales como en humanos. En los humanos, se sabe que varios coronavirus causan infecciones respiratorias que pueden ir desde el resfriado común hasta enfermedades más graves como el síndrome respiratorio de Oriente Medio (MERS) y el síndrome respiratorio agudo severo (SRAS). El coronavirus que se ha descubierto más recientemente causa la enfermedad por coronavirus COVID-19.

El COVID‑19 es la enfermedad infecciosa causada por el coronavirus que se ha descubierto más recientemente. Tanto este nuevo virus como la enfermedad que provoca, eran desconocidos antes de que estallara el brote en Wuhan (China) en diciembre de 2019. Actualmente el COVID‑19 es una pandemia que afecta a muchos países de todo el mundo.

Aunque algunas soluciones de la medicina occidental o remedios caseros pueden funcionar como paliativos y aliviar los síntomas leves de la COVID-19, hasta ahora ningún medicamento ha demostrado prevenir o curar esta enfermedad. La Organización Mundial de la Salud (OMS) recomienda usar tapaboca, lavarse las manos frecuentemente y desinfectar con alcohol, como únicos recursos para prevenir el COVID-19. Sin embargo, hay varios ensayos clínicos en marcha, tanto de medicamentos occidentales como tradicionales. La OMS también está coordinando la labor de desarrollo de vacunas y medicamentos para prevenir y tratar el coronavirus y seguirá proporcionando información actualizada a medida que se disponga de los resultados de las investigaciones.

### **La Sputnik V**

El Gobierno Argentino anunció la compra anticipada de la vacuna Sputnik-V (vacuna rusa), aparentemente similar a la vacuna de la Universidad de Oxford y AstraZeneca. Los responsables de la vacuna de Oxford adelantaron que podrían proveer dosis masivamente recién para abril de 2021; con la vacuna rusa se habla de 10 millones de dosis en diciembre de 2020. En ambos casos, nada avanzaría sin la debida aprobación del ANMAT, como debe ser.

Si bien la vacuna fue registrada oficialmente por el gobierno ruso en agosto último, antes de que se iniciaran los ensayos de la fase III, algunos especialistas (Enrico Bucci, Luke O’Neill y Saad Shakir) han puesto en duda los resultados de los ensayos de la vacuna y advierten falta de información sobre los procesos científicos. Hasta señalaron que los investigadores probaron la vacuna en sí mismos, lo que representaría una falta de ética grave.

El sitio Chequeado indicó que, además, estos especialistas advirtieron que la vacuna no fue probada en grandes grupos de personas, de acuerdo con los estándares internacionales, y solo hay registro de la prueba con 76 voluntarios. Esto fue desmentido categóricamente por la periodista rusa Inna Afinogenova (quien también fue voluntaria para vacunarse): “Somos 40 mil voluntarios de la tercera fase, señor. Y usted está mintiendo. Siga mi canal de Telegram, cuento mi experiencia allí”, dijo en respuesta al Diputado Nacional Luis Petri, en su Twitter.

El 4 de septiembre, se publicaron en _The Lancet_ los resultados de fase I y II de la vacuna rusa que mostraron que el 100% de los 76 participantes desarrollaron anticuerpos contra el nuevo coronavirus y que no hubo efectos secundarios graves. Tras la publicación, un grupo de investigadores escribieron una carta abierta donde advirtieron acerca la aparición de valores duplicados en el estudio y pidieron acceso a los datos subyacentes de la investigación de la vacuna.

El 21 de septiembre, los autores del estudio ruso respondieron a las críticas con otro artículo donde enfatizaron que todos los datos presentados, que se obtuvieron durante los experimentos, fueron revisados ​​2 veces y que, en grupos pequeños de sujetos de prueba, los resultados podrían considerarse idénticos.

Contrariamente, Bucci sentenció al respecto, en el _Chemistry World_:"Es como entrar en una habitación con nueve personas y sumar sus edades y encontrar que ese resultado equivale al peso del total de esas personas".

Lo que queda claro, afirma Pablo Esteban (para Página 12), es que hasta que la etapa incompleta no esté culminada, ningún ciudadano podrá inmunizarse. El segundo aspecto que no puede dejar de mencionarse es que, tal y como han afirmado desde la cartera de Salud Argentina, no han cesado las negociaciones con otros laboratorios y farmacéuticas. De hecho, en poco tiempo  el país comenzará a producir -a través de mAbxience, de Grupo Insud- que sería la variante de AstraZeneca de Reino Unido, continúan las pruebas de Pfizer (EE.UU.) y BioNTech (Alemania) en el Hospital Militar de CABA, las de Sinopharm (China) en los centros Vacunar coordinadas desde la Fundación Huésped y las de Johnson & Johnson (EE.UU.) que ya inició el reclutamiento de voluntarios en territorio nacional.

Una vez que la vacuna COVID-19 demuestre ser segura y efectiva en un ensayo clínico, las agencias reguladoras evaluarán minuciosamente los datos antes de otorgar las aprobaciones. La OMS también supervisará un proceso de revisión independiente antes de otorgar su propia recomendación.

Por su parte, el ministro de Salud de la Nación, Ginés González García, señaló  que “ninguna vacuna, que no pruebe ser efectiva, será aprobada en el país” y aclaró que “todavía no está terminado el acuerdo comercial (con Rusia), pero no hay ninguna duda de que lo vamos a hacer”.

### **Brote de psicosis**

¿Por qué tanta desconfianza de repente? La realidad es que los humanos no nos hacemos demasiadas preguntas respecto del origen de las vacunas que están circulando. De hecho, difícilmente,  y a menos que quien se inocule sea un/a especialista en el campo, se conocerá de dónde proviene cada tipo de vacuna. La misma ignorancia se replica respecto de otros medicamentos y tratamientos. Mas allá de la ignorancia, existe un acceso ilimitado a la información y de ahí surgen tantas opiniones y especulaciones como seres humanos hay en el planeta.

Para el investigador del Conicet Martín Baña, una de las claves principales para comprender la difamación debe hallarse en la historia. “Existe una larga tradición de desconfianza y de desvalorización occidental respecto de Rusia. Se puede rastrear en el siglo XVIII, el XIX y ni hablar del XX con el comunismo”.

El jefe de Gabinete bonaerense, Carlos Bianco, afirmó en su momento que la vacuna Sputnik V contra el coronavirus adquirida por el gobierno argentino "no es un paso hacia el comunismo, no es que le estamos comprando la vacuna a la Unión de Repúblicas Socialistas Soviéticas (por la extinta URSS) sino a la Federación de Rusia, un país capitalista, con una tradición científica de varios centenios en esta materia".

Ciertamente cuando el médico rural inglés Edgard Jenner, por allá en 1796, encontró la vacuna para combatir la viruela, las poblaciones de aquel entonces, corrieron desaforados a inmunizarse sin siquiera preguntar por qué la salvación venía de una vaca. Eso explicaría, irónicamente, por qué aún no hayan surgido controversias en relación a la vacuna de Oxford. Porque, seguramente, aquella solitaria vaca no era cubana.

_La nueva tribuna_ (de Argentina) agregó que en las redes sociales estallaron los temores de quienes, lejos de ver una posible cura al azote del coronavirus, creen que lo que se les administrará será una dosis de comunismo. “No acepto transmisión de comunismo”, rezaba uno de los carteles subidos por un usuario de Facebook.

El portal del medio ABC de España señaló, por otro lado, que una de las teorías que ha surgido y se ha esparcido por redes sociales sobre la vacuna para el Covid-19 señala que se busca implantar a la población un nanochip con cada dosis del medicamento. El objetivo, según esas voces, sería “controlar” a la población con fines políticos y económicos. Detrás de este plan macabro estarían los magnates George Soros y Bill Gates; así, la vacuna para enfrentar al nuevo coronavirus tendría un ‘chip’ con nanotecnología de ADN que controlaría la actividad cerebral.

Hasta el momento, más allá de las voces disidentes, la variante rusa ha atravesado de manera adecuada -del mismo modo en que lo hicieron las otras- las Fases I y II y sus resultados (es segura y genera inmunidad robusta) fueron publicados _en The Lancet_. Por lo cual, es cuestión de esperar. No hay motivos para desconfiar especialmente de esta fórmula. Será aprobada y distribuida a la población cuando haya concluido la Fase III con buenos resultados. **Será de distribución gratuita y no obligatoria.**

***

##### **Sofía Gioja en** [**twitter**](https://twitter.com/SofiG83)