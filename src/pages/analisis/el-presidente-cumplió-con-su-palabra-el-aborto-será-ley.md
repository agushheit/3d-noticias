---
layout: analisis
author: Por Lucrecia Faccioli
category: analisis
title: 'El Presidente cumplió con su palabra: El aborto será Ley'
date: 2020-11-19T18:57:00.385+00:00
thumbnail: https://assets.3dnoticias.com.ar/lucrecia.webp

---
El Estado cuidará a las mujeres y personas gestantes que decidan interrumpir su embarazo, así como a las que deseen tener hijas e hijos.

Celebramos, no solo que el Presidente haya cumplido con su palabra -dada en la apertura de las sesiones ordinarias del Congreso de la Nación  el 1° de marzo- de enviar para su tratamiento un proyecto de ley para regular la interrupción voluntaria del embarazo, sino también que haya instalado el tema en el ámbito de la salud pública.

Así dice el art. 1: “La presente ley tiene por objeto regular el acceso a la interrupción voluntaria del embarazo y a la atención post-aborto, en cumplimiento de los compromisos asumidos por el Estado argentino en materia de salud pública y derechos humanos de las mujeres y de personas con otras identidades de género con capacidad de gestar y a fin de contribuir a la reducción de la morbilidad y mortalidad prevenible.

Igualdad ante la ley es igualdad ante la vida, dice Nelly Minyersky. Esto significa una ampliación de derechos, porque indudablemente la mujer o persona gestante que no quiera abortar no está obligada a hacerlo, pero quién sí lo decida va a tener oportunidad de hacerlo en un lugar seguro, sin riesgos, con la cobertura del sistema de salud y sin ser penalizadas.

**En el año 2018 murieron 35 mujeres por causa de aborto en la Argentina, precisamente de lo que trata este proyecto es de garantizar la vida.**

Las mujeres y otras personas con identidades de género con capacidad de gestar tienen derecho a decidir y acceder a la interrupción de su embarazo hasta la semana CATORCE (14) inclusive, fuera de ese período, solo en las situaciones previstas por la ILE:

* Si el embarazo fuere resultado de una violación, con el requerimiento y la declaración jurada pertinente de la persona gestante, ante el personal de salud interviniente. En los casos de niñas menores de TRECE (13) años de edad, la declaración jurada no será requerida.
* Si estuviere en peligro la vida o la salud integral de la persona gestante.

Se garantiza el derecho a acceder a la interrupción de su embarazo en los servicios del sistema de salud o con su asistencia, en un plazo máximo de DIEZ (10) días corridos desde su requerimiento.

**El personal de salud debe garantizar las siguientes condiciones mínimas y derechos en la atención del aborto y post-aborto: trato digno, privacidad, confidencialidad, autonomía de la voluntad, acceso a la información y calidad.**

Siguiendo el principio de la autonomía progresiva, podrán tomar la decisión:

* En los casos de personas menores de TRECE (13) años de edad, mediante su consentimiento informado con la asistencia de al menos uno/a de sus progenitores/as o representante legal.
* En los casos de adolescentes de entre TRECE (13) y DIECISÉIS (16) años de edad, se presume que cuentan con aptitud y madurez suficiente para decidir la práctica y prestar el debido consentimiento, a menos que deba utilizarse un procedimiento que implique un riesgo grave para su salud o su vida.
* Y Las personas mayores de DIECISÉIS (16) años de edad tienen plena capacidad por sí para prestar su consentimiento, a fin de ejercer los derechos que otorga la presente ley.
* En el caso de una persona con capacidad restringida por sentencia judicial y la restricción no tuviere relación con el ejercicio de los derechos que otorga la presente ley, podrá prestar su consentimiento informado sin ningún impedimento ni necesidad de autorización previa alguna y, si lo deseare, con la asistencia del sistema de apoyo previsto en el artículo 43 del Código Civil y Comercial de la Nación.

El art. 10 dice que **el o la profesional de salud tiene derecho a ejercer la objeción de conciencia, pero deberán derivar de buena fe a la paciente para que sea atendida por otro u otra profesional en forma temporánea y oportuna**, sin dilaciones, así como adoptar todas las medidas necesarias para garantizar el acceso a la práctica.

Por otro lado, indica que **el personal de salud no podrá negarse a la realización de la interrupción del embarazo en caso de que la vida o salud de la persona gestante esté en peligro y requiera atención inmediata e impostergable**. **Tampoco se podrá alegar objeción de conciencia para negarse a prestar atención sanitaria post-aborto**.

Otra cuestión muy importante de este proyecto es que en el art. 12 establece: Educación Sexual Integral y Salud Sexual y reproductiva: El Estado Nacional, las Provincias, la Ciudad Autónoma de Buenos Aires y los Municipios tienen la responsabilidad de implementar la Ley N° 26.150 de Educación Sexual Integral, estableciendo políticas activas para la promoción y el fortalecimiento de la salud sexual y reproductiva de toda la población. Con lo cual, las personas, luego de pasar por este tipo de situaciones, tienen las herramientas necesarias para cumplir con el eslogan de la Campaña Nacional por el Aborto Legal, seguro y Gratuito: “**Educación Sexual para decidir, anticonceptivos para no abortar y aborto legal para no morir**”

Este proyecto es acompañado con el que se conoce como el **Plan de los 1000 días**, lo que viene a reforzar la idea de que las mujeres que deciden y deseen tener hijos puedan hacerlo, y que aquellas de los sectores más vulnerables van a ser acompañadas y cuidadas por el Estado con un fortalecimiento de la seguridad social desde el nacimiento hasta los 3 años. Contempla brindarles a las personas elementos esenciales, vacunas, leyes, pero también una extensión de las asignaciones sociales.

***

##### **Lucrecia Faccioli |** Docente y Abogada, integrante de la CTA de los Trabajadores