import loadable from '@loadable/component'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'
const Content = loadable(() => pMinDelay(import('~src/components/pages/contacto'), 400))

const Contacto = () => {
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        seoTitle='Contacto'
        tabTitle='Contacto'
      />
      <Content fallback={Spinner} />
    </PageBase>
  )
}

export default Contacto
