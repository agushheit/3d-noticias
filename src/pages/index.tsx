import '~src/style.scss'

import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import creditos from '~src/constants/creditos'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/pages/index'), 400))

type Props = {
  data: {
    logo: {
      fixed: {
        src: string
        width: string
        height: string
      }
    }
  }
}

const IndexPage = (props: Props) => {
  const { data } = props
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  creditos()
  return (
    <PageBase>
      <Seo
        seoTitle='3D Noticias'
        imagePath={data.logo.fixed.src}
        imageWidth={data.logo.fixed.width}
        imageHeight={data.logo.fixed.height}
      />
      <Content fallback={Spinner} />
    </PageBase>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query {
    logo: imageSharp(fluid: {originalName: {eq: "avatar-varios.jpg"}}) {
      fixed {
        src
        height
        width
      }
    }
  }
`
