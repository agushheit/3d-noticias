import { faChevronRight } from '@fortawesome/free-solid-svg-icons/faChevronRight'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { graphql, Link, useStaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import React from 'react'
import { Button, Card, Col, Container, Row } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import styled from 'styled-components'

import Seo from '~src/components/atoms/Seo'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Micro = styled(Img)<FluidProps>`
  height: auto;
  width: 100%;
`
const Titulo = styled.h1`
  color: #CF0000;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 20.8px;
`
const Descripcion = styled.p`
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 18px;
`
const Boton = styled(Button)`
  background-color: transparent;
  border: 1px solid #cc0000;
  color: #cc0000;
  font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';
  margin-bottom: 10px;
  transition: all 0.15s;
  width: fit-content;

  :hover {
    background-color: #cc0000;
    color: white;
  }
`
const StyledCard = styled(Card)`
  border-color: #CF0000;
  margin-top: 20px;
    .card-body {
      padding: 15px;
    }
`
const StyledHeader = styled(Card.Header)`
  background-color: #ffebee;
  border-bottom: 1px solid #CF0000;
  height: fit-content;
  padding-bottom: 10px;
  padding-top: 15px;
`
const Icon = styled(FontAwesomeIcon)`
    font-size: 16px;
    margin-left: 5px;
`
const Error = styled.h2`
  font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';
  font-size: 50px;
  margin-bottom: 20px;
  margin-top: 30px;
`
const Mail = styled.a`
  color: #CF0000;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 18px;
`
const IndexPage = (): React.ReactElement => {
  const { imageSharp } = useStaticQuery(graphql`
    query {
      imageSharp: imageSharp(fluid: {originalName: {eq: "micro.jpg"}}) {
        fluid(quality: 100, maxWidth: 700) {
          ...GatsbyImageSharpFluid_withWebp_noBase64
        }
      }
    }
  `)
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        tabTitle='404'
        seoTitle='3D Noticias'
      />
      <Container>
        <StyledCard>
          <StyledHeader>
            <Titulo>Parece que la página que buscás no existe...</Titulo>
          </StyledHeader>
          <StyledCard.Body>
            <Row>
              <Col md={4}>
                <Micro fluid={imageSharp?.fluid}/>
              </Col>
              <Col>
                <Error>Error 404</Error>
                <Descripcion>
                  Lo sentimos, pero la página que buscás no existe.
                  <br/>
                  Por favor verificá la dirección introducida e intentalo de nuevo,
                  <br/>
                  o seguí navegando por la página:
                </Descripcion>
                <Link to='/'>
                  <Boton>
                    Página principal
                    <Icon icon={faChevronRight}/>
                  </Boton>
                </Link>
                <Descripcion>
                  También podés usar el menú superior para visitar
                  <br/>
                  nuestro contenido, o contactarnos vía&nbsp;
                  <Mail href="mailto:info@3dnoticias.ar">
                    e-mail
                  </Mail>
                </Descripcion>
              </Col>
            </Row>
          </StyledCard.Body>
        </StyledCard>

      </Container>
    </PageBase>
  )
}

export default IndexPage
