
import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/pages/nuestros-lectores'), 400))

type Props = {
  data: {
    masVisto: GraphqlArray<Noticia>
    logo: {
      fixed: {
        src: string
        width: string
        height: string
      }
    }
  }
  pageContext: PageContext<Carta>
}

const Cartas = (props: Props) => {
  const cartas = props.pageContext.group.map(edge => edge.node)
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        noBaseUrl
        tabTitle='Nuestros Lectores'
        seoTitle='Nuestros Lectores'
        imagePath={cartas[0].frontmatter.thumbnail}
        urlPath='/nuestros-lectores'
        description='Sección cartas del portal 3D Noticias.'
      />
      <Content fallback={Spinner} masVisto={props.data.masVisto} pageContext={props.pageContext} />
    </PageBase>
  )
}

export const pageQuery = graphql`
  query {
    logo: imageSharp(fluid: {originalName: {eq: "avatar-varios.jpg"}}) {
      fixed {
        src
        height
        width
      }
    }
    masVisto: allMarkdownRemark(
      sort: {order: DESC, fields: [frontmatter___date]},
      limit: 4,
      filter: {frontmatter: {category: {nin: ["Denuncias", "Protagonistas"], in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Estado Real", "Deportes"]}}} ) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            url
            resumen
            layout
            thumbnail
            author
            entradilla
            category
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
  }
`

export default Cartas
