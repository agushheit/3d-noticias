
import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/pages/agenda-ciudadana'), 400))

type Props = {
  data: {
    markdown: MarkdownRemark
    masVisto: GraphqlArray<Noticia>
    logo: {
      fixed: {
        src: string
        width: string
        height: string
      }
    }
  }
  pageContext: PageContext<Noticia>
}

const AgendaCiudadana = (props: Props) => {
  const noticias = props.pageContext.group.map(edge => edge.node)
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        noBaseUrl
        tabTitle='Agenda Ciudadana'
        seoTitle='Agenda Ciudadana'
        imagePath={noticias[0].frontmatter.thumbnail}
        urlPath='/agenda-ciudadana'
        description='Sección agenda ciudadana del portal 3D Noticias.'
      />
      <Content fallback={Spinner} masVisto={props.data.masVisto} context={props.pageContext} noticias={noticias} />
    </PageBase>
  )
}

export const pageQuery = graphql`
  query {
    logo: imageSharp(fluid: {originalName: {eq: "avatar-varios.jpg"}}) {
      fixed {
        src
        height
        width
      }
    }
    masVisto: allMarkdownRemark(
      sort: {order: DESC, fields: [frontmatter___date]}, 
      limit: 4,
      filter: {frontmatter: {category: {nin: ["Denuncias", "Protagonistas"], in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Estado Real", "Deportes", "Cultura"]}}}) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            url
            thumbnail
            resumen
            layout
            author
            entradilla
            category
            date(formatString: "MMMM DD, YYYY")
            link_del_video
          }
        }
      }
    }
  }
`

export default AgendaCiudadana
