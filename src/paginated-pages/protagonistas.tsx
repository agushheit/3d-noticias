
import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/templates/protagonistas'), 400))

type Props = {
  data: {
    masVisto: GraphqlArray<Noticia>
    logo: {
      fixed: {
        src: string
        width: string
        height: string
      }
    }
  }
  pageContext: PageContext<Entrevista>
}

const Protagonistas = (props: Props) => {
  const emptyContent = props.pageContext.group[0].node.mocked
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        tabTitle='Protagonistas'
        seoTitle='Protagonistas'
        imagePath={props.data.logo.fixed.src}
        imageWidth={props.data.logo.fixed.width}
        imageHeight={props.data.logo.fixed.height}
        urlPath='/protagonistas'
        description='Sección protagonistas del portal 3D Noticias.'
      />
      <Content fallback={Spinner} masVisto={props.data.masVisto} context={props.pageContext} empty={emptyContent}/>
    </PageBase>
  )
}

export const pageQuery = graphql`
  query {
    logo: imageSharp(fluid: {originalName: {eq: "avatar-varios.jpg"}}) {
      fixed {
        src
        height
        width
      }
    }
    masVisto: allMarkdownRemark(
      sort: {order: DESC, fields: [frontmatter___date]},
      limit: 4,
      filter: {frontmatter: {category: {nin: ["Denuncias", "Protagonistas"], in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Estado Real", "Deportes"]}}}) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          # localImage {
          #   childImageSharp {
          #     fluid(maxHeight: 500, quality: 100, toFormat: PNG) {
          #       ...GatsbyImageSharpFluid_withWebp_noBase64
          #     }
          #   }
          # }
          frontmatter {
            title
            url
            resumen
            layout
            author
            entradilla
            category
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
  }
`

export default Protagonistas
