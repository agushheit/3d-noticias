
import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/templates/cultura'), 400))

type Props = {
  data: {
    masVisto: GraphqlArray<Noticia>
    seoImage: {
      fixed: {
        src: string
        width: string
        height: string
      }
    }
  }
  pageContext: PageContext<Noticia>
}

const Cultura = (props: Props) => {
  const emptyContent = props.pageContext.group[0].node.mocked
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        tabTitle='Cultura'
        seoTitle='Cultura'
        imagePath={props.data.seoImage?.fixed.src}
        urlPath='/cultura'
        description='Sección cultura del portal 3D Noticias.'
      />
      <Content fallback={Spinner} masVisto={props.data.masVisto} noticia={props.pageContext.group[0].node} context={props.pageContext} empty={emptyContent} />
    </PageBase>
  )
}

export const pageQuery = graphql`
  query {
    seoImage: imageSharp(fixed: {originalName: {eq: "avatar-varios.jpg"}}) {
      fixed {
        src
        height
        width
      }
    }
    logo: imageSharp(fluid: {originalName: {eq: "avatar-varios.jpg"}}) {
      fixed {
        src
        height
        width
      }
    }
    masVisto: allMarkdownRemark(
      sort: {order: DESC, fields: [frontmatter___date]},
      limit: 4,
      filter: {frontmatter: {category: {nin: ["Denuncias", "Protagonistas"], in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Estado Real", "Deportes"]}}}) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            url
            resumen
            thumbnail
            layout
            author
            entradilla
            category
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
  }
`

export default Cultura
