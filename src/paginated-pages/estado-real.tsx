import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/pages/estado-real.tsx'), 400))

type Props = {
  data: {
    masVisto: GraphqlArray<Noticia>
  }
  pageContext: PageContext<Noticia>
}
const EstadoRealPage = (props: Props) => {
  const noticias = props.pageContext.group.map(edge => edge.node)
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        noBaseUrl
        tabTitle='Estado Real'
        seoTitle='Estado Real'
        imagePath={noticias[0].frontmatter.thumbnail}
        urlPath='/estado-real'
        description='Sección estado real del portal 3D Noticias.'
      />
      <Content fallback={Spinner} masVisto={props.data.masVisto} context={props.pageContext} noticias={noticias} />
    </PageBase>
  )
}

export default EstadoRealPage

export const pageQuery = graphql`
  query {
    masVisto: allMarkdownRemark(
      sort: {order: DESC, fields: [frontmatter___date]},
      limit: 4,
      filter: {frontmatter: {category: {nin: ["Denuncias", "Protagonistas"], in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Cultura", "Deportes"]}}}) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            url
            layout
            resumen
            author
            entradilla
            thumbnail
            category
            date(formatString: "MMMM DD, YYYY")
            link_del_video
          }
        }
      }
    }
  }
`
