type Analisis = {
    excerpt: string
    id: string
    fields: {
        slug: string
    },
    localImage: any
    html: string
    frontmatter: {
        author: string
        category: string
        date: string
        title: string
        thumbnail: string | any
    }
    mocked?: boolean
}
