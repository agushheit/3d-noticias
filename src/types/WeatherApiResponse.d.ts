/* eslint-disable camelcase */
interface Information {
    temperature: string;
    wind: string;
    humidity: string;
    pressure: string;
}

interface Locality {
    name: string;
    url_weather_forecast_15_days: string;
    url_hourly_forecast: string;
    country: string;
    url_country: string;
}

interface Day1 {
    date: string;
    temperature_max: number;
    temperature_min: number;
    icon: string;
    text: string;
    humidity: number;
    wind: number;
    wind_direction: string;
    icon_wind: string;
    sunrise: string;
    sunset: string;
    moonrise: string;
    moonset: string;
    moon_phases_icon: string;
}

interface Day2 {
    date: string;
    temperature_max: number;
    temperature_min: number;
    icon: string;
    text: string;
    humidity: number;
    wind: number;
    wind_direction: string;
    icon_wind: string;
    sunrise: string;
    sunset: string;
    moonrise: string;
    moonset: string;
    moon_phases_icon: string;
}

interface Day3 {
    date: string;
    temperature_max: number;
    temperature_min: number;
    icon: string;
    text: string;
    humidity: number;
    wind: number;
    wind_direction: string;
    icon_wind: string;
    sunrise: string;
    sunset: string;
    moonrise: string;
    moonset: string;
    moon_phases_icon: string;
}

interface Day4 {
    date: string;
    temperature_max: number;
    temperature_min: number;
    icon: string;
    text: string;
    humidity: number;
    wind: number;
    wind_direction: string;
    icon_wind: string;
    sunrise: string;
    sunset: string;
    moonrise: string;
    moonset: string;
    moon_phases_icon: string;
}

interface Day5 {
    date: string;
    temperature_max: number;
    temperature_min: number;
    icon: string;
    text: string;
    humidity: number;
    wind: number;
    wind_direction: string;
    icon_wind: string;
    sunrise: string;
    sunset: string;
    moonrise: string;
    moonset: string;
    moon_phases_icon: string;
}

interface Day6 {
    date: string;
    temperature_max: number;
    temperature_min: number;
    icon: string;
    text: string;
    humidity: number;
    wind: number;
    wind_direction: string;
    icon_wind: string;
    sunrise: string;
    sunset: string;
    moonrise: string;
    moonset: string;
    moon_phases_icon: string;
}

interface Day7 {
    date: string;
    temperature_max: number;
    temperature_min: number;
    icon: string;
    text: string;
    humidity: number;
    wind: number;
    wind_direction: string;
    icon_wind: string;
    sunrise: string;
    sunset: string;
    moonrise: string;
    moonset: string;
    moon_phases_icon: string;
}

interface Hour1 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour2 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour3 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour4 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour5 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour6 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour7 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour8 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour9 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour10 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour11 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour12 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour13 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour14 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour15 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour16 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour17 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour18 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour19 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour20 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour21 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour22 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour23 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour24 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface Hour25 {
    date: string;
    hour_data: string;
    temperature: number;
    text: string;
    humidity: number;
    pressure: number;
    icon: string;
    wind: number;
    wind_direction: string;
    icon_wind: string;
}

interface HourHour {
    hour1: Hour1;
    hour2: Hour2;
    hour3: Hour3;
    hour4: Hour4;
    hour5: Hour5;
    hour6: Hour6;
    hour7: Hour7;
    hour8: Hour8;
    hour9: Hour9;
    hour10: Hour10;
    hour11: Hour11;
    hour12: Hour12;
    hour13: Hour13;
    hour14: Hour14;
    hour15: Hour15;
    hour16: Hour16;
    hour17: Hour17;
    hour18: Hour18;
    hour19: Hour19;
    hour20: Hour20;
    hour21: Hour21;
    hour22: Hour22;
    hour23: Hour23;
    hour24: Hour24;
    hour25: Hour25;
}

type WeatherApiResponse = {
    copyright: string;
    use: string;
    information: Information;
    web: string;
    language: string;
    locality: Locality;
    day1: Day1;
    day2: Day2;
    day3: Day3;
    day4: Day4;
    day5: Day5;
    day6: Day6;
    day7: Day7;
    hour_hour: HourHour;
}
