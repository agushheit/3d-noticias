type Entrevista = {
  excerpt: string
  id: string
  fields: {
      slug: string
  },
  html: string
  frontmatter: {
      author: string
      resumen: string
      category: string
      date: string
      title: string
      thumbnail: string | any
  }
  mocked?: boolean
}
