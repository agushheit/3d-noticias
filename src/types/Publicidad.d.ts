import { GatsbyImageFluidProps } from 'gatsby-image'

export type Publicidad = {
  id: string
  localImage: any
  frontmatter: {
    url: string
    thumbnail: string | {
      childImageSharp: GatsbyImageFluidProps
    }
    name: string
    date: string
  },
}
