type PageContext<T> = {
    pathPrefix: string
    first: boolean
    last: boolean
    index: number
    pageCount: number
    group: Array<{
        node: T
    }>
}
