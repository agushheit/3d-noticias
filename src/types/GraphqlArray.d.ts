type GraphqlArray<T> = {
    edges: Array<{node: T}>
}
