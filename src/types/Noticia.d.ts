/* eslint-disable camelcase */
type Noticia = {
  id: string
  excerpt: string
  html: string
  fields?: {
    slug: string
  }
  localImage: any
  frontmatter: {
    description: string
    thumbnail: string | any
    title: string
    date: string
    author: string
    resumen: string
    category: 'agenda' | 'estado' | 'analisis' | 'cultura'
    entradilla: string
    url: string
    layout: string
    link_del_video: string
  }
  mocked?: boolean
}
