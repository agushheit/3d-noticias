type Carta = {
  id: string
  html: string
  fields?: {
    slug: string
  }
  localImage: any
  frontmatter: {
    description: string
    thumbnail: string | any
    title: string
    date: string
    author: string
    category: 'lectores'
    entradilla: string
  }
  mocked?: boolean
}
