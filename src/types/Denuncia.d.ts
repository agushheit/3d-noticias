type Denuncia = {
  id: string
  html: string
  fields?: {
    slug: string
  }
  frontmatter: {
    description: string
    thumbnail: string | any
    title: string
    date: string
    author: string
    category: 'denuncias'
    entradilla: string
  }
  mocked?: boolean
}
