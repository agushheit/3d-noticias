import React from 'react'
import { Container } from 'react-bootstrap'

import Empty from '~src/components/molecules/EmptyContent'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import TemplateCarta from '~src/components/organisms/TemplateCarta'

const CartaContent = (props: { carta: Carta, masVisto: GraphqlArray<Noticia>, empty?: boolean
}) => (
  <>
    <SocialMediaContainer />
    <Container>
      {!props.empty && <React.Fragment>
        <TemplateCarta masVisto={props.masVisto} carta={props.carta} />
      </React.Fragment>}
      {props.empty && (
        <Empty/>
      )}
    </Container>
  </>
)

export default CartaContent
