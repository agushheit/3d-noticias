import React from 'react'
import { Container } from 'react-bootstrap'

import Empty from '~src/components/molecules/EmptyContent'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import TemplateNoticiaVideo from '~src/components/organisms/TemplateNoticiaVideo'

const NoticiaVideoContent = (props: { noticia: Noticia, masVisto: GraphqlArray<Noticia>, empty?: boolean}) => (
  <>
    <SocialMediaContainer />
    <Container>
      {!props.empty && <React.Fragment>
        <TemplateNoticiaVideo masVisto={props.masVisto} post={props.noticia} />
      </React.Fragment>}
      {props.empty && (
        <Empty/>
      )}
    </Container>
  </>
)

export default NoticiaVideoContent
