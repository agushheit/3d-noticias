
import { faUserAlt } from '@fortawesome/free-solid-svg-icons/faUserAlt'
import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

import Content, { HTMLContent } from '~src/components/atoms/Content'
import LaFecha from '~src/components/atoms/Date'
import SearchBar from '~src/components/atoms/SearchBar'
import ShareButtons from '~src/components/atoms/ShareButtons'
import CommentsWidget from '~src/components/molecules/Categorias/CommentsWidget'
import Paginacion from '~src/components/molecules/Categorias/Paginacion'
import Empty from '~src/components/molecules/EmptyContent'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import PreviewCompatibleImage from '~src/components/molecules/PreviewCompatibleImage'
import Publicidades from '~src/components/molecules/Publicidades'
import MasVisto from '~src/components/organisms/Categorias/MasVisto'
import { Publicidad } from '~src/types/Publicidad'

const Foto = styled(PreviewCompatibleImage)`
  
`
const Titulo = styled.h1`
  box-sizing: border-box;
  color: #212529;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 27px;
  font-weight: 800;
  line-height: 1.2;
  margin-bottom: 1rem !important;
  text-align: left;
`
const NombreEntrevistado = styled.h2`
  box-sizing: border-box;
  color: #212529;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 22px;
  font-weight: 800;
  line-height: 1.2;
  margin-bottom: 1rem !important;
  text-align: left;
  text-indent: 15px;
`
const NombreAutor = styled.h3`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 19.2px;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 1rem;
  margin-top: 0;
  text-align: left;
`
const Contenido = styled.div`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 19.2px;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 1rem;
  margin-top: 0;
  text-align: left;

  img {
    width: 100%;
  }
  h1 {
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 1.25em !important;
    font-weight: 800;
  }
  h2 {
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 1.2em !important;
    font-weight: 700;
  }
  h3 {
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 1.15em !important;
    font-weight: 700;
  }
  h4 {
    color: #F0060D;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 1.1em !important;
    font-weight: 700;
  }
  h5 {
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 1em !important;
    font-weight: 700;
  }
  h6 {
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: .75 !important;
    font-weight: 500;
  }
  blockquote {
    background-color: #CF0000;
    border-left: 0;
    border-radius: 5px;
    margin-bottom: 20px;
    padding: 30px 30px 20px 45px;

    * {
      color: #fff;
    }

    :before {
      content: "";
    }

    p {
      font-family: 'Open Sans', serif;
      font-size: 16px;
      line-height: 1.4;
      padding-left: 15px;
      padding-top: 0;
      position: relative;

      :before {
        content: "\f10d";
        font-family: FontAwesome;
        font-size: 32px;
        font-style: normal;
        font-weight: normal;
        left: -27px;
        position: absolute;
        text-decoration: inherit;
        top: -27px;
      }
    }
  }
`
const Linea = styled.hr`
  border-top: 1px solid rgba(0, 0, 0, 0.1);
`
const Fecha = styled.p`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 15.34px;
  font-weight: 400;
  line-height: 1.2;
  text-align: left;
`
const GeneralContainer = styled(Row)`
  padding-top: 20px;
`

type Props = {
  context: PageContext<Entrevista>
  masVisto: GraphqlArray<Noticia>
  empty?: boolean
}

const ProtagonistasContent = (props: Props) => {
  const ProtagonistasContent = HTMLContent || Content
  const ultimaEntrevista = props.context.group[0].node
  const entrevistaUrl = 'https://3dnoticias.com.ar' + ultimaEntrevista.fields?.slug
  const data = useStaticQuery(graphql`
    {
      publicidades: allMarkdownRemark(sort: {order: DESC, fields: [frontmatter___date]}, filter: {fileAbsolutePath: {regex: "/src/pages/publicidades/"}}) {
        nodes {
          id
 
          frontmatter {
            thumbnail
            url
            name
            date
          }
        }
      }
    }
  `)
  const publicidades: Publicidad[] = [...data.publicidades.nodes].sort(() => Math.random() - 0.5)
  if (publicidades.length > 0) {
    while (publicidades.length < 2) {
      publicidades.push(...publicidades)
    }
  }
  return (
    <>
      <SocialMediaContainer />
      <LaFecha/>
      <Container>
        <SearchBar titulo='Protagonistas' icono={faUserAlt}/>
        <GeneralContainer>
          <Col md={8}>
            {!props.empty && <React.Fragment>
              <Titulo>
                {ultimaEntrevista.frontmatter?.title}
              </Titulo>
              <NombreEntrevistado>
                {ultimaEntrevista.frontmatter?.resumen}
              </NombreEntrevistado>
              <Foto imageInfo={{
                image: ultimaEntrevista.frontmatter?.thumbnail,
                alt: ultimaEntrevista.frontmatter?.title,
              }}/>
              <Linea/>
              <NombreAutor>
                {ultimaEntrevista.frontmatter?.author}
              </NombreAutor>
              <Linea/>
              <Contenido>
                <ProtagonistasContent content={ultimaEntrevista.html} />
              </Contenido>
              <Fecha>
              Santa Fe,
                {' '}
                {new Date(ultimaEntrevista.frontmatter?.date).toLocaleDateString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' })}
              </Fecha>
              <ShareButtons link={entrevistaUrl}/>
              <Paginacion context={props.context} nextPageText='Anterior' prevPageText='Más reciente'/>
              <CommentsWidget post={ultimaEntrevista}/>
            </React.Fragment>}
            {props.empty && (
              <Empty/>
            )}
          </Col>
          <Col md={4}>
            <Publicidades ad1={publicidades[0]} ad2={publicidades[1]} style={{ maxHeight: 500 }} />
          </Col>
          <MasVisto noticias={props.masVisto}/>
        </GeneralContainer>
      </Container>
    </>
  )
}
export default ProtagonistasContent
