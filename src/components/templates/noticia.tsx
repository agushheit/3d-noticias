import React from 'react'
import { Container } from 'react-bootstrap'

import Empty from '~src/components/molecules/EmptyContent'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import TemplateNoticia from '~src/components/organisms/TemplateNoticia'

const NoticiaContent = (props: { noticia: Noticia, masVisto: GraphqlArray<Noticia>, empty?: boolean}) => (
  <>
    <SocialMediaContainer />
    <Container>
      {!props.empty && <React.Fragment>
        <TemplateNoticia masVisto={props.masVisto} post={props.noticia} />
      </React.Fragment>}
      {props.empty && (
        <Empty/>
      )}
    </Container>
  </>
)

export default NoticiaContent
