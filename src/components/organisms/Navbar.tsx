import { Link } from 'gatsby'
import React from 'react'
import { Button, Nav, Navbar } from 'react-bootstrap'
import styled from 'styled-components'

import Dropdown from '~src/components/atoms/Dropdown'
import WidgetClima from '~src/components/atoms/WidgetClima'
import LogoImg from '~src/images/Logo.png'

const StyledNavbar = styled(Navbar)`
  align-content: center;
  background-color: #343a40;
  display: flex;
  height: auto;
  justify-content: space-between;
  min-height: 67px;
  padding: 0.5rem 0%;
  position: sticky;
  top: 0;
  z-index: 1030;
  @media (min-width: 1440px) {
    padding: 0 2%;
  }
  @media (min-width: 1500px) {
    padding: 0 5%;
  }
  @media (min-width: 1600px) {
    padding: 0 15%;
  }
  @media (min-width: 2045px) {
    padding: 0 20%;
  }
`
const Logo = styled.div`
  background-image: url(${LogoImg});
  background-size: contain;
  height: 43px;
  width: 200px;

  @media(max-width: 350px) {
    height: 30px;
    width: 150px;
  }
`
const Botoncito = styled(Button)`
  background-color: transparent;
  border: none;
  color: rgba(255, 255, 255, 0.75);
  font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';
  font-size: 14.4px;
  font-style: normal;
  height: 33px;
  line-height: 14.4px;
  list-style: none;
  padding: 8px;
  text-align: left;
  text-decoration: none;

  &:hover, &:active, &:focus {
      background-color: #CC0000 !important;
      border: none !important;
      box-shadow: none !important;
      color: rgba(255, 255, 255, 0.95);
  }


  @media (max-width: 991px) {
      padding: 8px 16px;
  }
`
const StyledNav = styled(Nav)`
  align-items: center;

  @media (max-width: 991px) {
    align-items: baseline;
    display: flex;
    flex-direction: column;
  }
`
const StyledContainer = styled.div`
  align-items: center;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: 0 auto;
  min-width: 80%;

  .navbar-collapse {
    flex-grow: unset;
  }

  @media (min-width: 991px) and (max-width: 1080px) {
    .collapse {
      display: none !important;
    }
    .navbar-toggler {
      display: block;
    }
  }
`

const NavBar = () => (
  <StyledNavbar collapseOnSelect expand="lg" bg="dark" variant="dark">
    <StyledContainer>
      <Link to="/" aria-label='Volver al inicio'>
        <Logo />
      </Link>
      <StyledNavbar.Toggle aria-controls="responsive-navbar-nav" />
      <StyledNavbar.Collapse id="responsive-navbar-nav">
        <StyledNav className="mr-auto">
          <Link to='/agenda-ciudadana'>
            <Botoncito>
              Agenda Ciudadana
            </Botoncito>
          </Link>
          <Link to='/estado-real'>
            <Botoncito>Estado Real</Botoncito>
          </Link>
          <Dropdown/>
          {/* <Link to='/opinion'>
            <Botoncito>Opinión</Botoncito>
          </Link> */}
          <Link to='/deportes'>
            <Botoncito>Deportes</Botoncito>
          </Link>
          <Link to='/la-ciudad'>
            <Botoncito>La ciudad</Botoncito>
          </Link>
          <Link to='/el-campo'>
            <Botoncito>El campo</Botoncito>
          </Link>
          <Link to='/cultura'>
            <Botoncito>Cultura</Botoncito>
          </Link>
          <WidgetClima />
        </StyledNav>
      </StyledNavbar.Collapse>
    </StyledContainer>
  </StyledNavbar>
)

export default NavBar
