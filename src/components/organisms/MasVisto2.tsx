
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

import NoticiasRecomendadas from '~src/components/molecules/NoticiasRecomendadas'

const Cabecera = styled.h5`
  box-sizing: border-box;
  color: #212529;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 20.8px;
  font-weight: 600;
  line-height: 21px;
  margin-bottom: 10px;
  margin-top: 25px;
  text-align: left;
`
const MasVisto2 = (props: { noticias: GraphqlArray<Noticia> }) => (
  <Container>
    <Row>
      <Cabecera>Lo más visto:</Cabecera>
      {props.noticias?.edges.map((edge, index) => (
        <Col lg={10} key={index}>
          <NoticiasRecomendadas noticia={edge.node} />
        </Col>
      ))}
    </Row>
  </Container>
)

export default MasVisto2
