import { faBookReader } from '@fortawesome/free-solid-svg-icons/faBookReader'
import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

import SearchBar from '~src/components/atoms/SearchBar'
import StyledAd from '~src/components/atoms/StyledAd'
import LectoresCards from '~src/components/molecules/Categorias/LectoresCards'
import Paginacion from '~src/components/molecules/Categorias/Paginacion'
import Publicidades from '~src/components/molecules/Publicidades'
import { Publicidad } from '~src/types/Publicidad'

const GeneralContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
  padding-right: 20px;
  width: 100%;
      
  @media (max-width: 1080px) {
      padding-left: 5%;
    }
`
const StyledCol = styled(Col)`
    @media (max-width: 600px) {
      padding-left: 5px;
      padding-right: 5px;
    }
`

const Importante = styled.p`
  border-top: 1px solid rgba(0, 0, 0, 0.1);
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 15.36px;
  font-weight: 400;
  line-height: 1.6;
  margin-bottom: 0;
  padding-top: 10px;
  text-align: center;
`

type Props = {
  context: PageContext<Carta>
}

const LectoresContainer = (props: Props) => {
  const cartas = props.context.group.map(edge => edge.node)
  const data = useStaticQuery(graphql`
    {
      publicidades: allMarkdownRemark(sort: {order: DESC, fields: [frontmatter___date]}, filter: {fileAbsolutePath: {regex: "/src/pages/publicidades/"}}) {
        nodes {
          id
          frontmatter {
            thumbnail
            url
            name
            date
          }
        }
      }
    }
  `)
  const publicidades: Publicidad[] = [...data.publicidades.nodes].sort(() => Math.random() - 0.5)
  if (publicidades.length > 0) {
    while (publicidades.length < 2) {
      publicidades.push(...publicidades)
    }
  }
  const adSenseSlots = ['5967380078', '9965484770', '2366884710']
  return (
    <Container>
      <Row>
        <Col lg={12}>
          <SearchBar icono={faBookReader} titulo='Nuestros lectores' />
        </Col>
        <StyledCol>
          <GeneralContainer>
            <Row>
              <StyledCol lg={8} sm={12}>
                {cartas.map((carta, index) => (
                  <StyledCol key={index}>
                    <LectoresCards carta={carta} />
                  </StyledCol>
                ))}
              </StyledCol>
              <StyledCol lg={4} sm={12}>
                <Publicidades ad1={publicidades[0]} ad2={publicidades[1]} />
                <StyledAd
                  client='ca-pub-7785551334553916'
                  slot={adSenseSlots[1]}
                  style={{
                    display: 'block',
                    height: 290,
                    marginBottom: 30,
                  }}
                />
                {typeof window !== 'undefined' &&
                  <script>
                    {`
                    try {
                      (window.adsbygoogle = window.adsbygoogle || []).push({});
                    } catch (error) {
                    }              
                    `}
                  </script>
                }
                <StyledAd
                  client='ca-pub-7785551334553916'
                  slot={adSenseSlots[0]}
                  style={{
                    display: 'block',
                    height: 290,
                  }}
                />
                {typeof window !== 'undefined' &&
                  <script>
                    {`
                    try {
                      (window.adsbygoogle = window.adsbygoogle || []).push({});
                    } catch (error) {
                    }              
                    `}
                  </script>
                }
              </StyledCol>
              <Paginacion context={props.context} nextPageText='Anteriores' prevPageText='Más recientes'/>
              <Importante>
          * IMPORTANTE: Los textos destinados a esta sección no deben exceder las 15 líneas o los 1100 caracteres. Debe constar el nombre del remitente, firma, domicilio, teléfono y número de documento. Por razones de espacio y de estilo, 3DNoticias podrá seleccionar el material y editarlo. Los mensajes deben enviarse a: redaccion@3dnoticias.ar
              </Importante>
            </Row>
          </GeneralContainer>
        </StyledCol>
      </Row>
    </Container>
  )
}

export default LectoresContainer
