import { faCheckDouble } from '@fortawesome/free-solid-svg-icons/faCheckDouble'
import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import { Card, Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

import SearchBar from '~src/components/atoms/SearchBar'
import StyledAd from '~src/components/atoms/StyledAd'
import Paginacion from '~src/components/molecules/Categorias/Paginacion'
import NoticiaCards from '~src/components/molecules/NoticiaCard2'
import Publicidades from '~src/components/molecules/Publicidades'
import { Publicidad } from '~src/types/Publicidad'

const GeneralContainer = styled.div`
    display: flex;
    flex-direction: row;
    height: 100%;
    padding-top: 20px;
    width: 100%;

    @media (max-width: 1080px) {
      padding-left: 5%;
      padding-right: 5%;
    }
`
const StyledCard = styled(Card)`
    height: calc(100% - 30px);
    margin-bottom: 30px;
    overflow: hidden;

    Body {
        padding-left: 10px !important;
    }
    Img {
        cursor: pointer !important;
        transition: 0.3s;

        &:hover {
          filter: saturate(1.5);
          transform: scale(1.2);
        }
    }
`

const StyledCol = styled(Col)` 
      margin-bottom: 30px;
`
const BContainer = styled(Container)`
    padding-left: 0 !important;
    padding-right: 0 !important;

    ${StyledCard} {
      margin-bottom: 30px!important;
    }
`
const AdsContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  max-height: 600px;
  width: 100%;

  @media (max-width: 1081px) {
      max-height: 670px ;
    }

    @media (max-width: 991px) {
      max-height: 600px;
    }

    @media (max-width: 767px) {
      max-height: 750px;
    }

    @media (max-width: 500px) {
      max-height: 800px;
    }
`
type Props = {
  noticias: Noticia[]
  context: PageContext<Noticia>
}

const EstadoContainer = (props: Props) => {
  const data = useStaticQuery(graphql`
    {
      publicidades: allMarkdownRemark(sort: {order: DESC, fields: [frontmatter___date]}, filter: {fileAbsolutePath: {regex: "/src/pages/publicidades/"}}) {
        nodes {
          id
          frontmatter {
            thumbnail
            url
            name
            date
          }
        }
      }
    }
  `)
  const publicidades: Publicidad[] = [...data.publicidades.nodes].sort(() => Math.random() - 0.5)
  if (publicidades.length > 0) {
    while (publicidades.length < props.noticias.length) {
      publicidades.push(...publicidades)
    }
  }
  const adSenseSlots = ['5967380078', '9965484770', '7095778852', '3156533843']
  return (
    <Container>
      <Row>
        <Col lg={12}>
          <SearchBar icono={faCheckDouble} titulo='Estado Real' />
        </Col>
        <Col lg={12}>
          <GeneralContainer>
            <BContainer>
              <Row>
                {props.noticias.map((noticia, index) => (
                  <React.Fragment key={index}>
                    <StyledCol lg={4} sm={6}>
                      <NoticiaCards noticia={noticia} />
                    </StyledCol>
                    {(index === 1) && (publicidades.length > 0) &&
                      <StyledCol lg={4} sm={6}>
                        <Publicidades ad1={publicidades[index - 1]} ad2={publicidades[index]} />
                      </StyledCol>}
                    {(index !== 1) && (index % 2 === 1) &&
                      <StyledCol lg={4} sm={6}>
                        <AdsContainer>
                          <StyledAd
                            client='ca-pub-7785551334553916'
                            slot={adSenseSlots[index - 3]}
                            style={{
                              display: 'block',
                              height: 290,
                            }}
                          />
                          {typeof window !== 'undefined' &&
                          <script>
                            {`
                            try {
                              (window.adsbygoogle = window.adsbygoogle || []).push({});
                            } catch (error) {
                              
                            }              
                            `}
                          </script>
                          }
                          <StyledAd
                            client='ca-pub-7785551334553916'
                            slot={adSenseSlots[index - 2]}
                            style={{
                              display: 'block',
                              height: 290,
                            }}
                          />
                          {typeof window !== 'undefined' &&
                          <script>
                            {`
                            try {
                              (window.adsbygoogle = window.adsbygoogle || []).push({});
                            } catch (error) {
                              
                            }              
                            `}
                          </script>
                          }
                        </AdsContainer>
                      </StyledCol>}
                  </React.Fragment>
                ))}
                <Paginacion context={props.context} nextPageText='Noticias anteriores' prevPageText='Noticias más recientes' />
              </Row>
            </BContainer>
          </GeneralContainer>
        </Col>
      </Row>
    </Container>
  )
}
export default EstadoContainer
