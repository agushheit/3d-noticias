import { faBullhorn } from '@fortawesome/free-solid-svg-icons/faBullhorn'
import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import { Card, Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

import SearchBar from '~src/components/atoms/SearchBar'
import StyledAd from '~src/components/atoms/StyledAd'
import DenunciaCards from '~src/components/molecules/Categorias/DenunciaCards'
import Paginacion from '~src/components/molecules/Categorias/Paginacion'
import Empty from '~src/components/molecules/EmptyContent'
import Publicidades from '~src/components/molecules/Publicidades'
import { Publicidad } from '~src/types/Publicidad'

const GeneralContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
  width: 100%;
      
    @media (max-width: 1080px) {
      padding-left: 5%;
      padding-right: 5%;
    }
`
const StyledCard = styled(Card)`
  height: calc(100% - 30px);
  margin-bottom: 30px;
  overflow: hidden;

    Body {
      padding-left: 10px !important;
    }
    Img {
      cursor: pointer !important;
      transition: 0.3s;

      &:hover {
        filter: saturate(1.5);
        transform: scale(1.2);
        }
      }
`
const StyledCol = styled(Col)`
  margin-bottom: 30px;
  padding-left: 5px !important;
  padding-right: 5px !important;

  @media (max-width: 991px) {
    padding: 0 !important;
  }
`
const BContainer = styled(Container)`
  padding-left: 15px !important;
  padding-right: 15px !important;
      
    ${StyledCard} { 
      margin-bottom: 30px!important;
      }
  @media (max-width: 991px) {
    padding: 0 !important;
  }
`

type Props = {
  context: PageContext<Denuncia>
  empty?: boolean
}

const DenunciasContainer = (props: Props) => {
  const denuncias = props.context.group.map(edge => edge.node)
  const data = useStaticQuery(graphql`
    {
      publicidades: allMarkdownRemark(sort: {order: DESC, fields: [frontmatter___date]}, filter: {fileAbsolutePath: {regex: "/src/pages/publicidades/"}}) {
        nodes {
          id
          frontmatter {
            thumbnail
            url
            name
            date
          }
        }
      }
    }
  `)
  const publicidades: Publicidad[] = [...data.publicidades.nodes].sort(() => Math.random() - 0.5)
  if (publicidades.length > 0) {
    while (publicidades.length < 2) {
      publicidades.push(...publicidades)
    }
  }
  const adSenseSlots = ['5967380078', '9965484770', '2366884710']
  return (
    <Container>
      <Row>
        <Col lg={12}>
          <SearchBar icono={faBullhorn} titulo='Denuncias ciudadanas' />
        </Col>
        <StyledCol lg={12}>
          <GeneralContainer>
            <BContainer>
              <Row>
                <Col lg={8} sm={6}>
                  {!props.empty && <React.Fragment>
                    {denuncias?.map((denuncia, index) => (
                      <StyledCol key={index}>
                        <DenunciaCards denuncia={denuncia} />
                      </StyledCol>
                    ))}
                  </React.Fragment>}
                  {props.empty && (
                    <Empty/>
                  )}
                </Col>
                <StyledCol lg={4} sm={6}>
                  <Publicidades ad1={publicidades[0]} ad2={publicidades[1]} />
                  <StyledAd
                    client='ca-pub-7785551334553916'
                    slot={adSenseSlots[1]}
                    style={{
                      display: 'block',
                      height: 290,
                      marginBottom: 30,
                    }}
                  />
                  {typeof window !== 'undefined' &&
                  <script>
                    {`
                    try {
                      (window.adsbygoogle = window.adsbygoogle || []).push({});
                    } catch (error) {
                    }              
                    `}
                  </script>
                  }
                  <StyledAd
                    client='ca-pub-7785551334553916'
                    slot={adSenseSlots[0]}
                    style={{
                      display: 'block',
                      height: 290,
                    }}
                  />
                  {typeof window !== 'undefined' &&
                  <script>
                    {`
                    try {
                      (window.adsbygoogle = window.adsbygoogle || []).push({});
                    } catch (error) {
                    }              
                    `}
                  </script>
                  }
                </StyledCol>
                <Paginacion context={props.context} nextPageText='Anteriores' prevPageText='Más recientes'/>
              </Row>
            </BContainer>
          </GeneralContainer>
        </StyledCol>
      </Row>
    </Container>
  )
}

export default DenunciasContainer
