import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

import NoticiaRecomendada from '~src/components/molecules/Categorias/NoticiaRecomendada'

const StyledContainer = styled(Container)`
  border-top: 1px solid rgba(31, 30, 30, 0.1);
  margin-top: 10px;

`
const Titulo = styled.h2`
  box-sizing: border-box;
  color: #212529;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 20.8px;
  font-weight: 600;
  line-height: 1.2;
  margin-bottom: 1.5rem !important;
  margin-top: 1.5rem !important;
  text-align: left;
`

const MasVisto = (props: { noticias: GraphqlArray<Noticia> }) => (
  <StyledContainer>
    <Titulo>Te puede interesar:</Titulo>
    <Row>
      {props.noticias?.edges.map((edge, index) => (
        <Col md={3} sm={6} key={index}>
          <NoticiaRecomendada noticia={edge.node}/>
        </Col>
      ))}
    </Row>
  </StyledContainer>
)

export default MasVisto
