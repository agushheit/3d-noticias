
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons/faPhoneAlt'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

import SearchBar from '~src/components/atoms/SearchBar'
import ContainerDeContacto from '~src/components/molecules/Categorias/ContainerDeContacto'
import InfoContainer from '~src/components/molecules/Categorias/InfoContainer'
import Logo from '~src/images/logo1.png'

const Imagen = styled.div`
    background-image: url(${Logo});
    background-size: contain;
    height: 40px;
    width: 190px;
`
const Aclaracion = styled.p`
  box-sizing: border-box;
  color: #212529;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 11.52px;
  font-weight: normal;
  line-height: 16px;
  margin-bottom: 0.5rem;
  margin-top: 15px;
  text-align: left;
  
`

const ContactContainer = () => {
  return (
    <Container>
      <Row>
        <Col lg={12}>
          <SearchBar icono={faPhoneAlt} titulo='Contacto' />
        </Col>
        <Col lg={8}>
          <iframe
            title='Nuestra ubicación'
            src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d59703.71537906519!2d-60.70755044416209!3d-31.629284610327666!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sar!4v1601642491252!5m2!1ses-419!2sar"
            width="100%"
            height="400"
            frameBorder="0"
            style={{ border: 0 }}
            allowFullScreen
            aria-hidden="false"
          />
        </Col>
        <Col lg={4}>
          <Imagen />
          <InfoContainer/>
          <ContainerDeContacto/>
          <Aclaracion>*3DNoticias se reserva el derecho de seleccionar la información recibida para ser publicada por este medio.</Aclaracion>
        </Col>
      </Row>
    </Container>
  )
}

export default ContactContainer
