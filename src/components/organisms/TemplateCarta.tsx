import { faHashtag } from '@fortawesome/free-solid-svg-icons/faHashtag'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import { Col, Row } from 'react-bootstrap'
import styled from 'styled-components'

import Cita from '~src/components/atoms/Cita'
import Content, { HTMLContent } from '~src/components/atoms/Content'
import ShareButtons from '~src/components/atoms/ShareButtons'
import StyledAd from '~src/components/atoms/StyledAd'
import Comments from '~src/components/molecules/Categorias/CommentsWidget'
import { Ad } from '~src/components/molecules/Publicidades'
import MasVisto2 from '~src/components/organisms/MasVisto2'
import { Publicidad } from '~src/types/Publicidad'

const CContainer = styled.div`
  background-color: #F51E1E;
  border-radius: 4px;
  box-sizing: border-box;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 16.6px;
  font-weight: 600;
  height: 25px;
  line-height: 1.2;
  margin-top: 20px;
  text-align: left;
  width: fit-content;
`
const Categoria = styled.h5`
  box-sizing: border-box;
  color: ghostwhite;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 16.6px;
  font-weight: 600;
  line-height: 1.2;
  margin-right: 5px;
  margin-top: 3px;
  text-align: left;
`
const Titulo = styled.h1`
  box-sizing: border-box;
  color: #212529;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 28.8px;
  font-weight: 800;
  line-height: 1.2;
  margin-bottom: 1rem !important;
  margin-top: 1.5rem !important;
  text-align: left;

`
const Fecha = styled.p`
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  border-top: 1px solid rgba(0, 0, 0, 0.1);
  box-sizing: border-box;
  color: #212529;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 17px;
  font-weight: 300;
  line-height: 1.2;
  margin: 16px 0;
  padding: 20px 0;
  text-align: left;
`
const Entradilla = styled.p`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 21px;
  font-weight: 300;
  line-height: 1.2;
  text-align: left;
  text-size-adjust: 100%;
`
const Contenido = styled.div`
  p, h1, h2, h3, h4, h5, em, li{
    box-sizing: border-box;
    color: #212529;
    font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
    font-size: 19.2px;
    font-weight: 400;
    line-height: 1.2;
    text-align: left;
  }
  h1 {
    font-size: 1.25em !important;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-weight: 800;
  }
  h2 {
    font-size: 1.2em !important;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-weight: 700;
  }
  h3 {
    font-size: 1.15em !important;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-weight: 700;
  }
  h4 {
    font-size: 1.1em !important;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    color: #F0060D;
    font-weight: 700;
  }
  h5 {
    font-size: 1em !important;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-weight: 700;
  }
  h6 {
    font-size: .75 !important;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-weight: 500;
  }
  li {
    font-size: 18.5px;
  }
  strong {
    font-weight: 600;
  }
  blockquote {
    background-color: #CF0000;
    border-left: 0;
    border-radius: 5px;
    margin-bottom: 20px;
    padding: 30px 30px 20px 45px;

    * {
      color: #fff;
    }

    :before {
      content: "";
    }

    p {
      font-family: 'Open Sans', serif;
      font-size: 16px;
      line-height: 1.4;
      padding-left: 15px;
      padding-top: 0;
      position: relative;

      :before {
        content: "\f10d";
        font-family: FontAwesome;
        font-size: 32px;
        font-style: normal;
        font-weight: normal;
        left: -27px;
        position: absolute;
        text-decoration: inherit;
        top: -27px;
      }
    }
  }
`
const Icon = styled(FontAwesomeIcon)`
    color: ghostwhite;
    margin-left: 5px;
    margin-right: 5px;
    margin-top: 3px;
`
const Foto = styled.img`
  height: auto;
  width: 100%;
`
export type TemplateProps = {
  carta: Carta,
  masVisto: GraphqlArray<Noticia>
}

const TemplateCarta = (props: TemplateProps) => {
  const PostContent = HTMLContent || Content
  const cartaUrl = 'https://3dnoticias.com.ar' + props.carta.fields?.slug
  const data = useStaticQuery(graphql`
    {
      publicidades: allMarkdownRemark(sort: {order: DESC, fields: [frontmatter___date]}, filter: {fileAbsolutePath: {regex: "/src/pages/publicidades/"}}) {
        nodes {
          id
          frontmatter {
            url
            name
            thumbnail
            date
          }
        }
      }
    }
  `)
  const publicidades: Publicidad[] = [...data.publicidades.nodes].sort(() => Math.random() - 0.5)
  if (publicidades.length === 1) {
    publicidades.push(...publicidades)
  }
  const adSenseSlots = ['5967380078', '9965484770', '2366884710']
  return (
    <>
      <CContainer>
        <Categoria>
          <Icon icon={faHashtag} />
          Nuestros lectores
        </Categoria>
      </CContainer>
      <Titulo>
        {props.carta.frontmatter.title}
      </Titulo>
      <Row>
        <Col lg={8}>
          <Entradilla>
            {props.carta.frontmatter.entradilla}
          </Entradilla>
          <Foto src={props.carta.frontmatter.thumbnail} alt={props.carta.frontmatter.title} />
          <Fecha>
            {' '}
            {new Date(props.carta.frontmatter.date).toLocaleDateString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' })}
          </Fecha>
          <Contenido>
            <PostContent content={props.carta.html} />
          </Contenido>
          <Cita>
            {props.carta.frontmatter.author}
          </Cita>
          <ShareButtons link={cartaUrl} />
          <Comments post={props.carta} />
        </Col>
        <Col lg={4}>
          {(publicidades.length > 0) && <Ad ad={publicidades[0]} />}
          <StyledAd
            client='ca-pub-7785551334553916'
            slot={adSenseSlots[1]}
            style={{
              display: 'block',
              height: 290,
            }}
          />
          {typeof window !== 'undefined' &&
            <script>
              {`
              try {
                (window.adsbygoogle = window.adsbygoogle || []).push({});
              } catch (error) {    
              }              
            `}
            </script>
          }
          <MasVisto2 noticias={props.masVisto} />
          <StyledAd
            client='ca-pub-7785551334553916'
            slot={adSenseSlots[0]}
            style={{
              display: 'block',
              height: 290,
            }}
          />
          {typeof window !== 'undefined' &&
            <script>
              {`
              try {
                (window.adsbygoogle = window.adsbygoogle || []).push({});
              } catch (error) {    
              }              
            `}
            </script>
          }
          {(publicidades.length > 0) && <Ad ad={publicidades[1]} />}
        </Col>
      </Row>
    </>
  )
}

export default TemplateCarta
