
import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Timeline } from 'react-twitter-widgets'
import styled from 'styled-components'

import NoticiaCard from '~src/components/molecules/Home/NoticiaCard'
import { Ad } from '~src/components/molecules/Publicidades'
import { Publicidad } from '~src/types/Publicidad'

const GeneralContainer = styled(Container)`
    display: flex;
    flex-direction: row;
    height: 100%;
    padding-top: 5px;
    width: 100%;

    @media (max-width: 1080px) {
      padding-left: 5%;
      padding-right: 5%;
    }
`
const StyledCol = styled(Col)`
    margin-bottom: 30px;
    @media (max-width: 991px) {
      margin-bottom: 30px;

      :last-child {
        margin-bottom: 0;
      }
    }
`

const HomepageCards = () => {
  const data = useStaticQuery(graphql`
    {
      noticias: allMarkdownRemark(sort: {order: DESC, fields: [frontmatter___date]}, filter: {frontmatter: {category: {in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Estado Real", "Deportes", "Cultura"]}} } limit: 7) {
      nodes {
        excerpt(pruneLength: 400)
        id
        fields {
          slug
        }
        frontmatter {
          title
          layout
          url
          author
          entradilla
          thumbnail
          category
          date(formatString: "MMMM DD, YYYY")
          link_del_video
        }
      }
    }
    publicidades: allMarkdownRemark(sort: {order: DESC, fields: [frontmatter___date]}, filter: {fileAbsolutePath: {regex: "/src/pages/publicidades/"}}) {
      nodes {
        id
        frontmatter {
          url
          name
          thumbnail
          date
        }
      }
    }
  }
  `)
  const noticias: Noticia[] = data.noticias.nodes
  const publicidades: Publicidad[] = [...data.publicidades.nodes].sort(() => Math.random() - 0.5)
  if (publicidades.length > 0) {
    while (publicidades.length < noticias.length) {
      publicidades.push(...publicidades)
    }
  }

  return (
    <GeneralContainer>
      <Row>
        {noticias.map((noticia, idx) => (
          <React.Fragment key={idx}>
            <StyledCol lg={4} sm={6} key={idx}>
              <NoticiaCard noticia={noticia} />
            </StyledCol>
            {idx === 1 && <Col lg={4} sm={6}>
              <Timeline
                dataSource={{
                  sourceType: 'profile',
                  screenName: '3DNoticiasSF',
                }}
                options={{
                  height: '500',
                  lang: 'es',
                }}
              />
            </Col>}
            {idx === 3 && (
              <Col lg={4} sm={6}>
                <Ad ad={publicidades[0]} firstAd />
                <Ad ad={publicidades[1]} />
              </Col>
            )}
          </React.Fragment>
        ))}
      </Row>
    </GeneralContainer>
  )
}
export default HomepageCards
