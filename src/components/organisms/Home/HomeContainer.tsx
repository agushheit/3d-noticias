
import React from 'react'
import styled from 'styled-components'

import CardsTitle from '~src/components/atoms/Home/CardsTitle'
import HomepageCards from '~src/components/organisms/Home/HomepageCards'
import LastOpinions from '~src/components/organisms/Home/LastOpinions'

const GeneralContainer = styled.div`
    border-top: 1px solid rgba(0, 0, 0, 0.1);
    display: flex;
    flex-direction: column;
    height: fit-content;
    margin-left: auto;
    margin-right: auto;
    margin-top: 5px;
    max-width: 1140px;
    padding-left: 0 !important;
    padding-right: 0 !important;
    padding-top: 20px;
    width: 100%;
`

const HomeContainer = () => (
  <GeneralContainer>
    <CardsTitle/>
    <HomepageCards />
    <LastOpinions />
  </GeneralContainer>
)

export default HomeContainer
