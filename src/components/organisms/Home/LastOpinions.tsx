import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

import OpinionRecomendada from '~src/components/molecules/Home/OpinionRecomendada'

const StyledContainer = styled(Container)`
  border-top: 1px solid rgba(0, 0, 0, 0.1);
`
const Titulo = styled.h2`
    color: #CF0000;
    font-size: 27px;
    font-weight: bold;
    margin-top: 10px;
    text-transform: uppercase;
    width: 100%;
`

const LastOpinions = () => {
  const { allMarkdownRemark: ultimasOpiniones } = useStaticQuery(graphql`
    {
      allMarkdownRemark(limit: 4, filter: {frontmatter: {category: {eq: "analisis"}}}, sort: {fields: frontmatter___date, order: DESC}) {
        nodes {
          fields {
            slug
          }
          frontmatter {
            title
            author
            thumbnail
          }
        }
      }
    }
  `)
  return (
    <StyledContainer>
      <Titulo>Opinión</Titulo>
      <Row>
        {ultimasOpiniones.nodes.map((node: any, index: number) => (
          <Col md={3} sm={6} key={index}>
            <OpinionRecomendada opinion={node} pageNumber={index + 1}/>
          </Col>
        ))}
      </Row>
    </StyledContainer>
  )
}

export default LastOpinions
