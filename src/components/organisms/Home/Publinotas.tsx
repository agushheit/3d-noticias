import { graphql, useStaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

const GeneralContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
  margin-top: 15px;
  width: 100%;
`
const TextContainer = styled.div`
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  display: flex;
  flex-direction: column;
  height: 100%;
`
const Ad2 = styled(Img) <FluidProps>`
  border-radius: 0.25rem !important;
  height: auto;
  max-width: 100%;
`
const Title = styled.h3`
  box-sizing: border-box;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 1.7rem;
  font-weight: 800;
  line-height: 1.2;
  margin-bottom: 0.5rem;
  margin-top: 0;
  text-align: left;
  text-transform: uppercase;
`
const Description = styled.p`
  box-sizing: border-box;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 1.2rem;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 1rem;
  margin-top: 0;
  text-align: left;
`

const UnorderedList = styled.ul`
  li {
    box-sizing: border-box;
    color: #212529;
    font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
    font-size: 1.2rem;
    font-weight: 400;
    line-height: 1.2;
  }
`
const Aclaracion = styled.p`
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-weight: 400;
  line-height: 1.2;
  margin-top: 15px;
  text-align: left;
`
const Linea = styled.hr`
  border-top: 1px solid rgba(0, 0, 0, 0.1);
`
const Publinotas = () => {
  const { imageSharp } = useStaticQuery(graphql`
    query {
      imageSharp(fluid: {originalName: {eq: "D1.jpg"}}) {
        id
        fluid(quality: 100, maxWidth: 700) {
          ...GatsbyImageSharpFluid_withWebp_noBase64
        }
      }
    }
  `)
  return (
    <GeneralContainer>
      <Container>
        <Linea />
        <Row>

          <Col lg={6}>
            <TextContainer>
              <Title>Espacio para publinotas</Title>
              <Description>
                Publicaciones en formato noticia, con fines publicitarios y de promoción específicos.
                Se publican en el sitio web en forma programada y se muestran en la portada del sitio
                junto con las últimas noticias durante el tiempo pactado. Poseen un tratamiento visual
                especial, con alto nivel de visibilidad.
              </Description>
              <UnorderedList>
                <li>
                  <strong>La empresa &quot;X&quot; lanzó su campaña de concientización *</strong>
                </li>
                <li>Recomendaciones</li>
                <li>Productos</li>
                <li>Servicios</li>
                <li>Su forma de trabajo</li>
              </UnorderedList>
              <Description>
                El contenido de las publinotas puede ser enviado por el cliente o generado por 3DNoticias
              </Description>
              <Aclaracion>* Por J. Gomez - CEO cofunder Empresa X</Aclaracion>
            </TextContainer>
          </Col>

          <Col lg={6}>
            <Ad2 fluid={imageSharp.fluid} alt='Publicidad en forma de noticia' />
          </Col>
        </Row>

      </Container>
    </GeneralContainer>
  )
}
export default Publinotas
