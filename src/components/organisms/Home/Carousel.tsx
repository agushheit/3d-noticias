
import { graphql, Link, useStaticQuery } from 'gatsby'
import React from 'react'
import { Carousel } from 'react-bootstrap'
import styled, { css } from 'styled-components'

const Titulo = styled.h1`
    box-sizing: border-box;
    color: #fff;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 1.8rem;
    font-weight: 800;
    line-height: 1.2;
    margin-bottom: 0.5rem;
    margin-top: 0;
    text-align: center;
    text-shadow: 0px 0px 6px #000;
    text-transform: uppercase;
`

const Descripcion = styled.p`
  background-color: black;
  box-sizing: border-box;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 1.2rem;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 1rem;
  margin-top: 0;
  opacity: .5;
  text-align: center;

  @media screen and (max-width: 768px) {
      display: none;
    }
`

interface ImgProps {
  image: string
}

const ImgContainer = styled.div<ImgProps>`
  background-position: center;
  background-size: cover;
  cursor: pointer;
  height: 500px;
  width: 100%;

  ${props => css`
    background-image: url(${props.image});
  `}
`
const HomeCarousel = () => {
  const { ultimasNoticias } = useStaticQuery(graphql`
    {
      ultimasNoticias: allMarkdownRemark(limit: 3, filter: {frontmatter: {category: {in: ["Estado Real", "Agenda Ciudadana", "Deportes", "El Campo", "La Ciudad"]}, layout: {eq: "Noticia con imagen"}}}, sort: {fields: frontmatter___date, order: DESC}) {
        nodes {
          fields {
            slug
          }
          frontmatter {
            resumen
            entradilla
            thumbnail
            layout
          }
        }
      }
    }
  `)
  return (
    <Carousel>
      {ultimasNoticias.nodes.map((noticia: Noticia, key: number) => (
        <Carousel.Item key={key}>
          <Link to={noticia.fields.slug}>
            <ImgContainer image={noticia.frontmatter.thumbnail}>
              <Carousel.Caption>
                <Titulo>
                  {noticia.frontmatter.resumen}
                </Titulo>
                <Descripcion>
                  {noticia.frontmatter.entradilla}
                </Descripcion>
              </Carousel.Caption>
            </ImgContainer>
          </Link>
        </Carousel.Item>
      ))}
    </Carousel>
  )
}

export default HomeCarousel
