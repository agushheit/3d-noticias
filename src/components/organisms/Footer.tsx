import { graphql, Link, useStaticQuery } from 'gatsby'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

import FooterMediaContainer from '~src/components/molecules/FooterMediaContainer'
import LogoImg from '~src/images/Logo.png'

const FooterContainer = styled.div`
    background-color: transparent;
    color: white;
    display: flex;
    flex-direction: column;
    width: 100%;
`
const Logo = styled.div`
    background-image: url(${LogoImg});
    background-size: cover;
    height: 35px;
    margin-bottom: 15px;
    width: 170px;
`
const GeneralContainer = styled.div`
    border-top: 1px solid white;
    display: flex;
    flex-direction: row;
    max-width: 1140px;
    width: 100%;

    .row {
      width: 100%;
    }
`
const InfoContainer = styled.div`
    height: fit-content;
    margin-top: 20px;
`
const Info = styled.h3`
    color: white;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 14.4px;
    font-weight: normal;
    line-height: 1.2;
    text-align: left;
`
const Brand = styled.h4`
    color: white;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 12px;
    font-weight: normal;
    line-height: 1.2;
    text-align: left;
`
const ContactContainer = styled.div`
  height: fit-content;
  margin-top: 20px;

  h6, h5 {
    box-sizing: border-box;
    color: #fff !important;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 14.4px;
    font-weight: normal;
    line-height: 1.2;
    margin: 0.25rem !important;
    text-align: left !important;
    cursor: pointer;
  }

  p {
    box-sizing: border-box;
    color: #fff !important;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 11.5px;
    font-weight: normal;
    line-height: 1.2;
    margin: 0.25rem !important;
    text-align: left !important;
  }
  
  a {
    color: white;

    &:hover {
      color: white;
      text-decoration: none;
    }
  }
`

const Footer = () => {
  const data = useStaticQuery(graphql`
    query {
      currentBuildDate {
        currentDate
      }
    }
  `)
  const lastBuildDateTime = new Date(data.currentBuildDate.currentDate)
  const lastBuildDate = lastBuildDateTime.toLocaleDateString('es-AR')
  const lastBuildTime = lastBuildDateTime.toLocaleTimeString('es-AR')

  return (
    <FooterContainer>
      <Container>
        <Logo />
        <GeneralContainer>
          <Row>
            <Col lg={4} sm={6}>
              <InfoContainer>
                <Info>
                3DNoticias es una Productora Local de Contenidos:
                  <br />
                  Hacemos periodismo desde Santa Fe, Argentina.
                </Info>
                <Brand>
                Copyright © 3DNoticias 2020
                </Brand>
              </InfoContainer>
            </Col>
            <Col lg={4} sm={6}>
              <FooterMediaContainer />
            </Col>
            <Col lg={4} sm={6}>
              <ContactContainer>
                <h5>
                  <a
                    href="mailto:comercial@3dnoticias.ar"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                  ¿Cómo anunciar?
                  </a>
                </h5>
                <h6>
                  <Link to='/politicas-de-privacidad'>
                    Políticas de privacidad
                  </Link>
                </h6>
                <h6>
                  <a href="mailto:info@3dnoticias.ar?subject=Consulta%20desde%20el%20sitio%20web%203DNoticias&amp;body=Escriba%20su%20consulta.%20Le%20responderemos%20a%20la%20brevedad.">
                    info@3dnoticias.ar
                  </a>
                </h6>
                <p>
                  <span>Última Actualización: </span>
                  <span>
                    {lastBuildDate}
                  </span>
                  <span> - Hora: </span>
                  <span>
                    {lastBuildTime}
                  </span>
                </p>
              </ContactContainer>
            </Col>
          </Row>
        </GeneralContainer>
      </Container>
    </FooterContainer>
  )
}
export default Footer
