/* eslint-disable camelcase */

import { graphql, useStaticQuery } from 'gatsby'
import Img, { FluidObject, GatsbyImageFluidProps, GatsbyImageProps } from 'gatsby-image'
import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'

import { State } from '~src/constants/state'

const FirstDiv = styled.div`
    display: flex;
    flex-direction: row;
    margin-left: 30px;
`
const SecondDiv = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 10px;
    margin-right: 10px;
    margin-top: 10px;
  `
const ThirdDiv = styled.div`
    display: flex;
    flex-direction: column;
    text-align: center;
  `
const Temperatura = styled.strong`
  color: white;
  font-family: Arial,Verdana,Arial,Helvetica,sans-serif;
  font-size: 20px;
  line-height: 20px;
`

const Viento = styled.p`
    color: white;
    font-family: Arial,Verdana,Arial,Helvetica,sans-serif;
    font-size: 10px;
    line-height: 10px;
    text-align: right;
  `
const Dia = styled.p`
    color: white;
    font-family: Arial,Verdana,Arial,Helvetica,sans-serif;
    font-size: 11px;
    line-height: 11px;
    margin-top: 5px;
  `
const MaxMinTemp = styled.span`
    color: white;
    font-family: Arial,Verdana,Arial,Helvetica,sans-serif;
    font-size: 10px;
    margin-top: -3px;
  `
const Icon = styled(Img)<GatsbyImageProps>`
    align-self: center;
    margin: -13px 0 5px;
    width: 20px;
  `

interface WidgetImage extends FluidObject {
  relativePath: string
  childImageSharp: GatsbyImageFluidProps
}

const WidgetClima = () => {
  const data: { images: GraphqlArray<WidgetImage> } = useStaticQuery(graphql`
    query WeatherImages {
      images: allFile(filter: {dir: {regex: "/src/images/weather/"}}) {
        edges {
          node {
            childImageSharp {
              fluid(maxWidth: 50, maxHeight: 56) {
                ...GatsbyImageSharpFluid_withWebp_noBase64
              }
            }
            dir
            name
            relativePath
          }
        }
      }
  }
  `)
  const images = data.images.edges.map(edge => edge.node)
  const weatherData = useSelector((state: State) => state.weatherData)
  const imagenClimaDelDia = images.find(image => image.relativePath === weatherData?.imagePathClimaDelDia) || images[0]
  const imagenClimaAhora = images.find(image => image.relativePath === weatherData?.imagePathClimaAhora) || images[0]
  return (
    <FirstDiv>
      <Img fluid={imagenClimaAhora?.childImageSharp.fluid} style={{ width: 50 }}/>
      <SecondDiv>
        <Temperatura>
          {weatherData?.temperatura + '°C'}
        </Temperatura>
        <Viento>
          {weatherData?.viento + ' km/h'}
        </Viento>
      </SecondDiv>
      <ThirdDiv>
        <Dia>Hoy</Dia>
        <Icon fluid={imagenClimaDelDia?.childImageSharp.fluid} />
        <MaxMinTemp>
          {`${weatherData?.temperaturaMaxima}° | ${weatherData?.temperaturaMinima}°`}
        </MaxMinTemp>
      </ThirdDiv>
    </FirstDiv>
  )
}

export default WidgetClima
