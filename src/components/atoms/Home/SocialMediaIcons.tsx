import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF'
import { faInstagram } from '@fortawesome/free-brands-svg-icons/faInstagram'
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter'
import { faYoutube } from '@fortawesome/free-brands-svg-icons/faYoutube'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import styled from 'styled-components'

const GeneralContainer = styled.div`
    align-content: flex-end;
    align-items: flex-end;
    align-self: flex-end;
    display: flex;
    flex-direction: row;
    height: fit-content;
    margin-left: 60%;
    margin-right: 16.6%;
    text-align: right;
    width: fit-content;

    @media (max-width: 870px) {
      margin-left: 45%;
    }
    @media (max-width: 700px) {
      margin-left: 40%;
    }
    @media (max-width: 575px) {
      margin-left: 35%;
    }
    @media (max-width: 530px) {
      margin-left: 30%;
    }
    @media (max-width: 500px) {
      margin-left: 20%;
    }
    @media (max-width: 430px) {
      margin-left: 10%;
    }
    @media (max-width: 380px) {
      margin-left: 5%;
    }
    @media (max-width: 360px) {
      margin-left: 2%;
    }
    @media (max-width: 350px) {
      margin-left: 10%;
    }
    @media (max-width: 310px) {
      margin-left: 5%;
    }
`
const IconStack = styled.a`
    background-color: black;
    border-radius: 20px;
    cursor: pointer;
    height: 38px;
    margin-right: 10px;
    opacity: 0.8;
    text-align: center;
    transition: 0.3s;
    width: 38px;

    :hover{
        background-color: ${props => props.color};
    }
`
const Icon = styled(FontAwesomeIcon)`
    color: white;
    font-size: 19px !important;
    margin-top: 9px;
`
const SocialMediaIcons = () => {
  return (
    <GeneralContainer>
      <IconStack color='#00acee' href='https://twitter.com/3DNoticiasSF' target='_blank' rel='noopener noreferrer' aria-label="Visitanos en Twitter">
        <Icon icon={faTwitter} aria-hidden />
      </IconStack>
      <IconStack color="#3b5998" href='https://www.facebook.com/3DNoticiasSF/' target='_blank' rel='noopener noreferrer' aria-label='Visitanos en Facebook'>
        <Icon icon={faFacebookF} aria-hidden/>
      </IconStack>
      <IconStack color='#E501A4' href='https://www.instagram.com/3dnoticiassf' target='_blank' rel='noopener noreferrer' aria-label='Visitanos en Instagram'>
        <Icon icon={faInstagram} aria-hidden/>
      </IconStack>
      <IconStack color='#ff0000' href='https://www.youtube.com/channel/UC1HgY6jmNxBgzORiFdoiB5w/' target='_blank' rel='noopener noreferrer' aria-label='Visitanos en YouTube'>
        <Icon icon={faYoutube} aria-hidden/>
      </IconStack>
      <IconStack color='#cc0000' href='mailto:info@3dnoticias.ar' target='_blank' rel='noopener noreferrer' aria-label='Envianos un mail'>
        <Icon icon={faEnvelope} aria-hidden/>
      </IconStack>
    </GeneralContainer>
  )
}

export default SocialMediaIcons
