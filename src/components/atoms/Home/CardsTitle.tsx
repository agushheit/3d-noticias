
import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  display: flex;
  flex-direction: row;
  height: fit-content;
  margin-bottom: 10px;
  padding-bottom: 5px;
  width: 100%;

  @media (max-width: 1080px) {
    padding: 0 5%;
  }
`
const Title = styled.h2`
    color: #CF0000;

    font-size: 27px;
    font-weight: bold;
    text-transform: uppercase;
    width: 100%;
`
const AgendaCardsTitle = () => (
  <Container>
    <Title>
        Últimas noticias
    </Title>
  </Container>
)

export default AgendaCardsTitle
