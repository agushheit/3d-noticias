import React from 'react'
import { Spinner } from 'react-bootstrap'
import styled from 'styled-components'

const StyledSpinner = styled(Spinner)`
height: 4rem;
left: 50%;
margin-top: 25vh;
position: relative;
width: 4rem;
`

const Fallback = (
  <StyledSpinner variant="primary" animation="border" role="status">
    <span className="sr-only">Cargando...</span>
  </StyledSpinner>
)

export default Fallback
