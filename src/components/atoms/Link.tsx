/* eslint-disable react/prop-types */
import { Link } from 'gatsby'
import React from 'react'
import styled from 'styled-components'

const StyledLink = styled(Link)`
  :hover {
    text-decoration: none !important;
  }
`

type Props = {
  disabled?: boolean,
  children: React.ReactNode,
  to: string
}

const PaginationLink = (props: Props) => {
  return props.disabled
    ? (
      <span>
        {props.children}
      </span>
    )
    : (
      <StyledLink to={props.to}>
        {props.children}
      </StyledLink>
    )
}

export default PaginationLink
