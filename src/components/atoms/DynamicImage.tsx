import styled, { css } from 'styled-components'

export default styled.div<{image: string, height: number}>`
  background-position: center;
  background-size: cover;
  cursor: pointer;
  width: 100%;

  ${props => css`
    background-image: url(${props.image});
    height: ${props.height}px;
  `}
`
