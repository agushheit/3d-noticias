// @flow

import * as React from 'react'
import { Dropdown } from 'react-bootstrap'
import DropdownItem from 'react-bootstrap/esm/DropdownItem'
import DropdownMenu from 'react-bootstrap/esm/DropdownMenu'
import DropdownToggle from 'react-bootstrap/esm/DropdownToggle'
import styled from 'styled-components'

const Drop = styled(DropdownToggle)`
  background-color: transparent !important;
  border: none;
  color: rgba(255, 255, 255, 0.75);
  font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';
  font-size: 14.4px;
  font-style: normal;
  height: 33px;
  line-height: 14.4px;
  list-style: none;
  padding: 8px;
  text-align: left;
  text-decoration: none;

  &:hover, &:active, &:focus {
      background-color: #CC0000 !important;
      border: none !important;
      box-shadow: none !important;
      color: rgba(255, 255, 255, 0.95);
  }


  @media (max-width: 991px) {
      padding: 8px 16px;
  }
`

const Item = styled(DropdownItem)`
  font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';
  font-size: 14.4px;
`

const Dropdow = () => {
  return (
    <Dropdown>
      <Drop>
      Opinión
      </Drop>

      <DropdownMenu>
        <Item href='/opinion'>
          Análisis y opinión
        </Item>
        <Item href='/nuestros-lectores'>
          Cartas de lectores
        </Item>
      </DropdownMenu>
    </Dropdown>
  )
}

export default Dropdow
