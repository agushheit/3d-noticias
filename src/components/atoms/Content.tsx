import React from 'react'

export const HTMLContent = (props: { content: string, className?: string }) => (
  <div className={props.className} dangerouslySetInnerHTML={{ __html: props.content }} />
)

const Content = (props: { content: string, className?: string }) => (
  <div className={props.className}>
    {props.content}
  </div>
)

export default Content
