import { IconProp } from '@fortawesome/fontawesome-svg-core'
import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF'
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter'
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import styled from 'styled-components'

import { useClientSideRendering } from '~src/constants/utils'
import Print from '~src/images/Print.svg'

const StyledContainer = styled.div`
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    border-top: 1px solid rgba(0, 0, 0, 0.1);
    display: flex;
    flex-direction: row;
    height: fit-content;
    padding-bottom: 10px;
    padding-top: 10px;
    width: auto;
`
const Icon = styled(FontAwesomeIcon)`
    color: white;
    font-size: 19px !important;
    margin-top: 9px;
    transition: 0.3s;
`
const Icon2 = styled(Print)`
    fill: white;
    font-size: 19px !important;
    margin-top: 9px;
    transition: 0.3s;
`
const IconStack = styled.a`
    background-color: ${props => props.color};
    border-radius: 4px;
    cursor: pointer;
    height: 38px;
    margin-right: 10px;
    position: relative;
    text-align: center;
    top: 0;
    transition: 0.3s;
    white-space: nowrap;
    width: 38px;

    :last-child {
      margin-right: 0;
    }

    &:hover {
      opacity: .8;
      top: -4px;
    }
`

type ButtonProps = {
  link: string
  color: string
  icon: IconProp
}

const ShareButton = (props: ButtonProps) => (
  <IconStack color={props.color} rel="noreferrer" target="_blank" href={props.link}>
    <Icon icon={props.icon} />
  </IconStack>
)

type Props = {
  link: string
}

const print = () => useClientSideRendering(() => window.print())

const ShareButtons = (props: Props) => (
  <StyledContainer>
    <ShareButton
      color='#4267B2'
      icon={faFacebookF}
      link={`https://www.facebook.com/sharer/sharer.php?u=${props.link}`}
    />
    <ShareButton
      color='#55acee'
      icon={faTwitter}
      link={`https://twitter.com/intent/tweet?text=${props.link}`}
    />
    <ShareButton
      color='#7d7d7d'
      icon={faEnvelope}
      link={`mailto:?subject=3D Noticias&body=${props.link}`}
    />
    <IconStack
      color='#222222'
      onClick={print}
      onKeyPress={print}
    >
      <Icon2 />
    </IconStack>
    <ShareButton
      color='#25d366'
      icon={faWhatsapp}
      link={`https://wa.me/?text=${props.link}`}
    />
  </StyledContainer>
)

export default ShareButtons
