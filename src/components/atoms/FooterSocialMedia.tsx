import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF'
import { faInstagram } from '@fortawesome/free-brands-svg-icons/faInstagram'
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter'
import { faYoutube } from '@fortawesome/free-brands-svg-icons/faYoutube'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    flex-direction: row;
    height: fit-content;
    width: auto;
`

const Icon = styled(FontAwesomeIcon)`
    color: black;
    font-size: 19px !important;
    margin-top: 9px;
    transition: 0.3s;
`

const IconStack = styled.a`
    background-color: #F4F6F9;
    border-radius: 20px;
    cursor: pointer;
    height: 38px;
    margin-right: 15px;
    opacity: 0.8;
    text-align: center;
    transition: 0.3s;
    width: 38px;

    :last-child {
      margin-right: 0;
    }

    &:hover {
        background-color: #FFFFFF !important;
        opacity: 1;

        ${Icon} {
          color: ${props => props.color};
          
        }
    }
`

const FooterSocialMedia = () => (
  <Container>
    <IconStack href='https://twitter.com/3DNoticiasSF' target='_blank' color='#00acee' rel='noopener noreferrer' aria-label='Visitanos en Twitter'>
      <Icon icon={faTwitter} aria-hidden />
    </IconStack>
    <IconStack href='https://www.facebook.com/3DNoticiasSF/' target='_blank' color="#3b5998" rel='noopener noreferrer' aria-label='Visitanos en Facebook'>
      <Icon icon={faFacebookF} aria-hidden />
    </IconStack>
    <IconStack href='https://www.instagram.com/3dnoticiassf' target='_blank' color='#E501A4' rel='noopener noreferrer' aria-label='Visitanos en Instagram'>
      <Icon icon={faInstagram} aria-hidden />
    </IconStack>
    <IconStack href='https://www.youtube.com/channel/UC1HgY6jmNxBgzORiFdoiB5w/' target='_blank' color='#ff0000' rel='noopener noreferrer' aria-label='Visitanos en YouTube'>
      <Icon icon={faYoutube} aria-hidden />
    </IconStack>
  </Container>
)

export default FooterSocialMedia
