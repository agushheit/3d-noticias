import { IconProp } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { graphql, useStaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import React from 'react'
import styled from 'styled-components'

const SearchContainer = styled.div`
  background-color: #353535;
  border-radius: 0.25rem;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  height: 47px;
  justify-content: space-between !important;
  margin-bottom: 15px;
  margin-top: 10px;
  padding: 10px 15px;
`
const TitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: auto;
  width: auto;
`
const Titulo = styled.h2`
    box-sizing: border-box;
    color: #FFFFFF;
    display: flex;
    font-family: Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 19.2px;
    font-weight: bolder;
    line-height: 1.2;
    list-style: none;
    text-transform: uppercase;
    
    @media (max-width: 380px) {
      font-size: 15px;
    }
`
const Icon = styled(FontAwesomeIcon)`
    color: #FFFFFF;
    font-size: 19.2px;
    margin-right: 10px;
    margin-top: 2px;

    @media (max-width: 340px) {
      font-size: 15px;
    }

`
const Icono = styled(Img)<FluidProps>`
    color: #FFFFFF;
    height: 25px;
    width: 25px;
`
type Props = {
  icono: IconProp
  titulo: string
}

const SearchBar = (props: Props) => {
  const { imageSharp } = useStaticQuery(graphql`
    query {
      imageSharp(fixed: {originalName: {eq: "logosearchbar.png"}}) {
        fixed(quality: 100, width: 25) {
          ...GatsbyImageSharpFixed_withWebp_noBase64
        }
      }
    }
  `)
  // const fotitos = allImageSharp.edges.map((pic: { node: any }) => pic.node)
  return (
    <SearchContainer>
      <TitleContainer>
        <Icon icon={props.icono} />
        <Titulo>
          {props.titulo}
        </Titulo>
      </TitleContainer>
      <Icono fixed={imageSharp.fixed} alt='icono' />
    </SearchContainer>
  )
}

export default SearchBar
