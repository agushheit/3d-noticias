import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import { Helmet } from 'react-helmet'

import { usePageView } from '~src/constants/utils'
import favicon from '~src/images/favicon.ico'

type Props = {
  description?: string,
  lang?: string,
  tabTitle?: string,
  seoTitle: string,
  imagePath?: string,
  imageWidth?: string,
  imageHeight?: string,
  urlPath?: string,
  ogType?: string,
  noBaseUrl?: boolean
}

const Seo = ({
  description = '',
  lang = 'es',
  tabTitle,
  seoTitle,
  imagePath = '',
  urlPath = '',
  imageWidth,
  imageHeight,
  ogType,
  noBaseUrl,
}: Props): React.ReactElement => {
  const { site } = useStaticQuery(graphql`
      query SiteData {
        site {
          siteMetadata {
            title
            description
          }
        }
      }
    `,
  )
  const baseUrl = noBaseUrl ? '' : 'https://3dnoticias.com.ar'
  const image = `${baseUrl}${imagePath}`
  const metaUrl = `${baseUrl}${urlPath}`
  const metaDescription = description || site.siteMetadata.description
  usePageView()
  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      title={tabTitle}
      defaultTitle='3D Noticias'
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      link={[
        {
          rel: 'icon',
          type: 'image/png',
          href: favicon,
        },
      ]}
      meta={[
        // HTML Meta Tags
        {
          name: 'description',
          content: metaDescription,
        },

        // Google / Search Engine Tags
        {
          content: seoTitle,
          itemProp: 'name',
        },
        {
          content: description,
          itemProp: 'description',
        },
        {
          content: image,
          itemProp: 'image',
        },
        // Facebook / OpenGraph Meta Tags
        {
          property: 'og:url',
          content: metaUrl,
        },
        {
          property: 'og:type',
          content: ogType ?? 'website',
        },
        {
          property: 'og:title',
          content: seoTitle,
        },
        {
          property: 'og:description',
          content: description,
        },
        {
          property: 'og:image',
          content: image,
        },
        {
          property: 'og:image:secure_url',
          content: image,
        },
        {
          property: 'og:site_name',
          content: site.siteMetadata.title,
        },
        {
          property: 'og:image:type',
          content: `image/${imagePath.split('.').pop()}`,
        },
        {
          property: 'og:image:width',
          content: imageWidth,
        },
        {
          property: 'og:image:height',
          content: imageHeight,
        },
        {
          property: 'og:locale',
          content: 'es_LA',
        },
        // Twitter Meta Tags
        {
          name: 'twitter:card',
          content: 'summary_large_image',
        },
        {
          name: 'twitter:title',
          content: seoTitle,
        },
        {
          name: 'twitter:description',
          content: description,
        },
        {
          name: 'twitter:image',
          content: image,
        },
      ]}
    />
  )
}

export default Seo
