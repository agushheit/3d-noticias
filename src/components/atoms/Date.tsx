
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons/faMapMarkerAlt'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

const FechaContainer = styled.div`
    display: flex;
    flex-direction: row;
    height: fit-content;
    margin-top: 10px;
`
const Fecha = styled.h2`
    box-sizing: border-box;
    color: #212529;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 0.9rem;
    font-weight: normal;
    line-height: 1.2;
    margin-bottom: 0.5rem;
    text-align: left;
`
const Icon = styled(FontAwesomeIcon)`
    color: black;
    margin-right: 5px;
`
const DateContainer = () => {
  const currentDate = new Date()
  const options = { year: 'numeric', month: 'long', day: 'numeric' }
  const formattedDate = currentDate.toLocaleDateString('es-ES', options)

  return (
    <Container>
      <Row>
        <Col sm={7}>
          <FechaContainer>
            <Icon icon={faMapMarkerAlt} size='sm' />
            <Fecha>
              SANTA FE, ARGENTINA.
              <br/>
              {formattedDate}
            </Fecha>
          </FechaContainer>
        </Col>
      </Row>
    </Container>
  )
}

export default DateContainer
