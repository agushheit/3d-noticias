import AdSense from 'react-adsense'
import styled from 'styled-components'

import TuPublicidadAca from '~src/images/tu-publicidad-aca.jpeg'

const StyledAd = styled(AdSense.Google)`
  background-image: url(${TuPublicidadAca});
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
`

export default StyledAd
