import React from 'react'
import styled from 'styled-components'

const Autor = styled.blockquote`
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
`
type Props = {
  children: string
}

const Cita = ({ children }: Props) => (
  <Autor className="blockquote">
    <footer className="blockquote-footer">
      <cite title="Source Title">
        {children}
      </cite>
    </footer>
  </Autor>
)

export default Cita
