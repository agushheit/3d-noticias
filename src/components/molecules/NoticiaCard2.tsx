import { faAngleRight } from '@fortawesome/free-solid-svg-icons/faAngleRight'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Img from 'gatsby-image'
import React from 'react'
import { Button, Card } from 'react-bootstrap'
import ReactPlayer from 'react-player'
import styled from 'styled-components'

import DynamicImage from '~src/components/atoms/DynamicImage'
import Link from '~src/components/atoms/Link'

const StyledCard = styled(Card)`
    height: 100%;
    margin-bottom: 30px;
    max-height: 590px;
    overflow: hidden;

    .card-body {
        display: flex;
        flex-direction: column;
        padding-left: 10px !important;
    }

    img {
      transition: all 200ms ease-in-out !important;
    }

    ${DynamicImage} {
        cursor: pointer !important;
        transition: 0.3s;

        &:hover {
          filter: saturate(1.5);
          transform: scale(1.2);
        }
    }

    @media (max-width: 1081px) {
      max-height: 670px ;
    }

    @media (max-width: 991px) {
      max-height: 600px;
    }

    @media (max-width: 767px) {
      max-height: 750px;
    }

    @media (max-width: 500px) {
      max-height: 800px;
    }
`
const Title = styled.h2`
    box-sizing: border-box;
    color: #CF0000 !important;
    cursor: pointer;
    font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
    font-size: 20px;
    font-weight: 800;
    line-height: 1.2;
    margin-bottom: 0.75rem;
    margin-top: 0;
    text-align: left;
    text-decoration: none !important;

    :hover {
        color: #FF0000 !important;
    }
`
const Fecha = styled.span`
    box-sizing: border-box;
    color: #212529;
    font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
    font-size: 15.4px;
    font-weight: 400;
    line-height: 1.2;
    text-align: left;
`
const Text = styled.p`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 17px;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 0;
  margin-top: 0;
  text-align: left;
`

const Description = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  justify-content: space-between;
`
const StyledButton = styled(Button)`
  background-color: transparent;
  border: 1px solid #cc0000;
  color: #cc0000;
  font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';
  transition: all 0.15s;
  width: 165px;

  :hover {
    background-color: #cc0000;
    color: white;
  }
`
const Icon = styled(FontAwesomeIcon)`
    font-size: 16px;
    margin-left: 3px;
`
const StyledDiv = styled.div`
  height: 230px;
  @media (max-width: 1081px) {
      max-height: 670px ;
    }

    @media (max-width: 991px) {
      max-height: 600px;
    }

    @media (max-width: 767px) {
      max-height: 750px;
    }

    @media (max-width: 500px) {
      max-height: 800px;
    }
`

const ImageContainer = styled.div`
  height: 230px;
  overflow: hidden;
  
  img {
    max-width: 100%;
    min-height: 230px;
  }
`

const NoticiaCards = ({ noticia }: { noticia: Noticia }) => {
  return (
    <StyledCard>
      {noticia.frontmatter.layout === 'Noticia con video'
        ? (
          <StyledDiv>
            <ReactPlayer width='100%' height='100%' url={noticia.frontmatter.link_del_video} />
          </StyledDiv>
        )
        : (
          <Link to={noticia?.fields?.slug ?? '/'}>
            <ImageContainer>
              <DynamicImage image={noticia.frontmatter.thumbnail} height={230} />
            </ImageContainer>
            <Img
              fluid={noticia?.localImage?.childImageSharp.fluid}
              style={{ height: 230 }}
            />
          </Link>
        )
      }
      <StyledCard.Body>
        <Link to={noticia?.fields?.slug ?? '/'}>
          <Title>
            {noticia?.frontmatter.title}
          </Title>
        </Link>
        <Description>
          <Text>
            {noticia?.frontmatter.entradilla}
          </Text>
          <Fecha>
            Santa Fe,
            {' '}
            {new Date(noticia?.frontmatter.date).toLocaleDateString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' })}
          </Fecha>
          <Link to={noticia?.fields?.slug ?? '/'}>
            <StyledButton>
              Noticia completa
              <Icon icon={faAngleRight} />
            </StyledButton>
          </Link>
        </Description>
      </StyledCard.Body>
    </StyledCard>
  )
}

export default NoticiaCards
