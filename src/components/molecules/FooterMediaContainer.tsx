import React from 'react'
import styled from 'styled-components'

import FooterSocialMedia from '~src/components/atoms/FooterSocialMedia'

const Container = styled.div`
  background-color: transparent;
  box-sizing: border-box;
  color: #212529;
  height: 48px;
  line-height: 1.2;
  padding-top: 18px;
  width: auto;
`
const FooterMediaContainer = () => {
  return (
    <Container>
      <FooterSocialMedia/>
    </Container>
  )
}

export default FooterMediaContainer
