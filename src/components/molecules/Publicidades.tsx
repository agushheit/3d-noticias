
import React from 'react'
import { Card } from 'react-bootstrap'
import styled, { css } from 'styled-components'

import { useWindowSize } from '~src/constants/utils'
import { Publicidad } from '~src/types/Publicidad'

const AdsContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: space-between;
  max-height: 600px;
  width: 100%;

  @media (min-width: 576px) {
    padding-bottom: 25%;
  }
  @media (max-width: 1081px) {
      max-height: 670px ;
    }

    @media (max-width: 991px) {
      max-height: 600px;
    }

    @media (max-width: 767px) {
      max-height: 750px;
    }

    @media (max-width: 500px) {
      max-height: 800px;
    }
`
type AdProps = {
  src: string
  alt: string
  firstAd?: boolean
}

const AdImage = styled.img<AdProps>`
  border-top-left-radius: calc(.25rem - 1px);
  border-top-right-radius: calc(.25rem - 1px);
  flex-shrink: 0;
  width: 100%;

  ${props => props.firstAd && css`
    margin-bottom: 20px;
  `}
`

const StyledCard = styled(Card)`
    border: none;
`

export const Ad = (props: { ad: Publicidad, firstAd?: boolean }) => {
  return (
    <StyledCard>
      <a href={props.ad.frontmatter.url} target='blank' rel='noopener noreferrer'>
        <AdImage
          src={props.ad.frontmatter.thumbnail}
          alt={props.ad.frontmatter.name}
          firstAd={props.firstAd} />
      </a>
    </StyledCard>
  )
}

type Props = {
  ad1: Publicidad
  ad2: Publicidad
  style?: any
}

const Publicidades = (props: Props) => {
  const windowSize = useWindowSize()
  return (
    <AdsContainer style={props.style}>
      <Ad ad={props.ad1} firstAd />
      {(windowSize?.width ?? 0 > 575) && <Ad ad={props.ad2} />}
    </AdsContainer>
  )
}

export default Publicidades
