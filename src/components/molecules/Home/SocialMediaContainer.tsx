import React from 'react'
import styled from 'styled-components'

import SocialMediaIcons from '~src/components/atoms/Home/SocialMediaIcons'

const GeneralContainer = styled.div`
  background-color: lightgray;
  box-sizing: border-box;
  color: #212529;
  height: 48px;
  line-height: 1.2;
  padding-bottom: 5px;
  padding-top: 5px;
  width: 100%;
`
const SocialMediaContainer = () => {
  return (
    <GeneralContainer>
      <SocialMediaIcons/>
    </GeneralContainer>
  )
}

export default SocialMediaContainer
