import React from 'react'
import { Card } from 'react-bootstrap'
import styled from 'styled-components'

import DynamicImage from '~src/components/atoms/DynamicImage'
import Link from '~src/components/atoms/Link'

const StyledCard = styled(Card)`
  cursor: pointer;
  height: auto;
  max-height: 200px;

  &:hover {
    background-color: #343A40;
    border: 1px solid rgb(255,0,0, 0.3) !important;

    p {
      color: white;
      font-size: 15.6px;
      }

    ${DynamicImage} {
      filter: saturate(1.5);
    }
  }

  @media (max-width: 991px) {
    height: 290px;

    ${DynamicImage} {
      max-height: 133px;
    }
  }
  @media (max-width: 767px) {
    height: 230px;
    margin-bottom: 30px;

    ${DynamicImage} {
      max-height: 190px;
    }
  }
  @media (max-width: 575px) {
    max-height: 160px;
    ${DynamicImage} {
      max-height: 190px;
    }
  }
`
const Titulo = styled.h2`
  background-color: rgba(0, 0, 0, 0.6);
  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
  color: white;

  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 17px;
  font-weight: 600;
  line-height: 25px;
  margin-bottom: 0.5rem;
  position: absolute;
  text-align: center;
  text-shadow: 0px 0px 6px #000;
  top: 25%;

  @media (max-width: 991px) {
    font-size: 13px;
    top: 12%;
  }

  @media (max-width: 767px) {
    font-size: 15px;
    top: 30%;
  }
  @media (max-width: 575px) {
    font-size: 18px;
    top: 25%;
  }
  width: 100%;
`
const Entradilla = styled.p`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 15.3px;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 5px;
  margin-left: 5px;
  margin-top: 5px;
  text-align: left;
  text-indent: 10px;
`

const ImageContainer = styled.div`
  height: 170px;
  overflow: hidden;
  
  img {
    min-height: 170px;
    width: 100%;
  }
`

const OpinionRecomendada = ({ opinion, pageNumber }: { opinion: Analisis, pageNumber: number }) => {
  const url = `/opinion/${pageNumber > 1 ? pageNumber : ''}`
  return (
    <Link to={url}>
      <StyledCard>
        <ImageContainer>
          <DynamicImage image={opinion.frontmatter.thumbnail} height={170} />
        </ImageContainer>
        <Titulo>
          {opinion?.frontmatter.title}
        </Titulo>
        <Entradilla>
          {opinion?.frontmatter.author}
        </Entradilla>
      </StyledCard>
    </Link>
  )
}

export default OpinionRecomendada
