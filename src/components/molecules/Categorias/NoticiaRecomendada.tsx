import React from 'react'
import { Card } from 'react-bootstrap'
import ReactPlayer from 'react-player'
import styled from 'styled-components'

import DynamicImage from '~src/components/atoms/DynamicImage'
import Link from '~src/components/atoms/Link'

const StyledCard = styled(Card)`
  cursor: pointer;
  height: auto;
  max-height: 310px;

  &:hover {
    background-color: #343A40;
    border: 1px solid rgb(255,0,0, 0.3) !important;

    p {
      color: white;
      font-size: 15.6px;
      }

    Img {
      filter: saturate(1.5);
    }
  }
  Img {
    max-height: 187px;
  }

  @media (max-width: 991px) {
    height: 300px;

    Img {
      max-height: 133px;
    }
  }
  @media (max-width: 767px) {
    height: 320px;
    margin-bottom: 30px;

    Img {
      max-height: 190px;
    }
  }
  @media (max-width: 575px) {
    max-height: 270px;
    Img {
      max-height: 190px;
    }
  }
`
const Titulo = styled.h2`
  background-color: rgba(0, 0, 0, 0.6);
  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
  color: white;

  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 20.8px;
  font-weight: 600;
  line-height: 25px;
  margin-bottom: 0.5rem;
  position: absolute;
  text-align: center;
  text-shadow: 0px 0px 6px #000;
  top: 25%;

  @media (max-width: 991px) {
    font-size: 15px;
    top: 15%;
  }

  @media (max-width: 767px) {
    font-size: 18px;
    top: 30%;
  }
  @media (max-width: 575px) {
    font-size: 20px;
    top: 30%;
  }
  width: 100%;
`
const Entradilla = styled.p`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 15.3px;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 5px;
  margin-left: 5px;
  margin-top: 5px;
  text-align: left;
  text-indent: 10px;
`
const StyledDiv = styled.div`
  height: 190px;
  @media (max-width: 1081px) {
      max-height: 180px ;
    }

    @media (max-width: 991px) {
      max-height: 135px;
    }

    @media (max-width: 767px) {
      max-height: 190px;
    }

    @media (max-width: 500px) {
      max-height: 190px;
    }
`

const NoticiaRecomendada = ({ noticia }: { noticia: Noticia }) => (
  <Link to={noticia?.fields?.slug ?? '/'}>
    <StyledCard>
      {noticia.frontmatter.layout === 'Noticia con video'
        ? (
          <StyledDiv>
            <ReactPlayer width='100%' height='100%' url={noticia.frontmatter.link_del_video}/>
          </StyledDiv>
        )
        : (
          <DynamicImage image={noticia.frontmatter.thumbnail} height={190} />
        )
      }
      <Titulo>
        {noticia?.frontmatter.resumen}
      </Titulo>
      <Entradilla>
        {noticia?.frontmatter.entradilla}
      </Entradilla>
    </StyledCard>
  </Link>
)

export default NoticiaRecomendada
