import { faCommentDots } from '@fortawesome/free-regular-svg-icons/faCommentDots'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { DiscussionEmbed } from 'disqus-react'
import React from 'react'
import { Card } from 'react-bootstrap'
import styled from 'styled-components'

import { useClientSideRendering } from '~src/constants/utils'

const CommentsCard = styled(Card)`
  border-bottom: 1px solid rgba(0, 0, 0, 0.125);
  border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
  box-sizing: border-box;
  margin-bottom: 0;
  margin-top: 30px;

  .Body {
    padding: 10px;
  }
`

const CommentsIcon = styled(FontAwesomeIcon)`
  font-size: 20.8px;
  margin-left: 5px;
  margin-top: 2px;
`
const CommentsHeader = styled(Card.Header)`
  background-color: #F7F7F7;
  color: #212529;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 20.8px;
  font-weight: 600;
  position: sticky;
  text-align: left;
  top: 0;
`
export type CommentsProps = {
  post: Noticia | Carta | Analisis | Entrevista,
}
const Comments = (props: CommentsProps) => {
  const disqusPlugin = useClientSideRendering(() => {
    try {
      const disqusConfig = {
        url: 'https://3dnoticias.com.ar' + props.post?.fields?.slug,
        identifier: props.post?.id,
        title: props.post?.frontmatter?.title,
      }
      return (
        <DiscussionEmbed shortname='3d-noticias-1' config={disqusConfig} />
      )
    } catch {
      return null
    }
  })
  return (
    <CommentsCard>
      <CommentsHeader>
        ¿Qué opinás?
        <CommentsIcon icon={faCommentDots}/>
      </CommentsHeader>
      <CommentsCard.Body>
        {disqusPlugin}
      </CommentsCard.Body>
    </CommentsCard>

  )
}

export default Comments
