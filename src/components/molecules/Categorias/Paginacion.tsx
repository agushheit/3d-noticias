import { faChevronDown } from '@fortawesome/free-solid-svg-icons/faChevronDown'
import { faChevronUp } from '@fortawesome/free-solid-svg-icons/faChevronUp'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import styled, { css } from 'styled-components'

import Link from '~src/components/atoms/Link'

const Container = styled.ul`
  display: flex;
  flex-direction: row;
  height: 40px;
  justify-content: center;
  margin-top: 10px;
  width: 100%;
`

const Icon = styled(FontAwesomeIcon)`
  color: #222222;
  font-size: 16px;
  margin: 0 5px;
`

type ButtonProps = {
  disabled: boolean
  position: 'left' | 'right'
}
const PaginationButton = styled.button<ButtonProps>`   
  background-color: #fff;
  border: 1px solid #dee2e6;
  box-sizing: border-box;
  color: #222222;
  cursor: pointer;
  display: block;
  font-family: Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 16px;
  font-weight: 400;
  line-height: 1.25;
  list-style: none;
  margin-left: 0;
  padding: 0.5rem 0.75rem;
  position: relative;
  text-decoration: none;
  width: auto;

  @media (max-width: 468px){
    color: white;
    font-size: 0;
  }

  :focus {
    outline: none;

    svg {
      outline: none;
    }
  }

  ${props => props.position === 'left' && css`
    border-bottom-left-radius: 0.25rem;
    border-top-left-radius: 0.25rem;
  `}

  ${props => props.position === 'right' && css`
    border-bottom-right-radius: 0.25rem;
    border-top-right-radius: 0.25rem;
  `}

  ${props => props.disabled
    ? css`
      color: #6c757d;
      padding: 0.5rem 0.75rem;
      position: relative;
      text-decoration: none;
      width: auto;

      ${Icon} {
        color: #6c757d;
      }

      @media (max-width: 468px){
        color: white;
        font-size: 0;
      }
    `
    : css`
      &:hover {
        background-color: #ff0000;
        color: #ffffff;
        text-decoration: none;
        z-index: 2;

        ${Icon} {
          color: white;
        }
      }
  `
}
`

type Props = {
  nextPageText: string
  prevPageText: string
  context: PageContext<Analisis | Noticia | Denuncia | Carta | Entrevista>
}

const Paginacion = (props: Props) => {
  const { index, first, last } = props.context
  const previousPageNumber = index - 1 === 1 ? '' : (index - 1).toString()
  const previousPageUrl = `/${props.context.pathPrefix}/${previousPageNumber}`
  const nextPageUrl = `/${props.context.pathPrefix}/${index + 1}`
  return (
    <Container>
      <Link to={previousPageUrl} disabled={first}>
        <PaginationButton disabled={props.context.first} position='left'>
          <Icon icon={faChevronUp} />
          {props.prevPageText}
        </PaginationButton>
      </Link>
      <Link to={nextPageUrl} disabled={last}>
        <PaginationButton disabled={last} position='right'>
          {props.nextPageText}
          <Icon icon={faChevronDown} />
        </PaginationButton>
      </Link>
    </Container>
  )
}

export default Paginacion
