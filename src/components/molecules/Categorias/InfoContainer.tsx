import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons/faMapMarkerAlt'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import styled from 'styled-components'

const ContactInfo = styled.p`
color: #CF0000;
font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
font-size: 14.4px;
margin-bottom: 5px;
`
const Icon = styled(FontAwesomeIcon)`
  color: #CF0000;
  font-size: 14.4px;
  margin-right: 5px;
  margin-top: 3px;
`
const Container = styled.div`
border-bottom: 1px solid rgba(0, 0, 0, 0.1);
border-top: 1px solid rgba(0, 0, 0, 0.1);
display: flex;
flex-direction: column;
height: auto;
margin: 15px 0;
margin-bottom: 0;
padding: 10px 0;
width: 100%;

@media (max-width: 991px) {
      padding-left: 2%;
    }
`

type InfoProps = {
cursor?: string
// conMargenAbajo?: boolean
}

const Informacion = styled.div<InfoProps>`
cursor: ${props => props.cursor};
display: flex;
flex-direction: row;
height: auto;
width: 100%;

&:hover {
  ${ContactInfo} {
    color: #FF0000;
  }
  ${Icon} {
    color: #FF0000;
  }
}
`
/* ${props => props.conMargenAbajo && css`
margin-bottom: 40px;
`} */

const LocationIcon = styled(FontAwesomeIcon)`
  color: black;
  font-size: 14.4px;
  margin-right: 5px;
  margin-top: 2px;
`
const Ciudad = styled.p`
color: black;
font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
font-size: 14.4px;
margin-bottom: 5px;
`
const StyledA = styled.a`
  text-decoration: none !important;
`

const InfoContainer = () => (

  <Container>
    <Informacion>
      <LocationIcon icon={faMapMarkerAlt} />
      <Ciudad >Ciudad de Santa Fe</Ciudad>
    </Informacion>
    <StyledA>
      <Informacion cursor='pointer'>
        <Icon icon={faWhatsapp} />
        <ContactInfo>Línea de Whatsapp</ContactInfo>
      </Informacion>
    </StyledA>
    <StyledA href="mailto:info@3dnoticias.ar?subject=Consulta%20desde%20el%20sitio%20web%203DNoticias&amp;body=Escriba%20su%20consulta.%20Le%20responderemos%20a%20la%20brevedad.">
      <Informacion cursor='pointer'>
        <Icon icon={faEnvelope} />
        <ContactInfo>info@3dnoticias.ar</ContactInfo>
      </Informacion>
    </StyledA>
  </Container>
)

export default InfoContainer
