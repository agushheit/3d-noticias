import { faChevronRight } from '@fortawesome/free-solid-svg-icons/faChevronRight'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { Button } from 'react-bootstrap'
import styled from 'styled-components'

const Container = styled.div`
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  display: flex;
  flex-direction: column;
  margin-top: 15px;
`
const Contacto = styled.div`
  display: flex;
  flex-direction: row;
  height: auto;
  margin: auto;
  padding-bottom: 10px;
  width: 100%;
  @media (max-width: 991px) {
      padding-right: 30%;
      padding-left: 2%;
    }
`
const Info = styled.strong`
  box-sizing: border-box;
  color: #212529;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 14.4px;
  font-weight: bolder;
  line-height: 1.2;
  text-align: left;
`
const Icon = styled(FontAwesomeIcon)`
  font-size: 14.4px;
  margin-left: 5px;
  margin-top: 2px;

  @media (max-width: 1199px) {
    margin-left: 0;
  }
  @media (max-width: 991px) {
    margin-left: 5px;
  }
`
const Boton = styled(Button)`
  background-color: transparent;
  border: 1px solid #CF0000;
  border-radius: 0.25rem;
  box-sizing: border-box;
  color: #CF0000;
  cursor: pointer;
  font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';
  font-size: 13.44px !important;
  font-weight: 400;
  height: 35px;
  line-height: 17px;
  margin-left: auto;
  margin-top: -8px;
  padding: 6px 12px;
  text-align: left !important;
  text-decoration: none;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  width: 150px !important;

    &:hover {
      background-color: #FF0000;
      border-color: transparent;
      color: #FFFFFF;
    }
`
const StyledA = styled.a`
  margin-left: auto;
  text-decoration: none;
`
const ContainerDeContacto = () => (
  <Container>
    <Contacto>
      <Info>Enviar carta</Info>
      <Icon icon={faChevronRight}/>
      <StyledA href='mailto:redaccion@3dnoticias.ar' target='_blank' rel='noopener noreferrer' aria-label='Envianos un mail'>
        <Boton>Correo de lectores</Boton>
      </StyledA>
    </Contacto>
    <Contacto>
      <Info>Enviar denuncia</Info>
      <Icon icon={faChevronRight}/>
      <StyledA href='mailto:redaccion@3dnoticias.ar' target='_blank' rel='noopener noreferrer' aria-label='Envianos un mail'>
        <Boton>Denuncia ciudadana</Boton>
      </StyledA>
    </Contacto>
    <Contacto>
      <Info>Publicar en el sitio</Info>
      <Icon icon={faChevronRight}/>
      <StyledA href='mailto:comercial@3dnoticias.ar' target='_blank' rel='noopener noreferrer' aria-label='Envianos un mail'>
        <Boton >Anunciantes</Boton>
      </StyledA>
    </Contacto>
  </Container>
)

export default ContainerDeContacto
