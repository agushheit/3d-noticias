
import React from 'react'
import { Card, Col } from 'react-bootstrap'
import styled from 'styled-components'

import DynamicImage from '~src/components/atoms/DynamicImage'
import ShareButtons from '~src/components/atoms/ShareButtons'

const StyledCard = styled(Card)`
  border-color: #CF0000;
  margin-bottom: 15px;
  width: 100%;

  .card-body {
    padding: 15px;
    padding-bottom: 0 !important;
  }

  ${DynamicImage} {
    border-radius: 0.25rem;
    cursor: unset;
  }
`

const Titulo = styled.h2`
  box-sizing: border-box;
  color: #212529;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 24px;
  font-weight: 800;
  line-height: 1.2;
  margin-bottom: 0.75rem;
  margin-top: 0;
  text-align: left;
`
const Fecha = styled.p`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 18px;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 1rem;
  margin-top: 0;
  text-align: left;
`
const Categoria = styled.h1`
  box-sizing: border-box;
  color: #CF0000;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 20.8px;
  font-weight: 600;
  line-height: 1.2;
  margin-bottom: 0.5rem;
  margin-top: 0;
  text-align: left;
  text-transform: uppercase;
`
const Contenido = styled.p`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 19.2px;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 1rem;
  margin-top: 0;
  text-align: left;
  text-indent: 15px;
`

const StyledCol = styled(Col)`
  margin-bottom: 30px;
  padding-left: 5px !important;
  padding-right: 5px !important;
`
const DenunciaCards = ({ denuncia }: { denuncia: Denuncia }) => {
  const denunciaUrl = 'https://3dnoticias.com.ar' + denuncia.fields?.slug
  return (
    <StyledCard>
      <DynamicImage image={denuncia.frontmatter.thumbnail} height={230} />
      <StyledCol>
        <StyledCard.Body>
          <Categoria />
          <Titulo>
            {denuncia?.frontmatter.title}
          </Titulo>
          <Contenido>
            {denuncia?.frontmatter.entradilla}
          </Contenido>
          <Fecha>
            Santa Fe,
            {' '}
            {new Date(denuncia?.frontmatter.date).toLocaleDateString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' })}
          </Fecha>
          <ShareButtons link={denunciaUrl} />
        </StyledCard.Body>
      </StyledCol>
    </StyledCard>
  )
}
export default DenunciaCards
