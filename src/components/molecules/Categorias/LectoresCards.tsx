
import { faAngleRight } from '@fortawesome/free-solid-svg-icons/faAngleRight'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { Button, Card } from 'react-bootstrap'
import styled from 'styled-components'

import Link from '~src/components/atoms/Link'
import ShareButtons from '~src/components/atoms/ShareButtons'

const StyledCard = styled(Card)`
  border-color: #CF0000;
  margin-bottom: 15px;
  width: 100%;

  .Body{ 
    padding: 15px;
  }

  img { 
    transition: all 200ms ease-in-out !important;
    width: 100%;
    height: auto;
  }
  Img { 
        cursor: pointer !important;
        transition: 0.3s;

        &:hover {
          filter: saturate(1.5);
          transform: scale(1.2);
        }
  }
`
const Titulo = styled.h2`
  box-sizing: border-box;
  color: #CF0000;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 24px;
  font-weight: 800;
  line-height: 1.2;
  margin-bottom: 0.75rem;
  margin-top: 0;
  text-align: left;
  text-decoration: none !important;

  &:hover {
    text-decoration: none !important;
  }
`
const Fecha = styled.p`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 18px;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 1rem;
  margin-top: 0;
  text-align: left;
`
const Contenido = styled.p`
  box-sizing: border-box;
  color: #212529;
  font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
  font-size: 19.2px;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: 1rem;
  margin-top: 0;
  text-align: left;
  text-indent: 15px;
`
const StyledButton = styled(Button)`
  background-color: transparent;
  border: 1px solid #cc0000;
  color: #cc0000;
  font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';
  margin-bottom: 10px;
  transition: all 0.15s;
  width: fit-content;

  :hover {
    background-color: #cc0000;
    color: white;
  }
`
const Icon = styled(FontAwesomeIcon)`
    font-size: 16px;
    margin-left: 3px;
`
const ImgContainer = styled.div`
  overflow: hidden;
`
const LectoresCards = ({ carta }: {carta: Carta}) => {
  const cartaUrl = 'https://3dnoticias.com.ar' + carta.fields?.slug
  return (
    <StyledCard>
      <Link to={carta?.fields?.slug ?? '/'}>
        <ImgContainer>
          <img src={carta?.frontmatter?.thumbnail} alt={carta?.frontmatter?.title}/>
        </ImgContainer>
      </Link>
      <StyledCard.Body>
        <Link to={carta?.fields?.slug ?? '/'}>
          <Titulo>
            {carta?.frontmatter.title}
          </Titulo>
        </Link>
        <Contenido>
          {carta?.frontmatter.entradilla}
        </Contenido>
        <Fecha>
        Santa Fe,
          {' '}
          {new Date(carta?.frontmatter.date).toLocaleDateString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' })}
        </Fecha>
        <Link to={carta.fields?.slug ?? '/'}>
          <StyledButton>
            Continuar leyendo
            <Icon icon={faAngleRight}/>
          </StyledButton>
        </Link>
        <ShareButtons link={cartaUrl} />
      </StyledCard.Body>
    </StyledCard>
  )
}
export default LectoresCards
