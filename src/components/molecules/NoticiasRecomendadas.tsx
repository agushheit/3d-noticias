
import React from 'react'
import { Card } from 'react-bootstrap'
import ReactPlayer from 'react-player'
import styled, { css } from 'styled-components'

import Link from '~src/components/atoms/Link'

const StyledCard = styled(Card)`
  border: 1px solid #dee2e6;
  border-radius: 0.25rem;
  cursor: pointer;
  margin-bottom: 20px;
  padding: 5px;

  &:hover {
    h5 {
      color: #FF0000;
    }
  }

  @media (min-width: 1200px) {
    width: 300px;
  }
  @media (max-width: 1199px) {
    width: 260px;
  }
  @media (max-width: 991px) {
    width: 100%;
  }
`
const StyledDiv = styled.div`
  height: 187px;
  @media (max-width: 1081px) {
      max-height: 180px ;
    }

    @media (max-width: 991px) {
      max-height: 135px;
    }

    @media (max-width: 767px) {
      max-height: 190px;
    }

    @media (max-width: 500px) {
      max-height: 190px;
    }
`
const Titulo = styled.h5`
  box-sizing: border-box;
  color: #CF0000;
  font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
  font-size: 16.6px;
  font-weight: 600;
  line-height: 17px;
  margin-top: 5px;
  padding-left: 3px;
`

const ImgCard = styled.div<{ image: string }>`
  background-size: cover;
  height: 230px;
  width: 100%;

  ${props => css`
    background-image: url(${props.image});
  `}
`

const NoticiasRecomendadas = ({ noticia }: { noticia: Noticia }) => {
  return (
    <Link to={noticia?.fields?.slug ?? '/'}>
      <StyledCard>
        {noticia.frontmatter.layout === 'Noticia con video'
          ? (
            <StyledDiv>
              <ReactPlayer width='100%' height='100%' url={noticia.frontmatter.link_del_video} />
            </StyledDiv>
          )
          : (
            <ImgCard image={noticia?.frontmatter?.thumbnail} />
          )
        }
        <Titulo>
          {noticia?.frontmatter.resumen}
        </Titulo>
      </StyledCard>
    </Link>
  )
}

export default NoticiasRecomendadas
