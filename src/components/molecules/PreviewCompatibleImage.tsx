import Img, { GatsbyImageFluidProps } from 'gatsby-image'
import React from 'react'

type ImageProps = {
  imageInfo: {
    alt: string
    childImageSharp?: GatsbyImageFluidProps
    image: string | any
  }
  className?: string
}

const PreviewCompatibleImage = (props: ImageProps) => {
  const { alt = '', childImageSharp, image } = props.imageInfo
  const imageStyle = { borderRadius: '5px' }

  if (!!image && !!image.childImageSharp) {
    return (
      <Img className={props.className} style={imageStyle} fluid={image.childImageSharp.fluid} alt={alt} />
    )
  }

  if (childImageSharp) {
    return <Img className={props.className} style={imageStyle} fluid={childImageSharp.fluid} alt={alt} />
  }

  if (!!image && typeof image === 'string') { return <img style={imageStyle} src={image} alt={alt} /> }

  return <></>
}

export default PreviewCompatibleImage
