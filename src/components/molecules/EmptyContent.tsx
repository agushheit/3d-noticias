import { graphql, Link, useStaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import React from 'react'
import { Button, Card, Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'

const Micro = styled(Img)<FluidProps>`
  height: auto;
  width: 100%;
`
const Titulo = styled.h1`
  color: #CF0000;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 20.8px;
`
const Descripcion = styled.p`
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 18px;
  margin-top: 70px;
`
const Boton = styled(Button)`
  background-color: transparent;
  border: 1px solid #cc0000;
  color: #cc0000;
  font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';
  margin-bottom: 10px;
  transition: all 0.15s;
  width: fit-content;

  :hover {
    background-color: #cc0000;
    color: white;
  }
`
const StyledCard = styled(Card)`
  border-color: #CF0000;
    .card-body {
      padding: 15px;
    }
`
const StyledHeader = styled(Card.Header)`
  background-color: #ffebee;
  border-bottom: 1px solid #CF0000;
  height: fit-content;
  padding-bottom: 10px;
  padding-top: 15px;
`
const Empty = () => {
  const { imageSharp } = useStaticQuery(graphql`
    query {
      imageSharp: imageSharp(fluid: {originalName: {eq: "micro.jpg"}}) {
        fluid(quality: 100, maxWidth: 700) {
          ...GatsbyImageSharpFluid_withWebp_noBase64
        }
      }
    }
  `)
  return (
    <Container>
      <StyledCard>
        <StyledHeader>
          <Titulo>Parece que aún no hay contenido...</Titulo>
        </StyledHeader>
        <StyledCard.Body>
          <Row>
            <Col md={4}>
              <Micro fluid={imageSharp.fluid}/>
            </Col>
            <Col>
              <Descripcion>Te invitamos a seguir navegando por la página:</Descripcion>
              <Link to='/'>
                <Boton>Página principal</Boton>
              </Link>
            </Col>
          </Row>
        </StyledCard.Body>
      </StyledCard>

    </Container>
  )
}

export default Empty
