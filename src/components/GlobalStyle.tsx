import '@fortawesome/fontawesome-svg-core/styles.css'

import { config } from '@fortawesome/fontawesome-svg-core'
import { createGlobalStyle } from 'styled-components'

config.autoAddCss = false

const GlobalStyle = createGlobalStyle`
  /* Layout */
  html {
    box-sizing: border-box;
    overflow-y: scroll;
  }
  * {
    box-sizing: inherit;
  }
  *:before {
    box-sizing: inherit;
  }
  *:after {
    box-sizing: inherit;
  }

  body {
    color: #212529;
    font-family: 'Open Sans', Arial, sans-serif;
    font-weight: 400;
    font-display: swap;
    word-wrap: break-word;
    font-kerning: normal;
    -moz-font-feature-settings: "kern", "liga", "clig", "calt";
    -ms-font-feature-settings: "kern", "liga", "clig", "calt";
    -webkit-font-feature-settings: "kern", "liga", "clig", "calt";
    font-feature-settings: "kern", "liga", "clig", "calt";
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
  }
  /* Fonts */
  @font-face {
  font-family: "FontAwesome";
  src: url("/FontAwesome.otf");
}
`

export default GlobalStyle
