import 'normalize.css'

import React from 'react'
import AdSense from 'react-adsense'
import styled from 'styled-components'

import GlobalStyle from '~src/components/GlobalStyle'
import Footer from '~src/components/organisms/Footer'
import Navbar from '~src/components/organisms/Navbar'
import { useWindowSize } from '~src/constants/utils'

const PageContainer = styled.div`
  margin: 0;
  width: 100%;
`

const Main = styled.main`
  margin-bottom: 24px;
  margin-left: 0;
  margin-right: 0;
  min-height: calc(100vh - 298px);
`

const FooterContainer = styled.footer`
    background-color: #343a40 !important;
    box-sizing: border-box;
    display: flex;
    padding-bottom: 3rem !important;
    padding-top: 3rem !important;
    width: 100%;
`
const Publicidad = styled.div`
  bottom: 0;
  display: block;
  margin-bottom: 0;
  max-height: 100px !important;
  max-width: 575px;
  position: sticky;
  width: 100vw;
  z-index: 1030;
`

type Props = {
  children: React.ReactNode,
}

const Layout = ({ children }: Props): React.ReactElement => {
  const windowSize = useWindowSize()
  return (
    <>
      <GlobalStyle />
      <Navbar />
      <Main>
        <PageContainer>
          {children}
        </PageContainer>
      </Main>
      <FooterContainer>
        <PageContainer>
          <Footer />
        </PageContainer>
      </FooterContainer>
      {(windowSize?.width || 1000) < 575 && (
        <Publicidad>
          <AdSense.Google
            client='ca-pub-7785551334553916'
            slot='9190247708'
            style={{
              height: '100px',
              maxHeight: '100px',
              maxWidth: '575px',
              width: '100%',
              display: 'inline-block',
              format: 'rectangle, horizontal',
            }}
          />
          {typeof window !== 'undefined' &&
          <script>
            {`
              try {
                (window.adsbygoogle = window.adsbygoogle || []).push({});
              } catch (error) {    
              }              
            `}
          </script>
          }
        </Publicidad>
      )}
    </>
  )
}

export default Layout
