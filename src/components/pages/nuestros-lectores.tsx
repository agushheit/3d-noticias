import React from 'react'

import Date from '~src/components/atoms/Date'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import LectoresContainer from '~src/components/organisms/Categorias/LectoresContainer'
import MasVisto from '~src/components/organisms/Categorias/MasVisto'

type Props = {
  pageContext: PageContext<Carta>
  masVisto: GraphqlArray<Noticia>
  empty?: boolean
}

const DenunciasContent = (props: Props) => (
  <>
    <SocialMediaContainer/>
    <Date/>
    <LectoresContainer context={props.pageContext}/>
    <MasVisto noticias={props.masVisto}/>
  </>
)

export default DenunciasContent
