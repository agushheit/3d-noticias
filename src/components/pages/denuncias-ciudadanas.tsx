import React from 'react'

import Date from '~src/components/atoms/Date'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import DenunciasContainer from '~src/components/organisms/Categorias/DenunciasContainer'
import MasVisto from '~src/components/organisms/Categorias/MasVisto'

type Props = {
  masVisto: GraphqlArray<Noticia>
  context: PageContext<Denuncia>
  empty?: boolean
}

const DenunciasContent = (props: Props) => (
  <>
    <SocialMediaContainer/>
    <Date/>
    <DenunciasContainer context={props.context} empty={props.empty}/>
    <MasVisto noticias={props.masVisto}/>
  </>
)

export default DenunciasContent
