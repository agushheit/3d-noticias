import React from 'react'

import Date from '~src/components/atoms/Date'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import ContactContainer from '~src/components/organisms/Categorias/ContactContainer'

const Contacto = () => (
  <>
    <SocialMediaContainer/>
    <Date/>
    <ContactContainer/>
  </>
)

export default Contacto
