import React from 'react'

import Date from '~src/components/atoms/Date'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import DeportesContainer from '~src/components/organisms/Categorias/DeportesContainer'
import MasVisto from '~src/components/organisms/Categorias/MasVisto'

type Props = {
  noticias: Noticia[],
  masVisto: GraphqlArray<Noticia>
  context: PageContext<Noticia>
}

const DeportesContent = (props: Props) => (
  <>
    <SocialMediaContainer/>
    <Date/>
    <DeportesContainer context={props.context} noticias={props.noticias}/>
    <MasVisto noticias={props.masVisto}/>
  </>
)

export default DeportesContent
