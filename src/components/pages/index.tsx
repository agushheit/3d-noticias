import React from 'react'

import Fecha from '~src/components/atoms/Date'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import HomeCarousel from '~src/components/organisms/Home/Carousel'
import HomeContainer from '~src/components/organisms/Home/HomeContainer'

const IndexContent = () => (
  <>
    <HomeCarousel />
    <SocialMediaContainer />
    <Fecha />
    <HomeContainer/>
  </>
)

export default IndexContent
