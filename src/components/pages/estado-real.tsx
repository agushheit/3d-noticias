import React from 'react'

import Date from '~src/components/atoms/Date'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import EstadoContainer from '~src/components/organisms/Categorias/EstadoContainer'
import MasVisto from '~src/components/organisms/Categorias/MasVisto'

type Props = {
  noticias: Noticia[],
  masVisto: GraphqlArray<Noticia>
  context: PageContext<Noticia>
}

const EstadoRealContent = (props: Props) => (
  <>
    <SocialMediaContainer/>
    <Date/>
    <EstadoContainer context={props.context} noticias={props.noticias}/>
    <MasVisto noticias={props.masVisto}/>
  </>
)

export default EstadoRealContent
