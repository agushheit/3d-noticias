import React from 'react'

import Date from '~src/components/atoms/Date'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import CampoContainer from '~src/components/organisms/Categorias/CampoContainer'
import MasVisto from '~src/components/organisms/Categorias/MasVisto'

type Props = {
  noticias: Noticia[],
  masVisto: GraphqlArray<Noticia>
  context: PageContext<Noticia>
}

const CampoContent = (props: Props) => (
  <>
    <SocialMediaContainer/>
    <Date/>
    <CampoContainer context={props.context} noticias={props.noticias}/>
    <MasVisto noticias={props.masVisto}/>
  </>
)

export default CampoContent
