import React from 'react'

import Date from '~src/components/atoms/Date'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import AgendaContainer from '~src/components/organisms/Categorias/AgendaContainer'
import MasVisto from '~src/components/organisms/Categorias/MasVisto'

type Props = {
  noticias: Noticia[],
  masVisto: GraphqlArray<Noticia>
  context: PageContext<Noticia>
}

const AgendaCiudadanaContent = (props: Props) => (
  <>
    <SocialMediaContainer/>
    <Date/>
    <AgendaContainer context={props.context} noticias={props.noticias}/>
    <MasVisto noticias={props.masVisto}/>
  </>
)

export default AgendaCiudadanaContent
