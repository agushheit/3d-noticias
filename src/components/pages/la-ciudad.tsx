import React from 'react'

import Date from '~src/components/atoms/Date'
import SocialMediaContainer from '~src/components/molecules/Home/SocialMediaContainer'
import CiudadContainer from '~src/components/organisms/Categorias/CiudadContainer'
import MasVisto from '~src/components/organisms/Categorias/MasVisto'

type Props = {
  noticias: Noticia[],
  masVisto: GraphqlArray<Noticia>
  context: PageContext<Noticia>
}

const CiudadContent = (props: Props) => (
  <>
    <SocialMediaContainer/>
    <Date/>
    <CiudadContainer context={props.context} noticias={props.noticias}/>
    <MasVisto noticias={props.masVisto}/>
  </>
)

export default CiudadContent
