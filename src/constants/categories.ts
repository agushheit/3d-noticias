export const categories = {
  agenda: 'Agenda Ciudadana',
  estado: 'Estado Real',
  analisis: 'Análisis y opinión',
  protagonistas: 'Protagonistas',
  lectores: 'Nuestros Lectores',
  denuncias: 'Denuncias Ciudadanas',
  ciudad: 'La Ciudad',
  campo: 'El Campo',
  deportes: 'Deportes',
  cultura: 'Cultura',
}
