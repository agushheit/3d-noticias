/* eslint-disable camelcase */
import axios from 'axios'
import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import { call, put, takeLatest } from 'redux-saga/effects'

import { useGoogleAnalytics } from './utils'

type WeatherState = {
  imagePathClimaDelDia: string
  imagePathClimaAhora: string
  temperatura: string
  viento: string
  temperaturaMaxima: string
  temperaturaMinima: string
}

export type State = {
  weatherData: WeatherState
}

// Initial state
const initialState: State = {
  weatherData: {
    viento: '',
    temperatura: '',
    temperaturaMaxima: '',
    temperaturaMinima: '',
    imagePathClimaAhora: '',
    imagePathClimaDelDia: '',
  },
}

// Actions
const GET_WEATHER_DATA = 'GET_WEATHER_DATA'
export const getWeatherData = () => ({
  type: GET_WEATHER_DATA,
})

const SET_WEATHER_DATA = 'SET_WEATHER_DATA'
function * setWeatherData () {
  let weatherData
  try {
    const tuTiempoUrl = 'https://api.tutiempo.net/json/?lan=es&apid=4xTzaqqq4z44jtz&lid=43346'
    const { data } = yield call(() => axios.get(tuTiempoUrl))
    const [sunsetHour, sunsetMinutes] = (data.day1?.sunset.split(':') ?? []).map(Number)
    const sunsetDate = new Date()
    sunsetDate.setHours(sunsetHour)
    sunsetDate.setMinutes(sunsetMinutes)
    const esDeDia = new Date() <= sunsetDate
    const imagePathClimaDelDia = esDeDia
      ? `weather/${data.day1?.icon}.png`
      : `weather/wmi/02/${data.day1?.moon_phases_icon}.png`
    const imagePathClimaAhora = `weather/${data.hour_hour?.hour1.icon}.png`
    weatherData = {
      imagePathClimaDelDia,
      imagePathClimaAhora,
      temperatura: data.hour_hour?.hour1.temperature.toString(),
      viento: data.hour_hour?.hour1.wind.toString(),
      temperaturaMaxima: data.day1?.temperature_max.toString(),
      temperaturaMinima: data.day1?.temperature_min.toString(),
    }
  } catch (e) {
    weatherData = {
      imagePathClimaDelDia: 'weather/1.png',
      imagePathClimaAhora: 'weather/12.png',
      temperatura: '12', // data.hour_hour?.hour1.temperature.toString() ?? '',
      viento: '9', // data.hour_hour?.hour1.wind.toString() ?? '',
      temperaturaMaxima: '20', // : data.day1?.temperature_max.toString() ?? '',
      temperaturaMinima: '10', // : data.day1?.temperature_min.toString() ?? '',
    }
  } finally {
    yield put({ type: SET_WEATHER_DATA, payload: { weatherData } })
  }
}

function * weatherSaga () {
  yield takeLatest('GET_WEATHER_DATA', setWeatherData)
}

// Reducer
const reducer = (state = initialState, action): State => {
  switch (action.type) {
  case SET_WEATHER_DATA: {
    const { weatherData } = action.payload
    return { ...state, weatherData }
  }
  default:
    return state
  }
}

// preloadedState will be passed in by the Gatsby Redux plugin
export default preloadedState => {
  const sagaMiddleware = createSagaMiddleware()
  useGoogleAnalytics()
  const store = createStore(
    reducer,
    preloadedState,
    composeWithDevTools(applyMiddleware(sagaMiddleware)),
  )
  sagaMiddleware.run(weatherSaga)
  return store
}
