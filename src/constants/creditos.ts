/* eslint-disable no-console */
const styles = [
  'border: 1px solid #CF0000',
  'color: #CF0000',
  'display: block',
  'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)',
  'box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 5px 3px -5px rgba(0, 0, 0, 0.5), 0 -13px 5px -10px rgba(255, 255, 255, 0.4) inset',
  'line-height: 40px',
  'text-align: center',
  'font-weight: bold',
].join(';')

export default () => {
  const equipo = [
    {
      rol: 'Arquitectura de software & DevOps',
      nombre: 'Franco Rodríguez Roura',
      url: 'https://www.linkedin.com/in/franco-roura/',
    },
    {
      rol: 'Diseño gráfico & Frontend Development',
      nombre: 'Isabel Roura',
      url: 'https://www.linkedin.com/in/isabelroura/',
    },
    {
      rol: 'Diseño gráfico & Frontend Development',
      nombre: 'Agustina Heit',
      url: 'https://www.linkedin.com/in/j-agustina-heit/',
    },
  ]
  equipo.map(miembro => {
    console.log(`%c ${miembro.nombre} - ${miembro.rol} ${miembro.url} `, styles)
  })
}
