import { navigate } from 'gatsby'
import { useEffect, useState } from 'react'
import ReactGA from 'react-ga'

export const goTo = (somewhere: string | undefined) => () => navigate(somewhere ?? '/')

export const shortened = (text: string, maxChars: number) => {
  return text?.length > maxChars ? text.substring(0, maxChars) + '...' : text
}

export const useClientSideRendering = (callback: AnonymousFunction) => {
  if (typeof window !== 'undefined') {
    return callback()
  }
}

type WindowSize = {
  width?: number
  height?: number
} | undefined

export const useWindowSize = (): WindowSize => useClientSideRendering(() => {
  const [windowSize, setWindowSize] = useState<WindowSize>({
    width: undefined,
    height: undefined,
  })

  useEffect(() => {
    // Handler to call on window resize
    function handleResize () {
      // Set window width/height to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      })
    }

    // Add event listener
    window.addEventListener('resize', handleResize)

    // Call handler right away so state gets updated with initial window size
    handleResize()

    // Remove event listener on cleanup
    return () => window.removeEventListener('resize', handleResize)
  }, []) // Empty array ensures that effect is only run on mount

  return windowSize
})

export const useGoogleAnalytics = () => useClientSideRendering(() => {
  ReactGA.initialize('UA-179951556')
})

export const usePageView = () => useClientSideRendering(() => {
  ReactGA.pageview(window.location.pathname + window.location.search)
})
