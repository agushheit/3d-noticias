import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/templates/noticia'), 400))

type PageProps = {
  data: {
    noticia: Noticia
    masVisto: GraphqlArray<Noticia>
  }
}

const NoticiaPage = (props: PageProps) => {
  const { noticia, masVisto } = props.data
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        seoTitle={noticia.frontmatter.title}
        imagePath={noticia.localImage?.childImageSharp.fluid.src}
        imageWidth={noticia.localImage?.childImageSharp.fluid.presentationWidth}
        imageHeight={noticia.localImage?.childImageSharp.fluid.presentationHeight}
        urlPath={noticia.fields?.slug ?? ''}
        description={noticia.frontmatter.entradilla ?? ''}
        ogType='article'
      />
      <Content fallback={Spinner} masVisto={masVisto} noticia={noticia}/>
    </PageBase>
  )
}

export default NoticiaPage

export const pageQuery = graphql`
  query UltimaEntrevista($id: String!) {
    masVisto: allMarkdownRemark(
      sort: {order: DESC, fields: [frontmatter___date]},
      limit: 4,
      filter: {frontmatter: {category: {nin: ["Denuncias", "Protagonistas"], in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Estado Real", "Deportes"]}}}) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            url
            layout
            resumen
            author
            entradilla
            category
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
    noticia: markdownRemark(id: { eq: $id }) {
      id
      html
      fields {
        slug
      }
      frontmatter {
        author
        category
        entradilla
        date(formatString: "MM/DD/YYYY")
        title
      }
    }
  }
`
