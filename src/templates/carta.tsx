import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/templates/carta'), 400))

type PageProps = {
  data: {
    carta: Carta
    masVisto: GraphqlArray<Noticia>
  }
}

const CartaPage = (props: PageProps) => {
  const { carta, masVisto } = props.data
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        noBaseUrl
        seoTitle={carta.frontmatter.title}
        imagePath={carta.frontmatter.thumbnail}
        urlPath={carta.fields?.slug ?? ''}
        description={carta.frontmatter.entradilla ?? ''}
        ogType='article'
      />
      <Content fallback={Spinner} masVisto={masVisto} carta={carta}/>
    </PageBase>
  )
}

export default CartaPage

export const pageQuery = graphql`
  query CartaByID($id: String!) {
    masVisto: allMarkdownRemark(
      sort: {order: DESC, fields: [frontmatter___date]},
      limit: 4,
      filter: {frontmatter: {category: {nin: ["Denuncias", "Protagonistas"], in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Estado Real", "Deportes"]}}}) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            url
            layout
            resumen
            thumbnail
            author
            entradilla
            category
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
    carta: markdownRemark(id: { eq: $id }) {
      id
      html
      fields {
        slug
      }
      frontmatter {
        author
        category
        thumbnail
        entradilla
        date(formatString: "MM/DD/YYYY")
        title
      }
    }
  }
`
