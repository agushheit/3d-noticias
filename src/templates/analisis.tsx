import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/templates/analisis'), 400))

type PageProps = {
  data: {
    analisis: Analisis
    masVisto: GraphqlArray<Noticia>
  }
  context: PageContext<Analisis>
}

const AnalisisPage = (props: PageProps) => {
  const { analisis, masVisto } = props.data
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        noBaseUrl
        seoTitle={analisis.frontmatter.title}
        imagePath={analisis.frontmatter.thumbnail}
        description={analisis.frontmatter.author}
        urlPath={analisis.fields?.slug ?? ''}
        ogType='article'
      />
      <Content fallback={Spinner} masVisto={masVisto} analisis={analisis} />
    </PageBase>
  )
}

export default AnalisisPage

export const pageQuery = graphql`
  query UltimoAnalisis($id: String!) {
    masVisto: allMarkdownRemark(
      sort: {order: DESC, fields: [frontmatter___date]},
      limit: 4,
      filter: {frontmatter: {category: {nin: ["Denuncias", "Protagonistas"], in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Estado Real", "Deportes", "Cultura"]}}}) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            url
            layout
            resumen
            thumbnail
            author
            entradilla
            category
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
    analisis: markdownRemark(id: {eq: $id}) {
      fields {
        slug
      }
      frontmatter {
        author
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
      }
      html
    }
  }
`
