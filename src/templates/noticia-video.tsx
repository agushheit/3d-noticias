import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/templates/noticia-video'), 400))

type PageProps = {
  data: {
    noticiaVideo: Noticia
    masVisto: GraphqlArray<Noticia>
  }
}
const NoticiaPage = (props: PageProps) => {
  const { noticiaVideo, masVisto } = props.data
  const videoUrl = noticiaVideo.frontmatter.link_del_video
  let videoThumbnailUrl
  if (videoUrl) {
    const videoId = videoUrl.includes('youtu.be')
      ? videoUrl.split('/').pop()
      : videoUrl.split('?v=').pop()
    videoThumbnailUrl = `https://img.youtube.com/vi/${videoId}/0.jpg`
  }
  const thumbnailUrl = videoThumbnailUrl || noticiaVideo.localImage?.childImageSharp.fluid.src
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        noBaseUrl
        seoTitle={noticiaVideo.frontmatter.title}
        imagePath={thumbnailUrl}
        imageWidth={noticiaVideo.localImage?.childImageSharp.fluid.presentationWidth}
        imageHeight={noticiaVideo.localImage?.childImageSharp.fluid.presentationHeight}
        urlPath={noticiaVideo.fields?.slug ?? ''}
        description={noticiaVideo.frontmatter.entradilla ?? ''}
        ogType='article'
      />
      <Content fallback={Spinner} masVisto={masVisto} noticia={noticiaVideo}/>
    </PageBase>
  )
}

export default NoticiaPage

export const pageQuery = graphql`
  query NoticiaVideoByID($id: String!) {
    masVisto: allMarkdownRemark(
      sort: {order: DESC, fields: [frontmatter___date]},
      limit: 4,
      filter: {id: { ne: $id }, frontmatter: {category: {nin: ["Denuncias", "Protagonistas"], in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Estado Real", "Deportes"]}}}) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            url
            layout
            resumen
            thumbnail
            author
            entradilla
            category
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
    noticiaVideo: markdownRemark(id: { eq: $id }) {
      id
      html
      fields {
        slug
      }
      frontmatter {
        author
        category
        thumbnail
        entradilla
        date(formatString: "MM/DD/YYYY")
        title
        url
        link_del_video
      }
    }
  }
`
