import loadable from '@loadable/component'
import { graphql } from 'gatsby'
import pMinDelay from 'p-min-delay'
import React from 'react'
import { useDispatch } from 'react-redux'

import Seo from '~src/components/atoms/Seo'
import Spinner from '~src/components/atoms/Spinner'
import PageBase from '~src/components/PageBase'
import { getWeatherData } from '~src/constants/state'

const Content = loadable(() => pMinDelay(import('~src/components/templates/cultura'), 400))

type PageProps = {
  data: {
    noticia: Noticia
    masVisto: GraphqlArray<Noticia>
  }
}
const CulturaPage = (props: PageProps) => {
  const { noticia, masVisto } = props.data
  const dispatch = useDispatch()
  dispatch(getWeatherData())
  return (
    <PageBase>
      <Seo
        noBaseUrl
        seoTitle={noticia.frontmatter.title}
        imagePath={noticia.frontmatter.thumbnail}
        urlPath={noticia.fields?.slug ?? ''}
        description={noticia.frontmatter.entradilla ?? ''}
        ogType='article'
      />
      <Content fallback={Spinner} masVisto={masVisto} noticia={noticia}/>
    </PageBase>
  )
}

export default CulturaPage

export const pageQuery = graphql`
  query Cultura($id: String!) {
    masVisto: allMarkdownRemark(
      sort: {order: DESC, fields: [frontmatter___date]},
      limit: 4,
      filter: {id: { ne: $id }, frontmatter: {category: {nin: ["Denuncias", "Protagonistas"], in: ["El Campo", "La Ciudad", "Agenda Ciudadana", "Estado Real", "Deportes"]}}}) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            url
            layout
            thumbnail
            resumen
            author
            entradilla
            category
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
    noticia: markdownRemark(id: { eq: $id }) {
      id
      html
      fields {
        slug
      }
      frontmatter {
        author
        thumbnail
        category
        entradilla
        date(formatString: "MM/DD/YYYY")
        title
      }
    }
  }
`
