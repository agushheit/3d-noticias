const path = require('path')

module.exports = {
  pathPrefix: '3d-noticias',
  siteMetadata: {
    title: '3D Noticias',
    description: 'Portal de noticias de Santa Fe',
    author: '3D Noticias',
    siteUrl: "https://3dnoticias.com.ar/"
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: 'gatsby-plugin-eslint',
      options: {
        test: /\.js$|\.jsx$/,
        exclude: /(node_modules|.cache|public)/,
        options: {
          emitWarning: true,
          failOnError: false,
        },
      },
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-plugin-alias-imports',
      options: {
        alias: {
          '~src': path.join(__dirname, 'src'),
        },
        extensions: ['jsx', 'js'],
      },
    },
    'gatsby-plugin-offline',
    {
      resolve: 'gatsby-plugin-remove-console',
      options: {
        exclude: ['log'],
        include: ['warn', 'error'],
      }
    },
    {
      resolve: `gatsby-plugin-webfonts`,
      options: {
        fonts: {
          google: [
            {
              family: "Open Sans",
              variants: ["400", "600", "700"],
              fontDisplay: 'swap',
            },
            {
              family: "Work Sans",
              variants: ["600", "700"],
              fontDisplay: 'swap',
            },
          ],
        },
        usePreconnect: true,
      },
    },
    'gatsby-plugin-styled-components',
    'gatsby-plugin-sass',
    'gatsby-plugin-loadable-components-ssr',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    `gatsby-transformer-remark`,
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /images\/.*\.svg/,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-typescript',
      options: {
        isTSX: true, // defaults to false
        allExtensions: true, // defaults to false
      },
    },
    {
      resolve: `gatsby-plugin-purgecss`,
      options: {
        printRejected: true, // Print removed selectors and processed file names
        develop: true, // Enable while using `gatsby develop`
        // tailwind: true, // Enable tailwindcss support
        // whitelist: ['whitelist'], // Don't remove this selector
        // ignore: ['/ignored.css', 'prismjs/', 'docsearch.js/'], // Ignore files/folders
        purgeOnly: ['node_modules/bootstrap/'], // Purge only these files/folders
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        icon: `${__dirname}/src/images/newpwaicon.png`,
        icon_options: {
          purpose: `any maskable`,
        },
        name: `3D Noticias`,
        short_name: `3D Noticias`,
        start_url: `/`,
        background_color: `#343a40`,
        theme_color: `#343a40`,
        display: `standalone`,
        cache_busting_mode: 'none'
      },
    },
    'disqus-react',
    'gatsby-plugin-offline',
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-build-date`,
      options: {
        formatAsDateString: false,
        formatting: {
          format: 'DD/MM/YYYY-HH:mm:ss',
          utc: true,
        },
      },
    },
    {
      resolve: `gatsby-plugin-react-redux`,
      options: {
        pathToCreateStoreModule: `${__dirname}/src/constants/state`,
        serialize: {
          space: 0,
          isJSON: true,
          unsafe: false,
          ignoreFunction: true,
        },
      },
    },
  ],
}
