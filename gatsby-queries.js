const graphql = query => query

const estadoRealQuery = graphql`
estadoReal: allMarkdownRemark(sort: {order: DESC, fields: [frontmatter___date]}, filter: {frontmatter: {category: {eq: "Estado Real"}}}) {
  edges {
    node {
      excerpt(pruneLength: 400)
      id
      fields {
        slug
      }
      frontmatter {
        author
        category
        entradilla
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
        url
        link_del_video
        layout
      }
    }
  }
}
`

const agendaCiudadanaQuery = graphql`
agendaCiudadana: allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] } filter: { frontmatter: { category: { eq: "Agenda Ciudadana" } } }) {
  edges {
    node {
      excerpt(pruneLength: 400)
      id
      fields {
        slug
      }
      frontmatter {
        author
        category
        entradilla
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
        url
        link_del_video
        layout
      }
    }
  }
}
`

const elCampoQuery = graphql`
elCampo: allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] } filter: { frontmatter: { category: { eq: "El Campo" } } } ) {
  edges {
    node {
      excerpt(pruneLength: 400)
      id
      fields {
        slug
      }
      frontmatter {
        author
        category
        entradilla
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
        url
        link_del_video
        layout
      }
    }
  }
}
`

const laCiudadQuery = graphql`
laCiudad: allMarkdownRemark(
    sort: { order: DESC, fields: [frontmatter___date] }
    filter: { frontmatter: { category: { eq: "La Ciudad" } } }
  ) {
    edges {
      node {
        excerpt(pruneLength: 400)
        id
        fields {
          slug
        }
      frontmatter {
        author
        category
        entradilla
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
        url
        link_del_video
        layout
      }
    }
  }
}
`

const deportesQuery = graphql`
deportes: allMarkdownRemark(
  sort: { order: DESC, fields: [frontmatter___date] }
  filter: { frontmatter: { category: { eq: "Deportes" } } }
) {
  edges {
    node {
      excerpt(pruneLength: 400)
      id
      fields {
        slug
      }
      frontmatter {
        author
        category
        entradilla
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
        url
        link_del_video
        layout
      }
    }
  }
}
`

const analisisYOpinionQuery = graphql`
ultimoAnalisis: allMarkdownRemark(
  sort: { order: DESC, fields: [frontmatter___date] }
  filter: { frontmatter: { category: { eq: "analisis" } } }
) {
  edges {
    node {
      excerpt(pruneLength: 400)
      id
      fields {
        slug
      }
      html
      frontmatter {
        author
        category
        entradilla
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
        url
      }
    }
  }
}
`

const protagonistasQuery = graphql`
ultimaEntrevista: allMarkdownRemark(
  sort: { order: DESC, fields: [frontmatter___date] }
  filter: { frontmatter: { category: { eq: "Protagonistas" } } }
) {
  edges {
    node {
      excerpt(pruneLength: 400)
      id
      fields {
        slug
      }
      html
      frontmatter {
        author
        category
        entradilla
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
        url
      }
    }
  }
}
`

const denunciasQuery = graphql`
denunciasCiudadanas: allMarkdownRemark(
  sort: { order: DESC, fields: [frontmatter___date] }
  filter: { frontmatter: { category: { eq: "Denuncias Ciudadanas" } } }
) {
  edges {
    node {
      excerpt(pruneLength: 400)
      id
      fields {
        slug
      }
      frontmatter {
        author
        category
        entradilla
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
        url
      }
    }
  }
}
`

const lectoresQuery = graphql`
cartasDeLectores: allMarkdownRemark(
  sort: { order: DESC, fields: [frontmatter___date] }
  filter: { frontmatter: { category: { eq: "lectores" } } }
) {
  edges {
    node {
      excerpt(pruneLength: 400)
      id
      fields {
        slug
      }
      frontmatter {
        author
        category
        entradilla
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
        url
        layout
      }
    }
  }
}
`

const culturaQuery = graphql`
cultura: allMarkdownRemark(
  sort: { order: DESC, fields: [frontmatter___date] }
  filter: { frontmatter: { category: { eq: "Cultura" } } }
) {
  edges {
    node {
      excerpt(pruneLength: 400)
      id
      fields {
        slug
      }
      html
      frontmatter {
        author
        category
        entradilla
        thumbnail
        date(formatString: "MM/DD/YYYY")
        title
        url
      }
    }
  }
}
`

module.exports = {
  elCampoQuery,
  laCiudadQuery,
  lectoresQuery,
  denunciasQuery,
  estadoRealQuery,
  protagonistasQuery,
  agendaCiudadanaQuery,
  analisisYOpinionQuery,
  deportesQuery,
  culturaQuery,
}
