// tslint-disable
import React from 'react'
export const onPreRenderHTML = ({
  getHeadComponents,
  replaceHeadComponents,
  replacePostBodyComponents,
  getPostBodyComponents,
}) => {
  const headComponents = getHeadComponents().concat([
    <script
      key="googleAds"
      data-ad-client="ca-pub-7785551334553916"
      async
      src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
    />,
  ])
  headComponents.sort((a, b) => {
    if (a.type === 'meta') {
      return -1
    } else if (b.type === 'meta') {
      return 1
    }
    return 0
  })
  replaceHeadComponents(headComponents)

  const bodyComponents = getPostBodyComponents().concat([
    <script key="googleAnalyticsSdk" async src="https://www.googletagmanager.com/gtag/js?id=G-8D8S00K1M2" />,
    <script key="googleAnalyticsInit"
      dangerouslySetInnerHTML={{
        __html: `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-8D8S00K1M2');
      `,
      }}
    />,
  ])
  replacePostBodyComponents(bodyComponents)
}
