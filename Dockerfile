FROM node:lts-slim
# Install python/pip
ENV PYTHONUNBUFFERED=1
RUN apt update
RUN apt install -y python3 python3-pip locales
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8    
RUN pip3 install --no-cache --upgrade pip setuptools
RUN pip3 install awscli 